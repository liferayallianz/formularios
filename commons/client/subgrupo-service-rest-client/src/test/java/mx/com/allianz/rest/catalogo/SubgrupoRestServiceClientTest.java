/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.commons.catalogos.dto.SubgrupoDTO;
import mx.com.allianz.rest.catalogo.client.subgrupo.AllianzSubgrupoClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;


public class SubgrupoRestServiceClientTest {

	AllianzSubgrupoClient client;
	
	Map<String, Object> params; 

//	@Before
	public void generaCliente() {
		
		client = new AlliazRestClientFactory().buildClient(AllianzSubgrupoClient.class);	
		
		params = new HashMap<>(); 
	}
	


	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testGetSubgrupo() {
		System.out.println("Get test Subgrupo .... \n");
		List<SubgrupoDTO> lista = client.getSubgrupo();
		//lista.forEach((Subgrupo) -> System.out.println((Subgrupo)));
		assertNotNull(lista);
		System.out.println("not null 1:   ->"+lista.size());
		assertEquals(4, lista.size());
		
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("id_SubGrupo", 0);
		params.put("codigoSubGrupo","5565bmx");
		params.put("codigoGrupoPago","556865-bmx");
		params.put("descripcionSubGrupo","Banco bmx de mexico");
		params.put("codigoPais","492");
		params.put("codigoEstado","14");
		params.put("codigoCiudad","234");
		params.put("codigoMunicipio","534");
		params.put("direccion","Calle las palmas por que esta bien");
		params.put("nombreContacto","Giovanni");
		params.put("telefono","55674879");
		params.put("zip","54123");
		params.put("colonia","narvarte");
		params.put("stsCodigoSubGrupo","589uuh");
		params.put("tipoId","66");
		params.put("numeroId","2");
		params.put("dvId","543hy");
		
		
		List<SubgrupoDTO> lista = client.findSubgrupo(params);
		assertNotNull(lista);
		System.out.println("not null 2:   ->"+lista.size());
		assertEquals(1, lista.size());
	}

	//	@Test
	public void testFindSubgrupoByTwoCriteria() {
		System.out.println("Find test by Subgrupo Two Criterial .... \n");

		params.put("codigoEstado","14");
		params.put("codigoCiudad","234");
		
		List<SubgrupoDTO> lista = client.findSubgrupo(params);
		assertNotNull(lista);
		System.out.println("not null 3:   ->"+lista.size());
		assertEquals(4, lista.size());
		
	}


	//	@Test
	public void testFindSubgrupoByOneCriteria() {
		System.out.println("Find test by Subgrupo One Criterial .... \n");

		params.put("stsCodigoSubGrupo","589uuh");
		
		List<SubgrupoDTO> lista =  client.findSubgrupo(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:   ->"+lista.size());
		assertEquals(4, lista.size());

	}
}
