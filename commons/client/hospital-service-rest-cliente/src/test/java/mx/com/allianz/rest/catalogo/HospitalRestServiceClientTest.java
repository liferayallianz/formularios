/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.commons.catalogos.dto.HospitalDTO;
import mx.com.allianz.rest.catalogo.client.hospital.AllianzHospitalClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;

/**
 * @author 
 *
 */
public class HospitalRestServiceClientTest {

	AllianzHospitalClient client;
	Map<String, Object> params; 

//	@Before
	public void generaCliente() {
		client = new AlliazRestClientFactory().buildClient(AllianzHospitalClient.class);	
		params = new HashMap<>(); 
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos}.
	 */


	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos)}.
	 */
	//	@Test
	public void testGetHospital() {
		System.out.println("Get test Hospital .... \n");
		List<HospitalDTO> lista = client.getHospitales();
		//lista.forEach((estado) -> System.out.println(estado));
		assertNotNull(lista);
		System.out.println("not null 1:  ->"+lista.size());
		assertEquals(413, lista.size());
		
	}
	
	//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("id_Hospital", 2);
		params.put("codigo_Grupo", 94);
		params.put("codigo_SubGrupo", 35);
		params.put("codigo_Estado", 12);
		params.put("descripcion_Hospital", "SANATORIO DEL SAGRADO CORAZON, S.A.");
		
		
		
		List<HospitalDTO> lista = client.findHospitales(params);
		assertNotNull(lista);
		System.out.println("not null 2:  ->"+lista.size());
		assertEquals(0, lista.size());
	}

	//	@Test
	public void testFindHospitalByTwoCriteria() {
		System.out.println("Find test by Hospital Two Criterial .... \n");

		params.put("codigo_Grupo", 94);
		params.put("codigo_SubGrupo", 42);
		
		List<HospitalDTO> lista = client.findHospitales(params);
		assertNotNull(lista);
		System.out.println("not null 3:  ->"+lista.size());
		assertEquals(1, lista.size());
		
	}


	//	@Test
	public void testFindHospitalByOneCriteria() {
		System.out.println("Find test by Hospital One Criterial .... \n");

		params.put("descripcion_Hospital", "UNIDAD MEDICA INTEGRAL LOS CABOS ");
		
		List<HospitalDTO> lista =  client.findHospitales(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:  ->"+lista.size());
		assertEquals(1, lista.size());

	}
}
