/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.rest.catalogo.client.clasificador.AllianzClasificadorClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;
import mx.com.allianz.service.rest.clasificador.ClasificadorDTO;

/**
 * @author 
 *
 */
public class ClasificadorRestServiceClientTest {

	AllianzClasificadorClient client;
	Map<String, Object> params; 

	@Before
	public void generaCliente() {
		client = new AlliazRestClientFactory().buildClient(AllianzClasificadorClient.class);	
		params = new HashMap<>(); 
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos}.
	 */


	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos)}.
	 */
	@Test
	public void testGetClasificador() {
		System.out.println("Get test Clasificador .... \n");
		List<ClasificadorDTO> lista = client.getClasificador();
		//lista.forEach((estado) -> System.out.println(estado));
		assertNotNull(lista);
		System.out.println("not null 1:  ->"+lista.size());
		assertEquals(213, lista.size());
		
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("id_Clasificador", 2);
		params.put("nombre", "AVIACION");
		
		
		List<ClasificadorDTO> lista = client.findClasificadores(params);
		assertNotNull(lista);
		System.out.println("not null 2:  ->"+lista.size());
		assertEquals(33, lista.size());
	}

	@Test
	public void testFindClasificadorByTwoCriteria() {
		System.out.println("Find test by Clasificador Two Criterial .... \n");

		params.put("id_Clasificador", 86);
		
		List<ClasificadorDTO> lista = client.findClasificadores(params);
		assertNotNull(lista);
		System.out.println("not null 3:  ->"+lista.size());
		assertEquals(33, lista.size());
		
	}


	@Test
	public void testFindClasificadorByOneCriteria() {
		System.out.println("Find test by Clasificador One Criterial .... \n");

		params.put("nombre", "AVIACION");
		
		List<ClasificadorDTO> lista =  client.findClasificadores(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:  ->"+lista.size());
		assertEquals(33, lista.size());

	}
}
