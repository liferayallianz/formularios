/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.rest.catalogo.client.clasificador.AllianzClasificadorClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;

/**
 * @author alfonsomarquez
 *
 */
public class EstadoRestServiceClientTest {

	AllianzEstadoClient client;
	Map<String, Object> params; 

	@Before
	public void generaCliente() {
		client = new AlliazRestClientFactory().buildClient(AllianzEstadoClient.class);	
		params = new HashMap<>(); 
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#getEstados()}.
	 */
//	@Test
//	public void testGetEstadosOldSchool() {
//		try {
//			URL url = new URL("http://192.168.1.73:8080/o/allianz-ws/catalogos.estados/getEstados");
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			conn.setRequestMethod("GET");
//			conn.setRequestProperty("Accept", "application/json");
//			
//			assertFalse("Failed : HTTP error code : " + conn.getResponseCode(), conn.getResponseCode() != 200);
//			
//			BufferedReader br = new BufferedReader( new InputStreamReader(conn.getInputStream()) );
//			String output;
//			System.out.println("Output from Server .... \n");
//			while ((output = br.readLine()) != null) {
//				System.out.println(output);
//			}
//			conn.disconnect();
//
//		  } catch (MalformedURLException e) {
//			e.printStackTrace();
//			fail("Not yet implemented");
//		  } catch (IOException e) {
//			e.printStackTrace();
//			fail("Not yet implemented");
//		  }
//	}

	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	@Test
	public void testGetEstados() {
		System.out.println("Get test Estados .... \n");
		List<EstadoDTO> lista = client.getEstados();
		//lista.forEach((estado) -> System.out.println(estado));
		assertNotNull(lista);
		assertEquals(213, lista.size());
		
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("codigo_pais", 412);
		params.put("codigo_estado", -1);
		params.put("descripcion","A");
		
		List<EstadoDTO> lista = client.findEstados(params);
		assertNotNull(lista);
		assertEquals(29, lista.size());
	}

	@Test
	public void testFindEstadosByTwoCriteria() {
		System.out.println("Find test by Estados Two Criterial .... \n");

		params.put("codigo_pais", 412);
		params.put("descripcion","Y");
		
		List<EstadoDTO> lista = client.findEstados(params);
		assertNotNull(lista);
		assertEquals(2, lista.size());
		
	}


	@Test
	public void testFindEstadosByOneCriteria() {
		System.out.println("Find test by Estados One Criterial .... \n");

		params.put("codigo_estado",15);
		
		List<EstadoDTO> lista =  client.findEstados(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		assertEquals(2, lista.size());

	}
}
