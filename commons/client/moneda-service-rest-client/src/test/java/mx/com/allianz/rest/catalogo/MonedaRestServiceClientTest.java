package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.commons.catalogos.dto.MonedaDTO;
import mx.com.allianz.rest.catalogo.client.moneda.AllianzMonedaClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;

public class MonedaRestServiceClientTest {
	
AllianzMonedaClient client;
	
	Map<String, Object> params; 

//	@Before
	public void generaCliente() {
		
		client = new AlliazRestClientFactory().buildClient(AllianzMonedaClient.class);	
		
		params = new HashMap<>(); 
	}
	


	//	@Test
	public void testGetMoneda() {
		System.out.println("Get test Moneda .... \n");
		List<MonedaDTO> lista = client.getMonedas();
		//lista.forEach((ciudad) -> System.out.println((ciudad)));
		assertNotNull(lista);
		System.out.println("not null 1:  ->" +lista.size());
		assertEquals(3, lista.size());
		
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("id_moneda", 1);
		params.put("codigo_moneda", "PSM");
		params.put("descripcion", "Moneda Nacional");
		
		List<MonedaDTO> lista = client.findMonedas(params);
		assertNotNull(lista);
		System.out.println("not null 2:  ->" +lista.size());
		assertEquals(1, lista.size());
	}

	//	@Test
	public void testFindMonedaByTwoCriteria() {
		System.out.println("Find test by Moneda Two Criterial .... \n");

		params.put("id_moneda", 3);
		params.put("descripcion","Moneda Euros");
		
		List<MonedaDTO> lista = client.findMonedas(params);
		assertNotNull(lista);
		System.out.println("not null 3:  ->" +lista.size());
		assertEquals(1, lista.size());
		
	}


	//	@Test
	public void testFindMonedaByOneCriteria() {
		System.out.println("Find test by Moneda One Criterial .... \n");

		params.put("codigo_moneda","DL");
		
		List<MonedaDTO> lista =  client.findMonedas(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:  ->" +lista.size());
		assertEquals(1, lista.size());

	}

}
