package mx.com.allianz.rest.catalogo.client.moneda;

import java.util.List;
import java.util.Map;

import feign.QueryMap;
import feign.RequestLine;
import mx.com.allianz.commons.catalogos.dto.MonedaDTO;

/**
 *Interfaz que define el comportamiento del cliente del servicio de 
 *catalogos de estados implementado por open feign de forma automatica.
 */

public interface AllianzMonedaClient {
	
	/*
	 * @RequestLine anotacion de feign para ligar el llamado a la operacion del servicio
	 * a la implementacion de este metodo 
	 */
	@RequestLine("GET /catalogos.moneda/getMonedas")
	
	/**
	 * Metodo abstracto que regresa una lista de todos Moneda.
	 * 
	 * @return 
	 */
	List<MonedaDTO> getMonedas();
	
	/*
	 * @RequestLine anotacion de feign para ligar el llamado a la operacion del servicio
	 * a la implementacion de este metodo 
	 */
	@RequestLine("GET /catalogos.moneda/findMonedas")
	
	/**
	 * Metodo abstracto que regresa una lista de los Moneda filtrados.
	 * 
	 * @return 
	 */
	List<MonedaDTO> findMonedas(@QueryMap Map<String, Object> options);
	
	

}
