/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.commons.catalogos.dto.QuejaRefiereDTO;
import mx.com.allianz.rest.catalogo.client.quejarefiere.AllianzQuejarefiereClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;


public class QuejarefiereRestServiceClientTest {

	AllianzQuejarefiereClient client;
	
	Map<String, Object> params; 

//	@Before
	public void generaCliente() {
		
		client = new AlliazRestClientFactory().buildClient(AllianzQuejarefiereClient.class);	
		
		params = new HashMap<>(); 
	}
	


	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testGetQuejarefiere() {
		System.out.println("Get test Quejarefiere .... \n");
		List<QuejaRefiereDTO> lista = client.getQuejarefiere();
		//lista.forEach((Quejarefiere) -> System.out.println((Quejarefiere)));
		assertNotNull(lista);
		System.out.println("not null 1:   ->"+lista.size());
		assertEquals(49, lista.size());
		
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("clave", "QuejaAsegurado-1-1");
		params.put("clave_relacion", "QuejaAsegurado-1");
		params.put("descripcion","No estoy de acuerdo en la respuesta en tiempo y forma de mi tramite");
		params.put("ordena","0");
		params.put("flujoPuc","1");
		params.put("codSubProblema","QuejaAsegurado-1");
		

		
		List<QuejaRefiereDTO> lista = client.findQuejarefiere(params);
		assertNotNull(lista);
		System.out.println("not null 2:   ->"+lista.size());
		assertEquals(1, lista.size());
	}

	//	@Test
	public void testFindQuejarefiereByTwoCriteria() {
		System.out.println("Find test by Quejarefiere Two Criterial .... \n");

		params.put("clave", "QuejaAsegurado-1-1");
		params.put("clave_relacion", "QuejaAsegurado-1");
		
		List<QuejaRefiereDTO> lista = client.findQuejarefiere(params);
		assertNotNull(lista);
		System.out.println("not null 3:   ->"+lista.size());
		assertEquals(1, lista.size());
		
	}


	//	@Test
	public void testFindQuejarefiereByOneCriteria() {
		System.out.println("Find test by Quejarefiere One Criterial .... \n");

		params.put("flujoPuc","1");
		
		List<QuejaRefiereDTO> lista =  client.findQuejarefiere(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:   ->"+lista.size());
		assertEquals(19, lista.size());

	}
}
