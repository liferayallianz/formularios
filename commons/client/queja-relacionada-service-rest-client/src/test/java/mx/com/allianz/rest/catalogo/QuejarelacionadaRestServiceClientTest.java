/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.commons.catalogos.dto.QuejaRelacionadaDTO;
import mx.com.allianz.rest.catalogo.client.queja.relacionada.AllianzQuejarelacionadaClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;

public class QuejarelacionadaRestServiceClientTest {

	AllianzQuejarelacionadaClient client;

	Map<String, Object> params;

//	@Before
	public void generaCliente() {

		client = new AlliazRestClientFactory().buildClient(AllianzQuejarelacionadaClient.class);

		params = new HashMap<>();
	}

	/**
	 * Test method for
	 * {@link mx.com.allianz.rest.catalogos.EstadoService#findBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testGetQuejarelacionada() {
		System.out.println("Get test Quejarelacionada .... \n");
		List<QuejaRelacionadaDTO> lista = client.getQuejarelacionada();
		// lista.forEach((Quejarelacionada) -> System.out.println((Quejarelacionada)));
		assertNotNull(lista);
		System.out.println("not null 1:   ->" + lista.size());
		assertEquals(2, lista.size());

	}

	/**
	 * Test method for
	 * {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("clave", 60);
		params.put("descripcion", "TestProv");
		params.put("ordena", "moral");
		params.put("usted_Tiene", "ROCG900916JR5");
		params.put("es_Usted", "ROCG900916HDFDRV00");
		params.put("codigo_Problema", "Tester S.A de C.V");
		

		List<QuejaRelacionadaDTO> lista = client.findQuejarelacionada(params);
		assertNotNull(lista);
		System.out.println("not null 2:   ->" + lista.size());
		assertEquals(1, lista.size());
	}

	//	@Test
	public void testFindQuejarelacionadaByTwoCriteria() {
		System.out.println("Find test by Quejarelacionada Two Criterial .... \n");

		params.put("clave", "Test2 yuju");
		params.put("descripcion", "govanni");

		List<QuejaRelacionadaDTO> lista = client.findQuejarelacionada(params);
		assertNotNull(lista);
		System.out.println("not null 3:   ->" + lista.size());
		assertEquals(2, lista.size());

	}

	//	@Test
	public void testFindQuejarelacionadaByOneCriteria() {
		System.out.println("Find test by Quejarelacionada One Criterial .... \n");

		params.put("usted_Tiene", 0);

		List<QuejaRelacionadaDTO> lista = client.findQuejarelacionada(params);
		System.out.println(lista.size());

		assertNotNull(lista);
		System.out.println("not null 4:   ->" + lista.size());
		assertEquals(1, lista.size());

	}
}
