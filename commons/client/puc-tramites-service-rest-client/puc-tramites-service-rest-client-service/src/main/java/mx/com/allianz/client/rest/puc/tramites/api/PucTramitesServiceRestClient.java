package mx.com.allianz.client.rest.puc.tramites.api;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.StatusLine;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.client.puc.tramites.api.PucTramitesServiceClient;
import mx.com.allianz.client.puc.tramites.api.exception.PucTramitesServiceClientException;
import mx.com.allianz.client.rest.puc.tramites.api.configuration.PucTramitesServiceRestClientConfiguration;
import mx.com.allianz.commons.dto.puc.PucTramiteRespuestaRestDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;

@Component(
	configurationPid="mx.com.allianz.client.rest.puc.tramites.api.configuration.PucTramitesServiceRestClientConfiguration",
	immediate = true,
	property = {
	},
	service = PucTramitesServiceClient.class
)
public class PucTramitesServiceRestClient implements PucTramitesServiceClient {
	
	
	@Override
	public PucTramiteRespuestaRestDTO generarOtTramites(TramiteDto tramite) throws PucTramitesServiceClientException {

		if (_log.isInfoEnabled()) {
			_log.info("PucTramitesServiceRestClient -> generarOtTramites ");
			_log.info("generarOtTramites -> tramite = " + tramite);			
		}

		PucTramiteRespuestaRestDTO pucRespuesta = null;
		String pipedString = tramite.toPipedString();
		_log.info("****tramiteDTO -> getLugarAtencion() = " + tramite.getCplugarAtencion());
		_log.info("****tramiteDTO -> getTramites() = " + tramite.getCpTramite());
		String camposEncodedBase64 = Base64.getEncoder().encodeToString(pipedString.getBytes());
		Gson gson = new Gson();
		
		if( _log.isInfoEnabled()) {
			_log.info("generarOtTramites -> pipedString = " + pipedString);
			_log.info("generarOtTramites -> pipedString base64 = " + camposEncodedBase64);
		}

		String result = generarOtTramitesMultipart(camposEncodedBase64, tramite.getDocumentosMap());
		_log.info("****result ->  " + result);
		
		try {
			pucRespuesta = gson.fromJson(result, PucTramiteRespuestaRestDTO.class);
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error al intentar parsear la respuesta del servicio Puc Tramite Rest");
				_log.error(e);
			}
		}
		
		pucRespuesta = Optional.ofNullable(pucRespuesta).orElseGet(PucTramiteRespuestaRestDTO::new);	
		
		if (_log.isInfoEnabled()) {
			_log.info("pucRespuesta, result = " + result);
			_log.info("pucRespuesta = " + pucRespuesta);			
		}
		return pucRespuesta;
	}

	public PostMethod createPostMethod(String url){		
		
		if(_log.isDebugEnabled()) {
			_log.debug("PucTramitesServiceRestClient -> createPostMethod ");
			_log.debug("url = "+ url);
		}
		
		PostMethod postMethod = new PostMethod(url);
		String userpassCodificado  = Base64.getEncoder().encodeToString(getPucTramitesServiceRESTCredentials().getBytes());
		postMethod.addRequestHeader("Authorization", "Basic " + userpassCodificado );
		
		if(_log.isDebugEnabled()) {
			_log.debug("userpassCodificado = "+ userpassCodificado);
		}
		
		return postMethod;
	}
	
	/**
	 * Cliente que genera una Ot de Tramites
	 * 
	 */
	public String generarOtTramitesMultipart(String campos, Map<String, File> archivos)  throws PucTramitesServiceClientException{
		
		if (_log.isInfoEnabled()) {
			_log.info("PucTramitesServiceRestClient -> generarOtTramitesMultipart ");
			_log.info("archivos = " + Optional.ofNullable(archivos).orElse(new HashMap<>()).entrySet().stream().map(Map.Entry::getKey)
		            .collect(Collectors.joining(", ")));			
		}
				
		Map<String,String> fileExtentions = getPucTramitesServiceRESTFileExtentions();
		String url =  getPucTramitesServiceRESTEndPoint() + campos;
		_log.info("******** Campos :: **** " +  campos);
		String result = null;
		PostMethod postMethod = createPostMethod(url);
		Part[] parts = new Part[Optional.ofNullable(archivos).map(Map::size).orElse(0)];
		HttpClient httpClient = new HttpClient();
		HttpClientParams httpParams = httpClient.getParams();
		int statusCode = 500;		
		long timeout = getPucTramitesServiceRESTTimeout();
		MultipartRequestEntity entity = null;
		String responseBody = "";
		String mensajeError = "";
		
		if (_log.isDebugEnabled()) {
			_log.info("url = " + url);
			_log.info("parts length = " + parts.length);
			_log.info("timeout = " + timeout);
			_log.info("generarOtTramitesMultipart start");
			_log.info("archivos = " + fileExtentions.entrySet().stream()
		            .map(entry -> entry.getKey() + " - " + entry.getValue())
		            .collect(Collectors.joining(", ")));
		}
		
		httpParams.setConnectionManagerTimeout(timeout);
		httpClient.setParams(httpParams);
		
		parts = Optional.ofNullable(archivos).orElse(new HashMap<>()).entrySet().stream().map(entry -> {
			FilePart filePart = null;
			try { 
				String extention = FileUtil.getExtension(entry.getKey());
				String contentType = fileExtentions.get(extention);
				if (_log.isDebugEnabled()) {
					_log.info("fileName = " + entry.getKey());
					_log.info("extention = " + extention);
					_log.info("contentType = " + contentType);
				}
				//TODO quitar println
				System.out.println("fileName = " + entry.getKey());
				System.out.println("extention = " + extention);
				System.out.println("contentType = " + contentType);
				filePart = new FilePart(entry.getKey(), entry.getKey() , entry.getValue(), contentType, null);
			} catch (FileNotFoundException e) { 
				if (_log.isErrorEnabled()) _log.error(e); 
			}
			return filePart;
		}).collect(Collectors.toList()).toArray(parts);
		
		if (_log.isDebugEnabled()) {
			_log.info("parts = " + parts);
		}
		
		entity = new MultipartRequestEntity(parts, postMethod.getParams());
		postMethod.setRequestEntity(entity);
			
		if (_log.isInfoEnabled()) {
			_log.info("Iniciando envio para consumo de servicio Puc Tramite Rest");
		}		
		try {
			
			statusCode = httpClient.executeMethod(postMethod);
			
			if (_log.isInfoEnabled()) {
				_log.info("Respuesta, statusCode = " + statusCode);
			}			
			
			responseBody = postMethod.getResponseBodyAsString();
			
			if (_log.isInfoEnabled()) {
				_log.info("Respuesta, responseBody = " + responseBody);
			}

		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error al consumir servicio Puc Tramite Rest se escala error como PucTramitesServiceClientException: \n" + e.getMessage());
			}
			
			//TODO quitar print stack trace
			e.printStackTrace();
			throw new PucTramitesServiceClientException(e.getCause());
		}
		 
        if (statusCode != HttpStatus.SC_OK) {
        	if (_log.isErrorEnabled()) {
        		_log.error("Puc Tramite Rest, fallo al invocar metodo POST: " + postMethod.getStatusLine());
				_log.error("statusCode = " + statusCode);
			}

            result = Optional.ofNullable(postMethod.getStatusLine()).map(StatusLine::toString).orElse("");
            mensajeError = "POST metodo fallo: " + result + " : "+ responseBody;

            if(_log.isErrorEnabled()) {
            	_log.error("result = " + result);
            	_log.error("mensajeError = " + mensajeError);
            }
            throw new PucTramitesServiceClientException(mensajeError);  
        } else {
        	result = responseBody;
        	if (_log.isInfoEnabled()) {
        		_log.info("Puc Tramite Rest, exito al invocar metodo POST: ");
				_log.info("statusCode = " + statusCode);
				_log.info("result = " + result);
			}

        }
        
        postMethod.releaseConnection();
		
        if (_log.isDebugEnabled()) {
			_log.debug("Libera conexion a servicio Puc Tramite Rest");
		}
		return result;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public String getPucTramitesServiceRESTEndPoint() {
		return _configuration.pucTramitesServiceRESTEndPoint();
	}
	
	public String getPucTramitesServiceRESTCredentials() {
		return _configuration.pucTramitesServiceRESTCredentials();
	}
	
	
	public long getPucTramitesServiceRESTTimeout() {
		return _configuration.pucServiceRESTTimeout();
	}
	
	public Map<String,String> getPucTramitesServiceRESTFileExtentions() {
		return Arrays.stream(_configuration.validContenTypesExtentions()).map(row -> row.split(":"))
				.collect(Collectors.toMap(row -> row[0], row -> row[1]));
	}
	
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				PucTramitesServiceRestClientConfiguration.class, properties);
		
		if (_log.isInfoEnabled()) {
			_log.info("Configured PUC Tramites REST : { " +
					"Endpoint : " + getPucTramitesServiceRESTEndPoint() + ", " +
					"Credentials : " + getPucTramitesServiceRESTCredentials() + ", " +
					"Timeout : " + getPucTramitesServiceRESTTimeout() + ", " +
					"ContentTypes : " + getPucTramitesServiceRESTFileExtentions().entrySet().stream()
		            .map(entry -> entry.getKey() + " - " + entry.getValue())
		            .collect(Collectors.joining(", "))
					+ " }");
		}
	}
	
	private volatile PucTramitesServiceRestClientConfiguration _configuration;
	private static Log _log = LogFactoryUtil.getLog(PucTramitesServiceRestClient.class);

}