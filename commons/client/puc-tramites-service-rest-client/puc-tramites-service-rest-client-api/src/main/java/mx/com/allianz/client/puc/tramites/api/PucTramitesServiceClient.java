package mx.com.allianz.client.puc.tramites.api;

import aQute.bnd.annotation.ProviderType;
import mx.com.allianz.client.puc.tramites.api.exception.PucTramitesServiceClientException;
import mx.com.allianz.commons.dto.puc.PucTramiteRespuestaRestDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;

@ProviderType
public interface PucTramitesServiceClient {
	
	public PucTramiteRespuestaRestDTO generarOtTramites(TramiteDto tramite) throws PucTramitesServiceClientException;

}