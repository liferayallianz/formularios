/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.commons.dto.tramite.ProveedorDTO;
import mx.com.allianz.rest.catalogo.client.proveedor.AllianzProveedorClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;


public class ProveedorRestServiceClientTest {

	AllianzProveedorClient client;
	
	Map<String, Object> params; 

//	@Before
	public void generaCliente() {
		
		client = new AlliazRestClientFactory().buildClient(AllianzProveedorClient.class);	
		
		params = new HashMap<>(); 
	}
	


	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testGetProveedor() {
		System.out.println("Get test Proveedor .... \n");
		List<ProveedorDTO> lista = client.getProveedores();
		//lista.forEach((Proveedor) -> System.out.println((Proveedor)));
		assertNotNull(lista);
		System.out.println("not null 1:   ->"+lista.size());
		assertEquals(2, lista.size());
		
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("id_Proveedor", 60);
		params.put("codigo","TestProv");
		params.put("tipo_Persona","moral");
		params.put("rfc","ROCG900916JR5");
		params.put("curp","ROCG900916HDFDRV00");
		params.put("razon_Social","Tester S.A de C.V");
		params.put("nombre_Comercial","Test2 yuju");
		params.put("nombre","govanni");
		params.put("apellido_Paterno","Roco");
		params.put("apellido_Materno","Cruz");
		params.put("fecha_Nacimiento","16-septiembre-90");
		params.put("contrasenia","viejaConfiable");
		params.put("lada","065");
		params.put("telefono","34567765");
		params.put("fax","34567808");
		params.put("telefono_Celular","5533224455");
		params.put("email","pepe@gmail.com");
		params.put("pagina_Web","www.teste2.com.mx");
		params.put("calle","Mar Marmara");
		params.put("numero_Exterior","12345");
		params.put("numero_Interior","65");
		params.put("colonia","Cuahutemoc");
		params.put("codigo_Postal","543321");
		params.put("delegacion_Municipio","nolase");
		params.put("estado","Mexico");
		params.put("ciudad","Mexico");
		params.put("pais","Mexico");
		params.put("is_Tranferencia",true);
		params.put("is_Hospital",true);
		params.put("banco","Bancomer");
		params.put("numeroCuenta","123456789098643");
		params.put("clabe","555778hhj");
		params.put("is_Cheque", true);
		params.put("codigoGrupoPago","Bancomer Esp");
		params.put("codigoSubGrupoPago","Bancomer españa");

		
		List<ProveedorDTO> lista = client.findProveedores(params);
		assertNotNull(lista);
		System.out.println("not null 2:   ->"+lista.size());
		assertEquals(1, lista.size());
	}

	//	@Test
	public void testFindProveedorByTwoCriteria() {
		System.out.println("Find test by Proveedor Two Criterial .... \n");

		params.put("nombre_Comercial","Test2 yuju");
		params.put("nombre","govanni");
		
		List<ProveedorDTO> lista = client.findProveedores(params);
		assertNotNull(lista);
		System.out.println("not null 3:   ->"+lista.size());
		assertEquals(2, lista.size());
		
	}


	//	@Test
	public void testFindProveedorByOneCriteria() {
		System.out.println("Find test by Proveedor One Criterial .... \n");

		params.put("id_Proveedor", 0);
		
		List<ProveedorDTO> lista =  client.findProveedores(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:   ->"+lista.size());
		assertEquals(1, lista.size());

	}
}
