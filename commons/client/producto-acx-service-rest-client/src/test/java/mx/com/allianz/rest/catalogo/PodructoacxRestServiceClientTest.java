/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.allianz.commons.catalogos.dto.ProductoDTO;
import mx.com.allianz.rest.catalogo.client.producto.acx.AllianzProdructoacxClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;


public class PodructoacxRestServiceClientTest {

	AllianzProdructoacxClient client;
	
	Map<String, Object> params; 

//	@Before
	public void generaCliente() {
		
		client = new AlliazRestClientFactory().buildClient(AllianzProdructoacxClient.class);	
		
		params = new HashMap<>(); 
	}
	


	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testGetProductoacx() {
		System.out.println("Get test Productoacx .... \n");
		List<ProductoDTO> lista = client.getProductos();
		//lista.forEach((Productoacx) -> System.out.println((Productoacx)));
		assertNotNull(lista);
		System.out.println("not null 1:   ->"+lista.size());
		assertEquals(99, lista.size());
		
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("codigo_producto", "VIGR");
		params.put("descripcion_producto","VIDA GRUPO");
		params.put("tipo_producto","ACT");
		
		List<ProductoDTO> lista = client.findProductos(params);
		assertNotNull(lista);
		System.out.println("not null 2:   ->"+lista.size());
		assertEquals(1, lista.size());
	}

	//	@Test
	public void testFindProductoacxByTwoCriteria() {
		System.out.println("Find test by Productoacx Two Criterial .... \n");

		params.put("codigo_producto", "RCIP");
		params.put("descripcion_producto","RESPONSABILIDAD CIVIL PESOS");
		
		List<ProductoDTO> lista = client.findProductos(params);
		assertNotNull(lista);
		System.out.println("not null 3:   ->"+lista.size());
		assertEquals(1, lista.size());
		
	}


	//	@Test
	public void testFindProductoacxByOneCriteria() {
		System.out.println("Find test by Productoacx One Criterial .... \n");

		params.put("tipo_producto","ACT");
		
		List<ProductoDTO> lista =  client.findProductos(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:   ->"+lista.size());
		assertEquals(94, lista.size());

	}
}
