/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.commons.catalogos.dto.GrupoPagoDTO;
import mx.com.allianz.rest.catalogo.client.grupopago.AllianzGrupopagoClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;


public class GrupopagoRestServiceClientTest {

	AllianzGrupopagoClient client;
	
	Map<String, Object> params; 

//	@Before
	public void generaCliente() {
		
		client = new AlliazRestClientFactory().buildClient(AllianzGrupopagoClient.class);	
		
		params = new HashMap<>(); 
	}
	


	//	@Test
	public void testGetGrupopago() {
		System.out.println("Get test Grupopago .... \n");
		List<GrupoPagoDTO> lista = client.getGrupopago();
		//lista.forEach((ciudad) -> System.out.println((ciudad)));
		assertNotNull(lista);
		System.out.println("not null 1:   ->" + lista.size());
		assertEquals(21, lista.size());
		
	}
	
	//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("cod_GrupoPag", 88);
		params.put("stsDir_GrupoPag", "MODULOS HOSPITALTARIOS MATRIZ Y FORANEOS");
		params.put("descripGrupo","ACT");
		
		List<GrupoPagoDTO> lista = client.findGrupopago(params);
		assertNotNull(lista);
		System.out.println("not null 2:   ->" + lista.size());
		assertEquals(0, lista.size());
	}

	//	@Test
	public void testFindGrupopagoByTwoCriteria() {
		System.out.println("Find test by Grupopago Two Criterial .... \n");

		params.put("cod_GrupoPag", 90);
		params.put("descripGrupo","PROMOTORIAS ALLIANZ");
		
		List<GrupoPagoDTO> lista = client.findGrupopago(params);
		assertNotNull(lista);
		System.out.println("not null 3:   ->" + lista.size());
		assertEquals(1, lista.size());
		
	}


	//	@Test
	public void testFindGrupopagoByOneCriteria() {
		System.out.println("Find test by Grupopago One Criterial .... \n");

		params.put("stsDir_GrupoPag","CAJA MATRIZ CORREDORES");
		
		List<GrupoPagoDTO> lista =  client.findGrupopago(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:   ->" + lista.size());
		assertEquals(0, lista.size());

	}
}
