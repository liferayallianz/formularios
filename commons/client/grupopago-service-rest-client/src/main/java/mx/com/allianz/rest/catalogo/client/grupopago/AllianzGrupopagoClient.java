package mx.com.allianz.rest.catalogo.client.grupopago;

/**
 * La aplicaci\u00f3n genera los path para el consumo del servicio
 * Estados que permite visualizar una lista de datos, 
 * filtra el orden de la b\u00fasqueda y muestra el ordenamiento 
 * conforme al filtro
 *
 * @author  TestingZone
 * @version 1.0
 * @since   2016-12-07 
 */

import java.util.List;
import java.util.Map;

import feign.QueryMap;
import feign.RequestLine;
import mx.com.allianz.commons.catalogos.dto.GrupoPagoDTO;




/**
 *Interfaz que define el comportamiento del cliente del servicio de 
 *catalogos de estados implementado por open feign de forma automatica.
 */
public interface AllianzGrupopagoClient {
	/*
	 * @RequestLine anotacion de feign para ligar el llamado a la operacion del servicio
	 * a la implementacion de este metodo 
	 */
	@RequestLine("GET /catalogos.grupopago/getGrupopagos")
	
	/**
	 * Metodo abstracto que regresa una lista de todos Grupopagos.
	 * 
	 * @return
	 */
	List<GrupoPagoDTO> getGrupopago();
	
	/*
	 * @RequestLine anotacion de feign para ligar el llamado a la operacion del servicio
	 * a la implementacion de este metodo 
	 */
	@RequestLine("GET /catalogos.grupopago/findGrupopagos")
	
	/**
	 * Metodo abstracto que regresa una lista de los Grupopagos filtrados.
	 * 
	 * @return 
	 */
	List<GrupoPagoDTO> findGrupopago(@QueryMap Map<String, Object> options);

}