/**
 * 
 */
package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import mx.com.allian.rest.catalogo.cliente.sucursal.AllianzSucursalClient;
import mx.com.allianz.commons.catalogos.dto.SucursalDTO;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;


public class SucursalRestServiceClientTest {

	AllianzSucursalClient client;
	
	Map<String, Object> params; 

//	@Before
	public void generaCliente() {
		
		client = new AlliazRestClientFactory().buildClient(AllianzSucursalClient.class);	
		
		params = new HashMap<>(); 
	}
	


	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testGetSucursal() {
		System.out.println("Get test Sucursal .... \n");
		List<SucursalDTO> lista = client.getSucursales();
		//lista.forEach((Sucursal) -> System.out.println((Sucursal)));
		assertNotNull(lista);
		System.out.println("not null 1:   ->"+lista.size());
		assertEquals(3, lista.size());
		
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
	//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("id_sucursal", 2);
		params.put("descripcion","Matriz (D.F.)");
		
		List<SucursalDTO> lista = client.findSucursales(params);
		assertNotNull(lista);
		System.out.println("not null 2:   ->"+lista.size());
		assertEquals(1, lista.size());
	}

	//	@Test
	public void testFindSucursalByTwoCriteria() {
		System.out.println("Find test by Sucursal Two Criterial .... \n");

		params.put("codigo_Sucursal", 1);
		
		List<SucursalDTO> lista = client.findSucursales(params);
		assertNotNull(lista);
		System.out.println("not null 3:   ->"+lista.size());
		assertEquals(3, lista.size());
		
	}


	//	@Test
	public void testFindSucursalByOneCriteria() {
		System.out.println("Find test by Sucursal One Criterial .... \n");

		params.put("descripcion","Monterrey");
		
		List<SucursalDTO> lista =  client.findSucursales(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:   ->"+lista.size());
		assertEquals(1, lista.size());

	}
}
