package mx.com.allianz.rest.cliente.documento;

import java.util.List;
import java.util.Map;

import feign.QueryMap;
import feign.RequestLine;
import mx.com.allianz.commons.catalogos.dto.DocumentoDTO;

/**
 *Interfaz que define el comportamiento del cliente del servicio de 
 *catalogos de estados implementado por open feign de forma automatica.
 */

public interface AllianzDocumentoClient {
	
	/*
	 * @RequestLine anotacion de feign para ligar el llamado a la operacion del servicio
	 * a la implementacion de este metodo 
	 */
	@RequestLine("GET /catalogos.documentos/getDocumentos")
	
	/**
	 * Metodo abstracto que regresa una lista de todos estados.
	 * 
	 * @return 
	 */
	List<DocumentoDTO> getDocumento();
	
	/*
	 * @RequestLine anotacion de feign para ligar el llamado a la operacion del servicio
	 * a la implementacion de este metodo 
	 */
	@RequestLine("GET /catalogos.documentos/findDocumentos")
	
	/**
	 * Metodo abstracto que regresa una lista de los estados filtrados.
	 * 
	 * @return List<Estado>
	 */
	List<DocumentoDTO> findDocumento(@QueryMap Map<String, Object> options);
	
	

}
