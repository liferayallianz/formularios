package mx.com.allianz.rest.catalogo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import mx.com.allianz.commons.catalogos.dto.DocumentoDTO;
import mx.com.allianz.rest.cliente.documento.AllianzDocumentoClient;
import mx.com.allianz.rest.commons.client.AlliazRestClientFactory;

public class DocumentoRestServiceClientTest {
	
AllianzDocumentoClient client;
	
	Map<String, Object> params; 

//	@Before
	public void generaCliente() {
		
		client = new AlliazRestClientFactory().buildClient(AllianzDocumentoClient.class);	
		
		params = new HashMap<>(); 
	}
	


//	@Test
	public void testGetDocumento() {
		System.out.println("Get test Documento .... \n");
		List<DocumentoDTO> lista = client.getDocumento();
		//lista.forEach((ciudad) -> System.out.println((ciudad)));
		assertNotNull(lista);
		System.out.println("not null 1:  ->" +lista.size());
		assertEquals(33, lista.size());
		
	}
	
	/**
	 * Test method for {@link mx.com.allianz.rest.catalogos.EstadoService#findEstadosBy(int, int, java.lang.String)}.
	 */
//	@Test
	public void testFindByAllCriteria() {
		System.out.println("Find test by All Criterial .... \n");
		params.put("id_DocsTR", "VIDA_04");
		params.put("tipo", "VIDA");
		params.put("codigo", "IncidenciasMed");
		params.put("descripcion","Reporte de Incidencias del IMSS de RH de la Empresa contratante ó soporte de asegurado activo en vigencia de póliza");
		
		List<DocumentoDTO> lista = client.findDocumento(params);
		assertNotNull(lista);
		System.out.println("not null 2:  ->" +lista.size());
		assertEquals(0, lista.size());
	}

//	@Test
	public void testFindDocumentoByTwoCriteria() {
		System.out.println("Find test by Documento Two Criterial .... \n");

		params.put("id_DocsTR", "PROVMED_02");
		params.put("descripcion","Archivo del Recibo de honorarios");
		
		List<DocumentoDTO> lista = client.findDocumento(params);
		assertNotNull(lista);
		System.out.println("not null 3:  ->" +lista.size());
		assertEquals(1, lista.size());
		
	}


//	@Test
	public void testFindDocumentoByOneCriteria() {
		System.out.println("Find test by Documento One Criterial .... \n");

		params.put("codigo","Facturas");
		
		List<DocumentoDTO> lista =  client.findDocumento(params);
		System.out.println(lista.size());
		
		assertNotNull(lista);
		System.out.println("not null 4:  ->" +lista.size());
		assertEquals(0, lista.size());

	}

}
