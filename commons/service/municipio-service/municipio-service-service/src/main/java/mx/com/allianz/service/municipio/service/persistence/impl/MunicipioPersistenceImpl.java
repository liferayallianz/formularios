/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.municipio.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.municipio.exception.NoSuchMunicipioException;
import mx.com.allianz.service.municipio.model.Municipio;
import mx.com.allianz.service.municipio.model.impl.MunicipioImpl;
import mx.com.allianz.service.municipio.model.impl.MunicipioModelImpl;
import mx.com.allianz.service.municipio.service.persistence.MunicipioPK;
import mx.com.allianz.service.municipio.service.persistence.MunicipioPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the municipio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MunicipioPersistence
 * @see mx.com.allianz.service.municipio.service.persistence.MunicipioUtil
 * @generated
 */
@ProviderType
public class MunicipioPersistenceImpl extends BasePersistenceImpl<Municipio>
	implements MunicipioPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MunicipioUtil} to access the municipio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = MunicipioImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
			MunicipioModelImpl.FINDER_CACHE_ENABLED, MunicipioImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
			MunicipioModelImpl.FINDER_CACHE_ENABLED, MunicipioImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
			MunicipioModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
			MunicipioModelImpl.FINDER_CACHE_ENABLED, MunicipioImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByfindByDescripcion",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
			MunicipioModelImpl.FINDER_CACHE_ENABLED, MunicipioImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByDescripcion", new String[] { String.class.getName() },
			MunicipioModelImpl.DESCRIPCION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION = new FinderPath(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
			MunicipioModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByDescripcion", new String[] { String.class.getName() });

	/**
	 * Returns all the municipios where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the matching municipios
	 */
	@Override
	public List<Municipio> findByfindByDescripcion(String descripcion) {
		return findByfindByDescripcion(descripcion, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the municipios where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of municipios
	 * @param end the upper bound of the range of municipios (not inclusive)
	 * @return the range of matching municipios
	 */
	@Override
	public List<Municipio> findByfindByDescripcion(String descripcion,
		int start, int end) {
		return findByfindByDescripcion(descripcion, start, end, null);
	}

	/**
	 * Returns an ordered range of all the municipios where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of municipios
	 * @param end the upper bound of the range of municipios (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching municipios
	 */
	@Override
	public List<Municipio> findByfindByDescripcion(String descripcion,
		int start, int end, OrderByComparator<Municipio> orderByComparator) {
		return findByfindByDescripcion(descripcion, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the municipios where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of municipios
	 * @param end the upper bound of the range of municipios (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching municipios
	 */
	@Override
	public List<Municipio> findByfindByDescripcion(String descripcion,
		int start, int end, OrderByComparator<Municipio> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion, start, end, orderByComparator };
		}

		List<Municipio> list = null;

		if (retrieveFromCache) {
			list = (List<Municipio>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Municipio municipio : list) {
					if (!Objects.equals(descripcion, municipio.getDescripcion())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MUNICIPIO_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MunicipioModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				if (!pagination) {
					list = (List<Municipio>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Municipio>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first municipio in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching municipio
	 * @throws NoSuchMunicipioException if a matching municipio could not be found
	 */
	@Override
	public Municipio findByfindByDescripcion_First(String descripcion,
		OrderByComparator<Municipio> orderByComparator)
		throws NoSuchMunicipioException {
		Municipio municipio = fetchByfindByDescripcion_First(descripcion,
				orderByComparator);

		if (municipio != null) {
			return municipio;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMunicipioException(msg.toString());
	}

	/**
	 * Returns the first municipio in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching municipio, or <code>null</code> if a matching municipio could not be found
	 */
	@Override
	public Municipio fetchByfindByDescripcion_First(String descripcion,
		OrderByComparator<Municipio> orderByComparator) {
		List<Municipio> list = findByfindByDescripcion(descripcion, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last municipio in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching municipio
	 * @throws NoSuchMunicipioException if a matching municipio could not be found
	 */
	@Override
	public Municipio findByfindByDescripcion_Last(String descripcion,
		OrderByComparator<Municipio> orderByComparator)
		throws NoSuchMunicipioException {
		Municipio municipio = fetchByfindByDescripcion_Last(descripcion,
				orderByComparator);

		if (municipio != null) {
			return municipio;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMunicipioException(msg.toString());
	}

	/**
	 * Returns the last municipio in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching municipio, or <code>null</code> if a matching municipio could not be found
	 */
	@Override
	public Municipio fetchByfindByDescripcion_Last(String descripcion,
		OrderByComparator<Municipio> orderByComparator) {
		int count = countByfindByDescripcion(descripcion);

		if (count == 0) {
			return null;
		}

		List<Municipio> list = findByfindByDescripcion(descripcion, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the municipios before and after the current municipio in the ordered set where descripcion = &#63;.
	 *
	 * @param municipioPK the primary key of the current municipio
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next municipio
	 * @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	 */
	@Override
	public Municipio[] findByfindByDescripcion_PrevAndNext(
		MunicipioPK municipioPK, String descripcion,
		OrderByComparator<Municipio> orderByComparator)
		throws NoSuchMunicipioException {
		Municipio municipio = findByPrimaryKey(municipioPK);

		Session session = null;

		try {
			session = openSession();

			Municipio[] array = new MunicipioImpl[3];

			array[0] = getByfindByDescripcion_PrevAndNext(session, municipio,
					descripcion, orderByComparator, true);

			array[1] = municipio;

			array[2] = getByfindByDescripcion_PrevAndNext(session, municipio,
					descripcion, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Municipio getByfindByDescripcion_PrevAndNext(Session session,
		Municipio municipio, String descripcion,
		OrderByComparator<Municipio> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MUNICIPIO_WHERE);

		boolean bindDescripcion = false;

		if (descripcion == null) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
		}
		else if (descripcion.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
		}
		else {
			bindDescripcion = true;

			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MunicipioModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDescripcion) {
			qPos.add(descripcion);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(municipio);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Municipio> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the municipios where descripcion = &#63; from the database.
	 *
	 * @param descripcion the descripcion
	 */
	@Override
	public void removeByfindByDescripcion(String descripcion) {
		for (Municipio municipio : findByfindByDescripcion(descripcion,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(municipio);
		}
	}

	/**
	 * Returns the number of municipios where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the number of matching municipios
	 */
	@Override
	public int countByfindByDescripcion(String descripcion) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION;

		Object[] finderArgs = new Object[] { descripcion };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MUNICIPIO_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1 = "municipio.descripcion IS NULL";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2 = "municipio.descripcion = ?";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3 = "(municipio.descripcion IS NULL OR municipio.descripcion = '')";

	public MunicipioPersistenceImpl() {
		setModelClass(Municipio.class);
	}

	/**
	 * Caches the municipio in the entity cache if it is enabled.
	 *
	 * @param municipio the municipio
	 */
	@Override
	public void cacheResult(Municipio municipio) {
		entityCache.putResult(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
			MunicipioImpl.class, municipio.getPrimaryKey(), municipio);

		municipio.resetOriginalValues();
	}

	/**
	 * Caches the municipios in the entity cache if it is enabled.
	 *
	 * @param municipios the municipios
	 */
	@Override
	public void cacheResult(List<Municipio> municipios) {
		for (Municipio municipio : municipios) {
			if (entityCache.getResult(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
						MunicipioImpl.class, municipio.getPrimaryKey()) == null) {
				cacheResult(municipio);
			}
			else {
				municipio.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all municipios.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(MunicipioImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the municipio.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Municipio municipio) {
		entityCache.removeResult(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
			MunicipioImpl.class, municipio.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Municipio> municipios) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Municipio municipio : municipios) {
			entityCache.removeResult(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
				MunicipioImpl.class, municipio.getPrimaryKey());
		}
	}

	/**
	 * Creates a new municipio with the primary key. Does not add the municipio to the database.
	 *
	 * @param municipioPK the primary key for the new municipio
	 * @return the new municipio
	 */
	@Override
	public Municipio create(MunicipioPK municipioPK) {
		Municipio municipio = new MunicipioImpl();

		municipio.setNew(true);
		municipio.setPrimaryKey(municipioPK);

		return municipio;
	}

	/**
	 * Removes the municipio with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param municipioPK the primary key of the municipio
	 * @return the municipio that was removed
	 * @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	 */
	@Override
	public Municipio remove(MunicipioPK municipioPK)
		throws NoSuchMunicipioException {
		return remove((Serializable)municipioPK);
	}

	/**
	 * Removes the municipio with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the municipio
	 * @return the municipio that was removed
	 * @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	 */
	@Override
	public Municipio remove(Serializable primaryKey)
		throws NoSuchMunicipioException {
		Session session = null;

		try {
			session = openSession();

			Municipio municipio = (Municipio)session.get(MunicipioImpl.class,
					primaryKey);

			if (municipio == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMunicipioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(municipio);
		}
		catch (NoSuchMunicipioException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Municipio removeImpl(Municipio municipio) {
		municipio = toUnwrappedModel(municipio);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(municipio)) {
				municipio = (Municipio)session.get(MunicipioImpl.class,
						municipio.getPrimaryKeyObj());
			}

			if (municipio != null) {
				session.delete(municipio);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (municipio != null) {
			clearCache(municipio);
		}

		return municipio;
	}

	@Override
	public Municipio updateImpl(Municipio municipio) {
		municipio = toUnwrappedModel(municipio);

		boolean isNew = municipio.isNew();

		MunicipioModelImpl municipioModelImpl = (MunicipioModelImpl)municipio;

		Session session = null;

		try {
			session = openSession();

			if (municipio.isNew()) {
				session.save(municipio);

				municipio.setNew(false);
			}
			else {
				municipio = (Municipio)session.merge(municipio);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !MunicipioModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((municipioModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						municipioModelImpl.getOriginalDescripcion()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);

				args = new Object[] { municipioModelImpl.getDescripcion() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);
			}
		}

		entityCache.putResult(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
			MunicipioImpl.class, municipio.getPrimaryKey(), municipio, false);

		municipio.resetOriginalValues();

		return municipio;
	}

	protected Municipio toUnwrappedModel(Municipio municipio) {
		if (municipio instanceof MunicipioImpl) {
			return municipio;
		}

		MunicipioImpl municipioImpl = new MunicipioImpl();

		municipioImpl.setNew(municipio.isNew());
		municipioImpl.setPrimaryKey(municipio.getPrimaryKey());

		municipioImpl.setCodigoPais(municipio.getCodigoPais());
		municipioImpl.setCodigoEstado(municipio.getCodigoEstado());
		municipioImpl.setCodigoCiudad(municipio.getCodigoCiudad());
		municipioImpl.setCodigoMunicipio(municipio.getCodigoMunicipio());
		municipioImpl.setDescripcion(municipio.getDescripcion());

		return municipioImpl;
	}

	/**
	 * Returns the municipio with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the municipio
	 * @return the municipio
	 * @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	 */
	@Override
	public Municipio findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMunicipioException {
		Municipio municipio = fetchByPrimaryKey(primaryKey);

		if (municipio == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMunicipioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return municipio;
	}

	/**
	 * Returns the municipio with the primary key or throws a {@link NoSuchMunicipioException} if it could not be found.
	 *
	 * @param municipioPK the primary key of the municipio
	 * @return the municipio
	 * @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	 */
	@Override
	public Municipio findByPrimaryKey(MunicipioPK municipioPK)
		throws NoSuchMunicipioException {
		return findByPrimaryKey((Serializable)municipioPK);
	}

	/**
	 * Returns the municipio with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the municipio
	 * @return the municipio, or <code>null</code> if a municipio with the primary key could not be found
	 */
	@Override
	public Municipio fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
				MunicipioImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Municipio municipio = (Municipio)serializable;

		if (municipio == null) {
			Session session = null;

			try {
				session = openSession();

				municipio = (Municipio)session.get(MunicipioImpl.class,
						primaryKey);

				if (municipio != null) {
					cacheResult(municipio);
				}
				else {
					entityCache.putResult(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
						MunicipioImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(MunicipioModelImpl.ENTITY_CACHE_ENABLED,
					MunicipioImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return municipio;
	}

	/**
	 * Returns the municipio with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param municipioPK the primary key of the municipio
	 * @return the municipio, or <code>null</code> if a municipio with the primary key could not be found
	 */
	@Override
	public Municipio fetchByPrimaryKey(MunicipioPK municipioPK) {
		return fetchByPrimaryKey((Serializable)municipioPK);
	}

	@Override
	public Map<Serializable, Municipio> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Municipio> map = new HashMap<Serializable, Municipio>();

		for (Serializable primaryKey : primaryKeys) {
			Municipio municipio = fetchByPrimaryKey(primaryKey);

			if (municipio != null) {
				map.put(primaryKey, municipio);
			}
		}

		return map;
	}

	/**
	 * Returns all the municipios.
	 *
	 * @return the municipios
	 */
	@Override
	public List<Municipio> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the municipios.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of municipios
	 * @param end the upper bound of the range of municipios (not inclusive)
	 * @return the range of municipios
	 */
	@Override
	public List<Municipio> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the municipios.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of municipios
	 * @param end the upper bound of the range of municipios (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of municipios
	 */
	@Override
	public List<Municipio> findAll(int start, int end,
		OrderByComparator<Municipio> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the municipios.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of municipios
	 * @param end the upper bound of the range of municipios (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of municipios
	 */
	@Override
	public List<Municipio> findAll(int start, int end,
		OrderByComparator<Municipio> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Municipio> list = null;

		if (retrieveFromCache) {
			list = (List<Municipio>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_MUNICIPIO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MUNICIPIO;

				if (pagination) {
					sql = sql.concat(MunicipioModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Municipio>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Municipio>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the municipios from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Municipio municipio : findAll()) {
			remove(municipio);
		}
	}

	/**
	 * Returns the number of municipios.
	 *
	 * @return the number of municipios
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MUNICIPIO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return MunicipioModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the municipio persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(MunicipioImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_MUNICIPIO = "SELECT municipio FROM Municipio municipio";
	private static final String _SQL_SELECT_MUNICIPIO_WHERE = "SELECT municipio FROM Municipio municipio WHERE ";
	private static final String _SQL_COUNT_MUNICIPIO = "SELECT COUNT(municipio) FROM Municipio municipio";
	private static final String _SQL_COUNT_MUNICIPIO_WHERE = "SELECT COUNT(municipio) FROM Municipio municipio WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "municipio.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Municipio exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Municipio exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(MunicipioPersistenceImpl.class);
}