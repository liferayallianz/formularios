/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.municipio.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.municipio.model.Municipio;
import mx.com.allianz.service.municipio.service.persistence.MunicipioPK;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Municipio in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Municipio
 * @generated
 */
@ProviderType
public class MunicipioCacheModel implements CacheModel<Municipio>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MunicipioCacheModel)) {
			return false;
		}

		MunicipioCacheModel municipioCacheModel = (MunicipioCacheModel)obj;

		if (municipioPK.equals(municipioCacheModel.municipioPK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, municipioPK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{codigoPais=");
		sb.append(codigoPais);
		sb.append(", codigoEstado=");
		sb.append(codigoEstado);
		sb.append(", codigoCiudad=");
		sb.append(codigoCiudad);
		sb.append(", codigoMunicipio=");
		sb.append(codigoMunicipio);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Municipio toEntityModel() {
		MunicipioImpl municipioImpl = new MunicipioImpl();

		municipioImpl.setCodigoPais(codigoPais);
		municipioImpl.setCodigoEstado(codigoEstado);
		municipioImpl.setCodigoCiudad(codigoCiudad);
		municipioImpl.setCodigoMunicipio(codigoMunicipio);

		if (descripcion == null) {
			municipioImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			municipioImpl.setDescripcion(descripcion);
		}

		municipioImpl.resetOriginalValues();

		return municipioImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codigoPais = objectInput.readInt();

		codigoEstado = objectInput.readInt();

		codigoCiudad = objectInput.readInt();

		codigoMunicipio = objectInput.readInt();
		descripcion = objectInput.readUTF();

		municipioPK = new MunicipioPK(codigoPais, codigoEstado, codigoCiudad,
				codigoMunicipio);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(codigoPais);

		objectOutput.writeInt(codigoEstado);

		objectOutput.writeInt(codigoCiudad);

		objectOutput.writeInt(codigoMunicipio);

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}
	}

	public int codigoPais;
	public int codigoEstado;
	public int codigoCiudad;
	public int codigoMunicipio;
	public String descripcion;
	public transient MunicipioPK municipioPK;
}