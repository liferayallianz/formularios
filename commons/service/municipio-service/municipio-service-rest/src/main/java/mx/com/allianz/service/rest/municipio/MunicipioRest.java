package mx.com.allianz.service.rest.municipio;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.MunicipioDTO;
import mx.com.allianz.service.municipio.service.MunicipioLocalServiceUtil;



/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio municipio que
 * permite visualizar una lista de datos, filtra el orden de la b\u00fasqueda y
 * muestra el ordenamiento conforme al filtro.
 * 
 *
 * @author TestingZone
 * @version 1.0
 * @since 2016-12-2
 * 
 * 
 */

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.municipio")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)
public class MunicipioRest extends Application {

	public static final int CODIGO_PAIS_MEXICO = 412;
	public static final String CODIGO_PAIS_MEXICO_CADENA = "412";
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los municipios
	 * del catalogo.
	 * 
	 * @return getAllMunicipios()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getMunicipios")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<MunicipioDTO> getMunicipios() {

		// Se regresan todos los municipios.
		return getAllMunicipios();
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los municipios del
	 * catalogo.
	 * 
	 * @return getAllPaises().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findMunicipios")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<MunicipioDTO> findMunicipiosBy(
			// El valor @DefaultValue se utiliz\u00e1 para definir el valor por
			// defecto para el par\u00e1metro matrixParam.
			// @QueryParam se utiliz\u00e1 para extraer los par\u00e1metros de
			// consulta del "componente de consulta" de la URL de la solicitud.
			
			@DefaultValue(CODIGO_PAIS_MEXICO_CADENA) @QueryParam("codigo_pais") int codigoPais,
			@DefaultValue("-1") @QueryParam("codigo_estado") int codigoEstado,
			@DefaultValue("-1") @QueryParam("codigo_ciudad") int codigoCiudad,
			@DefaultValue("-1") @QueryParam("codigo_municipio") int codigoMuncipio,
			@DefaultValue("") @QueryParam("descripcion") String descripcion) {
		// stream() Devuelve un flujo secuencial considerando la colecci\u00f3n
		// como su origen.
		// Convierte la lista en stream.
	
		return getAllMunicipios().stream()
				// Filtrado por identificador del codigo pa\u00eds, si se envia -1 se
				// ignora el filtro.
				.filter((municipio) -> codigoPais == -1 || municipio.getCodigoPais() == codigoPais)
				// Filtrado por identificador del codigo estado, si se envia -1 se
				// ignora el filtro.
				.filter((municipio) -> codigoEstado == -1 || municipio.getCodigoEstado() == codigoEstado)
				// Filtrado por identificador del codigo ciudad, si se envia -1 se
				// ignora el filtro.
				.filter((municipio) -> codigoCiudad == -1 || municipio.getCodigoCiudad() == codigoCiudad)
				// Filtrado por identificador del codigo municipio, si se envia -1 se
				// ignora el filtro.
				.filter((municipio) -> codigoMuncipio == -1 || municipio.getCodigoMunicipio() == codigoMuncipio)
				// Filtrado por descripci\u00f3n del pa\u00eds, si es vac\u00edo se
				// ignora el filtro.
				.filter((municipio) -> descripcion.isEmpty() || municipio.getDescripcion().toUpperCase().contains(descripcion.toUpperCase()))
				// Salida de colecci\u00f3n y el stream se convierte en lista.
				.collect(Collectors.toList());

	}

	/**
	 * M\u00e9todo getAllPaises() del tipo list generada para mostrar un arreglo
	 * de la lista.
	 * 
	 * @return listaMunicipios
	 */
	private List<MunicipioDTO> getAllMunicipios() {
		
		return MunicipioLocalServiceUtil.getMunicipios(-1, -1).stream().
				map(municipio -> new MunicipioDTO(municipio.getCodigoMunicipio(), 
				municipio.getCodigoPais(), municipio.getCodigoEstado(), 
				municipio.getCodigoCiudad(), municipio.getDescripcion())).
				collect(Collectors.toList());
	}

}