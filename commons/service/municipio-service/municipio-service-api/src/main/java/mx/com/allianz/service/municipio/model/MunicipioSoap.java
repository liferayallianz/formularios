/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.municipio.model;

import aQute.bnd.annotation.ProviderType;

import mx.com.allianz.service.municipio.service.persistence.MunicipioPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class MunicipioSoap implements Serializable {
	public static MunicipioSoap toSoapModel(Municipio model) {
		MunicipioSoap soapModel = new MunicipioSoap();

		soapModel.setCodigoPais(model.getCodigoPais());
		soapModel.setCodigoEstado(model.getCodigoEstado());
		soapModel.setCodigoCiudad(model.getCodigoCiudad());
		soapModel.setCodigoMunicipio(model.getCodigoMunicipio());
		soapModel.setDescripcion(model.getDescripcion());

		return soapModel;
	}

	public static MunicipioSoap[] toSoapModels(Municipio[] models) {
		MunicipioSoap[] soapModels = new MunicipioSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MunicipioSoap[][] toSoapModels(Municipio[][] models) {
		MunicipioSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MunicipioSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MunicipioSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MunicipioSoap[] toSoapModels(List<Municipio> models) {
		List<MunicipioSoap> soapModels = new ArrayList<MunicipioSoap>(models.size());

		for (Municipio model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MunicipioSoap[soapModels.size()]);
	}

	public MunicipioSoap() {
	}

	public MunicipioPK getPrimaryKey() {
		return new MunicipioPK(_codigoPais, _codigoEstado, _codigoCiudad,
			_codigoMunicipio);
	}

	public void setPrimaryKey(MunicipioPK pk) {
		setCodigoPais(pk.codigoPais);
		setCodigoEstado(pk.codigoEstado);
		setCodigoCiudad(pk.codigoCiudad);
		setCodigoMunicipio(pk.codigoMunicipio);
	}

	public int getCodigoPais() {
		return _codigoPais;
	}

	public void setCodigoPais(int codigoPais) {
		_codigoPais = codigoPais;
	}

	public int getCodigoEstado() {
		return _codigoEstado;
	}

	public void setCodigoEstado(int codigoEstado) {
		_codigoEstado = codigoEstado;
	}

	public int getCodigoCiudad() {
		return _codigoCiudad;
	}

	public void setCodigoCiudad(int codigoCiudad) {
		_codigoCiudad = codigoCiudad;
	}

	public int getCodigoMunicipio() {
		return _codigoMunicipio;
	}

	public void setCodigoMunicipio(int codigoMunicipio) {
		_codigoMunicipio = codigoMunicipio;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	private int _codigoPais;
	private int _codigoEstado;
	private int _codigoCiudad;
	private int _codigoMunicipio;
	private String _descripcion;
}