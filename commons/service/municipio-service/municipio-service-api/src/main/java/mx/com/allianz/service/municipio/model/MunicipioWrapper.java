/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.municipio.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Municipio}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Municipio
 * @generated
 */
@ProviderType
public class MunicipioWrapper implements Municipio, ModelWrapper<Municipio> {
	public MunicipioWrapper(Municipio municipio) {
		_municipio = municipio;
	}

	@Override
	public Class<?> getModelClass() {
		return Municipio.class;
	}

	@Override
	public String getModelClassName() {
		return Municipio.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codigoPais", getCodigoPais());
		attributes.put("codigoEstado", getCodigoEstado());
		attributes.put("codigoCiudad", getCodigoCiudad());
		attributes.put("codigoMunicipio", getCodigoMunicipio());
		attributes.put("descripcion", getDescripcion());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer codigoPais = (Integer)attributes.get("codigoPais");

		if (codigoPais != null) {
			setCodigoPais(codigoPais);
		}

		Integer codigoEstado = (Integer)attributes.get("codigoEstado");

		if (codigoEstado != null) {
			setCodigoEstado(codigoEstado);
		}

		Integer codigoCiudad = (Integer)attributes.get("codigoCiudad");

		if (codigoCiudad != null) {
			setCodigoCiudad(codigoCiudad);
		}

		Integer codigoMunicipio = (Integer)attributes.get("codigoMunicipio");

		if (codigoMunicipio != null) {
			setCodigoMunicipio(codigoMunicipio);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _municipio.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _municipio.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _municipio.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _municipio.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Municipio> toCacheModel() {
		return _municipio.toCacheModel();
	}

	@Override
	public int compareTo(Municipio municipio) {
		return _municipio.compareTo(municipio);
	}

	/**
	* Returns the codigo ciudad of this municipio.
	*
	* @return the codigo ciudad of this municipio
	*/
	@Override
	public int getCodigoCiudad() {
		return _municipio.getCodigoCiudad();
	}

	/**
	* Returns the codigo estado of this municipio.
	*
	* @return the codigo estado of this municipio
	*/
	@Override
	public int getCodigoEstado() {
		return _municipio.getCodigoEstado();
	}

	/**
	* Returns the codigo municipio of this municipio.
	*
	* @return the codigo municipio of this municipio
	*/
	@Override
	public int getCodigoMunicipio() {
		return _municipio.getCodigoMunicipio();
	}

	/**
	* Returns the codigo pais of this municipio.
	*
	* @return the codigo pais of this municipio
	*/
	@Override
	public int getCodigoPais() {
		return _municipio.getCodigoPais();
	}

	@Override
	public int hashCode() {
		return _municipio.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _municipio.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new MunicipioWrapper((Municipio)_municipio.clone());
	}

	/**
	* Returns the descripcion of this municipio.
	*
	* @return the descripcion of this municipio
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _municipio.getDescripcion();
	}

	@Override
	public java.lang.String toString() {
		return _municipio.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _municipio.toXmlString();
	}

	@Override
	public Municipio toEscapedModel() {
		return new MunicipioWrapper(_municipio.toEscapedModel());
	}

	@Override
	public Municipio toUnescapedModel() {
		return new MunicipioWrapper(_municipio.toUnescapedModel());
	}

	/**
	* Returns the primary key of this municipio.
	*
	* @return the primary key of this municipio
	*/
	@Override
	public mx.com.allianz.service.municipio.service.persistence.MunicipioPK getPrimaryKey() {
		return _municipio.getPrimaryKey();
	}

	@Override
	public void persist() {
		_municipio.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_municipio.setCachedModel(cachedModel);
	}

	/**
	* Sets the codigo ciudad of this municipio.
	*
	* @param codigoCiudad the codigo ciudad of this municipio
	*/
	@Override
	public void setCodigoCiudad(int codigoCiudad) {
		_municipio.setCodigoCiudad(codigoCiudad);
	}

	/**
	* Sets the codigo estado of this municipio.
	*
	* @param codigoEstado the codigo estado of this municipio
	*/
	@Override
	public void setCodigoEstado(int codigoEstado) {
		_municipio.setCodigoEstado(codigoEstado);
	}

	/**
	* Sets the codigo municipio of this municipio.
	*
	* @param codigoMunicipio the codigo municipio of this municipio
	*/
	@Override
	public void setCodigoMunicipio(int codigoMunicipio) {
		_municipio.setCodigoMunicipio(codigoMunicipio);
	}

	/**
	* Sets the codigo pais of this municipio.
	*
	* @param codigoPais the codigo pais of this municipio
	*/
	@Override
	public void setCodigoPais(int codigoPais) {
		_municipio.setCodigoPais(codigoPais);
	}

	/**
	* Sets the descripcion of this municipio.
	*
	* @param descripcion the descripcion of this municipio
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_municipio.setDescripcion(descripcion);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_municipio.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_municipio.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_municipio.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_municipio.setNew(n);
	}

	/**
	* Sets the primary key of this municipio.
	*
	* @param primaryKey the primary key of this municipio
	*/
	@Override
	public void setPrimaryKey(
		mx.com.allianz.service.municipio.service.persistence.MunicipioPK primaryKey) {
		_municipio.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_municipio.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MunicipioWrapper)) {
			return false;
		}

		MunicipioWrapper municipioWrapper = (MunicipioWrapper)obj;

		if (Objects.equals(_municipio, municipioWrapper._municipio)) {
			return true;
		}

		return false;
	}

	@Override
	public Municipio getWrappedModel() {
		return _municipio;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _municipio.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _municipio.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_municipio.resetOriginalValues();
	}

	private final Municipio _municipio;
}