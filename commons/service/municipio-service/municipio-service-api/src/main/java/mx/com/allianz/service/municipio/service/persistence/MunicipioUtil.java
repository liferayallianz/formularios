/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.municipio.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.municipio.model.Municipio;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the municipio service. This utility wraps {@link mx.com.allianz.service.municipio.service.persistence.impl.MunicipioPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MunicipioPersistence
 * @see mx.com.allianz.service.municipio.service.persistence.impl.MunicipioPersistenceImpl
 * @generated
 */
@ProviderType
public class MunicipioUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Municipio municipio) {
		getPersistence().clearCache(municipio);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Municipio> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Municipio> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Municipio> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Municipio> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Municipio update(Municipio municipio) {
		return getPersistence().update(municipio);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Municipio update(Municipio municipio,
		ServiceContext serviceContext) {
		return getPersistence().update(municipio, serviceContext);
	}

	/**
	* Returns all the municipios where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching municipios
	*/
	public static List<Municipio> findByfindByDescripcion(
		java.lang.String descripcion) {
		return getPersistence().findByfindByDescripcion(descripcion);
	}

	/**
	* Returns a range of all the municipios where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @return the range of matching municipios
	*/
	public static List<Municipio> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end) {
		return getPersistence().findByfindByDescripcion(descripcion, start, end);
	}

	/**
	* Returns an ordered range of all the municipios where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching municipios
	*/
	public static List<Municipio> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<Municipio> orderByComparator) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the municipios where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching municipios
	*/
	public static List<Municipio> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<Municipio> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first municipio in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching municipio
	* @throws NoSuchMunicipioException if a matching municipio could not be found
	*/
	public static Municipio findByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<Municipio> orderByComparator)
		throws mx.com.allianz.service.municipio.exception.NoSuchMunicipioException {
		return getPersistence()
				   .findByfindByDescripcion_First(descripcion, orderByComparator);
	}

	/**
	* Returns the first municipio in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching municipio, or <code>null</code> if a matching municipio could not be found
	*/
	public static Municipio fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<Municipio> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_First(descripcion,
			orderByComparator);
	}

	/**
	* Returns the last municipio in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching municipio
	* @throws NoSuchMunicipioException if a matching municipio could not be found
	*/
	public static Municipio findByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<Municipio> orderByComparator)
		throws mx.com.allianz.service.municipio.exception.NoSuchMunicipioException {
		return getPersistence()
				   .findByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the last municipio in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching municipio, or <code>null</code> if a matching municipio could not be found
	*/
	public static Municipio fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<Municipio> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the municipios before and after the current municipio in the ordered set where descripcion = &#63;.
	*
	* @param municipioPK the primary key of the current municipio
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next municipio
	* @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	*/
	public static Municipio[] findByfindByDescripcion_PrevAndNext(
		MunicipioPK municipioPK, java.lang.String descripcion,
		OrderByComparator<Municipio> orderByComparator)
		throws mx.com.allianz.service.municipio.exception.NoSuchMunicipioException {
		return getPersistence()
				   .findByfindByDescripcion_PrevAndNext(municipioPK,
			descripcion, orderByComparator);
	}

	/**
	* Removes all the municipios where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public static void removeByfindByDescripcion(java.lang.String descripcion) {
		getPersistence().removeByfindByDescripcion(descripcion);
	}

	/**
	* Returns the number of municipios where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching municipios
	*/
	public static int countByfindByDescripcion(java.lang.String descripcion) {
		return getPersistence().countByfindByDescripcion(descripcion);
	}

	/**
	* Caches the municipio in the entity cache if it is enabled.
	*
	* @param municipio the municipio
	*/
	public static void cacheResult(Municipio municipio) {
		getPersistence().cacheResult(municipio);
	}

	/**
	* Caches the municipios in the entity cache if it is enabled.
	*
	* @param municipios the municipios
	*/
	public static void cacheResult(List<Municipio> municipios) {
		getPersistence().cacheResult(municipios);
	}

	/**
	* Creates a new municipio with the primary key. Does not add the municipio to the database.
	*
	* @param municipioPK the primary key for the new municipio
	* @return the new municipio
	*/
	public static Municipio create(MunicipioPK municipioPK) {
		return getPersistence().create(municipioPK);
	}

	/**
	* Removes the municipio with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param municipioPK the primary key of the municipio
	* @return the municipio that was removed
	* @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	*/
	public static Municipio remove(MunicipioPK municipioPK)
		throws mx.com.allianz.service.municipio.exception.NoSuchMunicipioException {
		return getPersistence().remove(municipioPK);
	}

	public static Municipio updateImpl(Municipio municipio) {
		return getPersistence().updateImpl(municipio);
	}

	/**
	* Returns the municipio with the primary key or throws a {@link NoSuchMunicipioException} if it could not be found.
	*
	* @param municipioPK the primary key of the municipio
	* @return the municipio
	* @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	*/
	public static Municipio findByPrimaryKey(MunicipioPK municipioPK)
		throws mx.com.allianz.service.municipio.exception.NoSuchMunicipioException {
		return getPersistence().findByPrimaryKey(municipioPK);
	}

	/**
	* Returns the municipio with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param municipioPK the primary key of the municipio
	* @return the municipio, or <code>null</code> if a municipio with the primary key could not be found
	*/
	public static Municipio fetchByPrimaryKey(MunicipioPK municipioPK) {
		return getPersistence().fetchByPrimaryKey(municipioPK);
	}

	public static java.util.Map<java.io.Serializable, Municipio> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the municipios.
	*
	* @return the municipios
	*/
	public static List<Municipio> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the municipios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @return the range of municipios
	*/
	public static List<Municipio> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the municipios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of municipios
	*/
	public static List<Municipio> findAll(int start, int end,
		OrderByComparator<Municipio> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the municipios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of municipios
	*/
	public static List<Municipio> findAll(int start, int end,
		OrderByComparator<Municipio> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the municipios from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of municipios.
	*
	* @return the number of municipios
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static MunicipioPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MunicipioPersistence, MunicipioPersistence> _serviceTracker =
		ServiceTrackerFactory.open(MunicipioPersistence.class);
}