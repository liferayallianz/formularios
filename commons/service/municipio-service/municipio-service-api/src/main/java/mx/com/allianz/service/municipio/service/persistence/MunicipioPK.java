/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.municipio.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class MunicipioPK implements Comparable<MunicipioPK>, Serializable {
	public int codigoPais;
	public int codigoEstado;
	public int codigoCiudad;
	public int codigoMunicipio;

	public MunicipioPK() {
	}

	public MunicipioPK(int codigoPais, int codigoEstado, int codigoCiudad,
		int codigoMunicipio) {
		this.codigoPais = codigoPais;
		this.codigoEstado = codigoEstado;
		this.codigoCiudad = codigoCiudad;
		this.codigoMunicipio = codigoMunicipio;
	}

	public int getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(int codigoPais) {
		this.codigoPais = codigoPais;
	}

	public int getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(int codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public int getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(int codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	public int getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public void setCodigoMunicipio(int codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	@Override
	public int compareTo(MunicipioPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (codigoPais < pk.codigoPais) {
			value = -1;
		}
		else if (codigoPais > pk.codigoPais) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (codigoEstado < pk.codigoEstado) {
			value = -1;
		}
		else if (codigoEstado > pk.codigoEstado) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (codigoCiudad < pk.codigoCiudad) {
			value = -1;
		}
		else if (codigoCiudad > pk.codigoCiudad) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (codigoMunicipio < pk.codigoMunicipio) {
			value = -1;
		}
		else if (codigoMunicipio > pk.codigoMunicipio) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MunicipioPK)) {
			return false;
		}

		MunicipioPK pk = (MunicipioPK)obj;

		if ((codigoPais == pk.codigoPais) && (codigoEstado == pk.codigoEstado) &&
				(codigoCiudad == pk.codigoCiudad) &&
				(codigoMunicipio == pk.codigoMunicipio)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, codigoPais);
		hashCode = HashUtil.hash(hashCode, codigoEstado);
		hashCode = HashUtil.hash(hashCode, codigoCiudad);
		hashCode = HashUtil.hash(hashCode, codigoMunicipio);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(20);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("codigoPais");
		sb.append(StringPool.EQUAL);
		sb.append(codigoPais);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codigoEstado");
		sb.append(StringPool.EQUAL);
		sb.append(codigoEstado);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codigoCiudad");
		sb.append(StringPool.EQUAL);
		sb.append(codigoCiudad);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codigoMunicipio");
		sb.append(StringPool.EQUAL);
		sb.append(codigoMunicipio);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}