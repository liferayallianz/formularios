/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.municipio.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.municipio.exception.NoSuchMunicipioException;
import mx.com.allianz.service.municipio.model.Municipio;

/**
 * The persistence interface for the municipio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.municipio.service.persistence.impl.MunicipioPersistenceImpl
 * @see MunicipioUtil
 * @generated
 */
@ProviderType
public interface MunicipioPersistence extends BasePersistence<Municipio> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MunicipioUtil} to access the municipio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the municipios where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching municipios
	*/
	public java.util.List<Municipio> findByfindByDescripcion(
		java.lang.String descripcion);

	/**
	* Returns a range of all the municipios where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @return the range of matching municipios
	*/
	public java.util.List<Municipio> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end);

	/**
	* Returns an ordered range of all the municipios where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching municipios
	*/
	public java.util.List<Municipio> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Municipio> orderByComparator);

	/**
	* Returns an ordered range of all the municipios where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching municipios
	*/
	public java.util.List<Municipio> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Municipio> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first municipio in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching municipio
	* @throws NoSuchMunicipioException if a matching municipio could not be found
	*/
	public Municipio findByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Municipio> orderByComparator)
		throws NoSuchMunicipioException;

	/**
	* Returns the first municipio in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching municipio, or <code>null</code> if a matching municipio could not be found
	*/
	public Municipio fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Municipio> orderByComparator);

	/**
	* Returns the last municipio in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching municipio
	* @throws NoSuchMunicipioException if a matching municipio could not be found
	*/
	public Municipio findByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Municipio> orderByComparator)
		throws NoSuchMunicipioException;

	/**
	* Returns the last municipio in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching municipio, or <code>null</code> if a matching municipio could not be found
	*/
	public Municipio fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Municipio> orderByComparator);

	/**
	* Returns the municipios before and after the current municipio in the ordered set where descripcion = &#63;.
	*
	* @param municipioPK the primary key of the current municipio
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next municipio
	* @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	*/
	public Municipio[] findByfindByDescripcion_PrevAndNext(
		MunicipioPK municipioPK, java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Municipio> orderByComparator)
		throws NoSuchMunicipioException;

	/**
	* Removes all the municipios where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public void removeByfindByDescripcion(java.lang.String descripcion);

	/**
	* Returns the number of municipios where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching municipios
	*/
	public int countByfindByDescripcion(java.lang.String descripcion);

	/**
	* Caches the municipio in the entity cache if it is enabled.
	*
	* @param municipio the municipio
	*/
	public void cacheResult(Municipio municipio);

	/**
	* Caches the municipios in the entity cache if it is enabled.
	*
	* @param municipios the municipios
	*/
	public void cacheResult(java.util.List<Municipio> municipios);

	/**
	* Creates a new municipio with the primary key. Does not add the municipio to the database.
	*
	* @param municipioPK the primary key for the new municipio
	* @return the new municipio
	*/
	public Municipio create(MunicipioPK municipioPK);

	/**
	* Removes the municipio with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param municipioPK the primary key of the municipio
	* @return the municipio that was removed
	* @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	*/
	public Municipio remove(MunicipioPK municipioPK)
		throws NoSuchMunicipioException;

	public Municipio updateImpl(Municipio municipio);

	/**
	* Returns the municipio with the primary key or throws a {@link NoSuchMunicipioException} if it could not be found.
	*
	* @param municipioPK the primary key of the municipio
	* @return the municipio
	* @throws NoSuchMunicipioException if a municipio with the primary key could not be found
	*/
	public Municipio findByPrimaryKey(MunicipioPK municipioPK)
		throws NoSuchMunicipioException;

	/**
	* Returns the municipio with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param municipioPK the primary key of the municipio
	* @return the municipio, or <code>null</code> if a municipio with the primary key could not be found
	*/
	public Municipio fetchByPrimaryKey(MunicipioPK municipioPK);

	@Override
	public java.util.Map<java.io.Serializable, Municipio> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the municipios.
	*
	* @return the municipios
	*/
	public java.util.List<Municipio> findAll();

	/**
	* Returns a range of all the municipios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @return the range of municipios
	*/
	public java.util.List<Municipio> findAll(int start, int end);

	/**
	* Returns an ordered range of all the municipios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of municipios
	*/
	public java.util.List<Municipio> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Municipio> orderByComparator);

	/**
	* Returns an ordered range of all the municipios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MunicipioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of municipios
	* @param end the upper bound of the range of municipios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of municipios
	*/
	public java.util.List<Municipio> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Municipio> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the municipios from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of municipios.
	*
	* @return the number of municipios
	*/
	public int countAll();
}