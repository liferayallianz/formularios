/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.documento.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class DocumentoSoap implements Serializable {
	public static DocumentoSoap toSoapModel(Documento model) {
		DocumentoSoap soapModel = new DocumentoSoap();

		soapModel.setIdDocumentos(model.getIdDocumentos());
		soapModel.setDescripcion(model.getDescripcion());
		soapModel.setTipo(model.getTipo());
		soapModel.setCodigo(model.getCodigo());

		return soapModel;
	}

	public static DocumentoSoap[] toSoapModels(Documento[] models) {
		DocumentoSoap[] soapModels = new DocumentoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DocumentoSoap[][] toSoapModels(Documento[][] models) {
		DocumentoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DocumentoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DocumentoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DocumentoSoap[] toSoapModels(List<Documento> models) {
		List<DocumentoSoap> soapModels = new ArrayList<DocumentoSoap>(models.size());

		for (Documento model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DocumentoSoap[soapModels.size()]);
	}

	public DocumentoSoap() {
	}

	public String getPrimaryKey() {
		return _idDocumentos;
	}

	public void setPrimaryKey(String pk) {
		setIdDocumentos(pk);
	}

	public String getIdDocumentos() {
		return _idDocumentos;
	}

	public void setIdDocumentos(String idDocumentos) {
		_idDocumentos = idDocumentos;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	public String getTipo() {
		return _tipo;
	}

	public void setTipo(String tipo) {
		_tipo = tipo;
	}

	public String getCodigo() {
		return _codigo;
	}

	public void setCodigo(String codigo) {
		_codigo = codigo;
	}

	private String _idDocumentos;
	private String _descripcion;
	private String _tipo;
	private String _codigo;
}