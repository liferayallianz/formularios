/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.documento.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the Documento service. Represents a row in the &quot;PTL_CAT_DOCUMENTOS_TR&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoModel
 * @see mx.com.allianz.service.documento.model.impl.DocumentoImpl
 * @see mx.com.allianz.service.documento.model.impl.DocumentoModelImpl
 * @generated
 */
@ImplementationClassName("mx.com.allianz.service.documento.model.impl.DocumentoImpl")
@ProviderType
public interface Documento extends DocumentoModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link mx.com.allianz.service.documento.model.impl.DocumentoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Documento, String> ID_DOCUMENTOS_ACCESSOR = new Accessor<Documento, String>() {
			@Override
			public String get(Documento documento) {
				return documento.getIdDocumentos();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<Documento> getTypeClass() {
				return Documento.class;
			}
		};
}