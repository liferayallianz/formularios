/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.documento.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.documento.model.Documento;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the documento service. This utility wraps {@link mx.com.allianz.service.documento.service.persistence.impl.DocumentoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoPersistence
 * @see mx.com.allianz.service.documento.service.persistence.impl.DocumentoPersistenceImpl
 * @generated
 */
@ProviderType
public class DocumentoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Documento documento) {
		getPersistence().clearCache(documento);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Documento> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Documento> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Documento> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Documento> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Documento update(Documento documento) {
		return getPersistence().update(documento);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Documento update(Documento documento,
		ServiceContext serviceContext) {
		return getPersistence().update(documento, serviceContext);
	}

	/**
	* Returns all the documentos where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching documentos
	*/
	public static List<Documento> findByfindByDescripcion(
		java.lang.String descripcion) {
		return getPersistence().findByfindByDescripcion(descripcion);
	}

	/**
	* Returns a range of all the documentos where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @return the range of matching documentos
	*/
	public static List<Documento> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end) {
		return getPersistence().findByfindByDescripcion(descripcion, start, end);
	}

	/**
	* Returns an ordered range of all the documentos where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documentos
	*/
	public static List<Documento> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<Documento> orderByComparator) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the documentos where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documentos
	*/
	public static List<Documento> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<Documento> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first documento in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento
	* @throws NoSuchDocumentoException if a matching documento could not be found
	*/
	public static Documento findByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<Documento> orderByComparator)
		throws mx.com.allianz.service.documento.exception.NoSuchDocumentoException {
		return getPersistence()
				   .findByfindByDescripcion_First(descripcion, orderByComparator);
	}

	/**
	* Returns the first documento in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento, or <code>null</code> if a matching documento could not be found
	*/
	public static Documento fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<Documento> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_First(descripcion,
			orderByComparator);
	}

	/**
	* Returns the last documento in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento
	* @throws NoSuchDocumentoException if a matching documento could not be found
	*/
	public static Documento findByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<Documento> orderByComparator)
		throws mx.com.allianz.service.documento.exception.NoSuchDocumentoException {
		return getPersistence()
				   .findByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the last documento in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento, or <code>null</code> if a matching documento could not be found
	*/
	public static Documento fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<Documento> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the documentos before and after the current documento in the ordered set where descripcion = &#63;.
	*
	* @param idDocumentos the primary key of the current documento
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento
	* @throws NoSuchDocumentoException if a documento with the primary key could not be found
	*/
	public static Documento[] findByfindByDescripcion_PrevAndNext(
		java.lang.String idDocumentos, java.lang.String descripcion,
		OrderByComparator<Documento> orderByComparator)
		throws mx.com.allianz.service.documento.exception.NoSuchDocumentoException {
		return getPersistence()
				   .findByfindByDescripcion_PrevAndNext(idDocumentos,
			descripcion, orderByComparator);
	}

	/**
	* Removes all the documentos where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public static void removeByfindByDescripcion(java.lang.String descripcion) {
		getPersistence().removeByfindByDescripcion(descripcion);
	}

	/**
	* Returns the number of documentos where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching documentos
	*/
	public static int countByfindByDescripcion(java.lang.String descripcion) {
		return getPersistence().countByfindByDescripcion(descripcion);
	}

	/**
	* Caches the documento in the entity cache if it is enabled.
	*
	* @param documento the documento
	*/
	public static void cacheResult(Documento documento) {
		getPersistence().cacheResult(documento);
	}

	/**
	* Caches the documentos in the entity cache if it is enabled.
	*
	* @param documentos the documentos
	*/
	public static void cacheResult(List<Documento> documentos) {
		getPersistence().cacheResult(documentos);
	}

	/**
	* Creates a new documento with the primary key. Does not add the documento to the database.
	*
	* @param idDocumentos the primary key for the new documento
	* @return the new documento
	*/
	public static Documento create(java.lang.String idDocumentos) {
		return getPersistence().create(idDocumentos);
	}

	/**
	* Removes the documento with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idDocumentos the primary key of the documento
	* @return the documento that was removed
	* @throws NoSuchDocumentoException if a documento with the primary key could not be found
	*/
	public static Documento remove(java.lang.String idDocumentos)
		throws mx.com.allianz.service.documento.exception.NoSuchDocumentoException {
		return getPersistence().remove(idDocumentos);
	}

	public static Documento updateImpl(Documento documento) {
		return getPersistence().updateImpl(documento);
	}

	/**
	* Returns the documento with the primary key or throws a {@link NoSuchDocumentoException} if it could not be found.
	*
	* @param idDocumentos the primary key of the documento
	* @return the documento
	* @throws NoSuchDocumentoException if a documento with the primary key could not be found
	*/
	public static Documento findByPrimaryKey(java.lang.String idDocumentos)
		throws mx.com.allianz.service.documento.exception.NoSuchDocumentoException {
		return getPersistence().findByPrimaryKey(idDocumentos);
	}

	/**
	* Returns the documento with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idDocumentos the primary key of the documento
	* @return the documento, or <code>null</code> if a documento with the primary key could not be found
	*/
	public static Documento fetchByPrimaryKey(java.lang.String idDocumentos) {
		return getPersistence().fetchByPrimaryKey(idDocumentos);
	}

	public static java.util.Map<java.io.Serializable, Documento> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the documentos.
	*
	* @return the documentos
	*/
	public static List<Documento> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the documentos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @return the range of documentos
	*/
	public static List<Documento> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the documentos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of documentos
	*/
	public static List<Documento> findAll(int start, int end,
		OrderByComparator<Documento> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the documentos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of documentos
	*/
	public static List<Documento> findAll(int start, int end,
		OrderByComparator<Documento> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the documentos from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of documentos.
	*
	* @return the number of documentos
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static DocumentoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DocumentoPersistence, DocumentoPersistence> _serviceTracker =
		ServiceTrackerFactory.open(DocumentoPersistence.class);
}