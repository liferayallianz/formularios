/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.documento.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Documento}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Documento
 * @generated
 */
@ProviderType
public class DocumentoWrapper implements Documento, ModelWrapper<Documento> {
	public DocumentoWrapper(Documento documento) {
		_documento = documento;
	}

	@Override
	public Class<?> getModelClass() {
		return Documento.class;
	}

	@Override
	public String getModelClassName() {
		return Documento.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idDocumentos", getIdDocumentos());
		attributes.put("descripcion", getDescripcion());
		attributes.put("tipo", getTipo());
		attributes.put("codigo", getCodigo());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String idDocumentos = (String)attributes.get("idDocumentos");

		if (idDocumentos != null) {
			setIdDocumentos(idDocumentos);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		String tipo = (String)attributes.get("tipo");

		if (tipo != null) {
			setTipo(tipo);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _documento.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _documento.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _documento.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _documento.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Documento> toCacheModel() {
		return _documento.toCacheModel();
	}

	@Override
	public int compareTo(Documento documento) {
		return _documento.compareTo(documento);
	}

	@Override
	public int hashCode() {
		return _documento.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _documento.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new DocumentoWrapper((Documento)_documento.clone());
	}

	/**
	* Returns the codigo of this documento.
	*
	* @return the codigo of this documento
	*/
	@Override
	public java.lang.String getCodigo() {
		return _documento.getCodigo();
	}

	/**
	* Returns the descripcion of this documento.
	*
	* @return the descripcion of this documento
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _documento.getDescripcion();
	}

	/**
	* Returns the id documentos of this documento.
	*
	* @return the id documentos of this documento
	*/
	@Override
	public java.lang.String getIdDocumentos() {
		return _documento.getIdDocumentos();
	}

	/**
	* Returns the primary key of this documento.
	*
	* @return the primary key of this documento
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _documento.getPrimaryKey();
	}

	/**
	* Returns the tipo of this documento.
	*
	* @return the tipo of this documento
	*/
	@Override
	public java.lang.String getTipo() {
		return _documento.getTipo();
	}

	@Override
	public java.lang.String toString() {
		return _documento.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _documento.toXmlString();
	}

	@Override
	public Documento toEscapedModel() {
		return new DocumentoWrapper(_documento.toEscapedModel());
	}

	@Override
	public Documento toUnescapedModel() {
		return new DocumentoWrapper(_documento.toUnescapedModel());
	}

	@Override
	public void persist() {
		_documento.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_documento.setCachedModel(cachedModel);
	}

	/**
	* Sets the codigo of this documento.
	*
	* @param codigo the codigo of this documento
	*/
	@Override
	public void setCodigo(java.lang.String codigo) {
		_documento.setCodigo(codigo);
	}

	/**
	* Sets the descripcion of this documento.
	*
	* @param descripcion the descripcion of this documento
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_documento.setDescripcion(descripcion);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_documento.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_documento.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_documento.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the id documentos of this documento.
	*
	* @param idDocumentos the id documentos of this documento
	*/
	@Override
	public void setIdDocumentos(java.lang.String idDocumentos) {
		_documento.setIdDocumentos(idDocumentos);
	}

	@Override
	public void setNew(boolean n) {
		_documento.setNew(n);
	}

	/**
	* Sets the primary key of this documento.
	*
	* @param primaryKey the primary key of this documento
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_documento.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_documento.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the tipo of this documento.
	*
	* @param tipo the tipo of this documento
	*/
	@Override
	public void setTipo(java.lang.String tipo) {
		_documento.setTipo(tipo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DocumentoWrapper)) {
			return false;
		}

		DocumentoWrapper documentoWrapper = (DocumentoWrapper)obj;

		if (Objects.equals(_documento, documentoWrapper._documento)) {
			return true;
		}

		return false;
	}

	@Override
	public Documento getWrappedModel() {
		return _documento;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _documento.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _documento.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_documento.resetOriginalValues();
	}

	private final Documento _documento;
}