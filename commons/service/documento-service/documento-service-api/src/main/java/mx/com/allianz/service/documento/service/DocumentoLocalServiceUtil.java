/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.documento.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Documento. This utility wraps
 * {@link mx.com.allianz.service.documento.service.impl.DocumentoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoLocalService
 * @see mx.com.allianz.service.documento.service.base.DocumentoLocalServiceBaseImpl
 * @see mx.com.allianz.service.documento.service.impl.DocumentoLocalServiceImpl
 * @generated
 */
@ProviderType
public class DocumentoLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link mx.com.allianz.service.documento.service.impl.DocumentoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of documentos.
	*
	* @return the number of documentos
	*/
	public static int getDocumentosCount() {
		return getService().getDocumentosCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.documento.model.impl.DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.documento.model.impl.DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the documentos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.documento.model.impl.DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @return the range of documentos
	*/
	public static java.util.List<mx.com.allianz.service.documento.model.Documento> getDocumentos(
		int start, int end) {
		return getService().getDocumentos(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the documento to the database. Also notifies the appropriate model listeners.
	*
	* @param documento the documento
	* @return the documento that was added
	*/
	public static mx.com.allianz.service.documento.model.Documento addDocumento(
		mx.com.allianz.service.documento.model.Documento documento) {
		return getService().addDocumento(documento);
	}

	/**
	* Creates a new documento with the primary key. Does not add the documento to the database.
	*
	* @param idDocumentos the primary key for the new documento
	* @return the new documento
	*/
	public static mx.com.allianz.service.documento.model.Documento createDocumento(
		java.lang.String idDocumentos) {
		return getService().createDocumento(idDocumentos);
	}

	/**
	* Deletes the documento with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idDocumentos the primary key of the documento
	* @return the documento that was removed
	* @throws PortalException if a documento with the primary key could not be found
	*/
	public static mx.com.allianz.service.documento.model.Documento deleteDocumento(
		java.lang.String idDocumentos)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteDocumento(idDocumentos);
	}

	/**
	* Deletes the documento from the database. Also notifies the appropriate model listeners.
	*
	* @param documento the documento
	* @return the documento that was removed
	*/
	public static mx.com.allianz.service.documento.model.Documento deleteDocumento(
		mx.com.allianz.service.documento.model.Documento documento) {
		return getService().deleteDocumento(documento);
	}

	public static mx.com.allianz.service.documento.model.Documento fetchDocumento(
		java.lang.String idDocumentos) {
		return getService().fetchDocumento(idDocumentos);
	}

	/**
	* Returns the documento with the primary key.
	*
	* @param idDocumentos the primary key of the documento
	* @return the documento
	* @throws PortalException if a documento with the primary key could not be found
	*/
	public static mx.com.allianz.service.documento.model.Documento getDocumento(
		java.lang.String idDocumentos)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getDocumento(idDocumentos);
	}

	/**
	* Updates the documento in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param documento the documento
	* @return the documento that was updated
	*/
	public static mx.com.allianz.service.documento.model.Documento updateDocumento(
		mx.com.allianz.service.documento.model.Documento documento) {
		return getService().updateDocumento(documento);
	}

	public static DocumentoLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DocumentoLocalService, DocumentoLocalService> _serviceTracker =
		ServiceTrackerFactory.open(DocumentoLocalService.class);
}