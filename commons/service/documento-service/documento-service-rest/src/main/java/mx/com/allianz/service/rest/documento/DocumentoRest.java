/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mx.com.allianz.service.rest.documento;


import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.DocumentoDTO;
import mx.com.allianz.service.documento.service.DocumentoLocalServiceUtil;


/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio
 * Documentos TR que permite visualizar una lista de datos, 
 * filtra el orden de la b\u00fasqueda y muestra el ordenamiento 
 * conforme al filtro
 * 
 *
 * @author  TestingZone
 * @version 1.0
 * @since   2016-12-5 
 * 
 * 
 */
/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n (Modulo)
 */
@ApplicationPath("/catalogos.documentos")

/*
 * @Component sirve para garantizar la seguridad del path
 */
@Component(
	immediate = true, property = {"jaxrs.application=true"},
	service = Application.class
)
public class DocumentoRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this).
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object)this);
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todo los documentos del cat\u00e1logo.
	 *  
	 * @return getAllDocTRs()
	 */
	
	//@GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	//@Path sirve para poner la ruta relativa donde la clase ser\u00e1 hospedada
	@Path("/getDocumentos")
	//@Produces sirve para especificar el tipo de formato de respuesta
	@Produces({MediaType.APPLICATION_JSON})
	public List<DocumentoDTO> getDocTRs() {
		// Se regresan todos los Documentos 
		return getAllDocTRs();
	}
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los documentos del cat\u00e1logo.
	 * 
	 * @return getAllDocTRs().stream().
	 */
	//@GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	//@Path sirve para poner la ruta relativa donde la clase ser\u00e1.
	@Path("/findDocumentos")
	//@Produces sirve para especificar el tipo de formato de respuesta
	@Produces({MediaType.APPLICATION_JSON})
	public List<DocumentoDTO>findDocTRsBy(
			//El valor @DefaultValue se utiliza para definir el valor por defecto para el par\u00e1metro matrixParam.
			//@QueryParam se utiliza para extraer los par\u00e1metros de consulta del "componente de consulta" de la URL de la solicitud.
			@DefaultValue("")@QueryParam("id_DocsTR") String idDocsTR,
			@DefaultValue("")@QueryParam("tipo") String tipo,
			@DefaultValue("")@QueryParam("codigo") String codigo,
			@DefaultValue("")@QueryParam("description") String description){
		//stream() Devuelve un flujo secuencial considerando la colecci\u00f3n como su origen
		//convierte la lista en stream.
		return getAllDocTRs().stream()
				
				// filtrado por identificador de documentos, si se env\u00eda -1 se ignora el filtro 
				.filter((docsTR) -> idDocsTR.isEmpty() ||  docsTR.getIdDocumentos().contains(idDocsTR))
				// filtrado por tipo de documento, si se env\u00eda vacio se ignora el filtro 
				.filter((docsTR) -> tipo.isEmpty() ||  docsTR.getTipo().contains(tipo))
				// filtrado por codigo de documento , si se env\u00eda vacio se ignora el filtro 
				.filter((docsTR) -> codigo.isEmpty() ||  docsTR.getCodigo().contains(codigo))
				// filtrado por descripcion del documento, si se env\u00eda vacio se ignora el filtro
				.filter((docsTR) -> description.isEmpty() || docsTR.getDescripcion().contains(description))
				//salida de colección y el stream se convierte en lista
				.collect(Collectors.toList());
				
	}
	
	/**
	 *Método getAllDocTRs del tipo list generada para mostrar un arreglo de la lista
	 * @return listaDocsTR
	 */
	private List<DocumentoDTO> getAllDocTRs(){
		
		return DocumentoLocalServiceUtil.getDocumentos(-1, -1).stream().
				map(documento -> new DocumentoDTO(documento.getIdDocumentos(),
						documento.getTipo(), documento.getCodigo(),
						documento.getDescripcion())).
				collect(Collectors.toList());

		
	}
	


}