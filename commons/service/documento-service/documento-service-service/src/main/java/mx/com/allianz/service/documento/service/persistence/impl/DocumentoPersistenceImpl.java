/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.documento.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.documento.exception.NoSuchDocumentoException;
import mx.com.allianz.service.documento.model.Documento;
import mx.com.allianz.service.documento.model.impl.DocumentoImpl;
import mx.com.allianz.service.documento.model.impl.DocumentoModelImpl;
import mx.com.allianz.service.documento.service.persistence.DocumentoPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the documento service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoPersistence
 * @see mx.com.allianz.service.documento.service.persistence.DocumentoUtil
 * @generated
 */
@ProviderType
public class DocumentoPersistenceImpl extends BasePersistenceImpl<Documento>
	implements DocumentoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DocumentoUtil} to access the documento persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DocumentoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoModelImpl.FINDER_CACHE_ENABLED, DocumentoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoModelImpl.FINDER_CACHE_ENABLED, DocumentoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoModelImpl.FINDER_CACHE_ENABLED, DocumentoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByfindByDescripcion",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoModelImpl.FINDER_CACHE_ENABLED, DocumentoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByDescripcion", new String[] { String.class.getName() },
			DocumentoModelImpl.DESCRIPCION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION = new FinderPath(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByDescripcion", new String[] { String.class.getName() });

	/**
	 * Returns all the documentos where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the matching documentos
	 */
	@Override
	public List<Documento> findByfindByDescripcion(String descripcion) {
		return findByfindByDescripcion(descripcion, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the documentos where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of documentos
	 * @param end the upper bound of the range of documentos (not inclusive)
	 * @return the range of matching documentos
	 */
	@Override
	public List<Documento> findByfindByDescripcion(String descripcion,
		int start, int end) {
		return findByfindByDescripcion(descripcion, start, end, null);
	}

	/**
	 * Returns an ordered range of all the documentos where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of documentos
	 * @param end the upper bound of the range of documentos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching documentos
	 */
	@Override
	public List<Documento> findByfindByDescripcion(String descripcion,
		int start, int end, OrderByComparator<Documento> orderByComparator) {
		return findByfindByDescripcion(descripcion, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the documentos where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of documentos
	 * @param end the upper bound of the range of documentos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching documentos
	 */
	@Override
	public List<Documento> findByfindByDescripcion(String descripcion,
		int start, int end, OrderByComparator<Documento> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion, start, end, orderByComparator };
		}

		List<Documento> list = null;

		if (retrieveFromCache) {
			list = (List<Documento>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Documento documento : list) {
					if (!Objects.equals(descripcion, documento.getDescripcion())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DOCUMENTO_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DocumentoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				if (!pagination) {
					list = (List<Documento>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Documento>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first documento in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento
	 * @throws NoSuchDocumentoException if a matching documento could not be found
	 */
	@Override
	public Documento findByfindByDescripcion_First(String descripcion,
		OrderByComparator<Documento> orderByComparator)
		throws NoSuchDocumentoException {
		Documento documento = fetchByfindByDescripcion_First(descripcion,
				orderByComparator);

		if (documento != null) {
			return documento;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDocumentoException(msg.toString());
	}

	/**
	 * Returns the first documento in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento, or <code>null</code> if a matching documento could not be found
	 */
	@Override
	public Documento fetchByfindByDescripcion_First(String descripcion,
		OrderByComparator<Documento> orderByComparator) {
		List<Documento> list = findByfindByDescripcion(descripcion, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last documento in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento
	 * @throws NoSuchDocumentoException if a matching documento could not be found
	 */
	@Override
	public Documento findByfindByDescripcion_Last(String descripcion,
		OrderByComparator<Documento> orderByComparator)
		throws NoSuchDocumentoException {
		Documento documento = fetchByfindByDescripcion_Last(descripcion,
				orderByComparator);

		if (documento != null) {
			return documento;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDocumentoException(msg.toString());
	}

	/**
	 * Returns the last documento in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento, or <code>null</code> if a matching documento could not be found
	 */
	@Override
	public Documento fetchByfindByDescripcion_Last(String descripcion,
		OrderByComparator<Documento> orderByComparator) {
		int count = countByfindByDescripcion(descripcion);

		if (count == 0) {
			return null;
		}

		List<Documento> list = findByfindByDescripcion(descripcion, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the documentos before and after the current documento in the ordered set where descripcion = &#63;.
	 *
	 * @param idDocumentos the primary key of the current documento
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next documento
	 * @throws NoSuchDocumentoException if a documento with the primary key could not be found
	 */
	@Override
	public Documento[] findByfindByDescripcion_PrevAndNext(
		String idDocumentos, String descripcion,
		OrderByComparator<Documento> orderByComparator)
		throws NoSuchDocumentoException {
		Documento documento = findByPrimaryKey(idDocumentos);

		Session session = null;

		try {
			session = openSession();

			Documento[] array = new DocumentoImpl[3];

			array[0] = getByfindByDescripcion_PrevAndNext(session, documento,
					descripcion, orderByComparator, true);

			array[1] = documento;

			array[2] = getByfindByDescripcion_PrevAndNext(session, documento,
					descripcion, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Documento getByfindByDescripcion_PrevAndNext(Session session,
		Documento documento, String descripcion,
		OrderByComparator<Documento> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DOCUMENTO_WHERE);

		boolean bindDescripcion = false;

		if (descripcion == null) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
		}
		else if (descripcion.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
		}
		else {
			bindDescripcion = true;

			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DocumentoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDescripcion) {
			qPos.add(descripcion);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(documento);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Documento> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the documentos where descripcion = &#63; from the database.
	 *
	 * @param descripcion the descripcion
	 */
	@Override
	public void removeByfindByDescripcion(String descripcion) {
		for (Documento documento : findByfindByDescripcion(descripcion,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(documento);
		}
	}

	/**
	 * Returns the number of documentos where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the number of matching documentos
	 */
	@Override
	public int countByfindByDescripcion(String descripcion) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION;

		Object[] finderArgs = new Object[] { descripcion };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCUMENTO_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1 = "documento.descripcion IS NULL";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2 = "documento.descripcion = ?";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3 = "(documento.descripcion IS NULL OR documento.descripcion = '')";

	public DocumentoPersistenceImpl() {
		setModelClass(Documento.class);
	}

	/**
	 * Caches the documento in the entity cache if it is enabled.
	 *
	 * @param documento the documento
	 */
	@Override
	public void cacheResult(Documento documento) {
		entityCache.putResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoImpl.class, documento.getPrimaryKey(), documento);

		documento.resetOriginalValues();
	}

	/**
	 * Caches the documentos in the entity cache if it is enabled.
	 *
	 * @param documentos the documentos
	 */
	@Override
	public void cacheResult(List<Documento> documentos) {
		for (Documento documento : documentos) {
			if (entityCache.getResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
						DocumentoImpl.class, documento.getPrimaryKey()) == null) {
				cacheResult(documento);
			}
			else {
				documento.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all documentos.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(DocumentoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the documento.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Documento documento) {
		entityCache.removeResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoImpl.class, documento.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Documento> documentos) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Documento documento : documentos) {
			entityCache.removeResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
				DocumentoImpl.class, documento.getPrimaryKey());
		}
	}

	/**
	 * Creates a new documento with the primary key. Does not add the documento to the database.
	 *
	 * @param idDocumentos the primary key for the new documento
	 * @return the new documento
	 */
	@Override
	public Documento create(String idDocumentos) {
		Documento documento = new DocumentoImpl();

		documento.setNew(true);
		documento.setPrimaryKey(idDocumentos);

		return documento;
	}

	/**
	 * Removes the documento with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idDocumentos the primary key of the documento
	 * @return the documento that was removed
	 * @throws NoSuchDocumentoException if a documento with the primary key could not be found
	 */
	@Override
	public Documento remove(String idDocumentos)
		throws NoSuchDocumentoException {
		return remove((Serializable)idDocumentos);
	}

	/**
	 * Removes the documento with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the documento
	 * @return the documento that was removed
	 * @throws NoSuchDocumentoException if a documento with the primary key could not be found
	 */
	@Override
	public Documento remove(Serializable primaryKey)
		throws NoSuchDocumentoException {
		Session session = null;

		try {
			session = openSession();

			Documento documento = (Documento)session.get(DocumentoImpl.class,
					primaryKey);

			if (documento == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDocumentoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(documento);
		}
		catch (NoSuchDocumentoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Documento removeImpl(Documento documento) {
		documento = toUnwrappedModel(documento);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(documento)) {
				documento = (Documento)session.get(DocumentoImpl.class,
						documento.getPrimaryKeyObj());
			}

			if (documento != null) {
				session.delete(documento);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (documento != null) {
			clearCache(documento);
		}

		return documento;
	}

	@Override
	public Documento updateImpl(Documento documento) {
		documento = toUnwrappedModel(documento);

		boolean isNew = documento.isNew();

		DocumentoModelImpl documentoModelImpl = (DocumentoModelImpl)documento;

		Session session = null;

		try {
			session = openSession();

			if (documento.isNew()) {
				session.save(documento);

				documento.setNew(false);
			}
			else {
				documento = (Documento)session.merge(documento);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DocumentoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((documentoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						documentoModelImpl.getOriginalDescripcion()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);

				args = new Object[] { documentoModelImpl.getDescripcion() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);
			}
		}

		entityCache.putResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoImpl.class, documento.getPrimaryKey(), documento, false);

		documento.resetOriginalValues();

		return documento;
	}

	protected Documento toUnwrappedModel(Documento documento) {
		if (documento instanceof DocumentoImpl) {
			return documento;
		}

		DocumentoImpl documentoImpl = new DocumentoImpl();

		documentoImpl.setNew(documento.isNew());
		documentoImpl.setPrimaryKey(documento.getPrimaryKey());

		documentoImpl.setIdDocumentos(documento.getIdDocumentos());
		documentoImpl.setDescripcion(documento.getDescripcion());
		documentoImpl.setTipo(documento.getTipo());
		documentoImpl.setCodigo(documento.getCodigo());

		return documentoImpl;
	}

	/**
	 * Returns the documento with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the documento
	 * @return the documento
	 * @throws NoSuchDocumentoException if a documento with the primary key could not be found
	 */
	@Override
	public Documento findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDocumentoException {
		Documento documento = fetchByPrimaryKey(primaryKey);

		if (documento == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDocumentoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return documento;
	}

	/**
	 * Returns the documento with the primary key or throws a {@link NoSuchDocumentoException} if it could not be found.
	 *
	 * @param idDocumentos the primary key of the documento
	 * @return the documento
	 * @throws NoSuchDocumentoException if a documento with the primary key could not be found
	 */
	@Override
	public Documento findByPrimaryKey(String idDocumentos)
		throws NoSuchDocumentoException {
		return findByPrimaryKey((Serializable)idDocumentos);
	}

	/**
	 * Returns the documento with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the documento
	 * @return the documento, or <code>null</code> if a documento with the primary key could not be found
	 */
	@Override
	public Documento fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
				DocumentoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Documento documento = (Documento)serializable;

		if (documento == null) {
			Session session = null;

			try {
				session = openSession();

				documento = (Documento)session.get(DocumentoImpl.class,
						primaryKey);

				if (documento != null) {
					cacheResult(documento);
				}
				else {
					entityCache.putResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
						DocumentoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
					DocumentoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return documento;
	}

	/**
	 * Returns the documento with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idDocumentos the primary key of the documento
	 * @return the documento, or <code>null</code> if a documento with the primary key could not be found
	 */
	@Override
	public Documento fetchByPrimaryKey(String idDocumentos) {
		return fetchByPrimaryKey((Serializable)idDocumentos);
	}

	@Override
	public Map<Serializable, Documento> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Documento> map = new HashMap<Serializable, Documento>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Documento documento = fetchByPrimaryKey(primaryKey);

			if (documento != null) {
				map.put(primaryKey, documento);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
					DocumentoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Documento)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 4) +
				1);

		query.append(_SQL_SELECT_DOCUMENTO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(StringPool.APOSTROPHE);
			query.append((String)primaryKey);
			query.append(StringPool.APOSTROPHE);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Documento documento : (List<Documento>)q.list()) {
				map.put(documento.getPrimaryKeyObj(), documento);

				cacheResult(documento);

				uncachedPrimaryKeys.remove(documento.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(DocumentoModelImpl.ENTITY_CACHE_ENABLED,
					DocumentoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the documentos.
	 *
	 * @return the documentos
	 */
	@Override
	public List<Documento> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the documentos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of documentos
	 * @param end the upper bound of the range of documentos (not inclusive)
	 * @return the range of documentos
	 */
	@Override
	public List<Documento> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the documentos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of documentos
	 * @param end the upper bound of the range of documentos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of documentos
	 */
	@Override
	public List<Documento> findAll(int start, int end,
		OrderByComparator<Documento> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the documentos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of documentos
	 * @param end the upper bound of the range of documentos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of documentos
	 */
	@Override
	public List<Documento> findAll(int start, int end,
		OrderByComparator<Documento> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Documento> list = null;

		if (retrieveFromCache) {
			list = (List<Documento>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_DOCUMENTO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DOCUMENTO;

				if (pagination) {
					sql = sql.concat(DocumentoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Documento>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Documento>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the documentos from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Documento documento : findAll()) {
			remove(documento);
		}
	}

	/**
	 * Returns the number of documentos.
	 *
	 * @return the number of documentos
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DOCUMENTO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return DocumentoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the documento persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(DocumentoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_DOCUMENTO = "SELECT documento FROM Documento documento";
	private static final String _SQL_SELECT_DOCUMENTO_WHERE_PKS_IN = "SELECT documento FROM Documento documento WHERE idDocumentos IN (";
	private static final String _SQL_SELECT_DOCUMENTO_WHERE = "SELECT documento FROM Documento documento WHERE ";
	private static final String _SQL_COUNT_DOCUMENTO = "SELECT COUNT(documento) FROM Documento documento";
	private static final String _SQL_COUNT_DOCUMENTO_WHERE = "SELECT COUNT(documento) FROM Documento documento WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "documento.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Documento exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Documento exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(DocumentoPersistenceImpl.class);
}