/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.documento.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.documento.model.Documento;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Documento in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Documento
 * @generated
 */
@ProviderType
public class DocumentoCacheModel implements CacheModel<Documento>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DocumentoCacheModel)) {
			return false;
		}

		DocumentoCacheModel documentoCacheModel = (DocumentoCacheModel)obj;

		if (idDocumentos.equals(documentoCacheModel.idDocumentos)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, idDocumentos);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{idDocumentos=");
		sb.append(idDocumentos);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append(", tipo=");
		sb.append(tipo);
		sb.append(", codigo=");
		sb.append(codigo);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Documento toEntityModel() {
		DocumentoImpl documentoImpl = new DocumentoImpl();

		if (idDocumentos == null) {
			documentoImpl.setIdDocumentos(StringPool.BLANK);
		}
		else {
			documentoImpl.setIdDocumentos(idDocumentos);
		}

		if (descripcion == null) {
			documentoImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			documentoImpl.setDescripcion(descripcion);
		}

		if (tipo == null) {
			documentoImpl.setTipo(StringPool.BLANK);
		}
		else {
			documentoImpl.setTipo(tipo);
		}

		if (codigo == null) {
			documentoImpl.setCodigo(StringPool.BLANK);
		}
		else {
			documentoImpl.setCodigo(codigo);
		}

		documentoImpl.resetOriginalValues();

		return documentoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idDocumentos = objectInput.readUTF();
		descripcion = objectInput.readUTF();
		tipo = objectInput.readUTF();
		codigo = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (idDocumentos == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(idDocumentos);
		}

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}

		if (tipo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipo);
		}

		if (codigo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigo);
		}
	}

	public String idDocumentos;
	public String descripcion;
	public String tipo;
	public String codigo;
}