/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.afectado.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.afectado.model.Afectado;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Afectado in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Afectado
 * @generated
 */
@ProviderType
public class AfectadoCacheModel implements CacheModel<Afectado>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AfectadoCacheModel)) {
			return false;
		}

		AfectadoCacheModel afectadoCacheModel = (AfectadoCacheModel)obj;

		if (id == afectadoCacheModel.id) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, id);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{id=");
		sb.append(id);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", apellidoPaterno=");
		sb.append(apellidoPaterno);
		sb.append(", apellidoMaterno=");
		sb.append(apellidoMaterno);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Afectado toEntityModel() {
		AfectadoImpl afectadoImpl = new AfectadoImpl();

		afectadoImpl.setId(id);

		if (nombre == null) {
			afectadoImpl.setNombre(StringPool.BLANK);
		}
		else {
			afectadoImpl.setNombre(nombre);
		}

		if (apellidoPaterno == null) {
			afectadoImpl.setApellidoPaterno(StringPool.BLANK);
		}
		else {
			afectadoImpl.setApellidoPaterno(apellidoPaterno);
		}

		if (apellidoMaterno == null) {
			afectadoImpl.setApellidoMaterno(StringPool.BLANK);
		}
		else {
			afectadoImpl.setApellidoMaterno(apellidoMaterno);
		}

		afectadoImpl.resetOriginalValues();

		return afectadoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		nombre = objectInput.readUTF();
		apellidoPaterno = objectInput.readUTF();
		apellidoMaterno = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (apellidoPaterno == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidoPaterno);
		}

		if (apellidoMaterno == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidoMaterno);
		}
	}

	public long id;
	public String nombre;
	public String apellidoPaterno;
	public String apellidoMaterno;
}