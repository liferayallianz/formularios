/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.afectado.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.afectado.exception.NoSuchAfectadoException;
import mx.com.allianz.service.afectado.model.Afectado;

/**
 * The persistence interface for the afectado service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.afectado.service.persistence.impl.AfectadoPersistenceImpl
 * @see AfectadoUtil
 * @generated
 */
@ProviderType
public interface AfectadoPersistence extends BasePersistence<Afectado> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AfectadoUtil} to access the afectado persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the afectados where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching afectados
	*/
	public java.util.List<Afectado> findBynombre(java.lang.String nombre);

	/**
	* Returns a range of all the afectados where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @return the range of matching afectados
	*/
	public java.util.List<Afectado> findBynombre(java.lang.String nombre,
		int start, int end);

	/**
	* Returns an ordered range of all the afectados where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching afectados
	*/
	public java.util.List<Afectado> findBynombre(java.lang.String nombre,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Afectado> orderByComparator);

	/**
	* Returns an ordered range of all the afectados where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching afectados
	*/
	public java.util.List<Afectado> findBynombre(java.lang.String nombre,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Afectado> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first afectado in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching afectado
	* @throws NoSuchAfectadoException if a matching afectado could not be found
	*/
	public Afectado findBynombre_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Afectado> orderByComparator)
		throws NoSuchAfectadoException;

	/**
	* Returns the first afectado in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching afectado, or <code>null</code> if a matching afectado could not be found
	*/
	public Afectado fetchBynombre_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Afectado> orderByComparator);

	/**
	* Returns the last afectado in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching afectado
	* @throws NoSuchAfectadoException if a matching afectado could not be found
	*/
	public Afectado findBynombre_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Afectado> orderByComparator)
		throws NoSuchAfectadoException;

	/**
	* Returns the last afectado in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching afectado, or <code>null</code> if a matching afectado could not be found
	*/
	public Afectado fetchBynombre_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Afectado> orderByComparator);

	/**
	* Returns the afectados before and after the current afectado in the ordered set where nombre = &#63;.
	*
	* @param id the primary key of the current afectado
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next afectado
	* @throws NoSuchAfectadoException if a afectado with the primary key could not be found
	*/
	public Afectado[] findBynombre_PrevAndNext(long id,
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Afectado> orderByComparator)
		throws NoSuchAfectadoException;

	/**
	* Removes all the afectados where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public void removeBynombre(java.lang.String nombre);

	/**
	* Returns the number of afectados where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching afectados
	*/
	public int countBynombre(java.lang.String nombre);

	/**
	* Caches the afectado in the entity cache if it is enabled.
	*
	* @param afectado the afectado
	*/
	public void cacheResult(Afectado afectado);

	/**
	* Caches the afectados in the entity cache if it is enabled.
	*
	* @param afectados the afectados
	*/
	public void cacheResult(java.util.List<Afectado> afectados);

	/**
	* Creates a new afectado with the primary key. Does not add the afectado to the database.
	*
	* @param id the primary key for the new afectado
	* @return the new afectado
	*/
	public Afectado create(long id);

	/**
	* Removes the afectado with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the afectado
	* @return the afectado that was removed
	* @throws NoSuchAfectadoException if a afectado with the primary key could not be found
	*/
	public Afectado remove(long id) throws NoSuchAfectadoException;

	public Afectado updateImpl(Afectado afectado);

	/**
	* Returns the afectado with the primary key or throws a {@link NoSuchAfectadoException} if it could not be found.
	*
	* @param id the primary key of the afectado
	* @return the afectado
	* @throws NoSuchAfectadoException if a afectado with the primary key could not be found
	*/
	public Afectado findByPrimaryKey(long id) throws NoSuchAfectadoException;

	/**
	* Returns the afectado with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the afectado
	* @return the afectado, or <code>null</code> if a afectado with the primary key could not be found
	*/
	public Afectado fetchByPrimaryKey(long id);

	@Override
	public java.util.Map<java.io.Serializable, Afectado> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the afectados.
	*
	* @return the afectados
	*/
	public java.util.List<Afectado> findAll();

	/**
	* Returns a range of all the afectados.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @return the range of afectados
	*/
	public java.util.List<Afectado> findAll(int start, int end);

	/**
	* Returns an ordered range of all the afectados.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of afectados
	*/
	public java.util.List<Afectado> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Afectado> orderByComparator);

	/**
	* Returns an ordered range of all the afectados.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of afectados
	*/
	public java.util.List<Afectado> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Afectado> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the afectados from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of afectados.
	*
	* @return the number of afectados
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}