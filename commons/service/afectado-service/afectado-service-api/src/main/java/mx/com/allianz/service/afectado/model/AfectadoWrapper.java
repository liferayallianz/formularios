/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.afectado.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Afectado}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Afectado
 * @generated
 */
@ProviderType
public class AfectadoWrapper implements Afectado, ModelWrapper<Afectado> {
	public AfectadoWrapper(Afectado afectado) {
		_afectado = afectado;
	}

	@Override
	public Class<?> getModelClass() {
		return Afectado.class;
	}

	@Override
	public String getModelClassName() {
		return Afectado.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("nombre", getNombre());
		attributes.put("apellidoPaterno", getApellidoPaterno());
		attributes.put("apellidoMaterno", getApellidoMaterno());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String apellidoPaterno = (String)attributes.get("apellidoPaterno");

		if (apellidoPaterno != null) {
			setApellidoPaterno(apellidoPaterno);
		}

		String apellidoMaterno = (String)attributes.get("apellidoMaterno");

		if (apellidoMaterno != null) {
			setApellidoMaterno(apellidoMaterno);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _afectado.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _afectado.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _afectado.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _afectado.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Afectado> toCacheModel() {
		return _afectado.toCacheModel();
	}

	@Override
	public int compareTo(Afectado afectado) {
		return _afectado.compareTo(afectado);
	}

	@Override
	public int hashCode() {
		return _afectado.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _afectado.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new AfectadoWrapper((Afectado)_afectado.clone());
	}

	/**
	* Returns the apellido materno of this afectado.
	*
	* @return the apellido materno of this afectado
	*/
	@Override
	public java.lang.String getApellidoMaterno() {
		return _afectado.getApellidoMaterno();
	}

	/**
	* Returns the apellido paterno of this afectado.
	*
	* @return the apellido paterno of this afectado
	*/
	@Override
	public java.lang.String getApellidoPaterno() {
		return _afectado.getApellidoPaterno();
	}

	/**
	* Returns the nombre of this afectado.
	*
	* @return the nombre of this afectado
	*/
	@Override
	public java.lang.String getNombre() {
		return _afectado.getNombre();
	}

	@Override
	public java.lang.String toString() {
		return _afectado.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _afectado.toXmlString();
	}

	/**
	* Returns the ID of this afectado.
	*
	* @return the ID of this afectado
	*/
	@Override
	public long getId() {
		return _afectado.getId();
	}

	/**
	* Returns the primary key of this afectado.
	*
	* @return the primary key of this afectado
	*/
	@Override
	public long getPrimaryKey() {
		return _afectado.getPrimaryKey();
	}

	@Override
	public Afectado toEscapedModel() {
		return new AfectadoWrapper(_afectado.toEscapedModel());
	}

	@Override
	public Afectado toUnescapedModel() {
		return new AfectadoWrapper(_afectado.toUnescapedModel());
	}

	@Override
	public void persist() {
		_afectado.persist();
	}

	/**
	* Sets the apellido materno of this afectado.
	*
	* @param apellidoMaterno the apellido materno of this afectado
	*/
	@Override
	public void setApellidoMaterno(java.lang.String apellidoMaterno) {
		_afectado.setApellidoMaterno(apellidoMaterno);
	}

	/**
	* Sets the apellido paterno of this afectado.
	*
	* @param apellidoPaterno the apellido paterno of this afectado
	*/
	@Override
	public void setApellidoPaterno(java.lang.String apellidoPaterno) {
		_afectado.setApellidoPaterno(apellidoPaterno);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_afectado.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_afectado.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_afectado.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_afectado.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the ID of this afectado.
	*
	* @param id the ID of this afectado
	*/
	@Override
	public void setId(long id) {
		_afectado.setId(id);
	}

	@Override
	public void setNew(boolean n) {
		_afectado.setNew(n);
	}

	/**
	* Sets the nombre of this afectado.
	*
	* @param nombre the nombre of this afectado
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_afectado.setNombre(nombre);
	}

	/**
	* Sets the primary key of this afectado.
	*
	* @param primaryKey the primary key of this afectado
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_afectado.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_afectado.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AfectadoWrapper)) {
			return false;
		}

		AfectadoWrapper afectadoWrapper = (AfectadoWrapper)obj;

		if (Objects.equals(_afectado, afectadoWrapper._afectado)) {
			return true;
		}

		return false;
	}

	@Override
	public Afectado getWrappedModel() {
		return _afectado;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _afectado.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _afectado.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_afectado.resetOriginalValues();
	}

	private final Afectado _afectado;
}