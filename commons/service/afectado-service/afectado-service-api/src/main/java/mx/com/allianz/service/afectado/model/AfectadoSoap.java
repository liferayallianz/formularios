/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.afectado.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class AfectadoSoap implements Serializable {
	public static AfectadoSoap toSoapModel(Afectado model) {
		AfectadoSoap soapModel = new AfectadoSoap();

		soapModel.setId(model.getId());
		soapModel.setNombre(model.getNombre());
		soapModel.setApellidoPaterno(model.getApellidoPaterno());
		soapModel.setApellidoMaterno(model.getApellidoMaterno());

		return soapModel;
	}

	public static AfectadoSoap[] toSoapModels(Afectado[] models) {
		AfectadoSoap[] soapModels = new AfectadoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AfectadoSoap[][] toSoapModels(Afectado[][] models) {
		AfectadoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AfectadoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AfectadoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AfectadoSoap[] toSoapModels(List<Afectado> models) {
		List<AfectadoSoap> soapModels = new ArrayList<AfectadoSoap>(models.size());

		for (Afectado model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AfectadoSoap[soapModels.size()]);
	}

	public AfectadoSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getApellidoPaterno() {
		return _apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		_apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return _apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		_apellidoMaterno = apellidoMaterno;
	}

	private long _id;
	private String _nombre;
	private String _apellidoPaterno;
	private String _apellidoMaterno;
}