/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.afectado.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AfectadoLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AfectadoLocalService
 * @generated
 */
@ProviderType
public class AfectadoLocalServiceWrapper implements AfectadoLocalService,
	ServiceWrapper<AfectadoLocalService> {
	public AfectadoLocalServiceWrapper(
		AfectadoLocalService afectadoLocalService) {
		_afectadoLocalService = afectadoLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _afectadoLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _afectadoLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _afectadoLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _afectadoLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _afectadoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of afectados.
	*
	* @return the number of afectados
	*/
	@Override
	public int getAfectadosCount() {
		return _afectadoLocalService.getAfectadosCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _afectadoLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _afectadoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.afectado.model.impl.AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _afectadoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.afectado.model.impl.AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _afectadoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the afectados.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.afectado.model.impl.AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @return the range of afectados
	*/
	@Override
	public java.util.List<mx.com.allianz.service.afectado.model.Afectado> getAfectados(
		int start, int end) {
		return _afectadoLocalService.getAfectados(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _afectadoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _afectadoLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the afectado to the database. Also notifies the appropriate model listeners.
	*
	* @param afectado the afectado
	* @return the afectado that was added
	*/
	@Override
	public mx.com.allianz.service.afectado.model.Afectado addAfectado(
		mx.com.allianz.service.afectado.model.Afectado afectado) {
		return _afectadoLocalService.addAfectado(afectado);
	}

	/**
	* Creates a new afectado with the primary key. Does not add the afectado to the database.
	*
	* @param id the primary key for the new afectado
	* @return the new afectado
	*/
	@Override
	public mx.com.allianz.service.afectado.model.Afectado createAfectado(
		long id) {
		return _afectadoLocalService.createAfectado(id);
	}

	/**
	* Deletes the afectado with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the afectado
	* @return the afectado that was removed
	* @throws PortalException if a afectado with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.afectado.model.Afectado deleteAfectado(
		long id) throws com.liferay.portal.kernel.exception.PortalException {
		return _afectadoLocalService.deleteAfectado(id);
	}

	/**
	* Deletes the afectado from the database. Also notifies the appropriate model listeners.
	*
	* @param afectado the afectado
	* @return the afectado that was removed
	*/
	@Override
	public mx.com.allianz.service.afectado.model.Afectado deleteAfectado(
		mx.com.allianz.service.afectado.model.Afectado afectado) {
		return _afectadoLocalService.deleteAfectado(afectado);
	}

	@Override
	public mx.com.allianz.service.afectado.model.Afectado fetchAfectado(long id) {
		return _afectadoLocalService.fetchAfectado(id);
	}

	/**
	* Returns the afectado with the primary key.
	*
	* @param id the primary key of the afectado
	* @return the afectado
	* @throws PortalException if a afectado with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.afectado.model.Afectado getAfectado(long id)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _afectadoLocalService.getAfectado(id);
	}

	/**
	* Updates the afectado in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param afectado the afectado
	* @return the afectado that was updated
	*/
	@Override
	public mx.com.allianz.service.afectado.model.Afectado updateAfectado(
		mx.com.allianz.service.afectado.model.Afectado afectado) {
		return _afectadoLocalService.updateAfectado(afectado);
	}

	@Override
	public AfectadoLocalService getWrappedService() {
		return _afectadoLocalService;
	}

	@Override
	public void setWrappedService(AfectadoLocalService afectadoLocalService) {
		_afectadoLocalService = afectadoLocalService;
	}

	private AfectadoLocalService _afectadoLocalService;
}