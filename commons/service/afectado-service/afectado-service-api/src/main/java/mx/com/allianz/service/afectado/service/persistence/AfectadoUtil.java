/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.afectado.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.afectado.model.Afectado;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the afectado service. This utility wraps {@link mx.com.allianz.service.afectado.service.persistence.impl.AfectadoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AfectadoPersistence
 * @see mx.com.allianz.service.afectado.service.persistence.impl.AfectadoPersistenceImpl
 * @generated
 */
@ProviderType
public class AfectadoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Afectado afectado) {
		getPersistence().clearCache(afectado);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Afectado> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Afectado> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Afectado> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Afectado> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Afectado update(Afectado afectado) {
		return getPersistence().update(afectado);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Afectado update(Afectado afectado,
		ServiceContext serviceContext) {
		return getPersistence().update(afectado, serviceContext);
	}

	/**
	* Returns all the afectados where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching afectados
	*/
	public static List<Afectado> findBynombre(java.lang.String nombre) {
		return getPersistence().findBynombre(nombre);
	}

	/**
	* Returns a range of all the afectados where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @return the range of matching afectados
	*/
	public static List<Afectado> findBynombre(java.lang.String nombre,
		int start, int end) {
		return getPersistence().findBynombre(nombre, start, end);
	}

	/**
	* Returns an ordered range of all the afectados where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching afectados
	*/
	public static List<Afectado> findBynombre(java.lang.String nombre,
		int start, int end, OrderByComparator<Afectado> orderByComparator) {
		return getPersistence()
				   .findBynombre(nombre, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the afectados where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching afectados
	*/
	public static List<Afectado> findBynombre(java.lang.String nombre,
		int start, int end, OrderByComparator<Afectado> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBynombre(nombre, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first afectado in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching afectado
	* @throws NoSuchAfectadoException if a matching afectado could not be found
	*/
	public static Afectado findBynombre_First(java.lang.String nombre,
		OrderByComparator<Afectado> orderByComparator)
		throws mx.com.allianz.service.afectado.exception.NoSuchAfectadoException {
		return getPersistence().findBynombre_First(nombre, orderByComparator);
	}

	/**
	* Returns the first afectado in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching afectado, or <code>null</code> if a matching afectado could not be found
	*/
	public static Afectado fetchBynombre_First(java.lang.String nombre,
		OrderByComparator<Afectado> orderByComparator) {
		return getPersistence().fetchBynombre_First(nombre, orderByComparator);
	}

	/**
	* Returns the last afectado in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching afectado
	* @throws NoSuchAfectadoException if a matching afectado could not be found
	*/
	public static Afectado findBynombre_Last(java.lang.String nombre,
		OrderByComparator<Afectado> orderByComparator)
		throws mx.com.allianz.service.afectado.exception.NoSuchAfectadoException {
		return getPersistence().findBynombre_Last(nombre, orderByComparator);
	}

	/**
	* Returns the last afectado in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching afectado, or <code>null</code> if a matching afectado could not be found
	*/
	public static Afectado fetchBynombre_Last(java.lang.String nombre,
		OrderByComparator<Afectado> orderByComparator) {
		return getPersistence().fetchBynombre_Last(nombre, orderByComparator);
	}

	/**
	* Returns the afectados before and after the current afectado in the ordered set where nombre = &#63;.
	*
	* @param id the primary key of the current afectado
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next afectado
	* @throws NoSuchAfectadoException if a afectado with the primary key could not be found
	*/
	public static Afectado[] findBynombre_PrevAndNext(long id,
		java.lang.String nombre, OrderByComparator<Afectado> orderByComparator)
		throws mx.com.allianz.service.afectado.exception.NoSuchAfectadoException {
		return getPersistence()
				   .findBynombre_PrevAndNext(id, nombre, orderByComparator);
	}

	/**
	* Removes all the afectados where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public static void removeBynombre(java.lang.String nombre) {
		getPersistence().removeBynombre(nombre);
	}

	/**
	* Returns the number of afectados where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching afectados
	*/
	public static int countBynombre(java.lang.String nombre) {
		return getPersistence().countBynombre(nombre);
	}

	/**
	* Caches the afectado in the entity cache if it is enabled.
	*
	* @param afectado the afectado
	*/
	public static void cacheResult(Afectado afectado) {
		getPersistence().cacheResult(afectado);
	}

	/**
	* Caches the afectados in the entity cache if it is enabled.
	*
	* @param afectados the afectados
	*/
	public static void cacheResult(List<Afectado> afectados) {
		getPersistence().cacheResult(afectados);
	}

	/**
	* Creates a new afectado with the primary key. Does not add the afectado to the database.
	*
	* @param id the primary key for the new afectado
	* @return the new afectado
	*/
	public static Afectado create(long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the afectado with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the afectado
	* @return the afectado that was removed
	* @throws NoSuchAfectadoException if a afectado with the primary key could not be found
	*/
	public static Afectado remove(long id)
		throws mx.com.allianz.service.afectado.exception.NoSuchAfectadoException {
		return getPersistence().remove(id);
	}

	public static Afectado updateImpl(Afectado afectado) {
		return getPersistence().updateImpl(afectado);
	}

	/**
	* Returns the afectado with the primary key or throws a {@link NoSuchAfectadoException} if it could not be found.
	*
	* @param id the primary key of the afectado
	* @return the afectado
	* @throws NoSuchAfectadoException if a afectado with the primary key could not be found
	*/
	public static Afectado findByPrimaryKey(long id)
		throws mx.com.allianz.service.afectado.exception.NoSuchAfectadoException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the afectado with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the afectado
	* @return the afectado, or <code>null</code> if a afectado with the primary key could not be found
	*/
	public static Afectado fetchByPrimaryKey(long id) {
		return getPersistence().fetchByPrimaryKey(id);
	}

	public static java.util.Map<java.io.Serializable, Afectado> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the afectados.
	*
	* @return the afectados
	*/
	public static List<Afectado> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the afectados.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @return the range of afectados
	*/
	public static List<Afectado> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the afectados.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of afectados
	*/
	public static List<Afectado> findAll(int start, int end,
		OrderByComparator<Afectado> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the afectados.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AfectadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of afectados
	* @param end the upper bound of the range of afectados (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of afectados
	*/
	public static List<Afectado> findAll(int start, int end,
		OrderByComparator<Afectado> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the afectados from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of afectados.
	*
	* @return the number of afectados
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static AfectadoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AfectadoPersistence, AfectadoPersistence> _serviceTracker =
		ServiceTrackerFactory.open(AfectadoPersistence.class);
}