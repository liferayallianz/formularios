package mx.com.allianz.service.afectado.rest;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.AfectadoDTO;
import mx.com.allianz.service.afectado.service.AfectadoLocalServiceUtil;

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.afectado")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)

public class AfectadoServiceRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todas las nacionalidades del
	 * catalogo.
	 * 
	 * @return getNacionalidades()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getAfectados")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List <AfectadoDTO> getAfectado(){
		return getAllAfectado();
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los hospitales del
	 * catalogo.
	 * 
	 * @return getAllCAT_Hospital().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findAfectados")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	
	public List<AfectadoDTO> findAfectadoBy(
			
			@DefaultValue("-1") @QueryParam("id") long id,
			@DefaultValue("") @QueryParam("nombre") String nombre,
			@DefaultValue("") @QueryParam("apellidoPaterno") String apellidoPaterno,
			@DefaultValue("") @QueryParam("apellidoMaterno") String apellidoMaterno){
		
	return getAllAfectado().stream()
			.filter((afectado) -> id == -1 || afectado.getId() == id)                             
			.filter((afectado) -> nombre.isEmpty() || afectado.getNombre().contains(nombre))
			.filter((afectado) -> apellidoPaterno.isEmpty() || afectado.getApellidoPaterno().contains(apellidoPaterno))
			.filter((afectado) -> apellidoMaterno.isEmpty() || afectado.getApellidoMaterno().contains(apellidoMaterno))
			.collect(Collectors.toList());
	}

	private List<AfectadoDTO> getAllAfectado() {
		return AfectadoLocalServiceUtil.getAfectados(-1, -1).stream()
				.map(afectado -> new AfectadoDTO(afectado.getId(),
						afectado.getNombre(),afectado.getApellidoPaterno(),
						afectado.getApellidoMaterno())).collect(Collectors.toList());
	}
	
}