/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.producto.acx.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.producto.acx.model.ProductoACX;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ProductoACX in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see ProductoACX
 * @generated
 */
@ProviderType
public class ProductoACXCacheModel implements CacheModel<ProductoACX>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProductoACXCacheModel)) {
			return false;
		}

		ProductoACXCacheModel productoACXCacheModel = (ProductoACXCacheModel)obj;

		if (codigoProducto.equals(productoACXCacheModel.codigoProducto)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, codigoProducto);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{codigoProducto=");
		sb.append(codigoProducto);
		sb.append(", descripcionProducto=");
		sb.append(descripcionProducto);
		sb.append(", tipoProducto=");
		sb.append(tipoProducto);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProductoACX toEntityModel() {
		ProductoACXImpl productoACXImpl = new ProductoACXImpl();

		if (codigoProducto == null) {
			productoACXImpl.setCodigoProducto(StringPool.BLANK);
		}
		else {
			productoACXImpl.setCodigoProducto(codigoProducto);
		}

		if (descripcionProducto == null) {
			productoACXImpl.setDescripcionProducto(StringPool.BLANK);
		}
		else {
			productoACXImpl.setDescripcionProducto(descripcionProducto);
		}

		if (tipoProducto == null) {
			productoACXImpl.setTipoProducto(StringPool.BLANK);
		}
		else {
			productoACXImpl.setTipoProducto(tipoProducto);
		}

		productoACXImpl.resetOriginalValues();

		return productoACXImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codigoProducto = objectInput.readUTF();
		descripcionProducto = objectInput.readUTF();
		tipoProducto = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (codigoProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoProducto);
		}

		if (descripcionProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcionProducto);
		}

		if (tipoProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoProducto);
		}
	}

	public String codigoProducto;
	public String descripcionProducto;
	public String tipoProducto;
}