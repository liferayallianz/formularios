package mx.com.allianz.service.rest.producto.acx;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.ProductoDTO;
import mx.com.allianz.service.producto.acx.service.ProductoACXLocalServiceUtil;


/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio
 * Paiss que permite visualizar una lista de datos, 
 * filtra el orden de la b\u00fasqueda y muestra el ordenamiento 
 * conforme al filtro
 *
 * @author  TestingZone
 * @version 1.0
 * @since   2016-12-5 
 */
/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n (Modulo)
 */
@ApplicationPath("/catalogos.producto.acx")
/*
 * @Component sirve para garantizar la seguridad del path
 */
@Component(
	immediate = true,
	property = {"jaxrs.application=true"},
	service = Application.class
)
public class ProductoACXRest extends Application {
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this).
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object)this);
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todas los Productos del cat\u00e1logo.
	 * 
	 * @return getAllProductos().
	 */
	//@GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	//@Path sirve para poner la ruta relativa donde la clase ser\u00e1 hospedada.
	@Path("/getProductos")
	//@Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({MediaType.APPLICATION_JSON})
	public List<ProductoDTO> getPaises() {
		
		// Se regresan todos los Paiss.
		return getAllProductos();
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los Paiss del cat\u00e1logo
	 * 
	 * @return getAllPaiss().stream()
	 */
	//@GET es el nombre del procesamiento de la petici\u00f3n
	@GET
	//@Path sirve para poner la ruta relativa donde la clase ser\u00e1
	@Path("/findProductos")
	//@Produces sirve para especificar el tipo de formato de respuesta
	@Produces({MediaType.APPLICATION_JSON})
	public List<ProductoDTO> findProductosBy(
			//El valor @DefaultValue se utiliza para definir el valor por defecto para el par\u00e1metro matrixParam.
			//@QueryParam se utiliza para extraer los par\u00e1metros de consulta del "componente de consulta" de la URL de la solicitud.
			@DefaultValue("")@QueryParam("codigo_producto") String codigoProducto,
			@DefaultValue("")@QueryParam("descripcion_producto") String descripcionProducto,
			@DefaultValue("")@QueryParam("tipo_producto") String tipoProducto){
		//stream() Devuelve un flujo secuencial considerando la colecci\u00f3n como su origen.
		//convierte la lista en stream
		return getAllProductos().stream()
				// filtrado pora c\u00f3digo del producto, si es vac\u00edo se ignora el filtro 
				.filter((producto) -> codigoProducto.isEmpty() || producto.getCodigoProducto().contains(codigoProducto))
				// filtrado pora descripci\u00f3n de producto, si es vac\u00edo se ignora el filtro 
				.filter((producto) -> descripcionProducto.isEmpty() || producto.getDescripcionProducto().toUpperCase().contains(descripcionProducto.toUpperCase()))
				// filtrado pora tipo producto, si es vac\u00edo se ignora el filtro 
				.filter((producto) -> tipoProducto.isEmpty() || producto.getTipoProducto().contains(tipoProducto))
				//salida de colecci\u00f3n y el stream se convierte en lista
				.collect(Collectors.toList());
	}

	/**
	 *M\u00e9todo getAllProductos del tipo list generada para mostrar un arreglo de la lista
	 * @return listaProductos
	 */
	private List<ProductoDTO> getAllProductos(){
		return ProductoACXLocalServiceUtil.getProductoACXs(-1,-1).stream().map((producto) -> 
		new ProductoDTO(producto.getCodigoProducto(), producto.getDescripcionProducto(), 
				producto.getTipoProducto())).collect(Collectors.toList());
	}
}