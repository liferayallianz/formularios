/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.producto.acx.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the ProductoACX service. Represents a row in the &quot;PLT_CAT_PRODUCTO_ACX&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ProductoACXModel
 * @see mx.com.allianz.service.producto.acx.model.impl.ProductoACXImpl
 * @see mx.com.allianz.service.producto.acx.model.impl.ProductoACXModelImpl
 * @generated
 */
@ImplementationClassName("mx.com.allianz.service.producto.acx.model.impl.ProductoACXImpl")
@ProviderType
public interface ProductoACX extends ProductoACXModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link mx.com.allianz.service.producto.acx.model.impl.ProductoACXImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<ProductoACX, String> CODIGO_PRODUCTO_ACCESSOR = new Accessor<ProductoACX, String>() {
			@Override
			public String get(ProductoACX productoACX) {
				return productoACX.getCodigoProducto();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<ProductoACX> getTypeClass() {
				return ProductoACX.class;
			}
		};
}