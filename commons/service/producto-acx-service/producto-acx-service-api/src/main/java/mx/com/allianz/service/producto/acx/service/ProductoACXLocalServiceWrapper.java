/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.producto.acx.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProductoACXLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ProductoACXLocalService
 * @generated
 */
@ProviderType
public class ProductoACXLocalServiceWrapper implements ProductoACXLocalService,
	ServiceWrapper<ProductoACXLocalService> {
	public ProductoACXLocalServiceWrapper(
		ProductoACXLocalService productoACXLocalService) {
		_productoACXLocalService = productoACXLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _productoACXLocalService.dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _productoACXLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _productoACXLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of producto a c xs.
	*
	* @return the number of producto a c xs
	*/
	@Override
	public int getProductoACXsCount() {
		return _productoACXLocalService.getProductoACXsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _productoACXLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _productoACXLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.producto.acx.model.impl.ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _productoACXLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.producto.acx.model.impl.ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _productoACXLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the producto a c xs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.producto.acx.model.impl.ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @return the range of producto a c xs
	*/
	@Override
	public java.util.List<mx.com.allianz.service.producto.acx.model.ProductoACX> getProductoACXs(
		int start, int end) {
		return _productoACXLocalService.getProductoACXs(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _productoACXLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _productoACXLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	/**
	* Adds the producto a c x to the database. Also notifies the appropriate model listeners.
	*
	* @param productoACX the producto a c x
	* @return the producto a c x that was added
	*/
	@Override
	public mx.com.allianz.service.producto.acx.model.ProductoACX addProductoACX(
		mx.com.allianz.service.producto.acx.model.ProductoACX productoACX) {
		return _productoACXLocalService.addProductoACX(productoACX);
	}

	/**
	* Creates a new producto a c x with the primary key. Does not add the producto a c x to the database.
	*
	* @param codigoProducto the primary key for the new producto a c x
	* @return the new producto a c x
	*/
	@Override
	public mx.com.allianz.service.producto.acx.model.ProductoACX createProductoACX(
		java.lang.String codigoProducto) {
		return _productoACXLocalService.createProductoACX(codigoProducto);
	}

	/**
	* Deletes the producto a c x with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codigoProducto the primary key of the producto a c x
	* @return the producto a c x that was removed
	* @throws PortalException if a producto a c x with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.producto.acx.model.ProductoACX deleteProductoACX(
		java.lang.String codigoProducto)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _productoACXLocalService.deleteProductoACX(codigoProducto);
	}

	/**
	* Deletes the producto a c x from the database. Also notifies the appropriate model listeners.
	*
	* @param productoACX the producto a c x
	* @return the producto a c x that was removed
	*/
	@Override
	public mx.com.allianz.service.producto.acx.model.ProductoACX deleteProductoACX(
		mx.com.allianz.service.producto.acx.model.ProductoACX productoACX) {
		return _productoACXLocalService.deleteProductoACX(productoACX);
	}

	@Override
	public mx.com.allianz.service.producto.acx.model.ProductoACX fetchProductoACX(
		java.lang.String codigoProducto) {
		return _productoACXLocalService.fetchProductoACX(codigoProducto);
	}

	/**
	* Returns the producto a c x with the primary key.
	*
	* @param codigoProducto the primary key of the producto a c x
	* @return the producto a c x
	* @throws PortalException if a producto a c x with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.producto.acx.model.ProductoACX getProductoACX(
		java.lang.String codigoProducto)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _productoACXLocalService.getProductoACX(codigoProducto);
	}

	/**
	* Updates the producto a c x in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param productoACX the producto a c x
	* @return the producto a c x that was updated
	*/
	@Override
	public mx.com.allianz.service.producto.acx.model.ProductoACX updateProductoACX(
		mx.com.allianz.service.producto.acx.model.ProductoACX productoACX) {
		return _productoACXLocalService.updateProductoACX(productoACX);
	}

	@Override
	public ProductoACXLocalService getWrappedService() {
		return _productoACXLocalService;
	}

	@Override
	public void setWrappedService(
		ProductoACXLocalService productoACXLocalService) {
		_productoACXLocalService = productoACXLocalService;
	}

	private ProductoACXLocalService _productoACXLocalService;
}