/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.producto.acx.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ProductoACX}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProductoACX
 * @generated
 */
@ProviderType
public class ProductoACXWrapper implements ProductoACX,
	ModelWrapper<ProductoACX> {
	public ProductoACXWrapper(ProductoACX productoACX) {
		_productoACX = productoACX;
	}

	@Override
	public Class<?> getModelClass() {
		return ProductoACX.class;
	}

	@Override
	public String getModelClassName() {
		return ProductoACX.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codigoProducto", getCodigoProducto());
		attributes.put("descripcionProducto", getDescripcionProducto());
		attributes.put("tipoProducto", getTipoProducto());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codigoProducto = (String)attributes.get("codigoProducto");

		if (codigoProducto != null) {
			setCodigoProducto(codigoProducto);
		}

		String descripcionProducto = (String)attributes.get(
				"descripcionProducto");

		if (descripcionProducto != null) {
			setDescripcionProducto(descripcionProducto);
		}

		String tipoProducto = (String)attributes.get("tipoProducto");

		if (tipoProducto != null) {
			setTipoProducto(tipoProducto);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _productoACX.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _productoACX.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _productoACX.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _productoACX.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ProductoACX> toCacheModel() {
		return _productoACX.toCacheModel();
	}

	@Override
	public int compareTo(ProductoACX productoACX) {
		return _productoACX.compareTo(productoACX);
	}

	@Override
	public int hashCode() {
		return _productoACX.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _productoACX.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ProductoACXWrapper((ProductoACX)_productoACX.clone());
	}

	/**
	* Returns the codigo producto of this producto a c x.
	*
	* @return the codigo producto of this producto a c x
	*/
	@Override
	public java.lang.String getCodigoProducto() {
		return _productoACX.getCodigoProducto();
	}

	/**
	* Returns the descripcion producto of this producto a c x.
	*
	* @return the descripcion producto of this producto a c x
	*/
	@Override
	public java.lang.String getDescripcionProducto() {
		return _productoACX.getDescripcionProducto();
	}

	/**
	* Returns the primary key of this producto a c x.
	*
	* @return the primary key of this producto a c x
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _productoACX.getPrimaryKey();
	}

	/**
	* Returns the tipo producto of this producto a c x.
	*
	* @return the tipo producto of this producto a c x
	*/
	@Override
	public java.lang.String getTipoProducto() {
		return _productoACX.getTipoProducto();
	}

	@Override
	public java.lang.String toString() {
		return _productoACX.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _productoACX.toXmlString();
	}

	@Override
	public ProductoACX toEscapedModel() {
		return new ProductoACXWrapper(_productoACX.toEscapedModel());
	}

	@Override
	public ProductoACX toUnescapedModel() {
		return new ProductoACXWrapper(_productoACX.toUnescapedModel());
	}

	@Override
	public void persist() {
		_productoACX.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_productoACX.setCachedModel(cachedModel);
	}

	/**
	* Sets the codigo producto of this producto a c x.
	*
	* @param codigoProducto the codigo producto of this producto a c x
	*/
	@Override
	public void setCodigoProducto(java.lang.String codigoProducto) {
		_productoACX.setCodigoProducto(codigoProducto);
	}

	/**
	* Sets the descripcion producto of this producto a c x.
	*
	* @param descripcionProducto the descripcion producto of this producto a c x
	*/
	@Override
	public void setDescripcionProducto(java.lang.String descripcionProducto) {
		_productoACX.setDescripcionProducto(descripcionProducto);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_productoACX.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_productoACX.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_productoACX.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_productoACX.setNew(n);
	}

	/**
	* Sets the primary key of this producto a c x.
	*
	* @param primaryKey the primary key of this producto a c x
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_productoACX.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_productoACX.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the tipo producto of this producto a c x.
	*
	* @param tipoProducto the tipo producto of this producto a c x
	*/
	@Override
	public void setTipoProducto(java.lang.String tipoProducto) {
		_productoACX.setTipoProducto(tipoProducto);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProductoACXWrapper)) {
			return false;
		}

		ProductoACXWrapper productoACXWrapper = (ProductoACXWrapper)obj;

		if (Objects.equals(_productoACX, productoACXWrapper._productoACX)) {
			return true;
		}

		return false;
	}

	@Override
	public ProductoACX getWrappedModel() {
		return _productoACX;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _productoACX.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _productoACX.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_productoACX.resetOriginalValues();
	}

	private final ProductoACX _productoACX;
}