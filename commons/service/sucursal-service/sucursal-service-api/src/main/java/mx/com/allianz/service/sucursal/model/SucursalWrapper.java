/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.sucursal.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Sucursal}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Sucursal
 * @generated
 */
@ProviderType
public class SucursalWrapper implements Sucursal, ModelWrapper<Sucursal> {
	public SucursalWrapper(Sucursal sucursal) {
		_sucursal = sucursal;
	}

	@Override
	public Class<?> getModelClass() {
		return Sucursal.class;
	}

	@Override
	public String getModelClassName() {
		return Sucursal.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idSucursal", getIdSucursal());
		attributes.put("descripcion", getDescripcion());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idSucursal = (Integer)attributes.get("idSucursal");

		if (idSucursal != null) {
			setIdSucursal(idSucursal);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _sucursal.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _sucursal.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _sucursal.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _sucursal.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Sucursal> toCacheModel() {
		return _sucursal.toCacheModel();
	}

	@Override
	public int compareTo(Sucursal sucursal) {
		return _sucursal.compareTo(sucursal);
	}

	/**
	* Returns the id sucursal of this sucursal.
	*
	* @return the id sucursal of this sucursal
	*/
	@Override
	public int getIdSucursal() {
		return _sucursal.getIdSucursal();
	}

	/**
	* Returns the primary key of this sucursal.
	*
	* @return the primary key of this sucursal
	*/
	@Override
	public int getPrimaryKey() {
		return _sucursal.getPrimaryKey();
	}

	@Override
	public int hashCode() {
		return _sucursal.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _sucursal.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SucursalWrapper((Sucursal)_sucursal.clone());
	}

	/**
	* Returns the descripcion of this sucursal.
	*
	* @return the descripcion of this sucursal
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _sucursal.getDescripcion();
	}

	@Override
	public java.lang.String toString() {
		return _sucursal.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _sucursal.toXmlString();
	}

	@Override
	public Sucursal toEscapedModel() {
		return new SucursalWrapper(_sucursal.toEscapedModel());
	}

	@Override
	public Sucursal toUnescapedModel() {
		return new SucursalWrapper(_sucursal.toUnescapedModel());
	}

	@Override
	public void persist() {
		_sucursal.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_sucursal.setCachedModel(cachedModel);
	}

	/**
	* Sets the descripcion of this sucursal.
	*
	* @param descripcion the descripcion of this sucursal
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_sucursal.setDescripcion(descripcion);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_sucursal.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_sucursal.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_sucursal.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the id sucursal of this sucursal.
	*
	* @param idSucursal the id sucursal of this sucursal
	*/
	@Override
	public void setIdSucursal(int idSucursal) {
		_sucursal.setIdSucursal(idSucursal);
	}

	@Override
	public void setNew(boolean n) {
		_sucursal.setNew(n);
	}

	/**
	* Sets the primary key of this sucursal.
	*
	* @param primaryKey the primary key of this sucursal
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_sucursal.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_sucursal.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SucursalWrapper)) {
			return false;
		}

		SucursalWrapper sucursalWrapper = (SucursalWrapper)obj;

		if (Objects.equals(_sucursal, sucursalWrapper._sucursal)) {
			return true;
		}

		return false;
	}

	@Override
	public Sucursal getWrappedModel() {
		return _sucursal;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _sucursal.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _sucursal.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_sucursal.resetOriginalValues();
	}

	private final Sucursal _sucursal;
}