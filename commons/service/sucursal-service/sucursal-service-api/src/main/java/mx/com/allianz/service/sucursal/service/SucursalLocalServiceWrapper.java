/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.sucursal.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SucursalLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SucursalLocalService
 * @generated
 */
@ProviderType
public class SucursalLocalServiceWrapper implements SucursalLocalService,
	ServiceWrapper<SucursalLocalService> {
	public SucursalLocalServiceWrapper(
		SucursalLocalService sucursalLocalService) {
		_sucursalLocalService = sucursalLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _sucursalLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _sucursalLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _sucursalLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sucursalLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sucursalLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of sucursals.
	*
	* @return the number of sucursals
	*/
	@Override
	public int getSucursalsCount() {
		return _sucursalLocalService.getSucursalsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _sucursalLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _sucursalLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.sucursal.model.impl.SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _sucursalLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.sucursal.model.impl.SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _sucursalLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the sucursals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.sucursal.model.impl.SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sucursals
	* @param end the upper bound of the range of sucursals (not inclusive)
	* @return the range of sucursals
	*/
	@Override
	public java.util.List<mx.com.allianz.service.sucursal.model.Sucursal> getSucursals(
		int start, int end) {
		return _sucursalLocalService.getSucursals(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _sucursalLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _sucursalLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the sucursal to the database. Also notifies the appropriate model listeners.
	*
	* @param sucursal the sucursal
	* @return the sucursal that was added
	*/
	@Override
	public mx.com.allianz.service.sucursal.model.Sucursal addSucursal(
		mx.com.allianz.service.sucursal.model.Sucursal sucursal) {
		return _sucursalLocalService.addSucursal(sucursal);
	}

	/**
	* Creates a new sucursal with the primary key. Does not add the sucursal to the database.
	*
	* @param idSucursal the primary key for the new sucursal
	* @return the new sucursal
	*/
	@Override
	public mx.com.allianz.service.sucursal.model.Sucursal createSucursal(
		int idSucursal) {
		return _sucursalLocalService.createSucursal(idSucursal);
	}

	/**
	* Deletes the sucursal with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idSucursal the primary key of the sucursal
	* @return the sucursal that was removed
	* @throws PortalException if a sucursal with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.sucursal.model.Sucursal deleteSucursal(
		int idSucursal)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sucursalLocalService.deleteSucursal(idSucursal);
	}

	/**
	* Deletes the sucursal from the database. Also notifies the appropriate model listeners.
	*
	* @param sucursal the sucursal
	* @return the sucursal that was removed
	*/
	@Override
	public mx.com.allianz.service.sucursal.model.Sucursal deleteSucursal(
		mx.com.allianz.service.sucursal.model.Sucursal sucursal) {
		return _sucursalLocalService.deleteSucursal(sucursal);
	}

	@Override
	public mx.com.allianz.service.sucursal.model.Sucursal fetchSucursal(
		int idSucursal) {
		return _sucursalLocalService.fetchSucursal(idSucursal);
	}

	/**
	* Returns the sucursal with the primary key.
	*
	* @param idSucursal the primary key of the sucursal
	* @return the sucursal
	* @throws PortalException if a sucursal with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.sucursal.model.Sucursal getSucursal(
		int idSucursal)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sucursalLocalService.getSucursal(idSucursal);
	}

	/**
	* Updates the sucursal in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param sucursal the sucursal
	* @return the sucursal that was updated
	*/
	@Override
	public mx.com.allianz.service.sucursal.model.Sucursal updateSucursal(
		mx.com.allianz.service.sucursal.model.Sucursal sucursal) {
		return _sucursalLocalService.updateSucursal(sucursal);
	}

	@Override
	public SucursalLocalService getWrappedService() {
		return _sucursalLocalService;
	}

	@Override
	public void setWrappedService(SucursalLocalService sucursalLocalService) {
		_sucursalLocalService = sucursalLocalService;
	}

	private SucursalLocalService _sucursalLocalService;
}