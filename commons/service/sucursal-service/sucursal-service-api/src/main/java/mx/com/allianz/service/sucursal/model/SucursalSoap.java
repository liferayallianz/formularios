/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.sucursal.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class SucursalSoap implements Serializable {
	public static SucursalSoap toSoapModel(Sucursal model) {
		SucursalSoap soapModel = new SucursalSoap();

		soapModel.setIdSucursal(model.getIdSucursal());
		soapModel.setDescripcion(model.getDescripcion());

		return soapModel;
	}

	public static SucursalSoap[] toSoapModels(Sucursal[] models) {
		SucursalSoap[] soapModels = new SucursalSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SucursalSoap[][] toSoapModels(Sucursal[][] models) {
		SucursalSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SucursalSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SucursalSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SucursalSoap[] toSoapModels(List<Sucursal> models) {
		List<SucursalSoap> soapModels = new ArrayList<SucursalSoap>(models.size());

		for (Sucursal model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SucursalSoap[soapModels.size()]);
	}

	public SucursalSoap() {
	}

	public int getPrimaryKey() {
		return _idSucursal;
	}

	public void setPrimaryKey(int pk) {
		setIdSucursal(pk);
	}

	public int getIdSucursal() {
		return _idSucursal;
	}

	public void setIdSucursal(int idSucursal) {
		_idSucursal = idSucursal;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	private int _idSucursal;
	private String _descripcion;
}