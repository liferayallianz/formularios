/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.sucursal.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.sucursal.exception.NoSuchSucursalException;
import mx.com.allianz.service.sucursal.model.Sucursal;

/**
 * The persistence interface for the sucursal service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.sucursal.service.persistence.impl.SucursalPersistenceImpl
 * @see SucursalUtil
 * @generated
 */
@ProviderType
public interface SucursalPersistence extends BasePersistence<Sucursal> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SucursalUtil} to access the sucursal persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the sucursals where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching sucursals
	*/
	public java.util.List<Sucursal> findByfindByDescripcion(
		java.lang.String descripcion);

	/**
	* Returns a range of all the sucursals where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of sucursals
	* @param end the upper bound of the range of sucursals (not inclusive)
	* @return the range of matching sucursals
	*/
	public java.util.List<Sucursal> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end);

	/**
	* Returns an ordered range of all the sucursals where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of sucursals
	* @param end the upper bound of the range of sucursals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sucursals
	*/
	public java.util.List<Sucursal> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Sucursal> orderByComparator);

	/**
	* Returns an ordered range of all the sucursals where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of sucursals
	* @param end the upper bound of the range of sucursals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sucursals
	*/
	public java.util.List<Sucursal> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Sucursal> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first sucursal in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sucursal
	* @throws NoSuchSucursalException if a matching sucursal could not be found
	*/
	public Sucursal findByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Sucursal> orderByComparator)
		throws NoSuchSucursalException;

	/**
	* Returns the first sucursal in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sucursal, or <code>null</code> if a matching sucursal could not be found
	*/
	public Sucursal fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Sucursal> orderByComparator);

	/**
	* Returns the last sucursal in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sucursal
	* @throws NoSuchSucursalException if a matching sucursal could not be found
	*/
	public Sucursal findByfindByDescripcion_Last(java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Sucursal> orderByComparator)
		throws NoSuchSucursalException;

	/**
	* Returns the last sucursal in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sucursal, or <code>null</code> if a matching sucursal could not be found
	*/
	public Sucursal fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Sucursal> orderByComparator);

	/**
	* Returns the sucursals before and after the current sucursal in the ordered set where descripcion = &#63;.
	*
	* @param idSucursal the primary key of the current sucursal
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sucursal
	* @throws NoSuchSucursalException if a sucursal with the primary key could not be found
	*/
	public Sucursal[] findByfindByDescripcion_PrevAndNext(int idSucursal,
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Sucursal> orderByComparator)
		throws NoSuchSucursalException;

	/**
	* Removes all the sucursals where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public void removeByfindByDescripcion(java.lang.String descripcion);

	/**
	* Returns the number of sucursals where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching sucursals
	*/
	public int countByfindByDescripcion(java.lang.String descripcion);

	/**
	* Caches the sucursal in the entity cache if it is enabled.
	*
	* @param sucursal the sucursal
	*/
	public void cacheResult(Sucursal sucursal);

	/**
	* Caches the sucursals in the entity cache if it is enabled.
	*
	* @param sucursals the sucursals
	*/
	public void cacheResult(java.util.List<Sucursal> sucursals);

	/**
	* Creates a new sucursal with the primary key. Does not add the sucursal to the database.
	*
	* @param idSucursal the primary key for the new sucursal
	* @return the new sucursal
	*/
	public Sucursal create(int idSucursal);

	/**
	* Removes the sucursal with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idSucursal the primary key of the sucursal
	* @return the sucursal that was removed
	* @throws NoSuchSucursalException if a sucursal with the primary key could not be found
	*/
	public Sucursal remove(int idSucursal) throws NoSuchSucursalException;

	public Sucursal updateImpl(Sucursal sucursal);

	/**
	* Returns the sucursal with the primary key or throws a {@link NoSuchSucursalException} if it could not be found.
	*
	* @param idSucursal the primary key of the sucursal
	* @return the sucursal
	* @throws NoSuchSucursalException if a sucursal with the primary key could not be found
	*/
	public Sucursal findByPrimaryKey(int idSucursal)
		throws NoSuchSucursalException;

	/**
	* Returns the sucursal with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idSucursal the primary key of the sucursal
	* @return the sucursal, or <code>null</code> if a sucursal with the primary key could not be found
	*/
	public Sucursal fetchByPrimaryKey(int idSucursal);

	@Override
	public java.util.Map<java.io.Serializable, Sucursal> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the sucursals.
	*
	* @return the sucursals
	*/
	public java.util.List<Sucursal> findAll();

	/**
	* Returns a range of all the sucursals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sucursals
	* @param end the upper bound of the range of sucursals (not inclusive)
	* @return the range of sucursals
	*/
	public java.util.List<Sucursal> findAll(int start, int end);

	/**
	* Returns an ordered range of all the sucursals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sucursals
	* @param end the upper bound of the range of sucursals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of sucursals
	*/
	public java.util.List<Sucursal> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Sucursal> orderByComparator);

	/**
	* Returns an ordered range of all the sucursals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sucursals
	* @param end the upper bound of the range of sucursals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of sucursals
	*/
	public java.util.List<Sucursal> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Sucursal> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the sucursals from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of sucursals.
	*
	* @return the number of sucursals
	*/
	public int countAll();
}