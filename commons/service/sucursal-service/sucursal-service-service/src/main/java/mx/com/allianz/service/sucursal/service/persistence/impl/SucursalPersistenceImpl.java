/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.sucursal.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.sucursal.exception.NoSuchSucursalException;
import mx.com.allianz.service.sucursal.model.Sucursal;
import mx.com.allianz.service.sucursal.model.impl.SucursalImpl;
import mx.com.allianz.service.sucursal.model.impl.SucursalModelImpl;
import mx.com.allianz.service.sucursal.service.persistence.SucursalPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the sucursal service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SucursalPersistence
 * @see mx.com.allianz.service.sucursal.service.persistence.SucursalUtil
 * @generated
 */
@ProviderType
public class SucursalPersistenceImpl extends BasePersistenceImpl<Sucursal>
	implements SucursalPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SucursalUtil} to access the sucursal persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SucursalImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SucursalModelImpl.ENTITY_CACHE_ENABLED,
			SucursalModelImpl.FINDER_CACHE_ENABLED, SucursalImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SucursalModelImpl.ENTITY_CACHE_ENABLED,
			SucursalModelImpl.FINDER_CACHE_ENABLED, SucursalImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SucursalModelImpl.ENTITY_CACHE_ENABLED,
			SucursalModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(SucursalModelImpl.ENTITY_CACHE_ENABLED,
			SucursalModelImpl.FINDER_CACHE_ENABLED, SucursalImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByfindByDescripcion",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(SucursalModelImpl.ENTITY_CACHE_ENABLED,
			SucursalModelImpl.FINDER_CACHE_ENABLED, SucursalImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByDescripcion", new String[] { String.class.getName() },
			SucursalModelImpl.DESCRIPCION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION = new FinderPath(SucursalModelImpl.ENTITY_CACHE_ENABLED,
			SucursalModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByDescripcion", new String[] { String.class.getName() });

	/**
	 * Returns all the sucursals where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the matching sucursals
	 */
	@Override
	public List<Sucursal> findByfindByDescripcion(String descripcion) {
		return findByfindByDescripcion(descripcion, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sucursals where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of sucursals
	 * @param end the upper bound of the range of sucursals (not inclusive)
	 * @return the range of matching sucursals
	 */
	@Override
	public List<Sucursal> findByfindByDescripcion(String descripcion,
		int start, int end) {
		return findByfindByDescripcion(descripcion, start, end, null);
	}

	/**
	 * Returns an ordered range of all the sucursals where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of sucursals
	 * @param end the upper bound of the range of sucursals (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching sucursals
	 */
	@Override
	public List<Sucursal> findByfindByDescripcion(String descripcion,
		int start, int end, OrderByComparator<Sucursal> orderByComparator) {
		return findByfindByDescripcion(descripcion, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the sucursals where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of sucursals
	 * @param end the upper bound of the range of sucursals (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching sucursals
	 */
	@Override
	public List<Sucursal> findByfindByDescripcion(String descripcion,
		int start, int end, OrderByComparator<Sucursal> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion, start, end, orderByComparator };
		}

		List<Sucursal> list = null;

		if (retrieveFromCache) {
			list = (List<Sucursal>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Sucursal sucursal : list) {
					if (!Objects.equals(descripcion, sucursal.getDescripcion())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SUCURSAL_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(SucursalModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				if (!pagination) {
					list = (List<Sucursal>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Sucursal>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first sucursal in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sucursal
	 * @throws NoSuchSucursalException if a matching sucursal could not be found
	 */
	@Override
	public Sucursal findByfindByDescripcion_First(String descripcion,
		OrderByComparator<Sucursal> orderByComparator)
		throws NoSuchSucursalException {
		Sucursal sucursal = fetchByfindByDescripcion_First(descripcion,
				orderByComparator);

		if (sucursal != null) {
			return sucursal;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSucursalException(msg.toString());
	}

	/**
	 * Returns the first sucursal in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sucursal, or <code>null</code> if a matching sucursal could not be found
	 */
	@Override
	public Sucursal fetchByfindByDescripcion_First(String descripcion,
		OrderByComparator<Sucursal> orderByComparator) {
		List<Sucursal> list = findByfindByDescripcion(descripcion, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last sucursal in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sucursal
	 * @throws NoSuchSucursalException if a matching sucursal could not be found
	 */
	@Override
	public Sucursal findByfindByDescripcion_Last(String descripcion,
		OrderByComparator<Sucursal> orderByComparator)
		throws NoSuchSucursalException {
		Sucursal sucursal = fetchByfindByDescripcion_Last(descripcion,
				orderByComparator);

		if (sucursal != null) {
			return sucursal;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSucursalException(msg.toString());
	}

	/**
	 * Returns the last sucursal in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sucursal, or <code>null</code> if a matching sucursal could not be found
	 */
	@Override
	public Sucursal fetchByfindByDescripcion_Last(String descripcion,
		OrderByComparator<Sucursal> orderByComparator) {
		int count = countByfindByDescripcion(descripcion);

		if (count == 0) {
			return null;
		}

		List<Sucursal> list = findByfindByDescripcion(descripcion, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the sucursals before and after the current sucursal in the ordered set where descripcion = &#63;.
	 *
	 * @param idSucursal the primary key of the current sucursal
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next sucursal
	 * @throws NoSuchSucursalException if a sucursal with the primary key could not be found
	 */
	@Override
	public Sucursal[] findByfindByDescripcion_PrevAndNext(int idSucursal,
		String descripcion, OrderByComparator<Sucursal> orderByComparator)
		throws NoSuchSucursalException {
		Sucursal sucursal = findByPrimaryKey(idSucursal);

		Session session = null;

		try {
			session = openSession();

			Sucursal[] array = new SucursalImpl[3];

			array[0] = getByfindByDescripcion_PrevAndNext(session, sucursal,
					descripcion, orderByComparator, true);

			array[1] = sucursal;

			array[2] = getByfindByDescripcion_PrevAndNext(session, sucursal,
					descripcion, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Sucursal getByfindByDescripcion_PrevAndNext(Session session,
		Sucursal sucursal, String descripcion,
		OrderByComparator<Sucursal> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SUCURSAL_WHERE);

		boolean bindDescripcion = false;

		if (descripcion == null) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
		}
		else if (descripcion.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
		}
		else {
			bindDescripcion = true;

			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(SucursalModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDescripcion) {
			qPos.add(descripcion);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(sucursal);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Sucursal> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the sucursals where descripcion = &#63; from the database.
	 *
	 * @param descripcion the descripcion
	 */
	@Override
	public void removeByfindByDescripcion(String descripcion) {
		for (Sucursal sucursal : findByfindByDescripcion(descripcion,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(sucursal);
		}
	}

	/**
	 * Returns the number of sucursals where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the number of matching sucursals
	 */
	@Override
	public int countByfindByDescripcion(String descripcion) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION;

		Object[] finderArgs = new Object[] { descripcion };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SUCURSAL_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1 = "sucursal.descripcion IS NULL";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2 = "sucursal.descripcion = ?";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3 = "(sucursal.descripcion IS NULL OR sucursal.descripcion = '')";

	public SucursalPersistenceImpl() {
		setModelClass(Sucursal.class);
	}

	/**
	 * Caches the sucursal in the entity cache if it is enabled.
	 *
	 * @param sucursal the sucursal
	 */
	@Override
	public void cacheResult(Sucursal sucursal) {
		entityCache.putResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
			SucursalImpl.class, sucursal.getPrimaryKey(), sucursal);

		sucursal.resetOriginalValues();
	}

	/**
	 * Caches the sucursals in the entity cache if it is enabled.
	 *
	 * @param sucursals the sucursals
	 */
	@Override
	public void cacheResult(List<Sucursal> sucursals) {
		for (Sucursal sucursal : sucursals) {
			if (entityCache.getResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
						SucursalImpl.class, sucursal.getPrimaryKey()) == null) {
				cacheResult(sucursal);
			}
			else {
				sucursal.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all sucursals.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(SucursalImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the sucursal.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Sucursal sucursal) {
		entityCache.removeResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
			SucursalImpl.class, sucursal.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Sucursal> sucursals) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Sucursal sucursal : sucursals) {
			entityCache.removeResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
				SucursalImpl.class, sucursal.getPrimaryKey());
		}
	}

	/**
	 * Creates a new sucursal with the primary key. Does not add the sucursal to the database.
	 *
	 * @param idSucursal the primary key for the new sucursal
	 * @return the new sucursal
	 */
	@Override
	public Sucursal create(int idSucursal) {
		Sucursal sucursal = new SucursalImpl();

		sucursal.setNew(true);
		sucursal.setPrimaryKey(idSucursal);

		return sucursal;
	}

	/**
	 * Removes the sucursal with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idSucursal the primary key of the sucursal
	 * @return the sucursal that was removed
	 * @throws NoSuchSucursalException if a sucursal with the primary key could not be found
	 */
	@Override
	public Sucursal remove(int idSucursal) throws NoSuchSucursalException {
		return remove((Serializable)idSucursal);
	}

	/**
	 * Removes the sucursal with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the sucursal
	 * @return the sucursal that was removed
	 * @throws NoSuchSucursalException if a sucursal with the primary key could not be found
	 */
	@Override
	public Sucursal remove(Serializable primaryKey)
		throws NoSuchSucursalException {
		Session session = null;

		try {
			session = openSession();

			Sucursal sucursal = (Sucursal)session.get(SucursalImpl.class,
					primaryKey);

			if (sucursal == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSucursalException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(sucursal);
		}
		catch (NoSuchSucursalException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Sucursal removeImpl(Sucursal sucursal) {
		sucursal = toUnwrappedModel(sucursal);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(sucursal)) {
				sucursal = (Sucursal)session.get(SucursalImpl.class,
						sucursal.getPrimaryKeyObj());
			}

			if (sucursal != null) {
				session.delete(sucursal);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (sucursal != null) {
			clearCache(sucursal);
		}

		return sucursal;
	}

	@Override
	public Sucursal updateImpl(Sucursal sucursal) {
		sucursal = toUnwrappedModel(sucursal);

		boolean isNew = sucursal.isNew();

		SucursalModelImpl sucursalModelImpl = (SucursalModelImpl)sucursal;

		Session session = null;

		try {
			session = openSession();

			if (sucursal.isNew()) {
				session.save(sucursal);

				sucursal.setNew(false);
			}
			else {
				sucursal = (Sucursal)session.merge(sucursal);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !SucursalModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((sucursalModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						sucursalModelImpl.getOriginalDescripcion()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);

				args = new Object[] { sucursalModelImpl.getDescripcion() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);
			}
		}

		entityCache.putResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
			SucursalImpl.class, sucursal.getPrimaryKey(), sucursal, false);

		sucursal.resetOriginalValues();

		return sucursal;
	}

	protected Sucursal toUnwrappedModel(Sucursal sucursal) {
		if (sucursal instanceof SucursalImpl) {
			return sucursal;
		}

		SucursalImpl sucursalImpl = new SucursalImpl();

		sucursalImpl.setNew(sucursal.isNew());
		sucursalImpl.setPrimaryKey(sucursal.getPrimaryKey());

		sucursalImpl.setIdSucursal(sucursal.getIdSucursal());
		sucursalImpl.setDescripcion(sucursal.getDescripcion());

		return sucursalImpl;
	}

	/**
	 * Returns the sucursal with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the sucursal
	 * @return the sucursal
	 * @throws NoSuchSucursalException if a sucursal with the primary key could not be found
	 */
	@Override
	public Sucursal findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSucursalException {
		Sucursal sucursal = fetchByPrimaryKey(primaryKey);

		if (sucursal == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSucursalException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return sucursal;
	}

	/**
	 * Returns the sucursal with the primary key or throws a {@link NoSuchSucursalException} if it could not be found.
	 *
	 * @param idSucursal the primary key of the sucursal
	 * @return the sucursal
	 * @throws NoSuchSucursalException if a sucursal with the primary key could not be found
	 */
	@Override
	public Sucursal findByPrimaryKey(int idSucursal)
		throws NoSuchSucursalException {
		return findByPrimaryKey((Serializable)idSucursal);
	}

	/**
	 * Returns the sucursal with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the sucursal
	 * @return the sucursal, or <code>null</code> if a sucursal with the primary key could not be found
	 */
	@Override
	public Sucursal fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
				SucursalImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Sucursal sucursal = (Sucursal)serializable;

		if (sucursal == null) {
			Session session = null;

			try {
				session = openSession();

				sucursal = (Sucursal)session.get(SucursalImpl.class, primaryKey);

				if (sucursal != null) {
					cacheResult(sucursal);
				}
				else {
					entityCache.putResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
						SucursalImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
					SucursalImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return sucursal;
	}

	/**
	 * Returns the sucursal with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idSucursal the primary key of the sucursal
	 * @return the sucursal, or <code>null</code> if a sucursal with the primary key could not be found
	 */
	@Override
	public Sucursal fetchByPrimaryKey(int idSucursal) {
		return fetchByPrimaryKey((Serializable)idSucursal);
	}

	@Override
	public Map<Serializable, Sucursal> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Sucursal> map = new HashMap<Serializable, Sucursal>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Sucursal sucursal = fetchByPrimaryKey(primaryKey);

			if (sucursal != null) {
				map.put(primaryKey, sucursal);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
					SucursalImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Sucursal)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_SUCURSAL_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Sucursal sucursal : (List<Sucursal>)q.list()) {
				map.put(sucursal.getPrimaryKeyObj(), sucursal);

				cacheResult(sucursal);

				uncachedPrimaryKeys.remove(sucursal.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(SucursalModelImpl.ENTITY_CACHE_ENABLED,
					SucursalImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the sucursals.
	 *
	 * @return the sucursals
	 */
	@Override
	public List<Sucursal> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sucursals.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sucursals
	 * @param end the upper bound of the range of sucursals (not inclusive)
	 * @return the range of sucursals
	 */
	@Override
	public List<Sucursal> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the sucursals.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sucursals
	 * @param end the upper bound of the range of sucursals (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of sucursals
	 */
	@Override
	public List<Sucursal> findAll(int start, int end,
		OrderByComparator<Sucursal> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the sucursals.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SucursalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sucursals
	 * @param end the upper bound of the range of sucursals (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of sucursals
	 */
	@Override
	public List<Sucursal> findAll(int start, int end,
		OrderByComparator<Sucursal> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Sucursal> list = null;

		if (retrieveFromCache) {
			list = (List<Sucursal>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_SUCURSAL);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SUCURSAL;

				if (pagination) {
					sql = sql.concat(SucursalModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Sucursal>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Sucursal>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the sucursals from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Sucursal sucursal : findAll()) {
			remove(sucursal);
		}
	}

	/**
	 * Returns the number of sucursals.
	 *
	 * @return the number of sucursals
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SUCURSAL);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return SucursalModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the sucursal persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(SucursalImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_SUCURSAL = "SELECT sucursal FROM Sucursal sucursal";
	private static final String _SQL_SELECT_SUCURSAL_WHERE_PKS_IN = "SELECT sucursal FROM Sucursal sucursal WHERE idSucursal IN (";
	private static final String _SQL_SELECT_SUCURSAL_WHERE = "SELECT sucursal FROM Sucursal sucursal WHERE ";
	private static final String _SQL_COUNT_SUCURSAL = "SELECT COUNT(sucursal) FROM Sucursal sucursal";
	private static final String _SQL_COUNT_SUCURSAL_WHERE = "SELECT COUNT(sucursal) FROM Sucursal sucursal WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "sucursal.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Sucursal exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Sucursal exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(SucursalPersistenceImpl.class);
}