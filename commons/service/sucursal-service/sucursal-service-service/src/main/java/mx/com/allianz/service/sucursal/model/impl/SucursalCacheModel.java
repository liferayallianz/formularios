/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.sucursal.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.sucursal.model.Sucursal;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Sucursal in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Sucursal
 * @generated
 */
@ProviderType
public class SucursalCacheModel implements CacheModel<Sucursal>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SucursalCacheModel)) {
			return false;
		}

		SucursalCacheModel sucursalCacheModel = (SucursalCacheModel)obj;

		if (idSucursal == sucursalCacheModel.idSucursal) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, idSucursal);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{idSucursal=");
		sb.append(idSucursal);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Sucursal toEntityModel() {
		SucursalImpl sucursalImpl = new SucursalImpl();

		sucursalImpl.setIdSucursal(idSucursal);

		if (descripcion == null) {
			sucursalImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			sucursalImpl.setDescripcion(descripcion);
		}

		sucursalImpl.resetOriginalValues();

		return sucursalImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idSucursal = objectInput.readInt();
		descripcion = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(idSucursal);

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}
	}

	public int idSucursal;
	public String descripcion;
}