/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.moneda.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.moneda.exception.NoSuchMonedaException;
import mx.com.allianz.service.moneda.model.Moneda;
import mx.com.allianz.service.moneda.model.impl.MonedaImpl;
import mx.com.allianz.service.moneda.model.impl.MonedaModelImpl;
import mx.com.allianz.service.moneda.service.persistence.MonedaPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the moneda service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MonedaPersistence
 * @see mx.com.allianz.service.moneda.service.persistence.MonedaUtil
 * @generated
 */
@ProviderType
public class MonedaPersistenceImpl extends BasePersistenceImpl<Moneda>
	implements MonedaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MonedaUtil} to access the moneda persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = MonedaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MonedaModelImpl.ENTITY_CACHE_ENABLED,
			MonedaModelImpl.FINDER_CACHE_ENABLED, MonedaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MonedaModelImpl.ENTITY_CACHE_ENABLED,
			MonedaModelImpl.FINDER_CACHE_ENABLED, MonedaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MonedaModelImpl.ENTITY_CACHE_ENABLED,
			MonedaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(MonedaModelImpl.ENTITY_CACHE_ENABLED,
			MonedaModelImpl.FINDER_CACHE_ENABLED, MonedaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByfindByDescripcion",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(MonedaModelImpl.ENTITY_CACHE_ENABLED,
			MonedaModelImpl.FINDER_CACHE_ENABLED, MonedaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByDescripcion", new String[] { String.class.getName() },
			MonedaModelImpl.DESCRIPCION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION = new FinderPath(MonedaModelImpl.ENTITY_CACHE_ENABLED,
			MonedaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByDescripcion", new String[] { String.class.getName() });

	/**
	 * Returns all the monedas where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the matching monedas
	 */
	@Override
	public List<Moneda> findByfindByDescripcion(String descripcion) {
		return findByfindByDescripcion(descripcion, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the monedas where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of monedas
	 * @param end the upper bound of the range of monedas (not inclusive)
	 * @return the range of matching monedas
	 */
	@Override
	public List<Moneda> findByfindByDescripcion(String descripcion, int start,
		int end) {
		return findByfindByDescripcion(descripcion, start, end, null);
	}

	/**
	 * Returns an ordered range of all the monedas where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of monedas
	 * @param end the upper bound of the range of monedas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching monedas
	 */
	@Override
	public List<Moneda> findByfindByDescripcion(String descripcion, int start,
		int end, OrderByComparator<Moneda> orderByComparator) {
		return findByfindByDescripcion(descripcion, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the monedas where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of monedas
	 * @param end the upper bound of the range of monedas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching monedas
	 */
	@Override
	public List<Moneda> findByfindByDescripcion(String descripcion, int start,
		int end, OrderByComparator<Moneda> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion, start, end, orderByComparator };
		}

		List<Moneda> list = null;

		if (retrieveFromCache) {
			list = (List<Moneda>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Moneda moneda : list) {
					if (!Objects.equals(descripcion, moneda.getDescripcion())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MONEDA_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MonedaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				if (!pagination) {
					list = (List<Moneda>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Moneda>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first moneda in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching moneda
	 * @throws NoSuchMonedaException if a matching moneda could not be found
	 */
	@Override
	public Moneda findByfindByDescripcion_First(String descripcion,
		OrderByComparator<Moneda> orderByComparator)
		throws NoSuchMonedaException {
		Moneda moneda = fetchByfindByDescripcion_First(descripcion,
				orderByComparator);

		if (moneda != null) {
			return moneda;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMonedaException(msg.toString());
	}

	/**
	 * Returns the first moneda in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching moneda, or <code>null</code> if a matching moneda could not be found
	 */
	@Override
	public Moneda fetchByfindByDescripcion_First(String descripcion,
		OrderByComparator<Moneda> orderByComparator) {
		List<Moneda> list = findByfindByDescripcion(descripcion, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last moneda in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching moneda
	 * @throws NoSuchMonedaException if a matching moneda could not be found
	 */
	@Override
	public Moneda findByfindByDescripcion_Last(String descripcion,
		OrderByComparator<Moneda> orderByComparator)
		throws NoSuchMonedaException {
		Moneda moneda = fetchByfindByDescripcion_Last(descripcion,
				orderByComparator);

		if (moneda != null) {
			return moneda;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMonedaException(msg.toString());
	}

	/**
	 * Returns the last moneda in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching moneda, or <code>null</code> if a matching moneda could not be found
	 */
	@Override
	public Moneda fetchByfindByDescripcion_Last(String descripcion,
		OrderByComparator<Moneda> orderByComparator) {
		int count = countByfindByDescripcion(descripcion);

		if (count == 0) {
			return null;
		}

		List<Moneda> list = findByfindByDescripcion(descripcion, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the monedas before and after the current moneda in the ordered set where descripcion = &#63;.
	 *
	 * @param idMoneda the primary key of the current moneda
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next moneda
	 * @throws NoSuchMonedaException if a moneda with the primary key could not be found
	 */
	@Override
	public Moneda[] findByfindByDescripcion_PrevAndNext(int idMoneda,
		String descripcion, OrderByComparator<Moneda> orderByComparator)
		throws NoSuchMonedaException {
		Moneda moneda = findByPrimaryKey(idMoneda);

		Session session = null;

		try {
			session = openSession();

			Moneda[] array = new MonedaImpl[3];

			array[0] = getByfindByDescripcion_PrevAndNext(session, moneda,
					descripcion, orderByComparator, true);

			array[1] = moneda;

			array[2] = getByfindByDescripcion_PrevAndNext(session, moneda,
					descripcion, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Moneda getByfindByDescripcion_PrevAndNext(Session session,
		Moneda moneda, String descripcion,
		OrderByComparator<Moneda> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MONEDA_WHERE);

		boolean bindDescripcion = false;

		if (descripcion == null) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
		}
		else if (descripcion.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
		}
		else {
			bindDescripcion = true;

			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MonedaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDescripcion) {
			qPos.add(descripcion);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(moneda);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Moneda> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the monedas where descripcion = &#63; from the database.
	 *
	 * @param descripcion the descripcion
	 */
	@Override
	public void removeByfindByDescripcion(String descripcion) {
		for (Moneda moneda : findByfindByDescripcion(descripcion,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(moneda);
		}
	}

	/**
	 * Returns the number of monedas where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the number of matching monedas
	 */
	@Override
	public int countByfindByDescripcion(String descripcion) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION;

		Object[] finderArgs = new Object[] { descripcion };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MONEDA_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1 = "moneda.descripcion IS NULL";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2 = "moneda.descripcion = ?";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3 = "(moneda.descripcion IS NULL OR moneda.descripcion = '')";

	public MonedaPersistenceImpl() {
		setModelClass(Moneda.class);
	}

	/**
	 * Caches the moneda in the entity cache if it is enabled.
	 *
	 * @param moneda the moneda
	 */
	@Override
	public void cacheResult(Moneda moneda) {
		entityCache.putResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
			MonedaImpl.class, moneda.getPrimaryKey(), moneda);

		moneda.resetOriginalValues();
	}

	/**
	 * Caches the monedas in the entity cache if it is enabled.
	 *
	 * @param monedas the monedas
	 */
	@Override
	public void cacheResult(List<Moneda> monedas) {
		for (Moneda moneda : monedas) {
			if (entityCache.getResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
						MonedaImpl.class, moneda.getPrimaryKey()) == null) {
				cacheResult(moneda);
			}
			else {
				moneda.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all monedas.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(MonedaImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the moneda.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Moneda moneda) {
		entityCache.removeResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
			MonedaImpl.class, moneda.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Moneda> monedas) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Moneda moneda : monedas) {
			entityCache.removeResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
				MonedaImpl.class, moneda.getPrimaryKey());
		}
	}

	/**
	 * Creates a new moneda with the primary key. Does not add the moneda to the database.
	 *
	 * @param idMoneda the primary key for the new moneda
	 * @return the new moneda
	 */
	@Override
	public Moneda create(int idMoneda) {
		Moneda moneda = new MonedaImpl();

		moneda.setNew(true);
		moneda.setPrimaryKey(idMoneda);

		return moneda;
	}

	/**
	 * Removes the moneda with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idMoneda the primary key of the moneda
	 * @return the moneda that was removed
	 * @throws NoSuchMonedaException if a moneda with the primary key could not be found
	 */
	@Override
	public Moneda remove(int idMoneda) throws NoSuchMonedaException {
		return remove((Serializable)idMoneda);
	}

	/**
	 * Removes the moneda with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the moneda
	 * @return the moneda that was removed
	 * @throws NoSuchMonedaException if a moneda with the primary key could not be found
	 */
	@Override
	public Moneda remove(Serializable primaryKey) throws NoSuchMonedaException {
		Session session = null;

		try {
			session = openSession();

			Moneda moneda = (Moneda)session.get(MonedaImpl.class, primaryKey);

			if (moneda == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMonedaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(moneda);
		}
		catch (NoSuchMonedaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Moneda removeImpl(Moneda moneda) {
		moneda = toUnwrappedModel(moneda);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(moneda)) {
				moneda = (Moneda)session.get(MonedaImpl.class,
						moneda.getPrimaryKeyObj());
			}

			if (moneda != null) {
				session.delete(moneda);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (moneda != null) {
			clearCache(moneda);
		}

		return moneda;
	}

	@Override
	public Moneda updateImpl(Moneda moneda) {
		moneda = toUnwrappedModel(moneda);

		boolean isNew = moneda.isNew();

		MonedaModelImpl monedaModelImpl = (MonedaModelImpl)moneda;

		Session session = null;

		try {
			session = openSession();

			if (moneda.isNew()) {
				session.save(moneda);

				moneda.setNew(false);
			}
			else {
				moneda = (Moneda)session.merge(moneda);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !MonedaModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((monedaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						monedaModelImpl.getOriginalDescripcion()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);

				args = new Object[] { monedaModelImpl.getDescripcion() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);
			}
		}

		entityCache.putResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
			MonedaImpl.class, moneda.getPrimaryKey(), moneda, false);

		moneda.resetOriginalValues();

		return moneda;
	}

	protected Moneda toUnwrappedModel(Moneda moneda) {
		if (moneda instanceof MonedaImpl) {
			return moneda;
		}

		MonedaImpl monedaImpl = new MonedaImpl();

		monedaImpl.setNew(moneda.isNew());
		monedaImpl.setPrimaryKey(moneda.getPrimaryKey());

		monedaImpl.setIdMoneda(moneda.getIdMoneda());
		monedaImpl.setCodigoMoneda(moneda.getCodigoMoneda());
		monedaImpl.setDescripcion(moneda.getDescripcion());

		return monedaImpl;
	}

	/**
	 * Returns the moneda with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the moneda
	 * @return the moneda
	 * @throws NoSuchMonedaException if a moneda with the primary key could not be found
	 */
	@Override
	public Moneda findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMonedaException {
		Moneda moneda = fetchByPrimaryKey(primaryKey);

		if (moneda == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMonedaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return moneda;
	}

	/**
	 * Returns the moneda with the primary key or throws a {@link NoSuchMonedaException} if it could not be found.
	 *
	 * @param idMoneda the primary key of the moneda
	 * @return the moneda
	 * @throws NoSuchMonedaException if a moneda with the primary key could not be found
	 */
	@Override
	public Moneda findByPrimaryKey(int idMoneda) throws NoSuchMonedaException {
		return findByPrimaryKey((Serializable)idMoneda);
	}

	/**
	 * Returns the moneda with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the moneda
	 * @return the moneda, or <code>null</code> if a moneda with the primary key could not be found
	 */
	@Override
	public Moneda fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
				MonedaImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Moneda moneda = (Moneda)serializable;

		if (moneda == null) {
			Session session = null;

			try {
				session = openSession();

				moneda = (Moneda)session.get(MonedaImpl.class, primaryKey);

				if (moneda != null) {
					cacheResult(moneda);
				}
				else {
					entityCache.putResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
						MonedaImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
					MonedaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return moneda;
	}

	/**
	 * Returns the moneda with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idMoneda the primary key of the moneda
	 * @return the moneda, or <code>null</code> if a moneda with the primary key could not be found
	 */
	@Override
	public Moneda fetchByPrimaryKey(int idMoneda) {
		return fetchByPrimaryKey((Serializable)idMoneda);
	}

	@Override
	public Map<Serializable, Moneda> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Moneda> map = new HashMap<Serializable, Moneda>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Moneda moneda = fetchByPrimaryKey(primaryKey);

			if (moneda != null) {
				map.put(primaryKey, moneda);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
					MonedaImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Moneda)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_MONEDA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Moneda moneda : (List<Moneda>)q.list()) {
				map.put(moneda.getPrimaryKeyObj(), moneda);

				cacheResult(moneda);

				uncachedPrimaryKeys.remove(moneda.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(MonedaModelImpl.ENTITY_CACHE_ENABLED,
					MonedaImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the monedas.
	 *
	 * @return the monedas
	 */
	@Override
	public List<Moneda> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the monedas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of monedas
	 * @param end the upper bound of the range of monedas (not inclusive)
	 * @return the range of monedas
	 */
	@Override
	public List<Moneda> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the monedas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of monedas
	 * @param end the upper bound of the range of monedas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of monedas
	 */
	@Override
	public List<Moneda> findAll(int start, int end,
		OrderByComparator<Moneda> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the monedas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of monedas
	 * @param end the upper bound of the range of monedas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of monedas
	 */
	@Override
	public List<Moneda> findAll(int start, int end,
		OrderByComparator<Moneda> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Moneda> list = null;

		if (retrieveFromCache) {
			list = (List<Moneda>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_MONEDA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MONEDA;

				if (pagination) {
					sql = sql.concat(MonedaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Moneda>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Moneda>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the monedas from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Moneda moneda : findAll()) {
			remove(moneda);
		}
	}

	/**
	 * Returns the number of monedas.
	 *
	 * @return the number of monedas
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MONEDA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return MonedaModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the moneda persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(MonedaImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_MONEDA = "SELECT moneda FROM Moneda moneda";
	private static final String _SQL_SELECT_MONEDA_WHERE_PKS_IN = "SELECT moneda FROM Moneda moneda WHERE idMoneda IN (";
	private static final String _SQL_SELECT_MONEDA_WHERE = "SELECT moneda FROM Moneda moneda WHERE ";
	private static final String _SQL_COUNT_MONEDA = "SELECT COUNT(moneda) FROM Moneda moneda";
	private static final String _SQL_COUNT_MONEDA_WHERE = "SELECT COUNT(moneda) FROM Moneda moneda WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "moneda.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Moneda exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Moneda exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(MonedaPersistenceImpl.class);
}