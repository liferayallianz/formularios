/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.moneda.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.moneda.exception.NoSuchMonedaException;
import mx.com.allianz.service.moneda.model.Moneda;

/**
 * The persistence interface for the moneda service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.moneda.service.persistence.impl.MonedaPersistenceImpl
 * @see MonedaUtil
 * @generated
 */
@ProviderType
public interface MonedaPersistence extends BasePersistence<Moneda> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MonedaUtil} to access the moneda persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the monedas where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching monedas
	*/
	public java.util.List<Moneda> findByfindByDescripcion(
		java.lang.String descripcion);

	/**
	* Returns a range of all the monedas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @return the range of matching monedas
	*/
	public java.util.List<Moneda> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end);

	/**
	* Returns an ordered range of all the monedas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching monedas
	*/
	public java.util.List<Moneda> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Moneda> orderByComparator);

	/**
	* Returns an ordered range of all the monedas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching monedas
	*/
	public java.util.List<Moneda> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Moneda> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first moneda in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching moneda
	* @throws NoSuchMonedaException if a matching moneda could not be found
	*/
	public Moneda findByfindByDescripcion_First(java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Moneda> orderByComparator)
		throws NoSuchMonedaException;

	/**
	* Returns the first moneda in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching moneda, or <code>null</code> if a matching moneda could not be found
	*/
	public Moneda fetchByfindByDescripcion_First(java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Moneda> orderByComparator);

	/**
	* Returns the last moneda in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching moneda
	* @throws NoSuchMonedaException if a matching moneda could not be found
	*/
	public Moneda findByfindByDescripcion_Last(java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Moneda> orderByComparator)
		throws NoSuchMonedaException;

	/**
	* Returns the last moneda in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching moneda, or <code>null</code> if a matching moneda could not be found
	*/
	public Moneda fetchByfindByDescripcion_Last(java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Moneda> orderByComparator);

	/**
	* Returns the monedas before and after the current moneda in the ordered set where descripcion = &#63;.
	*
	* @param idMoneda the primary key of the current moneda
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next moneda
	* @throws NoSuchMonedaException if a moneda with the primary key could not be found
	*/
	public Moneda[] findByfindByDescripcion_PrevAndNext(int idMoneda,
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Moneda> orderByComparator)
		throws NoSuchMonedaException;

	/**
	* Removes all the monedas where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public void removeByfindByDescripcion(java.lang.String descripcion);

	/**
	* Returns the number of monedas where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching monedas
	*/
	public int countByfindByDescripcion(java.lang.String descripcion);

	/**
	* Caches the moneda in the entity cache if it is enabled.
	*
	* @param moneda the moneda
	*/
	public void cacheResult(Moneda moneda);

	/**
	* Caches the monedas in the entity cache if it is enabled.
	*
	* @param monedas the monedas
	*/
	public void cacheResult(java.util.List<Moneda> monedas);

	/**
	* Creates a new moneda with the primary key. Does not add the moneda to the database.
	*
	* @param idMoneda the primary key for the new moneda
	* @return the new moneda
	*/
	public Moneda create(int idMoneda);

	/**
	* Removes the moneda with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idMoneda the primary key of the moneda
	* @return the moneda that was removed
	* @throws NoSuchMonedaException if a moneda with the primary key could not be found
	*/
	public Moneda remove(int idMoneda) throws NoSuchMonedaException;

	public Moneda updateImpl(Moneda moneda);

	/**
	* Returns the moneda with the primary key or throws a {@link NoSuchMonedaException} if it could not be found.
	*
	* @param idMoneda the primary key of the moneda
	* @return the moneda
	* @throws NoSuchMonedaException if a moneda with the primary key could not be found
	*/
	public Moneda findByPrimaryKey(int idMoneda) throws NoSuchMonedaException;

	/**
	* Returns the moneda with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idMoneda the primary key of the moneda
	* @return the moneda, or <code>null</code> if a moneda with the primary key could not be found
	*/
	public Moneda fetchByPrimaryKey(int idMoneda);

	@Override
	public java.util.Map<java.io.Serializable, Moneda> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the monedas.
	*
	* @return the monedas
	*/
	public java.util.List<Moneda> findAll();

	/**
	* Returns a range of all the monedas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @return the range of monedas
	*/
	public java.util.List<Moneda> findAll(int start, int end);

	/**
	* Returns an ordered range of all the monedas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of monedas
	*/
	public java.util.List<Moneda> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Moneda> orderByComparator);

	/**
	* Returns an ordered range of all the monedas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of monedas
	*/
	public java.util.List<Moneda> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Moneda> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the monedas from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of monedas.
	*
	* @return the number of monedas
	*/
	public int countAll();
}