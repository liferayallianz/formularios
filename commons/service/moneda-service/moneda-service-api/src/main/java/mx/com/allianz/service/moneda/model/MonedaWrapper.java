/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.moneda.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Moneda}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Moneda
 * @generated
 */
@ProviderType
public class MonedaWrapper implements Moneda, ModelWrapper<Moneda> {
	public MonedaWrapper(Moneda moneda) {
		_moneda = moneda;
	}

	@Override
	public Class<?> getModelClass() {
		return Moneda.class;
	}

	@Override
	public String getModelClassName() {
		return Moneda.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idMoneda", getIdMoneda());
		attributes.put("codigoMoneda", getCodigoMoneda());
		attributes.put("descripcion", getDescripcion());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idMoneda = (Integer)attributes.get("idMoneda");

		if (idMoneda != null) {
			setIdMoneda(idMoneda);
		}

		String codigoMoneda = (String)attributes.get("codigoMoneda");

		if (codigoMoneda != null) {
			setCodigoMoneda(codigoMoneda);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _moneda.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _moneda.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _moneda.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _moneda.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Moneda> toCacheModel() {
		return _moneda.toCacheModel();
	}

	@Override
	public int compareTo(Moneda moneda) {
		return _moneda.compareTo(moneda);
	}

	/**
	* Returns the id moneda of this moneda.
	*
	* @return the id moneda of this moneda
	*/
	@Override
	public int getIdMoneda() {
		return _moneda.getIdMoneda();
	}

	/**
	* Returns the primary key of this moneda.
	*
	* @return the primary key of this moneda
	*/
	@Override
	public int getPrimaryKey() {
		return _moneda.getPrimaryKey();
	}

	@Override
	public int hashCode() {
		return _moneda.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _moneda.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new MonedaWrapper((Moneda)_moneda.clone());
	}

	/**
	* Returns the codigo moneda of this moneda.
	*
	* @return the codigo moneda of this moneda
	*/
	@Override
	public java.lang.String getCodigoMoneda() {
		return _moneda.getCodigoMoneda();
	}

	/**
	* Returns the descripcion of this moneda.
	*
	* @return the descripcion of this moneda
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _moneda.getDescripcion();
	}

	@Override
	public java.lang.String toString() {
		return _moneda.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _moneda.toXmlString();
	}

	@Override
	public Moneda toEscapedModel() {
		return new MonedaWrapper(_moneda.toEscapedModel());
	}

	@Override
	public Moneda toUnescapedModel() {
		return new MonedaWrapper(_moneda.toUnescapedModel());
	}

	@Override
	public void persist() {
		_moneda.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_moneda.setCachedModel(cachedModel);
	}

	/**
	* Sets the codigo moneda of this moneda.
	*
	* @param codigoMoneda the codigo moneda of this moneda
	*/
	@Override
	public void setCodigoMoneda(java.lang.String codigoMoneda) {
		_moneda.setCodigoMoneda(codigoMoneda);
	}

	/**
	* Sets the descripcion of this moneda.
	*
	* @param descripcion the descripcion of this moneda
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_moneda.setDescripcion(descripcion);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_moneda.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_moneda.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_moneda.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the id moneda of this moneda.
	*
	* @param idMoneda the id moneda of this moneda
	*/
	@Override
	public void setIdMoneda(int idMoneda) {
		_moneda.setIdMoneda(idMoneda);
	}

	@Override
	public void setNew(boolean n) {
		_moneda.setNew(n);
	}

	/**
	* Sets the primary key of this moneda.
	*
	* @param primaryKey the primary key of this moneda
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_moneda.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_moneda.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MonedaWrapper)) {
			return false;
		}

		MonedaWrapper monedaWrapper = (MonedaWrapper)obj;

		if (Objects.equals(_moneda, monedaWrapper._moneda)) {
			return true;
		}

		return false;
	}

	@Override
	public Moneda getWrappedModel() {
		return _moneda;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _moneda.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _moneda.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_moneda.resetOriginalValues();
	}

	private final Moneda _moneda;
}