package mx.com.allianz.service.rest.moneda;
/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.MonedaDTO;
import mx.com.allianz.service.moneda.service.MonedaLocalServiceUtil;

/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio MonedaDTO que
 * permite visualizar una lista de datos, filtra el orden de la b\u00fasqueda y
 * muestra el ordenamiento conforme al filtro.
 * 
 *
 * @author TestingZone
 * @version 1.0
 * @since 2016-12-2
 * 
 * 
 */

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.moneda")
/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(
	immediate = true, property = {"jaxrs.application=true"},
	service = Application.class
	)

public class MonedaRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object)this);
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los monedas
	 * del catalogo.
	 * 
	 * @return getAllMonedas()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getMonedas")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({MediaType.APPLICATION_JSON})
	public List<MonedaDTO> getMonedas() {
		// Se regresan todos las monedas.
		return getAllMonedas();
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los paises del
	 * catalogo.
	 * 
	 * @return getAllPaises().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findMonedas")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({MediaType.APPLICATION_JSON})
	public List<MonedaDTO>findMonedasBy(
			
			
			// El valor @DefaultValue se utiliz\u00e1 para definir el valor por
			// defecto para el par\u00e1metro matrixParam.
			// @QueryParam se utiliz\u00e1 para extraer los par\u00e1metros de
			// consulta del "componente de consulta" de la URL de la solicitud.
			@DefaultValue("-1")@QueryParam("id_moneda") int idMoneda,
			@DefaultValue("")@QueryParam("codigo_moneda") String codigoMoneda,
			@DefaultValue("")@QueryParam("descripcion") String descripcion){
			// stream() Devuelve un flujo secuencial considerando la colecci\u00f3n
			// como su origen.
			// Convierte la lista en stream.
		System.out.println("En findMonedasBy");
		
		return getAllMonedas().stream()
				// Filtrado por identificador de moneda, si se envia -1 se
				// ignora el filtro.
				.filter((moneda) -> idMoneda == -1 || moneda.getIdMoneda() == idMoneda)
				// Filtrado por codigo de la moneda, si es vacio se ignora el filtro.
				.filter((moneda) -> codigoMoneda.isEmpty() || moneda.getCodigoMoneda().contains(codigoMoneda))
				// Filtrado por descripcion de las monedas, si es vacio se ignora el filtro. 
				.filter((moneda) -> descripcion.isEmpty() || moneda.getDescripcion().contains(descripcion))
				// Salida de colecci\u00f3n y el stream se convierte en lista.
				.collect(Collectors.toList());
				
	}
	
	/**
	 * M\u00e9todo getAllMonedas() del tipo list generada para mostrar un arreglo
	 * de la lista.
	 * 
	 * @return listaMonedas
	 */
	
	private List<MonedaDTO> getAllMonedas(){
		
		return MonedaLocalServiceUtil.getMonedas(-1, -1).stream().
				map(moneda -> new MonedaDTO(moneda.getIdMoneda(), 
				moneda.getCodigoMoneda(), moneda.getDescripcion()))
				.collect(Collectors.toList());
	}

}
