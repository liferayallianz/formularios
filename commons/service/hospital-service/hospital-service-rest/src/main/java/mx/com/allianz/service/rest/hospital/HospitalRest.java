package mx.com.allianz.service.rest.hospital;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.HospitalDTO;
import mx.com.allianz.service.hospital.service.HospitalLocalServiceUtil;



/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio HospitalDTO que
 * permite visualizar una lista de datos, filtra el orden de la b\u00fasqueda y
 * muestra el ordenamiento conforme al filtro.
 * 
 *
 * @author TestingZone
 * @version 1.0
 * @since 2016-12-2
 * 
 * 
 */

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.hospital")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)
public class HospitalRest extends Application {

	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los hospitales del
	 * catalogo.
	 * 
	 * @return getAllCAT_Hospital()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getHospitales")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<HospitalDTO> getCatHospitales() {
		// Se regresan todos los hospitales.
		return getAllHospital();
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los hospitales del
	 * catalogo.
	 * 
	 * @return getAllCAT_Hospital().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findHospitales")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<HospitalDTO> findHospitalesBy(
			// El valor @DefaultValue se utiliz\u00e1 para definir el valor por
			// defecto para el par\u00e1metro matrixParam.
			// @QueryParam se utiliz\u00e1 para extraer los par\u00e1metros de
			// consulta del "componente de consulta" de la URL de la solicitud.
			@DefaultValue("-1") @QueryParam("id_Hospital") int idHospital,
			@DefaultValue("-1") @QueryParam("codigo_Grupo") int codigoGrupo,
			@DefaultValue("-1") @QueryParam("codigo_SubGrupo") int codigoSubGrupo,
			@DefaultValue("-1") @QueryParam("codigo_Estado") int codigoEstado,
			@DefaultValue("") @QueryParam("descripcion_Hospital") String descripcionHospital) {
		// stream() Devuelve un flujo secuencial considerando la colecci\u00f3n
		// como su origen.
		// Convierte la lista en stream.
		return getAllHospital().stream()

				// Filtrado por identificador de hospitales, si se envia -1 se
				// ignora el filtro.
				.filter((catHospital) -> idHospital == -1 || catHospital.getIdHospital() == idHospital)
				// Filtrado por Codigo de grupo de hospitales, si se envia -1 se
				// ignora el filtro .
				.filter((catHospital) -> codigoGrupo == -1 || catHospital.getCodigoGrupo() == codigoGrupo)
				// Filtrado por codigo de subgrupo de hospitales, si se envia -1
				// se ignora el filtro.
				.filter((catHospital) -> codigoSubGrupo == -1 || catHospital.getCodigoSubGrupo() == codigoSubGrupo)
				// Filtrado por codigo de estado de hospitales, si se envia -1
				// se ignora el filtro
				.filter((catHospital) -> codigoEstado == -1 || catHospital.getCodigoEstado() == codigoEstado)
				// Filtrado por descripcion de hospitales, si es vacio se ignora
				// el filtro.
				.filter((catHospital) -> descripcionHospital.isEmpty() || catHospital.getDescripcionHospital()
						.toUpperCase().contains(descripcionHospital.toUpperCase()))
				// Salida de colecci\u00f3n y el stream se convierte en lista.
				.collect(Collectors.toList());

	}

	/**
	 * M\u00e9todo getAllHospital() del tipo list generada para mostrar un
	 * arreglo de la lista.
	 * 
	 * @return listaCatHospilates
	 */
	private List<HospitalDTO> getAllHospital() {
		
		return HospitalLocalServiceUtil.getHospitals(-1, -1).stream().
				map(hospital -> new HospitalDTO(hospital.getIdHospital(),
				hospital.getCodigoGrupo(), hospital.getCodigoSubGrupo(), 
				hospital.getDescripcionHospital(), hospital.getCodigoEstado())).
				collect(Collectors.toList());
	}
}