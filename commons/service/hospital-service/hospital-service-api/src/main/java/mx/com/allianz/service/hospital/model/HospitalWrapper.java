/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.hospital.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Hospital}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Hospital
 * @generated
 */
@ProviderType
public class HospitalWrapper implements Hospital, ModelWrapper<Hospital> {
	public HospitalWrapper(Hospital hospital) {
		_hospital = hospital;
	}

	@Override
	public Class<?> getModelClass() {
		return Hospital.class;
	}

	@Override
	public String getModelClassName() {
		return Hospital.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idHospital", getIdHospital());
		attributes.put("codigoGrupo", getCodigoGrupo());
		attributes.put("codigoSubGrupo", getCodigoSubGrupo());
		attributes.put("descripcionHospital", getDescripcionHospital());
		attributes.put("codigoEstado", getCodigoEstado());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idHospital = (Integer)attributes.get("idHospital");

		if (idHospital != null) {
			setIdHospital(idHospital);
		}

		Integer codigoGrupo = (Integer)attributes.get("codigoGrupo");

		if (codigoGrupo != null) {
			setCodigoGrupo(codigoGrupo);
		}

		Integer codigoSubGrupo = (Integer)attributes.get("codigoSubGrupo");

		if (codigoSubGrupo != null) {
			setCodigoSubGrupo(codigoSubGrupo);
		}

		String descripcionHospital = (String)attributes.get(
				"descripcionHospital");

		if (descripcionHospital != null) {
			setDescripcionHospital(descripcionHospital);
		}

		Integer codigoEstado = (Integer)attributes.get("codigoEstado");

		if (codigoEstado != null) {
			setCodigoEstado(codigoEstado);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _hospital.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _hospital.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _hospital.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _hospital.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Hospital> toCacheModel() {
		return _hospital.toCacheModel();
	}

	@Override
	public int compareTo(Hospital hospital) {
		return _hospital.compareTo(hospital);
	}

	/**
	* Returns the codigo estado of this hospital.
	*
	* @return the codigo estado of this hospital
	*/
	@Override
	public int getCodigoEstado() {
		return _hospital.getCodigoEstado();
	}

	/**
	* Returns the codigo grupo of this hospital.
	*
	* @return the codigo grupo of this hospital
	*/
	@Override
	public int getCodigoGrupo() {
		return _hospital.getCodigoGrupo();
	}

	/**
	* Returns the codigo sub grupo of this hospital.
	*
	* @return the codigo sub grupo of this hospital
	*/
	@Override
	public int getCodigoSubGrupo() {
		return _hospital.getCodigoSubGrupo();
	}

	/**
	* Returns the id hospital of this hospital.
	*
	* @return the id hospital of this hospital
	*/
	@Override
	public int getIdHospital() {
		return _hospital.getIdHospital();
	}

	/**
	* Returns the primary key of this hospital.
	*
	* @return the primary key of this hospital
	*/
	@Override
	public int getPrimaryKey() {
		return _hospital.getPrimaryKey();
	}

	@Override
	public int hashCode() {
		return _hospital.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _hospital.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new HospitalWrapper((Hospital)_hospital.clone());
	}

	/**
	* Returns the descripcion hospital of this hospital.
	*
	* @return the descripcion hospital of this hospital
	*/
	@Override
	public java.lang.String getDescripcionHospital() {
		return _hospital.getDescripcionHospital();
	}

	@Override
	public java.lang.String toString() {
		return _hospital.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _hospital.toXmlString();
	}

	@Override
	public Hospital toEscapedModel() {
		return new HospitalWrapper(_hospital.toEscapedModel());
	}

	@Override
	public Hospital toUnescapedModel() {
		return new HospitalWrapper(_hospital.toUnescapedModel());
	}

	@Override
	public void persist() {
		_hospital.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_hospital.setCachedModel(cachedModel);
	}

	/**
	* Sets the codigo estado of this hospital.
	*
	* @param codigoEstado the codigo estado of this hospital
	*/
	@Override
	public void setCodigoEstado(int codigoEstado) {
		_hospital.setCodigoEstado(codigoEstado);
	}

	/**
	* Sets the codigo grupo of this hospital.
	*
	* @param codigoGrupo the codigo grupo of this hospital
	*/
	@Override
	public void setCodigoGrupo(int codigoGrupo) {
		_hospital.setCodigoGrupo(codigoGrupo);
	}

	/**
	* Sets the codigo sub grupo of this hospital.
	*
	* @param codigoSubGrupo the codigo sub grupo of this hospital
	*/
	@Override
	public void setCodigoSubGrupo(int codigoSubGrupo) {
		_hospital.setCodigoSubGrupo(codigoSubGrupo);
	}

	/**
	* Sets the descripcion hospital of this hospital.
	*
	* @param descripcionHospital the descripcion hospital of this hospital
	*/
	@Override
	public void setDescripcionHospital(java.lang.String descripcionHospital) {
		_hospital.setDescripcionHospital(descripcionHospital);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_hospital.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_hospital.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_hospital.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the id hospital of this hospital.
	*
	* @param idHospital the id hospital of this hospital
	*/
	@Override
	public void setIdHospital(int idHospital) {
		_hospital.setIdHospital(idHospital);
	}

	@Override
	public void setNew(boolean n) {
		_hospital.setNew(n);
	}

	/**
	* Sets the primary key of this hospital.
	*
	* @param primaryKey the primary key of this hospital
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_hospital.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_hospital.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof HospitalWrapper)) {
			return false;
		}

		HospitalWrapper hospitalWrapper = (HospitalWrapper)obj;

		if (Objects.equals(_hospital, hospitalWrapper._hospital)) {
			return true;
		}

		return false;
	}

	@Override
	public Hospital getWrappedModel() {
		return _hospital;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _hospital.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _hospital.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_hospital.resetOriginalValues();
	}

	private final Hospital _hospital;
}