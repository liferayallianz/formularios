/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.hospital.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.hospital.exception.NoSuchHospitalException;
import mx.com.allianz.service.hospital.model.Hospital;

/**
 * The persistence interface for the hospital service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.hospital.service.persistence.impl.HospitalPersistenceImpl
 * @see HospitalUtil
 * @generated
 */
@ProviderType
public interface HospitalPersistence extends BasePersistence<Hospital> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link HospitalUtil} to access the hospital persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the hospitals where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @return the matching hospitals
	*/
	public java.util.List<Hospital> findByfindByCodigoGrupo(int codigoGrupo);

	/**
	* Returns a range of all the hospitals where codigoGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoGrupo the codigo grupo
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @return the range of matching hospitals
	*/
	public java.util.List<Hospital> findByfindByCodigoGrupo(int codigoGrupo,
		int start, int end);

	/**
	* Returns an ordered range of all the hospitals where codigoGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoGrupo the codigo grupo
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching hospitals
	*/
	public java.util.List<Hospital> findByfindByCodigoGrupo(int codigoGrupo,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Hospital> orderByComparator);

	/**
	* Returns an ordered range of all the hospitals where codigoGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoGrupo the codigo grupo
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching hospitals
	*/
	public java.util.List<Hospital> findByfindByCodigoGrupo(int codigoGrupo,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Hospital> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching hospital
	* @throws NoSuchHospitalException if a matching hospital could not be found
	*/
	public Hospital findByfindByCodigoGrupo_First(int codigoGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<Hospital> orderByComparator)
		throws NoSuchHospitalException;

	/**
	* Returns the first hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching hospital, or <code>null</code> if a matching hospital could not be found
	*/
	public Hospital fetchByfindByCodigoGrupo_First(int codigoGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<Hospital> orderByComparator);

	/**
	* Returns the last hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching hospital
	* @throws NoSuchHospitalException if a matching hospital could not be found
	*/
	public Hospital findByfindByCodigoGrupo_Last(int codigoGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<Hospital> orderByComparator)
		throws NoSuchHospitalException;

	/**
	* Returns the last hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching hospital, or <code>null</code> if a matching hospital could not be found
	*/
	public Hospital fetchByfindByCodigoGrupo_Last(int codigoGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<Hospital> orderByComparator);

	/**
	* Returns the hospitals before and after the current hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param idHospital the primary key of the current hospital
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next hospital
	* @throws NoSuchHospitalException if a hospital with the primary key could not be found
	*/
	public Hospital[] findByfindByCodigoGrupo_PrevAndNext(int idHospital,
		int codigoGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<Hospital> orderByComparator)
		throws NoSuchHospitalException;

	/**
	* Removes all the hospitals where codigoGrupo = &#63; from the database.
	*
	* @param codigoGrupo the codigo grupo
	*/
	public void removeByfindByCodigoGrupo(int codigoGrupo);

	/**
	* Returns the number of hospitals where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @return the number of matching hospitals
	*/
	public int countByfindByCodigoGrupo(int codigoGrupo);

	/**
	* Caches the hospital in the entity cache if it is enabled.
	*
	* @param hospital the hospital
	*/
	public void cacheResult(Hospital hospital);

	/**
	* Caches the hospitals in the entity cache if it is enabled.
	*
	* @param hospitals the hospitals
	*/
	public void cacheResult(java.util.List<Hospital> hospitals);

	/**
	* Creates a new hospital with the primary key. Does not add the hospital to the database.
	*
	* @param idHospital the primary key for the new hospital
	* @return the new hospital
	*/
	public Hospital create(int idHospital);

	/**
	* Removes the hospital with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idHospital the primary key of the hospital
	* @return the hospital that was removed
	* @throws NoSuchHospitalException if a hospital with the primary key could not be found
	*/
	public Hospital remove(int idHospital) throws NoSuchHospitalException;

	public Hospital updateImpl(Hospital hospital);

	/**
	* Returns the hospital with the primary key or throws a {@link NoSuchHospitalException} if it could not be found.
	*
	* @param idHospital the primary key of the hospital
	* @return the hospital
	* @throws NoSuchHospitalException if a hospital with the primary key could not be found
	*/
	public Hospital findByPrimaryKey(int idHospital)
		throws NoSuchHospitalException;

	/**
	* Returns the hospital with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idHospital the primary key of the hospital
	* @return the hospital, or <code>null</code> if a hospital with the primary key could not be found
	*/
	public Hospital fetchByPrimaryKey(int idHospital);

	@Override
	public java.util.Map<java.io.Serializable, Hospital> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the hospitals.
	*
	* @return the hospitals
	*/
	public java.util.List<Hospital> findAll();

	/**
	* Returns a range of all the hospitals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @return the range of hospitals
	*/
	public java.util.List<Hospital> findAll(int start, int end);

	/**
	* Returns an ordered range of all the hospitals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of hospitals
	*/
	public java.util.List<Hospital> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Hospital> orderByComparator);

	/**
	* Returns an ordered range of all the hospitals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of hospitals
	*/
	public java.util.List<Hospital> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Hospital> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the hospitals from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of hospitals.
	*
	* @return the number of hospitals
	*/
	public int countAll();
}