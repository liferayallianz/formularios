/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.hospital.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.hospital.model.Hospital;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the hospital service. This utility wraps {@link mx.com.allianz.service.hospital.service.persistence.impl.HospitalPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see HospitalPersistence
 * @see mx.com.allianz.service.hospital.service.persistence.impl.HospitalPersistenceImpl
 * @generated
 */
@ProviderType
public class HospitalUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Hospital hospital) {
		getPersistence().clearCache(hospital);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Hospital> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Hospital> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Hospital> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Hospital> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Hospital update(Hospital hospital) {
		return getPersistence().update(hospital);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Hospital update(Hospital hospital,
		ServiceContext serviceContext) {
		return getPersistence().update(hospital, serviceContext);
	}

	/**
	* Returns all the hospitals where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @return the matching hospitals
	*/
	public static List<Hospital> findByfindByCodigoGrupo(int codigoGrupo) {
		return getPersistence().findByfindByCodigoGrupo(codigoGrupo);
	}

	/**
	* Returns a range of all the hospitals where codigoGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoGrupo the codigo grupo
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @return the range of matching hospitals
	*/
	public static List<Hospital> findByfindByCodigoGrupo(int codigoGrupo,
		int start, int end) {
		return getPersistence().findByfindByCodigoGrupo(codigoGrupo, start, end);
	}

	/**
	* Returns an ordered range of all the hospitals where codigoGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoGrupo the codigo grupo
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching hospitals
	*/
	public static List<Hospital> findByfindByCodigoGrupo(int codigoGrupo,
		int start, int end, OrderByComparator<Hospital> orderByComparator) {
		return getPersistence()
				   .findByfindByCodigoGrupo(codigoGrupo, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the hospitals where codigoGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoGrupo the codigo grupo
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching hospitals
	*/
	public static List<Hospital> findByfindByCodigoGrupo(int codigoGrupo,
		int start, int end, OrderByComparator<Hospital> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByCodigoGrupo(codigoGrupo, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching hospital
	* @throws NoSuchHospitalException if a matching hospital could not be found
	*/
	public static Hospital findByfindByCodigoGrupo_First(int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator)
		throws mx.com.allianz.service.hospital.exception.NoSuchHospitalException {
		return getPersistence()
				   .findByfindByCodigoGrupo_First(codigoGrupo, orderByComparator);
	}

	/**
	* Returns the first hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching hospital, or <code>null</code> if a matching hospital could not be found
	*/
	public static Hospital fetchByfindByCodigoGrupo_First(int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator) {
		return getPersistence()
				   .fetchByfindByCodigoGrupo_First(codigoGrupo,
			orderByComparator);
	}

	/**
	* Returns the last hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching hospital
	* @throws NoSuchHospitalException if a matching hospital could not be found
	*/
	public static Hospital findByfindByCodigoGrupo_Last(int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator)
		throws mx.com.allianz.service.hospital.exception.NoSuchHospitalException {
		return getPersistence()
				   .findByfindByCodigoGrupo_Last(codigoGrupo, orderByComparator);
	}

	/**
	* Returns the last hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching hospital, or <code>null</code> if a matching hospital could not be found
	*/
	public static Hospital fetchByfindByCodigoGrupo_Last(int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator) {
		return getPersistence()
				   .fetchByfindByCodigoGrupo_Last(codigoGrupo, orderByComparator);
	}

	/**
	* Returns the hospitals before and after the current hospital in the ordered set where codigoGrupo = &#63;.
	*
	* @param idHospital the primary key of the current hospital
	* @param codigoGrupo the codigo grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next hospital
	* @throws NoSuchHospitalException if a hospital with the primary key could not be found
	*/
	public static Hospital[] findByfindByCodigoGrupo_PrevAndNext(
		int idHospital, int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator)
		throws mx.com.allianz.service.hospital.exception.NoSuchHospitalException {
		return getPersistence()
				   .findByfindByCodigoGrupo_PrevAndNext(idHospital,
			codigoGrupo, orderByComparator);
	}

	/**
	* Removes all the hospitals where codigoGrupo = &#63; from the database.
	*
	* @param codigoGrupo the codigo grupo
	*/
	public static void removeByfindByCodigoGrupo(int codigoGrupo) {
		getPersistence().removeByfindByCodigoGrupo(codigoGrupo);
	}

	/**
	* Returns the number of hospitals where codigoGrupo = &#63;.
	*
	* @param codigoGrupo the codigo grupo
	* @return the number of matching hospitals
	*/
	public static int countByfindByCodigoGrupo(int codigoGrupo) {
		return getPersistence().countByfindByCodigoGrupo(codigoGrupo);
	}

	/**
	* Caches the hospital in the entity cache if it is enabled.
	*
	* @param hospital the hospital
	*/
	public static void cacheResult(Hospital hospital) {
		getPersistence().cacheResult(hospital);
	}

	/**
	* Caches the hospitals in the entity cache if it is enabled.
	*
	* @param hospitals the hospitals
	*/
	public static void cacheResult(List<Hospital> hospitals) {
		getPersistence().cacheResult(hospitals);
	}

	/**
	* Creates a new hospital with the primary key. Does not add the hospital to the database.
	*
	* @param idHospital the primary key for the new hospital
	* @return the new hospital
	*/
	public static Hospital create(int idHospital) {
		return getPersistence().create(idHospital);
	}

	/**
	* Removes the hospital with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idHospital the primary key of the hospital
	* @return the hospital that was removed
	* @throws NoSuchHospitalException if a hospital with the primary key could not be found
	*/
	public static Hospital remove(int idHospital)
		throws mx.com.allianz.service.hospital.exception.NoSuchHospitalException {
		return getPersistence().remove(idHospital);
	}

	public static Hospital updateImpl(Hospital hospital) {
		return getPersistence().updateImpl(hospital);
	}

	/**
	* Returns the hospital with the primary key or throws a {@link NoSuchHospitalException} if it could not be found.
	*
	* @param idHospital the primary key of the hospital
	* @return the hospital
	* @throws NoSuchHospitalException if a hospital with the primary key could not be found
	*/
	public static Hospital findByPrimaryKey(int idHospital)
		throws mx.com.allianz.service.hospital.exception.NoSuchHospitalException {
		return getPersistence().findByPrimaryKey(idHospital);
	}

	/**
	* Returns the hospital with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idHospital the primary key of the hospital
	* @return the hospital, or <code>null</code> if a hospital with the primary key could not be found
	*/
	public static Hospital fetchByPrimaryKey(int idHospital) {
		return getPersistence().fetchByPrimaryKey(idHospital);
	}

	public static java.util.Map<java.io.Serializable, Hospital> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the hospitals.
	*
	* @return the hospitals
	*/
	public static List<Hospital> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the hospitals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @return the range of hospitals
	*/
	public static List<Hospital> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the hospitals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of hospitals
	*/
	public static List<Hospital> findAll(int start, int end,
		OrderByComparator<Hospital> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the hospitals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of hospitals
	*/
	public static List<Hospital> findAll(int start, int end,
		OrderByComparator<Hospital> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the hospitals from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of hospitals.
	*
	* @return the number of hospitals
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static HospitalPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<HospitalPersistence, HospitalPersistence> _serviceTracker =
		ServiceTrackerFactory.open(HospitalPersistence.class);
}