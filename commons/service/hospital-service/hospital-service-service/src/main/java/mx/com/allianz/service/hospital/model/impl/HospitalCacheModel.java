/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.hospital.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.hospital.model.Hospital;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Hospital in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Hospital
 * @generated
 */
@ProviderType
public class HospitalCacheModel implements CacheModel<Hospital>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof HospitalCacheModel)) {
			return false;
		}

		HospitalCacheModel hospitalCacheModel = (HospitalCacheModel)obj;

		if (idHospital == hospitalCacheModel.idHospital) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, idHospital);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{idHospital=");
		sb.append(idHospital);
		sb.append(", codigoGrupo=");
		sb.append(codigoGrupo);
		sb.append(", codigoSubGrupo=");
		sb.append(codigoSubGrupo);
		sb.append(", descripcionHospital=");
		sb.append(descripcionHospital);
		sb.append(", codigoEstado=");
		sb.append(codigoEstado);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Hospital toEntityModel() {
		HospitalImpl hospitalImpl = new HospitalImpl();

		hospitalImpl.setIdHospital(idHospital);
		hospitalImpl.setCodigoGrupo(codigoGrupo);
		hospitalImpl.setCodigoSubGrupo(codigoSubGrupo);

		if (descripcionHospital == null) {
			hospitalImpl.setDescripcionHospital(StringPool.BLANK);
		}
		else {
			hospitalImpl.setDescripcionHospital(descripcionHospital);
		}

		hospitalImpl.setCodigoEstado(codigoEstado);

		hospitalImpl.resetOriginalValues();

		return hospitalImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idHospital = objectInput.readInt();

		codigoGrupo = objectInput.readInt();

		codigoSubGrupo = objectInput.readInt();
		descripcionHospital = objectInput.readUTF();

		codigoEstado = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(idHospital);

		objectOutput.writeInt(codigoGrupo);

		objectOutput.writeInt(codigoSubGrupo);

		if (descripcionHospital == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcionHospital);
		}

		objectOutput.writeInt(codigoEstado);
	}

	public int idHospital;
	public int codigoGrupo;
	public int codigoSubGrupo;
	public String descripcionHospital;
	public int codigoEstado;
}