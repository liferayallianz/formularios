/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.hospital.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.hospital.exception.NoSuchHospitalException;
import mx.com.allianz.service.hospital.model.Hospital;
import mx.com.allianz.service.hospital.model.impl.HospitalImpl;
import mx.com.allianz.service.hospital.model.impl.HospitalModelImpl;
import mx.com.allianz.service.hospital.service.persistence.HospitalPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the hospital service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see HospitalPersistence
 * @see mx.com.allianz.service.hospital.service.persistence.HospitalUtil
 * @generated
 */
@ProviderType
public class HospitalPersistenceImpl extends BasePersistenceImpl<Hospital>
	implements HospitalPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link HospitalUtil} to access the hospital persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = HospitalImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(HospitalModelImpl.ENTITY_CACHE_ENABLED,
			HospitalModelImpl.FINDER_CACHE_ENABLED, HospitalImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(HospitalModelImpl.ENTITY_CACHE_ENABLED,
			HospitalModelImpl.FINDER_CACHE_ENABLED, HospitalImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(HospitalModelImpl.ENTITY_CACHE_ENABLED,
			HospitalModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYCODIGOGRUPO =
		new FinderPath(HospitalModelImpl.ENTITY_CACHE_ENABLED,
			HospitalModelImpl.FINDER_CACHE_ENABLED, HospitalImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByfindByCodigoGrupo",
			new String[] {
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYCODIGOGRUPO =
		new FinderPath(HospitalModelImpl.ENTITY_CACHE_ENABLED,
			HospitalModelImpl.FINDER_CACHE_ENABLED, HospitalImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByCodigoGrupo",
			new String[] { Integer.class.getName() },
			HospitalModelImpl.CODIGOGRUPO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYCODIGOGRUPO = new FinderPath(HospitalModelImpl.ENTITY_CACHE_ENABLED,
			HospitalModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByCodigoGrupo", new String[] { Integer.class.getName() });

	/**
	 * Returns all the hospitals where codigoGrupo = &#63;.
	 *
	 * @param codigoGrupo the codigo grupo
	 * @return the matching hospitals
	 */
	@Override
	public List<Hospital> findByfindByCodigoGrupo(int codigoGrupo) {
		return findByfindByCodigoGrupo(codigoGrupo, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the hospitals where codigoGrupo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoGrupo the codigo grupo
	 * @param start the lower bound of the range of hospitals
	 * @param end the upper bound of the range of hospitals (not inclusive)
	 * @return the range of matching hospitals
	 */
	@Override
	public List<Hospital> findByfindByCodigoGrupo(int codigoGrupo, int start,
		int end) {
		return findByfindByCodigoGrupo(codigoGrupo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the hospitals where codigoGrupo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoGrupo the codigo grupo
	 * @param start the lower bound of the range of hospitals
	 * @param end the upper bound of the range of hospitals (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching hospitals
	 */
	@Override
	public List<Hospital> findByfindByCodigoGrupo(int codigoGrupo, int start,
		int end, OrderByComparator<Hospital> orderByComparator) {
		return findByfindByCodigoGrupo(codigoGrupo, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the hospitals where codigoGrupo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoGrupo the codigo grupo
	 * @param start the lower bound of the range of hospitals
	 * @param end the upper bound of the range of hospitals (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching hospitals
	 */
	@Override
	public List<Hospital> findByfindByCodigoGrupo(int codigoGrupo, int start,
		int end, OrderByComparator<Hospital> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYCODIGOGRUPO;
			finderArgs = new Object[] { codigoGrupo };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYCODIGOGRUPO;
			finderArgs = new Object[] { codigoGrupo, start, end, orderByComparator };
		}

		List<Hospital> list = null;

		if (retrieveFromCache) {
			list = (List<Hospital>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Hospital hospital : list) {
					if ((codigoGrupo != hospital.getCodigoGrupo())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_HOSPITAL_WHERE);

			query.append(_FINDER_COLUMN_FINDBYCODIGOGRUPO_CODIGOGRUPO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(HospitalModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(codigoGrupo);

				if (!pagination) {
					list = (List<Hospital>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Hospital>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first hospital in the ordered set where codigoGrupo = &#63;.
	 *
	 * @param codigoGrupo the codigo grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching hospital
	 * @throws NoSuchHospitalException if a matching hospital could not be found
	 */
	@Override
	public Hospital findByfindByCodigoGrupo_First(int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator)
		throws NoSuchHospitalException {
		Hospital hospital = fetchByfindByCodigoGrupo_First(codigoGrupo,
				orderByComparator);

		if (hospital != null) {
			return hospital;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoGrupo=");
		msg.append(codigoGrupo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchHospitalException(msg.toString());
	}

	/**
	 * Returns the first hospital in the ordered set where codigoGrupo = &#63;.
	 *
	 * @param codigoGrupo the codigo grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching hospital, or <code>null</code> if a matching hospital could not be found
	 */
	@Override
	public Hospital fetchByfindByCodigoGrupo_First(int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator) {
		List<Hospital> list = findByfindByCodigoGrupo(codigoGrupo, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last hospital in the ordered set where codigoGrupo = &#63;.
	 *
	 * @param codigoGrupo the codigo grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching hospital
	 * @throws NoSuchHospitalException if a matching hospital could not be found
	 */
	@Override
	public Hospital findByfindByCodigoGrupo_Last(int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator)
		throws NoSuchHospitalException {
		Hospital hospital = fetchByfindByCodigoGrupo_Last(codigoGrupo,
				orderByComparator);

		if (hospital != null) {
			return hospital;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoGrupo=");
		msg.append(codigoGrupo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchHospitalException(msg.toString());
	}

	/**
	 * Returns the last hospital in the ordered set where codigoGrupo = &#63;.
	 *
	 * @param codigoGrupo the codigo grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching hospital, or <code>null</code> if a matching hospital could not be found
	 */
	@Override
	public Hospital fetchByfindByCodigoGrupo_Last(int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator) {
		int count = countByfindByCodigoGrupo(codigoGrupo);

		if (count == 0) {
			return null;
		}

		List<Hospital> list = findByfindByCodigoGrupo(codigoGrupo, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the hospitals before and after the current hospital in the ordered set where codigoGrupo = &#63;.
	 *
	 * @param idHospital the primary key of the current hospital
	 * @param codigoGrupo the codigo grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next hospital
	 * @throws NoSuchHospitalException if a hospital with the primary key could not be found
	 */
	@Override
	public Hospital[] findByfindByCodigoGrupo_PrevAndNext(int idHospital,
		int codigoGrupo, OrderByComparator<Hospital> orderByComparator)
		throws NoSuchHospitalException {
		Hospital hospital = findByPrimaryKey(idHospital);

		Session session = null;

		try {
			session = openSession();

			Hospital[] array = new HospitalImpl[3];

			array[0] = getByfindByCodigoGrupo_PrevAndNext(session, hospital,
					codigoGrupo, orderByComparator, true);

			array[1] = hospital;

			array[2] = getByfindByCodigoGrupo_PrevAndNext(session, hospital,
					codigoGrupo, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Hospital getByfindByCodigoGrupo_PrevAndNext(Session session,
		Hospital hospital, int codigoGrupo,
		OrderByComparator<Hospital> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_HOSPITAL_WHERE);

		query.append(_FINDER_COLUMN_FINDBYCODIGOGRUPO_CODIGOGRUPO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(HospitalModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(codigoGrupo);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(hospital);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Hospital> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the hospitals where codigoGrupo = &#63; from the database.
	 *
	 * @param codigoGrupo the codigo grupo
	 */
	@Override
	public void removeByfindByCodigoGrupo(int codigoGrupo) {
		for (Hospital hospital : findByfindByCodigoGrupo(codigoGrupo,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(hospital);
		}
	}

	/**
	 * Returns the number of hospitals where codigoGrupo = &#63;.
	 *
	 * @param codigoGrupo the codigo grupo
	 * @return the number of matching hospitals
	 */
	@Override
	public int countByfindByCodigoGrupo(int codigoGrupo) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYCODIGOGRUPO;

		Object[] finderArgs = new Object[] { codigoGrupo };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_HOSPITAL_WHERE);

			query.append(_FINDER_COLUMN_FINDBYCODIGOGRUPO_CODIGOGRUPO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(codigoGrupo);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYCODIGOGRUPO_CODIGOGRUPO_2 = "hospital.codigoGrupo = ?";

	public HospitalPersistenceImpl() {
		setModelClass(Hospital.class);
	}

	/**
	 * Caches the hospital in the entity cache if it is enabled.
	 *
	 * @param hospital the hospital
	 */
	@Override
	public void cacheResult(Hospital hospital) {
		entityCache.putResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
			HospitalImpl.class, hospital.getPrimaryKey(), hospital);

		hospital.resetOriginalValues();
	}

	/**
	 * Caches the hospitals in the entity cache if it is enabled.
	 *
	 * @param hospitals the hospitals
	 */
	@Override
	public void cacheResult(List<Hospital> hospitals) {
		for (Hospital hospital : hospitals) {
			if (entityCache.getResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
						HospitalImpl.class, hospital.getPrimaryKey()) == null) {
				cacheResult(hospital);
			}
			else {
				hospital.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all hospitals.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(HospitalImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the hospital.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Hospital hospital) {
		entityCache.removeResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
			HospitalImpl.class, hospital.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Hospital> hospitals) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Hospital hospital : hospitals) {
			entityCache.removeResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
				HospitalImpl.class, hospital.getPrimaryKey());
		}
	}

	/**
	 * Creates a new hospital with the primary key. Does not add the hospital to the database.
	 *
	 * @param idHospital the primary key for the new hospital
	 * @return the new hospital
	 */
	@Override
	public Hospital create(int idHospital) {
		Hospital hospital = new HospitalImpl();

		hospital.setNew(true);
		hospital.setPrimaryKey(idHospital);

		return hospital;
	}

	/**
	 * Removes the hospital with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idHospital the primary key of the hospital
	 * @return the hospital that was removed
	 * @throws NoSuchHospitalException if a hospital with the primary key could not be found
	 */
	@Override
	public Hospital remove(int idHospital) throws NoSuchHospitalException {
		return remove((Serializable)idHospital);
	}

	/**
	 * Removes the hospital with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the hospital
	 * @return the hospital that was removed
	 * @throws NoSuchHospitalException if a hospital with the primary key could not be found
	 */
	@Override
	public Hospital remove(Serializable primaryKey)
		throws NoSuchHospitalException {
		Session session = null;

		try {
			session = openSession();

			Hospital hospital = (Hospital)session.get(HospitalImpl.class,
					primaryKey);

			if (hospital == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchHospitalException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(hospital);
		}
		catch (NoSuchHospitalException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Hospital removeImpl(Hospital hospital) {
		hospital = toUnwrappedModel(hospital);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(hospital)) {
				hospital = (Hospital)session.get(HospitalImpl.class,
						hospital.getPrimaryKeyObj());
			}

			if (hospital != null) {
				session.delete(hospital);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (hospital != null) {
			clearCache(hospital);
		}

		return hospital;
	}

	@Override
	public Hospital updateImpl(Hospital hospital) {
		hospital = toUnwrappedModel(hospital);

		boolean isNew = hospital.isNew();

		HospitalModelImpl hospitalModelImpl = (HospitalModelImpl)hospital;

		Session session = null;

		try {
			session = openSession();

			if (hospital.isNew()) {
				session.save(hospital);

				hospital.setNew(false);
			}
			else {
				hospital = (Hospital)session.merge(hospital);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !HospitalModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((hospitalModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYCODIGOGRUPO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						hospitalModelImpl.getOriginalCodigoGrupo()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYCODIGOGRUPO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYCODIGOGRUPO,
					args);

				args = new Object[] { hospitalModelImpl.getCodigoGrupo() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYCODIGOGRUPO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYCODIGOGRUPO,
					args);
			}
		}

		entityCache.putResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
			HospitalImpl.class, hospital.getPrimaryKey(), hospital, false);

		hospital.resetOriginalValues();

		return hospital;
	}

	protected Hospital toUnwrappedModel(Hospital hospital) {
		if (hospital instanceof HospitalImpl) {
			return hospital;
		}

		HospitalImpl hospitalImpl = new HospitalImpl();

		hospitalImpl.setNew(hospital.isNew());
		hospitalImpl.setPrimaryKey(hospital.getPrimaryKey());

		hospitalImpl.setIdHospital(hospital.getIdHospital());
		hospitalImpl.setCodigoGrupo(hospital.getCodigoGrupo());
		hospitalImpl.setCodigoSubGrupo(hospital.getCodigoSubGrupo());
		hospitalImpl.setDescripcionHospital(hospital.getDescripcionHospital());
		hospitalImpl.setCodigoEstado(hospital.getCodigoEstado());

		return hospitalImpl;
	}

	/**
	 * Returns the hospital with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the hospital
	 * @return the hospital
	 * @throws NoSuchHospitalException if a hospital with the primary key could not be found
	 */
	@Override
	public Hospital findByPrimaryKey(Serializable primaryKey)
		throws NoSuchHospitalException {
		Hospital hospital = fetchByPrimaryKey(primaryKey);

		if (hospital == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchHospitalException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return hospital;
	}

	/**
	 * Returns the hospital with the primary key or throws a {@link NoSuchHospitalException} if it could not be found.
	 *
	 * @param idHospital the primary key of the hospital
	 * @return the hospital
	 * @throws NoSuchHospitalException if a hospital with the primary key could not be found
	 */
	@Override
	public Hospital findByPrimaryKey(int idHospital)
		throws NoSuchHospitalException {
		return findByPrimaryKey((Serializable)idHospital);
	}

	/**
	 * Returns the hospital with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the hospital
	 * @return the hospital, or <code>null</code> if a hospital with the primary key could not be found
	 */
	@Override
	public Hospital fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
				HospitalImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Hospital hospital = (Hospital)serializable;

		if (hospital == null) {
			Session session = null;

			try {
				session = openSession();

				hospital = (Hospital)session.get(HospitalImpl.class, primaryKey);

				if (hospital != null) {
					cacheResult(hospital);
				}
				else {
					entityCache.putResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
						HospitalImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
					HospitalImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return hospital;
	}

	/**
	 * Returns the hospital with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idHospital the primary key of the hospital
	 * @return the hospital, or <code>null</code> if a hospital with the primary key could not be found
	 */
	@Override
	public Hospital fetchByPrimaryKey(int idHospital) {
		return fetchByPrimaryKey((Serializable)idHospital);
	}

	@Override
	public Map<Serializable, Hospital> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Hospital> map = new HashMap<Serializable, Hospital>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Hospital hospital = fetchByPrimaryKey(primaryKey);

			if (hospital != null) {
				map.put(primaryKey, hospital);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
					HospitalImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Hospital)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_HOSPITAL_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Hospital hospital : (List<Hospital>)q.list()) {
				map.put(hospital.getPrimaryKeyObj(), hospital);

				cacheResult(hospital);

				uncachedPrimaryKeys.remove(hospital.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(HospitalModelImpl.ENTITY_CACHE_ENABLED,
					HospitalImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the hospitals.
	 *
	 * @return the hospitals
	 */
	@Override
	public List<Hospital> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the hospitals.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of hospitals
	 * @param end the upper bound of the range of hospitals (not inclusive)
	 * @return the range of hospitals
	 */
	@Override
	public List<Hospital> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the hospitals.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of hospitals
	 * @param end the upper bound of the range of hospitals (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of hospitals
	 */
	@Override
	public List<Hospital> findAll(int start, int end,
		OrderByComparator<Hospital> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the hospitals.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of hospitals
	 * @param end the upper bound of the range of hospitals (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of hospitals
	 */
	@Override
	public List<Hospital> findAll(int start, int end,
		OrderByComparator<Hospital> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Hospital> list = null;

		if (retrieveFromCache) {
			list = (List<Hospital>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_HOSPITAL);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_HOSPITAL;

				if (pagination) {
					sql = sql.concat(HospitalModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Hospital>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Hospital>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the hospitals from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Hospital hospital : findAll()) {
			remove(hospital);
		}
	}

	/**
	 * Returns the number of hospitals.
	 *
	 * @return the number of hospitals
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_HOSPITAL);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return HospitalModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the hospital persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(HospitalImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_HOSPITAL = "SELECT hospital FROM Hospital hospital";
	private static final String _SQL_SELECT_HOSPITAL_WHERE_PKS_IN = "SELECT hospital FROM Hospital hospital WHERE idHospital IN (";
	private static final String _SQL_SELECT_HOSPITAL_WHERE = "SELECT hospital FROM Hospital hospital WHERE ";
	private static final String _SQL_COUNT_HOSPITAL = "SELECT COUNT(hospital) FROM Hospital hospital";
	private static final String _SQL_COUNT_HOSPITAL_WHERE = "SELECT COUNT(hospital) FROM Hospital hospital WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "hospital.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Hospital exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Hospital exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(HospitalPersistenceImpl.class);
}