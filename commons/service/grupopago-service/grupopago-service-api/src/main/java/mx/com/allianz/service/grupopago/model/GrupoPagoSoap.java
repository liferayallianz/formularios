/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.grupopago.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class GrupoPagoSoap implements Serializable {
	public static GrupoPagoSoap toSoapModel(GrupoPago model) {
		GrupoPagoSoap soapModel = new GrupoPagoSoap();

		soapModel.setCodigoGrupoPagos(model.getCodigoGrupoPagos());
		soapModel.setDescripcionGrupo(model.getDescripcionGrupo());
		soapModel.setSistemaDirectorioGrupoPago(model.getSistemaDirectorioGrupoPago());

		return soapModel;
	}

	public static GrupoPagoSoap[] toSoapModels(GrupoPago[] models) {
		GrupoPagoSoap[] soapModels = new GrupoPagoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static GrupoPagoSoap[][] toSoapModels(GrupoPago[][] models) {
		GrupoPagoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new GrupoPagoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new GrupoPagoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static GrupoPagoSoap[] toSoapModels(List<GrupoPago> models) {
		List<GrupoPagoSoap> soapModels = new ArrayList<GrupoPagoSoap>(models.size());

		for (GrupoPago model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new GrupoPagoSoap[soapModels.size()]);
	}

	public GrupoPagoSoap() {
	}

	public int getPrimaryKey() {
		return _codigoGrupoPagos;
	}

	public void setPrimaryKey(int pk) {
		setCodigoGrupoPagos(pk);
	}

	public int getCodigoGrupoPagos() {
		return _codigoGrupoPagos;
	}

	public void setCodigoGrupoPagos(int codigoGrupoPagos) {
		_codigoGrupoPagos = codigoGrupoPagos;
	}

	public String getDescripcionGrupo() {
		return _descripcionGrupo;
	}

	public void setDescripcionGrupo(String descripcionGrupo) {
		_descripcionGrupo = descripcionGrupo;
	}

	public String getSistemaDirectorioGrupoPago() {
		return _sistemaDirectorioGrupoPago;
	}

	public void setSistemaDirectorioGrupoPago(String sistemaDirectorioGrupoPago) {
		_sistemaDirectorioGrupoPago = sistemaDirectorioGrupoPago;
	}

	private int _codigoGrupoPagos;
	private String _descripcionGrupo;
	private String _sistemaDirectorioGrupoPago;
}