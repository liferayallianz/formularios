/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.grupopago.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the GrupoPago service. Represents a row in the &quot;PTL_CAT_GRUPOS_PAGO_TR&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see GrupoPagoModel
 * @see mx.com.allianz.service.grupopago.model.impl.GrupoPagoImpl
 * @see mx.com.allianz.service.grupopago.model.impl.GrupoPagoModelImpl
 * @generated
 */
@ImplementationClassName("mx.com.allianz.service.grupopago.model.impl.GrupoPagoImpl")
@ProviderType
public interface GrupoPago extends GrupoPagoModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link mx.com.allianz.service.grupopago.model.impl.GrupoPagoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<GrupoPago, Integer> CODIGO_GRUPO_PAGOS_ACCESSOR =
		new Accessor<GrupoPago, Integer>() {
			@Override
			public Integer get(GrupoPago grupoPago) {
				return grupoPago.getCodigoGrupoPagos();
			}

			@Override
			public Class<Integer> getAttributeClass() {
				return Integer.class;
			}

			@Override
			public Class<GrupoPago> getTypeClass() {
				return GrupoPago.class;
			}
		};
}