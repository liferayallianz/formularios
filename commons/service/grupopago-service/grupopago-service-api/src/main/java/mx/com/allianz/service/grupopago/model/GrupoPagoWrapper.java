/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.grupopago.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link GrupoPago}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GrupoPago
 * @generated
 */
@ProviderType
public class GrupoPagoWrapper implements GrupoPago, ModelWrapper<GrupoPago> {
	public GrupoPagoWrapper(GrupoPago grupoPago) {
		_grupoPago = grupoPago;
	}

	@Override
	public Class<?> getModelClass() {
		return GrupoPago.class;
	}

	@Override
	public String getModelClassName() {
		return GrupoPago.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codigoGrupoPagos", getCodigoGrupoPagos());
		attributes.put("descripcionGrupo", getDescripcionGrupo());
		attributes.put("sistemaDirectorioGrupoPago",
			getSistemaDirectorioGrupoPago());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer codigoGrupoPagos = (Integer)attributes.get("codigoGrupoPagos");

		if (codigoGrupoPagos != null) {
			setCodigoGrupoPagos(codigoGrupoPagos);
		}

		String descripcionGrupo = (String)attributes.get("descripcionGrupo");

		if (descripcionGrupo != null) {
			setDescripcionGrupo(descripcionGrupo);
		}

		String sistemaDirectorioGrupoPago = (String)attributes.get(
				"sistemaDirectorioGrupoPago");

		if (sistemaDirectorioGrupoPago != null) {
			setSistemaDirectorioGrupoPago(sistemaDirectorioGrupoPago);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _grupoPago.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _grupoPago.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _grupoPago.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _grupoPago.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<GrupoPago> toCacheModel() {
		return _grupoPago.toCacheModel();
	}

	@Override
	public int compareTo(GrupoPago grupoPago) {
		return _grupoPago.compareTo(grupoPago);
	}

	/**
	* Returns the codigo grupo pagos of this grupo pago.
	*
	* @return the codigo grupo pagos of this grupo pago
	*/
	@Override
	public int getCodigoGrupoPagos() {
		return _grupoPago.getCodigoGrupoPagos();
	}

	/**
	* Returns the primary key of this grupo pago.
	*
	* @return the primary key of this grupo pago
	*/
	@Override
	public int getPrimaryKey() {
		return _grupoPago.getPrimaryKey();
	}

	@Override
	public int hashCode() {
		return _grupoPago.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _grupoPago.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new GrupoPagoWrapper((GrupoPago)_grupoPago.clone());
	}

	/**
	* Returns the descripcion grupo of this grupo pago.
	*
	* @return the descripcion grupo of this grupo pago
	*/
	@Override
	public java.lang.String getDescripcionGrupo() {
		return _grupoPago.getDescripcionGrupo();
	}

	/**
	* Returns the sistema directorio grupo pago of this grupo pago.
	*
	* @return the sistema directorio grupo pago of this grupo pago
	*/
	@Override
	public java.lang.String getSistemaDirectorioGrupoPago() {
		return _grupoPago.getSistemaDirectorioGrupoPago();
	}

	@Override
	public java.lang.String toString() {
		return _grupoPago.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _grupoPago.toXmlString();
	}

	@Override
	public GrupoPago toEscapedModel() {
		return new GrupoPagoWrapper(_grupoPago.toEscapedModel());
	}

	@Override
	public GrupoPago toUnescapedModel() {
		return new GrupoPagoWrapper(_grupoPago.toUnescapedModel());
	}

	@Override
	public void persist() {
		_grupoPago.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_grupoPago.setCachedModel(cachedModel);
	}

	/**
	* Sets the codigo grupo pagos of this grupo pago.
	*
	* @param codigoGrupoPagos the codigo grupo pagos of this grupo pago
	*/
	@Override
	public void setCodigoGrupoPagos(int codigoGrupoPagos) {
		_grupoPago.setCodigoGrupoPagos(codigoGrupoPagos);
	}

	/**
	* Sets the descripcion grupo of this grupo pago.
	*
	* @param descripcionGrupo the descripcion grupo of this grupo pago
	*/
	@Override
	public void setDescripcionGrupo(java.lang.String descripcionGrupo) {
		_grupoPago.setDescripcionGrupo(descripcionGrupo);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_grupoPago.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_grupoPago.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_grupoPago.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_grupoPago.setNew(n);
	}

	/**
	* Sets the primary key of this grupo pago.
	*
	* @param primaryKey the primary key of this grupo pago
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_grupoPago.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_grupoPago.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the sistema directorio grupo pago of this grupo pago.
	*
	* @param sistemaDirectorioGrupoPago the sistema directorio grupo pago of this grupo pago
	*/
	@Override
	public void setSistemaDirectorioGrupoPago(
		java.lang.String sistemaDirectorioGrupoPago) {
		_grupoPago.setSistemaDirectorioGrupoPago(sistemaDirectorioGrupoPago);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof GrupoPagoWrapper)) {
			return false;
		}

		GrupoPagoWrapper grupoPagoWrapper = (GrupoPagoWrapper)obj;

		if (Objects.equals(_grupoPago, grupoPagoWrapper._grupoPago)) {
			return true;
		}

		return false;
	}

	@Override
	public GrupoPago getWrappedModel() {
		return _grupoPago;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _grupoPago.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _grupoPago.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_grupoPago.resetOriginalValues();
	}

	private final GrupoPago _grupoPago;
}