/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.grupopago.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.grupopago.exception.NoSuchGrupoPagoException;
import mx.com.allianz.service.grupopago.model.GrupoPago;
import mx.com.allianz.service.grupopago.model.impl.GrupoPagoImpl;
import mx.com.allianz.service.grupopago.model.impl.GrupoPagoModelImpl;
import mx.com.allianz.service.grupopago.service.persistence.GrupoPagoPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the grupo pago service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GrupoPagoPersistence
 * @see mx.com.allianz.service.grupopago.service.persistence.GrupoPagoUtil
 * @generated
 */
@ProviderType
public class GrupoPagoPersistenceImpl extends BasePersistenceImpl<GrupoPago>
	implements GrupoPagoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link GrupoPagoUtil} to access the grupo pago persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = GrupoPagoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
			GrupoPagoModelImpl.FINDER_CACHE_ENABLED, GrupoPagoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
			GrupoPagoModelImpl.FINDER_CACHE_ENABLED, GrupoPagoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
			GrupoPagoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCIONGRUPO =
		new FinderPath(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
			GrupoPagoModelImpl.FINDER_CACHE_ENABLED, GrupoPagoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByfindByDescripcionGrupo",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCIONGRUPO =
		new FinderPath(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
			GrupoPagoModelImpl.FINDER_CACHE_ENABLED, GrupoPagoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByDescripcionGrupo",
			new String[] { String.class.getName() },
			GrupoPagoModelImpl.DESCRIPCIONGRUPO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYDESCRIPCIONGRUPO = new FinderPath(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
			GrupoPagoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByDescripcionGrupo",
			new String[] { String.class.getName() });

	/**
	 * Returns all the grupo pagos where descripcionGrupo = &#63;.
	 *
	 * @param descripcionGrupo the descripcion grupo
	 * @return the matching grupo pagos
	 */
	@Override
	public List<GrupoPago> findByfindByDescripcionGrupo(String descripcionGrupo) {
		return findByfindByDescripcionGrupo(descripcionGrupo,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the grupo pagos where descripcionGrupo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcionGrupo the descripcion grupo
	 * @param start the lower bound of the range of grupo pagos
	 * @param end the upper bound of the range of grupo pagos (not inclusive)
	 * @return the range of matching grupo pagos
	 */
	@Override
	public List<GrupoPago> findByfindByDescripcionGrupo(
		String descripcionGrupo, int start, int end) {
		return findByfindByDescripcionGrupo(descripcionGrupo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the grupo pagos where descripcionGrupo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcionGrupo the descripcion grupo
	 * @param start the lower bound of the range of grupo pagos
	 * @param end the upper bound of the range of grupo pagos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching grupo pagos
	 */
	@Override
	public List<GrupoPago> findByfindByDescripcionGrupo(
		String descripcionGrupo, int start, int end,
		OrderByComparator<GrupoPago> orderByComparator) {
		return findByfindByDescripcionGrupo(descripcionGrupo, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the grupo pagos where descripcionGrupo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcionGrupo the descripcion grupo
	 * @param start the lower bound of the range of grupo pagos
	 * @param end the upper bound of the range of grupo pagos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching grupo pagos
	 */
	@Override
	public List<GrupoPago> findByfindByDescripcionGrupo(
		String descripcionGrupo, int start, int end,
		OrderByComparator<GrupoPago> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCIONGRUPO;
			finderArgs = new Object[] { descripcionGrupo };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCIONGRUPO;
			finderArgs = new Object[] {
					descripcionGrupo,
					
					start, end, orderByComparator
				};
		}

		List<GrupoPago> list = null;

		if (retrieveFromCache) {
			list = (List<GrupoPago>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (GrupoPago grupoPago : list) {
					if (!Objects.equals(descripcionGrupo,
								grupoPago.getDescripcionGrupo())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_GRUPOPAGO_WHERE);

			boolean bindDescripcionGrupo = false;

			if (descripcionGrupo == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_1);
			}
			else if (descripcionGrupo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_3);
			}
			else {
				bindDescripcionGrupo = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(GrupoPagoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcionGrupo) {
					qPos.add(descripcionGrupo);
				}

				if (!pagination) {
					list = (List<GrupoPago>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<GrupoPago>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first grupo pago in the ordered set where descripcionGrupo = &#63;.
	 *
	 * @param descripcionGrupo the descripcion grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching grupo pago
	 * @throws NoSuchGrupoPagoException if a matching grupo pago could not be found
	 */
	@Override
	public GrupoPago findByfindByDescripcionGrupo_First(
		String descripcionGrupo, OrderByComparator<GrupoPago> orderByComparator)
		throws NoSuchGrupoPagoException {
		GrupoPago grupoPago = fetchByfindByDescripcionGrupo_First(descripcionGrupo,
				orderByComparator);

		if (grupoPago != null) {
			return grupoPago;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcionGrupo=");
		msg.append(descripcionGrupo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchGrupoPagoException(msg.toString());
	}

	/**
	 * Returns the first grupo pago in the ordered set where descripcionGrupo = &#63;.
	 *
	 * @param descripcionGrupo the descripcion grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching grupo pago, or <code>null</code> if a matching grupo pago could not be found
	 */
	@Override
	public GrupoPago fetchByfindByDescripcionGrupo_First(
		String descripcionGrupo, OrderByComparator<GrupoPago> orderByComparator) {
		List<GrupoPago> list = findByfindByDescripcionGrupo(descripcionGrupo,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last grupo pago in the ordered set where descripcionGrupo = &#63;.
	 *
	 * @param descripcionGrupo the descripcion grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching grupo pago
	 * @throws NoSuchGrupoPagoException if a matching grupo pago could not be found
	 */
	@Override
	public GrupoPago findByfindByDescripcionGrupo_Last(
		String descripcionGrupo, OrderByComparator<GrupoPago> orderByComparator)
		throws NoSuchGrupoPagoException {
		GrupoPago grupoPago = fetchByfindByDescripcionGrupo_Last(descripcionGrupo,
				orderByComparator);

		if (grupoPago != null) {
			return grupoPago;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcionGrupo=");
		msg.append(descripcionGrupo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchGrupoPagoException(msg.toString());
	}

	/**
	 * Returns the last grupo pago in the ordered set where descripcionGrupo = &#63;.
	 *
	 * @param descripcionGrupo the descripcion grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching grupo pago, or <code>null</code> if a matching grupo pago could not be found
	 */
	@Override
	public GrupoPago fetchByfindByDescripcionGrupo_Last(
		String descripcionGrupo, OrderByComparator<GrupoPago> orderByComparator) {
		int count = countByfindByDescripcionGrupo(descripcionGrupo);

		if (count == 0) {
			return null;
		}

		List<GrupoPago> list = findByfindByDescripcionGrupo(descripcionGrupo,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the grupo pagos before and after the current grupo pago in the ordered set where descripcionGrupo = &#63;.
	 *
	 * @param codigoGrupoPagos the primary key of the current grupo pago
	 * @param descripcionGrupo the descripcion grupo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next grupo pago
	 * @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	 */
	@Override
	public GrupoPago[] findByfindByDescripcionGrupo_PrevAndNext(
		int codigoGrupoPagos, String descripcionGrupo,
		OrderByComparator<GrupoPago> orderByComparator)
		throws NoSuchGrupoPagoException {
		GrupoPago grupoPago = findByPrimaryKey(codigoGrupoPagos);

		Session session = null;

		try {
			session = openSession();

			GrupoPago[] array = new GrupoPagoImpl[3];

			array[0] = getByfindByDescripcionGrupo_PrevAndNext(session,
					grupoPago, descripcionGrupo, orderByComparator, true);

			array[1] = grupoPago;

			array[2] = getByfindByDescripcionGrupo_PrevAndNext(session,
					grupoPago, descripcionGrupo, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected GrupoPago getByfindByDescripcionGrupo_PrevAndNext(
		Session session, GrupoPago grupoPago, String descripcionGrupo,
		OrderByComparator<GrupoPago> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_GRUPOPAGO_WHERE);

		boolean bindDescripcionGrupo = false;

		if (descripcionGrupo == null) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_1);
		}
		else if (descripcionGrupo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_3);
		}
		else {
			bindDescripcionGrupo = true;

			query.append(_FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(GrupoPagoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDescripcionGrupo) {
			qPos.add(descripcionGrupo);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(grupoPago);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<GrupoPago> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the grupo pagos where descripcionGrupo = &#63; from the database.
	 *
	 * @param descripcionGrupo the descripcion grupo
	 */
	@Override
	public void removeByfindByDescripcionGrupo(String descripcionGrupo) {
		for (GrupoPago grupoPago : findByfindByDescripcionGrupo(
				descripcionGrupo, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(grupoPago);
		}
	}

	/**
	 * Returns the number of grupo pagos where descripcionGrupo = &#63;.
	 *
	 * @param descripcionGrupo the descripcion grupo
	 * @return the number of matching grupo pagos
	 */
	@Override
	public int countByfindByDescripcionGrupo(String descripcionGrupo) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYDESCRIPCIONGRUPO;

		Object[] finderArgs = new Object[] { descripcionGrupo };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_GRUPOPAGO_WHERE);

			boolean bindDescripcionGrupo = false;

			if (descripcionGrupo == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_1);
			}
			else if (descripcionGrupo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_3);
			}
			else {
				bindDescripcionGrupo = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcionGrupo) {
					qPos.add(descripcionGrupo);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_1 =
		"grupoPago.descripcionGrupo IS NULL";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_2 =
		"grupoPago.descripcionGrupo = ?";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCIONGRUPO_DESCRIPCIONGRUPO_3 =
		"(grupoPago.descripcionGrupo IS NULL OR grupoPago.descripcionGrupo = '')";

	public GrupoPagoPersistenceImpl() {
		setModelClass(GrupoPago.class);
	}

	/**
	 * Caches the grupo pago in the entity cache if it is enabled.
	 *
	 * @param grupoPago the grupo pago
	 */
	@Override
	public void cacheResult(GrupoPago grupoPago) {
		entityCache.putResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
			GrupoPagoImpl.class, grupoPago.getPrimaryKey(), grupoPago);

		grupoPago.resetOriginalValues();
	}

	/**
	 * Caches the grupo pagos in the entity cache if it is enabled.
	 *
	 * @param grupoPagos the grupo pagos
	 */
	@Override
	public void cacheResult(List<GrupoPago> grupoPagos) {
		for (GrupoPago grupoPago : grupoPagos) {
			if (entityCache.getResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
						GrupoPagoImpl.class, grupoPago.getPrimaryKey()) == null) {
				cacheResult(grupoPago);
			}
			else {
				grupoPago.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all grupo pagos.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(GrupoPagoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the grupo pago.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(GrupoPago grupoPago) {
		entityCache.removeResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
			GrupoPagoImpl.class, grupoPago.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<GrupoPago> grupoPagos) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (GrupoPago grupoPago : grupoPagos) {
			entityCache.removeResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
				GrupoPagoImpl.class, grupoPago.getPrimaryKey());
		}
	}

	/**
	 * Creates a new grupo pago with the primary key. Does not add the grupo pago to the database.
	 *
	 * @param codigoGrupoPagos the primary key for the new grupo pago
	 * @return the new grupo pago
	 */
	@Override
	public GrupoPago create(int codigoGrupoPagos) {
		GrupoPago grupoPago = new GrupoPagoImpl();

		grupoPago.setNew(true);
		grupoPago.setPrimaryKey(codigoGrupoPagos);

		return grupoPago;
	}

	/**
	 * Removes the grupo pago with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codigoGrupoPagos the primary key of the grupo pago
	 * @return the grupo pago that was removed
	 * @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	 */
	@Override
	public GrupoPago remove(int codigoGrupoPagos)
		throws NoSuchGrupoPagoException {
		return remove((Serializable)codigoGrupoPagos);
	}

	/**
	 * Removes the grupo pago with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the grupo pago
	 * @return the grupo pago that was removed
	 * @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	 */
	@Override
	public GrupoPago remove(Serializable primaryKey)
		throws NoSuchGrupoPagoException {
		Session session = null;

		try {
			session = openSession();

			GrupoPago grupoPago = (GrupoPago)session.get(GrupoPagoImpl.class,
					primaryKey);

			if (grupoPago == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchGrupoPagoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(grupoPago);
		}
		catch (NoSuchGrupoPagoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected GrupoPago removeImpl(GrupoPago grupoPago) {
		grupoPago = toUnwrappedModel(grupoPago);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(grupoPago)) {
				grupoPago = (GrupoPago)session.get(GrupoPagoImpl.class,
						grupoPago.getPrimaryKeyObj());
			}

			if (grupoPago != null) {
				session.delete(grupoPago);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (grupoPago != null) {
			clearCache(grupoPago);
		}

		return grupoPago;
	}

	@Override
	public GrupoPago updateImpl(GrupoPago grupoPago) {
		grupoPago = toUnwrappedModel(grupoPago);

		boolean isNew = grupoPago.isNew();

		GrupoPagoModelImpl grupoPagoModelImpl = (GrupoPagoModelImpl)grupoPago;

		Session session = null;

		try {
			session = openSession();

			if (grupoPago.isNew()) {
				session.save(grupoPago);

				grupoPago.setNew(false);
			}
			else {
				grupoPago = (GrupoPago)session.merge(grupoPago);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !GrupoPagoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((grupoPagoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCIONGRUPO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						grupoPagoModelImpl.getOriginalDescripcionGrupo()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCIONGRUPO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCIONGRUPO,
					args);

				args = new Object[] { grupoPagoModelImpl.getDescripcionGrupo() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCIONGRUPO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCIONGRUPO,
					args);
			}
		}

		entityCache.putResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
			GrupoPagoImpl.class, grupoPago.getPrimaryKey(), grupoPago, false);

		grupoPago.resetOriginalValues();

		return grupoPago;
	}

	protected GrupoPago toUnwrappedModel(GrupoPago grupoPago) {
		if (grupoPago instanceof GrupoPagoImpl) {
			return grupoPago;
		}

		GrupoPagoImpl grupoPagoImpl = new GrupoPagoImpl();

		grupoPagoImpl.setNew(grupoPago.isNew());
		grupoPagoImpl.setPrimaryKey(grupoPago.getPrimaryKey());

		grupoPagoImpl.setCodigoGrupoPagos(grupoPago.getCodigoGrupoPagos());
		grupoPagoImpl.setDescripcionGrupo(grupoPago.getDescripcionGrupo());
		grupoPagoImpl.setSistemaDirectorioGrupoPago(grupoPago.getSistemaDirectorioGrupoPago());

		return grupoPagoImpl;
	}

	/**
	 * Returns the grupo pago with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the grupo pago
	 * @return the grupo pago
	 * @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	 */
	@Override
	public GrupoPago findByPrimaryKey(Serializable primaryKey)
		throws NoSuchGrupoPagoException {
		GrupoPago grupoPago = fetchByPrimaryKey(primaryKey);

		if (grupoPago == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchGrupoPagoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return grupoPago;
	}

	/**
	 * Returns the grupo pago with the primary key or throws a {@link NoSuchGrupoPagoException} if it could not be found.
	 *
	 * @param codigoGrupoPagos the primary key of the grupo pago
	 * @return the grupo pago
	 * @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	 */
	@Override
	public GrupoPago findByPrimaryKey(int codigoGrupoPagos)
		throws NoSuchGrupoPagoException {
		return findByPrimaryKey((Serializable)codigoGrupoPagos);
	}

	/**
	 * Returns the grupo pago with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the grupo pago
	 * @return the grupo pago, or <code>null</code> if a grupo pago with the primary key could not be found
	 */
	@Override
	public GrupoPago fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
				GrupoPagoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		GrupoPago grupoPago = (GrupoPago)serializable;

		if (grupoPago == null) {
			Session session = null;

			try {
				session = openSession();

				grupoPago = (GrupoPago)session.get(GrupoPagoImpl.class,
						primaryKey);

				if (grupoPago != null) {
					cacheResult(grupoPago);
				}
				else {
					entityCache.putResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
						GrupoPagoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
					GrupoPagoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return grupoPago;
	}

	/**
	 * Returns the grupo pago with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param codigoGrupoPagos the primary key of the grupo pago
	 * @return the grupo pago, or <code>null</code> if a grupo pago with the primary key could not be found
	 */
	@Override
	public GrupoPago fetchByPrimaryKey(int codigoGrupoPagos) {
		return fetchByPrimaryKey((Serializable)codigoGrupoPagos);
	}

	@Override
	public Map<Serializable, GrupoPago> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, GrupoPago> map = new HashMap<Serializable, GrupoPago>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			GrupoPago grupoPago = fetchByPrimaryKey(primaryKey);

			if (grupoPago != null) {
				map.put(primaryKey, grupoPago);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
					GrupoPagoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (GrupoPago)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_GRUPOPAGO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (GrupoPago grupoPago : (List<GrupoPago>)q.list()) {
				map.put(grupoPago.getPrimaryKeyObj(), grupoPago);

				cacheResult(grupoPago);

				uncachedPrimaryKeys.remove(grupoPago.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(GrupoPagoModelImpl.ENTITY_CACHE_ENABLED,
					GrupoPagoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the grupo pagos.
	 *
	 * @return the grupo pagos
	 */
	@Override
	public List<GrupoPago> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the grupo pagos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of grupo pagos
	 * @param end the upper bound of the range of grupo pagos (not inclusive)
	 * @return the range of grupo pagos
	 */
	@Override
	public List<GrupoPago> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the grupo pagos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of grupo pagos
	 * @param end the upper bound of the range of grupo pagos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of grupo pagos
	 */
	@Override
	public List<GrupoPago> findAll(int start, int end,
		OrderByComparator<GrupoPago> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the grupo pagos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of grupo pagos
	 * @param end the upper bound of the range of grupo pagos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of grupo pagos
	 */
	@Override
	public List<GrupoPago> findAll(int start, int end,
		OrderByComparator<GrupoPago> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<GrupoPago> list = null;

		if (retrieveFromCache) {
			list = (List<GrupoPago>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_GRUPOPAGO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_GRUPOPAGO;

				if (pagination) {
					sql = sql.concat(GrupoPagoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<GrupoPago>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<GrupoPago>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the grupo pagos from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (GrupoPago grupoPago : findAll()) {
			remove(grupoPago);
		}
	}

	/**
	 * Returns the number of grupo pagos.
	 *
	 * @return the number of grupo pagos
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_GRUPOPAGO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return GrupoPagoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the grupo pago persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(GrupoPagoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_GRUPOPAGO = "SELECT grupoPago FROM GrupoPago grupoPago";
	private static final String _SQL_SELECT_GRUPOPAGO_WHERE_PKS_IN = "SELECT grupoPago FROM GrupoPago grupoPago WHERE codigoGrupoPagos IN (";
	private static final String _SQL_SELECT_GRUPOPAGO_WHERE = "SELECT grupoPago FROM GrupoPago grupoPago WHERE ";
	private static final String _SQL_COUNT_GRUPOPAGO = "SELECT COUNT(grupoPago) FROM GrupoPago grupoPago";
	private static final String _SQL_COUNT_GRUPOPAGO_WHERE = "SELECT COUNT(grupoPago) FROM GrupoPago grupoPago WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "grupoPago.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No GrupoPago exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No GrupoPago exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(GrupoPagoPersistenceImpl.class);
}