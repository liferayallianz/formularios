package mx.com.allianz.service.rest.grupopago;


import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.GrupoPagoDTO;
import mx.com.allianz.service.grupopago.service.GrupoPagoLocalServiceUtil;

/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio
 * Grupo Pagos que permite visualizar una lista de datos, 
 * filtra el orden de la b\u00fasqueda y muestra el ordenamiento 
 * conforme al filtro
 *
 * @author  TestingZone
 * @version 1.0
 * @since   2016-12-5 
 */
/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n (Modulo)
 */
@ApplicationPath("/catalogos.grupopago")
/*
 * @Component sirve para garantizar la seguridad del path
 */
@Component(
	immediate = true, property = {"jaxrs.application=true"},
	service = Application.class
)
public class GrupoPagoRest extends Application {

	public static final int CODIGO_PAIS_MEXICO=1;
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object)this);
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los Grupo Pagos del cat\u00e1logo
	 * 
	 * @return getAllGrupoPags()
	 */
	//@GET es el nombre del procesamiento de la petici\u00f3n
	@GET
	//@Path sirve para poner la ruta relativa donde la clase ser\u00e1 hospedada
	@Path("/getGrupopagos")
	//@Produces sirve para especificar el tipo de formato de respuesta
	@Produces({MediaType.APPLICATION_JSON})
	public List<GrupoPagoDTO> getGrupoPagos() {
		// Se regresan todas las ciudades
		return getAllGrupoPags();
	}
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los Grupo Pagos del cat\u00e1logo
	 * 
	 * @return getAllGrupoPags().stream()
	 */
	//@GET es el nombre del procesamiento de la petici\u00f3n
	@GET
	//@Path sirve para poner la ruta relativa donde la clase ser\u00e1
	@Path("/findGrupopagos")
	//@Produces sirve para especificar el tipo de formato de respuesta
	@Produces({MediaType.APPLICATION_JSON})
	public List<GrupoPagoDTO>findGrupoPagBy(
			//El valor @DefaultValue se utiliza para definir el valor por defecto para el par\u00e1metro matrixParam.
			//@QueryParam se utiliza para extraer los par\u00e1metros de consulta del "componente de consulta" de la URL de la solicitud.
			@DefaultValue("-1")@QueryParam("cod_GrupoPag") int codGrupoPag,
			@DefaultValue("")@QueryParam("stsDir_GrupoPag") String stsDirGrupoPag,
			@DefaultValue("")@QueryParam("descripGrupo") String descripGrupo){
		//stream() Devuelve un flujo secuencial considerando la colecci\u00f3n como su origen.
		//convierte la lista en stream
		return getAllGrupoPags().stream()
				// filtrado por c\u00f3digo del grupo de pago, si se env\u00eda -1 se ignora el filtro 
				.filter((grupoPag) -> codGrupoPag == -1 || grupoPag.getCodigoGrupoPagos() == codGrupoPag)
				// filtrado por sistema de directorio de grupo de pagos, si es vac\u00edo se ignora el filtro 
				.filter((grupoPag) -> stsDirGrupoPag.isEmpty() || grupoPag.getSistemaDirectorioGruposPagos().contains(stsDirGrupoPag))
				// filtrado por descripci\u00f3n de grupo de pagos, si es vac\u00edo se ignora el filtro 
				.filter((grupoPag) -> descripGrupo.isEmpty() || grupoPag.getDescripcionGrupo().contains(descripGrupo))
				//salida de colecci\u00f3n y el stream se convierte en lista
				.collect(Collectors.toList());
				
	}
	
	
	/**
	 *M\u00e9todo getAllGrupoPags del tipo list generada para mostrar un arreglo de la lista
	 * @return listaGrupoPags
	 */
	private List<GrupoPagoDTO> getAllGrupoPags(){
		// Se convierte objeto de negocio a objeto que se expone en el servicio
		return GrupoPagoLocalServiceUtil.getGrupoPagos(-1, -1).stream().
				map(grupoPago -> new GrupoPagoDTO(grupoPago.getCodigoGrupoPagos(),
						grupoPago.getDescripcionGrupo(), 
						grupoPago.getSistemaDirectorioGrupoPago()))
				.collect(Collectors.toList());
	}

}