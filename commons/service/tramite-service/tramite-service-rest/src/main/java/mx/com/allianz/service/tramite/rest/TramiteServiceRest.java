package mx.com.allianz.service.tramite.rest;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.TramiteDTO;
import mx.com.allianz.service.tramite.service.TramiteLocalServiceUtil;

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.tramite")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)

public class TramiteServiceRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todas las nacionalidades del
	 * catalogo.
	 * 
	 * @return getNacionalidades()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getTramites")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<TramiteDTO> getTramite(){
		return getAllTramite();
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los Tramites del
	 * catalogo.
	 * 
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findTramites")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<TramiteDTO> findTramiteBy(
			
			@DefaultValue("-1") @QueryParam("id") long id,
			@DefaultValue("") @QueryParam("nombre") String nombre,
			@DefaultValue("") @QueryParam("apellidoPaterno") String apellidoPaterno,
			@DefaultValue("") @QueryParam("apellidoMaterno") String apellidoMaterno,
			@DefaultValue("-1") @QueryParam("lada") int lada,
			@DefaultValue("") @QueryParam("telefono") String telefono,
			@DefaultValue("") @QueryParam("email") String email,
			@DefaultValue("-1") @QueryParam("ladaCel") int ladaCel,
			@DefaultValue("") @QueryParam("celular") String celular){
		return getAllTramite().stream()
				.filter((tramite) -> id == -1 || tramite.getId() == id)                             
				.filter((tramite) -> nombre.isEmpty() || tramite.getNombre().contains(nombre))
				.filter((tramite) -> apellidoPaterno.isEmpty() || tramite.getApellidoPaterno().contains(apellidoPaterno))
				.filter((tramite) -> apellidoMaterno.isEmpty() || tramite.getApellidoMaterno().contains(apellidoMaterno))
				.filter((tramite) -> lada == -1 || tramite.getLada() == lada)
				.filter((tramite) -> telefono.isEmpty() || tramite.getTelefono().contains(telefono))
				.filter((tramite) -> email.isEmpty() || tramite.getEmail().contains(email))
				.filter((tramite) -> ladaCel == -1 || tramite.getLadaCel() == ladaCel)
				.filter((tramite) -> celular.isEmpty() || tramite.getCelular().contains(celular))
				.collect(Collectors.toList());		
		
	}
	
	private List<TramiteDTO> getAllTramite() {
		return TramiteLocalServiceUtil.getTramites(-1, -1).stream()
				.map(tramite -> new TramiteDTO(tramite.getId(),tramite.getNombre(),
						tramite.getApellidoPaterno(),tramite.getApellidoMaterno(),
						tramite.getLada(),tramite.getTelefono(),tramite.getEmail(),
						tramite.getLadaCel(),tramite.getCelular()))
				.collect(Collectors.toList());
	}
}