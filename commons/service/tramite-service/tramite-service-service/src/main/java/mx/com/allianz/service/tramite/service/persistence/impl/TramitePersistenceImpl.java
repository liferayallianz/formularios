/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.tramite.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.tramite.exception.NoSuchTramiteException;
import mx.com.allianz.service.tramite.model.Tramite;
import mx.com.allianz.service.tramite.model.impl.TramiteImpl;
import mx.com.allianz.service.tramite.model.impl.TramiteModelImpl;
import mx.com.allianz.service.tramite.service.persistence.TramitePersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the tramite service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TramitePersistence
 * @see mx.com.allianz.service.tramite.service.persistence.TramiteUtil
 * @generated
 */
@ProviderType
public class TramitePersistenceImpl extends BasePersistenceImpl<Tramite>
	implements TramitePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TramiteUtil} to access the tramite persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TramiteImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TramiteModelImpl.ENTITY_CACHE_ENABLED,
			TramiteModelImpl.FINDER_CACHE_ENABLED, TramiteImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TramiteModelImpl.ENTITY_CACHE_ENABLED,
			TramiteModelImpl.FINDER_CACHE_ENABLED, TramiteImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TramiteModelImpl.ENTITY_CACHE_ENABLED,
			TramiteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMBRE = new FinderPath(TramiteModelImpl.ENTITY_CACHE_ENABLED,
			TramiteModelImpl.FINDER_CACHE_ENABLED, TramiteImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBynombre",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE =
		new FinderPath(TramiteModelImpl.ENTITY_CACHE_ENABLED,
			TramiteModelImpl.FINDER_CACHE_ENABLED, TramiteImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBynombre",
			new String[] { String.class.getName() },
			TramiteModelImpl.NOMBRE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NOMBRE = new FinderPath(TramiteModelImpl.ENTITY_CACHE_ENABLED,
			TramiteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBynombre",
			new String[] { String.class.getName() });

	/**
	 * Returns all the tramites where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the matching tramites
	 */
	@Override
	public List<Tramite> findBynombre(String nombre) {
		return findBynombre(nombre, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tramites where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of tramites
	 * @param end the upper bound of the range of tramites (not inclusive)
	 * @return the range of matching tramites
	 */
	@Override
	public List<Tramite> findBynombre(String nombre, int start, int end) {
		return findBynombre(nombre, start, end, null);
	}

	/**
	 * Returns an ordered range of all the tramites where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of tramites
	 * @param end the upper bound of the range of tramites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tramites
	 */
	@Override
	public List<Tramite> findBynombre(String nombre, int start, int end,
		OrderByComparator<Tramite> orderByComparator) {
		return findBynombre(nombre, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the tramites where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of tramites
	 * @param end the upper bound of the range of tramites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tramites
	 */
	@Override
	public List<Tramite> findBynombre(String nombre, int start, int end,
		OrderByComparator<Tramite> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE;
			finderArgs = new Object[] { nombre };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMBRE;
			finderArgs = new Object[] { nombre, start, end, orderByComparator };
		}

		List<Tramite> list = null;

		if (retrieveFromCache) {
			list = (List<Tramite>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Tramite tramite : list) {
					if (!Objects.equals(nombre, tramite.getNombre())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TRAMITE_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TramiteModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				if (!pagination) {
					list = (List<Tramite>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Tramite>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first tramite in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tramite
	 * @throws NoSuchTramiteException if a matching tramite could not be found
	 */
	@Override
	public Tramite findBynombre_First(String nombre,
		OrderByComparator<Tramite> orderByComparator)
		throws NoSuchTramiteException {
		Tramite tramite = fetchBynombre_First(nombre, orderByComparator);

		if (tramite != null) {
			return tramite;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTramiteException(msg.toString());
	}

	/**
	 * Returns the first tramite in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tramite, or <code>null</code> if a matching tramite could not be found
	 */
	@Override
	public Tramite fetchBynombre_First(String nombre,
		OrderByComparator<Tramite> orderByComparator) {
		List<Tramite> list = findBynombre(nombre, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last tramite in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tramite
	 * @throws NoSuchTramiteException if a matching tramite could not be found
	 */
	@Override
	public Tramite findBynombre_Last(String nombre,
		OrderByComparator<Tramite> orderByComparator)
		throws NoSuchTramiteException {
		Tramite tramite = fetchBynombre_Last(nombre, orderByComparator);

		if (tramite != null) {
			return tramite;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTramiteException(msg.toString());
	}

	/**
	 * Returns the last tramite in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tramite, or <code>null</code> if a matching tramite could not be found
	 */
	@Override
	public Tramite fetchBynombre_Last(String nombre,
		OrderByComparator<Tramite> orderByComparator) {
		int count = countBynombre(nombre);

		if (count == 0) {
			return null;
		}

		List<Tramite> list = findBynombre(nombre, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the tramites before and after the current tramite in the ordered set where nombre = &#63;.
	 *
	 * @param id the primary key of the current tramite
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next tramite
	 * @throws NoSuchTramiteException if a tramite with the primary key could not be found
	 */
	@Override
	public Tramite[] findBynombre_PrevAndNext(long id, String nombre,
		OrderByComparator<Tramite> orderByComparator)
		throws NoSuchTramiteException {
		Tramite tramite = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			Tramite[] array = new TramiteImpl[3];

			array[0] = getBynombre_PrevAndNext(session, tramite, nombre,
					orderByComparator, true);

			array[1] = tramite;

			array[2] = getBynombre_PrevAndNext(session, tramite, nombre,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Tramite getBynombre_PrevAndNext(Session session, Tramite tramite,
		String nombre, OrderByComparator<Tramite> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TRAMITE_WHERE);

		boolean bindNombre = false;

		if (nombre == null) {
			query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_1);
		}
		else if (nombre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
		}
		else {
			bindNombre = true;

			query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TramiteModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindNombre) {
			qPos.add(nombre);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(tramite);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Tramite> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the tramites where nombre = &#63; from the database.
	 *
	 * @param nombre the nombre
	 */
	@Override
	public void removeBynombre(String nombre) {
		for (Tramite tramite : findBynombre(nombre, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(tramite);
		}
	}

	/**
	 * Returns the number of tramites where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching tramites
	 */
	@Override
	public int countBynombre(String nombre) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NOMBRE;

		Object[] finderArgs = new Object[] { nombre };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TRAMITE_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_1 = "tramite.nombre IS NULL";
	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_2 = "tramite.nombre = ?";
	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_3 = "(tramite.nombre IS NULL OR tramite.nombre = '')";

	public TramitePersistenceImpl() {
		setModelClass(Tramite.class);
	}

	/**
	 * Caches the tramite in the entity cache if it is enabled.
	 *
	 * @param tramite the tramite
	 */
	@Override
	public void cacheResult(Tramite tramite) {
		entityCache.putResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
			TramiteImpl.class, tramite.getPrimaryKey(), tramite);

		tramite.resetOriginalValues();
	}

	/**
	 * Caches the tramites in the entity cache if it is enabled.
	 *
	 * @param tramites the tramites
	 */
	@Override
	public void cacheResult(List<Tramite> tramites) {
		for (Tramite tramite : tramites) {
			if (entityCache.getResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
						TramiteImpl.class, tramite.getPrimaryKey()) == null) {
				cacheResult(tramite);
			}
			else {
				tramite.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all tramites.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(TramiteImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the tramite.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Tramite tramite) {
		entityCache.removeResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
			TramiteImpl.class, tramite.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Tramite> tramites) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Tramite tramite : tramites) {
			entityCache.removeResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
				TramiteImpl.class, tramite.getPrimaryKey());
		}
	}

	/**
	 * Creates a new tramite with the primary key. Does not add the tramite to the database.
	 *
	 * @param id the primary key for the new tramite
	 * @return the new tramite
	 */
	@Override
	public Tramite create(long id) {
		Tramite tramite = new TramiteImpl();

		tramite.setNew(true);
		tramite.setPrimaryKey(id);

		return tramite;
	}

	/**
	 * Removes the tramite with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the tramite
	 * @return the tramite that was removed
	 * @throws NoSuchTramiteException if a tramite with the primary key could not be found
	 */
	@Override
	public Tramite remove(long id) throws NoSuchTramiteException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the tramite with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the tramite
	 * @return the tramite that was removed
	 * @throws NoSuchTramiteException if a tramite with the primary key could not be found
	 */
	@Override
	public Tramite remove(Serializable primaryKey)
		throws NoSuchTramiteException {
		Session session = null;

		try {
			session = openSession();

			Tramite tramite = (Tramite)session.get(TramiteImpl.class, primaryKey);

			if (tramite == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTramiteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(tramite);
		}
		catch (NoSuchTramiteException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Tramite removeImpl(Tramite tramite) {
		tramite = toUnwrappedModel(tramite);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(tramite)) {
				tramite = (Tramite)session.get(TramiteImpl.class,
						tramite.getPrimaryKeyObj());
			}

			if (tramite != null) {
				session.delete(tramite);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (tramite != null) {
			clearCache(tramite);
		}

		return tramite;
	}

	@Override
	public Tramite updateImpl(Tramite tramite) {
		tramite = toUnwrappedModel(tramite);

		boolean isNew = tramite.isNew();

		TramiteModelImpl tramiteModelImpl = (TramiteModelImpl)tramite;

		Session session = null;

		try {
			session = openSession();

			if (tramite.isNew()) {
				session.save(tramite);

				tramite.setNew(false);
			}
			else {
				tramite = (Tramite)session.merge(tramite);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TramiteModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((tramiteModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						tramiteModelImpl.getOriginalNombre()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBRE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE,
					args);

				args = new Object[] { tramiteModelImpl.getNombre() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBRE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE,
					args);
			}
		}

		entityCache.putResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
			TramiteImpl.class, tramite.getPrimaryKey(), tramite, false);

		tramite.resetOriginalValues();

		return tramite;
	}

	protected Tramite toUnwrappedModel(Tramite tramite) {
		if (tramite instanceof TramiteImpl) {
			return tramite;
		}

		TramiteImpl tramiteImpl = new TramiteImpl();

		tramiteImpl.setNew(tramite.isNew());
		tramiteImpl.setPrimaryKey(tramite.getPrimaryKey());

		tramiteImpl.setId(tramite.getId());
		tramiteImpl.setNombre(tramite.getNombre());
		tramiteImpl.setApellidoPaterno(tramite.getApellidoPaterno());
		tramiteImpl.setApellidoMaterno(tramite.getApellidoMaterno());
		tramiteImpl.setLada(tramite.getLada());
		tramiteImpl.setTelefono(tramite.getTelefono());
		tramiteImpl.setEmail(tramite.getEmail());
		tramiteImpl.setLadaCel(tramite.getLadaCel());
		tramiteImpl.setCelular(tramite.getCelular());

		return tramiteImpl;
	}

	/**
	 * Returns the tramite with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the tramite
	 * @return the tramite
	 * @throws NoSuchTramiteException if a tramite with the primary key could not be found
	 */
	@Override
	public Tramite findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTramiteException {
		Tramite tramite = fetchByPrimaryKey(primaryKey);

		if (tramite == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTramiteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return tramite;
	}

	/**
	 * Returns the tramite with the primary key or throws a {@link NoSuchTramiteException} if it could not be found.
	 *
	 * @param id the primary key of the tramite
	 * @return the tramite
	 * @throws NoSuchTramiteException if a tramite with the primary key could not be found
	 */
	@Override
	public Tramite findByPrimaryKey(long id) throws NoSuchTramiteException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the tramite with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the tramite
	 * @return the tramite, or <code>null</code> if a tramite with the primary key could not be found
	 */
	@Override
	public Tramite fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
				TramiteImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Tramite tramite = (Tramite)serializable;

		if (tramite == null) {
			Session session = null;

			try {
				session = openSession();

				tramite = (Tramite)session.get(TramiteImpl.class, primaryKey);

				if (tramite != null) {
					cacheResult(tramite);
				}
				else {
					entityCache.putResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
						TramiteImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
					TramiteImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return tramite;
	}

	/**
	 * Returns the tramite with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the tramite
	 * @return the tramite, or <code>null</code> if a tramite with the primary key could not be found
	 */
	@Override
	public Tramite fetchByPrimaryKey(long id) {
		return fetchByPrimaryKey((Serializable)id);
	}

	@Override
	public Map<Serializable, Tramite> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Tramite> map = new HashMap<Serializable, Tramite>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Tramite tramite = fetchByPrimaryKey(primaryKey);

			if (tramite != null) {
				map.put(primaryKey, tramite);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
					TramiteImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Tramite)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_TRAMITE_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Tramite tramite : (List<Tramite>)q.list()) {
				map.put(tramite.getPrimaryKeyObj(), tramite);

				cacheResult(tramite);

				uncachedPrimaryKeys.remove(tramite.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(TramiteModelImpl.ENTITY_CACHE_ENABLED,
					TramiteImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the tramites.
	 *
	 * @return the tramites
	 */
	@Override
	public List<Tramite> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tramites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tramites
	 * @param end the upper bound of the range of tramites (not inclusive)
	 * @return the range of tramites
	 */
	@Override
	public List<Tramite> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the tramites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tramites
	 * @param end the upper bound of the range of tramites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tramites
	 */
	@Override
	public List<Tramite> findAll(int start, int end,
		OrderByComparator<Tramite> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the tramites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tramites
	 * @param end the upper bound of the range of tramites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of tramites
	 */
	@Override
	public List<Tramite> findAll(int start, int end,
		OrderByComparator<Tramite> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Tramite> list = null;

		if (retrieveFromCache) {
			list = (List<Tramite>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_TRAMITE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TRAMITE;

				if (pagination) {
					sql = sql.concat(TramiteModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Tramite>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Tramite>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the tramites from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Tramite tramite : findAll()) {
			remove(tramite);
		}
	}

	/**
	 * Returns the number of tramites.
	 *
	 * @return the number of tramites
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TRAMITE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return TramiteModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the tramite persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(TramiteImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_TRAMITE = "SELECT tramite FROM Tramite tramite";
	private static final String _SQL_SELECT_TRAMITE_WHERE_PKS_IN = "SELECT tramite FROM Tramite tramite WHERE id_ IN (";
	private static final String _SQL_SELECT_TRAMITE_WHERE = "SELECT tramite FROM Tramite tramite WHERE ";
	private static final String _SQL_COUNT_TRAMITE = "SELECT COUNT(tramite) FROM Tramite tramite";
	private static final String _SQL_COUNT_TRAMITE_WHERE = "SELECT COUNT(tramite) FROM Tramite tramite WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "tramite.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Tramite exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Tramite exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(TramitePersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
}