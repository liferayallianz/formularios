/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.tramite.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.tramite.model.Tramite;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Tramite in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Tramite
 * @generated
 */
@ProviderType
public class TramiteCacheModel implements CacheModel<Tramite>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TramiteCacheModel)) {
			return false;
		}

		TramiteCacheModel tramiteCacheModel = (TramiteCacheModel)obj;

		if (id == tramiteCacheModel.id) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, id);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{id=");
		sb.append(id);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", apellidoPaterno=");
		sb.append(apellidoPaterno);
		sb.append(", apellidoMaterno=");
		sb.append(apellidoMaterno);
		sb.append(", lada=");
		sb.append(lada);
		sb.append(", telefono=");
		sb.append(telefono);
		sb.append(", email=");
		sb.append(email);
		sb.append(", ladaCel=");
		sb.append(ladaCel);
		sb.append(", celular=");
		sb.append(celular);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Tramite toEntityModel() {
		TramiteImpl tramiteImpl = new TramiteImpl();

		tramiteImpl.setId(id);

		if (nombre == null) {
			tramiteImpl.setNombre(StringPool.BLANK);
		}
		else {
			tramiteImpl.setNombre(nombre);
		}

		if (apellidoPaterno == null) {
			tramiteImpl.setApellidoPaterno(StringPool.BLANK);
		}
		else {
			tramiteImpl.setApellidoPaterno(apellidoPaterno);
		}

		if (apellidoMaterno == null) {
			tramiteImpl.setApellidoMaterno(StringPool.BLANK);
		}
		else {
			tramiteImpl.setApellidoMaterno(apellidoMaterno);
		}

		tramiteImpl.setLada(lada);

		if (telefono == null) {
			tramiteImpl.setTelefono(StringPool.BLANK);
		}
		else {
			tramiteImpl.setTelefono(telefono);
		}

		if (email == null) {
			tramiteImpl.setEmail(StringPool.BLANK);
		}
		else {
			tramiteImpl.setEmail(email);
		}

		tramiteImpl.setLadaCel(ladaCel);

		if (celular == null) {
			tramiteImpl.setCelular(StringPool.BLANK);
		}
		else {
			tramiteImpl.setCelular(celular);
		}

		tramiteImpl.resetOriginalValues();

		return tramiteImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		nombre = objectInput.readUTF();
		apellidoPaterno = objectInput.readUTF();
		apellidoMaterno = objectInput.readUTF();

		lada = objectInput.readInt();
		telefono = objectInput.readUTF();
		email = objectInput.readUTF();

		ladaCel = objectInput.readInt();
		celular = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (apellidoPaterno == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidoPaterno);
		}

		if (apellidoMaterno == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidoMaterno);
		}

		objectOutput.writeInt(lada);

		if (telefono == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(telefono);
		}

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		objectOutput.writeInt(ladaCel);

		if (celular == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(celular);
		}
	}

	public long id;
	public String nombre;
	public String apellidoPaterno;
	public String apellidoMaterno;
	public int lada;
	public String telefono;
	public String email;
	public int ladaCel;
	public String celular;
}