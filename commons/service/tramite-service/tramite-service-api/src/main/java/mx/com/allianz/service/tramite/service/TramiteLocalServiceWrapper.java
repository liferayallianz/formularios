/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.tramite.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TramiteLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see TramiteLocalService
 * @generated
 */
@ProviderType
public class TramiteLocalServiceWrapper implements TramiteLocalService,
	ServiceWrapper<TramiteLocalService> {
	public TramiteLocalServiceWrapper(TramiteLocalService tramiteLocalService) {
		_tramiteLocalService = tramiteLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _tramiteLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _tramiteLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _tramiteLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _tramiteLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _tramiteLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of tramites.
	*
	* @return the number of tramites
	*/
	@Override
	public int getTramitesCount() {
		return _tramiteLocalService.getTramitesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _tramiteLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _tramiteLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.tramite.model.impl.TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _tramiteLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.tramite.model.impl.TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _tramiteLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the tramites.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.tramite.model.impl.TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tramites
	* @param end the upper bound of the range of tramites (not inclusive)
	* @return the range of tramites
	*/
	@Override
	public java.util.List<mx.com.allianz.service.tramite.model.Tramite> getTramites(
		int start, int end) {
		return _tramiteLocalService.getTramites(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _tramiteLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _tramiteLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the tramite to the database. Also notifies the appropriate model listeners.
	*
	* @param tramite the tramite
	* @return the tramite that was added
	*/
	@Override
	public mx.com.allianz.service.tramite.model.Tramite addTramite(
		mx.com.allianz.service.tramite.model.Tramite tramite) {
		return _tramiteLocalService.addTramite(tramite);
	}

	/**
	* Creates a new tramite with the primary key. Does not add the tramite to the database.
	*
	* @param id the primary key for the new tramite
	* @return the new tramite
	*/
	@Override
	public mx.com.allianz.service.tramite.model.Tramite createTramite(long id) {
		return _tramiteLocalService.createTramite(id);
	}

	/**
	* Deletes the tramite with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the tramite
	* @return the tramite that was removed
	* @throws PortalException if a tramite with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.tramite.model.Tramite deleteTramite(long id)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _tramiteLocalService.deleteTramite(id);
	}

	/**
	* Deletes the tramite from the database. Also notifies the appropriate model listeners.
	*
	* @param tramite the tramite
	* @return the tramite that was removed
	*/
	@Override
	public mx.com.allianz.service.tramite.model.Tramite deleteTramite(
		mx.com.allianz.service.tramite.model.Tramite tramite) {
		return _tramiteLocalService.deleteTramite(tramite);
	}

	@Override
	public mx.com.allianz.service.tramite.model.Tramite fetchTramite(long id) {
		return _tramiteLocalService.fetchTramite(id);
	}

	/**
	* Returns the tramite with the primary key.
	*
	* @param id the primary key of the tramite
	* @return the tramite
	* @throws PortalException if a tramite with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.tramite.model.Tramite getTramite(long id)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _tramiteLocalService.getTramite(id);
	}

	/**
	* Updates the tramite in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param tramite the tramite
	* @return the tramite that was updated
	*/
	@Override
	public mx.com.allianz.service.tramite.model.Tramite updateTramite(
		mx.com.allianz.service.tramite.model.Tramite tramite) {
		return _tramiteLocalService.updateTramite(tramite);
	}

	@Override
	public TramiteLocalService getWrappedService() {
		return _tramiteLocalService;
	}

	@Override
	public void setWrappedService(TramiteLocalService tramiteLocalService) {
		_tramiteLocalService = tramiteLocalService;
	}

	private TramiteLocalService _tramiteLocalService;
}