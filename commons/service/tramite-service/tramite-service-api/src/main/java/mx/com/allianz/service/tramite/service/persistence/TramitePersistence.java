/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.tramite.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.tramite.exception.NoSuchTramiteException;
import mx.com.allianz.service.tramite.model.Tramite;

/**
 * The persistence interface for the tramite service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.tramite.service.persistence.impl.TramitePersistenceImpl
 * @see TramiteUtil
 * @generated
 */
@ProviderType
public interface TramitePersistence extends BasePersistence<Tramite> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TramiteUtil} to access the tramite persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the tramites where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching tramites
	*/
	public java.util.List<Tramite> findBynombre(java.lang.String nombre);

	/**
	* Returns a range of all the tramites where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of tramites
	* @param end the upper bound of the range of tramites (not inclusive)
	* @return the range of matching tramites
	*/
	public java.util.List<Tramite> findBynombre(java.lang.String nombre,
		int start, int end);

	/**
	* Returns an ordered range of all the tramites where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of tramites
	* @param end the upper bound of the range of tramites (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching tramites
	*/
	public java.util.List<Tramite> findBynombre(java.lang.String nombre,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Tramite> orderByComparator);

	/**
	* Returns an ordered range of all the tramites where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of tramites
	* @param end the upper bound of the range of tramites (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching tramites
	*/
	public java.util.List<Tramite> findBynombre(java.lang.String nombre,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Tramite> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first tramite in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tramite
	* @throws NoSuchTramiteException if a matching tramite could not be found
	*/
	public Tramite findBynombre_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Tramite> orderByComparator)
		throws NoSuchTramiteException;

	/**
	* Returns the first tramite in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tramite, or <code>null</code> if a matching tramite could not be found
	*/
	public Tramite fetchBynombre_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Tramite> orderByComparator);

	/**
	* Returns the last tramite in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tramite
	* @throws NoSuchTramiteException if a matching tramite could not be found
	*/
	public Tramite findBynombre_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Tramite> orderByComparator)
		throws NoSuchTramiteException;

	/**
	* Returns the last tramite in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tramite, or <code>null</code> if a matching tramite could not be found
	*/
	public Tramite fetchBynombre_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Tramite> orderByComparator);

	/**
	* Returns the tramites before and after the current tramite in the ordered set where nombre = &#63;.
	*
	* @param id the primary key of the current tramite
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next tramite
	* @throws NoSuchTramiteException if a tramite with the primary key could not be found
	*/
	public Tramite[] findBynombre_PrevAndNext(long id, java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Tramite> orderByComparator)
		throws NoSuchTramiteException;

	/**
	* Removes all the tramites where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public void removeBynombre(java.lang.String nombre);

	/**
	* Returns the number of tramites where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching tramites
	*/
	public int countBynombre(java.lang.String nombre);

	/**
	* Caches the tramite in the entity cache if it is enabled.
	*
	* @param tramite the tramite
	*/
	public void cacheResult(Tramite tramite);

	/**
	* Caches the tramites in the entity cache if it is enabled.
	*
	* @param tramites the tramites
	*/
	public void cacheResult(java.util.List<Tramite> tramites);

	/**
	* Creates a new tramite with the primary key. Does not add the tramite to the database.
	*
	* @param id the primary key for the new tramite
	* @return the new tramite
	*/
	public Tramite create(long id);

	/**
	* Removes the tramite with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the tramite
	* @return the tramite that was removed
	* @throws NoSuchTramiteException if a tramite with the primary key could not be found
	*/
	public Tramite remove(long id) throws NoSuchTramiteException;

	public Tramite updateImpl(Tramite tramite);

	/**
	* Returns the tramite with the primary key or throws a {@link NoSuchTramiteException} if it could not be found.
	*
	* @param id the primary key of the tramite
	* @return the tramite
	* @throws NoSuchTramiteException if a tramite with the primary key could not be found
	*/
	public Tramite findByPrimaryKey(long id) throws NoSuchTramiteException;

	/**
	* Returns the tramite with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the tramite
	* @return the tramite, or <code>null</code> if a tramite with the primary key could not be found
	*/
	public Tramite fetchByPrimaryKey(long id);

	@Override
	public java.util.Map<java.io.Serializable, Tramite> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the tramites.
	*
	* @return the tramites
	*/
	public java.util.List<Tramite> findAll();

	/**
	* Returns a range of all the tramites.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tramites
	* @param end the upper bound of the range of tramites (not inclusive)
	* @return the range of tramites
	*/
	public java.util.List<Tramite> findAll(int start, int end);

	/**
	* Returns an ordered range of all the tramites.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tramites
	* @param end the upper bound of the range of tramites (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tramites
	*/
	public java.util.List<Tramite> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Tramite> orderByComparator);

	/**
	* Returns an ordered range of all the tramites.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TramiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tramites
	* @param end the upper bound of the range of tramites (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of tramites
	*/
	public java.util.List<Tramite> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Tramite> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the tramites from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of tramites.
	*
	* @return the number of tramites
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}