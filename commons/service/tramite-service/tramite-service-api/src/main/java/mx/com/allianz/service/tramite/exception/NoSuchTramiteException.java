/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package mx.com.allianz.service.tramite.exception;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.NoSuchModelException;

/**
 * @author Brian Wing Shun Chan
 */
@ProviderType
public class NoSuchTramiteException extends NoSuchModelException {

	public NoSuchTramiteException() {
	}

	public NoSuchTramiteException(String msg) {
		super(msg);
	}

	public NoSuchTramiteException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public NoSuchTramiteException(Throwable cause) {
		super(cause);
	}

}