/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.tramite.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class TramiteSoap implements Serializable {
	public static TramiteSoap toSoapModel(Tramite model) {
		TramiteSoap soapModel = new TramiteSoap();

		soapModel.setId(model.getId());
		soapModel.setNombre(model.getNombre());
		soapModel.setApellidoPaterno(model.getApellidoPaterno());
		soapModel.setApellidoMaterno(model.getApellidoMaterno());
		soapModel.setLada(model.getLada());
		soapModel.setTelefono(model.getTelefono());
		soapModel.setEmail(model.getEmail());
		soapModel.setLadaCel(model.getLadaCel());
		soapModel.setCelular(model.getCelular());

		return soapModel;
	}

	public static TramiteSoap[] toSoapModels(Tramite[] models) {
		TramiteSoap[] soapModels = new TramiteSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TramiteSoap[][] toSoapModels(Tramite[][] models) {
		TramiteSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TramiteSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TramiteSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TramiteSoap[] toSoapModels(List<Tramite> models) {
		List<TramiteSoap> soapModels = new ArrayList<TramiteSoap>(models.size());

		for (Tramite model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TramiteSoap[soapModels.size()]);
	}

	public TramiteSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getApellidoPaterno() {
		return _apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		_apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return _apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		_apellidoMaterno = apellidoMaterno;
	}

	public int getLada() {
		return _lada;
	}

	public void setLada(int lada) {
		_lada = lada;
	}

	public String getTelefono() {
		return _telefono;
	}

	public void setTelefono(String telefono) {
		_telefono = telefono;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public int getLadaCel() {
		return _ladaCel;
	}

	public void setLadaCel(int ladaCel) {
		_ladaCel = ladaCel;
	}

	public String getCelular() {
		return _celular;
	}

	public void setCelular(String celular) {
		_celular = celular;
	}

	private long _id;
	private String _nombre;
	private String _apellidoPaterno;
	private String _apellidoMaterno;
	private int _lada;
	private String _telefono;
	private String _email;
	private int _ladaCel;
	private String _celular;
}