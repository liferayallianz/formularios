/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.tramite.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Tramite}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Tramite
 * @generated
 */
@ProviderType
public class TramiteWrapper implements Tramite, ModelWrapper<Tramite> {
	public TramiteWrapper(Tramite tramite) {
		_tramite = tramite;
	}

	@Override
	public Class<?> getModelClass() {
		return Tramite.class;
	}

	@Override
	public String getModelClassName() {
		return Tramite.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("nombre", getNombre());
		attributes.put("apellidoPaterno", getApellidoPaterno());
		attributes.put("apellidoMaterno", getApellidoMaterno());
		attributes.put("lada", getLada());
		attributes.put("telefono", getTelefono());
		attributes.put("email", getEmail());
		attributes.put("ladaCel", getLadaCel());
		attributes.put("celular", getCelular());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String apellidoPaterno = (String)attributes.get("apellidoPaterno");

		if (apellidoPaterno != null) {
			setApellidoPaterno(apellidoPaterno);
		}

		String apellidoMaterno = (String)attributes.get("apellidoMaterno");

		if (apellidoMaterno != null) {
			setApellidoMaterno(apellidoMaterno);
		}

		Integer lada = (Integer)attributes.get("lada");

		if (lada != null) {
			setLada(lada);
		}

		String telefono = (String)attributes.get("telefono");

		if (telefono != null) {
			setTelefono(telefono);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		Integer ladaCel = (Integer)attributes.get("ladaCel");

		if (ladaCel != null) {
			setLadaCel(ladaCel);
		}

		String celular = (String)attributes.get("celular");

		if (celular != null) {
			setCelular(celular);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _tramite.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _tramite.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _tramite.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _tramite.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Tramite> toCacheModel() {
		return _tramite.toCacheModel();
	}

	@Override
	public int compareTo(Tramite tramite) {
		return _tramite.compareTo(tramite);
	}

	/**
	* Returns the lada of this tramite.
	*
	* @return the lada of this tramite
	*/
	@Override
	public int getLada() {
		return _tramite.getLada();
	}

	/**
	* Returns the lada cel of this tramite.
	*
	* @return the lada cel of this tramite
	*/
	@Override
	public int getLadaCel() {
		return _tramite.getLadaCel();
	}

	@Override
	public int hashCode() {
		return _tramite.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _tramite.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new TramiteWrapper((Tramite)_tramite.clone());
	}

	/**
	* Returns the apellido materno of this tramite.
	*
	* @return the apellido materno of this tramite
	*/
	@Override
	public java.lang.String getApellidoMaterno() {
		return _tramite.getApellidoMaterno();
	}

	/**
	* Returns the apellido paterno of this tramite.
	*
	* @return the apellido paterno of this tramite
	*/
	@Override
	public java.lang.String getApellidoPaterno() {
		return _tramite.getApellidoPaterno();
	}

	/**
	* Returns the celular of this tramite.
	*
	* @return the celular of this tramite
	*/
	@Override
	public java.lang.String getCelular() {
		return _tramite.getCelular();
	}

	/**
	* Returns the email of this tramite.
	*
	* @return the email of this tramite
	*/
	@Override
	public java.lang.String getEmail() {
		return _tramite.getEmail();
	}

	/**
	* Returns the nombre of this tramite.
	*
	* @return the nombre of this tramite
	*/
	@Override
	public java.lang.String getNombre() {
		return _tramite.getNombre();
	}

	/**
	* Returns the telefono of this tramite.
	*
	* @return the telefono of this tramite
	*/
	@Override
	public java.lang.String getTelefono() {
		return _tramite.getTelefono();
	}

	@Override
	public java.lang.String toString() {
		return _tramite.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _tramite.toXmlString();
	}

	/**
	* Returns the ID of this tramite.
	*
	* @return the ID of this tramite
	*/
	@Override
	public long getId() {
		return _tramite.getId();
	}

	/**
	* Returns the primary key of this tramite.
	*
	* @return the primary key of this tramite
	*/
	@Override
	public long getPrimaryKey() {
		return _tramite.getPrimaryKey();
	}

	@Override
	public Tramite toEscapedModel() {
		return new TramiteWrapper(_tramite.toEscapedModel());
	}

	@Override
	public Tramite toUnescapedModel() {
		return new TramiteWrapper(_tramite.toUnescapedModel());
	}

	@Override
	public void persist() {
		_tramite.persist();
	}

	/**
	* Sets the apellido materno of this tramite.
	*
	* @param apellidoMaterno the apellido materno of this tramite
	*/
	@Override
	public void setApellidoMaterno(java.lang.String apellidoMaterno) {
		_tramite.setApellidoMaterno(apellidoMaterno);
	}

	/**
	* Sets the apellido paterno of this tramite.
	*
	* @param apellidoPaterno the apellido paterno of this tramite
	*/
	@Override
	public void setApellidoPaterno(java.lang.String apellidoPaterno) {
		_tramite.setApellidoPaterno(apellidoPaterno);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_tramite.setCachedModel(cachedModel);
	}

	/**
	* Sets the celular of this tramite.
	*
	* @param celular the celular of this tramite
	*/
	@Override
	public void setCelular(java.lang.String celular) {
		_tramite.setCelular(celular);
	}

	/**
	* Sets the email of this tramite.
	*
	* @param email the email of this tramite
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_tramite.setEmail(email);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_tramite.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_tramite.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_tramite.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the ID of this tramite.
	*
	* @param id the ID of this tramite
	*/
	@Override
	public void setId(long id) {
		_tramite.setId(id);
	}

	/**
	* Sets the lada of this tramite.
	*
	* @param lada the lada of this tramite
	*/
	@Override
	public void setLada(int lada) {
		_tramite.setLada(lada);
	}

	/**
	* Sets the lada cel of this tramite.
	*
	* @param ladaCel the lada cel of this tramite
	*/
	@Override
	public void setLadaCel(int ladaCel) {
		_tramite.setLadaCel(ladaCel);
	}

	@Override
	public void setNew(boolean n) {
		_tramite.setNew(n);
	}

	/**
	* Sets the nombre of this tramite.
	*
	* @param nombre the nombre of this tramite
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_tramite.setNombre(nombre);
	}

	/**
	* Sets the primary key of this tramite.
	*
	* @param primaryKey the primary key of this tramite
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_tramite.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_tramite.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the telefono of this tramite.
	*
	* @param telefono the telefono of this tramite
	*/
	@Override
	public void setTelefono(java.lang.String telefono) {
		_tramite.setTelefono(telefono);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TramiteWrapper)) {
			return false;
		}

		TramiteWrapper tramiteWrapper = (TramiteWrapper)obj;

		if (Objects.equals(_tramite, tramiteWrapper._tramite)) {
			return true;
		}

		return false;
	}

	@Override
	public Tramite getWrappedModel() {
		return _tramite;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _tramite.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _tramite.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_tramite.resetOriginalValues();
	}

	private final Tramite _tramite;
}