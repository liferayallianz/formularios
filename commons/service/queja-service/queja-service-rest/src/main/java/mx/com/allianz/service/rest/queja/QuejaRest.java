package mx.com.allianz.service.rest.queja;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.dto.queja.QuejaDTO;
import mx.com.allianz.service.queja.service.QuejaLocalServiceUtil;


/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio
 * QuejaDTO que permite visualizar una lista de datos, filtra el orden de
 * la b\u00fasqueda y muestra el ordenamiento conforme al filtro.
 * 
 *
 * @author TestingZone
 * @version 1.0
 * @since 2017-01-1
 * 
 * 
 */

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.queja")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)

public class QuejaRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todas las quejas 
	 * del catalogo.
	 * 
	 * @return getAllQuejas()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getQuejas")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<QuejaDTO> getQueja(){
		
		return getAllQuejas();
		
	}
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los Proveedores del
	 * catalogo.
	 * 
	 * @return getAllQuejas().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.

	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findQuejas")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	
	public List<QuejaDTO>findQuejaBy(
			
			@DefaultValue("") @QueryParam("clave") String clave, 
			@DefaultValue("") @QueryParam("tipo") String tipo,
			@DefaultValue("-1") @QueryParam("anio") int anio,
			@DefaultValue("-1") @QueryParam("folio") int folio,
			@DefaultValue("") @QueryParam("es_Usted") String esUsted, 
			@DefaultValue("") @QueryParam("numero_Poliza") String numeroPoliza,
			@DefaultValue("") @QueryParam("nombre") String nombre,
			@DefaultValue("") @QueryParam("apellidos") String apellidos,
			@DefaultValue("") @QueryParam("rfc") String rfc,
			@DefaultValue("") @QueryParam("telefonoParticular") String telefonoParticular,
			@DefaultValue("") @QueryParam("TelefonoCelular") String TelefonoCelular,
			@DefaultValue("") @QueryParam("email") String email,
			@DefaultValue("") @QueryParam("estado") String estado,
			@DefaultValue("") @QueryParam("quejaRelacionada") String quejaRelacionada,
			@DefaultValue("") @QueryParam("quejaRefiere") String quejaRefiere,
			@DefaultValue("") @QueryParam("observaciones") String observaciones,
			@DefaultValue("") @QueryParam("archivo") String archivo,
			@DefaultValue("") @QueryParam("sistemaOrigen") String sistemaOrigen,
			@DefaultValue("") @QueryParam("claveAgente") String claveAgente,
			@DefaultValue("") @QueryParam("negocio") String negocio,
			@DefaultValue("") @QueryParam("emisor") String emisor,
			@DefaultValue("") @QueryParam("claveQuejaRelacionada") String claveQuejaRelacionada,
			@DefaultValue("") @QueryParam("claveQuejaRefiere") String claveQuejaRefiere,
			@DefaultValue("") @QueryParam("razonSocial") String razonSocial){
		
		return getAllQuejas().stream()
		
	
	
	.filter((queja) -> clave.isEmpty() || queja.getClave().contains(clave))
	.filter((queja) -> tipo.isEmpty() || queja.getTipo().contains(tipo))
	.filter((queja) -> anio == -1 || queja.getAnio() == anio)
	.filter((queja) -> folio == -1 || queja.getFolio()== folio)
	.filter((queja) -> esUsted.isEmpty() || queja.getEsUsted().contains(esUsted))
	.filter((queja) -> numeroPoliza.isEmpty() || queja.getNumeroPoliza().contains(numeroPoliza))
	.filter((queja) -> nombre.isEmpty() || queja.getNombre().contains(nombre))
	.filter((queja) -> apellidos.isEmpty() || queja.getApellidos().contains(apellidos))
	.filter((queja) -> rfc.isEmpty() || queja.getRfc().contains(rfc))
	.filter((queja) -> telefonoParticular.isEmpty() || queja.getTelefonoParticular().contains(telefonoParticular))
	.filter((queja) -> TelefonoCelular.isEmpty() || queja.getTelefonoCelular().contains(TelefonoCelular))
	.filter((queja) -> email.isEmpty() || queja.getEmail().contains(email))
	.filter((queja) -> estado.isEmpty() || queja.getEstado().contains(estado))
	.filter((queja) -> quejaRelacionada.isEmpty() || queja.getQuejaRelacionada().contains(quejaRelacionada))
	.filter((queja) -> quejaRefiere.isEmpty() || queja.getQuejaRefiere().contains(quejaRefiere))
	.filter((queja) -> observaciones.isEmpty() || queja.getObservaciones().contains(observaciones))
	.filter((queja) -> archivo.isEmpty() || queja.getArchivo().contains(archivo))
	.filter((queja) -> sistemaOrigen.isEmpty() || queja.getSistemaOrigen().contains(sistemaOrigen))
	.filter((queja) -> claveAgente.isEmpty() || queja.getClaveAgente().contains(claveAgente))
	.filter((queja) -> negocio.isEmpty() || queja.getNegocio().contains(negocio))
	.filter((queja) -> emisor.isEmpty() || queja.getEmisor().contains(emisor))
	.filter((queja) -> claveQuejaRelacionada.isEmpty() || queja.getClaveQuejaRelacionada().contains(claveQuejaRelacionada))
	.filter((queja) -> claveQuejaRefiere.isEmpty() || queja.getClaveQuejaRefiere().contains(claveQuejaRefiere))
	.filter((queja) -> razonSocial.isEmpty() || queja.getRazonSocial().contains(razonSocial))

	.collect(Collectors.toList());
	}
	

	private List<QuejaDTO> getAllQuejas() {
		
		return QuejaLocalServiceUtil.getQuejas(-1, -1).stream().
				map(queja -> new QuejaDTO(queja.getClave(), queja.getTipo(),
				queja.getAnio(), queja.getFolio(), queja.getEsUsted(),
				queja.getNumeroPoliza(), queja.getNombre(), queja.getApellidos(),
				queja.getRfc(), queja.getTelefonoParticular(), 
				queja.getTelefonoCelular(), queja.getEmail(), queja.getEstado(), 
				queja.getClaveQuejaRelacionada(), queja.getClaveQuejaRefiere(), 
				queja.getObservaciones(), queja.getArchivo(), queja.getFechaRegistro(),
				queja.getSistemaOrigen(), queja.getClaveAgente(), queja.getNegocio(),
				queja.getEmisor(), queja.getClaveQuejaRelacionada(), 
				queja.getClaveQuejaRefiere(), queja.getRazonSocial())).
				collect(Collectors.toList());
	}

}
