/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.queja.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.queja.model.Queja;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the queja service. This utility wraps {@link mx.com.allianz.service.queja.service.persistence.impl.QuejaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuejaPersistence
 * @see mx.com.allianz.service.queja.service.persistence.impl.QuejaPersistenceImpl
 * @generated
 */
@ProviderType
public class QuejaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Queja queja) {
		getPersistence().clearCache(queja);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Queja> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Queja> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Queja> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Queja> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Queja update(Queja queja) {
		return getPersistence().update(queja);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Queja update(Queja queja, ServiceContext serviceContext) {
		return getPersistence().update(queja, serviceContext);
	}

	/**
	* Returns all the quejas where tipo = &#63;.
	*
	* @param tipo the tipo
	* @return the matching quejas
	*/
	public static List<Queja> findByfindByTipo(java.lang.String tipo) {
		return getPersistence().findByfindByTipo(tipo);
	}

	/**
	* Returns a range of all the quejas where tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipo the tipo
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @return the range of matching quejas
	*/
	public static List<Queja> findByfindByTipo(java.lang.String tipo,
		int start, int end) {
		return getPersistence().findByfindByTipo(tipo, start, end);
	}

	/**
	* Returns an ordered range of all the quejas where tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipo the tipo
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quejas
	*/
	public static List<Queja> findByfindByTipo(java.lang.String tipo,
		int start, int end, OrderByComparator<Queja> orderByComparator) {
		return getPersistence()
				   .findByfindByTipo(tipo, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the quejas where tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipo the tipo
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching quejas
	*/
	public static List<Queja> findByfindByTipo(java.lang.String tipo,
		int start, int end, OrderByComparator<Queja> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByTipo(tipo, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first queja in the ordered set where tipo = &#63;.
	*
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja
	* @throws NoSuchQuejaException if a matching queja could not be found
	*/
	public static Queja findByfindByTipo_First(java.lang.String tipo,
		OrderByComparator<Queja> orderByComparator)
		throws mx.com.allianz.service.queja.exception.NoSuchQuejaException {
		return getPersistence().findByfindByTipo_First(tipo, orderByComparator);
	}

	/**
	* Returns the first queja in the ordered set where tipo = &#63;.
	*
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja, or <code>null</code> if a matching queja could not be found
	*/
	public static Queja fetchByfindByTipo_First(java.lang.String tipo,
		OrderByComparator<Queja> orderByComparator) {
		return getPersistence().fetchByfindByTipo_First(tipo, orderByComparator);
	}

	/**
	* Returns the last queja in the ordered set where tipo = &#63;.
	*
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja
	* @throws NoSuchQuejaException if a matching queja could not be found
	*/
	public static Queja findByfindByTipo_Last(java.lang.String tipo,
		OrderByComparator<Queja> orderByComparator)
		throws mx.com.allianz.service.queja.exception.NoSuchQuejaException {
		return getPersistence().findByfindByTipo_Last(tipo, orderByComparator);
	}

	/**
	* Returns the last queja in the ordered set where tipo = &#63;.
	*
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja, or <code>null</code> if a matching queja could not be found
	*/
	public static Queja fetchByfindByTipo_Last(java.lang.String tipo,
		OrderByComparator<Queja> orderByComparator) {
		return getPersistence().fetchByfindByTipo_Last(tipo, orderByComparator);
	}

	/**
	* Returns the quejas before and after the current queja in the ordered set where tipo = &#63;.
	*
	* @param clave the primary key of the current queja
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next queja
	* @throws NoSuchQuejaException if a queja with the primary key could not be found
	*/
	public static Queja[] findByfindByTipo_PrevAndNext(java.lang.String clave,
		java.lang.String tipo, OrderByComparator<Queja> orderByComparator)
		throws mx.com.allianz.service.queja.exception.NoSuchQuejaException {
		return getPersistence()
				   .findByfindByTipo_PrevAndNext(clave, tipo, orderByComparator);
	}

	/**
	* Removes all the quejas where tipo = &#63; from the database.
	*
	* @param tipo the tipo
	*/
	public static void removeByfindByTipo(java.lang.String tipo) {
		getPersistence().removeByfindByTipo(tipo);
	}

	/**
	* Returns the number of quejas where tipo = &#63;.
	*
	* @param tipo the tipo
	* @return the number of matching quejas
	*/
	public static int countByfindByTipo(java.lang.String tipo) {
		return getPersistence().countByfindByTipo(tipo);
	}

	/**
	* Caches the queja in the entity cache if it is enabled.
	*
	* @param queja the queja
	*/
	public static void cacheResult(Queja queja) {
		getPersistence().cacheResult(queja);
	}

	/**
	* Caches the quejas in the entity cache if it is enabled.
	*
	* @param quejas the quejas
	*/
	public static void cacheResult(List<Queja> quejas) {
		getPersistence().cacheResult(quejas);
	}

	/**
	* Creates a new queja with the primary key. Does not add the queja to the database.
	*
	* @param clave the primary key for the new queja
	* @return the new queja
	*/
	public static Queja create(java.lang.String clave) {
		return getPersistence().create(clave);
	}

	/**
	* Removes the queja with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clave the primary key of the queja
	* @return the queja that was removed
	* @throws NoSuchQuejaException if a queja with the primary key could not be found
	*/
	public static Queja remove(java.lang.String clave)
		throws mx.com.allianz.service.queja.exception.NoSuchQuejaException {
		return getPersistence().remove(clave);
	}

	public static Queja updateImpl(Queja queja) {
		return getPersistence().updateImpl(queja);
	}

	/**
	* Returns the queja with the primary key or throws a {@link NoSuchQuejaException} if it could not be found.
	*
	* @param clave the primary key of the queja
	* @return the queja
	* @throws NoSuchQuejaException if a queja with the primary key could not be found
	*/
	public static Queja findByPrimaryKey(java.lang.String clave)
		throws mx.com.allianz.service.queja.exception.NoSuchQuejaException {
		return getPersistence().findByPrimaryKey(clave);
	}

	/**
	* Returns the queja with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param clave the primary key of the queja
	* @return the queja, or <code>null</code> if a queja with the primary key could not be found
	*/
	public static Queja fetchByPrimaryKey(java.lang.String clave) {
		return getPersistence().fetchByPrimaryKey(clave);
	}

	public static java.util.Map<java.io.Serializable, Queja> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the quejas.
	*
	* @return the quejas
	*/
	public static List<Queja> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the quejas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @return the range of quejas
	*/
	public static List<Queja> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the quejas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quejas
	*/
	public static List<Queja> findAll(int start, int end,
		OrderByComparator<Queja> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the quejas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of quejas
	*/
	public static List<Queja> findAll(int start, int end,
		OrderByComparator<Queja> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the quejas from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of quejas.
	*
	* @return the number of quejas
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static QuejaPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<QuejaPersistence, QuejaPersistence> _serviceTracker =
		ServiceTrackerFactory.open(QuejaPersistence.class);
}