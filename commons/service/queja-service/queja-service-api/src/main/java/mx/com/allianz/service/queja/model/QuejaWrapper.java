/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.queja.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Queja}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Queja
 * @generated
 */
@ProviderType
public class QuejaWrapper implements Queja, ModelWrapper<Queja> {
	public QuejaWrapper(Queja queja) {
		_queja = queja;
	}

	@Override
	public Class<?> getModelClass() {
		return Queja.class;
	}

	@Override
	public String getModelClassName() {
		return Queja.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("clave", getClave());
		attributes.put("tipo", getTipo());
		attributes.put("anio", getAnio());
		attributes.put("folio", getFolio());
		attributes.put("esUsted", getEsUsted());
		attributes.put("numeroPoliza", getNumeroPoliza());
		attributes.put("nombre", getNombre());
		attributes.put("apellidos", getApellidos());
		attributes.put("rfc", getRfc());
		attributes.put("telefonoParticular", getTelefonoParticular());
		attributes.put("TelefonoCelular", getTelefonoCelular());
		attributes.put("email", getEmail());
		attributes.put("estado", getEstado());
		attributes.put("quejaRelacionada", getQuejaRelacionada());
		attributes.put("quejaRefiere", getQuejaRefiere());
		attributes.put("observaciones", getObservaciones());
		attributes.put("archivo", getArchivo());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("sistemaOrigen", getSistemaOrigen());
		attributes.put("claveAgente", getClaveAgente());
		attributes.put("negocio", getNegocio());
		attributes.put("emisor", getEmisor());
		attributes.put("claveQuejaRelacionada", getClaveQuejaRelacionada());
		attributes.put("claveQuejaRefiere", getClaveQuejaRefiere());
		attributes.put("razonSocial", getRazonSocial());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String clave = (String)attributes.get("clave");

		if (clave != null) {
			setClave(clave);
		}

		String tipo = (String)attributes.get("tipo");

		if (tipo != null) {
			setTipo(tipo);
		}

		Integer anio = (Integer)attributes.get("anio");

		if (anio != null) {
			setAnio(anio);
		}

		Integer folio = (Integer)attributes.get("folio");

		if (folio != null) {
			setFolio(folio);
		}

		String esUsted = (String)attributes.get("esUsted");

		if (esUsted != null) {
			setEsUsted(esUsted);
		}

		String numeroPoliza = (String)attributes.get("numeroPoliza");

		if (numeroPoliza != null) {
			setNumeroPoliza(numeroPoliza);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String apellidos = (String)attributes.get("apellidos");

		if (apellidos != null) {
			setApellidos(apellidos);
		}

		String rfc = (String)attributes.get("rfc");

		if (rfc != null) {
			setRfc(rfc);
		}

		String telefonoParticular = (String)attributes.get("telefonoParticular");

		if (telefonoParticular != null) {
			setTelefonoParticular(telefonoParticular);
		}

		String TelefonoCelular = (String)attributes.get("TelefonoCelular");

		if (TelefonoCelular != null) {
			setTelefonoCelular(TelefonoCelular);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		String quejaRelacionada = (String)attributes.get("quejaRelacionada");

		if (quejaRelacionada != null) {
			setQuejaRelacionada(quejaRelacionada);
		}

		String quejaRefiere = (String)attributes.get("quejaRefiere");

		if (quejaRefiere != null) {
			setQuejaRefiere(quejaRefiere);
		}

		String observaciones = (String)attributes.get("observaciones");

		if (observaciones != null) {
			setObservaciones(observaciones);
		}

		String archivo = (String)attributes.get("archivo");

		if (archivo != null) {
			setArchivo(archivo);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String sistemaOrigen = (String)attributes.get("sistemaOrigen");

		if (sistemaOrigen != null) {
			setSistemaOrigen(sistemaOrigen);
		}

		String claveAgente = (String)attributes.get("claveAgente");

		if (claveAgente != null) {
			setClaveAgente(claveAgente);
		}

		String negocio = (String)attributes.get("negocio");

		if (negocio != null) {
			setNegocio(negocio);
		}

		String emisor = (String)attributes.get("emisor");

		if (emisor != null) {
			setEmisor(emisor);
		}

		String claveQuejaRelacionada = (String)attributes.get(
				"claveQuejaRelacionada");

		if (claveQuejaRelacionada != null) {
			setClaveQuejaRelacionada(claveQuejaRelacionada);
		}

		String claveQuejaRefiere = (String)attributes.get("claveQuejaRefiere");

		if (claveQuejaRefiere != null) {
			setClaveQuejaRefiere(claveQuejaRefiere);
		}

		String razonSocial = (String)attributes.get("razonSocial");

		if (razonSocial != null) {
			setRazonSocial(razonSocial);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _queja.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _queja.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _queja.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _queja.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Queja> toCacheModel() {
		return _queja.toCacheModel();
	}

	@Override
	public int compareTo(Queja queja) {
		return _queja.compareTo(queja);
	}

	/**
	* Returns the anio of this queja.
	*
	* @return the anio of this queja
	*/
	@Override
	public int getAnio() {
		return _queja.getAnio();
	}

	/**
	* Returns the folio of this queja.
	*
	* @return the folio of this queja
	*/
	@Override
	public int getFolio() {
		return _queja.getFolio();
	}

	@Override
	public int hashCode() {
		return _queja.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _queja.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new QuejaWrapper((Queja)_queja.clone());
	}

	/**
	* Returns the apellidos of this queja.
	*
	* @return the apellidos of this queja
	*/
	@Override
	public java.lang.String getApellidos() {
		return _queja.getApellidos();
	}

	/**
	* Returns the archivo of this queja.
	*
	* @return the archivo of this queja
	*/
	@Override
	public java.lang.String getArchivo() {
		return _queja.getArchivo();
	}

	/**
	* Returns the clave of this queja.
	*
	* @return the clave of this queja
	*/
	@Override
	public java.lang.String getClave() {
		return _queja.getClave();
	}

	/**
	* Returns the clave agente of this queja.
	*
	* @return the clave agente of this queja
	*/
	@Override
	public java.lang.String getClaveAgente() {
		return _queja.getClaveAgente();
	}

	/**
	* Returns the clave queja refiere of this queja.
	*
	* @return the clave queja refiere of this queja
	*/
	@Override
	public java.lang.String getClaveQuejaRefiere() {
		return _queja.getClaveQuejaRefiere();
	}

	/**
	* Returns the clave queja relacionada of this queja.
	*
	* @return the clave queja relacionada of this queja
	*/
	@Override
	public java.lang.String getClaveQuejaRelacionada() {
		return _queja.getClaveQuejaRelacionada();
	}

	/**
	* Returns the email of this queja.
	*
	* @return the email of this queja
	*/
	@Override
	public java.lang.String getEmail() {
		return _queja.getEmail();
	}

	/**
	* Returns the emisor of this queja.
	*
	* @return the emisor of this queja
	*/
	@Override
	public java.lang.String getEmisor() {
		return _queja.getEmisor();
	}

	/**
	* Returns the es usted of this queja.
	*
	* @return the es usted of this queja
	*/
	@Override
	public java.lang.String getEsUsted() {
		return _queja.getEsUsted();
	}

	/**
	* Returns the estado of this queja.
	*
	* @return the estado of this queja
	*/
	@Override
	public java.lang.String getEstado() {
		return _queja.getEstado();
	}

	/**
	* Returns the negocio of this queja.
	*
	* @return the negocio of this queja
	*/
	@Override
	public java.lang.String getNegocio() {
		return _queja.getNegocio();
	}

	/**
	* Returns the nombre of this queja.
	*
	* @return the nombre of this queja
	*/
	@Override
	public java.lang.String getNombre() {
		return _queja.getNombre();
	}

	/**
	* Returns the numero poliza of this queja.
	*
	* @return the numero poliza of this queja
	*/
	@Override
	public java.lang.String getNumeroPoliza() {
		return _queja.getNumeroPoliza();
	}

	/**
	* Returns the observaciones of this queja.
	*
	* @return the observaciones of this queja
	*/
	@Override
	public java.lang.String getObservaciones() {
		return _queja.getObservaciones();
	}

	/**
	* Returns the primary key of this queja.
	*
	* @return the primary key of this queja
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _queja.getPrimaryKey();
	}

	/**
	* Returns the queja refiere of this queja.
	*
	* @return the queja refiere of this queja
	*/
	@Override
	public java.lang.String getQuejaRefiere() {
		return _queja.getQuejaRefiere();
	}

	/**
	* Returns the queja relacionada of this queja.
	*
	* @return the queja relacionada of this queja
	*/
	@Override
	public java.lang.String getQuejaRelacionada() {
		return _queja.getQuejaRelacionada();
	}

	/**
	* Returns the razon social of this queja.
	*
	* @return the razon social of this queja
	*/
	@Override
	public java.lang.String getRazonSocial() {
		return _queja.getRazonSocial();
	}

	/**
	* Returns the rfc of this queja.
	*
	* @return the rfc of this queja
	*/
	@Override
	public java.lang.String getRfc() {
		return _queja.getRfc();
	}

	/**
	* Returns the sistema origen of this queja.
	*
	* @return the sistema origen of this queja
	*/
	@Override
	public java.lang.String getSistemaOrigen() {
		return _queja.getSistemaOrigen();
	}

	/**
	* Returns the telefono celular of this queja.
	*
	* @return the telefono celular of this queja
	*/
	@Override
	public java.lang.String getTelefonoCelular() {
		return _queja.getTelefonoCelular();
	}

	/**
	* Returns the telefono particular of this queja.
	*
	* @return the telefono particular of this queja
	*/
	@Override
	public java.lang.String getTelefonoParticular() {
		return _queja.getTelefonoParticular();
	}

	/**
	* Returns the tipo of this queja.
	*
	* @return the tipo of this queja
	*/
	@Override
	public java.lang.String getTipo() {
		return _queja.getTipo();
	}

	@Override
	public java.lang.String toString() {
		return _queja.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _queja.toXmlString();
	}

	/**
	* Returns the fecha registro of this queja.
	*
	* @return the fecha registro of this queja
	*/
	@Override
	public Date getFechaRegistro() {
		return _queja.getFechaRegistro();
	}

	@Override
	public Queja toEscapedModel() {
		return new QuejaWrapper(_queja.toEscapedModel());
	}

	@Override
	public Queja toUnescapedModel() {
		return new QuejaWrapper(_queja.toUnescapedModel());
	}

	@Override
	public void persist() {
		_queja.persist();
	}

	/**
	* Sets the anio of this queja.
	*
	* @param anio the anio of this queja
	*/
	@Override
	public void setAnio(int anio) {
		_queja.setAnio(anio);
	}

	/**
	* Sets the apellidos of this queja.
	*
	* @param apellidos the apellidos of this queja
	*/
	@Override
	public void setApellidos(java.lang.String apellidos) {
		_queja.setApellidos(apellidos);
	}

	/**
	* Sets the archivo of this queja.
	*
	* @param archivo the archivo of this queja
	*/
	@Override
	public void setArchivo(java.lang.String archivo) {
		_queja.setArchivo(archivo);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_queja.setCachedModel(cachedModel);
	}

	/**
	* Sets the clave of this queja.
	*
	* @param clave the clave of this queja
	*/
	@Override
	public void setClave(java.lang.String clave) {
		_queja.setClave(clave);
	}

	/**
	* Sets the clave agente of this queja.
	*
	* @param claveAgente the clave agente of this queja
	*/
	@Override
	public void setClaveAgente(java.lang.String claveAgente) {
		_queja.setClaveAgente(claveAgente);
	}

	/**
	* Sets the clave queja refiere of this queja.
	*
	* @param claveQuejaRefiere the clave queja refiere of this queja
	*/
	@Override
	public void setClaveQuejaRefiere(java.lang.String claveQuejaRefiere) {
		_queja.setClaveQuejaRefiere(claveQuejaRefiere);
	}

	/**
	* Sets the clave queja relacionada of this queja.
	*
	* @param claveQuejaRelacionada the clave queja relacionada of this queja
	*/
	@Override
	public void setClaveQuejaRelacionada(java.lang.String claveQuejaRelacionada) {
		_queja.setClaveQuejaRelacionada(claveQuejaRelacionada);
	}

	/**
	* Sets the email of this queja.
	*
	* @param email the email of this queja
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_queja.setEmail(email);
	}

	/**
	* Sets the emisor of this queja.
	*
	* @param emisor the emisor of this queja
	*/
	@Override
	public void setEmisor(java.lang.String emisor) {
		_queja.setEmisor(emisor);
	}

	/**
	* Sets the es usted of this queja.
	*
	* @param esUsted the es usted of this queja
	*/
	@Override
	public void setEsUsted(java.lang.String esUsted) {
		_queja.setEsUsted(esUsted);
	}

	/**
	* Sets the estado of this queja.
	*
	* @param estado the estado of this queja
	*/
	@Override
	public void setEstado(java.lang.String estado) {
		_queja.setEstado(estado);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_queja.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_queja.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_queja.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the fecha registro of this queja.
	*
	* @param fechaRegistro the fecha registro of this queja
	*/
	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_queja.setFechaRegistro(fechaRegistro);
	}

	/**
	* Sets the folio of this queja.
	*
	* @param folio the folio of this queja
	*/
	@Override
	public void setFolio(int folio) {
		_queja.setFolio(folio);
	}

	/**
	* Sets the negocio of this queja.
	*
	* @param negocio the negocio of this queja
	*/
	@Override
	public void setNegocio(java.lang.String negocio) {
		_queja.setNegocio(negocio);
	}

	@Override
	public void setNew(boolean n) {
		_queja.setNew(n);
	}

	/**
	* Sets the nombre of this queja.
	*
	* @param nombre the nombre of this queja
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_queja.setNombre(nombre);
	}

	/**
	* Sets the numero poliza of this queja.
	*
	* @param numeroPoliza the numero poliza of this queja
	*/
	@Override
	public void setNumeroPoliza(java.lang.String numeroPoliza) {
		_queja.setNumeroPoliza(numeroPoliza);
	}

	/**
	* Sets the observaciones of this queja.
	*
	* @param observaciones the observaciones of this queja
	*/
	@Override
	public void setObservaciones(java.lang.String observaciones) {
		_queja.setObservaciones(observaciones);
	}

	/**
	* Sets the primary key of this queja.
	*
	* @param primaryKey the primary key of this queja
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_queja.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_queja.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the queja refiere of this queja.
	*
	* @param quejaRefiere the queja refiere of this queja
	*/
	@Override
	public void setQuejaRefiere(java.lang.String quejaRefiere) {
		_queja.setQuejaRefiere(quejaRefiere);
	}

	/**
	* Sets the queja relacionada of this queja.
	*
	* @param quejaRelacionada the queja relacionada of this queja
	*/
	@Override
	public void setQuejaRelacionada(java.lang.String quejaRelacionada) {
		_queja.setQuejaRelacionada(quejaRelacionada);
	}

	/**
	* Sets the razon social of this queja.
	*
	* @param razonSocial the razon social of this queja
	*/
	@Override
	public void setRazonSocial(java.lang.String razonSocial) {
		_queja.setRazonSocial(razonSocial);
	}

	/**
	* Sets the rfc of this queja.
	*
	* @param rfc the rfc of this queja
	*/
	@Override
	public void setRfc(java.lang.String rfc) {
		_queja.setRfc(rfc);
	}

	/**
	* Sets the sistema origen of this queja.
	*
	* @param sistemaOrigen the sistema origen of this queja
	*/
	@Override
	public void setSistemaOrigen(java.lang.String sistemaOrigen) {
		_queja.setSistemaOrigen(sistemaOrigen);
	}

	/**
	* Sets the telefono celular of this queja.
	*
	* @param TelefonoCelular the telefono celular of this queja
	*/
	@Override
	public void setTelefonoCelular(java.lang.String TelefonoCelular) {
		_queja.setTelefonoCelular(TelefonoCelular);
	}

	/**
	* Sets the telefono particular of this queja.
	*
	* @param telefonoParticular the telefono particular of this queja
	*/
	@Override
	public void setTelefonoParticular(java.lang.String telefonoParticular) {
		_queja.setTelefonoParticular(telefonoParticular);
	}

	/**
	* Sets the tipo of this queja.
	*
	* @param tipo the tipo of this queja
	*/
	@Override
	public void setTipo(java.lang.String tipo) {
		_queja.setTipo(tipo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuejaWrapper)) {
			return false;
		}

		QuejaWrapper quejaWrapper = (QuejaWrapper)obj;

		if (Objects.equals(_queja, quejaWrapper._queja)) {
			return true;
		}

		return false;
	}

	@Override
	public Queja getWrappedModel() {
		return _queja;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _queja.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _queja.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_queja.resetOriginalValues();
	}

	private final Queja _queja;
}