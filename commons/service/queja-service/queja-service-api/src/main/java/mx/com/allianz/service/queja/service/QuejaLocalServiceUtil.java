/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.queja.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Queja. This utility wraps
 * {@link mx.com.allianz.service.queja.service.impl.QuejaLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see QuejaLocalService
 * @see mx.com.allianz.service.queja.service.base.QuejaLocalServiceBaseImpl
 * @see mx.com.allianz.service.queja.service.impl.QuejaLocalServiceImpl
 * @generated
 */
@ProviderType
public class QuejaLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link mx.com.allianz.service.queja.service.impl.QuejaLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of quejas.
	*
	* @return the number of quejas
	*/
	public static int getQuejasCount() {
		return getService().getQuejasCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.queja.model.impl.QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.queja.model.impl.QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the quejas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.queja.model.impl.QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @return the range of quejas
	*/
	public static java.util.List<mx.com.allianz.service.queja.model.Queja> getQuejas(
		int start, int end) {
		return getService().getQuejas(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the queja to the database. Also notifies the appropriate model listeners.
	*
	* @param queja the queja
	* @return the queja that was added
	*/
	public static mx.com.allianz.service.queja.model.Queja addQueja(
		mx.com.allianz.service.queja.model.Queja queja) {
		return getService().addQueja(queja);
	}

	/**
	* Creates a new queja with the primary key. Does not add the queja to the database.
	*
	* @param clave the primary key for the new queja
	* @return the new queja
	*/
	public static mx.com.allianz.service.queja.model.Queja createQueja(
		java.lang.String clave) {
		return getService().createQueja(clave);
	}

	/**
	* Deletes the queja with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clave the primary key of the queja
	* @return the queja that was removed
	* @throws PortalException if a queja with the primary key could not be found
	*/
	public static mx.com.allianz.service.queja.model.Queja deleteQueja(
		java.lang.String clave)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteQueja(clave);
	}

	/**
	* Deletes the queja from the database. Also notifies the appropriate model listeners.
	*
	* @param queja the queja
	* @return the queja that was removed
	*/
	public static mx.com.allianz.service.queja.model.Queja deleteQueja(
		mx.com.allianz.service.queja.model.Queja queja) {
		return getService().deleteQueja(queja);
	}

	public static mx.com.allianz.service.queja.model.Queja fetchQueja(
		java.lang.String clave) {
		return getService().fetchQueja(clave);
	}

	/**
	* Returns the queja with the primary key.
	*
	* @param clave the primary key of the queja
	* @return the queja
	* @throws PortalException if a queja with the primary key could not be found
	*/
	public static mx.com.allianz.service.queja.model.Queja getQueja(
		java.lang.String clave)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getQueja(clave);
	}

	/**
	* Updates the queja in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param queja the queja
	* @return the queja that was updated
	*/
	public static mx.com.allianz.service.queja.model.Queja updateQueja(
		mx.com.allianz.service.queja.model.Queja queja) {
		return getService().updateQueja(queja);
	}

	public static QuejaLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<QuejaLocalService, QuejaLocalService> _serviceTracker =
		ServiceTrackerFactory.open(QuejaLocalService.class);
}