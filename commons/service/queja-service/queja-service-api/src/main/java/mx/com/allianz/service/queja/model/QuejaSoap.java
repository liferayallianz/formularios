/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.queja.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class QuejaSoap implements Serializable {
	public static QuejaSoap toSoapModel(Queja model) {
		QuejaSoap soapModel = new QuejaSoap();

		soapModel.setClave(model.getClave());
		soapModel.setTipo(model.getTipo());
		soapModel.setAnio(model.getAnio());
		soapModel.setFolio(model.getFolio());
		soapModel.setEsUsted(model.getEsUsted());
		soapModel.setNumeroPoliza(model.getNumeroPoliza());
		soapModel.setNombre(model.getNombre());
		soapModel.setApellidos(model.getApellidos());
		soapModel.setRfc(model.getRfc());
		soapModel.setTelefonoParticular(model.getTelefonoParticular());
		soapModel.setTelefonoCelular(model.getTelefonoCelular());
		soapModel.setEmail(model.getEmail());
		soapModel.setEstado(model.getEstado());
		soapModel.setQuejaRelacionada(model.getQuejaRelacionada());
		soapModel.setQuejaRefiere(model.getQuejaRefiere());
		soapModel.setObservaciones(model.getObservaciones());
		soapModel.setArchivo(model.getArchivo());
		soapModel.setFechaRegistro(model.getFechaRegistro());
		soapModel.setSistemaOrigen(model.getSistemaOrigen());
		soapModel.setClaveAgente(model.getClaveAgente());
		soapModel.setNegocio(model.getNegocio());
		soapModel.setEmisor(model.getEmisor());
		soapModel.setClaveQuejaRelacionada(model.getClaveQuejaRelacionada());
		soapModel.setClaveQuejaRefiere(model.getClaveQuejaRefiere());
		soapModel.setRazonSocial(model.getRazonSocial());

		return soapModel;
	}

	public static QuejaSoap[] toSoapModels(Queja[] models) {
		QuejaSoap[] soapModels = new QuejaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static QuejaSoap[][] toSoapModels(Queja[][] models) {
		QuejaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new QuejaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new QuejaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static QuejaSoap[] toSoapModels(List<Queja> models) {
		List<QuejaSoap> soapModels = new ArrayList<QuejaSoap>(models.size());

		for (Queja model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new QuejaSoap[soapModels.size()]);
	}

	public QuejaSoap() {
	}

	public String getPrimaryKey() {
		return _clave;
	}

	public void setPrimaryKey(String pk) {
		setClave(pk);
	}

	public String getClave() {
		return _clave;
	}

	public void setClave(String clave) {
		_clave = clave;
	}

	public String getTipo() {
		return _tipo;
	}

	public void setTipo(String tipo) {
		_tipo = tipo;
	}

	public int getAnio() {
		return _anio;
	}

	public void setAnio(int anio) {
		_anio = anio;
	}

	public int getFolio() {
		return _folio;
	}

	public void setFolio(int folio) {
		_folio = folio;
	}

	public String getEsUsted() {
		return _esUsted;
	}

	public void setEsUsted(String esUsted) {
		_esUsted = esUsted;
	}

	public String getNumeroPoliza() {
		return _numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		_numeroPoliza = numeroPoliza;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getApellidos() {
		return _apellidos;
	}

	public void setApellidos(String apellidos) {
		_apellidos = apellidos;
	}

	public String getRfc() {
		return _rfc;
	}

	public void setRfc(String rfc) {
		_rfc = rfc;
	}

	public String getTelefonoParticular() {
		return _telefonoParticular;
	}

	public void setTelefonoParticular(String telefonoParticular) {
		_telefonoParticular = telefonoParticular;
	}

	public String getTelefonoCelular() {
		return _TelefonoCelular;
	}

	public void setTelefonoCelular(String TelefonoCelular) {
		_TelefonoCelular = TelefonoCelular;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getEstado() {
		return _estado;
	}

	public void setEstado(String estado) {
		_estado = estado;
	}

	public String getQuejaRelacionada() {
		return _quejaRelacionada;
	}

	public void setQuejaRelacionada(String quejaRelacionada) {
		_quejaRelacionada = quejaRelacionada;
	}

	public String getQuejaRefiere() {
		return _quejaRefiere;
	}

	public void setQuejaRefiere(String quejaRefiere) {
		_quejaRefiere = quejaRefiere;
	}

	public String getObservaciones() {
		return _observaciones;
	}

	public void setObservaciones(String observaciones) {
		_observaciones = observaciones;
	}

	public String getArchivo() {
		return _archivo;
	}

	public void setArchivo(String archivo) {
		_archivo = archivo;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	public String getSistemaOrigen() {
		return _sistemaOrigen;
	}

	public void setSistemaOrigen(String sistemaOrigen) {
		_sistemaOrigen = sistemaOrigen;
	}

	public String getClaveAgente() {
		return _claveAgente;
	}

	public void setClaveAgente(String claveAgente) {
		_claveAgente = claveAgente;
	}

	public String getNegocio() {
		return _negocio;
	}

	public void setNegocio(String negocio) {
		_negocio = negocio;
	}

	public String getEmisor() {
		return _emisor;
	}

	public void setEmisor(String emisor) {
		_emisor = emisor;
	}

	public String getClaveQuejaRelacionada() {
		return _claveQuejaRelacionada;
	}

	public void setClaveQuejaRelacionada(String claveQuejaRelacionada) {
		_claveQuejaRelacionada = claveQuejaRelacionada;
	}

	public String getClaveQuejaRefiere() {
		return _claveQuejaRefiere;
	}

	public void setClaveQuejaRefiere(String claveQuejaRefiere) {
		_claveQuejaRefiere = claveQuejaRefiere;
	}

	public String getRazonSocial() {
		return _razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		_razonSocial = razonSocial;
	}

	private String _clave;
	private String _tipo;
	private int _anio;
	private int _folio;
	private String _esUsted;
	private String _numeroPoliza;
	private String _nombre;
	private String _apellidos;
	private String _rfc;
	private String _telefonoParticular;
	private String _TelefonoCelular;
	private String _email;
	private String _estado;
	private String _quejaRelacionada;
	private String _quejaRefiere;
	private String _observaciones;
	private String _archivo;
	private Date _fechaRegistro;
	private String _sistemaOrigen;
	private String _claveAgente;
	private String _negocio;
	private String _emisor;
	private String _claveQuejaRelacionada;
	private String _claveQuejaRefiere;
	private String _razonSocial;
}