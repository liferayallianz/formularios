/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.queja.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.queja.exception.NoSuchQuejaException;
import mx.com.allianz.service.queja.model.Queja;

/**
 * The persistence interface for the queja service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.queja.service.persistence.impl.QuejaPersistenceImpl
 * @see QuejaUtil
 * @generated
 */
@ProviderType
public interface QuejaPersistence extends BasePersistence<Queja> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QuejaUtil} to access the queja persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the quejas where tipo = &#63;.
	*
	* @param tipo the tipo
	* @return the matching quejas
	*/
	public java.util.List<Queja> findByfindByTipo(java.lang.String tipo);

	/**
	* Returns a range of all the quejas where tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipo the tipo
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @return the range of matching quejas
	*/
	public java.util.List<Queja> findByfindByTipo(java.lang.String tipo,
		int start, int end);

	/**
	* Returns an ordered range of all the quejas where tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipo the tipo
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quejas
	*/
	public java.util.List<Queja> findByfindByTipo(java.lang.String tipo,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Queja> orderByComparator);

	/**
	* Returns an ordered range of all the quejas where tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipo the tipo
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching quejas
	*/
	public java.util.List<Queja> findByfindByTipo(java.lang.String tipo,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Queja> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first queja in the ordered set where tipo = &#63;.
	*
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja
	* @throws NoSuchQuejaException if a matching queja could not be found
	*/
	public Queja findByfindByTipo_First(java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator<Queja> orderByComparator)
		throws NoSuchQuejaException;

	/**
	* Returns the first queja in the ordered set where tipo = &#63;.
	*
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja, or <code>null</code> if a matching queja could not be found
	*/
	public Queja fetchByfindByTipo_First(java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator<Queja> orderByComparator);

	/**
	* Returns the last queja in the ordered set where tipo = &#63;.
	*
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja
	* @throws NoSuchQuejaException if a matching queja could not be found
	*/
	public Queja findByfindByTipo_Last(java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator<Queja> orderByComparator)
		throws NoSuchQuejaException;

	/**
	* Returns the last queja in the ordered set where tipo = &#63;.
	*
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja, or <code>null</code> if a matching queja could not be found
	*/
	public Queja fetchByfindByTipo_Last(java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator<Queja> orderByComparator);

	/**
	* Returns the quejas before and after the current queja in the ordered set where tipo = &#63;.
	*
	* @param clave the primary key of the current queja
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next queja
	* @throws NoSuchQuejaException if a queja with the primary key could not be found
	*/
	public Queja[] findByfindByTipo_PrevAndNext(java.lang.String clave,
		java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator<Queja> orderByComparator)
		throws NoSuchQuejaException;

	/**
	* Removes all the quejas where tipo = &#63; from the database.
	*
	* @param tipo the tipo
	*/
	public void removeByfindByTipo(java.lang.String tipo);

	/**
	* Returns the number of quejas where tipo = &#63;.
	*
	* @param tipo the tipo
	* @return the number of matching quejas
	*/
	public int countByfindByTipo(java.lang.String tipo);

	/**
	* Caches the queja in the entity cache if it is enabled.
	*
	* @param queja the queja
	*/
	public void cacheResult(Queja queja);

	/**
	* Caches the quejas in the entity cache if it is enabled.
	*
	* @param quejas the quejas
	*/
	public void cacheResult(java.util.List<Queja> quejas);

	/**
	* Creates a new queja with the primary key. Does not add the queja to the database.
	*
	* @param clave the primary key for the new queja
	* @return the new queja
	*/
	public Queja create(java.lang.String clave);

	/**
	* Removes the queja with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clave the primary key of the queja
	* @return the queja that was removed
	* @throws NoSuchQuejaException if a queja with the primary key could not be found
	*/
	public Queja remove(java.lang.String clave) throws NoSuchQuejaException;

	public Queja updateImpl(Queja queja);

	/**
	* Returns the queja with the primary key or throws a {@link NoSuchQuejaException} if it could not be found.
	*
	* @param clave the primary key of the queja
	* @return the queja
	* @throws NoSuchQuejaException if a queja with the primary key could not be found
	*/
	public Queja findByPrimaryKey(java.lang.String clave)
		throws NoSuchQuejaException;

	/**
	* Returns the queja with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param clave the primary key of the queja
	* @return the queja, or <code>null</code> if a queja with the primary key could not be found
	*/
	public Queja fetchByPrimaryKey(java.lang.String clave);

	@Override
	public java.util.Map<java.io.Serializable, Queja> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the quejas.
	*
	* @return the quejas
	*/
	public java.util.List<Queja> findAll();

	/**
	* Returns a range of all the quejas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @return the range of quejas
	*/
	public java.util.List<Queja> findAll(int start, int end);

	/**
	* Returns an ordered range of all the quejas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quejas
	*/
	public java.util.List<Queja> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Queja> orderByComparator);

	/**
	* Returns an ordered range of all the quejas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quejas
	* @param end the upper bound of the range of quejas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of quejas
	*/
	public java.util.List<Queja> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Queja> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the quejas from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of quejas.
	*
	* @return the number of quejas
	*/
	public int countAll();
}