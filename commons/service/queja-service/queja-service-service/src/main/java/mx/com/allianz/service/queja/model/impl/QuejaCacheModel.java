/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.queja.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.queja.model.Queja;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Queja in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Queja
 * @generated
 */
@ProviderType
public class QuejaCacheModel implements CacheModel<Queja>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuejaCacheModel)) {
			return false;
		}

		QuejaCacheModel quejaCacheModel = (QuejaCacheModel)obj;

		if (clave.equals(quejaCacheModel.clave)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, clave);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(51);

		sb.append("{clave=");
		sb.append(clave);
		sb.append(", tipo=");
		sb.append(tipo);
		sb.append(", anio=");
		sb.append(anio);
		sb.append(", folio=");
		sb.append(folio);
		sb.append(", esUsted=");
		sb.append(esUsted);
		sb.append(", numeroPoliza=");
		sb.append(numeroPoliza);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", apellidos=");
		sb.append(apellidos);
		sb.append(", rfc=");
		sb.append(rfc);
		sb.append(", telefonoParticular=");
		sb.append(telefonoParticular);
		sb.append(", TelefonoCelular=");
		sb.append(TelefonoCelular);
		sb.append(", email=");
		sb.append(email);
		sb.append(", estado=");
		sb.append(estado);
		sb.append(", quejaRelacionada=");
		sb.append(quejaRelacionada);
		sb.append(", quejaRefiere=");
		sb.append(quejaRefiere);
		sb.append(", observaciones=");
		sb.append(observaciones);
		sb.append(", archivo=");
		sb.append(archivo);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append(", sistemaOrigen=");
		sb.append(sistemaOrigen);
		sb.append(", claveAgente=");
		sb.append(claveAgente);
		sb.append(", negocio=");
		sb.append(negocio);
		sb.append(", emisor=");
		sb.append(emisor);
		sb.append(", claveQuejaRelacionada=");
		sb.append(claveQuejaRelacionada);
		sb.append(", claveQuejaRefiere=");
		sb.append(claveQuejaRefiere);
		sb.append(", razonSocial=");
		sb.append(razonSocial);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Queja toEntityModel() {
		QuejaImpl quejaImpl = new QuejaImpl();

		if (clave == null) {
			quejaImpl.setClave(StringPool.BLANK);
		}
		else {
			quejaImpl.setClave(clave);
		}

		if (tipo == null) {
			quejaImpl.setTipo(StringPool.BLANK);
		}
		else {
			quejaImpl.setTipo(tipo);
		}

		quejaImpl.setAnio(anio);
		quejaImpl.setFolio(folio);

		if (esUsted == null) {
			quejaImpl.setEsUsted(StringPool.BLANK);
		}
		else {
			quejaImpl.setEsUsted(esUsted);
		}

		if (numeroPoliza == null) {
			quejaImpl.setNumeroPoliza(StringPool.BLANK);
		}
		else {
			quejaImpl.setNumeroPoliza(numeroPoliza);
		}

		if (nombre == null) {
			quejaImpl.setNombre(StringPool.BLANK);
		}
		else {
			quejaImpl.setNombre(nombre);
		}

		if (apellidos == null) {
			quejaImpl.setApellidos(StringPool.BLANK);
		}
		else {
			quejaImpl.setApellidos(apellidos);
		}

		if (rfc == null) {
			quejaImpl.setRfc(StringPool.BLANK);
		}
		else {
			quejaImpl.setRfc(rfc);
		}

		if (telefonoParticular == null) {
			quejaImpl.setTelefonoParticular(StringPool.BLANK);
		}
		else {
			quejaImpl.setTelefonoParticular(telefonoParticular);
		}

		if (TelefonoCelular == null) {
			quejaImpl.setTelefonoCelular(StringPool.BLANK);
		}
		else {
			quejaImpl.setTelefonoCelular(TelefonoCelular);
		}

		if (email == null) {
			quejaImpl.setEmail(StringPool.BLANK);
		}
		else {
			quejaImpl.setEmail(email);
		}

		if (estado == null) {
			quejaImpl.setEstado(StringPool.BLANK);
		}
		else {
			quejaImpl.setEstado(estado);
		}

		if (quejaRelacionada == null) {
			quejaImpl.setQuejaRelacionada(StringPool.BLANK);
		}
		else {
			quejaImpl.setQuejaRelacionada(quejaRelacionada);
		}

		if (quejaRefiere == null) {
			quejaImpl.setQuejaRefiere(StringPool.BLANK);
		}
		else {
			quejaImpl.setQuejaRefiere(quejaRefiere);
		}

		if (observaciones == null) {
			quejaImpl.setObservaciones(StringPool.BLANK);
		}
		else {
			quejaImpl.setObservaciones(observaciones);
		}

		if (archivo == null) {
			quejaImpl.setArchivo(StringPool.BLANK);
		}
		else {
			quejaImpl.setArchivo(archivo);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			quejaImpl.setFechaRegistro(null);
		}
		else {
			quejaImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		if (sistemaOrigen == null) {
			quejaImpl.setSistemaOrigen(StringPool.BLANK);
		}
		else {
			quejaImpl.setSistemaOrigen(sistemaOrigen);
		}

		if (claveAgente == null) {
			quejaImpl.setClaveAgente(StringPool.BLANK);
		}
		else {
			quejaImpl.setClaveAgente(claveAgente);
		}

		if (negocio == null) {
			quejaImpl.setNegocio(StringPool.BLANK);
		}
		else {
			quejaImpl.setNegocio(negocio);
		}

		if (emisor == null) {
			quejaImpl.setEmisor(StringPool.BLANK);
		}
		else {
			quejaImpl.setEmisor(emisor);
		}

		if (claveQuejaRelacionada == null) {
			quejaImpl.setClaveQuejaRelacionada(StringPool.BLANK);
		}
		else {
			quejaImpl.setClaveQuejaRelacionada(claveQuejaRelacionada);
		}

		if (claveQuejaRefiere == null) {
			quejaImpl.setClaveQuejaRefiere(StringPool.BLANK);
		}
		else {
			quejaImpl.setClaveQuejaRefiere(claveQuejaRefiere);
		}

		if (razonSocial == null) {
			quejaImpl.setRazonSocial(StringPool.BLANK);
		}
		else {
			quejaImpl.setRazonSocial(razonSocial);
		}

		quejaImpl.resetOriginalValues();

		return quejaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		clave = objectInput.readUTF();
		tipo = objectInput.readUTF();

		anio = objectInput.readInt();

		folio = objectInput.readInt();
		esUsted = objectInput.readUTF();
		numeroPoliza = objectInput.readUTF();
		nombre = objectInput.readUTF();
		apellidos = objectInput.readUTF();
		rfc = objectInput.readUTF();
		telefonoParticular = objectInput.readUTF();
		TelefonoCelular = objectInput.readUTF();
		email = objectInput.readUTF();
		estado = objectInput.readUTF();
		quejaRelacionada = objectInput.readUTF();
		quejaRefiere = objectInput.readUTF();
		observaciones = objectInput.readUTF();
		archivo = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
		sistemaOrigen = objectInput.readUTF();
		claveAgente = objectInput.readUTF();
		negocio = objectInput.readUTF();
		emisor = objectInput.readUTF();
		claveQuejaRelacionada = objectInput.readUTF();
		claveQuejaRefiere = objectInput.readUTF();
		razonSocial = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (clave == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(clave);
		}

		if (tipo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipo);
		}

		objectOutput.writeInt(anio);

		objectOutput.writeInt(folio);

		if (esUsted == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(esUsted);
		}

		if (numeroPoliza == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(numeroPoliza);
		}

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (apellidos == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidos);
		}

		if (rfc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(rfc);
		}

		if (telefonoParticular == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(telefonoParticular);
		}

		if (TelefonoCelular == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(TelefonoCelular);
		}

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (estado == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estado);
		}

		if (quejaRelacionada == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(quejaRelacionada);
		}

		if (quejaRefiere == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(quejaRefiere);
		}

		if (observaciones == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(observaciones);
		}

		if (archivo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(archivo);
		}

		objectOutput.writeLong(fechaRegistro);

		if (sistemaOrigen == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(sistemaOrigen);
		}

		if (claveAgente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(claveAgente);
		}

		if (negocio == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(negocio);
		}

		if (emisor == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(emisor);
		}

		if (claveQuejaRelacionada == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(claveQuejaRelacionada);
		}

		if (claveQuejaRefiere == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(claveQuejaRefiere);
		}

		if (razonSocial == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(razonSocial);
		}
	}

	public String clave;
	public String tipo;
	public int anio;
	public int folio;
	public String esUsted;
	public String numeroPoliza;
	public String nombre;
	public String apellidos;
	public String rfc;
	public String telefonoParticular;
	public String TelefonoCelular;
	public String email;
	public String estado;
	public String quejaRelacionada;
	public String quejaRefiere;
	public String observaciones;
	public String archivo;
	public long fechaRegistro;
	public String sistemaOrigen;
	public String claveAgente;
	public String negocio;
	public String emisor;
	public String claveQuejaRelacionada;
	public String claveQuejaRefiere;
	public String razonSocial;
}