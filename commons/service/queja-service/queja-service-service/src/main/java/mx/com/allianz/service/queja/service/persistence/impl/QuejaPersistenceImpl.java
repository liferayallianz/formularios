/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.queja.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.queja.exception.NoSuchQuejaException;
import mx.com.allianz.service.queja.model.Queja;
import mx.com.allianz.service.queja.model.impl.QuejaImpl;
import mx.com.allianz.service.queja.model.impl.QuejaModelImpl;
import mx.com.allianz.service.queja.service.persistence.QuejaPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the queja service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuejaPersistence
 * @see mx.com.allianz.service.queja.service.persistence.QuejaUtil
 * @generated
 */
@ProviderType
public class QuejaPersistenceImpl extends BasePersistenceImpl<Queja>
	implements QuejaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link QuejaUtil} to access the queja persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = QuejaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QuejaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaModelImpl.FINDER_CACHE_ENABLED, QuejaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QuejaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaModelImpl.FINDER_CACHE_ENABLED, QuejaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QuejaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYTIPO =
		new FinderPath(QuejaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaModelImpl.FINDER_CACHE_ENABLED, QuejaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByfindByTipo",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYTIPO =
		new FinderPath(QuejaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaModelImpl.FINDER_CACHE_ENABLED, QuejaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByfindByTipo",
			new String[] { String.class.getName() },
			QuejaModelImpl.TIPO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYTIPO = new FinderPath(QuejaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByfindByTipo",
			new String[] { String.class.getName() });

	/**
	 * Returns all the quejas where tipo = &#63;.
	 *
	 * @param tipo the tipo
	 * @return the matching quejas
	 */
	@Override
	public List<Queja> findByfindByTipo(String tipo) {
		return findByfindByTipo(tipo, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the quejas where tipo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipo the tipo
	 * @param start the lower bound of the range of quejas
	 * @param end the upper bound of the range of quejas (not inclusive)
	 * @return the range of matching quejas
	 */
	@Override
	public List<Queja> findByfindByTipo(String tipo, int start, int end) {
		return findByfindByTipo(tipo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the quejas where tipo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipo the tipo
	 * @param start the lower bound of the range of quejas
	 * @param end the upper bound of the range of quejas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching quejas
	 */
	@Override
	public List<Queja> findByfindByTipo(String tipo, int start, int end,
		OrderByComparator<Queja> orderByComparator) {
		return findByfindByTipo(tipo, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the quejas where tipo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipo the tipo
	 * @param start the lower bound of the range of quejas
	 * @param end the upper bound of the range of quejas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching quejas
	 */
	@Override
	public List<Queja> findByfindByTipo(String tipo, int start, int end,
		OrderByComparator<Queja> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYTIPO;
			finderArgs = new Object[] { tipo };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYTIPO;
			finderArgs = new Object[] { tipo, start, end, orderByComparator };
		}

		List<Queja> list = null;

		if (retrieveFromCache) {
			list = (List<Queja>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Queja queja : list) {
					if (!Objects.equals(tipo, queja.getTipo())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUEJA_WHERE);

			boolean bindTipo = false;

			if (tipo == null) {
				query.append(_FINDER_COLUMN_FINDBYTIPO_TIPO_1);
			}
			else if (tipo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYTIPO_TIPO_3);
			}
			else {
				bindTipo = true;

				query.append(_FINDER_COLUMN_FINDBYTIPO_TIPO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuejaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTipo) {
					qPos.add(tipo);
				}

				if (!pagination) {
					list = (List<Queja>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Queja>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first queja in the ordered set where tipo = &#63;.
	 *
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching queja
	 * @throws NoSuchQuejaException if a matching queja could not be found
	 */
	@Override
	public Queja findByfindByTipo_First(String tipo,
		OrderByComparator<Queja> orderByComparator) throws NoSuchQuejaException {
		Queja queja = fetchByfindByTipo_First(tipo, orderByComparator);

		if (queja != null) {
			return queja;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipo=");
		msg.append(tipo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuejaException(msg.toString());
	}

	/**
	 * Returns the first queja in the ordered set where tipo = &#63;.
	 *
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching queja, or <code>null</code> if a matching queja could not be found
	 */
	@Override
	public Queja fetchByfindByTipo_First(String tipo,
		OrderByComparator<Queja> orderByComparator) {
		List<Queja> list = findByfindByTipo(tipo, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last queja in the ordered set where tipo = &#63;.
	 *
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching queja
	 * @throws NoSuchQuejaException if a matching queja could not be found
	 */
	@Override
	public Queja findByfindByTipo_Last(String tipo,
		OrderByComparator<Queja> orderByComparator) throws NoSuchQuejaException {
		Queja queja = fetchByfindByTipo_Last(tipo, orderByComparator);

		if (queja != null) {
			return queja;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipo=");
		msg.append(tipo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuejaException(msg.toString());
	}

	/**
	 * Returns the last queja in the ordered set where tipo = &#63;.
	 *
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching queja, or <code>null</code> if a matching queja could not be found
	 */
	@Override
	public Queja fetchByfindByTipo_Last(String tipo,
		OrderByComparator<Queja> orderByComparator) {
		int count = countByfindByTipo(tipo);

		if (count == 0) {
			return null;
		}

		List<Queja> list = findByfindByTipo(tipo, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the quejas before and after the current queja in the ordered set where tipo = &#63;.
	 *
	 * @param clave the primary key of the current queja
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next queja
	 * @throws NoSuchQuejaException if a queja with the primary key could not be found
	 */
	@Override
	public Queja[] findByfindByTipo_PrevAndNext(String clave, String tipo,
		OrderByComparator<Queja> orderByComparator) throws NoSuchQuejaException {
		Queja queja = findByPrimaryKey(clave);

		Session session = null;

		try {
			session = openSession();

			Queja[] array = new QuejaImpl[3];

			array[0] = getByfindByTipo_PrevAndNext(session, queja, tipo,
					orderByComparator, true);

			array[1] = queja;

			array[2] = getByfindByTipo_PrevAndNext(session, queja, tipo,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Queja getByfindByTipo_PrevAndNext(Session session, Queja queja,
		String tipo, OrderByComparator<Queja> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_QUEJA_WHERE);

		boolean bindTipo = false;

		if (tipo == null) {
			query.append(_FINDER_COLUMN_FINDBYTIPO_TIPO_1);
		}
		else if (tipo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYTIPO_TIPO_3);
		}
		else {
			bindTipo = true;

			query.append(_FINDER_COLUMN_FINDBYTIPO_TIPO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(QuejaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindTipo) {
			qPos.add(tipo);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(queja);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Queja> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the quejas where tipo = &#63; from the database.
	 *
	 * @param tipo the tipo
	 */
	@Override
	public void removeByfindByTipo(String tipo) {
		for (Queja queja : findByfindByTipo(tipo, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(queja);
		}
	}

	/**
	 * Returns the number of quejas where tipo = &#63;.
	 *
	 * @param tipo the tipo
	 * @return the number of matching quejas
	 */
	@Override
	public int countByfindByTipo(String tipo) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYTIPO;

		Object[] finderArgs = new Object[] { tipo };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUEJA_WHERE);

			boolean bindTipo = false;

			if (tipo == null) {
				query.append(_FINDER_COLUMN_FINDBYTIPO_TIPO_1);
			}
			else if (tipo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYTIPO_TIPO_3);
			}
			else {
				bindTipo = true;

				query.append(_FINDER_COLUMN_FINDBYTIPO_TIPO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTipo) {
					qPos.add(tipo);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYTIPO_TIPO_1 = "queja.tipo IS NULL";
	private static final String _FINDER_COLUMN_FINDBYTIPO_TIPO_2 = "queja.tipo = ?";
	private static final String _FINDER_COLUMN_FINDBYTIPO_TIPO_3 = "(queja.tipo IS NULL OR queja.tipo = '')";

	public QuejaPersistenceImpl() {
		setModelClass(Queja.class);
	}

	/**
	 * Caches the queja in the entity cache if it is enabled.
	 *
	 * @param queja the queja
	 */
	@Override
	public void cacheResult(Queja queja) {
		entityCache.putResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaImpl.class, queja.getPrimaryKey(), queja);

		queja.resetOriginalValues();
	}

	/**
	 * Caches the quejas in the entity cache if it is enabled.
	 *
	 * @param quejas the quejas
	 */
	@Override
	public void cacheResult(List<Queja> quejas) {
		for (Queja queja : quejas) {
			if (entityCache.getResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
						QuejaImpl.class, queja.getPrimaryKey()) == null) {
				cacheResult(queja);
			}
			else {
				queja.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all quejas.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(QuejaImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the queja.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Queja queja) {
		entityCache.removeResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaImpl.class, queja.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Queja> quejas) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Queja queja : quejas) {
			entityCache.removeResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
				QuejaImpl.class, queja.getPrimaryKey());
		}
	}

	/**
	 * Creates a new queja with the primary key. Does not add the queja to the database.
	 *
	 * @param clave the primary key for the new queja
	 * @return the new queja
	 */
	@Override
	public Queja create(String clave) {
		Queja queja = new QuejaImpl();

		queja.setNew(true);
		queja.setPrimaryKey(clave);

		return queja;
	}

	/**
	 * Removes the queja with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param clave the primary key of the queja
	 * @return the queja that was removed
	 * @throws NoSuchQuejaException if a queja with the primary key could not be found
	 */
	@Override
	public Queja remove(String clave) throws NoSuchQuejaException {
		return remove((Serializable)clave);
	}

	/**
	 * Removes the queja with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the queja
	 * @return the queja that was removed
	 * @throws NoSuchQuejaException if a queja with the primary key could not be found
	 */
	@Override
	public Queja remove(Serializable primaryKey) throws NoSuchQuejaException {
		Session session = null;

		try {
			session = openSession();

			Queja queja = (Queja)session.get(QuejaImpl.class, primaryKey);

			if (queja == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchQuejaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(queja);
		}
		catch (NoSuchQuejaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Queja removeImpl(Queja queja) {
		queja = toUnwrappedModel(queja);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(queja)) {
				queja = (Queja)session.get(QuejaImpl.class,
						queja.getPrimaryKeyObj());
			}

			if (queja != null) {
				session.delete(queja);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (queja != null) {
			clearCache(queja);
		}

		return queja;
	}

	@Override
	public Queja updateImpl(Queja queja) {
		queja = toUnwrappedModel(queja);

		boolean isNew = queja.isNew();

		QuejaModelImpl quejaModelImpl = (QuejaModelImpl)queja;

		Session session = null;

		try {
			session = openSession();

			if (queja.isNew()) {
				session.save(queja);

				queja.setNew(false);
			}
			else {
				queja = (Queja)session.merge(queja);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !QuejaModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((quejaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYTIPO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { quejaModelImpl.getOriginalTipo() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYTIPO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYTIPO,
					args);

				args = new Object[] { quejaModelImpl.getTipo() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYTIPO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYTIPO,
					args);
			}
		}

		entityCache.putResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaImpl.class, queja.getPrimaryKey(), queja, false);

		queja.resetOriginalValues();

		return queja;
	}

	protected Queja toUnwrappedModel(Queja queja) {
		if (queja instanceof QuejaImpl) {
			return queja;
		}

		QuejaImpl quejaImpl = new QuejaImpl();

		quejaImpl.setNew(queja.isNew());
		quejaImpl.setPrimaryKey(queja.getPrimaryKey());

		quejaImpl.setClave(queja.getClave());
		quejaImpl.setTipo(queja.getTipo());
		quejaImpl.setAnio(queja.getAnio());
		quejaImpl.setFolio(queja.getFolio());
		quejaImpl.setEsUsted(queja.getEsUsted());
		quejaImpl.setNumeroPoliza(queja.getNumeroPoliza());
		quejaImpl.setNombre(queja.getNombre());
		quejaImpl.setApellidos(queja.getApellidos());
		quejaImpl.setRfc(queja.getRfc());
		quejaImpl.setTelefonoParticular(queja.getTelefonoParticular());
		quejaImpl.setTelefonoCelular(queja.getTelefonoCelular());
		quejaImpl.setEmail(queja.getEmail());
		quejaImpl.setEstado(queja.getEstado());
		quejaImpl.setQuejaRelacionada(queja.getQuejaRelacionada());
		quejaImpl.setQuejaRefiere(queja.getQuejaRefiere());
		quejaImpl.setObservaciones(queja.getObservaciones());
		quejaImpl.setArchivo(queja.getArchivo());
		quejaImpl.setFechaRegistro(queja.getFechaRegistro());
		quejaImpl.setSistemaOrigen(queja.getSistemaOrigen());
		quejaImpl.setClaveAgente(queja.getClaveAgente());
		quejaImpl.setNegocio(queja.getNegocio());
		quejaImpl.setEmisor(queja.getEmisor());
		quejaImpl.setClaveQuejaRelacionada(queja.getClaveQuejaRelacionada());
		quejaImpl.setClaveQuejaRefiere(queja.getClaveQuejaRefiere());
		quejaImpl.setRazonSocial(queja.getRazonSocial());

		return quejaImpl;
	}

	/**
	 * Returns the queja with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the queja
	 * @return the queja
	 * @throws NoSuchQuejaException if a queja with the primary key could not be found
	 */
	@Override
	public Queja findByPrimaryKey(Serializable primaryKey)
		throws NoSuchQuejaException {
		Queja queja = fetchByPrimaryKey(primaryKey);

		if (queja == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchQuejaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return queja;
	}

	/**
	 * Returns the queja with the primary key or throws a {@link NoSuchQuejaException} if it could not be found.
	 *
	 * @param clave the primary key of the queja
	 * @return the queja
	 * @throws NoSuchQuejaException if a queja with the primary key could not be found
	 */
	@Override
	public Queja findByPrimaryKey(String clave) throws NoSuchQuejaException {
		return findByPrimaryKey((Serializable)clave);
	}

	/**
	 * Returns the queja with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the queja
	 * @return the queja, or <code>null</code> if a queja with the primary key could not be found
	 */
	@Override
	public Queja fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
				QuejaImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Queja queja = (Queja)serializable;

		if (queja == null) {
			Session session = null;

			try {
				session = openSession();

				queja = (Queja)session.get(QuejaImpl.class, primaryKey);

				if (queja != null) {
					cacheResult(queja);
				}
				else {
					entityCache.putResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
						QuejaImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
					QuejaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return queja;
	}

	/**
	 * Returns the queja with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param clave the primary key of the queja
	 * @return the queja, or <code>null</code> if a queja with the primary key could not be found
	 */
	@Override
	public Queja fetchByPrimaryKey(String clave) {
		return fetchByPrimaryKey((Serializable)clave);
	}

	@Override
	public Map<Serializable, Queja> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Queja> map = new HashMap<Serializable, Queja>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Queja queja = fetchByPrimaryKey(primaryKey);

			if (queja != null) {
				map.put(primaryKey, queja);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
					QuejaImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Queja)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 4) +
				1);

		query.append(_SQL_SELECT_QUEJA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(StringPool.APOSTROPHE);
			query.append((String)primaryKey);
			query.append(StringPool.APOSTROPHE);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Queja queja : (List<Queja>)q.list()) {
				map.put(queja.getPrimaryKeyObj(), queja);

				cacheResult(queja);

				uncachedPrimaryKeys.remove(queja.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(QuejaModelImpl.ENTITY_CACHE_ENABLED,
					QuejaImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the quejas.
	 *
	 * @return the quejas
	 */
	@Override
	public List<Queja> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the quejas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of quejas
	 * @param end the upper bound of the range of quejas (not inclusive)
	 * @return the range of quejas
	 */
	@Override
	public List<Queja> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the quejas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of quejas
	 * @param end the upper bound of the range of quejas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of quejas
	 */
	@Override
	public List<Queja> findAll(int start, int end,
		OrderByComparator<Queja> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the quejas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of quejas
	 * @param end the upper bound of the range of quejas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of quejas
	 */
	@Override
	public List<Queja> findAll(int start, int end,
		OrderByComparator<Queja> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Queja> list = null;

		if (retrieveFromCache) {
			list = (List<Queja>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_QUEJA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_QUEJA;

				if (pagination) {
					sql = sql.concat(QuejaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Queja>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Queja>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the quejas from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Queja queja : findAll()) {
			remove(queja);
		}
	}

	/**
	 * Returns the number of quejas.
	 *
	 * @return the number of quejas
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_QUEJA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return QuejaModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the queja persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(QuejaImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_QUEJA = "SELECT queja FROM Queja queja";
	private static final String _SQL_SELECT_QUEJA_WHERE_PKS_IN = "SELECT queja FROM Queja queja WHERE clave IN (";
	private static final String _SQL_SELECT_QUEJA_WHERE = "SELECT queja FROM Queja queja WHERE ";
	private static final String _SQL_COUNT_QUEJA = "SELECT COUNT(queja) FROM Queja queja";
	private static final String _SQL_COUNT_QUEJA_WHERE = "SELECT COUNT(queja) FROM Queja queja WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "queja.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Queja exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Queja exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(QuejaPersistenceImpl.class);
}