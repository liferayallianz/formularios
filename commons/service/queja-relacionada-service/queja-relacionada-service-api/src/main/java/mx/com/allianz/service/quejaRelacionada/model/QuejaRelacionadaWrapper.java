/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejaRelacionada.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link QuejaRelacionada}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRelacionada
 * @generated
 */
@ProviderType
public class QuejaRelacionadaWrapper implements QuejaRelacionada,
	ModelWrapper<QuejaRelacionada> {
	public QuejaRelacionadaWrapper(QuejaRelacionada quejaRelacionada) {
		_quejaRelacionada = quejaRelacionada;
	}

	@Override
	public Class<?> getModelClass() {
		return QuejaRelacionada.class;
	}

	@Override
	public String getModelClassName() {
		return QuejaRelacionada.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("claveRelacion", getClaveRelacion());
		attributes.put("descripcion", getDescripcion());
		attributes.put("ordena", getOrdena());
		attributes.put("ustedTiene", getUstedTiene());
		attributes.put("esUsted", getEsUsted());
		attributes.put("codigoProblema", getCodigoProblema());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String claveRelacion = (String)attributes.get("claveRelacion");

		if (claveRelacion != null) {
			setClaveRelacion(claveRelacion);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		String ordena = (String)attributes.get("ordena");

		if (ordena != null) {
			setOrdena(ordena);
		}

		String ustedTiene = (String)attributes.get("ustedTiene");

		if (ustedTiene != null) {
			setUstedTiene(ustedTiene);
		}

		String esUsted = (String)attributes.get("esUsted");

		if (esUsted != null) {
			setEsUsted(esUsted);
		}

		String codigoProblema = (String)attributes.get("codigoProblema");

		if (codigoProblema != null) {
			setCodigoProblema(codigoProblema);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _quejaRelacionada.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _quejaRelacionada.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _quejaRelacionada.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _quejaRelacionada.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<QuejaRelacionada> toCacheModel() {
		return _quejaRelacionada.toCacheModel();
	}

	@Override
	public int compareTo(QuejaRelacionada quejaRelacionada) {
		return _quejaRelacionada.compareTo(quejaRelacionada);
	}

	@Override
	public int hashCode() {
		return _quejaRelacionada.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _quejaRelacionada.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new QuejaRelacionadaWrapper((QuejaRelacionada)_quejaRelacionada.clone());
	}

	/**
	* Returns the clave relacion of this queja relacionada.
	*
	* @return the clave relacion of this queja relacionada
	*/
	@Override
	public java.lang.String getClaveRelacion() {
		return _quejaRelacionada.getClaveRelacion();
	}

	/**
	* Returns the codigo problema of this queja relacionada.
	*
	* @return the codigo problema of this queja relacionada
	*/
	@Override
	public java.lang.String getCodigoProblema() {
		return _quejaRelacionada.getCodigoProblema();
	}

	/**
	* Returns the descripcion of this queja relacionada.
	*
	* @return the descripcion of this queja relacionada
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _quejaRelacionada.getDescripcion();
	}

	/**
	* Returns the es usted of this queja relacionada.
	*
	* @return the es usted of this queja relacionada
	*/
	@Override
	public java.lang.String getEsUsted() {
		return _quejaRelacionada.getEsUsted();
	}

	/**
	* Returns the ordena of this queja relacionada.
	*
	* @return the ordena of this queja relacionada
	*/
	@Override
	public java.lang.String getOrdena() {
		return _quejaRelacionada.getOrdena();
	}

	/**
	* Returns the primary key of this queja relacionada.
	*
	* @return the primary key of this queja relacionada
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _quejaRelacionada.getPrimaryKey();
	}

	/**
	* Returns the usted tiene of this queja relacionada.
	*
	* @return the usted tiene of this queja relacionada
	*/
	@Override
	public java.lang.String getUstedTiene() {
		return _quejaRelacionada.getUstedTiene();
	}

	@Override
	public java.lang.String toString() {
		return _quejaRelacionada.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _quejaRelacionada.toXmlString();
	}

	@Override
	public QuejaRelacionada toEscapedModel() {
		return new QuejaRelacionadaWrapper(_quejaRelacionada.toEscapedModel());
	}

	@Override
	public QuejaRelacionada toUnescapedModel() {
		return new QuejaRelacionadaWrapper(_quejaRelacionada.toUnescapedModel());
	}

	@Override
	public void persist() {
		_quejaRelacionada.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_quejaRelacionada.setCachedModel(cachedModel);
	}

	/**
	* Sets the clave relacion of this queja relacionada.
	*
	* @param claveRelacion the clave relacion of this queja relacionada
	*/
	@Override
	public void setClaveRelacion(java.lang.String claveRelacion) {
		_quejaRelacionada.setClaveRelacion(claveRelacion);
	}

	/**
	* Sets the codigo problema of this queja relacionada.
	*
	* @param codigoProblema the codigo problema of this queja relacionada
	*/
	@Override
	public void setCodigoProblema(java.lang.String codigoProblema) {
		_quejaRelacionada.setCodigoProblema(codigoProblema);
	}

	/**
	* Sets the descripcion of this queja relacionada.
	*
	* @param descripcion the descripcion of this queja relacionada
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_quejaRelacionada.setDescripcion(descripcion);
	}

	/**
	* Sets the es usted of this queja relacionada.
	*
	* @param esUsted the es usted of this queja relacionada
	*/
	@Override
	public void setEsUsted(java.lang.String esUsted) {
		_quejaRelacionada.setEsUsted(esUsted);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_quejaRelacionada.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_quejaRelacionada.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_quejaRelacionada.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_quejaRelacionada.setNew(n);
	}

	/**
	* Sets the ordena of this queja relacionada.
	*
	* @param ordena the ordena of this queja relacionada
	*/
	@Override
	public void setOrdena(java.lang.String ordena) {
		_quejaRelacionada.setOrdena(ordena);
	}

	/**
	* Sets the primary key of this queja relacionada.
	*
	* @param primaryKey the primary key of this queja relacionada
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_quejaRelacionada.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_quejaRelacionada.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the usted tiene of this queja relacionada.
	*
	* @param ustedTiene the usted tiene of this queja relacionada
	*/
	@Override
	public void setUstedTiene(java.lang.String ustedTiene) {
		_quejaRelacionada.setUstedTiene(ustedTiene);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuejaRelacionadaWrapper)) {
			return false;
		}

		QuejaRelacionadaWrapper quejaRelacionadaWrapper = (QuejaRelacionadaWrapper)obj;

		if (Objects.equals(_quejaRelacionada,
					quejaRelacionadaWrapper._quejaRelacionada)) {
			return true;
		}

		return false;
	}

	@Override
	public QuejaRelacionada getWrappedModel() {
		return _quejaRelacionada;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _quejaRelacionada.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _quejaRelacionada.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_quejaRelacionada.resetOriginalValues();
	}

	private final QuejaRelacionada _quejaRelacionada;
}