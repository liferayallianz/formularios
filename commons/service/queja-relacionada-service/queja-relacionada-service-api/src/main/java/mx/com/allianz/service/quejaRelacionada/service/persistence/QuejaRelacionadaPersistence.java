/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejaRelacionada.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.quejaRelacionada.exception.NoSuchQuejaRelacionadaException;
import mx.com.allianz.service.quejaRelacionada.model.QuejaRelacionada;

/**
 * The persistence interface for the queja relacionada service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.quejaRelacionada.service.persistence.impl.QuejaRelacionadaPersistenceImpl
 * @see QuejaRelacionadaUtil
 * @generated
 */
@ProviderType
public interface QuejaRelacionadaPersistence extends BasePersistence<QuejaRelacionada> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QuejaRelacionadaUtil} to access the queja relacionada persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the queja relacionadas where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching queja relacionadas
	*/
	public java.util.List<QuejaRelacionada> findByfindByDescripcion(
		java.lang.String descripcion);

	/**
	* Returns a range of all the queja relacionadas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @return the range of matching queja relacionadas
	*/
	public java.util.List<QuejaRelacionada> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end);

	/**
	* Returns an ordered range of all the queja relacionadas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching queja relacionadas
	*/
	public java.util.List<QuejaRelacionada> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRelacionada> orderByComparator);

	/**
	* Returns an ordered range of all the queja relacionadas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching queja relacionadas
	*/
	public java.util.List<QuejaRelacionada> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRelacionada> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja relacionada
	* @throws NoSuchQuejaRelacionadaException if a matching queja relacionada could not be found
	*/
	public QuejaRelacionada findByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRelacionada> orderByComparator)
		throws NoSuchQuejaRelacionadaException;

	/**
	* Returns the first queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja relacionada, or <code>null</code> if a matching queja relacionada could not be found
	*/
	public QuejaRelacionada fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRelacionada> orderByComparator);

	/**
	* Returns the last queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja relacionada
	* @throws NoSuchQuejaRelacionadaException if a matching queja relacionada could not be found
	*/
	public QuejaRelacionada findByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRelacionada> orderByComparator)
		throws NoSuchQuejaRelacionadaException;

	/**
	* Returns the last queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja relacionada, or <code>null</code> if a matching queja relacionada could not be found
	*/
	public QuejaRelacionada fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRelacionada> orderByComparator);

	/**
	* Returns the queja relacionadas before and after the current queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param claveRelacion the primary key of the current queja relacionada
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next queja relacionada
	* @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	*/
	public QuejaRelacionada[] findByfindByDescripcion_PrevAndNext(
		java.lang.String claveRelacion, java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRelacionada> orderByComparator)
		throws NoSuchQuejaRelacionadaException;

	/**
	* Removes all the queja relacionadas where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public void removeByfindByDescripcion(java.lang.String descripcion);

	/**
	* Returns the number of queja relacionadas where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching queja relacionadas
	*/
	public int countByfindByDescripcion(java.lang.String descripcion);

	/**
	* Caches the queja relacionada in the entity cache if it is enabled.
	*
	* @param quejaRelacionada the queja relacionada
	*/
	public void cacheResult(QuejaRelacionada quejaRelacionada);

	/**
	* Caches the queja relacionadas in the entity cache if it is enabled.
	*
	* @param quejaRelacionadas the queja relacionadas
	*/
	public void cacheResult(java.util.List<QuejaRelacionada> quejaRelacionadas);

	/**
	* Creates a new queja relacionada with the primary key. Does not add the queja relacionada to the database.
	*
	* @param claveRelacion the primary key for the new queja relacionada
	* @return the new queja relacionada
	*/
	public QuejaRelacionada create(java.lang.String claveRelacion);

	/**
	* Removes the queja relacionada with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param claveRelacion the primary key of the queja relacionada
	* @return the queja relacionada that was removed
	* @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	*/
	public QuejaRelacionada remove(java.lang.String claveRelacion)
		throws NoSuchQuejaRelacionadaException;

	public QuejaRelacionada updateImpl(QuejaRelacionada quejaRelacionada);

	/**
	* Returns the queja relacionada with the primary key or throws a {@link NoSuchQuejaRelacionadaException} if it could not be found.
	*
	* @param claveRelacion the primary key of the queja relacionada
	* @return the queja relacionada
	* @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	*/
	public QuejaRelacionada findByPrimaryKey(java.lang.String claveRelacion)
		throws NoSuchQuejaRelacionadaException;

	/**
	* Returns the queja relacionada with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param claveRelacion the primary key of the queja relacionada
	* @return the queja relacionada, or <code>null</code> if a queja relacionada with the primary key could not be found
	*/
	public QuejaRelacionada fetchByPrimaryKey(java.lang.String claveRelacion);

	@Override
	public java.util.Map<java.io.Serializable, QuejaRelacionada> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the queja relacionadas.
	*
	* @return the queja relacionadas
	*/
	public java.util.List<QuejaRelacionada> findAll();

	/**
	* Returns a range of all the queja relacionadas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @return the range of queja relacionadas
	*/
	public java.util.List<QuejaRelacionada> findAll(int start, int end);

	/**
	* Returns an ordered range of all the queja relacionadas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of queja relacionadas
	*/
	public java.util.List<QuejaRelacionada> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRelacionada> orderByComparator);

	/**
	* Returns an ordered range of all the queja relacionadas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of queja relacionadas
	*/
	public java.util.List<QuejaRelacionada> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRelacionada> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the queja relacionadas from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of queja relacionadas.
	*
	* @return the number of queja relacionadas
	*/
	public int countAll();
}