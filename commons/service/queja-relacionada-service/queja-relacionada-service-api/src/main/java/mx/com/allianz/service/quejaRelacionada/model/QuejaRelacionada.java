/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejaRelacionada.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the QuejaRelacionada service. Represents a row in the &quot;QJA_RELACIONADA&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRelacionadaModel
 * @see mx.com.allianz.service.quejaRelacionada.model.impl.QuejaRelacionadaImpl
 * @see mx.com.allianz.service.quejaRelacionada.model.impl.QuejaRelacionadaModelImpl
 * @generated
 */
@ImplementationClassName("mx.com.allianz.service.quejaRelacionada.model.impl.QuejaRelacionadaImpl")
@ProviderType
public interface QuejaRelacionada extends QuejaRelacionadaModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link mx.com.allianz.service.quejaRelacionada.model.impl.QuejaRelacionadaImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<QuejaRelacionada, String> CLAVE_RELACION_ACCESSOR =
		new Accessor<QuejaRelacionada, String>() {
			@Override
			public String get(QuejaRelacionada quejaRelacionada) {
				return quejaRelacionada.getClaveRelacion();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<QuejaRelacionada> getTypeClass() {
				return QuejaRelacionada.class;
			}
		};
}