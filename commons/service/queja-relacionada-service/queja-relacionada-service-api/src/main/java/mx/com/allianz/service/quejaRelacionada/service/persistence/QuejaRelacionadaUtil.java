/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejaRelacionada.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.quejaRelacionada.model.QuejaRelacionada;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the queja relacionada service. This utility wraps {@link mx.com.allianz.service.quejaRelacionada.service.persistence.impl.QuejaRelacionadaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRelacionadaPersistence
 * @see mx.com.allianz.service.quejaRelacionada.service.persistence.impl.QuejaRelacionadaPersistenceImpl
 * @generated
 */
@ProviderType
public class QuejaRelacionadaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(QuejaRelacionada quejaRelacionada) {
		getPersistence().clearCache(quejaRelacionada);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<QuejaRelacionada> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<QuejaRelacionada> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<QuejaRelacionada> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<QuejaRelacionada> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static QuejaRelacionada update(QuejaRelacionada quejaRelacionada) {
		return getPersistence().update(quejaRelacionada);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static QuejaRelacionada update(QuejaRelacionada quejaRelacionada,
		ServiceContext serviceContext) {
		return getPersistence().update(quejaRelacionada, serviceContext);
	}

	/**
	* Returns all the queja relacionadas where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching queja relacionadas
	*/
	public static List<QuejaRelacionada> findByfindByDescripcion(
		java.lang.String descripcion) {
		return getPersistence().findByfindByDescripcion(descripcion);
	}

	/**
	* Returns a range of all the queja relacionadas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @return the range of matching queja relacionadas
	*/
	public static List<QuejaRelacionada> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end) {
		return getPersistence().findByfindByDescripcion(descripcion, start, end);
	}

	/**
	* Returns an ordered range of all the queja relacionadas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching queja relacionadas
	*/
	public static List<QuejaRelacionada> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<QuejaRelacionada> orderByComparator) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the queja relacionadas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching queja relacionadas
	*/
	public static List<QuejaRelacionada> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<QuejaRelacionada> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja relacionada
	* @throws NoSuchQuejaRelacionadaException if a matching queja relacionada could not be found
	*/
	public static QuejaRelacionada findByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator)
		throws mx.com.allianz.service.quejaRelacionada.exception.NoSuchQuejaRelacionadaException {
		return getPersistence()
				   .findByfindByDescripcion_First(descripcion, orderByComparator);
	}

	/**
	* Returns the first queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja relacionada, or <code>null</code> if a matching queja relacionada could not be found
	*/
	public static QuejaRelacionada fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_First(descripcion,
			orderByComparator);
	}

	/**
	* Returns the last queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja relacionada
	* @throws NoSuchQuejaRelacionadaException if a matching queja relacionada could not be found
	*/
	public static QuejaRelacionada findByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator)
		throws mx.com.allianz.service.quejaRelacionada.exception.NoSuchQuejaRelacionadaException {
		return getPersistence()
				   .findByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the last queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja relacionada, or <code>null</code> if a matching queja relacionada could not be found
	*/
	public static QuejaRelacionada fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the queja relacionadas before and after the current queja relacionada in the ordered set where descripcion = &#63;.
	*
	* @param claveRelacion the primary key of the current queja relacionada
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next queja relacionada
	* @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	*/
	public static QuejaRelacionada[] findByfindByDescripcion_PrevAndNext(
		java.lang.String claveRelacion, java.lang.String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator)
		throws mx.com.allianz.service.quejaRelacionada.exception.NoSuchQuejaRelacionadaException {
		return getPersistence()
				   .findByfindByDescripcion_PrevAndNext(claveRelacion,
			descripcion, orderByComparator);
	}

	/**
	* Removes all the queja relacionadas where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public static void removeByfindByDescripcion(java.lang.String descripcion) {
		getPersistence().removeByfindByDescripcion(descripcion);
	}

	/**
	* Returns the number of queja relacionadas where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching queja relacionadas
	*/
	public static int countByfindByDescripcion(java.lang.String descripcion) {
		return getPersistence().countByfindByDescripcion(descripcion);
	}

	/**
	* Caches the queja relacionada in the entity cache if it is enabled.
	*
	* @param quejaRelacionada the queja relacionada
	*/
	public static void cacheResult(QuejaRelacionada quejaRelacionada) {
		getPersistence().cacheResult(quejaRelacionada);
	}

	/**
	* Caches the queja relacionadas in the entity cache if it is enabled.
	*
	* @param quejaRelacionadas the queja relacionadas
	*/
	public static void cacheResult(List<QuejaRelacionada> quejaRelacionadas) {
		getPersistence().cacheResult(quejaRelacionadas);
	}

	/**
	* Creates a new queja relacionada with the primary key. Does not add the queja relacionada to the database.
	*
	* @param claveRelacion the primary key for the new queja relacionada
	* @return the new queja relacionada
	*/
	public static QuejaRelacionada create(java.lang.String claveRelacion) {
		return getPersistence().create(claveRelacion);
	}

	/**
	* Removes the queja relacionada with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param claveRelacion the primary key of the queja relacionada
	* @return the queja relacionada that was removed
	* @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	*/
	public static QuejaRelacionada remove(java.lang.String claveRelacion)
		throws mx.com.allianz.service.quejaRelacionada.exception.NoSuchQuejaRelacionadaException {
		return getPersistence().remove(claveRelacion);
	}

	public static QuejaRelacionada updateImpl(QuejaRelacionada quejaRelacionada) {
		return getPersistence().updateImpl(quejaRelacionada);
	}

	/**
	* Returns the queja relacionada with the primary key or throws a {@link NoSuchQuejaRelacionadaException} if it could not be found.
	*
	* @param claveRelacion the primary key of the queja relacionada
	* @return the queja relacionada
	* @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	*/
	public static QuejaRelacionada findByPrimaryKey(
		java.lang.String claveRelacion)
		throws mx.com.allianz.service.quejaRelacionada.exception.NoSuchQuejaRelacionadaException {
		return getPersistence().findByPrimaryKey(claveRelacion);
	}

	/**
	* Returns the queja relacionada with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param claveRelacion the primary key of the queja relacionada
	* @return the queja relacionada, or <code>null</code> if a queja relacionada with the primary key could not be found
	*/
	public static QuejaRelacionada fetchByPrimaryKey(
		java.lang.String claveRelacion) {
		return getPersistence().fetchByPrimaryKey(claveRelacion);
	}

	public static java.util.Map<java.io.Serializable, QuejaRelacionada> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the queja relacionadas.
	*
	* @return the queja relacionadas
	*/
	public static List<QuejaRelacionada> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the queja relacionadas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @return the range of queja relacionadas
	*/
	public static List<QuejaRelacionada> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the queja relacionadas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of queja relacionadas
	*/
	public static List<QuejaRelacionada> findAll(int start, int end,
		OrderByComparator<QuejaRelacionada> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the queja relacionadas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja relacionadas
	* @param end the upper bound of the range of queja relacionadas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of queja relacionadas
	*/
	public static List<QuejaRelacionada> findAll(int start, int end,
		OrderByComparator<QuejaRelacionada> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the queja relacionadas from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of queja relacionadas.
	*
	* @return the number of queja relacionadas
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static QuejaRelacionadaPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<QuejaRelacionadaPersistence, QuejaRelacionadaPersistence> _serviceTracker =
		ServiceTrackerFactory.open(QuejaRelacionadaPersistence.class);
}