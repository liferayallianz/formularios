/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejaRelacionada.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class QuejaRelacionadaSoap implements Serializable {
	public static QuejaRelacionadaSoap toSoapModel(QuejaRelacionada model) {
		QuejaRelacionadaSoap soapModel = new QuejaRelacionadaSoap();

		soapModel.setClaveRelacion(model.getClaveRelacion());
		soapModel.setDescripcion(model.getDescripcion());
		soapModel.setOrdena(model.getOrdena());
		soapModel.setUstedTiene(model.getUstedTiene());
		soapModel.setEsUsted(model.getEsUsted());
		soapModel.setCodigoProblema(model.getCodigoProblema());

		return soapModel;
	}

	public static QuejaRelacionadaSoap[] toSoapModels(QuejaRelacionada[] models) {
		QuejaRelacionadaSoap[] soapModels = new QuejaRelacionadaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static QuejaRelacionadaSoap[][] toSoapModels(
		QuejaRelacionada[][] models) {
		QuejaRelacionadaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new QuejaRelacionadaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new QuejaRelacionadaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static QuejaRelacionadaSoap[] toSoapModels(
		List<QuejaRelacionada> models) {
		List<QuejaRelacionadaSoap> soapModels = new ArrayList<QuejaRelacionadaSoap>(models.size());

		for (QuejaRelacionada model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new QuejaRelacionadaSoap[soapModels.size()]);
	}

	public QuejaRelacionadaSoap() {
	}

	public String getPrimaryKey() {
		return _claveRelacion;
	}

	public void setPrimaryKey(String pk) {
		setClaveRelacion(pk);
	}

	public String getClaveRelacion() {
		return _claveRelacion;
	}

	public void setClaveRelacion(String claveRelacion) {
		_claveRelacion = claveRelacion;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	public String getOrdena() {
		return _ordena;
	}

	public void setOrdena(String ordena) {
		_ordena = ordena;
	}

	public String getUstedTiene() {
		return _ustedTiene;
	}

	public void setUstedTiene(String ustedTiene) {
		_ustedTiene = ustedTiene;
	}

	public String getEsUsted() {
		return _esUsted;
	}

	public void setEsUsted(String esUsted) {
		_esUsted = esUsted;
	}

	public String getCodigoProblema() {
		return _codigoProblema;
	}

	public void setCodigoProblema(String codigoProblema) {
		_codigoProblema = codigoProblema;
	}

	private String _claveRelacion;
	private String _descripcion;
	private String _ordena;
	private String _ustedTiene;
	private String _esUsted;
	private String _codigoProblema;
}