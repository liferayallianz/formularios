package mx.com.allianz.service.rest.quejarelacionada;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.QuejaRelacionadaDTO;
import mx.com.allianz.service.quejaRelacionada.service.QuejaRelacionadaLocalServiceUtil;


/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio
 * QuejaRelacionadaDTO que permite visualizar una lista de datos, filtra el orden de
 * la b\u00fasqueda y muestra el ordenamiento conforme al filtro.
 * 
 *
 * @author TestingZone
 * @version 1.0
 * @since 2017-01-1
 * 
 * 
 */

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.quejarelacionada")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)
public class QuejaRelacionadaRest extends Application {
	
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todas las quejas 
	 * del catalogo.
	 * 
	 * @return getAllQuejasRelacionada()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getQuejarelacionada")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<QuejaRelacionadaDTO> getQuejarelacionada(){
		
		return getAllQuejasRelacionada();
		
	}
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los Proveedores del
	 * catalogo.
	 * 
	 * @return getAllProveedores().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.

	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findQuejarelacionada")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	
	public List<QuejaRelacionadaDTO>findQuejaRelacionadaBy(
			
			@DefaultValue("") @QueryParam("clave") String clave, 
			@DefaultValue("") @QueryParam("descripcion") String descripcion,
			@DefaultValue("-1") @QueryParam("ordena") String ordena,
			@DefaultValue("") @QueryParam("usted_Tiene") String ustedTiene, 
			@DefaultValue("") @QueryParam("es_Usted") String esUsted,
			@DefaultValue("") @QueryParam("codigo_Problema") String codigoProblema){
		
		return getAllQuejasRelacionada().stream()
		
	
	
	.filter((quejarelacionada) -> clave.isEmpty() || quejarelacionada.getClave().contains(clave))
	.filter((quejarelacionada) -> descripcion.isEmpty() || quejarelacionada.getDescripcion().contains(descripcion))
	.filter((quejarelacionada) -> ordena.isEmpty() || quejarelacionada.getOrdena() == ordena)
	.filter((quejarelacionada) -> ustedTiene.isEmpty() || quejarelacionada.getUstedTiene().contains(ustedTiene))
	.filter((quejarelacionada) -> esUsted.isEmpty() || quejarelacionada.getEsUsted().contains(esUsted))
	.filter((quejarelacionada) -> codigoProblema.isEmpty() || quejarelacionada.getCodigoProblema().contains(codigoProblema))

	.collect(Collectors.toList());
	}
	

	private List<QuejaRelacionadaDTO> getAllQuejasRelacionada() {
		
		return QuejaRelacionadaLocalServiceUtil.getQuejaRelacionadas(-1, -1).stream()
				.map(quejarelacionada -> new QuejaRelacionadaDTO(quejarelacionada.getClaveRelacion(),
						quejarelacionada.getDescripcion(), quejarelacionada.getOrdena(), 
						quejarelacionada.getUstedTiene(), quejarelacionada.getEsUsted(),
						quejarelacionada.getCodigoProblema())).collect(Collectors.toList());


	}

}
