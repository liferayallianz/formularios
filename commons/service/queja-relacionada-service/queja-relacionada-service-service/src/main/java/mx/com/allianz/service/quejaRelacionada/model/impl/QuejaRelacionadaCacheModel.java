/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejaRelacionada.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.quejaRelacionada.model.QuejaRelacionada;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing QuejaRelacionada in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRelacionada
 * @generated
 */
@ProviderType
public class QuejaRelacionadaCacheModel implements CacheModel<QuejaRelacionada>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuejaRelacionadaCacheModel)) {
			return false;
		}

		QuejaRelacionadaCacheModel quejaRelacionadaCacheModel = (QuejaRelacionadaCacheModel)obj;

		if (claveRelacion.equals(quejaRelacionadaCacheModel.claveRelacion)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, claveRelacion);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{claveRelacion=");
		sb.append(claveRelacion);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append(", ordena=");
		sb.append(ordena);
		sb.append(", ustedTiene=");
		sb.append(ustedTiene);
		sb.append(", esUsted=");
		sb.append(esUsted);
		sb.append(", codigoProblema=");
		sb.append(codigoProblema);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public QuejaRelacionada toEntityModel() {
		QuejaRelacionadaImpl quejaRelacionadaImpl = new QuejaRelacionadaImpl();

		if (claveRelacion == null) {
			quejaRelacionadaImpl.setClaveRelacion(StringPool.BLANK);
		}
		else {
			quejaRelacionadaImpl.setClaveRelacion(claveRelacion);
		}

		if (descripcion == null) {
			quejaRelacionadaImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			quejaRelacionadaImpl.setDescripcion(descripcion);
		}

		if (ordena == null) {
			quejaRelacionadaImpl.setOrdena(StringPool.BLANK);
		}
		else {
			quejaRelacionadaImpl.setOrdena(ordena);
		}

		if (ustedTiene == null) {
			quejaRelacionadaImpl.setUstedTiene(StringPool.BLANK);
		}
		else {
			quejaRelacionadaImpl.setUstedTiene(ustedTiene);
		}

		if (esUsted == null) {
			quejaRelacionadaImpl.setEsUsted(StringPool.BLANK);
		}
		else {
			quejaRelacionadaImpl.setEsUsted(esUsted);
		}

		if (codigoProblema == null) {
			quejaRelacionadaImpl.setCodigoProblema(StringPool.BLANK);
		}
		else {
			quejaRelacionadaImpl.setCodigoProblema(codigoProblema);
		}

		quejaRelacionadaImpl.resetOriginalValues();

		return quejaRelacionadaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		claveRelacion = objectInput.readUTF();
		descripcion = objectInput.readUTF();
		ordena = objectInput.readUTF();
		ustedTiene = objectInput.readUTF();
		esUsted = objectInput.readUTF();
		codigoProblema = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (claveRelacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(claveRelacion);
		}

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}

		if (ordena == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ordena);
		}

		if (ustedTiene == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ustedTiene);
		}

		if (esUsted == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(esUsted);
		}

		if (codigoProblema == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoProblema);
		}
	}

	public String claveRelacion;
	public String descripcion;
	public String ordena;
	public String ustedTiene;
	public String esUsted;
	public String codigoProblema;
}