/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejaRelacionada.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.quejaRelacionada.exception.NoSuchQuejaRelacionadaException;
import mx.com.allianz.service.quejaRelacionada.model.QuejaRelacionada;
import mx.com.allianz.service.quejaRelacionada.model.impl.QuejaRelacionadaImpl;
import mx.com.allianz.service.quejaRelacionada.model.impl.QuejaRelacionadaModelImpl;
import mx.com.allianz.service.quejaRelacionada.service.persistence.QuejaRelacionadaPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the queja relacionada service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRelacionadaPersistence
 * @see mx.com.allianz.service.quejaRelacionada.service.persistence.QuejaRelacionadaUtil
 * @generated
 */
@ProviderType
public class QuejaRelacionadaPersistenceImpl extends BasePersistenceImpl<QuejaRelacionada>
	implements QuejaRelacionadaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link QuejaRelacionadaUtil} to access the queja relacionada persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = QuejaRelacionadaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRelacionadaModelImpl.FINDER_CACHE_ENABLED,
			QuejaRelacionadaImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRelacionadaModelImpl.FINDER_CACHE_ENABLED,
			QuejaRelacionadaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRelacionadaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRelacionadaModelImpl.FINDER_CACHE_ENABLED,
			QuejaRelacionadaImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByfindByDescripcion",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRelacionadaModelImpl.FINDER_CACHE_ENABLED,
			QuejaRelacionadaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByDescripcion", new String[] { String.class.getName() },
			QuejaRelacionadaModelImpl.DESCRIPCION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION = new FinderPath(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRelacionadaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByDescripcion", new String[] { String.class.getName() });

	/**
	 * Returns all the queja relacionadas where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the matching queja relacionadas
	 */
	@Override
	public List<QuejaRelacionada> findByfindByDescripcion(String descripcion) {
		return findByfindByDescripcion(descripcion, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the queja relacionadas where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of queja relacionadas
	 * @param end the upper bound of the range of queja relacionadas (not inclusive)
	 * @return the range of matching queja relacionadas
	 */
	@Override
	public List<QuejaRelacionada> findByfindByDescripcion(String descripcion,
		int start, int end) {
		return findByfindByDescripcion(descripcion, start, end, null);
	}

	/**
	 * Returns an ordered range of all the queja relacionadas where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of queja relacionadas
	 * @param end the upper bound of the range of queja relacionadas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching queja relacionadas
	 */
	@Override
	public List<QuejaRelacionada> findByfindByDescripcion(String descripcion,
		int start, int end,
		OrderByComparator<QuejaRelacionada> orderByComparator) {
		return findByfindByDescripcion(descripcion, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the queja relacionadas where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of queja relacionadas
	 * @param end the upper bound of the range of queja relacionadas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching queja relacionadas
	 */
	@Override
	public List<QuejaRelacionada> findByfindByDescripcion(String descripcion,
		int start, int end,
		OrderByComparator<QuejaRelacionada> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion, start, end, orderByComparator };
		}

		List<QuejaRelacionada> list = null;

		if (retrieveFromCache) {
			list = (List<QuejaRelacionada>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (QuejaRelacionada quejaRelacionada : list) {
					if (!Objects.equals(descripcion,
								quejaRelacionada.getDescripcion())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUEJARELACIONADA_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuejaRelacionadaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				if (!pagination) {
					list = (List<QuejaRelacionada>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<QuejaRelacionada>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first queja relacionada in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching queja relacionada
	 * @throws NoSuchQuejaRelacionadaException if a matching queja relacionada could not be found
	 */
	@Override
	public QuejaRelacionada findByfindByDescripcion_First(String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator)
		throws NoSuchQuejaRelacionadaException {
		QuejaRelacionada quejaRelacionada = fetchByfindByDescripcion_First(descripcion,
				orderByComparator);

		if (quejaRelacionada != null) {
			return quejaRelacionada;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuejaRelacionadaException(msg.toString());
	}

	/**
	 * Returns the first queja relacionada in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching queja relacionada, or <code>null</code> if a matching queja relacionada could not be found
	 */
	@Override
	public QuejaRelacionada fetchByfindByDescripcion_First(String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator) {
		List<QuejaRelacionada> list = findByfindByDescripcion(descripcion, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last queja relacionada in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching queja relacionada
	 * @throws NoSuchQuejaRelacionadaException if a matching queja relacionada could not be found
	 */
	@Override
	public QuejaRelacionada findByfindByDescripcion_Last(String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator)
		throws NoSuchQuejaRelacionadaException {
		QuejaRelacionada quejaRelacionada = fetchByfindByDescripcion_Last(descripcion,
				orderByComparator);

		if (quejaRelacionada != null) {
			return quejaRelacionada;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuejaRelacionadaException(msg.toString());
	}

	/**
	 * Returns the last queja relacionada in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching queja relacionada, or <code>null</code> if a matching queja relacionada could not be found
	 */
	@Override
	public QuejaRelacionada fetchByfindByDescripcion_Last(String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator) {
		int count = countByfindByDescripcion(descripcion);

		if (count == 0) {
			return null;
		}

		List<QuejaRelacionada> list = findByfindByDescripcion(descripcion,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the queja relacionadas before and after the current queja relacionada in the ordered set where descripcion = &#63;.
	 *
	 * @param claveRelacion the primary key of the current queja relacionada
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next queja relacionada
	 * @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	 */
	@Override
	public QuejaRelacionada[] findByfindByDescripcion_PrevAndNext(
		String claveRelacion, String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator)
		throws NoSuchQuejaRelacionadaException {
		QuejaRelacionada quejaRelacionada = findByPrimaryKey(claveRelacion);

		Session session = null;

		try {
			session = openSession();

			QuejaRelacionada[] array = new QuejaRelacionadaImpl[3];

			array[0] = getByfindByDescripcion_PrevAndNext(session,
					quejaRelacionada, descripcion, orderByComparator, true);

			array[1] = quejaRelacionada;

			array[2] = getByfindByDescripcion_PrevAndNext(session,
					quejaRelacionada, descripcion, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected QuejaRelacionada getByfindByDescripcion_PrevAndNext(
		Session session, QuejaRelacionada quejaRelacionada, String descripcion,
		OrderByComparator<QuejaRelacionada> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_QUEJARELACIONADA_WHERE);

		boolean bindDescripcion = false;

		if (descripcion == null) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
		}
		else if (descripcion.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
		}
		else {
			bindDescripcion = true;

			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(QuejaRelacionadaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDescripcion) {
			qPos.add(descripcion);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(quejaRelacionada);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<QuejaRelacionada> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the queja relacionadas where descripcion = &#63; from the database.
	 *
	 * @param descripcion the descripcion
	 */
	@Override
	public void removeByfindByDescripcion(String descripcion) {
		for (QuejaRelacionada quejaRelacionada : findByfindByDescripcion(
				descripcion, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(quejaRelacionada);
		}
	}

	/**
	 * Returns the number of queja relacionadas where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the number of matching queja relacionadas
	 */
	@Override
	public int countByfindByDescripcion(String descripcion) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION;

		Object[] finderArgs = new Object[] { descripcion };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUEJARELACIONADA_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1 = "quejaRelacionada.descripcion IS NULL";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2 = "quejaRelacionada.descripcion = ?";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3 = "(quejaRelacionada.descripcion IS NULL OR quejaRelacionada.descripcion = '')";

	public QuejaRelacionadaPersistenceImpl() {
		setModelClass(QuejaRelacionada.class);
	}

	/**
	 * Caches the queja relacionada in the entity cache if it is enabled.
	 *
	 * @param quejaRelacionada the queja relacionada
	 */
	@Override
	public void cacheResult(QuejaRelacionada quejaRelacionada) {
		entityCache.putResult(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRelacionadaImpl.class, quejaRelacionada.getPrimaryKey(),
			quejaRelacionada);

		quejaRelacionada.resetOriginalValues();
	}

	/**
	 * Caches the queja relacionadas in the entity cache if it is enabled.
	 *
	 * @param quejaRelacionadas the queja relacionadas
	 */
	@Override
	public void cacheResult(List<QuejaRelacionada> quejaRelacionadas) {
		for (QuejaRelacionada quejaRelacionada : quejaRelacionadas) {
			if (entityCache.getResult(
						QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
						QuejaRelacionadaImpl.class,
						quejaRelacionada.getPrimaryKey()) == null) {
				cacheResult(quejaRelacionada);
			}
			else {
				quejaRelacionada.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all queja relacionadas.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(QuejaRelacionadaImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the queja relacionada.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(QuejaRelacionada quejaRelacionada) {
		entityCache.removeResult(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRelacionadaImpl.class, quejaRelacionada.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<QuejaRelacionada> quejaRelacionadas) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (QuejaRelacionada quejaRelacionada : quejaRelacionadas) {
			entityCache.removeResult(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
				QuejaRelacionadaImpl.class, quejaRelacionada.getPrimaryKey());
		}
	}

	/**
	 * Creates a new queja relacionada with the primary key. Does not add the queja relacionada to the database.
	 *
	 * @param claveRelacion the primary key for the new queja relacionada
	 * @return the new queja relacionada
	 */
	@Override
	public QuejaRelacionada create(String claveRelacion) {
		QuejaRelacionada quejaRelacionada = new QuejaRelacionadaImpl();

		quejaRelacionada.setNew(true);
		quejaRelacionada.setPrimaryKey(claveRelacion);

		return quejaRelacionada;
	}

	/**
	 * Removes the queja relacionada with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param claveRelacion the primary key of the queja relacionada
	 * @return the queja relacionada that was removed
	 * @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	 */
	@Override
	public QuejaRelacionada remove(String claveRelacion)
		throws NoSuchQuejaRelacionadaException {
		return remove((Serializable)claveRelacion);
	}

	/**
	 * Removes the queja relacionada with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the queja relacionada
	 * @return the queja relacionada that was removed
	 * @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	 */
	@Override
	public QuejaRelacionada remove(Serializable primaryKey)
		throws NoSuchQuejaRelacionadaException {
		Session session = null;

		try {
			session = openSession();

			QuejaRelacionada quejaRelacionada = (QuejaRelacionada)session.get(QuejaRelacionadaImpl.class,
					primaryKey);

			if (quejaRelacionada == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchQuejaRelacionadaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(quejaRelacionada);
		}
		catch (NoSuchQuejaRelacionadaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected QuejaRelacionada removeImpl(QuejaRelacionada quejaRelacionada) {
		quejaRelacionada = toUnwrappedModel(quejaRelacionada);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(quejaRelacionada)) {
				quejaRelacionada = (QuejaRelacionada)session.get(QuejaRelacionadaImpl.class,
						quejaRelacionada.getPrimaryKeyObj());
			}

			if (quejaRelacionada != null) {
				session.delete(quejaRelacionada);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (quejaRelacionada != null) {
			clearCache(quejaRelacionada);
		}

		return quejaRelacionada;
	}

	@Override
	public QuejaRelacionada updateImpl(QuejaRelacionada quejaRelacionada) {
		quejaRelacionada = toUnwrappedModel(quejaRelacionada);

		boolean isNew = quejaRelacionada.isNew();

		QuejaRelacionadaModelImpl quejaRelacionadaModelImpl = (QuejaRelacionadaModelImpl)quejaRelacionada;

		Session session = null;

		try {
			session = openSession();

			if (quejaRelacionada.isNew()) {
				session.save(quejaRelacionada);

				quejaRelacionada.setNew(false);
			}
			else {
				quejaRelacionada = (QuejaRelacionada)session.merge(quejaRelacionada);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !QuejaRelacionadaModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((quejaRelacionadaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						quejaRelacionadaModelImpl.getOriginalDescripcion()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);

				args = new Object[] { quejaRelacionadaModelImpl.getDescripcion() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);
			}
		}

		entityCache.putResult(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRelacionadaImpl.class, quejaRelacionada.getPrimaryKey(),
			quejaRelacionada, false);

		quejaRelacionada.resetOriginalValues();

		return quejaRelacionada;
	}

	protected QuejaRelacionada toUnwrappedModel(
		QuejaRelacionada quejaRelacionada) {
		if (quejaRelacionada instanceof QuejaRelacionadaImpl) {
			return quejaRelacionada;
		}

		QuejaRelacionadaImpl quejaRelacionadaImpl = new QuejaRelacionadaImpl();

		quejaRelacionadaImpl.setNew(quejaRelacionada.isNew());
		quejaRelacionadaImpl.setPrimaryKey(quejaRelacionada.getPrimaryKey());

		quejaRelacionadaImpl.setClaveRelacion(quejaRelacionada.getClaveRelacion());
		quejaRelacionadaImpl.setDescripcion(quejaRelacionada.getDescripcion());
		quejaRelacionadaImpl.setOrdena(quejaRelacionada.getOrdena());
		quejaRelacionadaImpl.setUstedTiene(quejaRelacionada.getUstedTiene());
		quejaRelacionadaImpl.setEsUsted(quejaRelacionada.getEsUsted());
		quejaRelacionadaImpl.setCodigoProblema(quejaRelacionada.getCodigoProblema());

		return quejaRelacionadaImpl;
	}

	/**
	 * Returns the queja relacionada with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the queja relacionada
	 * @return the queja relacionada
	 * @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	 */
	@Override
	public QuejaRelacionada findByPrimaryKey(Serializable primaryKey)
		throws NoSuchQuejaRelacionadaException {
		QuejaRelacionada quejaRelacionada = fetchByPrimaryKey(primaryKey);

		if (quejaRelacionada == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchQuejaRelacionadaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return quejaRelacionada;
	}

	/**
	 * Returns the queja relacionada with the primary key or throws a {@link NoSuchQuejaRelacionadaException} if it could not be found.
	 *
	 * @param claveRelacion the primary key of the queja relacionada
	 * @return the queja relacionada
	 * @throws NoSuchQuejaRelacionadaException if a queja relacionada with the primary key could not be found
	 */
	@Override
	public QuejaRelacionada findByPrimaryKey(String claveRelacion)
		throws NoSuchQuejaRelacionadaException {
		return findByPrimaryKey((Serializable)claveRelacion);
	}

	/**
	 * Returns the queja relacionada with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the queja relacionada
	 * @return the queja relacionada, or <code>null</code> if a queja relacionada with the primary key could not be found
	 */
	@Override
	public QuejaRelacionada fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
				QuejaRelacionadaImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		QuejaRelacionada quejaRelacionada = (QuejaRelacionada)serializable;

		if (quejaRelacionada == null) {
			Session session = null;

			try {
				session = openSession();

				quejaRelacionada = (QuejaRelacionada)session.get(QuejaRelacionadaImpl.class,
						primaryKey);

				if (quejaRelacionada != null) {
					cacheResult(quejaRelacionada);
				}
				else {
					entityCache.putResult(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
						QuejaRelacionadaImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
					QuejaRelacionadaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return quejaRelacionada;
	}

	/**
	 * Returns the queja relacionada with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param claveRelacion the primary key of the queja relacionada
	 * @return the queja relacionada, or <code>null</code> if a queja relacionada with the primary key could not be found
	 */
	@Override
	public QuejaRelacionada fetchByPrimaryKey(String claveRelacion) {
		return fetchByPrimaryKey((Serializable)claveRelacion);
	}

	@Override
	public Map<Serializable, QuejaRelacionada> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, QuejaRelacionada> map = new HashMap<Serializable, QuejaRelacionada>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			QuejaRelacionada quejaRelacionada = fetchByPrimaryKey(primaryKey);

			if (quejaRelacionada != null) {
				map.put(primaryKey, quejaRelacionada);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
					QuejaRelacionadaImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (QuejaRelacionada)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 4) +
				1);

		query.append(_SQL_SELECT_QUEJARELACIONADA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(StringPool.APOSTROPHE);
			query.append((String)primaryKey);
			query.append(StringPool.APOSTROPHE);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (QuejaRelacionada quejaRelacionada : (List<QuejaRelacionada>)q.list()) {
				map.put(quejaRelacionada.getPrimaryKeyObj(), quejaRelacionada);

				cacheResult(quejaRelacionada);

				uncachedPrimaryKeys.remove(quejaRelacionada.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(QuejaRelacionadaModelImpl.ENTITY_CACHE_ENABLED,
					QuejaRelacionadaImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the queja relacionadas.
	 *
	 * @return the queja relacionadas
	 */
	@Override
	public List<QuejaRelacionada> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the queja relacionadas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of queja relacionadas
	 * @param end the upper bound of the range of queja relacionadas (not inclusive)
	 * @return the range of queja relacionadas
	 */
	@Override
	public List<QuejaRelacionada> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the queja relacionadas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of queja relacionadas
	 * @param end the upper bound of the range of queja relacionadas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of queja relacionadas
	 */
	@Override
	public List<QuejaRelacionada> findAll(int start, int end,
		OrderByComparator<QuejaRelacionada> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the queja relacionadas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRelacionadaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of queja relacionadas
	 * @param end the upper bound of the range of queja relacionadas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of queja relacionadas
	 */
	@Override
	public List<QuejaRelacionada> findAll(int start, int end,
		OrderByComparator<QuejaRelacionada> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<QuejaRelacionada> list = null;

		if (retrieveFromCache) {
			list = (List<QuejaRelacionada>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_QUEJARELACIONADA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_QUEJARELACIONADA;

				if (pagination) {
					sql = sql.concat(QuejaRelacionadaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<QuejaRelacionada>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<QuejaRelacionada>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the queja relacionadas from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (QuejaRelacionada quejaRelacionada : findAll()) {
			remove(quejaRelacionada);
		}
	}

	/**
	 * Returns the number of queja relacionadas.
	 *
	 * @return the number of queja relacionadas
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_QUEJARELACIONADA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return QuejaRelacionadaModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the queja relacionada persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(QuejaRelacionadaImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_QUEJARELACIONADA = "SELECT quejaRelacionada FROM QuejaRelacionada quejaRelacionada";
	private static final String _SQL_SELECT_QUEJARELACIONADA_WHERE_PKS_IN = "SELECT quejaRelacionada FROM QuejaRelacionada quejaRelacionada WHERE claveRelacion IN (";
	private static final String _SQL_SELECT_QUEJARELACIONADA_WHERE = "SELECT quejaRelacionada FROM QuejaRelacionada quejaRelacionada WHERE ";
	private static final String _SQL_COUNT_QUEJARELACIONADA = "SELECT COUNT(quejaRelacionada) FROM QuejaRelacionada quejaRelacionada";
	private static final String _SQL_COUNT_QUEJARELACIONADA_WHERE = "SELECT COUNT(quejaRelacionada) FROM QuejaRelacionada quejaRelacionada WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "quejaRelacionada.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No QuejaRelacionada exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No QuejaRelacionada exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(QuejaRelacionadaPersistenceImpl.class);
}