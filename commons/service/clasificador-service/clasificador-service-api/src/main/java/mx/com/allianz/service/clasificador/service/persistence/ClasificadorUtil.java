/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.clasificador.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.clasificador.model.Clasificador;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the clasificador service. This utility wraps {@link mx.com.allianz.service.clasificador.service.persistence.impl.ClasificadorPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ClasificadorPersistence
 * @see mx.com.allianz.service.clasificador.service.persistence.impl.ClasificadorPersistenceImpl
 * @generated
 */
@ProviderType
public class ClasificadorUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Clasificador clasificador) {
		getPersistence().clearCache(clasificador);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Clasificador> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Clasificador> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Clasificador> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Clasificador> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Clasificador update(Clasificador clasificador) {
		return getPersistence().update(clasificador);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Clasificador update(Clasificador clasificador,
		ServiceContext serviceContext) {
		return getPersistence().update(clasificador, serviceContext);
	}

	/**
	* Returns all the clasificadors where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching clasificadors
	*/
	public static List<Clasificador> findByfindByDescripcion(
		java.lang.String descripcion) {
		return getPersistence().findByfindByDescripcion(descripcion);
	}

	/**
	* Returns a range of all the clasificadors where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @return the range of matching clasificadors
	*/
	public static List<Clasificador> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end) {
		return getPersistence().findByfindByDescripcion(descripcion, start, end);
	}

	/**
	* Returns an ordered range of all the clasificadors where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching clasificadors
	*/
	public static List<Clasificador> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<Clasificador> orderByComparator) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the clasificadors where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching clasificadors
	*/
	public static List<Clasificador> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<Clasificador> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first clasificador in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching clasificador
	* @throws NoSuchClasificadorException if a matching clasificador could not be found
	*/
	public static Clasificador findByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<Clasificador> orderByComparator)
		throws mx.com.allianz.service.clasificador.exception.NoSuchClasificadorException {
		return getPersistence()
				   .findByfindByDescripcion_First(descripcion, orderByComparator);
	}

	/**
	* Returns the first clasificador in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching clasificador, or <code>null</code> if a matching clasificador could not be found
	*/
	public static Clasificador fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<Clasificador> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_First(descripcion,
			orderByComparator);
	}

	/**
	* Returns the last clasificador in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching clasificador
	* @throws NoSuchClasificadorException if a matching clasificador could not be found
	*/
	public static Clasificador findByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<Clasificador> orderByComparator)
		throws mx.com.allianz.service.clasificador.exception.NoSuchClasificadorException {
		return getPersistence()
				   .findByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the last clasificador in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching clasificador, or <code>null</code> if a matching clasificador could not be found
	*/
	public static Clasificador fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<Clasificador> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the clasificadors before and after the current clasificador in the ordered set where descripcion = &#63;.
	*
	* @param idClasificador the primary key of the current clasificador
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next clasificador
	* @throws NoSuchClasificadorException if a clasificador with the primary key could not be found
	*/
	public static Clasificador[] findByfindByDescripcion_PrevAndNext(
		int idClasificador, java.lang.String descripcion,
		OrderByComparator<Clasificador> orderByComparator)
		throws mx.com.allianz.service.clasificador.exception.NoSuchClasificadorException {
		return getPersistence()
				   .findByfindByDescripcion_PrevAndNext(idClasificador,
			descripcion, orderByComparator);
	}

	/**
	* Removes all the clasificadors where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public static void removeByfindByDescripcion(java.lang.String descripcion) {
		getPersistence().removeByfindByDescripcion(descripcion);
	}

	/**
	* Returns the number of clasificadors where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching clasificadors
	*/
	public static int countByfindByDescripcion(java.lang.String descripcion) {
		return getPersistence().countByfindByDescripcion(descripcion);
	}

	/**
	* Caches the clasificador in the entity cache if it is enabled.
	*
	* @param clasificador the clasificador
	*/
	public static void cacheResult(Clasificador clasificador) {
		getPersistence().cacheResult(clasificador);
	}

	/**
	* Caches the clasificadors in the entity cache if it is enabled.
	*
	* @param clasificadors the clasificadors
	*/
	public static void cacheResult(List<Clasificador> clasificadors) {
		getPersistence().cacheResult(clasificadors);
	}

	/**
	* Creates a new clasificador with the primary key. Does not add the clasificador to the database.
	*
	* @param idClasificador the primary key for the new clasificador
	* @return the new clasificador
	*/
	public static Clasificador create(int idClasificador) {
		return getPersistence().create(idClasificador);
	}

	/**
	* Removes the clasificador with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idClasificador the primary key of the clasificador
	* @return the clasificador that was removed
	* @throws NoSuchClasificadorException if a clasificador with the primary key could not be found
	*/
	public static Clasificador remove(int idClasificador)
		throws mx.com.allianz.service.clasificador.exception.NoSuchClasificadorException {
		return getPersistence().remove(idClasificador);
	}

	public static Clasificador updateImpl(Clasificador clasificador) {
		return getPersistence().updateImpl(clasificador);
	}

	/**
	* Returns the clasificador with the primary key or throws a {@link NoSuchClasificadorException} if it could not be found.
	*
	* @param idClasificador the primary key of the clasificador
	* @return the clasificador
	* @throws NoSuchClasificadorException if a clasificador with the primary key could not be found
	*/
	public static Clasificador findByPrimaryKey(int idClasificador)
		throws mx.com.allianz.service.clasificador.exception.NoSuchClasificadorException {
		return getPersistence().findByPrimaryKey(idClasificador);
	}

	/**
	* Returns the clasificador with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idClasificador the primary key of the clasificador
	* @return the clasificador, or <code>null</code> if a clasificador with the primary key could not be found
	*/
	public static Clasificador fetchByPrimaryKey(int idClasificador) {
		return getPersistence().fetchByPrimaryKey(idClasificador);
	}

	public static java.util.Map<java.io.Serializable, Clasificador> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the clasificadors.
	*
	* @return the clasificadors
	*/
	public static List<Clasificador> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the clasificadors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @return the range of clasificadors
	*/
	public static List<Clasificador> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the clasificadors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of clasificadors
	*/
	public static List<Clasificador> findAll(int start, int end,
		OrderByComparator<Clasificador> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the clasificadors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of clasificadors
	*/
	public static List<Clasificador> findAll(int start, int end,
		OrderByComparator<Clasificador> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the clasificadors from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of clasificadors.
	*
	* @return the number of clasificadors
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ClasificadorPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ClasificadorPersistence, ClasificadorPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ClasificadorPersistence.class);
}