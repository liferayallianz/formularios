/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.clasificador.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ClasificadorLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ClasificadorLocalService
 * @generated
 */
@ProviderType
public class ClasificadorLocalServiceWrapper implements ClasificadorLocalService,
	ServiceWrapper<ClasificadorLocalService> {
	public ClasificadorLocalServiceWrapper(
		ClasificadorLocalService clasificadorLocalService) {
		_clasificadorLocalService = clasificadorLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _clasificadorLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _clasificadorLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _clasificadorLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _clasificadorLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _clasificadorLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of clasificadors.
	*
	* @return the number of clasificadors
	*/
	@Override
	public int getClasificadorsCount() {
		return _clasificadorLocalService.getClasificadorsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _clasificadorLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _clasificadorLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.clasificador.model.impl.ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _clasificadorLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.clasificador.model.impl.ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _clasificadorLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the clasificadors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.clasificador.model.impl.ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @return the range of clasificadors
	*/
	@Override
	public java.util.List<mx.com.allianz.service.clasificador.model.Clasificador> getClasificadors(
		int start, int end) {
		return _clasificadorLocalService.getClasificadors(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _clasificadorLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _clasificadorLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	/**
	* Adds the clasificador to the database. Also notifies the appropriate model listeners.
	*
	* @param clasificador the clasificador
	* @return the clasificador that was added
	*/
	@Override
	public mx.com.allianz.service.clasificador.model.Clasificador addClasificador(
		mx.com.allianz.service.clasificador.model.Clasificador clasificador) {
		return _clasificadorLocalService.addClasificador(clasificador);
	}

	/**
	* Creates a new clasificador with the primary key. Does not add the clasificador to the database.
	*
	* @param idClasificador the primary key for the new clasificador
	* @return the new clasificador
	*/
	@Override
	public mx.com.allianz.service.clasificador.model.Clasificador createClasificador(
		int idClasificador) {
		return _clasificadorLocalService.createClasificador(idClasificador);
	}

	/**
	* Deletes the clasificador with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idClasificador the primary key of the clasificador
	* @return the clasificador that was removed
	* @throws PortalException if a clasificador with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.clasificador.model.Clasificador deleteClasificador(
		int idClasificador)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _clasificadorLocalService.deleteClasificador(idClasificador);
	}

	/**
	* Deletes the clasificador from the database. Also notifies the appropriate model listeners.
	*
	* @param clasificador the clasificador
	* @return the clasificador that was removed
	*/
	@Override
	public mx.com.allianz.service.clasificador.model.Clasificador deleteClasificador(
		mx.com.allianz.service.clasificador.model.Clasificador clasificador) {
		return _clasificadorLocalService.deleteClasificador(clasificador);
	}

	@Override
	public mx.com.allianz.service.clasificador.model.Clasificador fetchClasificador(
		int idClasificador) {
		return _clasificadorLocalService.fetchClasificador(idClasificador);
	}

	/**
	* Returns the clasificador with the primary key.
	*
	* @param idClasificador the primary key of the clasificador
	* @return the clasificador
	* @throws PortalException if a clasificador with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.clasificador.model.Clasificador getClasificador(
		int idClasificador)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _clasificadorLocalService.getClasificador(idClasificador);
	}

	/**
	* Updates the clasificador in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clasificador the clasificador
	* @return the clasificador that was updated
	*/
	@Override
	public mx.com.allianz.service.clasificador.model.Clasificador updateClasificador(
		mx.com.allianz.service.clasificador.model.Clasificador clasificador) {
		return _clasificadorLocalService.updateClasificador(clasificador);
	}

	@Override
	public ClasificadorLocalService getWrappedService() {
		return _clasificadorLocalService;
	}

	@Override
	public void setWrappedService(
		ClasificadorLocalService clasificadorLocalService) {
		_clasificadorLocalService = clasificadorLocalService;
	}

	private ClasificadorLocalService _clasificadorLocalService;
}