/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mx.com.allianz.service.rest.clasificador;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.ClasificadorDTO;
import mx.com.allianz.service.clasificador.service.ClasificadorLocalServiceUtil;




/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio
 * clasificador que permite visualizar una lista de datos, 
 * filtra el orden de la b\u00fasqueda y muestra el ordenamiento 
 * conforme al filtro.
 * 
 *
 * @author  TestingZone
 * @version 1.0
 * @since   2016-12-5 
 * 
 * 
 */
/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n (Modulo).
 */
@ApplicationPath("/catalogos.clasificador")
/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(
	immediate = true, property = {"jaxrs.application=true"},
	service = Application.class
)
public class ClasificadorRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object)this);
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los clasficadores del cat\u00e1logo.
	 * 
	 * @return getAllClasificadores()
	 */
	//@GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	//@Path sirve para poner la ruta relativa donde la clase ser\u00e1 hospedada.
	@Path("/getClasificadores")
	//@Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({MediaType.APPLICATION_JSON})
	public List<ClasificadorDTO> getClasificadores() {
		// Se regresan todos los clasificadores. 
		return getAllClasificadores();
	}
	
	/**
	 * Operacion de servicio rest para consulta de todos los clasficadores del cat\u00e1logo.
	 * 
	 * @return getAllClasificadores().stream()
	 */
	//@GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	//@Path sirve para poner la ruta relativa donde la clase ser\u00e1.
	@Path("/findClasificadores")
	//@Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({MediaType.APPLICATION_JSON})
	public List<ClasificadorDTO>findClasificadorBy(
			//El valor @DefaultValue se utiliza para definir el valor por defecto para el par\u00e1metro matrixParam.
			//@QueryParam se utiliza para extraer los par\u00e1metros de consulta del "componente de consulta" de la URL de la solicitud.
			@DefaultValue("-1")@QueryParam("id_Clasificador") int idClasificador,
			@DefaultValue("")@QueryParam("nombre") String nombre,
			@DefaultValue("")@QueryParam("descripcion") String descripcion){
		// stream() Devuelve un flujo secuencial considerando la colecci\u00f3n
		// como su origen.
		// Convierte la lista en stream.
		return getAllClasificadores().stream()
				
				// Filtrado por identificador de clasificador, si se env\u00eda -1 se ignora el filtro. 
				.filter((clasificador) -> idClasificador == -1 || clasificador.getIdClasificador()  == idClasificador)
				// Filtrado por nombre de clasificador, si es vac\u00edo se ignora el filtro. 
				.filter((clasificador) -> nombre.isEmpty() || clasificador.getNombre().contains(nombre))
				// Filtrado por descripci\u00f3n de clasificador, si es vac\u00edo se ignora el filtro. 
				.filter((clasificador) -> descripcion.isEmpty() || clasificador.getDescripcion().contains(descripcion))
				//Salida de colecci\u00f3n y el stream se convierte en lista.
				.collect(Collectors.toList());
				
	}
	
	
	/**
	 *M\u00e9todo getAllClasificadores del tipo list generada para mostrar un arreglo de la lista.
	 *
	 * @return listaClasificadores
	 */
	private List<ClasificadorDTO> getAllClasificadores(){
		
		return ClasificadorLocalServiceUtil.getClasificadors(-1, -1).
				stream().map(clasificador -> new ClasificadorDTO(
						clasificador.getIdClasificador(), 
						clasificador.getNombre(), 
						clasificador.getDescripcion())).
				collect(Collectors.toList());
	
		}
}