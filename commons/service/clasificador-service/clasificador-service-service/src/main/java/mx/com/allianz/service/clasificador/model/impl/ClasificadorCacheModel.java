/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.clasificador.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.clasificador.model.Clasificador;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Clasificador in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Clasificador
 * @generated
 */
@ProviderType
public class ClasificadorCacheModel implements CacheModel<Clasificador>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClasificadorCacheModel)) {
			return false;
		}

		ClasificadorCacheModel clasificadorCacheModel = (ClasificadorCacheModel)obj;

		if (idClasificador == clasificadorCacheModel.idClasificador) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, idClasificador);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{idClasificador=");
		sb.append(idClasificador);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Clasificador toEntityModel() {
		ClasificadorImpl clasificadorImpl = new ClasificadorImpl();

		clasificadorImpl.setIdClasificador(idClasificador);

		if (nombre == null) {
			clasificadorImpl.setNombre(StringPool.BLANK);
		}
		else {
			clasificadorImpl.setNombre(nombre);
		}

		if (descripcion == null) {
			clasificadorImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			clasificadorImpl.setDescripcion(descripcion);
		}

		clasificadorImpl.resetOriginalValues();

		return clasificadorImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idClasificador = objectInput.readInt();
		nombre = objectInput.readUTF();
		descripcion = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(idClasificador);

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}
	}

	public int idClasificador;
	public String nombre;
	public String descripcion;
}