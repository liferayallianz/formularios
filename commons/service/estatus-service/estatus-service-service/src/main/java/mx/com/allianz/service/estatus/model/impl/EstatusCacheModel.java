/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.estatus.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.estatus.model.Estatus;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Estatus in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Estatus
 * @generated
 */
@ProviderType
public class EstatusCacheModel implements CacheModel<Estatus>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EstatusCacheModel)) {
			return false;
		}

		EstatusCacheModel estatusCacheModel = (EstatusCacheModel)obj;

		if (idTramite == estatusCacheModel.idTramite) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, idTramite);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{idTramite=");
		sb.append(idTramite);
		sb.append(", idOt=");
		sb.append(idOt);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append(", estatus=");
		sb.append(estatus);
		sb.append(", informacionTramite=");
		sb.append(informacionTramite);
		sb.append(", proveIdProvedor=");
		sb.append(proveIdProvedor);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Estatus toEntityModel() {
		EstatusImpl estatusImpl = new EstatusImpl();

		estatusImpl.setIdTramite(idTramite);
		estatusImpl.setIdOt(idOt);

		if (fechaRegistro == Long.MIN_VALUE) {
			estatusImpl.setFechaRegistro(null);
		}
		else {
			estatusImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		if (estatus == null) {
			estatusImpl.setEstatus(StringPool.BLANK);
		}
		else {
			estatusImpl.setEstatus(estatus);
		}

		if (informacionTramite == null) {
			estatusImpl.setInformacionTramite(StringPool.BLANK);
		}
		else {
			estatusImpl.setInformacionTramite(informacionTramite);
		}

		estatusImpl.setProveIdProvedor(proveIdProvedor);

		estatusImpl.resetOriginalValues();

		return estatusImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idTramite = objectInput.readInt();

		idOt = objectInput.readInt();
		fechaRegistro = objectInput.readLong();
		estatus = objectInput.readUTF();
		informacionTramite = objectInput.readUTF();

		proveIdProvedor = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(idTramite);

		objectOutput.writeInt(idOt);
		objectOutput.writeLong(fechaRegistro);

		if (estatus == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estatus);
		}

		if (informacionTramite == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(informacionTramite);
		}

		objectOutput.writeInt(proveIdProvedor);
	}

	public int idTramite;
	public int idOt;
	public long fechaRegistro;
	public String estatus;
	public String informacionTramite;
	public int proveIdProvedor;
}