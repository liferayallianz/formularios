/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.estatus.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.estatus.exception.NoSuchEstatusException;
import mx.com.allianz.service.estatus.model.Estatus;
import mx.com.allianz.service.estatus.model.impl.EstatusImpl;
import mx.com.allianz.service.estatus.model.impl.EstatusModelImpl;
import mx.com.allianz.service.estatus.service.persistence.EstatusPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the estatus service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EstatusPersistence
 * @see mx.com.allianz.service.estatus.service.persistence.EstatusUtil
 * @generated
 */
@ProviderType
public class EstatusPersistenceImpl extends BasePersistenceImpl<Estatus>
	implements EstatusPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EstatusUtil} to access the estatus persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EstatusImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EstatusModelImpl.ENTITY_CACHE_ENABLED,
			EstatusModelImpl.FINDER_CACHE_ENABLED, EstatusImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EstatusModelImpl.ENTITY_CACHE_ENABLED,
			EstatusModelImpl.FINDER_CACHE_ENABLED, EstatusImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EstatusModelImpl.ENTITY_CACHE_ENABLED,
			EstatusModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYINFORMACIONTRAMITE =
		new FinderPath(EstatusModelImpl.ENTITY_CACHE_ENABLED,
			EstatusModelImpl.FINDER_CACHE_ENABLED, EstatusImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByfindByInformacionTramite",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYINFORMACIONTRAMITE =
		new FinderPath(EstatusModelImpl.ENTITY_CACHE_ENABLED,
			EstatusModelImpl.FINDER_CACHE_ENABLED, EstatusImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByInformacionTramite",
			new String[] { String.class.getName() },
			EstatusModelImpl.INFORMACIONTRAMITE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYINFORMACIONTRAMITE =
		new FinderPath(EstatusModelImpl.ENTITY_CACHE_ENABLED,
			EstatusModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByInformacionTramite",
			new String[] { String.class.getName() });

	/**
	 * Returns all the estatuses where informacionTramite = &#63;.
	 *
	 * @param informacionTramite the informacion tramite
	 * @return the matching estatuses
	 */
	@Override
	public List<Estatus> findByfindByInformacionTramite(
		String informacionTramite) {
		return findByfindByInformacionTramite(informacionTramite,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the estatuses where informacionTramite = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param informacionTramite the informacion tramite
	 * @param start the lower bound of the range of estatuses
	 * @param end the upper bound of the range of estatuses (not inclusive)
	 * @return the range of matching estatuses
	 */
	@Override
	public List<Estatus> findByfindByInformacionTramite(
		String informacionTramite, int start, int end) {
		return findByfindByInformacionTramite(informacionTramite, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the estatuses where informacionTramite = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param informacionTramite the informacion tramite
	 * @param start the lower bound of the range of estatuses
	 * @param end the upper bound of the range of estatuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching estatuses
	 */
	@Override
	public List<Estatus> findByfindByInformacionTramite(
		String informacionTramite, int start, int end,
		OrderByComparator<Estatus> orderByComparator) {
		return findByfindByInformacionTramite(informacionTramite, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the estatuses where informacionTramite = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param informacionTramite the informacion tramite
	 * @param start the lower bound of the range of estatuses
	 * @param end the upper bound of the range of estatuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching estatuses
	 */
	@Override
	public List<Estatus> findByfindByInformacionTramite(
		String informacionTramite, int start, int end,
		OrderByComparator<Estatus> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYINFORMACIONTRAMITE;
			finderArgs = new Object[] { informacionTramite };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYINFORMACIONTRAMITE;
			finderArgs = new Object[] {
					informacionTramite,
					
					start, end, orderByComparator
				};
		}

		List<Estatus> list = null;

		if (retrieveFromCache) {
			list = (List<Estatus>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Estatus estatus : list) {
					if (!Objects.equals(informacionTramite,
								estatus.getInformacionTramite())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ESTATUS_WHERE);

			boolean bindInformacionTramite = false;

			if (informacionTramite == null) {
				query.append(_FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_1);
			}
			else if (informacionTramite.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_3);
			}
			else {
				bindInformacionTramite = true;

				query.append(_FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EstatusModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindInformacionTramite) {
					qPos.add(informacionTramite);
				}

				if (!pagination) {
					list = (List<Estatus>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Estatus>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first estatus in the ordered set where informacionTramite = &#63;.
	 *
	 * @param informacionTramite the informacion tramite
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching estatus
	 * @throws NoSuchEstatusException if a matching estatus could not be found
	 */
	@Override
	public Estatus findByfindByInformacionTramite_First(
		String informacionTramite, OrderByComparator<Estatus> orderByComparator)
		throws NoSuchEstatusException {
		Estatus estatus = fetchByfindByInformacionTramite_First(informacionTramite,
				orderByComparator);

		if (estatus != null) {
			return estatus;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("informacionTramite=");
		msg.append(informacionTramite);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEstatusException(msg.toString());
	}

	/**
	 * Returns the first estatus in the ordered set where informacionTramite = &#63;.
	 *
	 * @param informacionTramite the informacion tramite
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching estatus, or <code>null</code> if a matching estatus could not be found
	 */
	@Override
	public Estatus fetchByfindByInformacionTramite_First(
		String informacionTramite, OrderByComparator<Estatus> orderByComparator) {
		List<Estatus> list = findByfindByInformacionTramite(informacionTramite,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last estatus in the ordered set where informacionTramite = &#63;.
	 *
	 * @param informacionTramite the informacion tramite
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching estatus
	 * @throws NoSuchEstatusException if a matching estatus could not be found
	 */
	@Override
	public Estatus findByfindByInformacionTramite_Last(
		String informacionTramite, OrderByComparator<Estatus> orderByComparator)
		throws NoSuchEstatusException {
		Estatus estatus = fetchByfindByInformacionTramite_Last(informacionTramite,
				orderByComparator);

		if (estatus != null) {
			return estatus;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("informacionTramite=");
		msg.append(informacionTramite);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEstatusException(msg.toString());
	}

	/**
	 * Returns the last estatus in the ordered set where informacionTramite = &#63;.
	 *
	 * @param informacionTramite the informacion tramite
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching estatus, or <code>null</code> if a matching estatus could not be found
	 */
	@Override
	public Estatus fetchByfindByInformacionTramite_Last(
		String informacionTramite, OrderByComparator<Estatus> orderByComparator) {
		int count = countByfindByInformacionTramite(informacionTramite);

		if (count == 0) {
			return null;
		}

		List<Estatus> list = findByfindByInformacionTramite(informacionTramite,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the estatuses before and after the current estatus in the ordered set where informacionTramite = &#63;.
	 *
	 * @param idTramite the primary key of the current estatus
	 * @param informacionTramite the informacion tramite
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next estatus
	 * @throws NoSuchEstatusException if a estatus with the primary key could not be found
	 */
	@Override
	public Estatus[] findByfindByInformacionTramite_PrevAndNext(int idTramite,
		String informacionTramite, OrderByComparator<Estatus> orderByComparator)
		throws NoSuchEstatusException {
		Estatus estatus = findByPrimaryKey(idTramite);

		Session session = null;

		try {
			session = openSession();

			Estatus[] array = new EstatusImpl[3];

			array[0] = getByfindByInformacionTramite_PrevAndNext(session,
					estatus, informacionTramite, orderByComparator, true);

			array[1] = estatus;

			array[2] = getByfindByInformacionTramite_PrevAndNext(session,
					estatus, informacionTramite, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Estatus getByfindByInformacionTramite_PrevAndNext(
		Session session, Estatus estatus, String informacionTramite,
		OrderByComparator<Estatus> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ESTATUS_WHERE);

		boolean bindInformacionTramite = false;

		if (informacionTramite == null) {
			query.append(_FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_1);
		}
		else if (informacionTramite.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_3);
		}
		else {
			bindInformacionTramite = true;

			query.append(_FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EstatusModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindInformacionTramite) {
			qPos.add(informacionTramite);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(estatus);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Estatus> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the estatuses where informacionTramite = &#63; from the database.
	 *
	 * @param informacionTramite the informacion tramite
	 */
	@Override
	public void removeByfindByInformacionTramite(String informacionTramite) {
		for (Estatus estatus : findByfindByInformacionTramite(
				informacionTramite, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(estatus);
		}
	}

	/**
	 * Returns the number of estatuses where informacionTramite = &#63;.
	 *
	 * @param informacionTramite the informacion tramite
	 * @return the number of matching estatuses
	 */
	@Override
	public int countByfindByInformacionTramite(String informacionTramite) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYINFORMACIONTRAMITE;

		Object[] finderArgs = new Object[] { informacionTramite };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ESTATUS_WHERE);

			boolean bindInformacionTramite = false;

			if (informacionTramite == null) {
				query.append(_FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_1);
			}
			else if (informacionTramite.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_3);
			}
			else {
				bindInformacionTramite = true;

				query.append(_FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindInformacionTramite) {
					qPos.add(informacionTramite);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_1 =
		"estatus.informacionTramite IS NULL";
	private static final String _FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_2 =
		"estatus.informacionTramite = ?";
	private static final String _FINDER_COLUMN_FINDBYINFORMACIONTRAMITE_INFORMACIONTRAMITE_3 =
		"(estatus.informacionTramite IS NULL OR estatus.informacionTramite = '')";

	public EstatusPersistenceImpl() {
		setModelClass(Estatus.class);
	}

	/**
	 * Caches the estatus in the entity cache if it is enabled.
	 *
	 * @param estatus the estatus
	 */
	@Override
	public void cacheResult(Estatus estatus) {
		entityCache.putResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
			EstatusImpl.class, estatus.getPrimaryKey(), estatus);

		estatus.resetOriginalValues();
	}

	/**
	 * Caches the estatuses in the entity cache if it is enabled.
	 *
	 * @param estatuses the estatuses
	 */
	@Override
	public void cacheResult(List<Estatus> estatuses) {
		for (Estatus estatus : estatuses) {
			if (entityCache.getResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
						EstatusImpl.class, estatus.getPrimaryKey()) == null) {
				cacheResult(estatus);
			}
			else {
				estatus.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all estatuses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(EstatusImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the estatus.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Estatus estatus) {
		entityCache.removeResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
			EstatusImpl.class, estatus.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Estatus> estatuses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Estatus estatus : estatuses) {
			entityCache.removeResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
				EstatusImpl.class, estatus.getPrimaryKey());
		}
	}

	/**
	 * Creates a new estatus with the primary key. Does not add the estatus to the database.
	 *
	 * @param idTramite the primary key for the new estatus
	 * @return the new estatus
	 */
	@Override
	public Estatus create(int idTramite) {
		Estatus estatus = new EstatusImpl();

		estatus.setNew(true);
		estatus.setPrimaryKey(idTramite);

		return estatus;
	}

	/**
	 * Removes the estatus with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idTramite the primary key of the estatus
	 * @return the estatus that was removed
	 * @throws NoSuchEstatusException if a estatus with the primary key could not be found
	 */
	@Override
	public Estatus remove(int idTramite) throws NoSuchEstatusException {
		return remove((Serializable)idTramite);
	}

	/**
	 * Removes the estatus with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the estatus
	 * @return the estatus that was removed
	 * @throws NoSuchEstatusException if a estatus with the primary key could not be found
	 */
	@Override
	public Estatus remove(Serializable primaryKey)
		throws NoSuchEstatusException {
		Session session = null;

		try {
			session = openSession();

			Estatus estatus = (Estatus)session.get(EstatusImpl.class, primaryKey);

			if (estatus == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEstatusException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(estatus);
		}
		catch (NoSuchEstatusException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Estatus removeImpl(Estatus estatus) {
		estatus = toUnwrappedModel(estatus);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(estatus)) {
				estatus = (Estatus)session.get(EstatusImpl.class,
						estatus.getPrimaryKeyObj());
			}

			if (estatus != null) {
				session.delete(estatus);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (estatus != null) {
			clearCache(estatus);
		}

		return estatus;
	}

	@Override
	public Estatus updateImpl(Estatus estatus) {
		estatus = toUnwrappedModel(estatus);

		boolean isNew = estatus.isNew();

		EstatusModelImpl estatusModelImpl = (EstatusModelImpl)estatus;

		Session session = null;

		try {
			session = openSession();

			if (estatus.isNew()) {
				session.save(estatus);

				estatus.setNew(false);
			}
			else {
				estatus = (Estatus)session.merge(estatus);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EstatusModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((estatusModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYINFORMACIONTRAMITE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						estatusModelImpl.getOriginalInformacionTramite()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYINFORMACIONTRAMITE,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYINFORMACIONTRAMITE,
					args);

				args = new Object[] { estatusModelImpl.getInformacionTramite() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYINFORMACIONTRAMITE,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYINFORMACIONTRAMITE,
					args);
			}
		}

		entityCache.putResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
			EstatusImpl.class, estatus.getPrimaryKey(), estatus, false);

		estatus.resetOriginalValues();

		return estatus;
	}

	protected Estatus toUnwrappedModel(Estatus estatus) {
		if (estatus instanceof EstatusImpl) {
			return estatus;
		}

		EstatusImpl estatusImpl = new EstatusImpl();

		estatusImpl.setNew(estatus.isNew());
		estatusImpl.setPrimaryKey(estatus.getPrimaryKey());

		estatusImpl.setIdTramite(estatus.getIdTramite());
		estatusImpl.setIdOt(estatus.getIdOt());
		estatusImpl.setFechaRegistro(estatus.getFechaRegistro());
		estatusImpl.setEstatus(estatus.getEstatus());
		estatusImpl.setInformacionTramite(estatus.getInformacionTramite());
		estatusImpl.setProveIdProvedor(estatus.getProveIdProvedor());

		return estatusImpl;
	}

	/**
	 * Returns the estatus with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the estatus
	 * @return the estatus
	 * @throws NoSuchEstatusException if a estatus with the primary key could not be found
	 */
	@Override
	public Estatus findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEstatusException {
		Estatus estatus = fetchByPrimaryKey(primaryKey);

		if (estatus == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEstatusException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return estatus;
	}

	/**
	 * Returns the estatus with the primary key or throws a {@link NoSuchEstatusException} if it could not be found.
	 *
	 * @param idTramite the primary key of the estatus
	 * @return the estatus
	 * @throws NoSuchEstatusException if a estatus with the primary key could not be found
	 */
	@Override
	public Estatus findByPrimaryKey(int idTramite)
		throws NoSuchEstatusException {
		return findByPrimaryKey((Serializable)idTramite);
	}

	/**
	 * Returns the estatus with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the estatus
	 * @return the estatus, or <code>null</code> if a estatus with the primary key could not be found
	 */
	@Override
	public Estatus fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
				EstatusImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Estatus estatus = (Estatus)serializable;

		if (estatus == null) {
			Session session = null;

			try {
				session = openSession();

				estatus = (Estatus)session.get(EstatusImpl.class, primaryKey);

				if (estatus != null) {
					cacheResult(estatus);
				}
				else {
					entityCache.putResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
						EstatusImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
					EstatusImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return estatus;
	}

	/**
	 * Returns the estatus with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idTramite the primary key of the estatus
	 * @return the estatus, or <code>null</code> if a estatus with the primary key could not be found
	 */
	@Override
	public Estatus fetchByPrimaryKey(int idTramite) {
		return fetchByPrimaryKey((Serializable)idTramite);
	}

	@Override
	public Map<Serializable, Estatus> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Estatus> map = new HashMap<Serializable, Estatus>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Estatus estatus = fetchByPrimaryKey(primaryKey);

			if (estatus != null) {
				map.put(primaryKey, estatus);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
					EstatusImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Estatus)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_ESTATUS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Estatus estatus : (List<Estatus>)q.list()) {
				map.put(estatus.getPrimaryKeyObj(), estatus);

				cacheResult(estatus);

				uncachedPrimaryKeys.remove(estatus.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(EstatusModelImpl.ENTITY_CACHE_ENABLED,
					EstatusImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the estatuses.
	 *
	 * @return the estatuses
	 */
	@Override
	public List<Estatus> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the estatuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of estatuses
	 * @param end the upper bound of the range of estatuses (not inclusive)
	 * @return the range of estatuses
	 */
	@Override
	public List<Estatus> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the estatuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of estatuses
	 * @param end the upper bound of the range of estatuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of estatuses
	 */
	@Override
	public List<Estatus> findAll(int start, int end,
		OrderByComparator<Estatus> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the estatuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of estatuses
	 * @param end the upper bound of the range of estatuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of estatuses
	 */
	@Override
	public List<Estatus> findAll(int start, int end,
		OrderByComparator<Estatus> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Estatus> list = null;

		if (retrieveFromCache) {
			list = (List<Estatus>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_ESTATUS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ESTATUS;

				if (pagination) {
					sql = sql.concat(EstatusModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Estatus>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Estatus>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the estatuses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Estatus estatus : findAll()) {
			remove(estatus);
		}
	}

	/**
	 * Returns the number of estatuses.
	 *
	 * @return the number of estatuses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ESTATUS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return EstatusModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the estatus persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(EstatusImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_ESTATUS = "SELECT estatus FROM Estatus estatus";
	private static final String _SQL_SELECT_ESTATUS_WHERE_PKS_IN = "SELECT estatus FROM Estatus estatus WHERE idTramite IN (";
	private static final String _SQL_SELECT_ESTATUS_WHERE = "SELECT estatus FROM Estatus estatus WHERE ";
	private static final String _SQL_COUNT_ESTATUS = "SELECT COUNT(estatus) FROM Estatus estatus";
	private static final String _SQL_COUNT_ESTATUS_WHERE = "SELECT COUNT(estatus) FROM Estatus estatus WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "estatus.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Estatus exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Estatus exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(EstatusPersistenceImpl.class);
}