/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.estatus.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Estatus}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Estatus
 * @generated
 */
@ProviderType
public class EstatusWrapper implements Estatus, ModelWrapper<Estatus> {
	public EstatusWrapper(Estatus estatus) {
		_estatus = estatus;
	}

	@Override
	public Class<?> getModelClass() {
		return Estatus.class;
	}

	@Override
	public String getModelClassName() {
		return Estatus.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idTramite", getIdTramite());
		attributes.put("idOt", getIdOt());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("estatus", getEstatus());
		attributes.put("informacionTramite", getInformacionTramite());
		attributes.put("proveIdProvedor", getProveIdProvedor());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idTramite = (Integer)attributes.get("idTramite");

		if (idTramite != null) {
			setIdTramite(idTramite);
		}

		Integer idOt = (Integer)attributes.get("idOt");

		if (idOt != null) {
			setIdOt(idOt);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String estatus = (String)attributes.get("estatus");

		if (estatus != null) {
			setEstatus(estatus);
		}

		String informacionTramite = (String)attributes.get("informacionTramite");

		if (informacionTramite != null) {
			setInformacionTramite(informacionTramite);
		}

		Integer proveIdProvedor = (Integer)attributes.get("proveIdProvedor");

		if (proveIdProvedor != null) {
			setProveIdProvedor(proveIdProvedor);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _estatus.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _estatus.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _estatus.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _estatus.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Estatus> toCacheModel() {
		return _estatus.toCacheModel();
	}

	@Override
	public int compareTo(Estatus estatus) {
		return _estatus.compareTo(estatus);
	}

	/**
	* Returns the id ot of this estatus.
	*
	* @return the id ot of this estatus
	*/
	@Override
	public int getIdOt() {
		return _estatus.getIdOt();
	}

	/**
	* Returns the id tramite of this estatus.
	*
	* @return the id tramite of this estatus
	*/
	@Override
	public int getIdTramite() {
		return _estatus.getIdTramite();
	}

	/**
	* Returns the primary key of this estatus.
	*
	* @return the primary key of this estatus
	*/
	@Override
	public int getPrimaryKey() {
		return _estatus.getPrimaryKey();
	}

	/**
	* Returns the prove ID provedor of this estatus.
	*
	* @return the prove ID provedor of this estatus
	*/
	@Override
	public int getProveIdProvedor() {
		return _estatus.getProveIdProvedor();
	}

	@Override
	public int hashCode() {
		return _estatus.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _estatus.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new EstatusWrapper((Estatus)_estatus.clone());
	}

	/**
	* Returns the estatus of this estatus.
	*
	* @return the estatus of this estatus
	*/
	@Override
	public java.lang.String getEstatus() {
		return _estatus.getEstatus();
	}

	/**
	* Returns the informacion tramite of this estatus.
	*
	* @return the informacion tramite of this estatus
	*/
	@Override
	public java.lang.String getInformacionTramite() {
		return _estatus.getInformacionTramite();
	}

	@Override
	public java.lang.String toString() {
		return _estatus.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _estatus.toXmlString();
	}

	/**
	* Returns the fecha registro of this estatus.
	*
	* @return the fecha registro of this estatus
	*/
	@Override
	public Date getFechaRegistro() {
		return _estatus.getFechaRegistro();
	}

	@Override
	public Estatus toEscapedModel() {
		return new EstatusWrapper(_estatus.toEscapedModel());
	}

	@Override
	public Estatus toUnescapedModel() {
		return new EstatusWrapper(_estatus.toUnescapedModel());
	}

	@Override
	public void persist() {
		_estatus.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_estatus.setCachedModel(cachedModel);
	}

	/**
	* Sets the estatus of this estatus.
	*
	* @param estatus the estatus of this estatus
	*/
	@Override
	public void setEstatus(java.lang.String estatus) {
		_estatus.setEstatus(estatus);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_estatus.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_estatus.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_estatus.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the fecha registro of this estatus.
	*
	* @param fechaRegistro the fecha registro of this estatus
	*/
	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_estatus.setFechaRegistro(fechaRegistro);
	}

	/**
	* Sets the id ot of this estatus.
	*
	* @param idOt the id ot of this estatus
	*/
	@Override
	public void setIdOt(int idOt) {
		_estatus.setIdOt(idOt);
	}

	/**
	* Sets the id tramite of this estatus.
	*
	* @param idTramite the id tramite of this estatus
	*/
	@Override
	public void setIdTramite(int idTramite) {
		_estatus.setIdTramite(idTramite);
	}

	/**
	* Sets the informacion tramite of this estatus.
	*
	* @param informacionTramite the informacion tramite of this estatus
	*/
	@Override
	public void setInformacionTramite(java.lang.String informacionTramite) {
		_estatus.setInformacionTramite(informacionTramite);
	}

	@Override
	public void setNew(boolean n) {
		_estatus.setNew(n);
	}

	/**
	* Sets the primary key of this estatus.
	*
	* @param primaryKey the primary key of this estatus
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_estatus.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_estatus.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the prove ID provedor of this estatus.
	*
	* @param proveIdProvedor the prove ID provedor of this estatus
	*/
	@Override
	public void setProveIdProvedor(int proveIdProvedor) {
		_estatus.setProveIdProvedor(proveIdProvedor);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EstatusWrapper)) {
			return false;
		}

		EstatusWrapper estatusWrapper = (EstatusWrapper)obj;

		if (Objects.equals(_estatus, estatusWrapper._estatus)) {
			return true;
		}

		return false;
	}

	@Override
	public Estatus getWrappedModel() {
		return _estatus;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _estatus.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _estatus.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_estatus.resetOriginalValues();
	}

	private final Estatus _estatus;
}