/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.estatus.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.estatus.exception.NoSuchEstatusException;
import mx.com.allianz.service.estatus.model.Estatus;

/**
 * The persistence interface for the estatus service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.estatus.service.persistence.impl.EstatusPersistenceImpl
 * @see EstatusUtil
 * @generated
 */
@ProviderType
public interface EstatusPersistence extends BasePersistence<Estatus> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EstatusUtil} to access the estatus persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the estatuses where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @return the matching estatuses
	*/
	public java.util.List<Estatus> findByfindByInformacionTramite(
		java.lang.String informacionTramite);

	/**
	* Returns a range of all the estatuses where informacionTramite = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param informacionTramite the informacion tramite
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @return the range of matching estatuses
	*/
	public java.util.List<Estatus> findByfindByInformacionTramite(
		java.lang.String informacionTramite, int start, int end);

	/**
	* Returns an ordered range of all the estatuses where informacionTramite = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param informacionTramite the informacion tramite
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching estatuses
	*/
	public java.util.List<Estatus> findByfindByInformacionTramite(
		java.lang.String informacionTramite, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Estatus> orderByComparator);

	/**
	* Returns an ordered range of all the estatuses where informacionTramite = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param informacionTramite the informacion tramite
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching estatuses
	*/
	public java.util.List<Estatus> findByfindByInformacionTramite(
		java.lang.String informacionTramite, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Estatus> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching estatus
	* @throws NoSuchEstatusException if a matching estatus could not be found
	*/
	public Estatus findByfindByInformacionTramite_First(
		java.lang.String informacionTramite,
		com.liferay.portal.kernel.util.OrderByComparator<Estatus> orderByComparator)
		throws NoSuchEstatusException;

	/**
	* Returns the first estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching estatus, or <code>null</code> if a matching estatus could not be found
	*/
	public Estatus fetchByfindByInformacionTramite_First(
		java.lang.String informacionTramite,
		com.liferay.portal.kernel.util.OrderByComparator<Estatus> orderByComparator);

	/**
	* Returns the last estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching estatus
	* @throws NoSuchEstatusException if a matching estatus could not be found
	*/
	public Estatus findByfindByInformacionTramite_Last(
		java.lang.String informacionTramite,
		com.liferay.portal.kernel.util.OrderByComparator<Estatus> orderByComparator)
		throws NoSuchEstatusException;

	/**
	* Returns the last estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching estatus, or <code>null</code> if a matching estatus could not be found
	*/
	public Estatus fetchByfindByInformacionTramite_Last(
		java.lang.String informacionTramite,
		com.liferay.portal.kernel.util.OrderByComparator<Estatus> orderByComparator);

	/**
	* Returns the estatuses before and after the current estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param idTramite the primary key of the current estatus
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next estatus
	* @throws NoSuchEstatusException if a estatus with the primary key could not be found
	*/
	public Estatus[] findByfindByInformacionTramite_PrevAndNext(int idTramite,
		java.lang.String informacionTramite,
		com.liferay.portal.kernel.util.OrderByComparator<Estatus> orderByComparator)
		throws NoSuchEstatusException;

	/**
	* Removes all the estatuses where informacionTramite = &#63; from the database.
	*
	* @param informacionTramite the informacion tramite
	*/
	public void removeByfindByInformacionTramite(
		java.lang.String informacionTramite);

	/**
	* Returns the number of estatuses where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @return the number of matching estatuses
	*/
	public int countByfindByInformacionTramite(
		java.lang.String informacionTramite);

	/**
	* Caches the estatus in the entity cache if it is enabled.
	*
	* @param estatus the estatus
	*/
	public void cacheResult(Estatus estatus);

	/**
	* Caches the estatuses in the entity cache if it is enabled.
	*
	* @param estatuses the estatuses
	*/
	public void cacheResult(java.util.List<Estatus> estatuses);

	/**
	* Creates a new estatus with the primary key. Does not add the estatus to the database.
	*
	* @param idTramite the primary key for the new estatus
	* @return the new estatus
	*/
	public Estatus create(int idTramite);

	/**
	* Removes the estatus with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idTramite the primary key of the estatus
	* @return the estatus that was removed
	* @throws NoSuchEstatusException if a estatus with the primary key could not be found
	*/
	public Estatus remove(int idTramite) throws NoSuchEstatusException;

	public Estatus updateImpl(Estatus estatus);

	/**
	* Returns the estatus with the primary key or throws a {@link NoSuchEstatusException} if it could not be found.
	*
	* @param idTramite the primary key of the estatus
	* @return the estatus
	* @throws NoSuchEstatusException if a estatus with the primary key could not be found
	*/
	public Estatus findByPrimaryKey(int idTramite)
		throws NoSuchEstatusException;

	/**
	* Returns the estatus with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idTramite the primary key of the estatus
	* @return the estatus, or <code>null</code> if a estatus with the primary key could not be found
	*/
	public Estatus fetchByPrimaryKey(int idTramite);

	@Override
	public java.util.Map<java.io.Serializable, Estatus> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the estatuses.
	*
	* @return the estatuses
	*/
	public java.util.List<Estatus> findAll();

	/**
	* Returns a range of all the estatuses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @return the range of estatuses
	*/
	public java.util.List<Estatus> findAll(int start, int end);

	/**
	* Returns an ordered range of all the estatuses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of estatuses
	*/
	public java.util.List<Estatus> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Estatus> orderByComparator);

	/**
	* Returns an ordered range of all the estatuses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of estatuses
	*/
	public java.util.List<Estatus> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Estatus> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the estatuses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of estatuses.
	*
	* @return the number of estatuses
	*/
	public int countAll();
}