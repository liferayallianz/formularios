/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.estatus.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.estatus.model.Estatus;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the estatus service. This utility wraps {@link mx.com.allianz.service.estatus.service.persistence.impl.EstatusPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EstatusPersistence
 * @see mx.com.allianz.service.estatus.service.persistence.impl.EstatusPersistenceImpl
 * @generated
 */
@ProviderType
public class EstatusUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Estatus estatus) {
		getPersistence().clearCache(estatus);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Estatus> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Estatus> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Estatus> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Estatus> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Estatus update(Estatus estatus) {
		return getPersistence().update(estatus);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Estatus update(Estatus estatus, ServiceContext serviceContext) {
		return getPersistence().update(estatus, serviceContext);
	}

	/**
	* Returns all the estatuses where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @return the matching estatuses
	*/
	public static List<Estatus> findByfindByInformacionTramite(
		java.lang.String informacionTramite) {
		return getPersistence()
				   .findByfindByInformacionTramite(informacionTramite);
	}

	/**
	* Returns a range of all the estatuses where informacionTramite = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param informacionTramite the informacion tramite
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @return the range of matching estatuses
	*/
	public static List<Estatus> findByfindByInformacionTramite(
		java.lang.String informacionTramite, int start, int end) {
		return getPersistence()
				   .findByfindByInformacionTramite(informacionTramite, start,
			end);
	}

	/**
	* Returns an ordered range of all the estatuses where informacionTramite = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param informacionTramite the informacion tramite
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching estatuses
	*/
	public static List<Estatus> findByfindByInformacionTramite(
		java.lang.String informacionTramite, int start, int end,
		OrderByComparator<Estatus> orderByComparator) {
		return getPersistence()
				   .findByfindByInformacionTramite(informacionTramite, start,
			end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the estatuses where informacionTramite = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param informacionTramite the informacion tramite
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching estatuses
	*/
	public static List<Estatus> findByfindByInformacionTramite(
		java.lang.String informacionTramite, int start, int end,
		OrderByComparator<Estatus> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByInformacionTramite(informacionTramite, start,
			end, orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching estatus
	* @throws NoSuchEstatusException if a matching estatus could not be found
	*/
	public static Estatus findByfindByInformacionTramite_First(
		java.lang.String informacionTramite,
		OrderByComparator<Estatus> orderByComparator)
		throws mx.com.allianz.service.estatus.exception.NoSuchEstatusException {
		return getPersistence()
				   .findByfindByInformacionTramite_First(informacionTramite,
			orderByComparator);
	}

	/**
	* Returns the first estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching estatus, or <code>null</code> if a matching estatus could not be found
	*/
	public static Estatus fetchByfindByInformacionTramite_First(
		java.lang.String informacionTramite,
		OrderByComparator<Estatus> orderByComparator) {
		return getPersistence()
				   .fetchByfindByInformacionTramite_First(informacionTramite,
			orderByComparator);
	}

	/**
	* Returns the last estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching estatus
	* @throws NoSuchEstatusException if a matching estatus could not be found
	*/
	public static Estatus findByfindByInformacionTramite_Last(
		java.lang.String informacionTramite,
		OrderByComparator<Estatus> orderByComparator)
		throws mx.com.allianz.service.estatus.exception.NoSuchEstatusException {
		return getPersistence()
				   .findByfindByInformacionTramite_Last(informacionTramite,
			orderByComparator);
	}

	/**
	* Returns the last estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching estatus, or <code>null</code> if a matching estatus could not be found
	*/
	public static Estatus fetchByfindByInformacionTramite_Last(
		java.lang.String informacionTramite,
		OrderByComparator<Estatus> orderByComparator) {
		return getPersistence()
				   .fetchByfindByInformacionTramite_Last(informacionTramite,
			orderByComparator);
	}

	/**
	* Returns the estatuses before and after the current estatus in the ordered set where informacionTramite = &#63;.
	*
	* @param idTramite the primary key of the current estatus
	* @param informacionTramite the informacion tramite
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next estatus
	* @throws NoSuchEstatusException if a estatus with the primary key could not be found
	*/
	public static Estatus[] findByfindByInformacionTramite_PrevAndNext(
		int idTramite, java.lang.String informacionTramite,
		OrderByComparator<Estatus> orderByComparator)
		throws mx.com.allianz.service.estatus.exception.NoSuchEstatusException {
		return getPersistence()
				   .findByfindByInformacionTramite_PrevAndNext(idTramite,
			informacionTramite, orderByComparator);
	}

	/**
	* Removes all the estatuses where informacionTramite = &#63; from the database.
	*
	* @param informacionTramite the informacion tramite
	*/
	public static void removeByfindByInformacionTramite(
		java.lang.String informacionTramite) {
		getPersistence().removeByfindByInformacionTramite(informacionTramite);
	}

	/**
	* Returns the number of estatuses where informacionTramite = &#63;.
	*
	* @param informacionTramite the informacion tramite
	* @return the number of matching estatuses
	*/
	public static int countByfindByInformacionTramite(
		java.lang.String informacionTramite) {
		return getPersistence()
				   .countByfindByInformacionTramite(informacionTramite);
	}

	/**
	* Caches the estatus in the entity cache if it is enabled.
	*
	* @param estatus the estatus
	*/
	public static void cacheResult(Estatus estatus) {
		getPersistence().cacheResult(estatus);
	}

	/**
	* Caches the estatuses in the entity cache if it is enabled.
	*
	* @param estatuses the estatuses
	*/
	public static void cacheResult(List<Estatus> estatuses) {
		getPersistence().cacheResult(estatuses);
	}

	/**
	* Creates a new estatus with the primary key. Does not add the estatus to the database.
	*
	* @param idTramite the primary key for the new estatus
	* @return the new estatus
	*/
	public static Estatus create(int idTramite) {
		return getPersistence().create(idTramite);
	}

	/**
	* Removes the estatus with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idTramite the primary key of the estatus
	* @return the estatus that was removed
	* @throws NoSuchEstatusException if a estatus with the primary key could not be found
	*/
	public static Estatus remove(int idTramite)
		throws mx.com.allianz.service.estatus.exception.NoSuchEstatusException {
		return getPersistence().remove(idTramite);
	}

	public static Estatus updateImpl(Estatus estatus) {
		return getPersistence().updateImpl(estatus);
	}

	/**
	* Returns the estatus with the primary key or throws a {@link NoSuchEstatusException} if it could not be found.
	*
	* @param idTramite the primary key of the estatus
	* @return the estatus
	* @throws NoSuchEstatusException if a estatus with the primary key could not be found
	*/
	public static Estatus findByPrimaryKey(int idTramite)
		throws mx.com.allianz.service.estatus.exception.NoSuchEstatusException {
		return getPersistence().findByPrimaryKey(idTramite);
	}

	/**
	* Returns the estatus with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idTramite the primary key of the estatus
	* @return the estatus, or <code>null</code> if a estatus with the primary key could not be found
	*/
	public static Estatus fetchByPrimaryKey(int idTramite) {
		return getPersistence().fetchByPrimaryKey(idTramite);
	}

	public static java.util.Map<java.io.Serializable, Estatus> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the estatuses.
	*
	* @return the estatuses
	*/
	public static List<Estatus> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the estatuses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @return the range of estatuses
	*/
	public static List<Estatus> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the estatuses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of estatuses
	*/
	public static List<Estatus> findAll(int start, int end,
		OrderByComparator<Estatus> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the estatuses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of estatuses
	*/
	public static List<Estatus> findAll(int start, int end,
		OrderByComparator<Estatus> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the estatuses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of estatuses.
	*
	* @return the number of estatuses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static EstatusPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<EstatusPersistence, EstatusPersistence> _serviceTracker =
		ServiceTrackerFactory.open(EstatusPersistence.class);
}