/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.estatus.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class EstatusSoap implements Serializable {
	public static EstatusSoap toSoapModel(Estatus model) {
		EstatusSoap soapModel = new EstatusSoap();

		soapModel.setIdTramite(model.getIdTramite());
		soapModel.setIdOt(model.getIdOt());
		soapModel.setFechaRegistro(model.getFechaRegistro());
		soapModel.setEstatus(model.getEstatus());
		soapModel.setInformacionTramite(model.getInformacionTramite());
		soapModel.setProveIdProvedor(model.getProveIdProvedor());

		return soapModel;
	}

	public static EstatusSoap[] toSoapModels(Estatus[] models) {
		EstatusSoap[] soapModels = new EstatusSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EstatusSoap[][] toSoapModels(Estatus[][] models) {
		EstatusSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EstatusSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EstatusSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EstatusSoap[] toSoapModels(List<Estatus> models) {
		List<EstatusSoap> soapModels = new ArrayList<EstatusSoap>(models.size());

		for (Estatus model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EstatusSoap[soapModels.size()]);
	}

	public EstatusSoap() {
	}

	public int getPrimaryKey() {
		return _idTramite;
	}

	public void setPrimaryKey(int pk) {
		setIdTramite(pk);
	}

	public int getIdTramite() {
		return _idTramite;
	}

	public void setIdTramite(int idTramite) {
		_idTramite = idTramite;
	}

	public int getIdOt() {
		return _idOt;
	}

	public void setIdOt(int idOt) {
		_idOt = idOt;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	public String getEstatus() {
		return _estatus;
	}

	public void setEstatus(String estatus) {
		_estatus = estatus;
	}

	public String getInformacionTramite() {
		return _informacionTramite;
	}

	public void setInformacionTramite(String informacionTramite) {
		_informacionTramite = informacionTramite;
	}

	public int getProveIdProvedor() {
		return _proveIdProvedor;
	}

	public void setProveIdProvedor(int proveIdProvedor) {
		_proveIdProvedor = proveIdProvedor;
	}

	private int _idTramite;
	private int _idOt;
	private Date _fechaRegistro;
	private String _estatus;
	private String _informacionTramite;
	private int _proveIdProvedor;
}