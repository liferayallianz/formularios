/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.estatus.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link EstatusLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see EstatusLocalService
 * @generated
 */
@ProviderType
public class EstatusLocalServiceWrapper implements EstatusLocalService,
	ServiceWrapper<EstatusLocalService> {
	public EstatusLocalServiceWrapper(EstatusLocalService estatusLocalService) {
		_estatusLocalService = estatusLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _estatusLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _estatusLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _estatusLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _estatusLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _estatusLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of estatuses.
	*
	* @return the number of estatuses
	*/
	@Override
	public int getEstatusesCount() {
		return _estatusLocalService.getEstatusesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _estatusLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _estatusLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.estatus.model.impl.EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _estatusLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.estatus.model.impl.EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _estatusLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the estatuses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.estatus.model.impl.EstatusModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estatuses
	* @param end the upper bound of the range of estatuses (not inclusive)
	* @return the range of estatuses
	*/
	@Override
	public java.util.List<mx.com.allianz.service.estatus.model.Estatus> getEstatuses(
		int start, int end) {
		return _estatusLocalService.getEstatuses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _estatusLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _estatusLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the estatus to the database. Also notifies the appropriate model listeners.
	*
	* @param estatus the estatus
	* @return the estatus that was added
	*/
	@Override
	public mx.com.allianz.service.estatus.model.Estatus addEstatus(
		mx.com.allianz.service.estatus.model.Estatus estatus) {
		return _estatusLocalService.addEstatus(estatus);
	}

	/**
	* Creates a new estatus with the primary key. Does not add the estatus to the database.
	*
	* @param idTramite the primary key for the new estatus
	* @return the new estatus
	*/
	@Override
	public mx.com.allianz.service.estatus.model.Estatus createEstatus(
		int idTramite) {
		return _estatusLocalService.createEstatus(idTramite);
	}

	/**
	* Deletes the estatus with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idTramite the primary key of the estatus
	* @return the estatus that was removed
	* @throws PortalException if a estatus with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.estatus.model.Estatus deleteEstatus(
		int idTramite)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _estatusLocalService.deleteEstatus(idTramite);
	}

	/**
	* Deletes the estatus from the database. Also notifies the appropriate model listeners.
	*
	* @param estatus the estatus
	* @return the estatus that was removed
	*/
	@Override
	public mx.com.allianz.service.estatus.model.Estatus deleteEstatus(
		mx.com.allianz.service.estatus.model.Estatus estatus) {
		return _estatusLocalService.deleteEstatus(estatus);
	}

	@Override
	public mx.com.allianz.service.estatus.model.Estatus fetchEstatus(
		int idTramite) {
		return _estatusLocalService.fetchEstatus(idTramite);
	}

	/**
	* Returns the estatus with the primary key.
	*
	* @param idTramite the primary key of the estatus
	* @return the estatus
	* @throws PortalException if a estatus with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.estatus.model.Estatus getEstatus(
		int idTramite)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _estatusLocalService.getEstatus(idTramite);
	}

	/**
	* Updates the estatus in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param estatus the estatus
	* @return the estatus that was updated
	*/
	@Override
	public mx.com.allianz.service.estatus.model.Estatus updateEstatus(
		mx.com.allianz.service.estatus.model.Estatus estatus) {
		return _estatusLocalService.updateEstatus(estatus);
	}

	@Override
	public EstatusLocalService getWrappedService() {
		return _estatusLocalService;
	}

	@Override
	public void setWrappedService(EstatusLocalService estatusLocalService) {
		_estatusLocalService = estatusLocalService;
	}

	private EstatusLocalService _estatusLocalService;
}