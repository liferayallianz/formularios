package mx.com.allianz.service.rest.estatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.dto.tramite.EstatusDTO;
import mx.com.allianz.service.estatus.service.EstatusLocalServiceUtil;





/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n (Modulo)
 */
@ApplicationPath("/catalogos.estatus")
/*
 * @Component sirve para garantizar la seguridad del path
 */
@Component(
	immediate = true, property = {"jaxrs.application=true"},
	service = Application.class
)
public class EstatusRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this).
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object)this);
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de los estatus del cat\u00e1logo.
	 * 
	 * @return getAllEstatus().
	 */
	
	//@GET es el nombre del procesamiento de la petici\u00f3n.
		@GET
		//@Path sirve para poner la ruta relativa donde la clase ser\u00e1 hospedada.
		@Path("/getEstatus")
		//@Produces sirve para especificar el tipo de formato de respuesta.
		@Produces({MediaType.APPLICATION_JSON})
		public List<EstatusDTO> getEstatus() {
			
			// Se regresan todos los tramites.
			return getAllEstatus();
		}
		
		/**
		 * Operaci\u00f3n de servicio rest para consulta de todos los tramites del cat\u00e1logo
		 * 
		 * @return getAlltramites().stream()
		 */
		//@GET es el nombre del procesamiento de la petici\u00f3n
		@GET
		//@Path sirve para poner la ruta relativa donde la clase ser\u00e1
		@Path("/findEstatus")
		//@Produces sirve para especificar el tipo de formato de respuesta
		@Produces({MediaType.APPLICATION_JSON})
		public List<EstatusDTO> findtramitesBy(
				//El valor @DefaultValue se utiliza para definir el valor por defecto para el par\u00e1metro matrixParam.
				//@QueryParam se utiliza para extraer los par\u00e1metros de consulta del "componente de consulta" de la URL de la solicitud.
				@DefaultValue("-1")@QueryParam("id_Tramite") int idTramite,
				@DefaultValue("")@QueryParam("id_Ot") int idOt,
				@DefaultValue("")@QueryParam("fecha_Registro") String fechaRegistroString,
				@DefaultValue("")@QueryParam("estatus") String estatus,
				@DefaultValue("")@QueryParam("informacion_Tramite") String informacionTramite,
				@DefaultValue("")@QueryParam("prove_Id_Provedor") int proveIdProvedor){
			
			Date fechaAhora = new Date();
			
			//stream() Devuelve un flujo secuencial considerando la colecci\u00f3n como su origen.
			//convierte la lista en stream
			return getAllEstatus().stream()
					.filter((tramite) ->idTramite == -1 || tramite.getIdTramite() == idTramite)
					.filter((tramite) -> idOt == -1 || tramite.getIdOt() == idOt)
					.filter((tramite) -> fechaRegistroString.equals("") 
							|| !(fechaAhora.compareTo(fromString(fechaRegistroString, fechaAhora)) == 0) 
							|| tramite.getFechaRegistro().compareTo(fromString(fechaRegistroString, fechaAhora)) == 0 )
					.filter((tramite) -> proveIdProvedor == -1 || tramite.getProveIdProvedor() == proveIdProvedor)
					.filter((tramite) -> estatus.isEmpty() || tramite.getEstatus().toUpperCase().contains(estatus.toUpperCase()))
					.filter((tramite) -> informacionTramite.isEmpty() || tramite.getInformacionTramite().toUpperCase().contains(informacionTramite.toUpperCase()))          
					.collect(Collectors.toList());
		}
		private List<EstatusDTO> getAllEstatus(){
			return EstatusLocalServiceUtil.getEstatuses(-1,-1).stream().
					map(estatus -> new EstatusDTO(estatus.getIdTramite(), 
					estatus.getIdOt(), estatus.getFechaRegistro(), 
					estatus.getEstatus(), estatus.getInformacionTramite(), 
					estatus.getProveIdProvedor())).
					collect(Collectors.toList());
		}
		
		public static final String format = "yyyy-MM-dd"; // set the format to whatever you need

	   public Date fromString(String string, Date defaultValue) {
	       SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
	       Optional<Date> fechaParseadaOpcional = null;
	       try {
	    	   Date fechaParseada = simpleDateFormat.parse(string);
	    	   fechaParseadaOpcional = Optional.ofNullable(fechaParseada);
	       } catch (ParseException ex) {
	    	   fechaParseadaOpcional = null;
	       }
	       return fechaParseadaOpcional.orElse(defaultValue);
	   }

		   
	   public String toString(Date t) {
	       return new SimpleDateFormat(format).format(t);
	   }

}
