package mx.com.allianz.service.rest.subgrupo;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.SubgrupoDTO;
import mx.com.allianz.service.subGrupo.service.SubGrupoLocalServiceUtil;


/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio HospitalDTO que
 * permite visualizar una lista de datos, filtra el orden de la b\u00fasqueda y
 * muestra el ordenamiento conforme al filtro.
 * 
 *
 * @author TestingZone
 * @version 1.0
 * @since 2016-12-2
 * 
 * 
 */

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.subgrupo")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)

public class SubgrupoRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los SubGrupos pagos del
	 * catalogo.
	 * 
	 * @return getAllSubgrupo()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getSubgrupo")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<SubgrupoDTO> getSubgrupo(){
		
		return getAllSubgrupo();
		
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los SubGrupo Pago del
	 * catalogo.
	 * 
	 * @return getAllSubgrupo().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findSubgrupo")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<SubgrupoDTO> findSubgrupoBy(
			
			
			@DefaultValue("-1") @QueryParam("id_SubGrupo") int idSubGrupo,
			@DefaultValue("") @QueryParam("codigoSubGrupo") String codigoSubGrupo,
			@DefaultValue("") @QueryParam("codigoGrupoPago") String codigoGrupoPago,
			@DefaultValue("") @QueryParam("descripcionSubGrupo") String descripcionSubGrupo,
			@DefaultValue("") @QueryParam("codigoPais") String codigoPais,
			@DefaultValue("") @QueryParam("codigoEstado") String codigoEstado,
			@DefaultValue("") @QueryParam("codigoCiudad") String codigoCiudad,
			@DefaultValue("") @QueryParam("codigoMunicipio") String codigoMunicipio,
			@DefaultValue("") @QueryParam("direccion") String direccion,
			@DefaultValue("") @QueryParam("nombreContacto") String nombreContacto,
			@DefaultValue("") @QueryParam("telefono") String telefono,
			@DefaultValue("") @QueryParam("zip") String zip,
			@DefaultValue("") @QueryParam("colonia") String colonia,
			@DefaultValue("") @QueryParam("stsCodigoSubGrupo") String stsCodigoSubGrupo,
			@DefaultValue("") @QueryParam("tipoId") String tipoId,
			@DefaultValue("") @QueryParam("numeroId") String numeroId,
			@DefaultValue("") @QueryParam("dvId") String dvId){
		
		return getAllSubgrupo().stream()
				
				.filter((subgrupo) -> idSubGrupo == -1 || subgrupo.getIdSubGrupo() == idSubGrupo)
				.filter((subgrupo) -> codigoSubGrupo.isEmpty() || subgrupo.getCodigoSubGrupo().contains(codigoSubGrupo))
				.filter((subgrupo) -> codigoGrupoPago.isEmpty() || subgrupo.getCodigoGrupoPago().contains(codigoGrupoPago))
				.filter((subgrupo) -> descripcionSubGrupo.isEmpty() || subgrupo.getDescripcionSubGrupo().contains(descripcionSubGrupo))
				.filter((subgrupo) -> codigoPais.isEmpty() || subgrupo.getCodigoPais().contains(codigoPais))
				.filter((subgrupo) -> codigoEstado.isEmpty() || subgrupo.getCodigoEstado().contains(codigoEstado))
				.filter((subgrupo) -> codigoCiudad.isEmpty() || subgrupo.getCodigoCiudad().contains(codigoCiudad))
				.filter((subgrupo) -> codigoMunicipio.isEmpty() || subgrupo.getCodigoMunicipio().contains(codigoMunicipio))
				.filter((subgrupo) -> direccion.isEmpty() || subgrupo.getDireccion().contains(direccion))
				.filter((subgrupo) -> nombreContacto.isEmpty() || subgrupo.getNombreContacto().contains(nombreContacto))
				.filter((subgrupo) -> telefono.isEmpty() || subgrupo.getTelefono().contains(telefono))
				.filter((subgrupo) -> zip.isEmpty() || subgrupo.getZip().contains(zip))
				.filter((subgrupo) -> colonia.isEmpty() || subgrupo.getColonia().contains(colonia))
				.filter((subgrupo) -> stsCodigoSubGrupo.isEmpty() || subgrupo.getStsCodigoSubGrupo().contains(stsCodigoSubGrupo))
				.filter((subgrupo) -> tipoId.isEmpty() || subgrupo.getTipoId().contains(tipoId))
				.filter((subgrupo) -> numeroId.isEmpty() || subgrupo.getNumeroId().contains(numeroId))	
				.filter((subgrupo) -> dvId.isEmpty() || subgrupo.getDvId().contains(dvId))	
		
			.collect(Collectors.toList());
	}
	
	
	
	private List<SubgrupoDTO> getAllSubgrupo() {
		return SubGrupoLocalServiceUtil.getSubGrupos(-1, -1).stream().
			map(subgrupo -> new SubgrupoDTO(subgrupo.getIdSubGrupo(), 
			subgrupo.getCodigoSubGrupo(), subgrupo.getCodigoGrupoPago(),
			subgrupo.getDescripcionSubGrupo(), subgrupo.getCodigoPais(),
			subgrupo.getCodigoEstado(), subgrupo.getCodigoCiudad(),
			subgrupo.getCodigoMunicipio(), subgrupo.getDireccion(),
			subgrupo.getNombreContacto(), subgrupo.getTelefono(),
			subgrupo.getZip(), subgrupo.getColonia(), 
			subgrupo.getStsCodigoSubGrupo(), subgrupo.getTipoId(),
			subgrupo.getNumeroId(), subgrupo.getDvId())).
			collect(Collectors.toList());

	}
	
	
	
	

}
