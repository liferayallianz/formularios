/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.subGrupo.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the SubGrupo service. Represents a row in the &quot;PTL_SUBGRUPOS_PAGO_TR&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see SubGrupoModel
 * @see mx.com.allianz.service.subGrupo.model.impl.SubGrupoImpl
 * @see mx.com.allianz.service.subGrupo.model.impl.SubGrupoModelImpl
 * @generated
 */
@ImplementationClassName("mx.com.allianz.service.subGrupo.model.impl.SubGrupoImpl")
@ProviderType
public interface SubGrupo extends SubGrupoModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link mx.com.allianz.service.subGrupo.model.impl.SubGrupoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<SubGrupo, Integer> ID_SUB_GRUPO_ACCESSOR = new Accessor<SubGrupo, Integer>() {
			@Override
			public Integer get(SubGrupo subGrupo) {
				return subGrupo.getIdSubGrupo();
			}

			@Override
			public Class<Integer> getAttributeClass() {
				return Integer.class;
			}

			@Override
			public Class<SubGrupo> getTypeClass() {
				return SubGrupo.class;
			}
		};
}