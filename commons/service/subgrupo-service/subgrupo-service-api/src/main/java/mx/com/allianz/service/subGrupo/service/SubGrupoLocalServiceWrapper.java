/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.subGrupo.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SubGrupoLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SubGrupoLocalService
 * @generated
 */
@ProviderType
public class SubGrupoLocalServiceWrapper implements SubGrupoLocalService,
	ServiceWrapper<SubGrupoLocalService> {
	public SubGrupoLocalServiceWrapper(
		SubGrupoLocalService subGrupoLocalService) {
		_subGrupoLocalService = subGrupoLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _subGrupoLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _subGrupoLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _subGrupoLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _subGrupoLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _subGrupoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of sub grupos.
	*
	* @return the number of sub grupos
	*/
	@Override
	public int getSubGruposCount() {
		return _subGrupoLocalService.getSubGruposCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _subGrupoLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _subGrupoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.subGrupo.model.impl.SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _subGrupoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.subGrupo.model.impl.SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _subGrupoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the sub grupos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.subGrupo.model.impl.SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @return the range of sub grupos
	*/
	@Override
	public java.util.List<mx.com.allianz.service.subGrupo.model.SubGrupo> getSubGrupos(
		int start, int end) {
		return _subGrupoLocalService.getSubGrupos(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _subGrupoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _subGrupoLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the sub grupo to the database. Also notifies the appropriate model listeners.
	*
	* @param subGrupo the sub grupo
	* @return the sub grupo that was added
	*/
	@Override
	public mx.com.allianz.service.subGrupo.model.SubGrupo addSubGrupo(
		mx.com.allianz.service.subGrupo.model.SubGrupo subGrupo) {
		return _subGrupoLocalService.addSubGrupo(subGrupo);
	}

	/**
	* Creates a new sub grupo with the primary key. Does not add the sub grupo to the database.
	*
	* @param idSubGrupo the primary key for the new sub grupo
	* @return the new sub grupo
	*/
	@Override
	public mx.com.allianz.service.subGrupo.model.SubGrupo createSubGrupo(
		int idSubGrupo) {
		return _subGrupoLocalService.createSubGrupo(idSubGrupo);
	}

	/**
	* Deletes the sub grupo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idSubGrupo the primary key of the sub grupo
	* @return the sub grupo that was removed
	* @throws PortalException if a sub grupo with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.subGrupo.model.SubGrupo deleteSubGrupo(
		int idSubGrupo)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _subGrupoLocalService.deleteSubGrupo(idSubGrupo);
	}

	/**
	* Deletes the sub grupo from the database. Also notifies the appropriate model listeners.
	*
	* @param subGrupo the sub grupo
	* @return the sub grupo that was removed
	*/
	@Override
	public mx.com.allianz.service.subGrupo.model.SubGrupo deleteSubGrupo(
		mx.com.allianz.service.subGrupo.model.SubGrupo subGrupo) {
		return _subGrupoLocalService.deleteSubGrupo(subGrupo);
	}

	@Override
	public mx.com.allianz.service.subGrupo.model.SubGrupo fetchSubGrupo(
		int idSubGrupo) {
		return _subGrupoLocalService.fetchSubGrupo(idSubGrupo);
	}

	/**
	* Returns the sub grupo with the primary key.
	*
	* @param idSubGrupo the primary key of the sub grupo
	* @return the sub grupo
	* @throws PortalException if a sub grupo with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.subGrupo.model.SubGrupo getSubGrupo(
		int idSubGrupo)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _subGrupoLocalService.getSubGrupo(idSubGrupo);
	}

	/**
	* Updates the sub grupo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param subGrupo the sub grupo
	* @return the sub grupo that was updated
	*/
	@Override
	public mx.com.allianz.service.subGrupo.model.SubGrupo updateSubGrupo(
		mx.com.allianz.service.subGrupo.model.SubGrupo subGrupo) {
		return _subGrupoLocalService.updateSubGrupo(subGrupo);
	}

	@Override
	public SubGrupoLocalService getWrappedService() {
		return _subGrupoLocalService;
	}

	@Override
	public void setWrappedService(SubGrupoLocalService subGrupoLocalService) {
		_subGrupoLocalService = subGrupoLocalService;
	}

	private SubGrupoLocalService _subGrupoLocalService;
}