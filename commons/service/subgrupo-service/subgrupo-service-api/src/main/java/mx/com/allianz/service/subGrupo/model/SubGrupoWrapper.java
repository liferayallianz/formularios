/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.subGrupo.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link SubGrupo}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SubGrupo
 * @generated
 */
@ProviderType
public class SubGrupoWrapper implements SubGrupo, ModelWrapper<SubGrupo> {
	public SubGrupoWrapper(SubGrupo subGrupo) {
		_subGrupo = subGrupo;
	}

	@Override
	public Class<?> getModelClass() {
		return SubGrupo.class;
	}

	@Override
	public String getModelClassName() {
		return SubGrupo.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idSubGrupo", getIdSubGrupo());
		attributes.put("codigoSubGrupo", getCodigoSubGrupo());
		attributes.put("codigoGrupoPago", getCodigoGrupoPago());
		attributes.put("descripcionSubGrupo", getDescripcionSubGrupo());
		attributes.put("codigoPais", getCodigoPais());
		attributes.put("codigoEstado", getCodigoEstado());
		attributes.put("codigoCiudad", getCodigoCiudad());
		attributes.put("codigoMunicipio", getCodigoMunicipio());
		attributes.put("direccion", getDireccion());
		attributes.put("nombreContacto", getNombreContacto());
		attributes.put("telefono", getTelefono());
		attributes.put("zip", getZip());
		attributes.put("colonia", getColonia());
		attributes.put("stsCodigoSubGrupo", getStsCodigoSubGrupo());
		attributes.put("tipoId", getTipoId());
		attributes.put("numeroId", getNumeroId());
		attributes.put("dvId", getDvId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idSubGrupo = (Integer)attributes.get("idSubGrupo");

		if (idSubGrupo != null) {
			setIdSubGrupo(idSubGrupo);
		}

		String codigoSubGrupo = (String)attributes.get("codigoSubGrupo");

		if (codigoSubGrupo != null) {
			setCodigoSubGrupo(codigoSubGrupo);
		}

		String codigoGrupoPago = (String)attributes.get("codigoGrupoPago");

		if (codigoGrupoPago != null) {
			setCodigoGrupoPago(codigoGrupoPago);
		}

		String descripcionSubGrupo = (String)attributes.get(
				"descripcionSubGrupo");

		if (descripcionSubGrupo != null) {
			setDescripcionSubGrupo(descripcionSubGrupo);
		}

		String codigoPais = (String)attributes.get("codigoPais");

		if (codigoPais != null) {
			setCodigoPais(codigoPais);
		}

		String codigoEstado = (String)attributes.get("codigoEstado");

		if (codigoEstado != null) {
			setCodigoEstado(codigoEstado);
		}

		String codigoCiudad = (String)attributes.get("codigoCiudad");

		if (codigoCiudad != null) {
			setCodigoCiudad(codigoCiudad);
		}

		String codigoMunicipio = (String)attributes.get("codigoMunicipio");

		if (codigoMunicipio != null) {
			setCodigoMunicipio(codigoMunicipio);
		}

		String direccion = (String)attributes.get("direccion");

		if (direccion != null) {
			setDireccion(direccion);
		}

		String nombreContacto = (String)attributes.get("nombreContacto");

		if (nombreContacto != null) {
			setNombreContacto(nombreContacto);
		}

		String telefono = (String)attributes.get("telefono");

		if (telefono != null) {
			setTelefono(telefono);
		}

		String zip = (String)attributes.get("zip");

		if (zip != null) {
			setZip(zip);
		}

		String colonia = (String)attributes.get("colonia");

		if (colonia != null) {
			setColonia(colonia);
		}

		String stsCodigoSubGrupo = (String)attributes.get("stsCodigoSubGrupo");

		if (stsCodigoSubGrupo != null) {
			setStsCodigoSubGrupo(stsCodigoSubGrupo);
		}

		String tipoId = (String)attributes.get("tipoId");

		if (tipoId != null) {
			setTipoId(tipoId);
		}

		String numeroId = (String)attributes.get("numeroId");

		if (numeroId != null) {
			setNumeroId(numeroId);
		}

		String dvId = (String)attributes.get("dvId");

		if (dvId != null) {
			setDvId(dvId);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _subGrupo.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _subGrupo.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _subGrupo.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _subGrupo.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<SubGrupo> toCacheModel() {
		return _subGrupo.toCacheModel();
	}

	@Override
	public int compareTo(SubGrupo subGrupo) {
		return _subGrupo.compareTo(subGrupo);
	}

	/**
	* Returns the id sub grupo of this sub grupo.
	*
	* @return the id sub grupo of this sub grupo
	*/
	@Override
	public int getIdSubGrupo() {
		return _subGrupo.getIdSubGrupo();
	}

	/**
	* Returns the primary key of this sub grupo.
	*
	* @return the primary key of this sub grupo
	*/
	@Override
	public int getPrimaryKey() {
		return _subGrupo.getPrimaryKey();
	}

	@Override
	public int hashCode() {
		return _subGrupo.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _subGrupo.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SubGrupoWrapper((SubGrupo)_subGrupo.clone());
	}

	/**
	* Returns the codigo ciudad of this sub grupo.
	*
	* @return the codigo ciudad of this sub grupo
	*/
	@Override
	public java.lang.String getCodigoCiudad() {
		return _subGrupo.getCodigoCiudad();
	}

	/**
	* Returns the codigo estado of this sub grupo.
	*
	* @return the codigo estado of this sub grupo
	*/
	@Override
	public java.lang.String getCodigoEstado() {
		return _subGrupo.getCodigoEstado();
	}

	/**
	* Returns the codigo grupo pago of this sub grupo.
	*
	* @return the codigo grupo pago of this sub grupo
	*/
	@Override
	public java.lang.String getCodigoGrupoPago() {
		return _subGrupo.getCodigoGrupoPago();
	}

	/**
	* Returns the codigo municipio of this sub grupo.
	*
	* @return the codigo municipio of this sub grupo
	*/
	@Override
	public java.lang.String getCodigoMunicipio() {
		return _subGrupo.getCodigoMunicipio();
	}

	/**
	* Returns the codigo pais of this sub grupo.
	*
	* @return the codigo pais of this sub grupo
	*/
	@Override
	public java.lang.String getCodigoPais() {
		return _subGrupo.getCodigoPais();
	}

	/**
	* Returns the codigo sub grupo of this sub grupo.
	*
	* @return the codigo sub grupo of this sub grupo
	*/
	@Override
	public java.lang.String getCodigoSubGrupo() {
		return _subGrupo.getCodigoSubGrupo();
	}

	/**
	* Returns the colonia of this sub grupo.
	*
	* @return the colonia of this sub grupo
	*/
	@Override
	public java.lang.String getColonia() {
		return _subGrupo.getColonia();
	}

	/**
	* Returns the descripcion sub grupo of this sub grupo.
	*
	* @return the descripcion sub grupo of this sub grupo
	*/
	@Override
	public java.lang.String getDescripcionSubGrupo() {
		return _subGrupo.getDescripcionSubGrupo();
	}

	/**
	* Returns the direccion of this sub grupo.
	*
	* @return the direccion of this sub grupo
	*/
	@Override
	public java.lang.String getDireccion() {
		return _subGrupo.getDireccion();
	}

	/**
	* Returns the dv ID of this sub grupo.
	*
	* @return the dv ID of this sub grupo
	*/
	@Override
	public java.lang.String getDvId() {
		return _subGrupo.getDvId();
	}

	/**
	* Returns the nombre contacto of this sub grupo.
	*
	* @return the nombre contacto of this sub grupo
	*/
	@Override
	public java.lang.String getNombreContacto() {
		return _subGrupo.getNombreContacto();
	}

	/**
	* Returns the numero ID of this sub grupo.
	*
	* @return the numero ID of this sub grupo
	*/
	@Override
	public java.lang.String getNumeroId() {
		return _subGrupo.getNumeroId();
	}

	/**
	* Returns the sts codigo sub grupo of this sub grupo.
	*
	* @return the sts codigo sub grupo of this sub grupo
	*/
	@Override
	public java.lang.String getStsCodigoSubGrupo() {
		return _subGrupo.getStsCodigoSubGrupo();
	}

	/**
	* Returns the telefono of this sub grupo.
	*
	* @return the telefono of this sub grupo
	*/
	@Override
	public java.lang.String getTelefono() {
		return _subGrupo.getTelefono();
	}

	/**
	* Returns the tipo ID of this sub grupo.
	*
	* @return the tipo ID of this sub grupo
	*/
	@Override
	public java.lang.String getTipoId() {
		return _subGrupo.getTipoId();
	}

	/**
	* Returns the zip of this sub grupo.
	*
	* @return the zip of this sub grupo
	*/
	@Override
	public java.lang.String getZip() {
		return _subGrupo.getZip();
	}

	@Override
	public java.lang.String toString() {
		return _subGrupo.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _subGrupo.toXmlString();
	}

	@Override
	public SubGrupo toEscapedModel() {
		return new SubGrupoWrapper(_subGrupo.toEscapedModel());
	}

	@Override
	public SubGrupo toUnescapedModel() {
		return new SubGrupoWrapper(_subGrupo.toUnescapedModel());
	}

	@Override
	public void persist() {
		_subGrupo.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_subGrupo.setCachedModel(cachedModel);
	}

	/**
	* Sets the codigo ciudad of this sub grupo.
	*
	* @param codigoCiudad the codigo ciudad of this sub grupo
	*/
	@Override
	public void setCodigoCiudad(java.lang.String codigoCiudad) {
		_subGrupo.setCodigoCiudad(codigoCiudad);
	}

	/**
	* Sets the codigo estado of this sub grupo.
	*
	* @param codigoEstado the codigo estado of this sub grupo
	*/
	@Override
	public void setCodigoEstado(java.lang.String codigoEstado) {
		_subGrupo.setCodigoEstado(codigoEstado);
	}

	/**
	* Sets the codigo grupo pago of this sub grupo.
	*
	* @param codigoGrupoPago the codigo grupo pago of this sub grupo
	*/
	@Override
	public void setCodigoGrupoPago(java.lang.String codigoGrupoPago) {
		_subGrupo.setCodigoGrupoPago(codigoGrupoPago);
	}

	/**
	* Sets the codigo municipio of this sub grupo.
	*
	* @param codigoMunicipio the codigo municipio of this sub grupo
	*/
	@Override
	public void setCodigoMunicipio(java.lang.String codigoMunicipio) {
		_subGrupo.setCodigoMunicipio(codigoMunicipio);
	}

	/**
	* Sets the codigo pais of this sub grupo.
	*
	* @param codigoPais the codigo pais of this sub grupo
	*/
	@Override
	public void setCodigoPais(java.lang.String codigoPais) {
		_subGrupo.setCodigoPais(codigoPais);
	}

	/**
	* Sets the codigo sub grupo of this sub grupo.
	*
	* @param codigoSubGrupo the codigo sub grupo of this sub grupo
	*/
	@Override
	public void setCodigoSubGrupo(java.lang.String codigoSubGrupo) {
		_subGrupo.setCodigoSubGrupo(codigoSubGrupo);
	}

	/**
	* Sets the colonia of this sub grupo.
	*
	* @param colonia the colonia of this sub grupo
	*/
	@Override
	public void setColonia(java.lang.String colonia) {
		_subGrupo.setColonia(colonia);
	}

	/**
	* Sets the descripcion sub grupo of this sub grupo.
	*
	* @param descripcionSubGrupo the descripcion sub grupo of this sub grupo
	*/
	@Override
	public void setDescripcionSubGrupo(java.lang.String descripcionSubGrupo) {
		_subGrupo.setDescripcionSubGrupo(descripcionSubGrupo);
	}

	/**
	* Sets the direccion of this sub grupo.
	*
	* @param direccion the direccion of this sub grupo
	*/
	@Override
	public void setDireccion(java.lang.String direccion) {
		_subGrupo.setDireccion(direccion);
	}

	/**
	* Sets the dv ID of this sub grupo.
	*
	* @param dvId the dv ID of this sub grupo
	*/
	@Override
	public void setDvId(java.lang.String dvId) {
		_subGrupo.setDvId(dvId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_subGrupo.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_subGrupo.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_subGrupo.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the id sub grupo of this sub grupo.
	*
	* @param idSubGrupo the id sub grupo of this sub grupo
	*/
	@Override
	public void setIdSubGrupo(int idSubGrupo) {
		_subGrupo.setIdSubGrupo(idSubGrupo);
	}

	@Override
	public void setNew(boolean n) {
		_subGrupo.setNew(n);
	}

	/**
	* Sets the nombre contacto of this sub grupo.
	*
	* @param nombreContacto the nombre contacto of this sub grupo
	*/
	@Override
	public void setNombreContacto(java.lang.String nombreContacto) {
		_subGrupo.setNombreContacto(nombreContacto);
	}

	/**
	* Sets the numero ID of this sub grupo.
	*
	* @param numeroId the numero ID of this sub grupo
	*/
	@Override
	public void setNumeroId(java.lang.String numeroId) {
		_subGrupo.setNumeroId(numeroId);
	}

	/**
	* Sets the primary key of this sub grupo.
	*
	* @param primaryKey the primary key of this sub grupo
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_subGrupo.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_subGrupo.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the sts codigo sub grupo of this sub grupo.
	*
	* @param stsCodigoSubGrupo the sts codigo sub grupo of this sub grupo
	*/
	@Override
	public void setStsCodigoSubGrupo(java.lang.String stsCodigoSubGrupo) {
		_subGrupo.setStsCodigoSubGrupo(stsCodigoSubGrupo);
	}

	/**
	* Sets the telefono of this sub grupo.
	*
	* @param telefono the telefono of this sub grupo
	*/
	@Override
	public void setTelefono(java.lang.String telefono) {
		_subGrupo.setTelefono(telefono);
	}

	/**
	* Sets the tipo ID of this sub grupo.
	*
	* @param tipoId the tipo ID of this sub grupo
	*/
	@Override
	public void setTipoId(java.lang.String tipoId) {
		_subGrupo.setTipoId(tipoId);
	}

	/**
	* Sets the zip of this sub grupo.
	*
	* @param zip the zip of this sub grupo
	*/
	@Override
	public void setZip(java.lang.String zip) {
		_subGrupo.setZip(zip);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SubGrupoWrapper)) {
			return false;
		}

		SubGrupoWrapper subGrupoWrapper = (SubGrupoWrapper)obj;

		if (Objects.equals(_subGrupo, subGrupoWrapper._subGrupo)) {
			return true;
		}

		return false;
	}

	@Override
	public SubGrupo getWrappedModel() {
		return _subGrupo;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _subGrupo.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _subGrupo.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_subGrupo.resetOriginalValues();
	}

	private final SubGrupo _subGrupo;
}