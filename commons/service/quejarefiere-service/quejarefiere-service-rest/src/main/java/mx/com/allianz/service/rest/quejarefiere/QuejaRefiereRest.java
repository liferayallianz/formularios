package mx.com.allianz.service.rest.quejarefiere;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.QuejaRefiereDTO;
import mx.com.allianz.service.quejarefiere.service.QuejaRefiereLocalServiceUtil;


/**
 * La aplicaci\u00f3n genera un path para el consumo del servicio
 * QuejaRefiereDTO que permite visualizar una lista de datos, filtra el orden de
 * la b\u00fasqueda y muestra el ordenamiento conforme al filtro.
 * 
 *
 * @author TestingZone
 * @version 1.0
 * @since 2017-01-1
 * 
 * 
 */

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.quejarefiere")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)
public class QuejaRefiereRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todas las quejas 
	 * del catalogo.
	 * 
	 * @return getAllQuejasRefiere()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getQuejarefiere")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<QuejaRefiereDTO> getQuejarefiere(){
		
		return getAllQuejasRefiere();
		
	}
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los Proveedores del
	 * catalogo.
	 * 
	 * @return getAllProveedores().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.

	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findQuejarefiere")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	
	public List<QuejaRefiereDTO>findQuejaRefiereBy(
			
			@DefaultValue("") @QueryParam("clave") String clave,
			@DefaultValue("") @QueryParam("clave_relacion") String claverelacion,
			@DefaultValue("") @QueryParam("descripcion") String descripcion,
			@DefaultValue("")@QueryParam("ordena") String ordena,
			@DefaultValue("") @QueryParam("flujoPuc") String flujoPuc, 
			@DefaultValue("") @QueryParam("codSubProblema") String codSubProblema){
		
		return getAllQuejasRefiere().stream()
		
	
	
	.filter((quejarefiere) -> clave.isEmpty() || quejarefiere.getClave().contains(clave))
	.filter((quejarefiere) -> claverelacion.isEmpty() || quejarefiere.getClaverelacion().contains(claverelacion))
	.filter((quejarefiere) -> descripcion.isEmpty() || quejarefiere.getDescripcion().contains(descripcion))
	.filter((quejarefiere) -> ordena.isEmpty() || quejarefiere.getOrdena() == ordena)
	.filter((quejarefiere) -> flujoPuc.isEmpty() || quejarefiere.getFlujoPuc().contains(flujoPuc))
	.filter((quejarefiere) -> codSubProblema.isEmpty() || quejarefiere.getCodSubProblema().contains(codSubProblema))

	.collect(Collectors.toList());
	}
	

	private List<QuejaRefiereDTO> getAllQuejasRefiere() {
		return QuejaRefiereLocalServiceUtil.getQuejaRefieres(-1, -1).stream()
				.map(quejarefiere -> new QuejaRefiereDTO(quejarefiere.getClaveReferencia(),
				quejarefiere.getDescripcion(), quejarefiere.getOrdena()+"", 
				quejarefiere.getFlujoPuc(), quejarefiere.getCodSubProblema(), 
				quejarefiere.getClaveRelacion())).collect(Collectors.toList());


	}
	
}
