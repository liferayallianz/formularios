/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejarefiere.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link QuejaRefiere}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRefiere
 * @generated
 */
@ProviderType
public class QuejaRefiereWrapper implements QuejaRefiere,
	ModelWrapper<QuejaRefiere> {
	public QuejaRefiereWrapper(QuejaRefiere quejaRefiere) {
		_quejaRefiere = quejaRefiere;
	}

	@Override
	public Class<?> getModelClass() {
		return QuejaRefiere.class;
	}

	@Override
	public String getModelClassName() {
		return QuejaRefiere.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("claveReferencia", getClaveReferencia());
		attributes.put("claveRelacion", getClaveRelacion());
		attributes.put("descripcion", getDescripcion());
		attributes.put("ordena", getOrdena());
		attributes.put("flujoPuc", getFlujoPuc());
		attributes.put("codSubProblema", getCodSubProblema());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String claveReferencia = (String)attributes.get("claveReferencia");

		if (claveReferencia != null) {
			setClaveReferencia(claveReferencia);
		}

		String claveRelacion = (String)attributes.get("claveRelacion");

		if (claveRelacion != null) {
			setClaveRelacion(claveRelacion);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		String ordena = (String)attributes.get("ordena");

		if (ordena != null) {
			setOrdena(ordena);
		}

		String flujoPuc = (String)attributes.get("flujoPuc");

		if (flujoPuc != null) {
			setFlujoPuc(flujoPuc);
		}

		String codSubProblema = (String)attributes.get("codSubProblema");

		if (codSubProblema != null) {
			setCodSubProblema(codSubProblema);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _quejaRefiere.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _quejaRefiere.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _quejaRefiere.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _quejaRefiere.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<QuejaRefiere> toCacheModel() {
		return _quejaRefiere.toCacheModel();
	}

	@Override
	public int compareTo(QuejaRefiere quejaRefiere) {
		return _quejaRefiere.compareTo(quejaRefiere);
	}

	@Override
	public int hashCode() {
		return _quejaRefiere.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _quejaRefiere.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new QuejaRefiereWrapper((QuejaRefiere)_quejaRefiere.clone());
	}

	/**
	* Returns the clave referencia of this queja refiere.
	*
	* @return the clave referencia of this queja refiere
	*/
	@Override
	public java.lang.String getClaveReferencia() {
		return _quejaRefiere.getClaveReferencia();
	}

	/**
	* Returns the clave relacion of this queja refiere.
	*
	* @return the clave relacion of this queja refiere
	*/
	@Override
	public java.lang.String getClaveRelacion() {
		return _quejaRefiere.getClaveRelacion();
	}

	/**
	* Returns the cod sub problema of this queja refiere.
	*
	* @return the cod sub problema of this queja refiere
	*/
	@Override
	public java.lang.String getCodSubProblema() {
		return _quejaRefiere.getCodSubProblema();
	}

	/**
	* Returns the descripcion of this queja refiere.
	*
	* @return the descripcion of this queja refiere
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _quejaRefiere.getDescripcion();
	}

	/**
	* Returns the flujo puc of this queja refiere.
	*
	* @return the flujo puc of this queja refiere
	*/
	@Override
	public java.lang.String getFlujoPuc() {
		return _quejaRefiere.getFlujoPuc();
	}

	/**
	* Returns the ordena of this queja refiere.
	*
	* @return the ordena of this queja refiere
	*/
	@Override
	public java.lang.String getOrdena() {
		return _quejaRefiere.getOrdena();
	}

	@Override
	public java.lang.String toString() {
		return _quejaRefiere.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _quejaRefiere.toXmlString();
	}

	@Override
	public QuejaRefiere toEscapedModel() {
		return new QuejaRefiereWrapper(_quejaRefiere.toEscapedModel());
	}

	@Override
	public QuejaRefiere toUnescapedModel() {
		return new QuejaRefiereWrapper(_quejaRefiere.toUnescapedModel());
	}

	/**
	* Returns the primary key of this queja refiere.
	*
	* @return the primary key of this queja refiere
	*/
	@Override
	public mx.com.allianz.service.quejarefiere.service.persistence.QuejaRefierePK getPrimaryKey() {
		return _quejaRefiere.getPrimaryKey();
	}

	@Override
	public void persist() {
		_quejaRefiere.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_quejaRefiere.setCachedModel(cachedModel);
	}

	/**
	* Sets the clave referencia of this queja refiere.
	*
	* @param claveReferencia the clave referencia of this queja refiere
	*/
	@Override
	public void setClaveReferencia(java.lang.String claveReferencia) {
		_quejaRefiere.setClaveReferencia(claveReferencia);
	}

	/**
	* Sets the clave relacion of this queja refiere.
	*
	* @param claveRelacion the clave relacion of this queja refiere
	*/
	@Override
	public void setClaveRelacion(java.lang.String claveRelacion) {
		_quejaRefiere.setClaveRelacion(claveRelacion);
	}

	/**
	* Sets the cod sub problema of this queja refiere.
	*
	* @param codSubProblema the cod sub problema of this queja refiere
	*/
	@Override
	public void setCodSubProblema(java.lang.String codSubProblema) {
		_quejaRefiere.setCodSubProblema(codSubProblema);
	}

	/**
	* Sets the descripcion of this queja refiere.
	*
	* @param descripcion the descripcion of this queja refiere
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_quejaRefiere.setDescripcion(descripcion);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_quejaRefiere.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_quejaRefiere.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_quejaRefiere.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the flujo puc of this queja refiere.
	*
	* @param flujoPuc the flujo puc of this queja refiere
	*/
	@Override
	public void setFlujoPuc(java.lang.String flujoPuc) {
		_quejaRefiere.setFlujoPuc(flujoPuc);
	}

	@Override
	public void setNew(boolean n) {
		_quejaRefiere.setNew(n);
	}

	/**
	* Sets the ordena of this queja refiere.
	*
	* @param ordena the ordena of this queja refiere
	*/
	@Override
	public void setOrdena(java.lang.String ordena) {
		_quejaRefiere.setOrdena(ordena);
	}

	/**
	* Sets the primary key of this queja refiere.
	*
	* @param primaryKey the primary key of this queja refiere
	*/
	@Override
	public void setPrimaryKey(
		mx.com.allianz.service.quejarefiere.service.persistence.QuejaRefierePK primaryKey) {
		_quejaRefiere.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_quejaRefiere.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuejaRefiereWrapper)) {
			return false;
		}

		QuejaRefiereWrapper quejaRefiereWrapper = (QuejaRefiereWrapper)obj;

		if (Objects.equals(_quejaRefiere, quejaRefiereWrapper._quejaRefiere)) {
			return true;
		}

		return false;
	}

	@Override
	public QuejaRefiere getWrappedModel() {
		return _quejaRefiere;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _quejaRefiere.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _quejaRefiere.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_quejaRefiere.resetOriginalValues();
	}

	private final QuejaRefiere _quejaRefiere;
}