/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejarefiere.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the QuejaRefiere service. Represents a row in the &quot;QJA_REFIERE&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRefiereModel
 * @see mx.com.allianz.service.quejarefiere.model.impl.QuejaRefiereImpl
 * @see mx.com.allianz.service.quejarefiere.model.impl.QuejaRefiereModelImpl
 * @generated
 */
@ImplementationClassName("mx.com.allianz.service.quejarefiere.model.impl.QuejaRefiereImpl")
@ProviderType
public interface QuejaRefiere extends QuejaRefiereModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link mx.com.allianz.service.quejarefiere.model.impl.QuejaRefiereImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<QuejaRefiere, String> CLAVE_REFERENCIA_ACCESSOR =
		new Accessor<QuejaRefiere, String>() {
			@Override
			public String get(QuejaRefiere quejaRefiere) {
				return quejaRefiere.getClaveReferencia();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<QuejaRefiere> getTypeClass() {
				return QuejaRefiere.class;
			}
		};

	public static final Accessor<QuejaRefiere, String> CLAVE_RELACION_ACCESSOR = new Accessor<QuejaRefiere, String>() {
			@Override
			public String get(QuejaRefiere quejaRefiere) {
				return quejaRefiere.getClaveRelacion();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<QuejaRefiere> getTypeClass() {
				return QuejaRefiere.class;
			}
		};
}