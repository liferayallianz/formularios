/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejarefiere.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class QuejaRefierePK implements Comparable<QuejaRefierePK>, Serializable {
	public String claveReferencia;
	public String claveRelacion;

	public QuejaRefierePK() {
	}

	public QuejaRefierePK(String claveReferencia, String claveRelacion) {
		this.claveReferencia = claveReferencia;
		this.claveRelacion = claveRelacion;
	}

	public String getClaveReferencia() {
		return claveReferencia;
	}

	public void setClaveReferencia(String claveReferencia) {
		this.claveReferencia = claveReferencia;
	}

	public String getClaveRelacion() {
		return claveRelacion;
	}

	public void setClaveRelacion(String claveRelacion) {
		this.claveRelacion = claveRelacion;
	}

	@Override
	public int compareTo(QuejaRefierePK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		value = claveReferencia.compareTo(pk.claveReferencia);

		if (value != 0) {
			return value;
		}

		value = claveRelacion.compareTo(pk.claveRelacion);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuejaRefierePK)) {
			return false;
		}

		QuejaRefierePK pk = (QuejaRefierePK)obj;

		if ((claveReferencia.equals(pk.claveReferencia)) &&
				(claveRelacion.equals(pk.claveRelacion))) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, claveReferencia);
		hashCode = HashUtil.hash(hashCode, claveRelacion);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("claveReferencia");
		sb.append(StringPool.EQUAL);
		sb.append(claveReferencia);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("claveRelacion");
		sb.append(StringPool.EQUAL);
		sb.append(claveRelacion);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}