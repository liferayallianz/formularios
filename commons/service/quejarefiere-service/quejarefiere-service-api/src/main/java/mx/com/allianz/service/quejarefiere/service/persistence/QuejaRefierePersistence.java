/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejarefiere.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.quejarefiere.exception.NoSuchQuejaRefiereException;
import mx.com.allianz.service.quejarefiere.model.QuejaRefiere;

/**
 * The persistence interface for the queja refiere service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.quejarefiere.service.persistence.impl.QuejaRefierePersistenceImpl
 * @see QuejaRefiereUtil
 * @generated
 */
@ProviderType
public interface QuejaRefierePersistence extends BasePersistence<QuejaRefiere> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QuejaRefiereUtil} to access the queja refiere persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the queja refieres where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching queja refieres
	*/
	public java.util.List<QuejaRefiere> findByfindByDescripcion(
		java.lang.String descripcion);

	/**
	* Returns a range of all the queja refieres where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @return the range of matching queja refieres
	*/
	public java.util.List<QuejaRefiere> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end);

	/**
	* Returns an ordered range of all the queja refieres where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching queja refieres
	*/
	public java.util.List<QuejaRefiere> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRefiere> orderByComparator);

	/**
	* Returns an ordered range of all the queja refieres where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching queja refieres
	*/
	public java.util.List<QuejaRefiere> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRefiere> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja refiere
	* @throws NoSuchQuejaRefiereException if a matching queja refiere could not be found
	*/
	public QuejaRefiere findByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRefiere> orderByComparator)
		throws NoSuchQuejaRefiereException;

	/**
	* Returns the first queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja refiere, or <code>null</code> if a matching queja refiere could not be found
	*/
	public QuejaRefiere fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRefiere> orderByComparator);

	/**
	* Returns the last queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja refiere
	* @throws NoSuchQuejaRefiereException if a matching queja refiere could not be found
	*/
	public QuejaRefiere findByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRefiere> orderByComparator)
		throws NoSuchQuejaRefiereException;

	/**
	* Returns the last queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja refiere, or <code>null</code> if a matching queja refiere could not be found
	*/
	public QuejaRefiere fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRefiere> orderByComparator);

	/**
	* Returns the queja refieres before and after the current queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param quejaRefierePK the primary key of the current queja refiere
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next queja refiere
	* @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	*/
	public QuejaRefiere[] findByfindByDescripcion_PrevAndNext(
		QuejaRefierePK quejaRefierePK, java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRefiere> orderByComparator)
		throws NoSuchQuejaRefiereException;

	/**
	* Removes all the queja refieres where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public void removeByfindByDescripcion(java.lang.String descripcion);

	/**
	* Returns the number of queja refieres where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching queja refieres
	*/
	public int countByfindByDescripcion(java.lang.String descripcion);

	/**
	* Caches the queja refiere in the entity cache if it is enabled.
	*
	* @param quejaRefiere the queja refiere
	*/
	public void cacheResult(QuejaRefiere quejaRefiere);

	/**
	* Caches the queja refieres in the entity cache if it is enabled.
	*
	* @param quejaRefieres the queja refieres
	*/
	public void cacheResult(java.util.List<QuejaRefiere> quejaRefieres);

	/**
	* Creates a new queja refiere with the primary key. Does not add the queja refiere to the database.
	*
	* @param quejaRefierePK the primary key for the new queja refiere
	* @return the new queja refiere
	*/
	public QuejaRefiere create(QuejaRefierePK quejaRefierePK);

	/**
	* Removes the queja refiere with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param quejaRefierePK the primary key of the queja refiere
	* @return the queja refiere that was removed
	* @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	*/
	public QuejaRefiere remove(QuejaRefierePK quejaRefierePK)
		throws NoSuchQuejaRefiereException;

	public QuejaRefiere updateImpl(QuejaRefiere quejaRefiere);

	/**
	* Returns the queja refiere with the primary key or throws a {@link NoSuchQuejaRefiereException} if it could not be found.
	*
	* @param quejaRefierePK the primary key of the queja refiere
	* @return the queja refiere
	* @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	*/
	public QuejaRefiere findByPrimaryKey(QuejaRefierePK quejaRefierePK)
		throws NoSuchQuejaRefiereException;

	/**
	* Returns the queja refiere with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param quejaRefierePK the primary key of the queja refiere
	* @return the queja refiere, or <code>null</code> if a queja refiere with the primary key could not be found
	*/
	public QuejaRefiere fetchByPrimaryKey(QuejaRefierePK quejaRefierePK);

	@Override
	public java.util.Map<java.io.Serializable, QuejaRefiere> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the queja refieres.
	*
	* @return the queja refieres
	*/
	public java.util.List<QuejaRefiere> findAll();

	/**
	* Returns a range of all the queja refieres.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @return the range of queja refieres
	*/
	public java.util.List<QuejaRefiere> findAll(int start, int end);

	/**
	* Returns an ordered range of all the queja refieres.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of queja refieres
	*/
	public java.util.List<QuejaRefiere> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRefiere> orderByComparator);

	/**
	* Returns an ordered range of all the queja refieres.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of queja refieres
	*/
	public java.util.List<QuejaRefiere> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuejaRefiere> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the queja refieres from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of queja refieres.
	*
	* @return the number of queja refieres
	*/
	public int countAll();
}