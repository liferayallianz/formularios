/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejarefiere.model;

import aQute.bnd.annotation.ProviderType;

import mx.com.allianz.service.quejarefiere.service.persistence.QuejaRefierePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class QuejaRefiereSoap implements Serializable {
	public static QuejaRefiereSoap toSoapModel(QuejaRefiere model) {
		QuejaRefiereSoap soapModel = new QuejaRefiereSoap();

		soapModel.setClaveReferencia(model.getClaveReferencia());
		soapModel.setClaveRelacion(model.getClaveRelacion());
		soapModel.setDescripcion(model.getDescripcion());
		soapModel.setOrdena(model.getOrdena());
		soapModel.setFlujoPuc(model.getFlujoPuc());
		soapModel.setCodSubProblema(model.getCodSubProblema());

		return soapModel;
	}

	public static QuejaRefiereSoap[] toSoapModels(QuejaRefiere[] models) {
		QuejaRefiereSoap[] soapModels = new QuejaRefiereSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static QuejaRefiereSoap[][] toSoapModels(QuejaRefiere[][] models) {
		QuejaRefiereSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new QuejaRefiereSoap[models.length][models[0].length];
		}
		else {
			soapModels = new QuejaRefiereSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static QuejaRefiereSoap[] toSoapModels(List<QuejaRefiere> models) {
		List<QuejaRefiereSoap> soapModels = new ArrayList<QuejaRefiereSoap>(models.size());

		for (QuejaRefiere model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new QuejaRefiereSoap[soapModels.size()]);
	}

	public QuejaRefiereSoap() {
	}

	public QuejaRefierePK getPrimaryKey() {
		return new QuejaRefierePK(_claveReferencia, _claveRelacion);
	}

	public void setPrimaryKey(QuejaRefierePK pk) {
		setClaveReferencia(pk.claveReferencia);
		setClaveRelacion(pk.claveRelacion);
	}

	public String getClaveReferencia() {
		return _claveReferencia;
	}

	public void setClaveReferencia(String claveReferencia) {
		_claveReferencia = claveReferencia;
	}

	public String getClaveRelacion() {
		return _claveRelacion;
	}

	public void setClaveRelacion(String claveRelacion) {
		_claveRelacion = claveRelacion;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	public String getOrdena() {
		return _ordena;
	}

	public void setOrdena(String ordena) {
		_ordena = ordena;
	}

	public String getFlujoPuc() {
		return _flujoPuc;
	}

	public void setFlujoPuc(String flujoPuc) {
		_flujoPuc = flujoPuc;
	}

	public String getCodSubProblema() {
		return _codSubProblema;
	}

	public void setCodSubProblema(String codSubProblema) {
		_codSubProblema = codSubProblema;
	}

	private String _claveReferencia;
	private String _claveRelacion;
	private String _descripcion;
	private String _ordena;
	private String _flujoPuc;
	private String _codSubProblema;
}