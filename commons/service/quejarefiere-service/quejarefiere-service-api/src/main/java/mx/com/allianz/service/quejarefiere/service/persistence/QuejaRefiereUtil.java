/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejarefiere.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.quejarefiere.model.QuejaRefiere;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the queja refiere service. This utility wraps {@link mx.com.allianz.service.quejarefiere.service.persistence.impl.QuejaRefierePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRefierePersistence
 * @see mx.com.allianz.service.quejarefiere.service.persistence.impl.QuejaRefierePersistenceImpl
 * @generated
 */
@ProviderType
public class QuejaRefiereUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(QuejaRefiere quejaRefiere) {
		getPersistence().clearCache(quejaRefiere);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<QuejaRefiere> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<QuejaRefiere> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<QuejaRefiere> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<QuejaRefiere> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static QuejaRefiere update(QuejaRefiere quejaRefiere) {
		return getPersistence().update(quejaRefiere);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static QuejaRefiere update(QuejaRefiere quejaRefiere,
		ServiceContext serviceContext) {
		return getPersistence().update(quejaRefiere, serviceContext);
	}

	/**
	* Returns all the queja refieres where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching queja refieres
	*/
	public static List<QuejaRefiere> findByfindByDescripcion(
		java.lang.String descripcion) {
		return getPersistence().findByfindByDescripcion(descripcion);
	}

	/**
	* Returns a range of all the queja refieres where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @return the range of matching queja refieres
	*/
	public static List<QuejaRefiere> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end) {
		return getPersistence().findByfindByDescripcion(descripcion, start, end);
	}

	/**
	* Returns an ordered range of all the queja refieres where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching queja refieres
	*/
	public static List<QuejaRefiere> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<QuejaRefiere> orderByComparator) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the queja refieres where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching queja refieres
	*/
	public static List<QuejaRefiere> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<QuejaRefiere> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja refiere
	* @throws NoSuchQuejaRefiereException if a matching queja refiere could not be found
	*/
	public static QuejaRefiere findByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator)
		throws mx.com.allianz.service.quejarefiere.exception.NoSuchQuejaRefiereException {
		return getPersistence()
				   .findByfindByDescripcion_First(descripcion, orderByComparator);
	}

	/**
	* Returns the first queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching queja refiere, or <code>null</code> if a matching queja refiere could not be found
	*/
	public static QuejaRefiere fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_First(descripcion,
			orderByComparator);
	}

	/**
	* Returns the last queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja refiere
	* @throws NoSuchQuejaRefiereException if a matching queja refiere could not be found
	*/
	public static QuejaRefiere findByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator)
		throws mx.com.allianz.service.quejarefiere.exception.NoSuchQuejaRefiereException {
		return getPersistence()
				   .findByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the last queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching queja refiere, or <code>null</code> if a matching queja refiere could not be found
	*/
	public static QuejaRefiere fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the queja refieres before and after the current queja refiere in the ordered set where descripcion = &#63;.
	*
	* @param quejaRefierePK the primary key of the current queja refiere
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next queja refiere
	* @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	*/
	public static QuejaRefiere[] findByfindByDescripcion_PrevAndNext(
		QuejaRefierePK quejaRefierePK, java.lang.String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator)
		throws mx.com.allianz.service.quejarefiere.exception.NoSuchQuejaRefiereException {
		return getPersistence()
				   .findByfindByDescripcion_PrevAndNext(quejaRefierePK,
			descripcion, orderByComparator);
	}

	/**
	* Removes all the queja refieres where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public static void removeByfindByDescripcion(java.lang.String descripcion) {
		getPersistence().removeByfindByDescripcion(descripcion);
	}

	/**
	* Returns the number of queja refieres where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching queja refieres
	*/
	public static int countByfindByDescripcion(java.lang.String descripcion) {
		return getPersistence().countByfindByDescripcion(descripcion);
	}

	/**
	* Caches the queja refiere in the entity cache if it is enabled.
	*
	* @param quejaRefiere the queja refiere
	*/
	public static void cacheResult(QuejaRefiere quejaRefiere) {
		getPersistence().cacheResult(quejaRefiere);
	}

	/**
	* Caches the queja refieres in the entity cache if it is enabled.
	*
	* @param quejaRefieres the queja refieres
	*/
	public static void cacheResult(List<QuejaRefiere> quejaRefieres) {
		getPersistence().cacheResult(quejaRefieres);
	}

	/**
	* Creates a new queja refiere with the primary key. Does not add the queja refiere to the database.
	*
	* @param quejaRefierePK the primary key for the new queja refiere
	* @return the new queja refiere
	*/
	public static QuejaRefiere create(QuejaRefierePK quejaRefierePK) {
		return getPersistence().create(quejaRefierePK);
	}

	/**
	* Removes the queja refiere with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param quejaRefierePK the primary key of the queja refiere
	* @return the queja refiere that was removed
	* @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	*/
	public static QuejaRefiere remove(QuejaRefierePK quejaRefierePK)
		throws mx.com.allianz.service.quejarefiere.exception.NoSuchQuejaRefiereException {
		return getPersistence().remove(quejaRefierePK);
	}

	public static QuejaRefiere updateImpl(QuejaRefiere quejaRefiere) {
		return getPersistence().updateImpl(quejaRefiere);
	}

	/**
	* Returns the queja refiere with the primary key or throws a {@link NoSuchQuejaRefiereException} if it could not be found.
	*
	* @param quejaRefierePK the primary key of the queja refiere
	* @return the queja refiere
	* @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	*/
	public static QuejaRefiere findByPrimaryKey(
		QuejaRefierePK quejaRefierePK)
		throws mx.com.allianz.service.quejarefiere.exception.NoSuchQuejaRefiereException {
		return getPersistence().findByPrimaryKey(quejaRefierePK);
	}

	/**
	* Returns the queja refiere with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param quejaRefierePK the primary key of the queja refiere
	* @return the queja refiere, or <code>null</code> if a queja refiere with the primary key could not be found
	*/
	public static QuejaRefiere fetchByPrimaryKey(
		QuejaRefierePK quejaRefierePK) {
		return getPersistence().fetchByPrimaryKey(quejaRefierePK);
	}

	public static java.util.Map<java.io.Serializable, QuejaRefiere> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the queja refieres.
	*
	* @return the queja refieres
	*/
	public static List<QuejaRefiere> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the queja refieres.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @return the range of queja refieres
	*/
	public static List<QuejaRefiere> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the queja refieres.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of queja refieres
	*/
	public static List<QuejaRefiere> findAll(int start, int end,
		OrderByComparator<QuejaRefiere> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the queja refieres.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of queja refieres
	* @param end the upper bound of the range of queja refieres (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of queja refieres
	*/
	public static List<QuejaRefiere> findAll(int start, int end,
		OrderByComparator<QuejaRefiere> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the queja refieres from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of queja refieres.
	*
	* @return the number of queja refieres
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static QuejaRefierePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<QuejaRefierePersistence, QuejaRefierePersistence> _serviceTracker =
		ServiceTrackerFactory.open(QuejaRefierePersistence.class);
}