/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejarefiere.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.quejarefiere.exception.NoSuchQuejaRefiereException;
import mx.com.allianz.service.quejarefiere.model.QuejaRefiere;
import mx.com.allianz.service.quejarefiere.model.impl.QuejaRefiereImpl;
import mx.com.allianz.service.quejarefiere.model.impl.QuejaRefiereModelImpl;
import mx.com.allianz.service.quejarefiere.service.persistence.QuejaRefierePK;
import mx.com.allianz.service.quejarefiere.service.persistence.QuejaRefierePersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the queja refiere service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRefierePersistence
 * @see mx.com.allianz.service.quejarefiere.service.persistence.QuejaRefiereUtil
 * @generated
 */
@ProviderType
public class QuejaRefierePersistenceImpl extends BasePersistenceImpl<QuejaRefiere>
	implements QuejaRefierePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link QuejaRefiereUtil} to access the queja refiere persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = QuejaRefiereImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRefiereModelImpl.FINDER_CACHE_ENABLED, QuejaRefiereImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRefiereModelImpl.FINDER_CACHE_ENABLED, QuejaRefiereImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRefiereModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRefiereModelImpl.FINDER_CACHE_ENABLED, QuejaRefiereImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByfindByDescripcion",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION =
		new FinderPath(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRefiereModelImpl.FINDER_CACHE_ENABLED, QuejaRefiereImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByDescripcion", new String[] { String.class.getName() },
			QuejaRefiereModelImpl.DESCRIPCION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION = new FinderPath(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRefiereModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByDescripcion", new String[] { String.class.getName() });

	/**
	 * Returns all the queja refieres where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the matching queja refieres
	 */
	@Override
	public List<QuejaRefiere> findByfindByDescripcion(String descripcion) {
		return findByfindByDescripcion(descripcion, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the queja refieres where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of queja refieres
	 * @param end the upper bound of the range of queja refieres (not inclusive)
	 * @return the range of matching queja refieres
	 */
	@Override
	public List<QuejaRefiere> findByfindByDescripcion(String descripcion,
		int start, int end) {
		return findByfindByDescripcion(descripcion, start, end, null);
	}

	/**
	 * Returns an ordered range of all the queja refieres where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of queja refieres
	 * @param end the upper bound of the range of queja refieres (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching queja refieres
	 */
	@Override
	public List<QuejaRefiere> findByfindByDescripcion(String descripcion,
		int start, int end, OrderByComparator<QuejaRefiere> orderByComparator) {
		return findByfindByDescripcion(descripcion, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the queja refieres where descripcion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of queja refieres
	 * @param end the upper bound of the range of queja refieres (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching queja refieres
	 */
	@Override
	public List<QuejaRefiere> findByfindByDescripcion(String descripcion,
		int start, int end, OrderByComparator<QuejaRefiere> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYDESCRIPCION;
			finderArgs = new Object[] { descripcion, start, end, orderByComparator };
		}

		List<QuejaRefiere> list = null;

		if (retrieveFromCache) {
			list = (List<QuejaRefiere>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (QuejaRefiere quejaRefiere : list) {
					if (!Objects.equals(descripcion,
								quejaRefiere.getDescripcion())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUEJAREFIERE_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuejaRefiereModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				if (!pagination) {
					list = (List<QuejaRefiere>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<QuejaRefiere>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first queja refiere in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching queja refiere
	 * @throws NoSuchQuejaRefiereException if a matching queja refiere could not be found
	 */
	@Override
	public QuejaRefiere findByfindByDescripcion_First(String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator)
		throws NoSuchQuejaRefiereException {
		QuejaRefiere quejaRefiere = fetchByfindByDescripcion_First(descripcion,
				orderByComparator);

		if (quejaRefiere != null) {
			return quejaRefiere;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuejaRefiereException(msg.toString());
	}

	/**
	 * Returns the first queja refiere in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching queja refiere, or <code>null</code> if a matching queja refiere could not be found
	 */
	@Override
	public QuejaRefiere fetchByfindByDescripcion_First(String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator) {
		List<QuejaRefiere> list = findByfindByDescripcion(descripcion, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last queja refiere in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching queja refiere
	 * @throws NoSuchQuejaRefiereException if a matching queja refiere could not be found
	 */
	@Override
	public QuejaRefiere findByfindByDescripcion_Last(String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator)
		throws NoSuchQuejaRefiereException {
		QuejaRefiere quejaRefiere = fetchByfindByDescripcion_Last(descripcion,
				orderByComparator);

		if (quejaRefiere != null) {
			return quejaRefiere;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuejaRefiereException(msg.toString());
	}

	/**
	 * Returns the last queja refiere in the ordered set where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching queja refiere, or <code>null</code> if a matching queja refiere could not be found
	 */
	@Override
	public QuejaRefiere fetchByfindByDescripcion_Last(String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator) {
		int count = countByfindByDescripcion(descripcion);

		if (count == 0) {
			return null;
		}

		List<QuejaRefiere> list = findByfindByDescripcion(descripcion,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the queja refieres before and after the current queja refiere in the ordered set where descripcion = &#63;.
	 *
	 * @param quejaRefierePK the primary key of the current queja refiere
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next queja refiere
	 * @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	 */
	@Override
	public QuejaRefiere[] findByfindByDescripcion_PrevAndNext(
		QuejaRefierePK quejaRefierePK, String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator)
		throws NoSuchQuejaRefiereException {
		QuejaRefiere quejaRefiere = findByPrimaryKey(quejaRefierePK);

		Session session = null;

		try {
			session = openSession();

			QuejaRefiere[] array = new QuejaRefiereImpl[3];

			array[0] = getByfindByDescripcion_PrevAndNext(session,
					quejaRefiere, descripcion, orderByComparator, true);

			array[1] = quejaRefiere;

			array[2] = getByfindByDescripcion_PrevAndNext(session,
					quejaRefiere, descripcion, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected QuejaRefiere getByfindByDescripcion_PrevAndNext(Session session,
		QuejaRefiere quejaRefiere, String descripcion,
		OrderByComparator<QuejaRefiere> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_QUEJAREFIERE_WHERE);

		boolean bindDescripcion = false;

		if (descripcion == null) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
		}
		else if (descripcion.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
		}
		else {
			bindDescripcion = true;

			query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(QuejaRefiereModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDescripcion) {
			qPos.add(descripcion);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(quejaRefiere);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<QuejaRefiere> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the queja refieres where descripcion = &#63; from the database.
	 *
	 * @param descripcion the descripcion
	 */
	@Override
	public void removeByfindByDescripcion(String descripcion) {
		for (QuejaRefiere quejaRefiere : findByfindByDescripcion(descripcion,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(quejaRefiere);
		}
	}

	/**
	 * Returns the number of queja refieres where descripcion = &#63;.
	 *
	 * @param descripcion the descripcion
	 * @return the number of matching queja refieres
	 */
	@Override
	public int countByfindByDescripcion(String descripcion) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION;

		Object[] finderArgs = new Object[] { descripcion };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUEJAREFIERE_WHERE);

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_1 = "quejaRefiere.descripcion IS NULL";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_2 = "quejaRefiere.descripcion = ?";
	private static final String _FINDER_COLUMN_FINDBYDESCRIPCION_DESCRIPCION_3 = "(quejaRefiere.descripcion IS NULL OR quejaRefiere.descripcion = '')";

	public QuejaRefierePersistenceImpl() {
		setModelClass(QuejaRefiere.class);
	}

	/**
	 * Caches the queja refiere in the entity cache if it is enabled.
	 *
	 * @param quejaRefiere the queja refiere
	 */
	@Override
	public void cacheResult(QuejaRefiere quejaRefiere) {
		entityCache.putResult(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRefiereImpl.class, quejaRefiere.getPrimaryKey(), quejaRefiere);

		quejaRefiere.resetOriginalValues();
	}

	/**
	 * Caches the queja refieres in the entity cache if it is enabled.
	 *
	 * @param quejaRefieres the queja refieres
	 */
	@Override
	public void cacheResult(List<QuejaRefiere> quejaRefieres) {
		for (QuejaRefiere quejaRefiere : quejaRefieres) {
			if (entityCache.getResult(
						QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
						QuejaRefiereImpl.class, quejaRefiere.getPrimaryKey()) == null) {
				cacheResult(quejaRefiere);
			}
			else {
				quejaRefiere.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all queja refieres.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(QuejaRefiereImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the queja refiere.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(QuejaRefiere quejaRefiere) {
		entityCache.removeResult(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRefiereImpl.class, quejaRefiere.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<QuejaRefiere> quejaRefieres) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (QuejaRefiere quejaRefiere : quejaRefieres) {
			entityCache.removeResult(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
				QuejaRefiereImpl.class, quejaRefiere.getPrimaryKey());
		}
	}

	/**
	 * Creates a new queja refiere with the primary key. Does not add the queja refiere to the database.
	 *
	 * @param quejaRefierePK the primary key for the new queja refiere
	 * @return the new queja refiere
	 */
	@Override
	public QuejaRefiere create(QuejaRefierePK quejaRefierePK) {
		QuejaRefiere quejaRefiere = new QuejaRefiereImpl();

		quejaRefiere.setNew(true);
		quejaRefiere.setPrimaryKey(quejaRefierePK);

		return quejaRefiere;
	}

	/**
	 * Removes the queja refiere with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param quejaRefierePK the primary key of the queja refiere
	 * @return the queja refiere that was removed
	 * @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	 */
	@Override
	public QuejaRefiere remove(QuejaRefierePK quejaRefierePK)
		throws NoSuchQuejaRefiereException {
		return remove((Serializable)quejaRefierePK);
	}

	/**
	 * Removes the queja refiere with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the queja refiere
	 * @return the queja refiere that was removed
	 * @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	 */
	@Override
	public QuejaRefiere remove(Serializable primaryKey)
		throws NoSuchQuejaRefiereException {
		Session session = null;

		try {
			session = openSession();

			QuejaRefiere quejaRefiere = (QuejaRefiere)session.get(QuejaRefiereImpl.class,
					primaryKey);

			if (quejaRefiere == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchQuejaRefiereException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(quejaRefiere);
		}
		catch (NoSuchQuejaRefiereException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected QuejaRefiere removeImpl(QuejaRefiere quejaRefiere) {
		quejaRefiere = toUnwrappedModel(quejaRefiere);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(quejaRefiere)) {
				quejaRefiere = (QuejaRefiere)session.get(QuejaRefiereImpl.class,
						quejaRefiere.getPrimaryKeyObj());
			}

			if (quejaRefiere != null) {
				session.delete(quejaRefiere);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (quejaRefiere != null) {
			clearCache(quejaRefiere);
		}

		return quejaRefiere;
	}

	@Override
	public QuejaRefiere updateImpl(QuejaRefiere quejaRefiere) {
		quejaRefiere = toUnwrappedModel(quejaRefiere);

		boolean isNew = quejaRefiere.isNew();

		QuejaRefiereModelImpl quejaRefiereModelImpl = (QuejaRefiereModelImpl)quejaRefiere;

		Session session = null;

		try {
			session = openSession();

			if (quejaRefiere.isNew()) {
				session.save(quejaRefiere);

				quejaRefiere.setNew(false);
			}
			else {
				quejaRefiere = (QuejaRefiere)session.merge(quejaRefiere);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !QuejaRefiereModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((quejaRefiereModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						quejaRefiereModelImpl.getOriginalDescripcion()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);

				args = new Object[] { quejaRefiereModelImpl.getDescripcion() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYDESCRIPCION,
					args);
			}
		}

		entityCache.putResult(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
			QuejaRefiereImpl.class, quejaRefiere.getPrimaryKey(), quejaRefiere,
			false);

		quejaRefiere.resetOriginalValues();

		return quejaRefiere;
	}

	protected QuejaRefiere toUnwrappedModel(QuejaRefiere quejaRefiere) {
		if (quejaRefiere instanceof QuejaRefiereImpl) {
			return quejaRefiere;
		}

		QuejaRefiereImpl quejaRefiereImpl = new QuejaRefiereImpl();

		quejaRefiereImpl.setNew(quejaRefiere.isNew());
		quejaRefiereImpl.setPrimaryKey(quejaRefiere.getPrimaryKey());

		quejaRefiereImpl.setClaveReferencia(quejaRefiere.getClaveReferencia());
		quejaRefiereImpl.setClaveRelacion(quejaRefiere.getClaveRelacion());
		quejaRefiereImpl.setDescripcion(quejaRefiere.getDescripcion());
		quejaRefiereImpl.setOrdena(quejaRefiere.getOrdena());
		quejaRefiereImpl.setFlujoPuc(quejaRefiere.getFlujoPuc());
		quejaRefiereImpl.setCodSubProblema(quejaRefiere.getCodSubProblema());

		return quejaRefiereImpl;
	}

	/**
	 * Returns the queja refiere with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the queja refiere
	 * @return the queja refiere
	 * @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	 */
	@Override
	public QuejaRefiere findByPrimaryKey(Serializable primaryKey)
		throws NoSuchQuejaRefiereException {
		QuejaRefiere quejaRefiere = fetchByPrimaryKey(primaryKey);

		if (quejaRefiere == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchQuejaRefiereException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return quejaRefiere;
	}

	/**
	 * Returns the queja refiere with the primary key or throws a {@link NoSuchQuejaRefiereException} if it could not be found.
	 *
	 * @param quejaRefierePK the primary key of the queja refiere
	 * @return the queja refiere
	 * @throws NoSuchQuejaRefiereException if a queja refiere with the primary key could not be found
	 */
	@Override
	public QuejaRefiere findByPrimaryKey(QuejaRefierePK quejaRefierePK)
		throws NoSuchQuejaRefiereException {
		return findByPrimaryKey((Serializable)quejaRefierePK);
	}

	/**
	 * Returns the queja refiere with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the queja refiere
	 * @return the queja refiere, or <code>null</code> if a queja refiere with the primary key could not be found
	 */
	@Override
	public QuejaRefiere fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
				QuejaRefiereImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		QuejaRefiere quejaRefiere = (QuejaRefiere)serializable;

		if (quejaRefiere == null) {
			Session session = null;

			try {
				session = openSession();

				quejaRefiere = (QuejaRefiere)session.get(QuejaRefiereImpl.class,
						primaryKey);

				if (quejaRefiere != null) {
					cacheResult(quejaRefiere);
				}
				else {
					entityCache.putResult(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
						QuejaRefiereImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(QuejaRefiereModelImpl.ENTITY_CACHE_ENABLED,
					QuejaRefiereImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return quejaRefiere;
	}

	/**
	 * Returns the queja refiere with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param quejaRefierePK the primary key of the queja refiere
	 * @return the queja refiere, or <code>null</code> if a queja refiere with the primary key could not be found
	 */
	@Override
	public QuejaRefiere fetchByPrimaryKey(QuejaRefierePK quejaRefierePK) {
		return fetchByPrimaryKey((Serializable)quejaRefierePK);
	}

	@Override
	public Map<Serializable, QuejaRefiere> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, QuejaRefiere> map = new HashMap<Serializable, QuejaRefiere>();

		for (Serializable primaryKey : primaryKeys) {
			QuejaRefiere quejaRefiere = fetchByPrimaryKey(primaryKey);

			if (quejaRefiere != null) {
				map.put(primaryKey, quejaRefiere);
			}
		}

		return map;
	}

	/**
	 * Returns all the queja refieres.
	 *
	 * @return the queja refieres
	 */
	@Override
	public List<QuejaRefiere> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the queja refieres.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of queja refieres
	 * @param end the upper bound of the range of queja refieres (not inclusive)
	 * @return the range of queja refieres
	 */
	@Override
	public List<QuejaRefiere> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the queja refieres.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of queja refieres
	 * @param end the upper bound of the range of queja refieres (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of queja refieres
	 */
	@Override
	public List<QuejaRefiere> findAll(int start, int end,
		OrderByComparator<QuejaRefiere> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the queja refieres.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuejaRefiereModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of queja refieres
	 * @param end the upper bound of the range of queja refieres (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of queja refieres
	 */
	@Override
	public List<QuejaRefiere> findAll(int start, int end,
		OrderByComparator<QuejaRefiere> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<QuejaRefiere> list = null;

		if (retrieveFromCache) {
			list = (List<QuejaRefiere>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_QUEJAREFIERE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_QUEJAREFIERE;

				if (pagination) {
					sql = sql.concat(QuejaRefiereModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<QuejaRefiere>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<QuejaRefiere>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the queja refieres from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (QuejaRefiere quejaRefiere : findAll()) {
			remove(quejaRefiere);
		}
	}

	/**
	 * Returns the number of queja refieres.
	 *
	 * @return the number of queja refieres
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_QUEJAREFIERE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return QuejaRefiereModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the queja refiere persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(QuejaRefiereImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_QUEJAREFIERE = "SELECT quejaRefiere FROM QuejaRefiere quejaRefiere";
	private static final String _SQL_SELECT_QUEJAREFIERE_WHERE = "SELECT quejaRefiere FROM QuejaRefiere quejaRefiere WHERE ";
	private static final String _SQL_COUNT_QUEJAREFIERE = "SELECT COUNT(quejaRefiere) FROM QuejaRefiere quejaRefiere";
	private static final String _SQL_COUNT_QUEJAREFIERE_WHERE = "SELECT COUNT(quejaRefiere) FROM QuejaRefiere quejaRefiere WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "quejaRefiere.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No QuejaRefiere exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No QuejaRefiere exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(QuejaRefierePersistenceImpl.class);
}