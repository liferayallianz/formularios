/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.quejarefiere.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.quejarefiere.model.QuejaRefiere;
import mx.com.allianz.service.quejarefiere.service.persistence.QuejaRefierePK;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing QuejaRefiere in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see QuejaRefiere
 * @generated
 */
@ProviderType
public class QuejaRefiereCacheModel implements CacheModel<QuejaRefiere>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuejaRefiereCacheModel)) {
			return false;
		}

		QuejaRefiereCacheModel quejaRefiereCacheModel = (QuejaRefiereCacheModel)obj;

		if (quejaRefierePK.equals(quejaRefiereCacheModel.quejaRefierePK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, quejaRefierePK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{claveReferencia=");
		sb.append(claveReferencia);
		sb.append(", claveRelacion=");
		sb.append(claveRelacion);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append(", ordena=");
		sb.append(ordena);
		sb.append(", flujoPuc=");
		sb.append(flujoPuc);
		sb.append(", codSubProblema=");
		sb.append(codSubProblema);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public QuejaRefiere toEntityModel() {
		QuejaRefiereImpl quejaRefiereImpl = new QuejaRefiereImpl();

		if (claveReferencia == null) {
			quejaRefiereImpl.setClaveReferencia(StringPool.BLANK);
		}
		else {
			quejaRefiereImpl.setClaveReferencia(claveReferencia);
		}

		if (claveRelacion == null) {
			quejaRefiereImpl.setClaveRelacion(StringPool.BLANK);
		}
		else {
			quejaRefiereImpl.setClaveRelacion(claveRelacion);
		}

		if (descripcion == null) {
			quejaRefiereImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			quejaRefiereImpl.setDescripcion(descripcion);
		}

		if (ordena == null) {
			quejaRefiereImpl.setOrdena(StringPool.BLANK);
		}
		else {
			quejaRefiereImpl.setOrdena(ordena);
		}

		if (flujoPuc == null) {
			quejaRefiereImpl.setFlujoPuc(StringPool.BLANK);
		}
		else {
			quejaRefiereImpl.setFlujoPuc(flujoPuc);
		}

		if (codSubProblema == null) {
			quejaRefiereImpl.setCodSubProblema(StringPool.BLANK);
		}
		else {
			quejaRefiereImpl.setCodSubProblema(codSubProblema);
		}

		quejaRefiereImpl.resetOriginalValues();

		return quejaRefiereImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		claveReferencia = objectInput.readUTF();
		claveRelacion = objectInput.readUTF();
		descripcion = objectInput.readUTF();
		ordena = objectInput.readUTF();
		flujoPuc = objectInput.readUTF();
		codSubProblema = objectInput.readUTF();

		quejaRefierePK = new QuejaRefierePK(claveReferencia, claveRelacion);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (claveReferencia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(claveReferencia);
		}

		if (claveRelacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(claveRelacion);
		}

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}

		if (ordena == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ordena);
		}

		if (flujoPuc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flujoPuc);
		}

		if (codSubProblema == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codSubProblema);
		}
	}

	public String claveReferencia;
	public String claveRelacion;
	public String descripcion;
	public String ordena;
	public String flujoPuc;
	public String codSubProblema;
	public transient QuejaRefierePK quejaRefierePK;
}