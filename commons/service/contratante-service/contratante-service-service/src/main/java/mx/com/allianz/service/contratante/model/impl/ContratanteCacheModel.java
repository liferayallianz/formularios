/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.contratante.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.contratante.model.Contratante;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Contratante in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Contratante
 * @generated
 */
@ProviderType
public class ContratanteCacheModel implements CacheModel<Contratante>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContratanteCacheModel)) {
			return false;
		}

		ContratanteCacheModel contratanteCacheModel = (ContratanteCacheModel)obj;

		if (id == contratanteCacheModel.id) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, id);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{id=");
		sb.append(id);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", apellidoPaterno=");
		sb.append(apellidoPaterno);
		sb.append(", apellidoMaterno=");
		sb.append(apellidoMaterno);
		sb.append(", numeroPoliza=");
		sb.append(numeroPoliza);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Contratante toEntityModel() {
		ContratanteImpl contratanteImpl = new ContratanteImpl();

		contratanteImpl.setId(id);

		if (nombre == null) {
			contratanteImpl.setNombre(StringPool.BLANK);
		}
		else {
			contratanteImpl.setNombre(nombre);
		}

		if (apellidoPaterno == null) {
			contratanteImpl.setApellidoPaterno(StringPool.BLANK);
		}
		else {
			contratanteImpl.setApellidoPaterno(apellidoPaterno);
		}

		if (apellidoMaterno == null) {
			contratanteImpl.setApellidoMaterno(StringPool.BLANK);
		}
		else {
			contratanteImpl.setApellidoMaterno(apellidoMaterno);
		}

		if (numeroPoliza == null) {
			contratanteImpl.setNumeroPoliza(StringPool.BLANK);
		}
		else {
			contratanteImpl.setNumeroPoliza(numeroPoliza);
		}

		contratanteImpl.resetOriginalValues();

		return contratanteImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		nombre = objectInput.readUTF();
		apellidoPaterno = objectInput.readUTF();
		apellidoMaterno = objectInput.readUTF();
		numeroPoliza = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (apellidoPaterno == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidoPaterno);
		}

		if (apellidoMaterno == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidoMaterno);
		}

		if (numeroPoliza == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(numeroPoliza);
		}
	}

	public long id;
	public String nombre;
	public String apellidoPaterno;
	public String apellidoMaterno;
	public String numeroPoliza;
}