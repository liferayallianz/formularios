/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.contratante.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.contratante.exception.NoSuchContratanteException;
import mx.com.allianz.service.contratante.model.Contratante;
import mx.com.allianz.service.contratante.model.impl.ContratanteImpl;
import mx.com.allianz.service.contratante.model.impl.ContratanteModelImpl;
import mx.com.allianz.service.contratante.service.persistence.ContratantePersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the contratante service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ContratantePersistence
 * @see mx.com.allianz.service.contratante.service.persistence.ContratanteUtil
 * @generated
 */
@ProviderType
public class ContratantePersistenceImpl extends BasePersistenceImpl<Contratante>
	implements ContratantePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ContratanteUtil} to access the contratante persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ContratanteImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
			ContratanteModelImpl.FINDER_CACHE_ENABLED, ContratanteImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
			ContratanteModelImpl.FINDER_CACHE_ENABLED, ContratanteImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
			ContratanteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMBRE = new FinderPath(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
			ContratanteModelImpl.FINDER_CACHE_ENABLED, ContratanteImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBynombre",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE =
		new FinderPath(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
			ContratanteModelImpl.FINDER_CACHE_ENABLED, ContratanteImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBynombre",
			new String[] { String.class.getName() },
			ContratanteModelImpl.NOMBRE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NOMBRE = new FinderPath(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
			ContratanteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBynombre",
			new String[] { String.class.getName() });

	/**
	 * Returns all the contratantes where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the matching contratantes
	 */
	@Override
	public List<Contratante> findBynombre(String nombre) {
		return findBynombre(nombre, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the contratantes where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of contratantes
	 * @param end the upper bound of the range of contratantes (not inclusive)
	 * @return the range of matching contratantes
	 */
	@Override
	public List<Contratante> findBynombre(String nombre, int start, int end) {
		return findBynombre(nombre, start, end, null);
	}

	/**
	 * Returns an ordered range of all the contratantes where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of contratantes
	 * @param end the upper bound of the range of contratantes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching contratantes
	 */
	@Override
	public List<Contratante> findBynombre(String nombre, int start, int end,
		OrderByComparator<Contratante> orderByComparator) {
		return findBynombre(nombre, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the contratantes where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of contratantes
	 * @param end the upper bound of the range of contratantes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching contratantes
	 */
	@Override
	public List<Contratante> findBynombre(String nombre, int start, int end,
		OrderByComparator<Contratante> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE;
			finderArgs = new Object[] { nombre };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMBRE;
			finderArgs = new Object[] { nombre, start, end, orderByComparator };
		}

		List<Contratante> list = null;

		if (retrieveFromCache) {
			list = (List<Contratante>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Contratante contratante : list) {
					if (!Objects.equals(nombre, contratante.getNombre())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CONTRATANTE_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ContratanteModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				if (!pagination) {
					list = (List<Contratante>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Contratante>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first contratante in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contratante
	 * @throws NoSuchContratanteException if a matching contratante could not be found
	 */
	@Override
	public Contratante findBynombre_First(String nombre,
		OrderByComparator<Contratante> orderByComparator)
		throws NoSuchContratanteException {
		Contratante contratante = fetchBynombre_First(nombre, orderByComparator);

		if (contratante != null) {
			return contratante;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchContratanteException(msg.toString());
	}

	/**
	 * Returns the first contratante in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contratante, or <code>null</code> if a matching contratante could not be found
	 */
	@Override
	public Contratante fetchBynombre_First(String nombre,
		OrderByComparator<Contratante> orderByComparator) {
		List<Contratante> list = findBynombre(nombre, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last contratante in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contratante
	 * @throws NoSuchContratanteException if a matching contratante could not be found
	 */
	@Override
	public Contratante findBynombre_Last(String nombre,
		OrderByComparator<Contratante> orderByComparator)
		throws NoSuchContratanteException {
		Contratante contratante = fetchBynombre_Last(nombre, orderByComparator);

		if (contratante != null) {
			return contratante;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchContratanteException(msg.toString());
	}

	/**
	 * Returns the last contratante in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contratante, or <code>null</code> if a matching contratante could not be found
	 */
	@Override
	public Contratante fetchBynombre_Last(String nombre,
		OrderByComparator<Contratante> orderByComparator) {
		int count = countBynombre(nombre);

		if (count == 0) {
			return null;
		}

		List<Contratante> list = findBynombre(nombre, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the contratantes before and after the current contratante in the ordered set where nombre = &#63;.
	 *
	 * @param id the primary key of the current contratante
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next contratante
	 * @throws NoSuchContratanteException if a contratante with the primary key could not be found
	 */
	@Override
	public Contratante[] findBynombre_PrevAndNext(long id, String nombre,
		OrderByComparator<Contratante> orderByComparator)
		throws NoSuchContratanteException {
		Contratante contratante = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			Contratante[] array = new ContratanteImpl[3];

			array[0] = getBynombre_PrevAndNext(session, contratante, nombre,
					orderByComparator, true);

			array[1] = contratante;

			array[2] = getBynombre_PrevAndNext(session, contratante, nombre,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Contratante getBynombre_PrevAndNext(Session session,
		Contratante contratante, String nombre,
		OrderByComparator<Contratante> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CONTRATANTE_WHERE);

		boolean bindNombre = false;

		if (nombre == null) {
			query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_1);
		}
		else if (nombre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
		}
		else {
			bindNombre = true;

			query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ContratanteModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindNombre) {
			qPos.add(nombre);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(contratante);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Contratante> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the contratantes where nombre = &#63; from the database.
	 *
	 * @param nombre the nombre
	 */
	@Override
	public void removeBynombre(String nombre) {
		for (Contratante contratante : findBynombre(nombre, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(contratante);
		}
	}

	/**
	 * Returns the number of contratantes where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching contratantes
	 */
	@Override
	public int countBynombre(String nombre) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NOMBRE;

		Object[] finderArgs = new Object[] { nombre };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CONTRATANTE_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_1 = "contratante.nombre IS NULL";
	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_2 = "contratante.nombre = ?";
	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_3 = "(contratante.nombre IS NULL OR contratante.nombre = '')";

	public ContratantePersistenceImpl() {
		setModelClass(Contratante.class);
	}

	/**
	 * Caches the contratante in the entity cache if it is enabled.
	 *
	 * @param contratante the contratante
	 */
	@Override
	public void cacheResult(Contratante contratante) {
		entityCache.putResult(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
			ContratanteImpl.class, contratante.getPrimaryKey(), contratante);

		contratante.resetOriginalValues();
	}

	/**
	 * Caches the contratantes in the entity cache if it is enabled.
	 *
	 * @param contratantes the contratantes
	 */
	@Override
	public void cacheResult(List<Contratante> contratantes) {
		for (Contratante contratante : contratantes) {
			if (entityCache.getResult(
						ContratanteModelImpl.ENTITY_CACHE_ENABLED,
						ContratanteImpl.class, contratante.getPrimaryKey()) == null) {
				cacheResult(contratante);
			}
			else {
				contratante.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all contratantes.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ContratanteImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the contratante.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Contratante contratante) {
		entityCache.removeResult(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
			ContratanteImpl.class, contratante.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Contratante> contratantes) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Contratante contratante : contratantes) {
			entityCache.removeResult(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
				ContratanteImpl.class, contratante.getPrimaryKey());
		}
	}

	/**
	 * Creates a new contratante with the primary key. Does not add the contratante to the database.
	 *
	 * @param id the primary key for the new contratante
	 * @return the new contratante
	 */
	@Override
	public Contratante create(long id) {
		Contratante contratante = new ContratanteImpl();

		contratante.setNew(true);
		contratante.setPrimaryKey(id);

		return contratante;
	}

	/**
	 * Removes the contratante with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the contratante
	 * @return the contratante that was removed
	 * @throws NoSuchContratanteException if a contratante with the primary key could not be found
	 */
	@Override
	public Contratante remove(long id) throws NoSuchContratanteException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the contratante with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the contratante
	 * @return the contratante that was removed
	 * @throws NoSuchContratanteException if a contratante with the primary key could not be found
	 */
	@Override
	public Contratante remove(Serializable primaryKey)
		throws NoSuchContratanteException {
		Session session = null;

		try {
			session = openSession();

			Contratante contratante = (Contratante)session.get(ContratanteImpl.class,
					primaryKey);

			if (contratante == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchContratanteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(contratante);
		}
		catch (NoSuchContratanteException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Contratante removeImpl(Contratante contratante) {
		contratante = toUnwrappedModel(contratante);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(contratante)) {
				contratante = (Contratante)session.get(ContratanteImpl.class,
						contratante.getPrimaryKeyObj());
			}

			if (contratante != null) {
				session.delete(contratante);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (contratante != null) {
			clearCache(contratante);
		}

		return contratante;
	}

	@Override
	public Contratante updateImpl(Contratante contratante) {
		contratante = toUnwrappedModel(contratante);

		boolean isNew = contratante.isNew();

		ContratanteModelImpl contratanteModelImpl = (ContratanteModelImpl)contratante;

		Session session = null;

		try {
			session = openSession();

			if (contratante.isNew()) {
				session.save(contratante);

				contratante.setNew(false);
			}
			else {
				contratante = (Contratante)session.merge(contratante);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ContratanteModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((contratanteModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						contratanteModelImpl.getOriginalNombre()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBRE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE,
					args);

				args = new Object[] { contratanteModelImpl.getNombre() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBRE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBRE,
					args);
			}
		}

		entityCache.putResult(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
			ContratanteImpl.class, contratante.getPrimaryKey(), contratante,
			false);

		contratante.resetOriginalValues();

		return contratante;
	}

	protected Contratante toUnwrappedModel(Contratante contratante) {
		if (contratante instanceof ContratanteImpl) {
			return contratante;
		}

		ContratanteImpl contratanteImpl = new ContratanteImpl();

		contratanteImpl.setNew(contratante.isNew());
		contratanteImpl.setPrimaryKey(contratante.getPrimaryKey());

		contratanteImpl.setId(contratante.getId());
		contratanteImpl.setNombre(contratante.getNombre());
		contratanteImpl.setApellidoPaterno(contratante.getApellidoPaterno());
		contratanteImpl.setApellidoMaterno(contratante.getApellidoMaterno());
		contratanteImpl.setNumeroPoliza(contratante.getNumeroPoliza());

		return contratanteImpl;
	}

	/**
	 * Returns the contratante with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the contratante
	 * @return the contratante
	 * @throws NoSuchContratanteException if a contratante with the primary key could not be found
	 */
	@Override
	public Contratante findByPrimaryKey(Serializable primaryKey)
		throws NoSuchContratanteException {
		Contratante contratante = fetchByPrimaryKey(primaryKey);

		if (contratante == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchContratanteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return contratante;
	}

	/**
	 * Returns the contratante with the primary key or throws a {@link NoSuchContratanteException} if it could not be found.
	 *
	 * @param id the primary key of the contratante
	 * @return the contratante
	 * @throws NoSuchContratanteException if a contratante with the primary key could not be found
	 */
	@Override
	public Contratante findByPrimaryKey(long id)
		throws NoSuchContratanteException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the contratante with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the contratante
	 * @return the contratante, or <code>null</code> if a contratante with the primary key could not be found
	 */
	@Override
	public Contratante fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
				ContratanteImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Contratante contratante = (Contratante)serializable;

		if (contratante == null) {
			Session session = null;

			try {
				session = openSession();

				contratante = (Contratante)session.get(ContratanteImpl.class,
						primaryKey);

				if (contratante != null) {
					cacheResult(contratante);
				}
				else {
					entityCache.putResult(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
						ContratanteImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
					ContratanteImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return contratante;
	}

	/**
	 * Returns the contratante with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the contratante
	 * @return the contratante, or <code>null</code> if a contratante with the primary key could not be found
	 */
	@Override
	public Contratante fetchByPrimaryKey(long id) {
		return fetchByPrimaryKey((Serializable)id);
	}

	@Override
	public Map<Serializable, Contratante> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Contratante> map = new HashMap<Serializable, Contratante>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Contratante contratante = fetchByPrimaryKey(primaryKey);

			if (contratante != null) {
				map.put(primaryKey, contratante);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
					ContratanteImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Contratante)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_CONTRATANTE_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Contratante contratante : (List<Contratante>)q.list()) {
				map.put(contratante.getPrimaryKeyObj(), contratante);

				cacheResult(contratante);

				uncachedPrimaryKeys.remove(contratante.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ContratanteModelImpl.ENTITY_CACHE_ENABLED,
					ContratanteImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the contratantes.
	 *
	 * @return the contratantes
	 */
	@Override
	public List<Contratante> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the contratantes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contratantes
	 * @param end the upper bound of the range of contratantes (not inclusive)
	 * @return the range of contratantes
	 */
	@Override
	public List<Contratante> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the contratantes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contratantes
	 * @param end the upper bound of the range of contratantes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of contratantes
	 */
	@Override
	public List<Contratante> findAll(int start, int end,
		OrderByComparator<Contratante> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the contratantes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contratantes
	 * @param end the upper bound of the range of contratantes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of contratantes
	 */
	@Override
	public List<Contratante> findAll(int start, int end,
		OrderByComparator<Contratante> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Contratante> list = null;

		if (retrieveFromCache) {
			list = (List<Contratante>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CONTRATANTE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CONTRATANTE;

				if (pagination) {
					sql = sql.concat(ContratanteModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Contratante>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Contratante>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the contratantes from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Contratante contratante : findAll()) {
			remove(contratante);
		}
	}

	/**
	 * Returns the number of contratantes.
	 *
	 * @return the number of contratantes
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CONTRATANTE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ContratanteModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the contratante persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ContratanteImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_CONTRATANTE = "SELECT contratante FROM Contratante contratante";
	private static final String _SQL_SELECT_CONTRATANTE_WHERE_PKS_IN = "SELECT contratante FROM Contratante contratante WHERE id_ IN (";
	private static final String _SQL_SELECT_CONTRATANTE_WHERE = "SELECT contratante FROM Contratante contratante WHERE ";
	private static final String _SQL_COUNT_CONTRATANTE = "SELECT COUNT(contratante) FROM Contratante contratante";
	private static final String _SQL_COUNT_CONTRATANTE_WHERE = "SELECT COUNT(contratante) FROM Contratante contratante WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "contratante.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Contratante exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Contratante exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ContratantePersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
}