package mx.com.allianz.service.contratante.rest;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.ContratanteDTO;
import mx.com.allianz.service.contratante.service.ContratanteLocalServiceUtil;

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.contratante")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)

public class ContratanteServiceRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todas las nacionalidades del
	 * catalogo.
	 * 
	 * @return getNacionalidades()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getContratantes")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	
	public List<ContratanteDTO> getContratante(){
		return getAllContratante();
	}
	
	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los hospitales del
	 * catalogo.
	 * 
	 * @return getAllCAT_Hospital().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findContratantes")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ContratanteDTO> findContratante(
			
			@DefaultValue("-1") @QueryParam("id") long id,
			@DefaultValue("") @QueryParam("nombre") String nombre,
			@DefaultValue("") @QueryParam("apellidoPaterno") String apellidoPaterno,
			@DefaultValue("") @QueryParam("apellidoMaterno") String apellidoMaterno,
			@DefaultValue("") @QueryParam("numeroPoliza") String numeroPoliza){

		return getAllContratante().stream()
				.filter((contratante) -> id == -1 || contratante.getId() == id)                             
				.filter((contratante) -> nombre.isEmpty() || contratante.getNombre().contains(nombre))
				.filter((contratante) -> apellidoPaterno.isEmpty() || contratante.getApellidoPaterno().contains(apellidoPaterno))
				.filter((contratante) -> apellidoMaterno.isEmpty() || contratante.getApellidoMaterno().contains(apellidoMaterno))
				.filter((contratante) -> numeroPoliza.isEmpty() || contratante.getApellidoMaterno().contains(numeroPoliza))
				.collect(Collectors.toList());		
	}
	
	private List<ContratanteDTO> getAllContratante() {
		return ContratanteLocalServiceUtil.getContratantes(-1, -1).stream()
				.map(contratante -> new ContratanteDTO(contratante.getId(),
						contratante.getNombre(),contratante.getApellidoPaterno(),
						contratante.getApellidoMaterno(),contratante.getNumeroPoliza()))
				.collect(Collectors.toList());
	}
	
}