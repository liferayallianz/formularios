/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.contratante.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.contratante.exception.NoSuchContratanteException;
import mx.com.allianz.service.contratante.model.Contratante;

/**
 * The persistence interface for the contratante service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.contratante.service.persistence.impl.ContratantePersistenceImpl
 * @see ContratanteUtil
 * @generated
 */
@ProviderType
public interface ContratantePersistence extends BasePersistence<Contratante> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ContratanteUtil} to access the contratante persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the contratantes where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching contratantes
	*/
	public java.util.List<Contratante> findBynombre(java.lang.String nombre);

	/**
	* Returns a range of all the contratantes where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @return the range of matching contratantes
	*/
	public java.util.List<Contratante> findBynombre(java.lang.String nombre,
		int start, int end);

	/**
	* Returns an ordered range of all the contratantes where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching contratantes
	*/
	public java.util.List<Contratante> findBynombre(java.lang.String nombre,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Contratante> orderByComparator);

	/**
	* Returns an ordered range of all the contratantes where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching contratantes
	*/
	public java.util.List<Contratante> findBynombre(java.lang.String nombre,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Contratante> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first contratante in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contratante
	* @throws NoSuchContratanteException if a matching contratante could not be found
	*/
	public Contratante findBynombre_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Contratante> orderByComparator)
		throws NoSuchContratanteException;

	/**
	* Returns the first contratante in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contratante, or <code>null</code> if a matching contratante could not be found
	*/
	public Contratante fetchBynombre_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Contratante> orderByComparator);

	/**
	* Returns the last contratante in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contratante
	* @throws NoSuchContratanteException if a matching contratante could not be found
	*/
	public Contratante findBynombre_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Contratante> orderByComparator)
		throws NoSuchContratanteException;

	/**
	* Returns the last contratante in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contratante, or <code>null</code> if a matching contratante could not be found
	*/
	public Contratante fetchBynombre_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Contratante> orderByComparator);

	/**
	* Returns the contratantes before and after the current contratante in the ordered set where nombre = &#63;.
	*
	* @param id the primary key of the current contratante
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next contratante
	* @throws NoSuchContratanteException if a contratante with the primary key could not be found
	*/
	public Contratante[] findBynombre_PrevAndNext(long id,
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Contratante> orderByComparator)
		throws NoSuchContratanteException;

	/**
	* Removes all the contratantes where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public void removeBynombre(java.lang.String nombre);

	/**
	* Returns the number of contratantes where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching contratantes
	*/
	public int countBynombre(java.lang.String nombre);

	/**
	* Caches the contratante in the entity cache if it is enabled.
	*
	* @param contratante the contratante
	*/
	public void cacheResult(Contratante contratante);

	/**
	* Caches the contratantes in the entity cache if it is enabled.
	*
	* @param contratantes the contratantes
	*/
	public void cacheResult(java.util.List<Contratante> contratantes);

	/**
	* Creates a new contratante with the primary key. Does not add the contratante to the database.
	*
	* @param id the primary key for the new contratante
	* @return the new contratante
	*/
	public Contratante create(long id);

	/**
	* Removes the contratante with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the contratante
	* @return the contratante that was removed
	* @throws NoSuchContratanteException if a contratante with the primary key could not be found
	*/
	public Contratante remove(long id) throws NoSuchContratanteException;

	public Contratante updateImpl(Contratante contratante);

	/**
	* Returns the contratante with the primary key or throws a {@link NoSuchContratanteException} if it could not be found.
	*
	* @param id the primary key of the contratante
	* @return the contratante
	* @throws NoSuchContratanteException if a contratante with the primary key could not be found
	*/
	public Contratante findByPrimaryKey(long id)
		throws NoSuchContratanteException;

	/**
	* Returns the contratante with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the contratante
	* @return the contratante, or <code>null</code> if a contratante with the primary key could not be found
	*/
	public Contratante fetchByPrimaryKey(long id);

	@Override
	public java.util.Map<java.io.Serializable, Contratante> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the contratantes.
	*
	* @return the contratantes
	*/
	public java.util.List<Contratante> findAll();

	/**
	* Returns a range of all the contratantes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @return the range of contratantes
	*/
	public java.util.List<Contratante> findAll(int start, int end);

	/**
	* Returns an ordered range of all the contratantes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of contratantes
	*/
	public java.util.List<Contratante> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Contratante> orderByComparator);

	/**
	* Returns an ordered range of all the contratantes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of contratantes
	*/
	public java.util.List<Contratante> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Contratante> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the contratantes from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of contratantes.
	*
	* @return the number of contratantes
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}