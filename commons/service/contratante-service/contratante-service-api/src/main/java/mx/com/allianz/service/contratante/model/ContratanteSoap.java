/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.contratante.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ContratanteSoap implements Serializable {
	public static ContratanteSoap toSoapModel(Contratante model) {
		ContratanteSoap soapModel = new ContratanteSoap();

		soapModel.setId(model.getId());
		soapModel.setNombre(model.getNombre());
		soapModel.setApellidoPaterno(model.getApellidoPaterno());
		soapModel.setApellidoMaterno(model.getApellidoMaterno());
		soapModel.setNumeroPoliza(model.getNumeroPoliza());

		return soapModel;
	}

	public static ContratanteSoap[] toSoapModels(Contratante[] models) {
		ContratanteSoap[] soapModels = new ContratanteSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ContratanteSoap[][] toSoapModels(Contratante[][] models) {
		ContratanteSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ContratanteSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ContratanteSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ContratanteSoap[] toSoapModels(List<Contratante> models) {
		List<ContratanteSoap> soapModels = new ArrayList<ContratanteSoap>(models.size());

		for (Contratante model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ContratanteSoap[soapModels.size()]);
	}

	public ContratanteSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getApellidoPaterno() {
		return _apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		_apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return _apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		_apellidoMaterno = apellidoMaterno;
	}

	public String getNumeroPoliza() {
		return _numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		_numeroPoliza = numeroPoliza;
	}

	private long _id;
	private String _nombre;
	private String _apellidoPaterno;
	private String _apellidoMaterno;
	private String _numeroPoliza;
}