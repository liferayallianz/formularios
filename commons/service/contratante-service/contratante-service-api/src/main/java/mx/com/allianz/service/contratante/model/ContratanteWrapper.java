/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.contratante.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Contratante}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Contratante
 * @generated
 */
@ProviderType
public class ContratanteWrapper implements Contratante,
	ModelWrapper<Contratante> {
	public ContratanteWrapper(Contratante contratante) {
		_contratante = contratante;
	}

	@Override
	public Class<?> getModelClass() {
		return Contratante.class;
	}

	@Override
	public String getModelClassName() {
		return Contratante.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("nombre", getNombre());
		attributes.put("apellidoPaterno", getApellidoPaterno());
		attributes.put("apellidoMaterno", getApellidoMaterno());
		attributes.put("numeroPoliza", getNumeroPoliza());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String apellidoPaterno = (String)attributes.get("apellidoPaterno");

		if (apellidoPaterno != null) {
			setApellidoPaterno(apellidoPaterno);
		}

		String apellidoMaterno = (String)attributes.get("apellidoMaterno");

		if (apellidoMaterno != null) {
			setApellidoMaterno(apellidoMaterno);
		}

		String numeroPoliza = (String)attributes.get("numeroPoliza");

		if (numeroPoliza != null) {
			setNumeroPoliza(numeroPoliza);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _contratante.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _contratante.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _contratante.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _contratante.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Contratante> toCacheModel() {
		return _contratante.toCacheModel();
	}

	@Override
	public int compareTo(Contratante contratante) {
		return _contratante.compareTo(contratante);
	}

	@Override
	public int hashCode() {
		return _contratante.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _contratante.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ContratanteWrapper((Contratante)_contratante.clone());
	}

	/**
	* Returns the apellido materno of this contratante.
	*
	* @return the apellido materno of this contratante
	*/
	@Override
	public java.lang.String getApellidoMaterno() {
		return _contratante.getApellidoMaterno();
	}

	/**
	* Returns the apellido paterno of this contratante.
	*
	* @return the apellido paterno of this contratante
	*/
	@Override
	public java.lang.String getApellidoPaterno() {
		return _contratante.getApellidoPaterno();
	}

	/**
	* Returns the nombre of this contratante.
	*
	* @return the nombre of this contratante
	*/
	@Override
	public java.lang.String getNombre() {
		return _contratante.getNombre();
	}

	/**
	* Returns the numero poliza of this contratante.
	*
	* @return the numero poliza of this contratante
	*/
	@Override
	public java.lang.String getNumeroPoliza() {
		return _contratante.getNumeroPoliza();
	}

	@Override
	public java.lang.String toString() {
		return _contratante.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _contratante.toXmlString();
	}

	/**
	* Returns the ID of this contratante.
	*
	* @return the ID of this contratante
	*/
	@Override
	public long getId() {
		return _contratante.getId();
	}

	/**
	* Returns the primary key of this contratante.
	*
	* @return the primary key of this contratante
	*/
	@Override
	public long getPrimaryKey() {
		return _contratante.getPrimaryKey();
	}

	@Override
	public Contratante toEscapedModel() {
		return new ContratanteWrapper(_contratante.toEscapedModel());
	}

	@Override
	public Contratante toUnescapedModel() {
		return new ContratanteWrapper(_contratante.toUnescapedModel());
	}

	@Override
	public void persist() {
		_contratante.persist();
	}

	/**
	* Sets the apellido materno of this contratante.
	*
	* @param apellidoMaterno the apellido materno of this contratante
	*/
	@Override
	public void setApellidoMaterno(java.lang.String apellidoMaterno) {
		_contratante.setApellidoMaterno(apellidoMaterno);
	}

	/**
	* Sets the apellido paterno of this contratante.
	*
	* @param apellidoPaterno the apellido paterno of this contratante
	*/
	@Override
	public void setApellidoPaterno(java.lang.String apellidoPaterno) {
		_contratante.setApellidoPaterno(apellidoPaterno);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_contratante.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_contratante.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_contratante.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_contratante.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the ID of this contratante.
	*
	* @param id the ID of this contratante
	*/
	@Override
	public void setId(long id) {
		_contratante.setId(id);
	}

	@Override
	public void setNew(boolean n) {
		_contratante.setNew(n);
	}

	/**
	* Sets the nombre of this contratante.
	*
	* @param nombre the nombre of this contratante
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_contratante.setNombre(nombre);
	}

	/**
	* Sets the numero poliza of this contratante.
	*
	* @param numeroPoliza the numero poliza of this contratante
	*/
	@Override
	public void setNumeroPoliza(java.lang.String numeroPoliza) {
		_contratante.setNumeroPoliza(numeroPoliza);
	}

	/**
	* Sets the primary key of this contratante.
	*
	* @param primaryKey the primary key of this contratante
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_contratante.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_contratante.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContratanteWrapper)) {
			return false;
		}

		ContratanteWrapper contratanteWrapper = (ContratanteWrapper)obj;

		if (Objects.equals(_contratante, contratanteWrapper._contratante)) {
			return true;
		}

		return false;
	}

	@Override
	public Contratante getWrappedModel() {
		return _contratante;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _contratante.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _contratante.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_contratante.resetOriginalValues();
	}

	private final Contratante _contratante;
}