/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.contratante.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ContratanteLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ContratanteLocalService
 * @generated
 */
@ProviderType
public class ContratanteLocalServiceWrapper implements ContratanteLocalService,
	ServiceWrapper<ContratanteLocalService> {
	public ContratanteLocalServiceWrapper(
		ContratanteLocalService contratanteLocalService) {
		_contratanteLocalService = contratanteLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _contratanteLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _contratanteLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _contratanteLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _contratanteLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _contratanteLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of contratantes.
	*
	* @return the number of contratantes
	*/
	@Override
	public int getContratantesCount() {
		return _contratanteLocalService.getContratantesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _contratanteLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _contratanteLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.contratante.model.impl.ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _contratanteLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.contratante.model.impl.ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _contratanteLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the contratantes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.contratante.model.impl.ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @return the range of contratantes
	*/
	@Override
	public java.util.List<mx.com.allianz.service.contratante.model.Contratante> getContratantes(
		int start, int end) {
		return _contratanteLocalService.getContratantes(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _contratanteLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _contratanteLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	/**
	* Adds the contratante to the database. Also notifies the appropriate model listeners.
	*
	* @param contratante the contratante
	* @return the contratante that was added
	*/
	@Override
	public mx.com.allianz.service.contratante.model.Contratante addContratante(
		mx.com.allianz.service.contratante.model.Contratante contratante) {
		return _contratanteLocalService.addContratante(contratante);
	}

	/**
	* Creates a new contratante with the primary key. Does not add the contratante to the database.
	*
	* @param id the primary key for the new contratante
	* @return the new contratante
	*/
	@Override
	public mx.com.allianz.service.contratante.model.Contratante createContratante(
		long id) {
		return _contratanteLocalService.createContratante(id);
	}

	/**
	* Deletes the contratante with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the contratante
	* @return the contratante that was removed
	* @throws PortalException if a contratante with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.contratante.model.Contratante deleteContratante(
		long id) throws com.liferay.portal.kernel.exception.PortalException {
		return _contratanteLocalService.deleteContratante(id);
	}

	/**
	* Deletes the contratante from the database. Also notifies the appropriate model listeners.
	*
	* @param contratante the contratante
	* @return the contratante that was removed
	*/
	@Override
	public mx.com.allianz.service.contratante.model.Contratante deleteContratante(
		mx.com.allianz.service.contratante.model.Contratante contratante) {
		return _contratanteLocalService.deleteContratante(contratante);
	}

	@Override
	public mx.com.allianz.service.contratante.model.Contratante fetchContratante(
		long id) {
		return _contratanteLocalService.fetchContratante(id);
	}

	/**
	* Returns the contratante with the primary key.
	*
	* @param id the primary key of the contratante
	* @return the contratante
	* @throws PortalException if a contratante with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.contratante.model.Contratante getContratante(
		long id) throws com.liferay.portal.kernel.exception.PortalException {
		return _contratanteLocalService.getContratante(id);
	}

	/**
	* Updates the contratante in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param contratante the contratante
	* @return the contratante that was updated
	*/
	@Override
	public mx.com.allianz.service.contratante.model.Contratante updateContratante(
		mx.com.allianz.service.contratante.model.Contratante contratante) {
		return _contratanteLocalService.updateContratante(contratante);
	}

	@Override
	public ContratanteLocalService getWrappedService() {
		return _contratanteLocalService;
	}

	@Override
	public void setWrappedService(
		ContratanteLocalService contratanteLocalService) {
		_contratanteLocalService = contratanteLocalService;
	}

	private ContratanteLocalService _contratanteLocalService;
}