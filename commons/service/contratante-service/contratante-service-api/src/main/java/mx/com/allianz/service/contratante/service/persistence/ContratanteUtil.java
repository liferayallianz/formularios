/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.contratante.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.contratante.model.Contratante;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the contratante service. This utility wraps {@link mx.com.allianz.service.contratante.service.persistence.impl.ContratantePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ContratantePersistence
 * @see mx.com.allianz.service.contratante.service.persistence.impl.ContratantePersistenceImpl
 * @generated
 */
@ProviderType
public class ContratanteUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Contratante contratante) {
		getPersistence().clearCache(contratante);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Contratante> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Contratante> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Contratante> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Contratante> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Contratante update(Contratante contratante) {
		return getPersistence().update(contratante);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Contratante update(Contratante contratante,
		ServiceContext serviceContext) {
		return getPersistence().update(contratante, serviceContext);
	}

	/**
	* Returns all the contratantes where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching contratantes
	*/
	public static List<Contratante> findBynombre(java.lang.String nombre) {
		return getPersistence().findBynombre(nombre);
	}

	/**
	* Returns a range of all the contratantes where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @return the range of matching contratantes
	*/
	public static List<Contratante> findBynombre(java.lang.String nombre,
		int start, int end) {
		return getPersistence().findBynombre(nombre, start, end);
	}

	/**
	* Returns an ordered range of all the contratantes where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching contratantes
	*/
	public static List<Contratante> findBynombre(java.lang.String nombre,
		int start, int end, OrderByComparator<Contratante> orderByComparator) {
		return getPersistence()
				   .findBynombre(nombre, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the contratantes where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching contratantes
	*/
	public static List<Contratante> findBynombre(java.lang.String nombre,
		int start, int end, OrderByComparator<Contratante> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBynombre(nombre, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first contratante in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contratante
	* @throws NoSuchContratanteException if a matching contratante could not be found
	*/
	public static Contratante findBynombre_First(java.lang.String nombre,
		OrderByComparator<Contratante> orderByComparator)
		throws mx.com.allianz.service.contratante.exception.NoSuchContratanteException {
		return getPersistence().findBynombre_First(nombre, orderByComparator);
	}

	/**
	* Returns the first contratante in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contratante, or <code>null</code> if a matching contratante could not be found
	*/
	public static Contratante fetchBynombre_First(java.lang.String nombre,
		OrderByComparator<Contratante> orderByComparator) {
		return getPersistence().fetchBynombre_First(nombre, orderByComparator);
	}

	/**
	* Returns the last contratante in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contratante
	* @throws NoSuchContratanteException if a matching contratante could not be found
	*/
	public static Contratante findBynombre_Last(java.lang.String nombre,
		OrderByComparator<Contratante> orderByComparator)
		throws mx.com.allianz.service.contratante.exception.NoSuchContratanteException {
		return getPersistence().findBynombre_Last(nombre, orderByComparator);
	}

	/**
	* Returns the last contratante in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contratante, or <code>null</code> if a matching contratante could not be found
	*/
	public static Contratante fetchBynombre_Last(java.lang.String nombre,
		OrderByComparator<Contratante> orderByComparator) {
		return getPersistence().fetchBynombre_Last(nombre, orderByComparator);
	}

	/**
	* Returns the contratantes before and after the current contratante in the ordered set where nombre = &#63;.
	*
	* @param id the primary key of the current contratante
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next contratante
	* @throws NoSuchContratanteException if a contratante with the primary key could not be found
	*/
	public static Contratante[] findBynombre_PrevAndNext(long id,
		java.lang.String nombre,
		OrderByComparator<Contratante> orderByComparator)
		throws mx.com.allianz.service.contratante.exception.NoSuchContratanteException {
		return getPersistence()
				   .findBynombre_PrevAndNext(id, nombre, orderByComparator);
	}

	/**
	* Removes all the contratantes where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public static void removeBynombre(java.lang.String nombre) {
		getPersistence().removeBynombre(nombre);
	}

	/**
	* Returns the number of contratantes where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching contratantes
	*/
	public static int countBynombre(java.lang.String nombre) {
		return getPersistence().countBynombre(nombre);
	}

	/**
	* Caches the contratante in the entity cache if it is enabled.
	*
	* @param contratante the contratante
	*/
	public static void cacheResult(Contratante contratante) {
		getPersistence().cacheResult(contratante);
	}

	/**
	* Caches the contratantes in the entity cache if it is enabled.
	*
	* @param contratantes the contratantes
	*/
	public static void cacheResult(List<Contratante> contratantes) {
		getPersistence().cacheResult(contratantes);
	}

	/**
	* Creates a new contratante with the primary key. Does not add the contratante to the database.
	*
	* @param id the primary key for the new contratante
	* @return the new contratante
	*/
	public static Contratante create(long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the contratante with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the contratante
	* @return the contratante that was removed
	* @throws NoSuchContratanteException if a contratante with the primary key could not be found
	*/
	public static Contratante remove(long id)
		throws mx.com.allianz.service.contratante.exception.NoSuchContratanteException {
		return getPersistence().remove(id);
	}

	public static Contratante updateImpl(Contratante contratante) {
		return getPersistence().updateImpl(contratante);
	}

	/**
	* Returns the contratante with the primary key or throws a {@link NoSuchContratanteException} if it could not be found.
	*
	* @param id the primary key of the contratante
	* @return the contratante
	* @throws NoSuchContratanteException if a contratante with the primary key could not be found
	*/
	public static Contratante findByPrimaryKey(long id)
		throws mx.com.allianz.service.contratante.exception.NoSuchContratanteException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the contratante with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the contratante
	* @return the contratante, or <code>null</code> if a contratante with the primary key could not be found
	*/
	public static Contratante fetchByPrimaryKey(long id) {
		return getPersistence().fetchByPrimaryKey(id);
	}

	public static java.util.Map<java.io.Serializable, Contratante> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the contratantes.
	*
	* @return the contratantes
	*/
	public static List<Contratante> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the contratantes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @return the range of contratantes
	*/
	public static List<Contratante> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the contratantes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of contratantes
	*/
	public static List<Contratante> findAll(int start, int end,
		OrderByComparator<Contratante> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the contratantes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContratanteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contratantes
	* @param end the upper bound of the range of contratantes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of contratantes
	*/
	public static List<Contratante> findAll(int start, int end,
		OrderByComparator<Contratante> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the contratantes from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of contratantes.
	*
	* @return the number of contratantes
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ContratantePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ContratantePersistence, ContratantePersistence> _serviceTracker =
		ServiceTrackerFactory.open(ContratantePersistence.class);
}