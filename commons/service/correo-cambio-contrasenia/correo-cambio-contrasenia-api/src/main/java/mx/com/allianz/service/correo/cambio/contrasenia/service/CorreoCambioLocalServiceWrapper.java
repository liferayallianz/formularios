/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.correo.cambio.contrasenia.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CorreoCambioLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CorreoCambioLocalService
 * @generated
 */
@ProviderType
public class CorreoCambioLocalServiceWrapper implements CorreoCambioLocalService,
	ServiceWrapper<CorreoCambioLocalService> {
	public CorreoCambioLocalServiceWrapper(
		CorreoCambioLocalService correoCambioLocalService) {
		_correoCambioLocalService = correoCambioLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _correoCambioLocalService.dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _correoCambioLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _correoCambioLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of correo cambios.
	*
	* @return the number of correo cambios
	*/
	@Override
	public int getCorreoCambiosCount() {
		return _correoCambioLocalService.getCorreoCambiosCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _correoCambioLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _correoCambioLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.correo.cambio.contrasenia.model.impl.CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _correoCambioLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.correo.cambio.contrasenia.model.impl.CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _correoCambioLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the correo cambios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.correo.cambio.contrasenia.model.impl.CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @return the range of correo cambios
	*/
	@Override
	public java.util.List<mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio> getCorreoCambios(
		int start, int end) {
		return _correoCambioLocalService.getCorreoCambios(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _correoCambioLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _correoCambioLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	/**
	* Adds the correo cambio to the database. Also notifies the appropriate model listeners.
	*
	* @param correoCambio the correo cambio
	* @return the correo cambio that was added
	*/
	@Override
	public mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio addCorreoCambio(
		mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio correoCambio) {
		return _correoCambioLocalService.addCorreoCambio(correoCambio);
	}

	/**
	* Creates a new correo cambio with the primary key. Does not add the correo cambio to the database.
	*
	* @param idCorreo the primary key for the new correo cambio
	* @return the new correo cambio
	*/
	@Override
	public mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio createCorreoCambio(
		java.lang.String idCorreo) {
		return _correoCambioLocalService.createCorreoCambio(idCorreo);
	}

	/**
	* Deletes the correo cambio with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idCorreo the primary key of the correo cambio
	* @return the correo cambio that was removed
	* @throws PortalException if a correo cambio with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio deleteCorreoCambio(
		java.lang.String idCorreo)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _correoCambioLocalService.deleteCorreoCambio(idCorreo);
	}

	/**
	* Deletes the correo cambio from the database. Also notifies the appropriate model listeners.
	*
	* @param correoCambio the correo cambio
	* @return the correo cambio that was removed
	*/
	@Override
	public mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio deleteCorreoCambio(
		mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio correoCambio) {
		return _correoCambioLocalService.deleteCorreoCambio(correoCambio);
	}

	@Override
	public mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio fetchCorreoCambio(
		java.lang.String idCorreo) {
		return _correoCambioLocalService.fetchCorreoCambio(idCorreo);
	}

	/**
	* Returns the correo cambio with the primary key.
	*
	* @param idCorreo the primary key of the correo cambio
	* @return the correo cambio
	* @throws PortalException if a correo cambio with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio getCorreoCambio(
		java.lang.String idCorreo)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _correoCambioLocalService.getCorreoCambio(idCorreo);
	}

	/**
	* Updates the correo cambio in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param correoCambio the correo cambio
	* @return the correo cambio that was updated
	*/
	@Override
	public mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio updateCorreoCambio(
		mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio correoCambio) {
		return _correoCambioLocalService.updateCorreoCambio(correoCambio);
	}

	@Override
	public CorreoCambioLocalService getWrappedService() {
		return _correoCambioLocalService;
	}

	@Override
	public void setWrappedService(
		CorreoCambioLocalService correoCambioLocalService) {
		_correoCambioLocalService = correoCambioLocalService;
	}

	private CorreoCambioLocalService _correoCambioLocalService;
}