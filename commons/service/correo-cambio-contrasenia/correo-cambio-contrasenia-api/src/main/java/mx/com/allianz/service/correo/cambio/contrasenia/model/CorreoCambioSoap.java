/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.correo.cambio.contrasenia.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class CorreoCambioSoap implements Serializable {
	public static CorreoCambioSoap toSoapModel(CorreoCambio model) {
		CorreoCambioSoap soapModel = new CorreoCambioSoap();

		soapModel.setIdCorreo(model.getIdCorreo());
		soapModel.setIdProveedor(model.getIdProveedor());
		soapModel.setRfcProveedor(model.getRfcProveedor());
		soapModel.setVigencia(model.getVigencia());
		soapModel.setEstatus(model.getEstatus());

		return soapModel;
	}

	public static CorreoCambioSoap[] toSoapModels(CorreoCambio[] models) {
		CorreoCambioSoap[] soapModels = new CorreoCambioSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CorreoCambioSoap[][] toSoapModels(CorreoCambio[][] models) {
		CorreoCambioSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CorreoCambioSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CorreoCambioSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CorreoCambioSoap[] toSoapModels(List<CorreoCambio> models) {
		List<CorreoCambioSoap> soapModels = new ArrayList<CorreoCambioSoap>(models.size());

		for (CorreoCambio model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CorreoCambioSoap[soapModels.size()]);
	}

	public CorreoCambioSoap() {
	}

	public String getPrimaryKey() {
		return _idCorreo;
	}

	public void setPrimaryKey(String pk) {
		setIdCorreo(pk);
	}

	public String getIdCorreo() {
		return _idCorreo;
	}

	public void setIdCorreo(String idCorreo) {
		_idCorreo = idCorreo;
	}

	public String getIdProveedor() {
		return _idProveedor;
	}

	public void setIdProveedor(String idProveedor) {
		_idProveedor = idProveedor;
	}

	public String getRfcProveedor() {
		return _rfcProveedor;
	}

	public void setRfcProveedor(String rfcProveedor) {
		_rfcProveedor = rfcProveedor;
	}

	public Date getVigencia() {
		return _vigencia;
	}

	public void setVigencia(Date vigencia) {
		_vigencia = vigencia;
	}

	public Boolean getEstatus() {
		return _estatus;
	}

	public void setEstatus(Boolean estatus) {
		_estatus = estatus;
	}

	private String _idCorreo;
	private String _idProveedor;
	private String _rfcProveedor;
	private Date _vigencia;
	private Boolean _estatus;
}