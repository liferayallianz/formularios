/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.correo.cambio.contrasenia.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the correo cambio service. This utility wraps {@link mx.com.allianz.service.correo.cambio.contrasenia.service.persistence.impl.CorreoCambioPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CorreoCambioPersistence
 * @see mx.com.allianz.service.correo.cambio.contrasenia.service.persistence.impl.CorreoCambioPersistenceImpl
 * @generated
 */
@ProviderType
public class CorreoCambioUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(CorreoCambio correoCambio) {
		getPersistence().clearCache(correoCambio);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CorreoCambio> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CorreoCambio> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CorreoCambio> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<CorreoCambio> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static CorreoCambio update(CorreoCambio correoCambio) {
		return getPersistence().update(correoCambio);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static CorreoCambio update(CorreoCambio correoCambio,
		ServiceContext serviceContext) {
		return getPersistence().update(correoCambio, serviceContext);
	}

	/**
	* Returns all the correo cambios where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @return the matching correo cambios
	*/
	public static List<CorreoCambio> findByidProveedor(
		java.lang.String idProveedor) {
		return getPersistence().findByidProveedor(idProveedor);
	}

	/**
	* Returns a range of all the correo cambios where idProveedor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idProveedor the id proveedor
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @return the range of matching correo cambios
	*/
	public static List<CorreoCambio> findByidProveedor(
		java.lang.String idProveedor, int start, int end) {
		return getPersistence().findByidProveedor(idProveedor, start, end);
	}

	/**
	* Returns an ordered range of all the correo cambios where idProveedor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idProveedor the id proveedor
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching correo cambios
	*/
	public static List<CorreoCambio> findByidProveedor(
		java.lang.String idProveedor, int start, int end,
		OrderByComparator<CorreoCambio> orderByComparator) {
		return getPersistence()
				   .findByidProveedor(idProveedor, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the correo cambios where idProveedor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idProveedor the id proveedor
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching correo cambios
	*/
	public static List<CorreoCambio> findByidProveedor(
		java.lang.String idProveedor, int start, int end,
		OrderByComparator<CorreoCambio> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByidProveedor(idProveedor, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching correo cambio
	* @throws NoSuchCorreoCambioException if a matching correo cambio could not be found
	*/
	public static CorreoCambio findByidProveedor_First(
		java.lang.String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator)
		throws mx.com.allianz.service.correo.cambio.contrasenia.exception.NoSuchCorreoCambioException {
		return getPersistence()
				   .findByidProveedor_First(idProveedor, orderByComparator);
	}

	/**
	* Returns the first correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching correo cambio, or <code>null</code> if a matching correo cambio could not be found
	*/
	public static CorreoCambio fetchByidProveedor_First(
		java.lang.String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator) {
		return getPersistence()
				   .fetchByidProveedor_First(idProveedor, orderByComparator);
	}

	/**
	* Returns the last correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching correo cambio
	* @throws NoSuchCorreoCambioException if a matching correo cambio could not be found
	*/
	public static CorreoCambio findByidProveedor_Last(
		java.lang.String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator)
		throws mx.com.allianz.service.correo.cambio.contrasenia.exception.NoSuchCorreoCambioException {
		return getPersistence()
				   .findByidProveedor_Last(idProveedor, orderByComparator);
	}

	/**
	* Returns the last correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching correo cambio, or <code>null</code> if a matching correo cambio could not be found
	*/
	public static CorreoCambio fetchByidProveedor_Last(
		java.lang.String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator) {
		return getPersistence()
				   .fetchByidProveedor_Last(idProveedor, orderByComparator);
	}

	/**
	* Returns the correo cambios before and after the current correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idCorreo the primary key of the current correo cambio
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next correo cambio
	* @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	*/
	public static CorreoCambio[] findByidProveedor_PrevAndNext(
		java.lang.String idCorreo, java.lang.String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator)
		throws mx.com.allianz.service.correo.cambio.contrasenia.exception.NoSuchCorreoCambioException {
		return getPersistence()
				   .findByidProveedor_PrevAndNext(idCorreo, idProveedor,
			orderByComparator);
	}

	/**
	* Removes all the correo cambios where idProveedor = &#63; from the database.
	*
	* @param idProveedor the id proveedor
	*/
	public static void removeByidProveedor(java.lang.String idProveedor) {
		getPersistence().removeByidProveedor(idProveedor);
	}

	/**
	* Returns the number of correo cambios where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @return the number of matching correo cambios
	*/
	public static int countByidProveedor(java.lang.String idProveedor) {
		return getPersistence().countByidProveedor(idProveedor);
	}

	/**
	* Caches the correo cambio in the entity cache if it is enabled.
	*
	* @param correoCambio the correo cambio
	*/
	public static void cacheResult(CorreoCambio correoCambio) {
		getPersistence().cacheResult(correoCambio);
	}

	/**
	* Caches the correo cambios in the entity cache if it is enabled.
	*
	* @param correoCambios the correo cambios
	*/
	public static void cacheResult(List<CorreoCambio> correoCambios) {
		getPersistence().cacheResult(correoCambios);
	}

	/**
	* Creates a new correo cambio with the primary key. Does not add the correo cambio to the database.
	*
	* @param idCorreo the primary key for the new correo cambio
	* @return the new correo cambio
	*/
	public static CorreoCambio create(java.lang.String idCorreo) {
		return getPersistence().create(idCorreo);
	}

	/**
	* Removes the correo cambio with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idCorreo the primary key of the correo cambio
	* @return the correo cambio that was removed
	* @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	*/
	public static CorreoCambio remove(java.lang.String idCorreo)
		throws mx.com.allianz.service.correo.cambio.contrasenia.exception.NoSuchCorreoCambioException {
		return getPersistence().remove(idCorreo);
	}

	public static CorreoCambio updateImpl(CorreoCambio correoCambio) {
		return getPersistence().updateImpl(correoCambio);
	}

	/**
	* Returns the correo cambio with the primary key or throws a {@link NoSuchCorreoCambioException} if it could not be found.
	*
	* @param idCorreo the primary key of the correo cambio
	* @return the correo cambio
	* @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	*/
	public static CorreoCambio findByPrimaryKey(java.lang.String idCorreo)
		throws mx.com.allianz.service.correo.cambio.contrasenia.exception.NoSuchCorreoCambioException {
		return getPersistence().findByPrimaryKey(idCorreo);
	}

	/**
	* Returns the correo cambio with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idCorreo the primary key of the correo cambio
	* @return the correo cambio, or <code>null</code> if a correo cambio with the primary key could not be found
	*/
	public static CorreoCambio fetchByPrimaryKey(java.lang.String idCorreo) {
		return getPersistence().fetchByPrimaryKey(idCorreo);
	}

	public static java.util.Map<java.io.Serializable, CorreoCambio> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the correo cambios.
	*
	* @return the correo cambios
	*/
	public static List<CorreoCambio> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the correo cambios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @return the range of correo cambios
	*/
	public static List<CorreoCambio> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the correo cambios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of correo cambios
	*/
	public static List<CorreoCambio> findAll(int start, int end,
		OrderByComparator<CorreoCambio> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the correo cambios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of correo cambios
	*/
	public static List<CorreoCambio> findAll(int start, int end,
		OrderByComparator<CorreoCambio> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the correo cambios from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of correo cambios.
	*
	* @return the number of correo cambios
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static CorreoCambioPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<CorreoCambioPersistence, CorreoCambioPersistence> _serviceTracker =
		ServiceTrackerFactory.open(CorreoCambioPersistence.class);
}