/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.correo.cambio.contrasenia.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link CorreoCambio}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CorreoCambio
 * @generated
 */
@ProviderType
public class CorreoCambioWrapper implements CorreoCambio,
	ModelWrapper<CorreoCambio> {
	public CorreoCambioWrapper(CorreoCambio correoCambio) {
		_correoCambio = correoCambio;
	}

	@Override
	public Class<?> getModelClass() {
		return CorreoCambio.class;
	}

	@Override
	public String getModelClassName() {
		return CorreoCambio.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idCorreo", getIdCorreo());
		attributes.put("idProveedor", getIdProveedor());
		attributes.put("rfcProveedor", getRfcProveedor());
		attributes.put("vigencia", getVigencia());
		attributes.put("estatus", getEstatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String idCorreo = (String)attributes.get("idCorreo");

		if (idCorreo != null) {
			setIdCorreo(idCorreo);
		}

		String idProveedor = (String)attributes.get("idProveedor");

		if (idProveedor != null) {
			setIdProveedor(idProveedor);
		}

		String rfcProveedor = (String)attributes.get("rfcProveedor");

		if (rfcProveedor != null) {
			setRfcProveedor(rfcProveedor);
		}

		Date vigencia = (Date)attributes.get("vigencia");

		if (vigencia != null) {
			setVigencia(vigencia);
		}

		Boolean estatus = (Boolean)attributes.get("estatus");

		if (estatus != null) {
			setEstatus(estatus);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _correoCambio.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _correoCambio.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _correoCambio.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _correoCambio.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<CorreoCambio> toCacheModel() {
		return _correoCambio.toCacheModel();
	}

	@Override
	public int compareTo(CorreoCambio correoCambio) {
		return _correoCambio.compareTo(correoCambio);
	}

	@Override
	public int hashCode() {
		return _correoCambio.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _correoCambio.getPrimaryKeyObj();
	}

	/**
	* Returns the estatus of this correo cambio.
	*
	* @return the estatus of this correo cambio
	*/
	@Override
	public java.lang.Boolean getEstatus() {
		return _correoCambio.getEstatus();
	}

	@Override
	public java.lang.Object clone() {
		return new CorreoCambioWrapper((CorreoCambio)_correoCambio.clone());
	}

	/**
	* Returns the id correo of this correo cambio.
	*
	* @return the id correo of this correo cambio
	*/
	@Override
	public java.lang.String getIdCorreo() {
		return _correoCambio.getIdCorreo();
	}

	/**
	* Returns the id proveedor of this correo cambio.
	*
	* @return the id proveedor of this correo cambio
	*/
	@Override
	public java.lang.String getIdProveedor() {
		return _correoCambio.getIdProveedor();
	}

	/**
	* Returns the primary key of this correo cambio.
	*
	* @return the primary key of this correo cambio
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _correoCambio.getPrimaryKey();
	}

	/**
	* Returns the rfc proveedor of this correo cambio.
	*
	* @return the rfc proveedor of this correo cambio
	*/
	@Override
	public java.lang.String getRfcProveedor() {
		return _correoCambio.getRfcProveedor();
	}

	@Override
	public java.lang.String toString() {
		return _correoCambio.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _correoCambio.toXmlString();
	}

	/**
	* Returns the vigencia of this correo cambio.
	*
	* @return the vigencia of this correo cambio
	*/
	@Override
	public Date getVigencia() {
		return _correoCambio.getVigencia();
	}

	@Override
	public CorreoCambio toEscapedModel() {
		return new CorreoCambioWrapper(_correoCambio.toEscapedModel());
	}

	@Override
	public CorreoCambio toUnescapedModel() {
		return new CorreoCambioWrapper(_correoCambio.toUnescapedModel());
	}

	@Override
	public void persist() {
		_correoCambio.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_correoCambio.setCachedModel(cachedModel);
	}

	/**
	* Sets the estatus of this correo cambio.
	*
	* @param estatus the estatus of this correo cambio
	*/
	@Override
	public void setEstatus(java.lang.Boolean estatus) {
		_correoCambio.setEstatus(estatus);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_correoCambio.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_correoCambio.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_correoCambio.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the id correo of this correo cambio.
	*
	* @param idCorreo the id correo of this correo cambio
	*/
	@Override
	public void setIdCorreo(java.lang.String idCorreo) {
		_correoCambio.setIdCorreo(idCorreo);
	}

	/**
	* Sets the id proveedor of this correo cambio.
	*
	* @param idProveedor the id proveedor of this correo cambio
	*/
	@Override
	public void setIdProveedor(java.lang.String idProveedor) {
		_correoCambio.setIdProveedor(idProveedor);
	}

	@Override
	public void setNew(boolean n) {
		_correoCambio.setNew(n);
	}

	/**
	* Sets the primary key of this correo cambio.
	*
	* @param primaryKey the primary key of this correo cambio
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_correoCambio.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_correoCambio.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the rfc proveedor of this correo cambio.
	*
	* @param rfcProveedor the rfc proveedor of this correo cambio
	*/
	@Override
	public void setRfcProveedor(java.lang.String rfcProveedor) {
		_correoCambio.setRfcProveedor(rfcProveedor);
	}

	/**
	* Sets the vigencia of this correo cambio.
	*
	* @param vigencia the vigencia of this correo cambio
	*/
	@Override
	public void setVigencia(Date vigencia) {
		_correoCambio.setVigencia(vigencia);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CorreoCambioWrapper)) {
			return false;
		}

		CorreoCambioWrapper correoCambioWrapper = (CorreoCambioWrapper)obj;

		if (Objects.equals(_correoCambio, correoCambioWrapper._correoCambio)) {
			return true;
		}

		return false;
	}

	@Override
	public CorreoCambio getWrappedModel() {
		return _correoCambio;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _correoCambio.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _correoCambio.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_correoCambio.resetOriginalValues();
	}

	private final CorreoCambio _correoCambio;
}