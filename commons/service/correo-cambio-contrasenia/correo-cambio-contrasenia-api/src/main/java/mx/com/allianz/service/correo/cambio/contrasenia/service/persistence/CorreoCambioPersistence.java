/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.correo.cambio.contrasenia.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.correo.cambio.contrasenia.exception.NoSuchCorreoCambioException;
import mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio;

/**
 * The persistence interface for the correo cambio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.correo.cambio.contrasenia.service.persistence.impl.CorreoCambioPersistenceImpl
 * @see CorreoCambioUtil
 * @generated
 */
@ProviderType
public interface CorreoCambioPersistence extends BasePersistence<CorreoCambio> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CorreoCambioUtil} to access the correo cambio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the correo cambios where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @return the matching correo cambios
	*/
	public java.util.List<CorreoCambio> findByidProveedor(
		java.lang.String idProveedor);

	/**
	* Returns a range of all the correo cambios where idProveedor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idProveedor the id proveedor
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @return the range of matching correo cambios
	*/
	public java.util.List<CorreoCambio> findByidProveedor(
		java.lang.String idProveedor, int start, int end);

	/**
	* Returns an ordered range of all the correo cambios where idProveedor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idProveedor the id proveedor
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching correo cambios
	*/
	public java.util.List<CorreoCambio> findByidProveedor(
		java.lang.String idProveedor, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CorreoCambio> orderByComparator);

	/**
	* Returns an ordered range of all the correo cambios where idProveedor = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idProveedor the id proveedor
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching correo cambios
	*/
	public java.util.List<CorreoCambio> findByidProveedor(
		java.lang.String idProveedor, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CorreoCambio> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching correo cambio
	* @throws NoSuchCorreoCambioException if a matching correo cambio could not be found
	*/
	public CorreoCambio findByidProveedor_First(java.lang.String idProveedor,
		com.liferay.portal.kernel.util.OrderByComparator<CorreoCambio> orderByComparator)
		throws NoSuchCorreoCambioException;

	/**
	* Returns the first correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching correo cambio, or <code>null</code> if a matching correo cambio could not be found
	*/
	public CorreoCambio fetchByidProveedor_First(java.lang.String idProveedor,
		com.liferay.portal.kernel.util.OrderByComparator<CorreoCambio> orderByComparator);

	/**
	* Returns the last correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching correo cambio
	* @throws NoSuchCorreoCambioException if a matching correo cambio could not be found
	*/
	public CorreoCambio findByidProveedor_Last(java.lang.String idProveedor,
		com.liferay.portal.kernel.util.OrderByComparator<CorreoCambio> orderByComparator)
		throws NoSuchCorreoCambioException;

	/**
	* Returns the last correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching correo cambio, or <code>null</code> if a matching correo cambio could not be found
	*/
	public CorreoCambio fetchByidProveedor_Last(java.lang.String idProveedor,
		com.liferay.portal.kernel.util.OrderByComparator<CorreoCambio> orderByComparator);

	/**
	* Returns the correo cambios before and after the current correo cambio in the ordered set where idProveedor = &#63;.
	*
	* @param idCorreo the primary key of the current correo cambio
	* @param idProveedor the id proveedor
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next correo cambio
	* @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	*/
	public CorreoCambio[] findByidProveedor_PrevAndNext(
		java.lang.String idCorreo, java.lang.String idProveedor,
		com.liferay.portal.kernel.util.OrderByComparator<CorreoCambio> orderByComparator)
		throws NoSuchCorreoCambioException;

	/**
	* Removes all the correo cambios where idProveedor = &#63; from the database.
	*
	* @param idProveedor the id proveedor
	*/
	public void removeByidProveedor(java.lang.String idProveedor);

	/**
	* Returns the number of correo cambios where idProveedor = &#63;.
	*
	* @param idProveedor the id proveedor
	* @return the number of matching correo cambios
	*/
	public int countByidProveedor(java.lang.String idProveedor);

	/**
	* Caches the correo cambio in the entity cache if it is enabled.
	*
	* @param correoCambio the correo cambio
	*/
	public void cacheResult(CorreoCambio correoCambio);

	/**
	* Caches the correo cambios in the entity cache if it is enabled.
	*
	* @param correoCambios the correo cambios
	*/
	public void cacheResult(java.util.List<CorreoCambio> correoCambios);

	/**
	* Creates a new correo cambio with the primary key. Does not add the correo cambio to the database.
	*
	* @param idCorreo the primary key for the new correo cambio
	* @return the new correo cambio
	*/
	public CorreoCambio create(java.lang.String idCorreo);

	/**
	* Removes the correo cambio with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idCorreo the primary key of the correo cambio
	* @return the correo cambio that was removed
	* @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	*/
	public CorreoCambio remove(java.lang.String idCorreo)
		throws NoSuchCorreoCambioException;

	public CorreoCambio updateImpl(CorreoCambio correoCambio);

	/**
	* Returns the correo cambio with the primary key or throws a {@link NoSuchCorreoCambioException} if it could not be found.
	*
	* @param idCorreo the primary key of the correo cambio
	* @return the correo cambio
	* @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	*/
	public CorreoCambio findByPrimaryKey(java.lang.String idCorreo)
		throws NoSuchCorreoCambioException;

	/**
	* Returns the correo cambio with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idCorreo the primary key of the correo cambio
	* @return the correo cambio, or <code>null</code> if a correo cambio with the primary key could not be found
	*/
	public CorreoCambio fetchByPrimaryKey(java.lang.String idCorreo);

	@Override
	public java.util.Map<java.io.Serializable, CorreoCambio> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the correo cambios.
	*
	* @return the correo cambios
	*/
	public java.util.List<CorreoCambio> findAll();

	/**
	* Returns a range of all the correo cambios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @return the range of correo cambios
	*/
	public java.util.List<CorreoCambio> findAll(int start, int end);

	/**
	* Returns an ordered range of all the correo cambios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of correo cambios
	*/
	public java.util.List<CorreoCambio> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CorreoCambio> orderByComparator);

	/**
	* Returns an ordered range of all the correo cambios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of correo cambios
	* @param end the upper bound of the range of correo cambios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of correo cambios
	*/
	public java.util.List<CorreoCambio> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CorreoCambio> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the correo cambios from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of correo cambios.
	*
	* @return the number of correo cambios
	*/
	public int countAll();
}