package mx.com.allianz.service.correo.cambio.contrasenia.rest;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import mx.com.allianz.commons.catalogos.dto.CorreoCambioContraseniaDTO;
import mx.com.allianz.service.correo.cambio.contrasenia.service.CorreoCambioLocalServiceUtil;

/*
 * @ApplicationPath sirve para poner la ruta relativa de la aplicaci\u00f3n
 * (Modulo).
 */
@ApplicationPath("/catalogos.correocambio")

/*
 * @Component sirve para garantizar la seguridad del path.
 */
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)

public class CorreoCambioServiceRest extends Application {
	
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todas las nacionalidades del
	 * catalogo.
	 * 
	 * @return getNacionalidades()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/getCorreocambios")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CorreoCambioContraseniaDTO> getCorreocambio(){
		return getAllCorreoCambioContrasenia();
	}

	/**
	 * Operaci\u00f3n de servicio rest para consulta de todos los hospitales del
	 * catalogo.
	 * 
	 * @return getAllCAT_Hospital().stream()
	 */
	// @GET es el nombre del procesamiento de la petici\u00f3n.
	@GET
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/findCorreocambios")
	// @Produces sirve para especificar el tipo de formato de respuesta.
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CorreoCambioContraseniaDTO> findCorreoCambio(
			
			@DefaultValue("") @QueryParam("idCorreo") String idCorreo,
			@DefaultValue("") @QueryParam("idProveedor") String idProveedor,
			@DefaultValue("") @QueryParam("rfcProveedor") String rfcProveedor,
			@DefaultValue("") @QueryParam("vigencia") Date vigencia,
			@DefaultValue("") @QueryParam("estatus") Boolean estatus){
		
		return getAllCorreoCambioContrasenia().stream()
				.filter((correocambio) -> idCorreo.isEmpty() || correocambio.getIdCorreo().contains(idCorreo))
				.filter((correocambio) -> idProveedor.isEmpty() || correocambio.getIdProveedor().contains(idProveedor))
				.filter((correocambio) -> rfcProveedor.isEmpty() || correocambio.getRfcProveedor().contains(rfcProveedor))
				.collect(Collectors.toList());	
		
		
	}
	
	private List<CorreoCambioContraseniaDTO> getAllCorreoCambioContrasenia() {
		return CorreoCambioLocalServiceUtil.getCorreoCambios(-1, -1).stream()
				.map(correocambio -> new CorreoCambioContraseniaDTO(correocambio.getIdCorreo(),
						correocambio.getIdProveedor(),correocambio.getRfcProveedor(),
						correocambio.getVigencia(),correocambio.getEstatus()))
				.collect(Collectors.toList());
	}
	
}