/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.correo.cambio.contrasenia.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CorreoCambio in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see CorreoCambio
 * @generated
 */
@ProviderType
public class CorreoCambioCacheModel implements CacheModel<CorreoCambio>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CorreoCambioCacheModel)) {
			return false;
		}

		CorreoCambioCacheModel correoCambioCacheModel = (CorreoCambioCacheModel)obj;

		if (idCorreo.equals(correoCambioCacheModel.idCorreo)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, idCorreo);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{idCorreo=");
		sb.append(idCorreo);
		sb.append(", idProveedor=");
		sb.append(idProveedor);
		sb.append(", rfcProveedor=");
		sb.append(rfcProveedor);
		sb.append(", vigencia=");
		sb.append(vigencia);
		sb.append(", estatus=");
		sb.append(estatus);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CorreoCambio toEntityModel() {
		CorreoCambioImpl correoCambioImpl = new CorreoCambioImpl();

		if (idCorreo == null) {
			correoCambioImpl.setIdCorreo(StringPool.BLANK);
		}
		else {
			correoCambioImpl.setIdCorreo(idCorreo);
		}

		if (idProveedor == null) {
			correoCambioImpl.setIdProveedor(StringPool.BLANK);
		}
		else {
			correoCambioImpl.setIdProveedor(idProveedor);
		}

		if (rfcProveedor == null) {
			correoCambioImpl.setRfcProveedor(StringPool.BLANK);
		}
		else {
			correoCambioImpl.setRfcProveedor(rfcProveedor);
		}

		if (vigencia == Long.MIN_VALUE) {
			correoCambioImpl.setVigencia(null);
		}
		else {
			correoCambioImpl.setVigencia(new Date(vigencia));
		}

		correoCambioImpl.setEstatus(estatus);

		correoCambioImpl.resetOriginalValues();

		return correoCambioImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idCorreo = objectInput.readUTF();
		idProveedor = objectInput.readUTF();
		rfcProveedor = objectInput.readUTF();
		vigencia = objectInput.readLong();

		estatus = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (idCorreo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(idCorreo);
		}

		if (idProveedor == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(idProveedor);
		}

		if (rfcProveedor == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(rfcProveedor);
		}

		objectOutput.writeLong(vigencia);

		objectOutput.writeBoolean(estatus);
	}

	public String idCorreo;
	public String idProveedor;
	public String rfcProveedor;
	public long vigencia;
	public boolean estatus;
}