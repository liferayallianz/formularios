/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.correo.cambio.contrasenia.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.correo.cambio.contrasenia.exception.NoSuchCorreoCambioException;
import mx.com.allianz.service.correo.cambio.contrasenia.model.CorreoCambio;
import mx.com.allianz.service.correo.cambio.contrasenia.model.impl.CorreoCambioImpl;
import mx.com.allianz.service.correo.cambio.contrasenia.model.impl.CorreoCambioModelImpl;
import mx.com.allianz.service.correo.cambio.contrasenia.service.persistence.CorreoCambioPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the correo cambio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CorreoCambioPersistence
 * @see mx.com.allianz.service.correo.cambio.contrasenia.service.persistence.CorreoCambioUtil
 * @generated
 */
@ProviderType
public class CorreoCambioPersistenceImpl extends BasePersistenceImpl<CorreoCambio>
	implements CorreoCambioPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CorreoCambioUtil} to access the correo cambio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CorreoCambioImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
			CorreoCambioModelImpl.FINDER_CACHE_ENABLED, CorreoCambioImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
			CorreoCambioModelImpl.FINDER_CACHE_ENABLED, CorreoCambioImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
			CorreoCambioModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDPROVEEDOR =
		new FinderPath(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
			CorreoCambioModelImpl.FINDER_CACHE_ENABLED, CorreoCambioImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByidProveedor",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDPROVEEDOR =
		new FinderPath(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
			CorreoCambioModelImpl.FINDER_CACHE_ENABLED, CorreoCambioImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByidProveedor",
			new String[] { String.class.getName() },
			CorreoCambioModelImpl.IDPROVEEDOR_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDPROVEEDOR = new FinderPath(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
			CorreoCambioModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByidProveedor",
			new String[] { String.class.getName() });

	/**
	 * Returns all the correo cambios where idProveedor = &#63;.
	 *
	 * @param idProveedor the id proveedor
	 * @return the matching correo cambios
	 */
	@Override
	public List<CorreoCambio> findByidProveedor(String idProveedor) {
		return findByidProveedor(idProveedor, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the correo cambios where idProveedor = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param idProveedor the id proveedor
	 * @param start the lower bound of the range of correo cambios
	 * @param end the upper bound of the range of correo cambios (not inclusive)
	 * @return the range of matching correo cambios
	 */
	@Override
	public List<CorreoCambio> findByidProveedor(String idProveedor, int start,
		int end) {
		return findByidProveedor(idProveedor, start, end, null);
	}

	/**
	 * Returns an ordered range of all the correo cambios where idProveedor = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param idProveedor the id proveedor
	 * @param start the lower bound of the range of correo cambios
	 * @param end the upper bound of the range of correo cambios (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching correo cambios
	 */
	@Override
	public List<CorreoCambio> findByidProveedor(String idProveedor, int start,
		int end, OrderByComparator<CorreoCambio> orderByComparator) {
		return findByidProveedor(idProveedor, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the correo cambios where idProveedor = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param idProveedor the id proveedor
	 * @param start the lower bound of the range of correo cambios
	 * @param end the upper bound of the range of correo cambios (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching correo cambios
	 */
	@Override
	public List<CorreoCambio> findByidProveedor(String idProveedor, int start,
		int end, OrderByComparator<CorreoCambio> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDPROVEEDOR;
			finderArgs = new Object[] { idProveedor };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDPROVEEDOR;
			finderArgs = new Object[] { idProveedor, start, end, orderByComparator };
		}

		List<CorreoCambio> list = null;

		if (retrieveFromCache) {
			list = (List<CorreoCambio>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CorreoCambio correoCambio : list) {
					if (!Objects.equals(idProveedor,
								correoCambio.getIdProveedor())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CORREOCAMBIO_WHERE);

			boolean bindIdProveedor = false;

			if (idProveedor == null) {
				query.append(_FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_1);
			}
			else if (idProveedor.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_3);
			}
			else {
				bindIdProveedor = true;

				query.append(_FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CorreoCambioModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIdProveedor) {
					qPos.add(idProveedor);
				}

				if (!pagination) {
					list = (List<CorreoCambio>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<CorreoCambio>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first correo cambio in the ordered set where idProveedor = &#63;.
	 *
	 * @param idProveedor the id proveedor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching correo cambio
	 * @throws NoSuchCorreoCambioException if a matching correo cambio could not be found
	 */
	@Override
	public CorreoCambio findByidProveedor_First(String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator)
		throws NoSuchCorreoCambioException {
		CorreoCambio correoCambio = fetchByidProveedor_First(idProveedor,
				orderByComparator);

		if (correoCambio != null) {
			return correoCambio;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("idProveedor=");
		msg.append(idProveedor);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCorreoCambioException(msg.toString());
	}

	/**
	 * Returns the first correo cambio in the ordered set where idProveedor = &#63;.
	 *
	 * @param idProveedor the id proveedor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching correo cambio, or <code>null</code> if a matching correo cambio could not be found
	 */
	@Override
	public CorreoCambio fetchByidProveedor_First(String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator) {
		List<CorreoCambio> list = findByidProveedor(idProveedor, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last correo cambio in the ordered set where idProveedor = &#63;.
	 *
	 * @param idProveedor the id proveedor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching correo cambio
	 * @throws NoSuchCorreoCambioException if a matching correo cambio could not be found
	 */
	@Override
	public CorreoCambio findByidProveedor_Last(String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator)
		throws NoSuchCorreoCambioException {
		CorreoCambio correoCambio = fetchByidProveedor_Last(idProveedor,
				orderByComparator);

		if (correoCambio != null) {
			return correoCambio;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("idProveedor=");
		msg.append(idProveedor);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCorreoCambioException(msg.toString());
	}

	/**
	 * Returns the last correo cambio in the ordered set where idProveedor = &#63;.
	 *
	 * @param idProveedor the id proveedor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching correo cambio, or <code>null</code> if a matching correo cambio could not be found
	 */
	@Override
	public CorreoCambio fetchByidProveedor_Last(String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator) {
		int count = countByidProveedor(idProveedor);

		if (count == 0) {
			return null;
		}

		List<CorreoCambio> list = findByidProveedor(idProveedor, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the correo cambios before and after the current correo cambio in the ordered set where idProveedor = &#63;.
	 *
	 * @param idCorreo the primary key of the current correo cambio
	 * @param idProveedor the id proveedor
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next correo cambio
	 * @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	 */
	@Override
	public CorreoCambio[] findByidProveedor_PrevAndNext(String idCorreo,
		String idProveedor, OrderByComparator<CorreoCambio> orderByComparator)
		throws NoSuchCorreoCambioException {
		CorreoCambio correoCambio = findByPrimaryKey(idCorreo);

		Session session = null;

		try {
			session = openSession();

			CorreoCambio[] array = new CorreoCambioImpl[3];

			array[0] = getByidProveedor_PrevAndNext(session, correoCambio,
					idProveedor, orderByComparator, true);

			array[1] = correoCambio;

			array[2] = getByidProveedor_PrevAndNext(session, correoCambio,
					idProveedor, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CorreoCambio getByidProveedor_PrevAndNext(Session session,
		CorreoCambio correoCambio, String idProveedor,
		OrderByComparator<CorreoCambio> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CORREOCAMBIO_WHERE);

		boolean bindIdProveedor = false;

		if (idProveedor == null) {
			query.append(_FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_1);
		}
		else if (idProveedor.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_3);
		}
		else {
			bindIdProveedor = true;

			query.append(_FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CorreoCambioModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindIdProveedor) {
			qPos.add(idProveedor);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(correoCambio);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CorreoCambio> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the correo cambios where idProveedor = &#63; from the database.
	 *
	 * @param idProveedor the id proveedor
	 */
	@Override
	public void removeByidProveedor(String idProveedor) {
		for (CorreoCambio correoCambio : findByidProveedor(idProveedor,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(correoCambio);
		}
	}

	/**
	 * Returns the number of correo cambios where idProveedor = &#63;.
	 *
	 * @param idProveedor the id proveedor
	 * @return the number of matching correo cambios
	 */
	@Override
	public int countByidProveedor(String idProveedor) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDPROVEEDOR;

		Object[] finderArgs = new Object[] { idProveedor };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CORREOCAMBIO_WHERE);

			boolean bindIdProveedor = false;

			if (idProveedor == null) {
				query.append(_FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_1);
			}
			else if (idProveedor.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_3);
			}
			else {
				bindIdProveedor = true;

				query.append(_FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIdProveedor) {
					qPos.add(idProveedor);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_1 = "correoCambio.idProveedor IS NULL";
	private static final String _FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_2 = "correoCambio.idProveedor = ?";
	private static final String _FINDER_COLUMN_IDPROVEEDOR_IDPROVEEDOR_3 = "(correoCambio.idProveedor IS NULL OR correoCambio.idProveedor = '')";

	public CorreoCambioPersistenceImpl() {
		setModelClass(CorreoCambio.class);
	}

	/**
	 * Caches the correo cambio in the entity cache if it is enabled.
	 *
	 * @param correoCambio the correo cambio
	 */
	@Override
	public void cacheResult(CorreoCambio correoCambio) {
		entityCache.putResult(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
			CorreoCambioImpl.class, correoCambio.getPrimaryKey(), correoCambio);

		correoCambio.resetOriginalValues();
	}

	/**
	 * Caches the correo cambios in the entity cache if it is enabled.
	 *
	 * @param correoCambios the correo cambios
	 */
	@Override
	public void cacheResult(List<CorreoCambio> correoCambios) {
		for (CorreoCambio correoCambio : correoCambios) {
			if (entityCache.getResult(
						CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
						CorreoCambioImpl.class, correoCambio.getPrimaryKey()) == null) {
				cacheResult(correoCambio);
			}
			else {
				correoCambio.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all correo cambios.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(CorreoCambioImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the correo cambio.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CorreoCambio correoCambio) {
		entityCache.removeResult(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
			CorreoCambioImpl.class, correoCambio.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CorreoCambio> correoCambios) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CorreoCambio correoCambio : correoCambios) {
			entityCache.removeResult(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
				CorreoCambioImpl.class, correoCambio.getPrimaryKey());
		}
	}

	/**
	 * Creates a new correo cambio with the primary key. Does not add the correo cambio to the database.
	 *
	 * @param idCorreo the primary key for the new correo cambio
	 * @return the new correo cambio
	 */
	@Override
	public CorreoCambio create(String idCorreo) {
		CorreoCambio correoCambio = new CorreoCambioImpl();

		correoCambio.setNew(true);
		correoCambio.setPrimaryKey(idCorreo);

		return correoCambio;
	}

	/**
	 * Removes the correo cambio with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idCorreo the primary key of the correo cambio
	 * @return the correo cambio that was removed
	 * @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	 */
	@Override
	public CorreoCambio remove(String idCorreo)
		throws NoSuchCorreoCambioException {
		return remove((Serializable)idCorreo);
	}

	/**
	 * Removes the correo cambio with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the correo cambio
	 * @return the correo cambio that was removed
	 * @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	 */
	@Override
	public CorreoCambio remove(Serializable primaryKey)
		throws NoSuchCorreoCambioException {
		Session session = null;

		try {
			session = openSession();

			CorreoCambio correoCambio = (CorreoCambio)session.get(CorreoCambioImpl.class,
					primaryKey);

			if (correoCambio == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCorreoCambioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(correoCambio);
		}
		catch (NoSuchCorreoCambioException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CorreoCambio removeImpl(CorreoCambio correoCambio) {
		correoCambio = toUnwrappedModel(correoCambio);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(correoCambio)) {
				correoCambio = (CorreoCambio)session.get(CorreoCambioImpl.class,
						correoCambio.getPrimaryKeyObj());
			}

			if (correoCambio != null) {
				session.delete(correoCambio);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (correoCambio != null) {
			clearCache(correoCambio);
		}

		return correoCambio;
	}

	@Override
	public CorreoCambio updateImpl(CorreoCambio correoCambio) {
		correoCambio = toUnwrappedModel(correoCambio);

		boolean isNew = correoCambio.isNew();

		CorreoCambioModelImpl correoCambioModelImpl = (CorreoCambioModelImpl)correoCambio;

		Session session = null;

		try {
			session = openSession();

			if (correoCambio.isNew()) {
				session.save(correoCambio);

				correoCambio.setNew(false);
			}
			else {
				correoCambio = (CorreoCambio)session.merge(correoCambio);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CorreoCambioModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((correoCambioModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDPROVEEDOR.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						correoCambioModelImpl.getOriginalIdProveedor()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_IDPROVEEDOR, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDPROVEEDOR,
					args);

				args = new Object[] { correoCambioModelImpl.getIdProveedor() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_IDPROVEEDOR, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDPROVEEDOR,
					args);
			}
		}

		entityCache.putResult(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
			CorreoCambioImpl.class, correoCambio.getPrimaryKey(), correoCambio,
			false);

		correoCambio.resetOriginalValues();

		return correoCambio;
	}

	protected CorreoCambio toUnwrappedModel(CorreoCambio correoCambio) {
		if (correoCambio instanceof CorreoCambioImpl) {
			return correoCambio;
		}

		CorreoCambioImpl correoCambioImpl = new CorreoCambioImpl();

		correoCambioImpl.setNew(correoCambio.isNew());
		correoCambioImpl.setPrimaryKey(correoCambio.getPrimaryKey());

		correoCambioImpl.setIdCorreo(correoCambio.getIdCorreo());
		correoCambioImpl.setIdProveedor(correoCambio.getIdProveedor());
		correoCambioImpl.setRfcProveedor(correoCambio.getRfcProveedor());
		correoCambioImpl.setVigencia(correoCambio.getVigencia());
		correoCambioImpl.setEstatus(correoCambio.getEstatus());

		return correoCambioImpl;
	}

	/**
	 * Returns the correo cambio with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the correo cambio
	 * @return the correo cambio
	 * @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	 */
	@Override
	public CorreoCambio findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCorreoCambioException {
		CorreoCambio correoCambio = fetchByPrimaryKey(primaryKey);

		if (correoCambio == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCorreoCambioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return correoCambio;
	}

	/**
	 * Returns the correo cambio with the primary key or throws a {@link NoSuchCorreoCambioException} if it could not be found.
	 *
	 * @param idCorreo the primary key of the correo cambio
	 * @return the correo cambio
	 * @throws NoSuchCorreoCambioException if a correo cambio with the primary key could not be found
	 */
	@Override
	public CorreoCambio findByPrimaryKey(String idCorreo)
		throws NoSuchCorreoCambioException {
		return findByPrimaryKey((Serializable)idCorreo);
	}

	/**
	 * Returns the correo cambio with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the correo cambio
	 * @return the correo cambio, or <code>null</code> if a correo cambio with the primary key could not be found
	 */
	@Override
	public CorreoCambio fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
				CorreoCambioImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		CorreoCambio correoCambio = (CorreoCambio)serializable;

		if (correoCambio == null) {
			Session session = null;

			try {
				session = openSession();

				correoCambio = (CorreoCambio)session.get(CorreoCambioImpl.class,
						primaryKey);

				if (correoCambio != null) {
					cacheResult(correoCambio);
				}
				else {
					entityCache.putResult(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
						CorreoCambioImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
					CorreoCambioImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return correoCambio;
	}

	/**
	 * Returns the correo cambio with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idCorreo the primary key of the correo cambio
	 * @return the correo cambio, or <code>null</code> if a correo cambio with the primary key could not be found
	 */
	@Override
	public CorreoCambio fetchByPrimaryKey(String idCorreo) {
		return fetchByPrimaryKey((Serializable)idCorreo);
	}

	@Override
	public Map<Serializable, CorreoCambio> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, CorreoCambio> map = new HashMap<Serializable, CorreoCambio>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			CorreoCambio correoCambio = fetchByPrimaryKey(primaryKey);

			if (correoCambio != null) {
				map.put(primaryKey, correoCambio);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
					CorreoCambioImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (CorreoCambio)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 4) +
				1);

		query.append(_SQL_SELECT_CORREOCAMBIO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(StringPool.APOSTROPHE);
			query.append((String)primaryKey);
			query.append(StringPool.APOSTROPHE);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (CorreoCambio correoCambio : (List<CorreoCambio>)q.list()) {
				map.put(correoCambio.getPrimaryKeyObj(), correoCambio);

				cacheResult(correoCambio);

				uncachedPrimaryKeys.remove(correoCambio.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(CorreoCambioModelImpl.ENTITY_CACHE_ENABLED,
					CorreoCambioImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the correo cambios.
	 *
	 * @return the correo cambios
	 */
	@Override
	public List<CorreoCambio> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the correo cambios.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of correo cambios
	 * @param end the upper bound of the range of correo cambios (not inclusive)
	 * @return the range of correo cambios
	 */
	@Override
	public List<CorreoCambio> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the correo cambios.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of correo cambios
	 * @param end the upper bound of the range of correo cambios (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of correo cambios
	 */
	@Override
	public List<CorreoCambio> findAll(int start, int end,
		OrderByComparator<CorreoCambio> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the correo cambios.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CorreoCambioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of correo cambios
	 * @param end the upper bound of the range of correo cambios (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of correo cambios
	 */
	@Override
	public List<CorreoCambio> findAll(int start, int end,
		OrderByComparator<CorreoCambio> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CorreoCambio> list = null;

		if (retrieveFromCache) {
			list = (List<CorreoCambio>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CORREOCAMBIO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CORREOCAMBIO;

				if (pagination) {
					sql = sql.concat(CorreoCambioModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CorreoCambio>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<CorreoCambio>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the correo cambios from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (CorreoCambio correoCambio : findAll()) {
			remove(correoCambio);
		}
	}

	/**
	 * Returns the number of correo cambios.
	 *
	 * @return the number of correo cambios
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CORREOCAMBIO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return CorreoCambioModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the correo cambio persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(CorreoCambioImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_CORREOCAMBIO = "SELECT correoCambio FROM CorreoCambio correoCambio";
	private static final String _SQL_SELECT_CORREOCAMBIO_WHERE_PKS_IN = "SELECT correoCambio FROM CorreoCambio correoCambio WHERE idCorreo IN (";
	private static final String _SQL_SELECT_CORREOCAMBIO_WHERE = "SELECT correoCambio FROM CorreoCambio correoCambio WHERE ";
	private static final String _SQL_COUNT_CORREOCAMBIO = "SELECT COUNT(correoCambio) FROM CorreoCambio correoCambio";
	private static final String _SQL_COUNT_CORREOCAMBIO_WHERE = "SELECT COUNT(correoCambio) FROM CorreoCambio correoCambio WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "correoCambio.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CorreoCambio exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CorreoCambio exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(CorreoCambioPersistenceImpl.class);
}