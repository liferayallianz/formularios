package mx.com.allianz.contacto.datospersonales.portlet;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import mx.com.allianz.commons.dto.tramite.TramiteDto;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para recibir de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        // fin Se agregan parametros para recibir de eventos de portlet
		// Se agregan parametros para compartir atributos de request en la session
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        "com.liferay.portlet.remoteable=true",
		"com.liferay.portlet.preferences-unique-per-layout=true",
		// Fin parametros para compartir atributos de request en la session
		"requires-namespaced-parameters=false",
		"javax.portlet.display-name=Commons Datos Personales",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agregan parametro para identificar el portlet en otros contextos
        "javax.portlet.name=" + DatosPersonalesConstants.KEY,
		// Fin parametro para identificar el portlet en otros contextos
        "javax.portlet.security-role-ref=power-user,user,guest",
        "javax.portlet.supports.mime-type=text/html"
	},
	service = Portlet.class
)
public class DatosPersonalesPortlet extends MVCPortlet {

//	public static final String LLAVE_TRAMITE = "tramite"; 
//	public static final String LLAVE_TRAMITE_EVENT = "clienteTramiteEvent"; 
//	public static final String LLAVE_CLIENTE_FIRMADO = "clienteFirmado"; 
//	public static final String LLAVE_CLIENTE_HABILITADO = "tramiteClienteHabilitado"; 
//	
//	@Override
//	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {
//		Gson gson = new Gson();
//		PortletSession session = request.getPortletSession();
//		Optional<Event> event = Optional.ofNullable(request).map(EventRequest::getEvent);
//		_log.debug("NumeroPolizaPortlet -> processEvent -> procesando el evento" + event.map(Event::getName).get());
//		
//		if(event.map(Event::getName).filter(LLAVE_TRAMITE_EVENT::equals).isPresent()){
//			event.map(Event::getValue)
//				 .filter(val -> (val instanceof String))
//				 .map(json -> gson.fromJson((String) json, TramiteDto.class))
//				 .ifPresent(tramite -> {
//					 _log.debug("NumeroPolizaPortlet -> processEvent -> tramite cliente habilitado = " + tramite.isTramiteClienteHabilitado());
//					 _log.debug("NumeroPolizaPortlet -> processEvent -> tramite productos = " + tramite.getProductos());
//
//					if (tramite.isTramiteClienteHabilitado()) {
//						session.setAttribute(LLAVE_CLIENTE_HABILITADO, tramite.isTramiteClienteHabilitado(),
//								PortletSession.APPLICATION_SCOPE);
//					}
//				 });
//		}
//		super.processEvent(request, response);
//	}
	
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet datospersonales - 1.0.1, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(DatosPersonalesPortlet.class);

	
}