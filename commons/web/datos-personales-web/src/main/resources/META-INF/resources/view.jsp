<%@ include file="/init.jsp" %>

<aui:form action="#" name="fm_datos_personales">
		<aui:input name="version-datospersonales" type="hidden" value="1.0.1"  />
		<aui:input cssClass="formularioCampoTexto" id="datos_personales_nombre" name="nombre" type="text" value="" label="" placeholder="datos.personales.nombre.obligatorio" required="true" >
			<aui:validator name="required" errorMessage="El nombre es requerido"/>
			<aui:validator name="maxLength" errorMessage="La longitud del nombre debe ser menor a 75 caracteres">75</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" id="datos_personales_apellido_paterno" name="apellidoPaterno" type="text" value="" label="" placeholder="datos.personales.apellido.paterno.obligatorio" required="true">
			<aui:validator name="required" errorMessage="El apellido paterno es requerido"/>
			<aui:validator name="maxLength" errorMessage="La longitud del apellido paterno debe ser menor a 50 caracteres">50</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>		
		    </aui:input>
		<aui:input cssClass="formularioCampoTexto" id="datos_personales_apellido_materno" name="apellidoMaterno" type="text" value="" label="" placeholder="datos.personales.apellido.materno" >
			<aui:validator name="maxLength" errorMessage="La longitud del apellido materno debe ser menor a 50 caracteres">50</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>				
		    </aui:input>
</aui:form>

<script type="text/javascript">

console.log("datospersonales v1.0.1");
Liferay.on('cargaDatosPersonalesDeCliente', function(cliente){
	if (cliente){
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />datos_personales_nombre').set('value', cliente.nombre ? cliente.nombre : "");
			A.one('#<portlet:namespace />datos_personales_apellido_paterno').set('value', cliente.apellidoPaterno? cliente.apellidoPaterno : "");
			A.one('#<portlet:namespace />datos_personales_apellido_materno').set('value', cliente.apellidoMaterno? cliente.apellidoMaterno : "");
		});
	} 
});
</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
	
	var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_personales').formValidator;
	
	Liferay.on('validaDatosPersonales', function(event) {
		console.log('Entrando a validaDatosPersonales');
		formValidator.validate();
		var eventoRespuesta = {};
		eventoRespuesta.valido = !formValidator.hasErrors();
		console.log('ValidaDatosPersonales eventoRespuesta.valido = ' + eventoRespuesta.valido);
		if (eventoRespuesta.valido) {
			eventoRespuesta.nombre = A.one('#<portlet:namespace />datos_personales_nombre').get('value');
			eventoRespuesta.apellidoPaterno = A.one('#<portlet:namespace />datos_personales_apellido_paterno').get('value');
			eventoRespuesta.apellidoMaterno = A.one('#<portlet:namespace />datos_personales_apellido_materno').get('value');
		}	
		Liferay.fire('validaDatosPersonalesRespuesta', eventoRespuesta );
	});
	
</aui:script>

