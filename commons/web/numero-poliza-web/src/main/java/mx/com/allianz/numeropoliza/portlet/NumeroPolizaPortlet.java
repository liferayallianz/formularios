package mx.com.allianz.numeropoliza.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.catalogos.dto.ProductoDTO;
import mx.com.allianz.commons.dto.cliente.ProductoClienteDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.service.clasificador.model.Clasificador;
import mx.com.allianz.service.clasificador.service.ClasificadorLocalServiceUtil;
import mx.com.allianz.service.producto.acx.service.ProductoACXLocalServiceUtil;

@Component(immediate = true, property = { 
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
		"com.liferay.portlet.requires-namespaced-parameters=false", "com.liferay.portlet.ajaxable=true",
		// Fin parametros para llamado de eventos de portlet
		// Se agregan parametros para compartir atributos de request en la
		// session
		"com.liferay.portlet.private-request-attributes=false", "com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.remoteable=true", "com.liferay.portlet.preferences-unique-per-layout=true",
		// Fin parametros para compartir atributos de request en la session
		"javax.portlet.display-name=Commons Numero Poliza", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest", "javax.portlet.supports.mime-type=text/html",
        "javax.portlet.supported-processing-event=clienteTramiteEvent;http://mx-allianz-liferay-namespace.com/events/clienteTramiteEvent",
		"javax.portlet.security-role-ref=power-user,user,guest"
}, 
	service = Portlet.class)
public class NumeroPolizaPortlet extends MVCPortlet {

	public static final String LLAVE_TRAMITE = "tramite"; 
	public static final String LLAVE_TRAMITE_EVENT = "clienteTramiteEvent"; 
	public static final String LLAVE_CLIENTE_FIRMADO = "clienteFirmado"; 
//	public static final String LLAVE_CLIENTE_FIRMADO_EVENT = "clienteFirmadoEvent"; 
	public static final String LLAVE_CLIENTE_HABILITADO = "tramiteClienteHabilitado"; 
//	public static final String LLAVE_CLIENTE_HABILITADO_EVENT = "tramiteClienteHabilitadoEvent"; 
	public static final String EVENTO_SIN_NOMBRE = "EVENTO SIN NOMBRE";
	
	public static final String LLAVE_TIPO_TRAMITE = "tipoTramite";
	public static final String LLAVE_TIPO_TRAMITE_GMM = "GMM";
	public static final String[] LLAVE_TRAMITES_GMM = {"GMMC", "GMMI", "GMMM"};
	public static final String LLAVE_TIPO_TRAMITE_VIDA = "Vida";
	public static final String[] LLAVE_TRAMITE_VIDA = {"SVIP", "VIEM", "CGEO", "VIPD", "DEFI", "VIPP",
		"VGMM", "VGRP", "OPMX", "SVID", "PLUS", "OPCD", "GEO3", "PLU2", "RVIM", "PENP",
		"ACTD", "OPPT", "VERT", "VINM", "RESU", "RES2", "CARA", "OPED", "VDEM", "ACTM",
		"FTOT", "RUBA", "GEO2", "PEMN", "PEDL"};

	public static final String LLAVE_PRODUCTOS_CLIENTE = "productosCliente";
	public static final String LLAVE_PRODUCTOS = "productos";
	public static final String PRODUCTO_ACTIVO = "ACT";

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		_log.info("Numero Poliza -> render -> inicio");

		Gson gson = new Gson();
		try {
			Optional<PortletSession> sessionO = Optional.ofNullable(renderRequest)
														.map(RenderRequest::getPortletSession);
			if (_log.isDebugEnabled()) {
				_log.debug(sessionO.map(session -> 
											session.getAttribute(LLAVE_TIPO_TRAMITE, PortletSession.APPLICATION_SCOPE)
									)
								   .orElse("Sin parametros en request")
				);
			}
			boolean tramiteClienteHabilitado = 
					sessionO.map(session -> 
									session.getAttribute(LLAVE_CLIENTE_HABILITADO,PortletSession.APPLICATION_SCOPE)
							)
							.filter(p -> p instanceof Boolean)
							.map(p -> (Boolean) p)
							.orElse(false);
			_log.debug("Numero Poliza -> render -> tramiteClienteHabilitado = " + tramiteClienteHabilitado);
			renderRequest.setAttribute(LLAVE_CLIENTE_HABILITADO, tramiteClienteHabilitado);

			List<String> filter = 
					sessionO.map(session -> 
									session.getAttribute(LLAVE_TIPO_TRAMITE, PortletSession.APPLICATION_SCOPE)
							)
							.filter(obj -> (obj instanceof String))
							.map(obj -> (String) obj)
							.map(tipoTramite -> 
									LLAVE_TIPO_TRAMITE_GMM.equals(tipoTramite) ? Arrays.asList(LLAVE_TRAMITES_GMM) : 
									LLAVE_TIPO_TRAMITE_VIDA.equals(tipoTramite) ? Arrays.asList(LLAVE_TRAMITE_VIDA) : 
									new ArrayList<String>()
							)
							.orElseGet(ArrayList<String>::new);
			if (_log.isDebugEnabled()) {
				_log.debug("render -> filter = " + filter);
			}

			List<ProductoClienteDTO> productosCliente = 
					sessionO.map(session -> 
									session.getAttribute(LLAVE_PRODUCTOS_CLIENTE, PortletSession.APPLICATION_SCOPE)
							)
							.filter(obj -> obj instanceof List)
							.map(obj -> (List<ProductoClienteDTO>) obj)
							.orElseGet(ArrayList<ProductoClienteDTO>::new);
			renderRequest.setAttribute(LLAVE_PRODUCTOS_CLIENTE, gson.toJson(productosCliente));
			
			_log.debug("Numero Poliza -> serveResource -> listado de productos = ");
			List<ProductoDTO> productos = ProductoACXLocalServiceUtil.getProductoACXs(-1, -1)
					.stream()
					.map(producto -> 
							new ProductoDTO(producto.getCodigoProducto(), 
											producto.getDescripcionProducto(),
											producto.getTipoProducto())
					)
					.filter(producto -> PRODUCTO_ACTIVO.equals(producto.getTipoProducto()))
					.filter(producto -> filter.isEmpty() || filter.contains(producto.getCodigoProducto()))
					.filter(producto -> {
						return (tramiteClienteHabilitado && !productosCliente.isEmpty()) ? 
								productosCliente.stream()
												.map(ProductoClienteDTO::getCodigoProducto)
												.filter(producto.getCodigoProducto()::equals)
												.findAny()
												.isPresent() : true;
					})
					.peek(_log::debug)
					.collect(Collectors.toList());
			renderRequest.setAttribute(LLAVE_PRODUCTOS, productos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.render(renderRequest, renderResponse);
	}

	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {
		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		Optional<Event> event = Optional.ofNullable(request).map(EventRequest::getEvent);
		_log.debug("NumeroPolizaPortlet -> processEvent -> procesando el evento" + event.map(Event::getName).get());
		
		switch(event.map(Event::getName).orElse(EVENTO_SIN_NOMBRE)){
			case LLAVE_TRAMITE_EVENT:
				event.map(Event::getValue)
					 .filter(val -> (val instanceof String))
					 .map(json -> gson.fromJson((String) json, TramiteDto.class))
					 .ifPresent(tramite -> {
						 session.setAttribute(LLAVE_TIPO_TRAMITE, tramite.getTipoTramite(),
							PortletSession.APPLICATION_SCOPE);
						 _log.debug("NumeroPolizaPortlet -> processEvent -> tramite cliente habilitado = " + tramite.isTramiteClienteHabilitado());
						 _log.debug("NumeroPolizaPortlet -> processEvent -> tramite productos = " + tramite.getProductos());

						if (tramite.isTramiteClienteHabilitado()) {
							session.setAttribute(LLAVE_CLIENTE_HABILITADO, tramite.isTramiteClienteHabilitado(),
									PortletSession.APPLICATION_SCOPE);
							session.setAttribute(LLAVE_PRODUCTOS_CLIENTE, tramite.getProductos(),
									PortletSession.APPLICATION_SCOPE);
						}
					 });
			break;
			default:
		}
		super.processEvent(request, response);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		if (_log.isDebugEnabled()) {
			_log.debug("Entrando a NumeroPolizaPortlet -> serveResource ");
		}
		_log.info("Entrando a NumeroPolizaPortlet -> serveResource -> siempre se invoca el serveResource ");

		Gson gson = new Gson();
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		Optional<PortletSession> sessionO = Optional.ofNullable(resourceRequest)
													.map(ResourceRequest::getPortletSession);
		String tipoContacto = Optional.ofNullable(ParamUtil.getString(resourceRequest, "tipoContacto"))
									  .orElse("Queja");
		String tipoSolicitud = Optional.ofNullable(ParamUtil.getString(resourceRequest, "tipoSolicitud"))
									   .orElse("1");
		try {
			boolean tramiteClienteHabilitado = 
					sessionO.map(session -> 
									session.getAttribute(LLAVE_TIPO_TRAMITE, PortletSession.APPLICATION_SCOPE)
							)
							.filter(p -> p instanceof Boolean)
							.map(p -> (Boolean) p)
							.orElse(false);
			
			List<ProductoClienteDTO> productosCliente = 
					sessionO.map(session -> 
									session.getAttribute(LLAVE_PRODUCTOS_CLIENTE, PortletSession.APPLICATION_SCOPE)
							)
							.filter(obj -> obj instanceof List)
							.map(obj -> (List<ProductoClienteDTO>) obj)
							.orElseGet(ArrayList<ProductoClienteDTO>::new);
			
			List<String> clasificadores = 
					Optional.ofNullable(ClasificadorLocalServiceUtil.getClasificadors(-1, -1))
							.orElseGet(ArrayList<Clasificador>::new).stream()
							.filter(clasificador -> clasificador.getNombre().split("-").length > 2)
							.filter(clasificador -> {
								String[] filtrosClasificador = clasificador.getNombre().split("-");
								return tipoContacto.equals(filtrosClasificador[0])
										&& tipoSolicitud.equals(filtrosClasificador[1]);
							})
							.map(clasificador -> clasificador.getNombre().split("-")[2])
							.collect(Collectors.toList());

			ProductoACXLocalServiceUtil.getProductoACXs(-1, -1)
					.stream()
					.filter(producto -> PRODUCTO_ACTIVO.equals(producto.getTipoProducto()))
					.filter(producto -> clasificadores.isEmpty()
							|| clasificadores.contains(producto.getCodigoProducto()))
					.map(producto -> {
						String codigoProductoCustom = producto.getCodigoProducto();
						String descripcionProductoCustom = producto.getDescripcionProducto();
						if ("PLUS".equals(codigoProductoCustom)) {
							codigoProductoCustom = codigoProductoCustom + ", PLU2";
							descripcionProductoCustom = descripcionProductoCustom + " Y OPTIMAXX PLUS FIDEICOMISO";
						}
						return new ProductoDTO(producto.getCodigoProducto(), codigoProductoCustom,
											   producto.getDescripcionProducto(), descripcionProductoCustom,
											   producto.getTipoProducto());
					})
					.filter(producto -> 
								(tramiteClienteHabilitado && !productosCliente.isEmpty()) ? 
								productosCliente.stream()
												.map(ProductoClienteDTO::getCodigoProducto)
												.filter(producto.getCodigoProducto()::equals)
												.findAny()
												.isPresent() : true
					)
					.sorted(Comparator.comparing(ProductoDTO::getCodigoProducto))
					.map(gson::toJson)
					.map(NumeroPolizaPortlet::parse)
					.filter(Optional::isPresent)
					.map(Optional::get)
					.forEach(jsonArray::put);

			resourceResponse.setContentType("text/javascript");
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonArray.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Optional<JSONObject> parse(String gsonString) {
		Optional<JSONObject> objectJson = Optional.empty();
		JSONParser parser = new JSONParser();
		try {
			objectJson = Optional.ofNullable((JSONObject) parser.parse(gsonString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return objectJson;
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet producto.poliza.web - 1.0.1, Cargado");
		}
	}

	private static Log _log = LogFactoryUtil.getLog(NumeroPolizaPortlet.class);

}