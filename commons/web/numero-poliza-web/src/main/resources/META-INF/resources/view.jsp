<%@ include file="/init.jsp" %>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mx.com.allianz.commons.catalogos.dto.ProductoDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.ProductoClienteDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.ContratanteDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.AseguradoDTO"%>
<%@ page import="com.google.gson.Gson" %>


<portlet:resourceURL var="fetchSortersResourceURL" />

<% 
	List<ProductoDTO> productos = new ArrayList<>();
    Object object = request.getAttribute("productos");
	if (object != null && object instanceof List) {
		productos = (List<ProductoDTO>) object;
	}
	//List<ProductoClienteDTO> productosCliente = new ArrayList<>();
    String jsonProductosCliente = "";
	Object object2 = request.getAttribute("productosCliente");
	if (object2 != null && object2 instanceof String) {
		jsonProductosCliente = (String) object2;
	}
	
	Boolean tramiteClienteHabilitado = false;
    Object object3 = request.getAttribute("tramiteClienteHabilitado");
	if (object3 != null && object3 instanceof Boolean){
		tramiteClienteHabilitado = (Boolean) object3;
	}
%>
<div id="divNumeroPolza">
<aui:form action="#" name="fm_numero_poliza">
	
	<aui:input name="version-producto.poliza.web" type="hidden" value="1.0.1"  />
	<aui:select cssClass="formularioCampoTexto" id="numero_poliza_emisor_poliza" name="emisorPoliza" label="" onChange="saveNumeroPolizaSelected(this);" required="<%=tramiteClienteHabilitado %>" errorMessage="Debe seleccionar un asegurado">
	    <aui:option selected="true" value="">
	        <liferay-ui:message key="numero.poliza.select.vacio" />
	    </aui:option>
	    <%
	    		for (ProductoDTO producto : productos) {
	    %>
    		<aui:option value="<%=producto.getCodigoProducto()%>"><%=producto.getCodigoProducto()%> - <%=producto.getDescripcionProducto()%></aui:option>
	    <%
	        }
	    %>
    </aui:select>
	<aui:input cssClass="formularioCampoTexto" id="numero_poliza_numero_poliza" name="numeroPoliza" type="text" value="" label="" placeholder="numero.poliza.numero.poliza" >
		<aui:validator name="rangeLength" errorMessage="La longitud del n&uacute;mero de p&oacute;liza debe ser mayor a 3 y menor a 10 caracteres">[3,10]</aui:validator>
		<aui:validator name="required" errorMessage="El n&uacute;mero de p&oacute;liza es requerido cuando se selecciona un producto">
              function(val) {
                     return A.one('#<portlet:namespace />numero_poliza_emisor_poliza').get('selectedIndex') > 0;
              }
   		</aui:validator>
   		<aui:validator name="custom" errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros o guiones">
	        function (val, fieldNode, ruleValue) {
	                var returnValue = true;
	                var iChars = "-0123456789";
	               	var value = fieldNode.get('value');
	                for (var i = 0; i < value.length; i++) {
	                    if (returnValue && iChars.indexOf(value.charAt(i)) == -1) {                  
	                    		returnValue = false;
	                    }
	                }
	                return returnValue;
	        }
	    </aui:validator>
	</aui:input>
	<aui:input type="hidden" id="hidden_numero_poliza_tipo_contacto" name="hiddenTipoContactoTemp" value="Queja"/>
	<aui:input type="hidden" id="hidden_numero_poliza_tipo_solicitud" name="hiddenTipoContactoTemp" value="1"/>
	<aui:input type="hidden" id="hidden_numero_poliza_numero_poliza" name="hiddenNumeroPolizaSelected" value=""/>
	
</aui:form>
</div>



<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("productos v1.0.3");
var productosCliente = <%=jsonProductosCliente%>;
console.log("productos productosCliente = " + productosCliente);
var tramiteClienteHabilitadoJS = <%= tramiteClienteHabilitado %>;
console.log("productos tramiteClienteHabilitadoJS = " + tramiteClienteHabilitadoJS);
var productosClienteQuejaNumeroPoliza = [];

var formValidator = Liferay.Form.get('<portlet:namespace />fm_numero_poliza').formValidator;

Liferay.on('validaNumeroPoliza', function(event) {
	formValidator.validate();
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaNumeroPoliza eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if(eventoRespuesta.valido){
		eventoRespuesta.emisorPoliza = A.one('#<portlet:namespace />numero_poliza_emisor_poliza').get('value');
		eventoRespuesta.numeroPoliza = A.one('#<portlet:namespace />numero_poliza_numero_poliza').get('value');
	}
	console.log("eventoRespuesta de numero-poliza-web");
	console.log(eventoRespuesta);
	Liferay.fire('validaNumeroPolizaRespuesta', eventoRespuesta );
});

Liferay.on('ocultarNumeroPoliza', function(event) {
/* 	var tipoContacto = A.one('#<portlet:namespace />hidden_numero_poliza_tipo_contacto').set('value','');
	var tipoSolicitud = A.one('#<portlet:namespace />hidden_numero_poliza_tipo_solicitud').set('value',''); */
	document.getElementById("divNumeroPolza").style.display = 'none' ;
});
Liferay.on('mostrarNumeroPoliza', function(event) {
	var tipoContacto = A.one('#<portlet:namespace />hidden_numero_poliza_tipo_contacto').set('value','Queja');
	document.getElementById("divNumeroPolza").style.display = 'block' ;
});

Liferay.on('clienteQuejaHabilitado', function(event) {
	console.log("clienteQuejaHabilitado on");
	console.log(event)
	tramiteClienteHabilitadoJS = true;
	console.log("event.length = " + event.productos);
	productosCliente = event.productos;
	llenaProductos2(productosCliente);
});

Liferay.on('actualizarNumerosPoliza', function(event) {
	var tipoSolicitante = event ? (event.tipoSolicitante ? event.tipoSolicitante : '1') : '1';
	if (tipoSolicitante == -1){
		tipoSolicitante = 1;
	} else {
		A.one('#<portlet:namespace />hidden_numero_poliza_numero_poliza').set('value','');
	}
	var tipoSolicitud = A.one('#<portlet:namespace />hidden_numero_poliza_tipo_solicitud').set('value',tipoSolicitante);
	Liferay.fire('cargarClasificador',{});
});
Liferay.on('cargarClasificador', function(event) {
	if (tramiteClienteHabilitadoJS) {
		console.log("quejaClienteHabilitadoHidden = true");
	} else {
		console.log("quejaClienteHabilitadoHidden = false");

		console.log("productos se llama a clasificador");
	
		var fetchTopicsURL = '<%= fetchSortersResourceURL.toString() %>';
		var tipoContacto = A.one('#<portlet:namespace />hidden_numero_poliza_tipo_contacto').get('value');
		tipoContacto = tipoContacto ? tipoContacto : 'Queja';
		var tipoSolicitud = A.one('#<portlet:namespace />hidden_numero_poliza_tipo_solicitud').get('value');
		tipoSolicitud = tipoSolicitud ? tipoSolicitud : '1';
	
		console.log('cargarClasificador, tipoContacto = ' + tipoContacto);
		console.log('cargarClasificador, tipoSolicitud = ' + tipoSolicitud);
		
		var dataProductos = {
	       	 tipoContacto: tipoContacto,
	    	 tipoSolicitud: tipoSolicitud
		};
		
	    A.io.request (
	         fetchTopicsURL, {
	         data: dataProductos,
	         dataType: 'json',
	         on: {
	                 failure: function() {
	                     console.log("Ajax failed! There was some error at the server");
	                 },
	                 success: function(event, id, obj) {
	               		if (tramiteClienteHabilitadoJS) {
	               			console.log("quejaClienteHabilitadoHidden en fetch polizas = true");
	               		
	               		} else {
	               			console.log("quejaClienteHabilitadoHidden en fetch polizas = false");
						    var productos = this.get('responseData');
						    console.log(productos);
						    llenaProductos1(productos);
						}
	                }
	             }
         }); 
	}
});

function llenaProductos1(productos){
	
	var productosSelect = A.one('#<portlet:namespace />numero_poliza_emisor_poliza');
    
	if (productos) {                    
        productosSelect.html("");
        productosSelect.append("<option value=''>Seleccione un producto </option>");
        for (var j=0; j < productos.length; j++) {
        	productosSelect.append("<option value='" + productos[j].codigoProducto + "'>" + productos[j].codigoProductoCustom + " - " + productos[j].descripcionProductoCustom + "</option>");
        }
	}
	var selectedIndex = A.one('#<portlet:namespace />hidden_numero_poliza_numero_poliza').get('value');
	console.log('selectedIndex = ' + selectedIndex);
	if (selectedIndex) {
		productosSelect.set('selectedIndex',selectedIndex);
	}
	
}

function llenaProductos2(productos){
	console.log("entro a llenaProductos2 pero lo ignora");
	console.log(productos);

	var productosSelect = A.one('#<portlet:namespace />numero_poliza_emisor_poliza');
    var seleccion = null;
    var numPoliza = null;
	if (productos) {                    
        productosSelect.html("");
        productosSelect.append("<option value=''>Seleccione un producto </option>");
        for (var j=0; j < productos.length; j++) {
        	if (productos[j].selected){
        		seleccion = productos[j].codigoProducto;
        		numPoliza = productos[j].numeroPoliza;
        		console.log("Se localiz� el producto elegido anteriormente: "+ productos[j].codigoProducto);
        	}
        	productosSelect.append("<option value='" + productos[j].codigoProducto + "'>" 
        			+ productos[j].codigoProducto + " - " + productos[j].descripcionProducto + "</option>");
        }
	}
	if (seleccion){
		productosSelect.set('value',seleccion);
		A.one('#<portlet:namespace />numero_poliza_numero_poliza').set('value',numPoliza);
		A.one('#<portlet:namespace />hidden_numero_poliza_numero_poliza').set('value',numPoliza);
	}
	var selectedIndex = A.one('#<portlet:namespace />hidden_numero_poliza_numero_poliza').get('value');
	console.log('selectedIndex = ' + selectedIndex);
// 	if (selectedIndex) {
// 		productosSelect.set('selectedIndex',selectedIndex);
// 	}
	
}

</aui:script>

<script type="text/javascript"> 

var productosCliente = <%=jsonProductosCliente%>;
console.log("productos productosCliente = " + productosCliente);
var tramiteClienteHabilitadoJS = <%= tramiteClienteHabilitado %>;
console.log("productos tramiteClienteHabilitadoJS = " + tramiteClienteHabilitadoJS);
var productosClienteQuejaNumeroPoliza = [];

Liferay.provide(window, 'saveNumeroPolizaSelected', function(param) {
	var selectedIndex = param.selectedIndex;
	new AUI().use(function(A) {
		A.one('#<portlet:namespace />hidden_numero_poliza_numero_poliza').set('value',selectedIndex); 
		console.log("productos saveNumeroPolizaSelected, selectedIndex = " +  param.selectedIndex);
		console.log("productos saveNumeroPolizaSelected, value = !!" +  param.value + "!!");
		
		if (tramiteClienteHabilitadoJS){
			for (prod in productosCliente) {
				console.log("codigo producto en productosCliente = !!" + productosCliente[prod].codigoProducto +"!!");
	
				console.log("existe producto en productosCliente? " +  (param.value == productosCliente[prod].codigoProducto));
	
				if (param.value == productosCliente[prod].codigoProducto){
					var productoPoliza = productosCliente[prod];
					var numeroPoliza = productoPoliza.numeroPoliza; 
					var asegurados = productoPoliza.asegurados;
					var contratante = productoPoliza.contratante;
					console.log(productoPoliza);
					console.log("contratante en productosCliente = " + contratante);
					console.log("productoPoliza en productosCliente = " + productoPoliza);
					console.log("numeroPoliza en productosCliente = " + numeroPoliza);
	
					A.one('#<portlet:namespace />numero_poliza_numero_poliza').set('value',numeroPoliza); 
					Liferay.fire('presentarAseguradosPoliza', {asegurados:asegurados[0]});
					Liferay.fire('presentarContratantePoliza', {contratante:contratante});
					
				}
			}
		}
	
	});
	
});
Liferay.on('cargadDatosPoliza', function(event) {
	console.log("Cargando datos de poliza");
	new AUI().use(function(A) {
		var selectPoliza = A.one('#<portlet:namespace />numero_poliza_emisor_poliza');
		selectPoliza.set('value',event.producto);
		saveNumeroPolizaSelected(selectPoliza);
		A.one('#<portlet:namespace />numero_poliza_numero_poliza').set("value",event.numeroPoliza);
	});
});
</script>