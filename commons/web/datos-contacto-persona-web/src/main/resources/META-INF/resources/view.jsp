<%@ include file="/init.jsp" %>
<aui:form action="#" name="fm_datos_contacto_persona">
	<aui:input name="version-datos.contacto.persona.web" type="hidden" value="1.0.1"  />
	<aui:input cssClass="formularioCampoTexto" id="datos_contacto_persona_telefono_particular" name="telefonoParticular" type="text" value="" label="" placeholder="datos_contacto_persona_web_DatosContactoPersona.telefonoParticular" >
		<aui:validator name="required" errorMessage="El tel&eacute;fono es requerido"/>
		<aui:validator name="rangeLength" errorMessage="La longitud del tel&eacute;fono debe ser mayor a 8 y menor a 25 caracteres">[8,25]</aui:validator>	
		<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros, guiones, parentesis o espacio">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~`!@#$%^&*_=[]\\\';,./{}|\":<>?abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
	</aui:input>
	<aui:input cssClass="formularioCampoTexto" id="datos_contacto_persona_telefono_celular" name="telefonoCelular" type="text" value="" label="" placeholder="datos_contacto_persona_web_DatosContactoPersona.telefonoCelular" >
	<aui:validator name="rangeLength" errorMessage="La longitud del tel&eacute;fono celular debe ser mayor a 8 y menor a 25 caracteres">[8,25]</aui:validator>	
		<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros, guiones, parentesis o espacio">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~`!@#$%^&*_=[]\\\';,./{}|\":<>?abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
	</aui:input>
	<aui:input cssClass="formularioCampoTexto" id="datos_contacto_persona_email" name="email" type="text" value="" label="" placeholder="datos_contacto_persona_web_DatosContactoPersona.email" >
			<aui:validator name="required" errorMessage="El correo electr&oacute;nico es requerido"/>
			<aui:validator name="email" errorMessage="Por favor ingrese un correo electr&oacute;nico v&aacute;lido, ej. nombre@mail.com"/>
	</aui:input>
	<aui:input cssClass="formularioCampoTexto" id="datos_contacto_persona_email_confirmacion" name="emailConfirmacion" type="text" value="" label="" placeholder="datos_contacto_persona_web_DatosContactoPersona.email.confirmacion" >
			<aui:validator name="required" errorMessage="La confirmaci&oacute;n de correo electr&oacute;nico es requerida"/>
			<aui:validator name="email" errorMessage="Por favor ingrese la confirmaci&oacute;n de correo electr&oacute;nico con formato de correo electr&oacute;nico v&aacute;lido, ej. nombre@mail.com"/>
			<aui:validator name="equalTo" errorMessage="La confirmaci&oacute;n de correo electr&oacute;nico debe ser igual al correo electr&oacute;nico">'#<portlet:namespace />datos_contacto_persona_email'</aui:validator>
	</aui:input>
	
</aui:form>



<script type="text/javascript">
Liferay.on('cargaDatosContactoDeCliente', function(cliente){
	console.log("1");
	if(cliente && cliente.contacto) {
		new AUI().use(function(A) {
			var contacto = cliente.contacto;
			console.log("2");
			if (contacto.telefonos) {
				var telefonos = contacto.telefonos;
				console.log("3");
	
					for(var tel in telefonos) {
						console.log(tel);
						console.log(telefonos[tel]);
						var telefono = telefonos[tel];
						if("C" == telefono.tipoTelefono|| 
								  (telefono.lada && "044"== telefono.lada)){
							A.one('#<portlet:namespace />datos_contacto_persona_telefono_celular').set('value', telefono.telefono ? telefono.telefono:"");
							A.one('#<portlet:namespace />datos_contacto_persona_telefono_particular').set('value', "");
						} else {
							A.one('#<portlet:namespace />datos_contacto_persona_telefono_celular').set('value', "");
							A.one('#<portlet:namespace />datos_contacto_persona_telefono_particular').set('value', telefono.telefono ? telefono.telefono:"");
						}
					}
			}
			var correo = contacto.correos ? (contacto.correos[0].correoElectronico ? contacto.correos[0].correoElectronico : ""):"";
			A.one('#<portlet:namespace />datos_contacto_persona_email').set('value', correo);
			A.one('#<portlet:namespace />datos_contacto_persona_email_confirmacion').set('value', correo);
		});
	}
});

Liferay.on('cargaDatosContactoDeQueja', function(queja){
	console.log("Cargando datos desde queja bien");
	if(queja && queja.email) {
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />datos_contacto_persona_email').set('value', queja.email);
			A.one('#<portlet:namespace />datos_contacto_persona_email_confirmacion').set('value', queja.email);
			A.one('#<portlet:namespace />datos_contacto_persona_telefono_particular').set('value', queja.telefonoParticular?queja.telefonoParticular:"");
			A.one('#<portlet:namespace />datos_contacto_persona_telefono_celular').set('value', queja.telefonoCelular?queja.telefonoCelular:"");
		});
	}
});
</script>
<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("datos.contacto.persona.web v1.0.1");

var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_contacto_persona').formValidator;

Liferay.on('validaDatosContacto', function(event) {
	//new AUI().use(function(A) {
	formValidator.validate();
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaDatosContactoRespuesta eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if(eventoRespuesta.valido){
		eventoRespuesta.telefonoParticular = A.one('#<portlet:namespace />datos_contacto_persona_telefono_particular').get('value');
		eventoRespuesta.telefonoCelular = A.one('#<portlet:namespace />datos_contacto_persona_telefono_celular').get('value');
		eventoRespuesta.email = A.one('#<portlet:namespace />datos_contacto_persona_email').get('value');
	}
	Liferay.fire('validaDatosContactoRespuesta', eventoRespuesta );
	//});
});
</aui:script>

