<%@ include file="/init.jsp" %>


<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mx.com.allianz.commons.catalogos.dto.EstadoDTO"%>

<% 
	List<EstadoDTO> estados = new ArrayList<>();
    Object object = request.getAttribute("estados");
	if (object != null && object instanceof List) {
		estados = (List<EstadoDTO>) object;
	}
%>

<portlet:resourceURL var="fetchCitiesResourceURL" />
 
<aui:form action="#" name="fm_direccion">
	<aui:input cssClass="formularioCampoTexto" id="direccion_direccion" name="direccion" type="text" value="" label="" placeholder="direccion.direccion" >
		<aui:validator name="maxLength" >150</aui:validator>
		<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros, letras, comas, puntos o espacio.">
	        function (val, fieldNode, ruleValue) {
	                var returnValue = true;
	                var iChars = "~`!@#$%^&*()_+=-[]\\\';/{}|\":<>?";
	                for (var i = 0; i < val.length; i++) {
	                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
	                     returnValue = false;
	                    }
	                }
	                return returnValue;
	        }
	    </aui:validator>
	</aui:input>
	<aui:input cssClass="formularioCampoTexto" id="direccion_codigo_postal" name="codigoPostal" type="number" value="" label="" placeholder="direccion.codigo.postal" >
		<aui:validator name="digits" errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros"/>
		<aui:validator name="minLength" errorMessage="La longitud del c&oacute;digo postal debe ser de 5 caracteres">5</aui:validator>
		<aui:validator name="maxLength" errorMessage="La longitud del c&oacute;digo postal debe ser de 5 caracteres">5</aui:validator>	
	</aui:input>
	<aui:select cssClass="formularioCampoTexto" id="direccion_estado" name="estado" label="" required="true" onChange='fetchCities();' >
	    <aui:option selected="true" value="">
	        <liferay-ui:message key="direccion.estado.obligatorio" />
	    </aui:option>
	    <% 
	    		for(EstadoDTO estado: estados){
	   	%>
     	<aui:option value="<%=estado.getCodigoEstado()%>"><%=estado.getDescripcion()%></aui:option>
	   	<% 	   
		   };
		%>
	</aui:select>
	<aui:select cssClass="formularioCampoTexto" id="direccion_ciudad" name="ciudad" label="" required="true" >
	    <aui:option selected="true" value="">
	        <liferay-ui:message key="direccion.ciudad.obligatorio" />
	    </aui:option>
	</aui:select>	
</aui:form>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
var formValidator = Liferay.Form.get('<portlet:namespace />fm_direccion').formValidator;

Liferay.on('validaDireccion', function(event) {
	formValidator.validate();
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaDireccion eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if(eventoRespuesta.valido){
		var estadoSelect = A.one('#<portlet:namespace />direccion_estado');
		var ciudadSelect = A.one('#<portlet:namespace />direccion_ciudad');
		console.log(estadoSelect.get('options')._nodes[estadoSelect.get('selectedIndex')].text);

		eventoRespuesta.direccion = A.one('#<portlet:namespace />direccion_direccion').get('value');
		eventoRespuesta.codigoPostal = A.one('#<portlet:namespace />direccion_codigo_postal').get('value');
		eventoRespuesta.estado = estadoSelect.get('value');
		eventoRespuesta.estadoDescripcion = estadoSelect.get('options')._nodes[estadoSelect.get('selectedIndex')].text;
		eventoRespuesta.ciudad = ciudadSelect.get('value');
		eventoRespuesta.ciudadDescripcion = ciudadSelect.get('options')._nodes[ciudadSelect.get('selectedIndex')].text;

	}
	Liferay.fire('validaDireccionRespuesta', eventoRespuesta );
});

Liferay.provide(window, 'fetchCities', function() {
    var fetchTopicsURL = '<%= fetchCitiesResourceURL.toString() %>';
	var estadoAjax = A.one('#<portlet:namespace />direccion_estado');
	var ciudadAjax = A.one('#<portlet:namespace />direccion_ciudad');
	console.log('Estado = ' + estadoAjax.get('value'));
    A.io.request (
         fetchTopicsURL, {
         data: {
             estado: estadoAjax.get('value')
         },
         dataType: 'json',
         on: {
                 failure: function() {
                     console.log("Ajax failed! There was some error at the server");
                 },
                 success: function(event, id, obj) {
                     var cities = this.get('responseData');
                     console.log(cities);
                     ciudadAjax.html("");
                     ciudadAjax.append("<option value=''>Seleccione su ciudad *</option>");
                     for (var j=0; j < cities.length; j++) {
                    	 	ciudadAjax.append("<option value='" + cities[j].codigoCiudad + "'>" + cities[j].descripcion + "</option>");
                     }
                 }
             }
         }); 
});
</aui:script>
