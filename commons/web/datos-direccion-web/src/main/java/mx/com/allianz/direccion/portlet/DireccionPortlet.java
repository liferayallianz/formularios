package mx.com.allianz.direccion.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.osgi.service.component.annotations.Component;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.catalogos.dto.CiudadDTO;
import mx.com.allianz.commons.catalogos.dto.EstadoDTO;
import mx.com.allianz.service.ciudad.model.Ciudad;
import mx.com.allianz.service.ciudad.service.CiudadLocalServiceUtil;
import mx.com.allianz.service.estado.service.EstadoLocalServiceUtil;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para recibir de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        // fin Se agregan parametros para recibir de eventos de portlet
		// Se agregan parametros para compartir atributos de request en la session
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        "com.liferay.portlet.remoteable=true",
		"com.liferay.portlet.preferences-unique-per-layout=true",
		// Fin parametros para compartir atributos de request en la session
		"javax.portlet.display-name=Commons Datos de Direccion",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DireccionPortlet extends MVCPortlet {

	public static final int CODIGO_PAIS_MEXICO=412;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try {

		List<EstadoDTO> estados = EstadoLocalServiceUtil.getEstados(-1,-1).stream().filter
			(estado -> estado.getCodigoPais() == CODIGO_PAIS_MEXICO).map(estado ->
			new EstadoDTO(estado.getCodigoPais(), estado.getCodigoEstado(),
			estado.getDescripcion())).sorted(Comparator.comparing(EstadoDTO::getDescripcion))
			.collect(Collectors.toList());
		renderRequest.setAttribute("estados", estados);
		} catch (Exception e ) { e.printStackTrace(); }

		super.render(renderRequest, renderResponse);
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws IOException,
            PortletException {
		System.out.println("DireccionPortlet -> serveResource");
		String valorParametro = "9";
		Gson gson = new Gson();

		try {
			String parametro = ParamUtil.getString(resourceRequest, "estado");
			if(parametro != null){
				valorParametro = parametro;
			}
			System.out.println("parametro = " + parametro);
			Integer codigoEstado = Integer.parseInt(valorParametro);
			
	        JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
	        
	        Optional.ofNullable(CiudadLocalServiceUtil.getCiudads(-1,-1))
	        .orElseGet(ArrayList<Ciudad>::new).stream()
	        .filter(ciudad -> ciudad.getCodigoPais() == CODIGO_PAIS_MEXICO)
	        .filter(ciudad -> ciudad.getCodigoEstado() == codigoEstado)
	        .filter(ciudad -> !"SIN CIUDAD".equals(ciudad.getDescripcion()))
	        .map((ciudad) -> new CiudadDTO(ciudad.getCodigoPais(), ciudad.getCodigoEstado(),
	                ciudad.getCodigoCiudad(), ciudad.getDescripcion()))
	        .sorted((ciudad1, ciudad2) -> ciudad1.getDescripcion().compareTo(ciudad2.getDescripcion()))
	        .map(gson::toJson).map(DireccionPortlet::parse).filter(Optional::isPresent).map(Optional::get)
	        .forEach(jsonArray::put); 
	        
	        resourceResponse.setContentType("text/javascript");
	        PrintWriter writer = resourceResponse.getWriter();
	        writer.write(jsonArray.toString());
		} catch (Exception e ) { e.printStackTrace(); }
    }
	
	private static Optional<JSONObject> parse(String gsonString) {
		JSONParser parser = new JSONParser();
		Optional<JSONObject> objectJson = null;
		try {
			objectJson = Optional.ofNullable((JSONObject)parser.parse(gsonString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return objectJson;
	}
}