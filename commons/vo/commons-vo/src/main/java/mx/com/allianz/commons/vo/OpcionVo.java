package mx.com.allianz.commons.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OpcionVo implements Serializable { 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -324505772110410607L;
	private String descripcion;
	private String valor;
	
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}