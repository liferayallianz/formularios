package mx.com.allianz.commons.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ClasificadorVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6439443100261186131L;
	private String idClasificador;
	private String nombre;
	private String descripcion;
	private String predeterminado;
	
	public ClasificadorVo() {
	}

	public ClasificadorVo(String idClasificador, String nombre, String descripcion, String predeterminado) {
		super();
		this.idClasificador = idClasificador;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.predeterminado = predeterminado;
	}

	@Override
	public String toString() {
		return "ClasificadoresVo [idClasificador=" + idClasificador
				+ ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", predeterminado=" + predeterminado + "]";
	}

	public String getIdClasificador() {
		return idClasificador;
	}

	public void setIdClasificador(String idClasificador) {
		this.idClasificador = idClasificador;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setNOMBRE(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setDESCRIPCION(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getPredeterminado() {
		return predeterminado;
	}

	public void setPredeterminado(String predeterminado) {
		this.predeterminado = predeterminado;
	}
}
