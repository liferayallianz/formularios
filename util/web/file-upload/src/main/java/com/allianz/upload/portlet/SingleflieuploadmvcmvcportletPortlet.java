package com.allianz.upload.portlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

import com.google.gson.Gson;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import mx.com.allianz.commons.catalogos.dto.DocumentoDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.service.documento.model.Documento;
import mx.com.allianz.service.documento.service.DocumentoLocalServiceUtil;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // fin parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Util Carga Archivos ",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest",
		//Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-processing-event=solicitanteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent"
	},
	service = Portlet.class
)
public class SingleflieuploadmvcmvcportletPortlet extends MVCPortlet {
	
	private static String ROOT_FOLDER_NAME = "upload";
	private static String ROOT_FOLDER_DESCRIPTION = "My folder description";
	private static long PARENT_FOLDER_ID = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
	
	private String folderName = "";
	@Override
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		System.out.println("serveResource -> SingleflieuploadmvcmvcportletPortlet");
		folderName = ROOT_FOLDER_NAME;
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
        ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
        System.out.println("themeDisplay: " + themeDisplay );
        System.out.println("ids_docs:"+uploadRequest.getParameter("docIds"));
        Integer numberOfUploads = new Integer(uploadRequest.getParameter("numUploads"));
        resourceRequest.getPortletSession().setAttribute("ids_documentos", uploadRequest.getParameter("docIds"), PortletSession.APPLICATION_SCOPE);
        
        String[] rutas = new String[numberOfUploads];
        
        
        createFolder(resourceRequest, themeDisplay);
        
		// declara ayyay
        for(int index=0 ; index<numberOfUploads ; index++){
            String fileInputName = "uploadFile_"+index;
            rutas[index] = fileUpload(themeDisplay, resourceRequest, fileInputName);
            //agrega al array
        }
        // sube a sesion
        resourceRequest.getPortletSession().setAttribute("archivos_subidos", rutas, PortletSession.APPLICATION_SCOPE);
        
		super.serveResource(resourceRequest, resourceResponse);

    }
	
	public Folder createFolder(ResourceRequest actionRequest,ThemeDisplay themeDisplay){
	
        boolean folderExist = isFolderExist(themeDisplay);
        Folder folder = null;
		if (!folderExist) {
			long repositoryId = themeDisplay.getScopeGroupId();		
			try {
				ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(), actionRequest);
				folder = DLAppServiceUtil.addFolder(repositoryId,PARENT_FOLDER_ID, folderName,ROOT_FOLDER_DESCRIPTION, serviceContext);
			} catch (PortalException e1) {
				e1.printStackTrace();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}			
		}
		return folder;
	}
	
	
	public boolean isFolderExist(ThemeDisplay themeDisplay){
		
		boolean folderExist = false;
		
		try {
			DLAppServiceUtil.getFolder(themeDisplay.getScopeGroupId(), PARENT_FOLDER_ID, folderName);
			folderExist = true;
			System.out.println("Upload folder already exists");
		} catch (Exception e) {	
			System.out.println(e.getMessage());
		}
		return folderExist;
	}
	
	public Folder getFolder(ThemeDisplay themeDisplay){
		Folder folder = null;
		try {
			folder =DLAppServiceUtil.getFolder(themeDisplay.getScopeGroupId(), PARENT_FOLDER_ID, folderName);
		} catch (Exception e) {	
			System.out.println(e.getMessage());
		}
		return folder;
	}
	
	public String fileUpload(ThemeDisplay themeDisplay,ResourceRequest actionRequest, String filename){
		String ruta = null;
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
      
		String fileName=uploadPortletRequest.getFileName(filename);		 			
		File file = uploadPortletRequest.getFile(filename);
		String mimeType = uploadPortletRequest.getContentType(filename);
		
        String title = fileName;
		String description = "This file is added via programatically";
		long repositoryId = themeDisplay.getScopeGroupId();
		
	    try (InputStream is = new FileInputStream( file )){
	    	Folder folder = getFolder(themeDisplay);
	    	ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(), actionRequest);
	    	ruta = new Date().getTime() + fileName;
	    	DLAppServiceUtil.addFileEntry(repositoryId, folder.getFolderId(), ruta, mimeType, 
	    			title, description, "", is, file.getTotalSpace(), serviceContext);
	    	
	    	//Obteniendo url del archivo 
	    	
	    	String ImgURL = getImageURL(themeDisplay, title);
	    	
	    	System.out.println("***Image Url on server "+ImgURL);
	    	
	     } catch (Exception e){
	    
	       System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }
	    return ruta;

	}
		
	
	public String getImageURL(ThemeDisplay themeDisplay, String filename){
		
		String imageURL= "";
		long repositoryId = themeDisplay.getScopeGroupId();
		try {
			Folder folder =getFolder(themeDisplay);
			List<FileEntry> fileEntries = DLAppServiceUtil.getFileEntries(repositoryId, folder.getFolderId());
			 for (FileEntry file : fileEntries) {
				String url = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + "/" + 
						      file.getFolderId() +  "/" +file.getTitle() ;
				
				String filenameStored = file.getTitle();
				
				if(filenameStored.equals(filename)){
					imageURL = url;
					System.out.println("URL is : "+imageURL+" filename is "+file.getTitle().split("\\.")[0]);
					
				}
			   
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return imageURL;
		
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		System.out.println("------------------------------------render de file upload---------------------------");
		if(_log.isDebugEnabled()){
			_log.debug("SingleflieuploadmvcmvcportletPortlet -> render de file upload");
		}
		try {

			PortletSession session = renderRequest.getPortletSession();
			String tipoTramite = (String) session.getAttribute("tipoTramite", PortletSession.APPLICATION_SCOPE);
			System.out.println("tramite:" + tipoTramite);
			if(_log.isDebugEnabled()){
				_log.debug("tramite:" + tipoTramite);
			}
			List<DocumentoDTO> documentos = Optional.ofNullable(DocumentoLocalServiceUtil.getDocumentos(-1,-1))
													.orElseGet(ArrayList<Documento>::new)
													.stream()
													.filter(documento -> 
														documento.getTipo().equalsIgnoreCase(tipoTramite)
													)
													.map(documento -> 
														new DocumentoDTO(documento.getIdDocumentos(),
																		 documento.getTipo(), 
																		 documento.getCodigo(),
																		 documento.getDescripcion())
													)
													.sorted(Comparator.comparing(DocumentoDTO::getIdDocumentos))
													.collect(Collectors.toList());
			renderRequest.setAttribute("documentos", documentos);
			
		} catch (Exception e ) { e.printStackTrace(); }

		super.render(renderRequest, renderResponse);
	}
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		if(_log.isDebugEnabled()) {
			_log.debug("SingleflieuploadmvcmvcportletPortlet -> processEvent");
		}
		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		Optional<Event> event = Optional.ofNullable(request)
										.map(EventRequest::getEvent);
		if (event.map(Event::getName)
				 .filter("solicitanteTramiteClienteEvent"::equals)
				 .isPresent()) {
			event.map(Event::getValue)
				 .filter(val -> (val instanceof String))
				 .map(json -> gson.fromJson((String) json, TramiteDto.class))
				 .map(TramiteDto::getTipoTramite)
				 .ifPresent(tramite -> {
					 	if(_log.isDebugEnabled()){
							_log.debug("Objeto tramite: " + tramite);
						}
					 	session.setAttribute("tipoTramite", tramite, PortletSession.APPLICATION_SCOPE);
				 });
		}
		super.processEvent(request, response);
	}

	private static Log _log = LogFactoryUtil.getLog(SingleflieuploadmvcmvcportletPortlet.class);

}