<%@ include file="/init.jsp" %>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mx.com.allianz.commons.catalogos.dto.DocumentoDTO"%>

<% 
	List<DocumentoDTO> documentos = new ArrayList<>();
    Object object = request.getAttribute("documentos");
	if (object != null && object instanceof List) {
		documentos = (List<DocumentoDTO>) object;
	}
%>

<portlet:defineObjects />
<portlet:resourceURL var="uploadFile"/>

<p style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;font-size: 14px; font-style: normal; font-variant: normal; font-weight: bold; line-height: 20px;color: #003781;"><h4>Documentos</h4></p>

<p>Por cada archivo adjunto deber&aacute; presionar el bot&oacute;n  <input type="button" id="addUploadTag" value="Agregar Archivo" /></p> <br/>

<form method="post" enctype="multipart/form-data" id="uploadForm" name="uploadForm">

	<table id="uploadTable">
		<tr>
			<td>
				<select id="upload_form_documentos" name="documentos" style="width:240px;">
					<option selected="true" value="">
				        Seleccione un archivo
				    </option>
				    <% for(DocumentoDTO documento: documentos){ %>
			     	<option value="<%=documento.getIdDocumentos()%>"><%=documento.getDescripcion()%></option>
				   	<%  }; %>
				</select>
			
			</td>
			<td> <input type="file" id="fileUpload" name="fileUpload" label="Seleccione un tipo de archivo"/> </td>
		</tr>
	</table>
</form> 


<div class="fade in lex-modal modal" id="<portlet:namespace/>indicaciones_carga_archivo_modal"
role="dialog" tabindex="-1" style="display:none;">
	<div class="modal-dialog modal-full-screen" style="height:500px;">
		<div class="modal-content">
			<div id="<portlet:namespace/>indicaciones_carga_archivo_modal_div">
					<div class="modal-header">
						<button aria-labelledby="Close" class="btn btn-default close" data-dismiss="modal" role="button" type="button" onClick="closeIndicacionesCargaArchivoModal();">
							<svg aria-hidden="true" class="lexicon-icon lexicon-icon-times">
								<use xlink:href="<%= themeDisplay.getPathThemeImages() + "/lexicon/icons.svg" %>#times" />
							</svg>
						</button>
						<h4 class="modal-title" ><liferay-ui:message key="Tr&aacute;mites @Clientes - Documentos adjuntos" /></h4>
					</div>
					<div class="modal-body" >
						<h4>DOCUMENTOS ADJUNTOS PARA EL TR&Aacute;MTE</h4>
						</br>
						<p>Puede adjuntar al tr&aacute;mite la cantidad de archivos que requiera siempre y cuando el tama&nacute;o total estos no sea mayor a 10 MB.</p>
						<p>A fin de que los documentos se transmitan de la forma m�s r�pida posible se recomienda:</p>	
							<p>&emsp;1.-Escanear como imagen.</p>
							<p>&emsp;2.-Usar en su esc&aacute;ner resoluciones bajas de 150 � 300 PPP (puntos por pulgada).</p>
					</div>

					<div class="modal-footer">
						<button class="btn btn-link close-modal" data-dismiss="modal" type="button" onClick="closeIndicacionesCargaArchivoModal();"><liferay-ui:message key="Cerrar" /></button>
					</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function() {
		
		//Funcion eliminar fila tabla upload files
		var deleteCallBack = function(){
        	var index = $(".deleteBtnClass").index(this)+2;
            $("#uploadTable tr:nth-child("+index+")").remove();
        }
		
		//Funcion agregar fila upload file y boton eliminar
		$("#addUploadTag").click(function() {
			new AUI().use(function(A) {
				A.one('#<portlet:namespace />indicaciones_carga_archivo_modal')._node.style.display = 'block';
			});	
		
			var row = $("<tr>");
			
			var btn = $("<input>",{
                type:"button",
                val:"Eliminar"
            });
			btn.click(deleteCallBack).addClass("deleteBtnClass");

			var select = $('<select name="documentos" style="width:240px;">');
			$("<option />", {value: "", text: "Seleccione un tipo de archivo"}).appendTo(select);
			<% for(DocumentoDTO documento: documentos){ %>
			$("<option />", {value: '<%=documento.getIdDocumentos()%>', text: '<%=documento.getDescripcion()%>'}).appendTo(select);<%  }; %>
			
			$("<td>").append(select).appendTo(row);
			$("<td>").append("<input type='file' name='uploadedFile'>").appendTo(row);
			$("<td>").append(btn).appendTo(row);
			row.appendTo("#uploadTable");
		});
		
	});
	
	Liferay.on('validaFileUplaod', function (event) {
		new AUI().use(function(A) {
			var eventoRespuesta = {};
			eventoRespuesta.valido = true;
			console.log('validaFileUplaod eventoRespuesta.valido = ' + eventoRespuesta.valido);
			var index = 0;
			var ids = "";
			$("#uploadForm table tr input[type='file']").each(function () {
				console.log("Value of file input "+this.name);
			    this.name= "uploadFile_"+index;
			    this.id= "uploadFile_"+index;
			    index++;
			});
			
			$("#uploadForm table tr select option:selected").each(function () {
				console.log(this.value);
				ids += this.value + ',' ; 
			});
			console.log(ids.substring(0,ids.length-1));
			
			$("#uploadForm").append("<input type='hidden' id='numUploads' name='numUploads' />");
			$("#numUploads").attr("value", index);
		
		 	$.post('<%=uploadFile.toString()%>', {
		 		docIds:     ids.substring(0,ids.length-1),
		 			id: 'uploadForm',
		                upload: true
		            }, function(data, status){
		                console.log("success!!");
						Liferay.fire('validaFileUplaodRespuesta', eventoRespuesta );
			});
		});
	});
	Liferay.provide(window, 'closeIndicacionesCargaArchivoModal', function() { 
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />indicaciones_carga_archivo_modal')._node.style.display = 'none';
		});
	});
</script>
