<%@ include file="/init.jsp" %>

<aui:form name="adjuntarDocumento" enctype="multipart/form-data">
    <p class="formularioEtiquetaTexto">Adjuntar un documento</p>
    <aui:input cssClass="formularioCampoTexto" type="file" name="myFile" label="">
    		<aui:validator name="acceptFiles">'doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,png,tif,gif'</aui:validator>
    </aui:input>
    <p class="formularioEtiquetaTexto">Los formatos v&aacute;lidos son Word, Excel, Power Point, PDF, JPEG con un peso m&aacute;ximo de 10 MB.</p>
</aui:form>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
var formValidator = Liferay.Form.get('<portlet:namespace />adjuntarDocumento').formValidator;

Liferay.on('validaArchivo', function (event) {
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaArchivo eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if(eventoRespuesta.valido) {
		//eventoRespuesta.archivo = null;//A.one('#<portlet:namespace />myFile');
	}
	Liferay.fire('validaArchivoRespuesta', eventoRespuesta );
});
</aui:script>