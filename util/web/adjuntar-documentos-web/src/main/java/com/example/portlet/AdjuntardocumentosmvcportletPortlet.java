package com.example.portlet;

import static javax.portlet.PortletSession.APPLICATION_SCOPE;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // Fin Se agregan parametros para llamado de eventos de portle
		"javax.portlet.display-name=Util Adjuntar Documentos",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest",
		"javax.portlet.supported-processing-event=detalleQuejaEvent;http://mx-allianz-liferay-namespace.com/events/detalleQuejaEvent",
	},
	service = Portlet.class
)
public class AdjuntardocumentosmvcportletPortlet extends MVCPortlet {

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		_log.debug("Ingresando a Adjuntar documentos queja");

		UploadPortletRequest uploadPortletRequest = null;
		File targetFile = null;
		File sourcefile = null;
		String carpeta = null;
		String filename = null;
		JSONObject jsonMessageObject = JSONFactoryUtil.createJSONObject();
		
		try (PrintWriter writer = resourceResponse.getWriter()){
			uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
	    	
			_log.debug("!!!!!!!!!!!  sourcefile !!!!!!!!!!");
			sourcefile = uploadPortletRequest.getFile("myFile");
	    	carpeta = Optional.ofNullable(resourceRequest)
							  .map(ResourceRequest::getPortletSession)
							  .map(session -> session.getAttribute("carpeta", APPLICATION_SCOPE))
							  .filter(val -> (val instanceof String))
							  .map(val -> (String) val)
							  .orElse("");
	    	filename = ParamUtil.getString(resourceRequest, "uploadFileName");
	    	if (filename.contains("\\")) {
	    		filename = filename.substring(filename.lastIndexOf("\\")+1, filename.length());
	    	}
	    	
			_log.debug("processActionQuejasSenderEnvio found queja: " + carpeta);
			_log.debug("documentos queja path = " + carpeta);
			_log.debug("documentos queja filename = " + filename);

			try {
				Files.walk(Paths.get(carpeta))
					 .map(Path::toFile)
					 .peek(System.out::println)
					 .forEach(File::delete);
				
				targetFile = new File(carpeta, filename);
				if (!targetFile.exists()) {
					FileUtil.copyFile(sourcefile, targetFile);
				}
				sourcefile.delete();
				
				jsonMessageObject.put("success",true);
				resourceResponse.setContentType("text/plain");
				writer.write(jsonMessageObject.toString());
				_log.debug("documentos queja success");
			} catch (IOException e1) {
				e1.printStackTrace();
				writeError(resourceResponse, "Error al cargar Archivo");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
    	super.serveResource(resourceRequest, resourceResponse);
	}
	
	private void writeError(ResourceResponse response, String errorMessage){
    		_log.debug("Write Error: " + errorMessage);
    		JSONObject jsonMessageSuccess = JSONFactoryUtil.createJSONObject();
    		jsonMessageSuccess.put("success", false); 
    		jsonMessageSuccess.put("errorMessage", errorMessage); 
    		response.setContentType("text/plain");
			try (PrintWriter writer = response.getWriter()){
				writer.write(jsonMessageSuccess.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		_log.debug("Process event -> adjuntar documentos");
		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		Optional<Event> event = Optional.ofNullable(request)
										.map(EventRequest::getEvent);
	    if(event.map(Event::getName)
	    		.filter("detalleQuejaEvent"::equals)
	    		.isPresent()){
	    	event.map(Event::getValue)
	    		 .filter(val -> (val instanceof String))
	    		 .map(obj -> (String)obj)
	    		 .map(param -> gson.fromJson(param, QuejaFlujoDTO.class))
	    		 .map(QuejaFlujoDTO::getCarpeta)
	    		 .ifPresent(carpeta -> {
	    			 _log.debug("Parsear Cadena en process event carpeta : " + carpeta);
	    			 System.out.println("Parsear Cadena en process event carpeta : " + carpeta);

	    			 session.setAttribute("carpeta", carpeta, PortletSession.APPLICATION_SCOPE);
	    		 });
	    }
	    super.processEvent(request, response);
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet adjuntardocumentos - 1.0.1, Cargado");
		}
	}


	private static final Log _log = LogFactoryUtil.getLog(
			AdjuntardocumentosmvcportletPortlet.class);
}