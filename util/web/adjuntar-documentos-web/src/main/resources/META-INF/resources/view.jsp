<%@ include file="/init.jsp" %>

<aui:form name="form_adjuntar_documentos" enctype="multipart/form-data">
	<aui:input name="version-adjuntardocumentos" type="hidden" value="1.0.1"  />
    <p class="formularioEtiquetaTexto">Adjuntar un documento</p>
    <aui:input cssClass="formularioCampoTexto" id="upload_file_button" type="file" name="myFile" label="">
    	<aui:validator name="acceptFiles">'doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,png,tif,gif'</aui:validator>
    </aui:input>
    <aui:input id="hidden_upload_file_name" name="uploadFileName" type="hidden" value="" />
    <p class="formularioEtiquetaTexto">Formatos: Word, Excel, Power Point, PDF, JPG, JPEG, PNG, GIF m�ximo 10 MB.</p>
</aui:form>


<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("adjuntardocumentos v1.0.1");

var formValidator = Liferay.Form.get('<portlet:namespace />form_adjuntar_documentos').formValidator;

Liferay.on('validaFileUplaod', function(event){
	formValidator.validate();
	var eventoRespuesta = {};
    eventoRespuesta.valido = !formValidator.hasErrors();
    console.log('validaDireccion eventoRespuesta.valido = ' + eventoRespuesta.valido);
    if(eventoRespuesta.valido){
		var uploadTextName = A.one('#<portlet:namespace/>upload_file_button').get('value');
		console.log("uploadTextName = " + uploadTextName);
		
		if (uploadTextName.indexOf('C:\\fakepath\\') != -1) {
			uploadTextName = uploadTextName.replace(/C:\\fakepath\\/, '');
		}
		A.one('#<portlet:namespace/>hidden_upload_file_name').set('value', uploadTextName);
    	uploadFile(eventoRespuesta);
    } else {
    	Liferay.fire('validaFileUplaodRespuesta', evento );
    }
});
	
function uploadFile(evento){
	<portlet:resourceURL var="uploadFileActionURL" />
	console.log('--------------------File Upload Form--------------------');
	A.io.request("<%= uploadFileActionURL %>", {
		form: {
			id:"<portlet:namespace/>form_adjuntar_documentos",
			upload: true
		},
		dataType:'text',
		on: {
          	 complete: function(data){
             	console.log("Objeto respuesta !!!!!!!!!!!!");
                console.log(data);
                console.log("Objeto respuesta !!!!!!!!!!!!");
                if (data && data.details && data.details.length && data.details.length > 1 && data.details[1].responseText) {
	                var response = JSON.parse(data.details[1].responseText);
	                evento.valido = (response.errorMessage != "");
	                if (!evento.valido){
						console.log("error" + response.errorMessage);
					}
                } else {
                	evento.valido = true;
                }
            	Liferay.fire('validaFileUplaodRespuesta', evento );

            }
        }
	});
}
	
	
</aui:script>