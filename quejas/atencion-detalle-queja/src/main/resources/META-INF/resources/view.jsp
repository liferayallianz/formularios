<%@ include file="/init.jsp" %>

<%@page import="mx.com.allianz.queja.portlet.exception.QuejasException"%>
<%@page import="mx.com.allianz.queja.portlet.exception.QuejasWSException"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="mx.com.allianz.commons.dto.queja.QuejaFlujoDTO"%>
<%@page import="com.google.gson.Gson"%>


<% 
		QuejaFlujoDTO queja = null;
        Object object = request.getAttribute("queja");
		if (object != null && object instanceof QuejaFlujoDTO){
			queja = (QuejaFlujoDTO) object;
		}
		
		
		
%>

<liferay-ui:error exception="<%= QuejasException.class %>" message="send.queja.puc.failed" />
<liferay-ui:error exception="<%= QuejasWSException.class %>" message="send.queja.puc.failed.ws" />

<portlet:actionURL var="quejasSenderEnvioActionURL" name="processActionQuejasSenderEnvio">
</portlet:actionURL>

<portlet:resourceURL var="actualizaDetalleQueja" />
<aui:form name="form_detalle_queja" action="<%=quejasSenderEnvioActionURL.toString()%>">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="cmd_next_page"  />
	<aui:input name="version-datosclientetramitesender" type="hidden" value="1.0.1"  />
	<aui:button-row>
		<aui:button name="saveButtonQueja" type="button" value="quejassenderenvio_Quejassenderenviomvcportlet.send" id="submitBtn" onclick="validateDetalleQuejaForm('cmd_next_page');" />
		<aui:button name="cancelButtonQueja" type="button" value="quejassenderenvio_Quejassenderenviomvcportlet.back" onclick="validateDetalleQuejaForm('cmd_back_page');"  />
	</aui:button-row>
</aui:form>

<script type="text/javascript"> 
console.log('atencion detalle queja v1.0.2');

<% if (queja != null && ( queja.getObservaciones() != null || queja.getQuejaRefiere() != null || queja.getQuejaRelacionada() != null )){ %>
console.log('Cargando datos del detalle de queja');
Liferay.fire('cargaDatosQueja', <%=new Gson().toJson(queja)%> );
<% }%>


var formsValidSectionDetalleQueja = [];

Liferay.provide(window, 'validateDetalleQuejaForm', function(cmd) {
	console.log("validateDatosSolicitudEmpleoForm:"+cmd);
	$("#<portlet:namespace/><%= Constants.CMD %>").val(cmd);
	formsValidSectionDetalleQueja = [];
	Liferay.fire('validaEspecificacionesQueja', {} );
	Liferay.fire('validaFileUplaod', {} );
	//Liferay.fire('validaCaptcha', {} );
	console.log("fire end validateDetalleQuejaForm");
});

Liferay.on('validaEspecificacionesQuejaRespuesta', validaSeccionDetalleQueja);
Liferay.on('validaFileUplaodRespuesta', validaSeccionDetalleQueja);
//Liferay.on('validaCaptchaRespuesta', validaSeccionDetalleQueja);

function validaSeccionDetalleQueja(respuestaSeccion) { 
	console.log("desde la respesta la seccion es" + respuestaSeccion.valido);
	//if (respuestaSeccion.valido) {
		formsValidSectionDetalleQueja.push(respuestaSeccion.valido);
		callServeResourceDetalleQueja(respuestaSeccion);
	//}
}

function sendSubmitQueja() {
	console.log("submit formsValidSectionDetalleQueja.length" + formsValidSectionDetalleQueja.length);
	if(formsValidSectionDetalleQueja.length % 2 === 0) {
		console.log("submit");
		if($("#<portlet:namespace/><%= Constants.CMD %>").val() == 'cmd_next_page'){
			var todosVerdaderos = true;
			for(llave in formsValidSectionDetalleQueja){
				todosVerdaderos = todosVerdaderos && formsValidSectionDetalleQueja[llave];
				if(!todosVerdaderos) return;
				
			}
		}
		new AUI().use(function(A) {
			var formaDetalleQueja = A.one('#<portlet:namespace/>form_detalle_queja');
			console.log("submit formaDetalleQueja" + formaDetalleQueja);
			formaDetalleQueja.submit();
		});
	}
}

function callServeResourceDetalleQueja(seccion) {
	$.post('<%=actualizaDetalleQueja.toString()%>',{
		quejaRelacionada: seccion.quejaRelacionada,
		descripcionQuejaRelacionada: seccion.descripcionQuejaRelacionada,
		quejaRefiere: seccion.quejaRefiere,
		descripcionQuejaRefiere: seccion.descripcionQuejaRefiere,
		comentarios: seccion.comentarios,
	    //	archivo: seccion.archivo,
	    captcha: seccion.captcha
	},function(data, status){
		sendSubmitQueja();
	}); 
}

</script>