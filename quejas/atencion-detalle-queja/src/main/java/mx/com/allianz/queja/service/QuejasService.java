package mx.com.allianz.queja.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import mx.com.allianz.commons.dto.RefiereDto;
import mx.com.allianz.commons.dto.RelacionadaDto;
import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;
import mx.com.allianz.commons.vo.ClasificadorVo;
import mx.com.allianz.service.clasificador.model.Clasificador;
import mx.com.allianz.service.clasificador.service.ClasificadorLocalServiceUtil;
import mx.com.allianz.service.queja.model.Queja;
import mx.com.allianz.service.queja.service.QuejaLocalServiceUtil;
import mx.com.allianz.service.quejaRelacionada.model.QuejaRelacionada;
import mx.com.allianz.service.quejaRelacionada.service.QuejaRelacionadaLocalServiceUtil;

public class QuejasService {
	
	private static Log _log = LogFactoryUtil.getLog(QuejasService.class);

	public QuejaFlujoDTO guardarQueja(QuejaFlujoDTO quejadto) {
		if(_log.isDebugEnabled()) {
			_log.debug("Ingresando a guardarQueja");
			_log.debug("quejadto = " + quejadto);
		}
		
		quejadto.generaClave();
		
		Queja queja = QuejaLocalServiceUtil.createQueja(quejadto.getClave());
		queja.setTipo(quejadto.getTipoContacto());
		queja.setAnio(quejadto.getAnio());
		queja.setFolio(quejadto.getFolio());
		queja.setEsUsted(quejadto.getTipoSolicitante());
		queja.setNumeroPoliza(quejadto.getNumeroPoliza());
		queja.setNombre(quejadto.getNombre());
		queja.setApellidos(quejadto.getApellidoPaterno() + " " + quejadto.getApellidoMaterno());
		queja.setRfc(quejadto.getRfc());
		queja.setRazonSocial(quejadto.getRazonSocial());
		queja.setTelefonoParticular(quejadto.getTelefonoParticular());
		queja.setTelefonoCelular(quejadto.getTelefonoCelular());
		queja.setEmail(quejadto.getEmail());
		queja.setEstado(quejadto.getEstado());
		queja.setQuejaRelacionada(quejadto.getQuejaRelacionada());
		queja.setQuejaRefiere(quejadto.getQuejaRefiere());
		queja.setObservaciones(quejadto.getObservaciones());
		queja.setArchivo(quejadto.getArchivo());
		queja.setFechaRegistro(quejadto.getFechaRegistro());
		queja.setSistemaOrigen(quejadto.getSistemaOrigen());
		queja.setClaveAgente(quejadto.getClaveAgente());
		queja.setNegocio(quejadto.getEmisor());
		queja.setEmisor(quejadto.getEmisor());
		queja.setClaveQuejaRelacionada(quejadto.getClaveQuejaRelacionada());
		queja.setClaveQuejaRefiere(quejadto.getClaveQuejaRefiere());
		queja = QuejaLocalServiceUtil.addQueja(queja);
				
		return quejadto;
	}
	
	public List<RelacionadaDto> recuperaTodosRelacionadoOrdenado() {
		List<RelacionadaDto> result = Optional.ofNullable(QuejaRelacionadaLocalServiceUtil
				.getQuejaRelacionadas(-1, -1)).orElseGet(ArrayList<QuejaRelacionada>::new)
				.stream().map(quejaRel -> {return new RelacionadaDto(quejaRel.getClaveRelacion(), 
				quejaRel.getCodigoProblema(), quejaRel.getDescripcion(), Integer.parseInt(quejaRel.getOrdena()),
				quejaRel.getUstedTiene(), quejaRel.getEsUsted(), new HashSet<RefiereDto>());}).collect(Collectors.toList());
		return result;
	}
	
	public List<ClasificadorVo> recuperarListaDeClasificadores() {
		List<ClasificadorVo> result = Optional.ofNullable(ClasificadorLocalServiceUtil.getClasificadors(-1, -1)).
				orElseGet(ArrayList<Clasificador>::new)
				.stream().map(clasificador -> { return new ClasificadorVo();}).collect(Collectors.toList());
		return result;
	}

}
