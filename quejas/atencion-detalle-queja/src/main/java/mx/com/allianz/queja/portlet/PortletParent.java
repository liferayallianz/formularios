package mx.com.allianz.queja.portlet;

import static javax.portlet.PortletSession.APPLICATION_SCOPE;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.function.Supplier;

import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.xml.namespace.QName;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;

import mx.com.allianz.commons.constants.QueueEvent;
import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;

public interface PortletParent {

	public Log getLog();

	public default <T, X extends Enum & FormularioCamposPadre> T build(PortletRequest request, T type,
			Class<X> enumForm) {
		if (getLog().isDebugEnabled()) {
			getLog().debug("Ingresando a build -> build generic");
			getLog().debug("build " + type.getClass().getName() + " = " + type);
			getLog().debug("enumForm " + enumForm.getName() + " = " + enumForm.getEnumConstants());
		}
		PortletSession session = request.getPortletSession();
		Arrays.stream(enumForm.getEnumConstants())
			  .forEach(campo -> 
			  		campo.build(type, campo.getParamFromSession(session))
			   );
		
		return type;
	}
	
	public default <X extends Enum & FormularioCamposPadre> void cleanParams(PortletRequest request,
			Class<X> enumForm) {
		if (getLog().isDebugEnabled()) {
			getLog().debug("Ingresando a build -> build generic");
			getLog().debug("enumForm " + enumForm.getName() + " = " + enumForm.getEnumConstants());
		}
		PortletSession session = request.getPortletSession();
		Arrays.stream(enumForm.getEnumConstants())
			  .forEach(campo -> 
			  		session.removeAttribute(campo.name(), PortletSession.APPLICATION_SCOPE)
			   );
		
	}

	public default <X extends Enum & FormularioCamposPadre> void serve(PortletRequest request, Class<X> enumForm) {
		if (getLog().isDebugEnabled()) {
			getLog().debug("Ingresando a serve -> uploadToSession generic");
			getLog().debug("build " + enumForm.getName() + " = " + enumForm.getEnumConstants());
		}
		
		PortletSession session = request.getPortletSession();
		Arrays.stream(enumForm.getEnumConstants())
			  .forEach(campo -> {
				  campo.uploadToSession(session, campo.getParamFromRequest(request));
			  });
	}

	public default <T> void process(EventRequest request, QueueEvent queueEvent) {
		if (getLog().isDebugEnabled()) {
			getLog().debug("Ingresando a process -> process event generic");
		}
		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		Optional<Event> event = Optional.ofNullable(request)
										.map(EventRequest::getEvent);
		if (event.map(Event::getName)
				 .filter(queueEvent.name()::equals)
				 .isPresent()) {
			event.map(Event::getValue)
				 .filter(val -> (val instanceof String))
				 .map(obj -> (String) obj)
				 .map(json -> gson.fromJson(json, QuejaFlujoDTO.class))
				 .ifPresent(obj -> {
					session.setAttribute(queueEvent.getParam(), obj, PortletSession.APPLICATION_SCOPE);
				 });
		}
		
		if (getLog().isDebugEnabled()) {
			getLog().debug("obj = " + event.map(Event::getValue)
										   .filter(val -> (val instanceof String))
										   .map(obj -> (String) obj)
										   .map(json -> gson.fromJson(json, QuejaFlujoDTO.class)));
		}
	}

	public default <T extends Serializable> void sendQueue(ActionResponse actionResponse, QueueEvent event, T param) {
		if (getLog().isDebugEnabled()) {
			getLog().debug("Ingresando a sendQueue -> sendQueue generic");
			getLog().debug("build " + event + ", param  = " + param);
		}
		QName qName = new QName(event.getQueue(), event.name());
		actionResponse.setEvent(qName, param);
	}

	public default <T> void upload(PortletRequest request, T type, QueueEvent event) {
		if (getLog().isDebugEnabled()) {
			getLog().debug("Ingresando a upload generic");
			getLog().debug("type " + type + ", param  = " + event.getParam());
		}
		request.getPortletSession().setAttribute(event.getParam(), type, APPLICATION_SCOPE);
	}

	public default <T, X extends Enum & FormularioCamposPadre> T getFromSessionOrNew(PortletRequest request, Class<T> type, Supplier<T> emptyfactory,
			Class<X> enumForm, QueueEvent event) {
		PortletSession session = request.getPortletSession();
		T t = Optional.ofNullable(session)
					  .map(s -> s.getAttribute(event.getParam(), APPLICATION_SCOPE))
					  .filter(obj -> obj.getClass().equals(type))
					  .map(obj -> (T) obj)
					  .orElseGet(emptyfactory);
		if (getLog().isDebugEnabled()) {
			getLog().debug("Despues de obtener de session -> getFromSession generic");
			getLog().debug("getFromSessionOrNew " + type.getName() + ", t  = " + t);
		}		
		t = build(request, t, enumForm);
    	upload(request, t, event);
    	

    	return t;
	}
	
	public default <T extends Serializable> void sendQueueAndRedirect(ActionResponse actionResponse, QueueEvent event, T param, String redirectURL) throws IOException {
		sendQueue(actionResponse, event, param);			
		actionResponse.sendRedirect(redirectURL);
	}
	
	public default boolean invalidRequest(PortletRequest request, long delay){
		return Optional.ofNullable(request.getPortletSession().getAttribute("invalidRequestQueja", 
				PortletSession.APPLICATION_SCOPE))
				.map(param -> (Long) param)
				.filter(lastTime -> (lastTime + delay) > new Date().getTime())
				.isPresent();
	}
	
}
