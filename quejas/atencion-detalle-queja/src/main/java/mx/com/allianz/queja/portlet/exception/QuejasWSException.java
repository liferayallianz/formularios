package mx.com.allianz.queja.portlet.exception;

public class QuejasWSException extends Exception {

	private static final long serialVersionUID = 6788137864432607885L;

	public QuejasWSException() {
		super();
	}

	public QuejasWSException(String message) {
		super(message);
	}

	public QuejasWSException(Throwable cause) {
        super(cause);
    }
}
