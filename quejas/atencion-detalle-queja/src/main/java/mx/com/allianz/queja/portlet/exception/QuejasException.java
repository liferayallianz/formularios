package mx.com.allianz.queja.portlet.exception;

public class QuejasException extends Exception {

	private static final long serialVersionUID = 6788137864432607885L;

	public QuejasException() {
		super();
	}

	public QuejasException(String message) {
		super(message);
	}

	public QuejasException(Throwable cause) {
        super(cause);
    }
}
