package mx.com.allianz.queja.service;

import java.io.File;
import java.util.Calendar;
import java.util.Optional;
import java.util.StringJoiner;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import mx.com.allianz.commons.constants.ConstantesCadena;
import mx.com.allianz.commons.constants.EncodingUtil;
import mx.com.allianz.commons.constants.FileUtil;
import mx.com.allianz.commons.constants.TipoClasificador;
import mx.com.allianz.commons.constants.TipoContacto;
import mx.com.allianz.commons.constants.TipoQueja;
import mx.com.allianz.commons.constants.TipoSeparador;
import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;

public class QuejasFachada {
	
	
	public QuejaFlujoDTO generaOrdendTrabajoPUC(QuejaFlujoDTO quejaDTO) {
		// Obtiene las variables de quejas que se ocupan para generar valores
		String tipoContacto = quejaDTO.getTipoContacto();
		String emisor = quejaDTO.getEmisor();
		String quejaRelacionada = quejaDTO.getQuejaRelacionada();
		String carpeta = quejaDTO.getCarpeta();
		if(_log.isDebugEnabled()) {
			_log.debug("Inregresando a generaOrdendTrabajoPUC");
			_log.debug("tipoContacto = " + tipoContacto);
			_log.debug("emisor = " + emisor);
			_log.debug("quejaRelacionada = " + quejaRelacionada);
			_log.debug("carpeta = " + carpeta);
		}

		TipoContacto tipoContactoEnum = TipoContacto.valueOf(Optional.ofNullable(tipoContacto).filter(tc ->  !"".equals(tc)).orElse("Queja"));
		_log.debug("tipoContactoEnum = " + tipoContactoEnum);
		
		if(quejaDTO.getQuejaRelacionada() == null || "".equals(quejaDTO.getQuejaRelacionada() )) {
			quejaDTO.setQuejaRelacionada("5");
			quejaDTO.setDescQuejaRelacionada("Otros");
			quejaDTO.setQuejaRefiere("4");
			quejaDTO.setDescQuejaRefiere("Otros");
		};
		
		System.out.println("queja = " + quejaDTO.toString());
		_log.info("QuejasFachada -> queja = " + quejaDTO.toString());

		// Asigna fecha y a\u00f1io de registro
		asignarFechaRegistroAnio(quejaDTO);
	
		// Asigna archivos cargados al servidor
		quejaDTO.setArchivos(FileUtil.getInstance().getFolderFiles(carpeta));
				
		//asigna un clasificador dependiendo la combinacion de tipo contacto, 
		//producto y queja relacionada
		quejaDTO.setClasificador(generarClasificador(emisor, tipoContactoEnum, 
				quejaRelacionada, quejaDTO));	

		// Llenado de Parametros a enviar al Servicio PUC Quejas		
		// CLAVE, TIPO CONTACTO, A\u00d1IO, FOLIO, 
		// ESUSTED, CLAVE_AGENTE, EMISOR, NEGOCIO,
		// NUMPOL, NOMBRE, 
		// APELLIDOS, RFC, 
		// TEL_PART, TEL_CEL, MAIL, 
		// ESTADO, RELACIONADA, 
		// REFIERE, CLAVE_REL,
		// CLAVE_REF, SIST_ORIGEN, RAZON_SOCIAL
		quejaDTO.setCampos(EncodingUtil.generarListaEncodigParametros.apply(new String[] {
			null,quejaDTO.getTipoContacto(),Integer.toString(quejaDTO.getAnio()), null, 
			quejaDTO.getTipoSolicitante(), quejaDTO.getClaveAgente(), quejaDTO.getEmisor(), null, 
			quejaDTO.getNumeroPoliza(), quejaDTO.getNombre(), 
			quejaDTO.getApellidoPaterno() + " " + quejaDTO.getApellidoMaterno(), quejaDTO.getRfc(), 
			quejaDTO.getTelefonoParticular(), quejaDTO.getTelefonoCelular(), quejaDTO.getEmail(), 
			quejaDTO.getDescripcionEstado(), quejaDTO.getDescQuejaRelacionada(), 
			quejaDTO.getDescQuejaRefiere(), quejaDTO.getQuejaRelacionada(), 
			quejaDTO.getQuejaRefiere(), quejaDTO.getSistemaOrigen(), quejaDTO.getRazonSocial()}));
		// Asigna una clave Flujo cuando es una Queja de otro modo la clave es Sugerencia
		quejaDTO.setClaveFlujo(generaClaveFlujo(tipoContactoEnum, quejaRelacionada));

		if(_log.isDebugEnabled()) {
			_log.debug("Al terminar generaOrdendTrabajoPUC");
			_log.debug("User Name = " + quejaDTO.getUsername());
			_log.debug("Fecha Registro = " + quejaDTO.getFechaRegistro());
			_log.debug("Anio = " + quejaDTO.getAnio());
			_log.debug("Archivo = ");
			quejaDTO.getArchivos().stream().map(File::getName).forEach(_log::debug);
			_log.debug("Clasificador = " + quejaDTO.getClasificador());
			_log.debug("Clave Flujo = " + quejaDTO.getClaveFlujo());
		}

		return quejaDTO;
	}
	
	private void asignarFechaRegistroAnio(QuejaFlujoDTO queja){
		Calendar fechaDeRegistro = Calendar.getInstance();
		queja.setFechaRegistro(fechaDeRegistro.getTime());
		Integer anoFolio = fechaDeRegistro.get(Calendar.YEAR);
		queja.setAnio(anoFolio);
	}
	
	private String generaClaveFlujo(TipoContacto tipoContactoEnum, String quejaRelacionada){
		String tipoContacto = tipoContactoEnum.name();
		String claveFlujo = TipoContacto.QUEJA == tipoContactoEnum ? 
				tipoContacto + quejaRelacionada : tipoContacto;
		return TipoQueja.valueOf(claveFlujo).getTipo();
	}
	
	private String generarClasificador(String emisor, TipoContacto tipoContacto, String quejaRelacionada, QuejaFlujoDTO queja){
		StringJoiner joiner = new StringJoiner(TipoSeparador.SORTER_JOINER.name());
		String clasificador = emisor;

		joiner.add(tipoContacto.name());
		if(TipoContacto.QUEJA == tipoContacto) {
			quejaRelacionada = (quejaRelacionada != null && !quejaRelacionada.trim().isEmpty() ) ? quejaRelacionada : "5";
			joiner.add(quejaRelacionada);
		}
		if(emisor == null || emisor.trim().isEmpty() || 
				TipoContacto.QUEJA != tipoContacto){
			clasificador = TipoClasificador.CALIDAD.name();
			queja.setEmisor(ConstantesCadena.CADENA_VACIA.name());
			queja.setNumeroPoliza(ConstantesCadena.CADENA_VACIA.name());
		}
		joiner.add(clasificador);
		clasificador = joiner.toString();
		return clasificador;
	}
	
	private static Log _log = LogFactoryUtil.getLog(QuejasFachada.class);
}
