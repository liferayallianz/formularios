package mx.com.allianz.queja.portlet;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.ws.WebServiceException;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.api.CRMService;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.exception.CRMServiceException;
import mx.com.allianz.commons.constants.EncodingUtil;
import mx.com.allianz.commons.constants.FileUtil;
import mx.com.allianz.commons.constants.QueueEvent;
import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;
import mx.com.allianz.queja.portlet.configuration.QuejaDetallePortletConfiguration;
import mx.com.allianz.queja.portlet.exception.QuejasException;
import mx.com.allianz.queja.portlet.exception.QuejasWSException;
import mx.com.allianz.queja.service.QuejasFachada;
import mx.com.allianz.service.contacto.api.ContactoService;

@Component(
	configurationPid="mx.com.allianz.queja.portlet.configuration.QuejaDetallePortletConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // fin Se agregan parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Queja Detalle Sender",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-processing-event=detalleQuejaEvent;http://mx-allianz-liferay-namespace.com/events/detalleQuejaEvent",
		"javax.portlet.supported-publishing-event=atencionQuejaEvent;http://mx-allianz-liferay-namespace.com/events/atencionQuejaEvent",
		"javax.portlet.supported-publishing-event=detalleQuejaEvent;http://mx-allianz-liferay-namespace.com/events/detalleQuejaEvent",
		"javax.portlet.supported-publishing-event=folioQuejaEvent;http://mx-allianz-liferay-namespace.com/events/folioQuejaEvent",
		"javax.portlet.supported-publishing-event=inconsistenciaQuejaEvent;http://mx-allianz-liferay-namespace.com/events/inconsistenciaQuejaEvent",

		// Fin se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)


public class QuejassenderenviomvcportletPortlet extends MVCPortlet implements PortletParent {
	
	public static final String CMD_NEXT_PAGE = "cmd_next_page";
	public static final String CMD_BACK_PAGE = "cmd_back_page";
	
	public void processActionQuejasSenderEnvio(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		_log.debug("QuejassenderenviomvcportletPortlet -> En processAction quejas sender method enviar");
		if (!invalidRequest(actionRequest,_configuration.waitRequest())) {
			
			
			
			String currentURL = PortalUtil.getCurrentURL(actionRequest);
			_log.info("QuejassenderenviomvcportletPortlet -> procesando queja -> currentURL = " + currentURL);
			
			Gson gson = new Gson();
			// definicion de llamado de eventos de formulario
			QuejaFlujoDTO queja = getFromSessionOrNew(actionRequest, QuejaFlujoDTO.class,
					QuejaFlujoDTO::new, FormularioDetalleQuejasCampos.class, QueueEvent.QUEJA_DETALLE);
		    
			String uris[] = Arrays.stream(redirectPair())
					  .map(uri -> uri.split(":"))
					  .filter(pairURI -> currentURL.contains(pairURI[0]))
					  .findAny()
					  .orElse(new String[]{"detalle-queja","/web/guest/confirmacion-queja","/web/guest/atencion-queja"});

			
			_log.info("El valor de cmd =" +ParamUtil.getString(actionRequest, Constants.CMD));
			
			if ( CMD_NEXT_PAGE.equals(ParamUtil.getString(actionRequest, Constants.CMD)) ){
				actionRequest.getPortletSession().setAttribute("invalidRequestQueja", new Date().getTime(), PortletSession.APPLICATION_SCOPE);
				
				if(camposObligatoriosInvalidos(queja)){
					_log.info("QuejassenderenviomvcportletPortlet -> procesando queja -> Un caso mas 1");
					sendQueueAndRedirect(actionResponse, QueueEvent.QUEJA_ATENCION_INCONSISTENCY, gson.toJson(queja), uris[2]);
			    } else {
					try {
						enviarQueja(queja);
						cleanParams(actionRequest, FormularioQuejasCamposPrevio.class);
						cleanParams(actionRequest, FormularioDetalleQuejasCampos.class);
						_log.info("QuejassenderenviomvcportletPortlet -> processActionQuejasSenderEnvio ->  queja json = " + gson.toJson(queja));
						sendQueueAndRedirect(actionResponse, QueueEvent.QUEJA_FINAL, gson.toJson(queja), uris[1]);
					} catch (QuejasException | QuejasWSException e) {
						if (_log.isErrorEnabled()) {
							_log.error(e.getMessage());
						}
						if (_log.isInfoEnabled()) {
							_log.info(e.getMessage());
						}
						SessionErrors.add(actionRequest, e.getClass(), e);
						_log.info("QuejassenderenviomvcportletPortlet -> procesando queja -> Error");
	
						String[] currentPair = Arrays.stream(redirectPair())
								  .map(uri -> uri.split(":"))
								  .peek(rurl -> System.out.println("redirectURL pair = [" + rurl[0] + ", " + rurl[1] + ", " + rurl[2] +"]" ))
								  .filter(pairURI -> currentURL.contains(pairURI[0]))
								  .findAny()
								  .orElse(redirectPair()[0].split(":"));
						String[] paths = uris[1].split("/");
						String prefix = paths.length > 2 ? Arrays.stream(Arrays.copyOfRange(paths, 0, paths.length-1)).collect(Collectors.joining("/")) : "" ;

						String redirect = (currentPair[0].split("/").length > 1 ) ? currentPair[0] : prefix + (currentPair[0].contains("/") ? currentPair[0] : ( "/" + currentPair[0]) ) ;
						sendQueueAndRedirect(actionResponse, QueueEvent.QUEJA_DETALLE, gson.toJson(queja), redirect);			
					}
			    }
			}else {
				_log.info("QuejassenderenviomvcportletPortlet -> procesando queja -> Regreso");
				sendQueueAndRedirect(actionResponse, QueueEvent.QUEJA_ATENCION, gson.toJson(queja), "/group/guest/atencion-queja");	
			}
		}
	}
	
	private boolean camposObligatoriosInvalidos(QuejaFlujoDTO queja) {
		return Arrays.asList(queja.getTipoContacto(), queja.getTipoSolicitante(), 
				queja.getNombre(), queja.getApellidoPaterno(),
				queja.getTelefonoParticular(), queja.getEmail())
				.stream()
				.anyMatch(p -> p == null || p.trim().isEmpty());
	}
	
	private void enviarQueja(QuejaFlujoDTO queja) throws QuejasException, QuejasWSException {
		Integer folioPUC = 0;
		// Asigna nombre de usuario de portal
		queja.setUsername(_configuration.userPuc());
		// Asigna sistema origen
		queja.setSistemaOrigen(_configuration.sistemaOrigen());
		new QuejasFachada().generaOrdendTrabajoPUC(queja);
		
		// Envia orden de trabajo a PUC Quejas
		try {
			_log.debug("-------- enviarQueja queja = " + queja);
			folioPUC = crmService.generarOTQuejas(queja.getUsername(), queja.getClaveFlujo(), 
					queja.getClasificador(), queja.getCampos(), 
					EncodingUtil.encondeStringOrDefault.apply(queja.getObservaciones()), 
					queja.getArchivos());
			queja.setFolio(folioPUC);
			queja.generaClave();
		} catch (CRMServiceException | WebServiceException e) {
			e.printStackTrace();
			if (_log.isErrorEnabled()) {
				_log.error("enviarQueja -> catch e = " + e.getMessage());
				_log.error("Tramite -> catch e " + Arrays.stream(e.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.joining("\n")));
			}
			throw new QuejasWSException("Hubo un error al enviar su queja a un sistema remoto, por favor contacte al administrador.");
		}	
		
		//Borra carpeta de archivos
		new Thread(() -> 
			FileUtil.getInstance().deleteFolder(queja.getCarpeta())
		).start();
		
		//Envia Correo de confirmacion
		new Thread(() -> {	
			try{
				if(contactoService != null) {
					contactoService.enviarCorreoQueja(queja);
				} else {
					if (_log.isErrorEnabled()) {
						_log.error("enviarQueja -> contactoService es nulo");
						_log.error("Reiniciar modulo contacto.service.service ");
						_log.error("$ telnet localhost 11311 ");
						_log.error("g! lb | grep contacto.service.service ");
						_log.error("g! stop PID ");
						_log.error("g! start PID ");
						_log.error("g! disconnect ");
						_log.error("YES ");
					}
				}
			} catch(Exception e) { 
				if (_log.isErrorEnabled()) {
					_log.error("enviarQueja -> contactoService es nulo" + e.getMessage());
				}
				try {
					contactoService.sendMail(_configuration.correoSoporte(), _configuration.tituloCorreoSoporte(), e.getMessage());	
				}catch (Exception ex) {
					if (_log.isErrorEnabled()) {
						_log.error("enviarQueja -> contactoService es nulo" + e.getMessage());
					}
				}
			}
		}).start();
		
	}

	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		if(_log.isDebugEnabled()){
			_log.debug("Ingresando a QuejassenderenviomvcportletPortlet -> processEvent");
		}
		try {
			serve(resourceRequest, FormularioDetalleQuejasCampos.class);
		} catch (Exception e) { 
			e.printStackTrace(); 
			if (_log.isErrorEnabled()) {
				_log.error("QuejassenderenviomvcportletPortlet -> serveResource -> catch e ");
				_log.error(e.getMessage());
			}
		}

		super.serveResource(resourceRequest, resourceResponse);
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		
		if ( renderRequest.getPortletSession().getAttribute("queja", PortletSession.APPLICATION_SCOPE) != null ){
			renderRequest.setAttribute("queja", renderRequest.getPortletSession().getAttribute("queja", PortletSession.APPLICATION_SCOPE));
		}
		super.render(renderRequest, renderResponse);
	}
	
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException { 
		if(_log.isDebugEnabled()){
			_log.debug("Ingresando a QuejassenderenviomvcportletPortlet -> processEvent");
		}
		try {
			process(request, QueueEvent.QUEJA_DETALLE);
		} catch (Exception e) { 
			if (_log.isErrorEnabled()) {
				_log.error(e.getMessage());
			}
			e.printStackTrace(); 
		}
	    super.processEvent(request, response);
	}
	    
	@Override
	public Log getLog() {
		return _log;
	}
	
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	protected void setCRMService(CRMService crmService) {
		this.crmService = crmService;
	}

	protected void unsetCRMService(CRMService crmService) {
		this.crmService = null;
	}
	
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	protected void setContactoService(ContactoService contactoService) {
		this.contactoService = contactoService;
	}

	protected void unsetContactoService(ContactoService contactoService) {
		this.contactoService = null;
	}
	
	private String[] redirectPair(){
		return _configuration.redirectPair();
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				QuejaDetallePortletConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info("Configured Queja Detalle (quejassenderenvio - v1.0.1) : \n{ " +
					"sistema origen : " + _configuration.sistemaOrigen() + ", " +
					"user puc : " + _configuration.userPuc() 
					+ " }");
		}
	}
	
	private volatile QuejaDetallePortletConfiguration _configuration;
	private ContactoService contactoService;
	private CRMService crmService;
	private static Log _log = LogFactoryUtil.getLog(QuejassenderenviomvcportletPortlet.class);
}

enum FormularioQuejasCamposPrevio implements FormularioCamposPadre<QuejaFlujoDTO>  {
	TIPO_CONTACTO("tipoContacto"), TIPO_SOLICITANTE("solicitante"), 
	EMISOR_POLIZA("emisorPoliza"),NUMERO_POLIZA("numeroPoliza"), 
	NOMBRE("nombre"), APELLIDO_PATERNO("apellidoPaterno"), 
	APELLIDO_MATERNO("apellidoMaterno"), RAZON_SOCIAL("razonSocial"),
	RFC("rfc"), TELEFONO_PARTICULAR("telefonoParticular"),
	TELEFONO_CELULAR("telefonoCelular"), EMAIL("email"), 
	ESTADO("estado"), ESTADO_DESCRIPCION("estadoDescripcion"),
	CLAVE_AGENTE("claveAgente");
	FormularioQuejasCamposPrevio(String valor) {
        try {
            Field fieldName = getClass().getSuperclass().getDeclaredField("name");
            fieldName.setAccessible(true);
            fieldName.set(this, valor);
            fieldName.setAccessible(false);
        } catch (Exception e) {}
	}
	@Override
	public String getName() {
		return this.name();
	}
	@Override
	public void build(QuejaFlujoDTO type, String param) {}
}