package mx.com.allianz.queja.portlet.configuration;


import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

/**
 * @author Testing Zone
 */
@ExtendedObjectClassDefinition(category = "Productivity")
@Meta.OCD(
		id = "mx.com.allianz.queja.portlet.configuration.QuejaDetallePortletConfiguration",
		localization = "content/Language",
		name = "Configuracion Queja Detalle"
	)
public interface QuejaDetallePortletConfiguration {
	@Meta.AD(
		deflt = "admin.quejas", 
		description = "Usuario PUC",
		required = false
	)
	public String userPuc();
	
	@Meta.AD(
		deflt = "publico", 
		description = "Sistema Origen (Portal) ",
		required = false
	)
	public String sistemaOrigen();

	@Meta.AD(
		deflt = "5000", 
		description = "Request Wait ",
		required = false
	)
	public long waitRequest();
	
	@Meta.AD(
		deflt = "juan.arrieta@allianz.com.mx", 
		description = "Correo Soporte ",
		required = false
	)
	public String correoSoporte();
	
	@Meta.AD(
			deflt = "Falla al enviar correo de quejas o sugerencias ", 
			description = "Titulo correo",
			required = false
		)
		public String tituloCorreoSoporte();
	
	@Meta.AD(
			deflt =   "detalle-queja:/web/guest/confirmacion-queja:/web/guest/atencion-queja,"
					+ "detallep-queja:/group/guest/confirmacionp-queja:/group/guest/atencionp-queja", 
			description = "Mapeo de posible ruta con redireccionamiento",
			required = false
		)
	public String[] redirectPair();
	
}
