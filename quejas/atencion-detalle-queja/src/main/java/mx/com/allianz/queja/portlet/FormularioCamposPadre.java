package mx.com.allianz.queja.portlet;

import static javax.portlet.PortletSession.APPLICATION_SCOPE;
import static mx.com.allianz.commons.constants.FormularioConstants.EMPTY;

import java.util.Optional;
import java.util.function.Predicate;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import com.liferay.portal.kernel.util.ParamUtil;

public interface FormularioCamposPadre<T> {
	public default String getValue(String param, String previous) {
		return Optional.ofNullable(param).orElse(Optional.ofNullable(param).orElse(EMPTY));
	}
	
	public abstract String getName();

	public abstract void build(T type, String param);
	
	public default void uploadToSession(PortletSession session, String parametro){
		Optional.ofNullable(parametro).filter(NOT_EMPTY).ifPresent(value -> session.setAttribute(getName(), value, APPLICATION_SCOPE));
	}
	
	public default String getParamFromRequest(PortletRequest request){
		return Optional.ofNullable(ParamUtil.getString(request, getName())).map(this::tryString).orElse(EMPTY);
	}
	
	public default String getParamFromSession(PortletSession session){
		return Optional.ofNullable(session.getAttribute(getName(), APPLICATION_SCOPE)).map(this::tryString).orElse(EMPTY);
	}

	
	public default Integer tryParseInt(String text) {
			return (text.matches("\\d+")) ? Integer.parseInt(text) : null;
	}
	
	public default String tryString(Object param) {
		 return (param instanceof String) ? (String) param : null;
	}
	
	public static final Predicate<String> NOT_EMPTY = (String it) -> !it.isEmpty();

}
