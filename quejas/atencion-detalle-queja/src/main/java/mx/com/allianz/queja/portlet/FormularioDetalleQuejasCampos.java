package mx.com.allianz.queja.portlet;

import java.lang.reflect.Field;

import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;

public enum FormularioDetalleQuejasCampos implements FormularioCamposPadre<QuejaFlujoDTO> {
	QUEJA_RELACIONADA("quejaRelacionada"), QUEJA_REFIERE("quejaRefiere"), 
	DESCRIPCION_QUEJA_RELACIONADA("descripcionQuejaRelacionada"), DESCRIPCION_QUEJA_REFIERE("descripcionQuejaRefiere"), 
	COMENTARIOS("comentarios"), ARCHIVO("archivo"), 
	CAPTCHA("captcha");
	
	FormularioDetalleQuejasCampos(String valor) {
        try {
            Field fieldName = getClass().getSuperclass().getDeclaredField("name");
            fieldName.setAccessible(true);
            fieldName.set(this, valor);
            fieldName.setAccessible(false);
        } catch (Exception e) {}
	}
	
	public void build(QuejaFlujoDTO queja, String parametro) {
		switch (this) {
		case QUEJA_RELACIONADA:
			queja.setQuejaRelacionada(getValue(parametro, queja.getQuejaRelacionada()));
			break;
		case DESCRIPCION_QUEJA_RELACIONADA:
			queja.setDescQuejaRelacionada(getValue(parametro,queja.getQuejaRelacionada()));
			break;
		case QUEJA_REFIERE:
			queja.setQuejaRefiere(getValue(parametro, queja.getQuejaRefiere()));
			break;
		case DESCRIPCION_QUEJA_REFIERE:
			queja.setDescQuejaRefiere(getValue(parametro, queja.getDescQuejaRefiere()));
			break;
		case COMENTARIOS:
			queja.setObservaciones(getValue(parametro, queja.getObservaciones()));
			break;
		case CAPTCHA:
			queja.setInputCaptcha(getValue(parametro, queja.getInputCaptcha()));
			break;
		default:
			break;
		}
	}

	@Override
	public String getName() {
		return this.name();
	}


}
