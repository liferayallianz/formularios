<%@ include file="/init.jsp"%>

<p class="formularioTituloEncabezado">Seleccione tipo de contacto</p>

<div class="formularioRadioCentrado col-md-12 text-center">
	<aui:input name="version-tipocontacto" type="hidden" value="1.0.1"  />
	<aui:field-wrapper name="">
		<aui:input id="radioQueja" checked="<%=true%>" inlineLabel="right" name="tipoContacto" type="radio" value="queja" label="tipocontacto_Tipocontactomvcportlet.queja" onClick="seleccionQueja();"/> &nbsp;
		<aui:input id="radioSugerencia"  inlineLabel="right" name="tipoContacto" type="radio" value="sugerencia" label="tipocontacto_Tipocontactomvcportlet.sugerencia" onClick="seleccionSugerencia();" />
	</aui:field-wrapper>
</div> <br/>

<script type="text/javascript">


console.log("tipocontacto v1.0.1");
// add valid form
Liferay.on('validaTipoContacto', function(event){
	new AUI().use(function(A) {
		var tipoContacto = A.one('#<portlet:namespace />radioQueja');
		var tipoContactoChecked = tipoContacto.get('checked') ? 'Queja' : 'Sugerencia';

		var eventoRespuesta = {};
		eventoRespuesta.valido = (tipoContacto != '' );
		console.log('validaTipoContacto eventoRespuesta.valido = ' + eventoRespuesta.valido);
		if (eventoRespuesta.valido) {
			eventoRespuesta.tipoContacto = tipoContactoChecked;
		}	
		Liferay.fire('validaTipoContactoRespuesta', eventoRespuesta );
	});
});

Liferay.provide(window, 'seleccionQueja', function() {
	Liferay.fire('mostrarNumeroPoliza', {} );
	Liferay.fire('cambiaTipoContactoQuejaSolicitante', {});
});

Liferay.provide(window, 'seleccionSugerencia', function() {
	Liferay.fire('ocultarNumeroPoliza', {} );
	Liferay.fire('cambiaTipoContactoSolicitudSolicitante', {});
});

Liferay.provide(window, 'estaSeleccionadoSugerencia', function() {
	return document.getElementById('<portlet:namespace />radioSugerencia').checked;
});
</script>