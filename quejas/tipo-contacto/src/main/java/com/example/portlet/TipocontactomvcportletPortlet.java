package com.example.portlet;

import java.io.IOException;
import java.util.Map;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para recibir de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
		// fin Se agregan parametros para recibir de eventos de portlet
		"requires-namespaced-parameters=false",
		"javax.portlet.display-name=Queja Tipo Queja",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-processing-event=tipoContactoEvent;http://mx-allianz-liferay-namespace.com/events/tipoContacto",
		// Fin Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class TipocontactomvcportletPortlet extends MVCPortlet {
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
        System.out.println("tipoContactoEvent Event search!");
	    Event event = request.getEvent();
	    if(event.getName().equals("tipoContactoEvent")){
	    	System.out.println("tipoContactoEvent Event found!");
	        String eventValue = (String) event.getValue();
	        System.out.println("show me that value from the IPC-Event: " + eventValue);
	     }
	     super.processEvent(request, response);
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet tipocontacto - 1.0.1, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(TipocontactomvcportletPortlet.class);
	
}