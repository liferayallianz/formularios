package com.example.portlet.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;


@ExtendedObjectClassDefinition(
		category = "Productivity",
		scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
	)
	@Meta.OCD(
		localization = "content/Language",
		name = "Configuracion Queja Confirmacion",
		id = "com.example.portlet.configuration.QuejaConfirmacionPortletConfiguration"
	)
public interface QuejaConfirmacionPortletConfiguration {
	
	@Meta.AD(
			deflt = "confirmacion-queja:/web/guest/atencion-queja,confirmacionp-queja:/group/guest/atencionp-queja", 
			description = "Mapeo de posible ruta con redireccionamiento",
			required = false
		)
	public String[] redirectPair();
	
}
