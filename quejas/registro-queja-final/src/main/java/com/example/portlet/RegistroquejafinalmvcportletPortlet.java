package com.example.portlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.example.portlet.configuration.QuejaConfirmacionPortletConfiguration;
import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;

@Component(
	configurationPid="com.example.portlet.configuration.QuejaConfirmacionPortletConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // fin parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Queja Confirmacion",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-processing-event=folioQuejaEvent;http://mx-allianz-liferay-namespace.com/events/folioQuejaEvent",
		// Fin se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class RegistroquejafinalmvcportletPortlet extends MVCPortlet {
	public String folio;
	
	@Override
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		String currentURL = PortalUtil.getCurrentURL(actionRequest);
		_log.info("RegistroquejafinalmvcportletPortlet -> procesando queja -> currentURL = " + currentURL);

		String redirectURL = Arrays.stream(redirectPair())
			  .map(uri -> uri.split(":"))
			  .filter(pairURI -> currentURL.contains(pairURI[0]))
			  .map(pairURI -> pairURI[1])
			  .findAny()
			  .orElse("/atencion-queja");
		
		_log.info("RegistroquejafinalmvcportletPortlet -> processAction -> redirectURL = " + redirectURL);
		actionResponse.sendRedirect(redirectURL);
	}
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		_log.debug("RegistroquejafinalmvcportletPortlet -> processEvent ");
		
		try {
			Gson gson = new Gson();
			PortletSession session = request.getPortletSession();
			Optional<Event> event = Optional.ofNullable(request)
											.map(EventRequest::getEvent);
			if (event.map(Event::getName)
					 .filter("folioQuejaEvent"::equals)
					 .isPresent()) {
				event.map(Event::getValue)
					 .filter(val -> (val instanceof String))
					 .map(obj -> (String) obj)
					 .map(json -> gson.fromJson(json, QuejaFlujoDTO.class))
					 .ifPresent(obj -> {
						session.setAttribute("queja", obj, PortletSession.APPLICATION_SCOPE);
					 });
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	    // Se invoca al procesamiento por defecto de GenericPortlet
	     super.processEvent(request, response);
	}

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try {
			PortletSession session = renderRequest.getPortletSession();
	        Gson gson = new Gson();
	        QuejaFlujoDTO queja = Optional.ofNullable(session)
	        						.map(s -> 
	        							  s.getAttribute("queja", PortletSession.APPLICATION_SCOPE))
	        						.filter(o -> (o instanceof QuejaFlujoDTO))
	        						.map(o -> (QuejaFlujoDTO)o)
	        						.orElseGet(() -> {
										QuejaFlujoDTO q = new QuejaFlujoDTO(); 
										q.setClave("NA");
										q.setTipoContacto("Queja");
										return q;	
									});
	        
	        renderRequest.setAttribute("queja", queja);
		} catch (Exception e) {
			e.printStackTrace();
		}
        super.render(renderRequest, renderResponse);
	}
	
	private String[] redirectPair(){
		return _configuration.redirectPair();
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				QuejaConfirmacionPortletConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info("Configured Confirmacion Queja : { " +
					"redirect pair : " + _configuration.redirectPair() +
					" }");
		}
	}
	
	private QuejaConfirmacionPortletConfiguration _configuration ;
	private static Log _log = LogFactoryUtil.getLog(RegistroquejafinalmvcportletPortlet.class);

}