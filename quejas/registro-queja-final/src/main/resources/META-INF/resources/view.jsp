<%@ include file="/init.jsp" %>

<%@page import="mx.com.allianz.commons.dto.queja.QuejaFlujoDTO"%>

<% 
		QuejaFlujoDTO queja = new QuejaFlujoDTO();
        Object object = request.getAttribute("queja");
		if (object != null && object instanceof QuejaFlujoDTO){
			queja = (QuejaFlujoDTO) object;
		}
%>

<portlet:actionURL var="regresarQuejaActionURL" name="processAction">
</portlet:actionURL> 

<div class="formularioConfirmacionCentrado">

<p class="text-left">Agradecemos nos hayas contactado para externar tu <%= queja.getTipoContacto() %>, misma que qued&oacute; registrada con el <%= queja.getClave() %>. </p>

<p class="text-left">En este momento ser&aacute; enviada a la persona correspondiente, si tienes cualquier otra duda puedes contactarnos al 01-800-1111-002 (sin costo), en horarios de oficina de lunes a jueves de 9:00 a 13:30 y de 14:30 a 17:30 hrs y viernes de 9 a 14:30 hrs.</p>

<p class="text-left">Atentamente.</p> 


<p class="text-left">Allianz M&eacutexico.</p>
</div>
<aui:form name="fm" action="<%=regresarQuejaActionURL.toString()%>">
<div style="text-align:center;">
	<aui:button name="saveButton" class="btn btn-default " type="submit" value="Regresar" id="submitBtnQueja"  />
</div>
</aui:form>
