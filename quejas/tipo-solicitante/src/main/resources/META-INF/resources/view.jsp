<%@ include file="/init.jsp" %>

<div id="divTipoSolicitante">
<p class="formularioTituloEncabezado">Seleccione tipo de solicitante</p>

<div class="formularioRadioCentrado col-md-12 text-center" >
	<aui:field-wrapper name="">
		<aui:input id="solicitante_cliente" checked="<%=true%>" inlineLabel="right" name="solicitante" type="radio" value="Cliente" label="tiposolicitante_Tiposolicitantemvcportlet.cliente" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
		<aui:input id="solicitante_asegurado" inlineLabel="right" name="solicitante" type="radio" value="Asegurado" label="tiposolicitante_Tiposolicitantemvcportlet.asegurado" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
		<aui:input id="solicitante_beneficiario" inlineLabel="right" name="solicitante" type="radio" value="Beneficiario" label="tiposolicitante_Tiposolicitantemvcportlet.beneficiario" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
		<aui:input id="solicitante_agente" inlineLabel="right" name="solicitante" type="radio" value="Agente" label="tiposolicitante_Tiposolicitantemvcportlet.agente" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
		<aui:input id="solicitante_proveedor" inlineLabel="right" name="solicitante" type="radio" value="Proveedor" label="tiposolicitante_Tiposolicitantemvcportlet.proveedor" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
		<aui:input id="solicitante_prospecto" inlineLabel="right" name="solicitante" type="radio" value="Prospecto" label="tiposolicitante_Tiposolicitantemvcportlet.prospecto" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
	</aui:field-wrapper>
	<aui:input name="hidden_solicitante_cliente" type="hidden" value="Cliente" />
	<aui:input name="hidden_solicitante_tipoContacto" type="hidden" value="Queja" />
	<aui:input name="version-tiposolicitante" type="hidden" value="1.0.1"  />
</div> 
<br/>
<br/>
<div id="divClaveAgente" style="display:none">
    <aui:input cssClass="formularioCampoTexto" id="idClaveAgente" name="claveAgente" maxlength="9" value="" label="" placeHolder="Clave de Agente*">
    	<aui:validator name="maxLength" errorMessage="La longitud de la clave de agente debe ser m&aacute;ximo a 8">8</aui:validator>
    	<aui:validator name="digits" errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros"/>
    	<aui:validator name="required" errorMessage="El n&uacute;mero de p&oacute;liza es requerido cuando se selecciona un producto">
              function(val) {
                     return A.one('#<portlet:namespace />hidden_solicitante_cliente').get('value') == "Agente";
              }
   		</aui:validator>
    	
    </aui:input>
</div>
</div> 

<script type="text/javascript">

console.log("tiposolicitante v1.0.1");

Liferay.on('cargaTipoSolicitante', function(queja){
	console.log("Cargando datos desde queja bien");
	if( queja ) {
		
		document.getElementById('<portlet:namespace />solicitante_'+queja.tipoSolicitante.toLowerCase()).checked = true
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />solicitante_'+queja.tipoSolicitante.toLowerCase()).set('checked', true);
			if ( queja.tipoSolicitante = 'Agente' ){
				document.getElementById("divClaveAgente").style.display = 'block' ;
				A.one('#<portlet:namespace />hidden_solicitante_cliente').set('value', queja.tipoSolicitante);
				A.one('#<portlet:namespace />idClaveAgente').set('value', queja.claveAgente);
			} else if(queja.tipoSolicitante == 'Prospecto') {
				Liferay.fire('ocultarNumeroPoliza', {});
			}
		});
	}
});

Liferay.on('ocultaTipoSolicitante', function(event){
	console.log("Ocultando tipo solicitante");
	document.getElementById("divTipoSolicitante").style.display = 'none' ;
});


</script>
<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
Liferay.on('cambiaTipoContactoQuejaSolicitante', function(event){
	A.one('#<portlet:namespace />hidden_solicitante_tipoContacto').set('value','Queja');
	console.log('cambiaTipoContactoQuejaSolicitante,  Queja');

});
Liferay.on('cambiaTipoContactoSolicitudSolicitante', function(event){
	A.one('#<portlet:namespace />hidden_solicitante_tipoContacto').set('value','Solicitud');
	console.log('cambiaTipoContactoSolicitudSolicitante,  Solicitud');
});

Liferay.provide(window, 'actualizarTipoSolicitante', function(param) {
	var solicitante = param.value;
	A.one('#<portlet:namespace />hidden_solicitante_cliente').set('value',solicitante);
	
	var tipoContacto = A.one('#<portlet:namespace />hidden_solicitante_tipoContacto').get('value');
	console.log("actualizarTipoSolicitante, tipoContacto = " + tipoContacto);
	if(solicitante == 'Agente') {
		document.getElementById("divClaveAgente").style.display = 'block' ;
	} else {
		document.getElementById("divClaveAgente").style.display = 'none' ;
	}
	
	if(solicitante == 'Prospecto') {
		Liferay.fire('ocultarNumeroPoliza', {});
	} else if(tipoContacto == 'Queja'){
		Liferay.fire('mostrarNumeroPoliza', {});
	}
	
	var idTipoSolicitante = getIdTipoSolicitante(solicitante);
	console.log('idTipoSolicitante = ' + idTipoSolicitante);
	
	Liferay.fire('actualizarNumerosPoliza', {tipoSolicitante:idTipoSolicitante });
});

// add valid form
Liferay.on('validaTipoSolicitante', function(event){
	var solicitante = getTipoSolicitante();
	var eventoRespuesta = {};
	eventoRespuesta.valido = (solicitante != '' );
	console.log('validaTipoSolicitante eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if (eventoRespuesta.valido) {
		
		var claveAgente = A.one('#<portlet:namespace />idClaveAgente');

		eventoRespuesta.claveAgente = claveAgente.get('value');
		eventoRespuesta.solicitante = solicitante;
	}	
	Liferay.fire('validaTipoSolicitanteRespuesta', eventoRespuesta );
});

function getTipoSolicitante() {
	var solicitante = A.one('#<portlet:namespace />hidden_solicitante_cliente').get('value');
	return solicitante;
}

function getIdTipoSolicitante(solicitante){
	var idTipoSolicitante = 1;
	switch(solicitante) {
	case 'Cliente':
		idTipoSolicitante = 1;
		break;
	case 'Asegurado':
		idTipoSolicitante = 2;
		break;
	case 'Beneficiario':
		idTipoSolicitante = 3;
		break;
	case 'Agente':
		idTipoSolicitante = 4;
		break;
	case 'Proveedor':
		idTipoSolicitante = 5;
		break;
	case 'Prospecto':
		idTipoSolicitante = 6;
		break;
	}
	return idTipoSolicitante;
}

function actualizaNumerosPoliza() {
	Liferay.fire('actualizarNumerosPoliza', {tipoSolicitante: -1});
}
setTimeout(actualizaNumerosPoliza, 1000);

</aui:script>


