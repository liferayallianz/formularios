package com.example.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import mx.com.allianz.commons.catalogos.dto.EstadoDTO;
import mx.com.allianz.service.estado.model.Estado;
import mx.com.allianz.service.estado.service.EstadoLocalServiceUtil;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para recibir de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        // fin Se agregan parametros para recibir de eventos de portlet
		// Se agregan parametros para compartir atributos de request en la session
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        "com.liferay.portlet.remoteable=true",
		"com.liferay.portlet.preferences-unique-per-layout=true",
		// Fin parametros para compartir atributos de request en la session
		"javax.portlet.display-name=Commons Estado Contacto",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class EstadocontactomvcportletPortlet extends MVCPortlet {
	public static final int CODIGO_PAIS_MEXICO=412;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try {

		List<EstadoDTO> estados = Optional.ofNullable(EstadoLocalServiceUtil.getEstados(-1,-1)).orElseGet(ArrayList<Estado>::new).stream().filter
			(estado -> estado.getCodigoPais() == CODIGO_PAIS_MEXICO).map(estado ->
			new EstadoDTO(estado.getCodigoPais(), estado.getCodigoEstado(),
			estado.getDescripcion())).sorted(Comparator.comparing(EstadoDTO::getDescripcion))
			.collect(Collectors.toList());
		renderRequest.setAttribute("estados", estados);
		} catch (Exception e ) { e.printStackTrace(); }

		super.render(renderRequest, renderResponse);
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet datos.contacto.persona.web - 1.0.1, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(EstadocontactomvcportletPortlet.class);
}