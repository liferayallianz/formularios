<%@ include file="/init.jsp" %>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mx.com.allianz.commons.catalogos.dto.EstadoDTO"%>

<% 
	List<EstadoDTO> estados = new ArrayList<>();
    Object object = request.getAttribute("estados");
	if (object != null && object instanceof List) {
		estados = (List<EstadoDTO>) object;
	}
%>
<aui:form action="#" name="fm_estado_contacto">
	<aui:input name="version-estadocontacto" type="hidden" value="1.0.1"  />
	<aui:select cssClass="formularioCampoTexto" id="estado_contacto_estado" name="estado" label="">
	    <aui:option selected="true" value="">
	        <liferay-ui:message key="estado.contacto.estado.contacto" />
	    </aui:option>
	    <% 
	    		for(EstadoDTO estado: estados){
	   	%>
     	<aui:option value="<%=estado.getCodigoEstado()%>"><%=estado.getDescripcion()%></aui:option>
	   	<% 	   
		   };
		%>
	</aui:select>
</aui:form>

<script type="text/javascript">

console.log("estadocontacto v1.0.1");

Liferay.on('cargaEstadoContacto', function(queja){
	console.log("Cargando datos desde queja bien");
	if( queja ) {
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />estado_contacto_estado').set('value', queja.estado);
		});
	}
});
</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
var formValidator = Liferay.Form.get('<portlet:namespace />fm_estado_contacto').formValidator;
// add valid form
Liferay.on('validaEstadoContacto', function(event){
	formValidator.validate();
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaEstadoContacto eventoRespuesta.valido = ' + eventoRespuesta.valido);

	if (eventoRespuesta.valido) {
		var estadoContactoSelect = A.one('#<portlet:namespace />estado_contacto_estado');
		eventoRespuesta.estado = estadoContactoSelect.get('value');
		eventoRespuesta.estadoDescripcion = (eventoRespuesta.estado) ? estadoContactoSelect.get('options')._nodes[estadoContactoSelect.get('selectedIndex')].text : '';
	}	
	Liferay.fire('validaEstadoContactoRespuesta', eventoRespuesta );
});
</aui:script>