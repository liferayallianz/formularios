<%@ include file="/init.jsp" %>

<p class="formularioTituloEncabezado">Especificaciones</p>
<aui:form name="form_especificaciones_queja" action="#">

<aui:input name="version-datosqueja" type="hidden" value="1.0.1"  />

<p class="formularioEtiquetaTexto"><liferay-ui:message key="datosqueja_Datosquejamvcportlet.tipoQueja" /></p>    
<aui:select cssClass="formularioCampoTexto"  label="" name="quejaRelacionada" required="true">
		<aui:option value=""><liferay-ui:message key="Selecciona una opci&oacute;n" /></aui:option>
		<aui:option value="1"><liferay-ui:message key="Problemas en la atenci&oacute;n de un siniestro" /></aui:option>
		<aui:option value="2"><liferay-ui:message key="Problemas con tu p&oacute;liza" /></aui:option>
		<aui:option value="3"><liferay-ui:message key="Problemas con el pago de tu p&oacute;liza / cobranza" /></aui:option>
		<aui:option value="4"><liferay-ui:message key="Problemas con tu recibo" /></aui:option>
		<aui:option value="5"><liferay-ui:message key="Otros" /></aui:option>
</aui:select>

<p class="formularioEtiquetaTexto"><liferay-ui:message key="datosqueja_Datosquejamvcportlet.especificacionQueja" /></p>    
<aui:select cssClass="formularioCampoTexto" label="" name="quejaRefiere" required="true">
		<aui:option value=""><liferay-ui:message key="Selecciona una opci&oacute;n" /></aui:option>
		<aui:option value="1"><liferay-ui:message key="No estoy de acuerdo en la respuesta en tiempo y forma de mi tr&aacute;mite" /></aui:option>
		<aui:option value="2"><liferay-ui:message key="Problemas en el flujo de atenci&oacute;n de un ajustador o m&eacute;dico dictaminador, red m&eacute;dica o pago a proveedores" /></aui:option>
		<aui:option value="3"><liferay-ui:message key="Problemas de servicio con el Call Center" /></aui:option>
		<aui:option value="4"><liferay-ui:message key="Otros" /></aui:option>
</aui:select>
<br>
<p class="formularioEtiquetaTexto">Por favor especifica de que se trata tu queja y el producto con el que cuentas para que pueda ser atendida por la persona id&oacute;nea</p>
<br>
<aui:input cssClass="formularioCampoTexto" type="textarea" label="" placeholder="datosqueja_Datosquejamvcportlet.comentarios" name="comentarios" />
</aui:form>

<script type="text/javascript">

Liferay.on('cargaDatosQueja', function(queja){
	console.log("Cargando datos desde queja");
	if( queja ) {
		$('#<portlet:namespace />quejaRefiere').val(queja.quejaRefiere);
		$('#<portlet:namespace />quejaRelacionada').val(queja.quejaRelacionada);
		$('#<portlet:namespace />comentarios').val(queja.observaciones);
// 		new AUI().use(function(A) {
// 			console.log( A );
			
// 			A.one('#<portlet:namespace />quejaRefiere').set('value', queja.quejaRefiere);
// 			A.one('#<portlet:namespace />quejaRelacionada').set('value', queja.quejaRelacionada);
// 			A.one('#<portlet:namespace />comentarios').set('value', queja.comentarios);
// 		});
	}
});
</script>

<script type="text/javascript">
console.log("datosqueja v1.0.1");
// add valid form
Liferay.on('validaEspecificacionesQueja', function(event){
	new AUI().use(function(A) {
		var eventoRespuesta = {};
		eventoRespuesta.valido = true;
		console.log('validaEspecificacionesQueja eventoRespuesta.valido = ' + eventoRespuesta.valido);
		if (eventoRespuesta.valido) {
			var quejaRel = A.one('#<portlet:namespace />quejaRelacionada');
			var quejaRef = A.one('#<portlet:namespace />quejaRefiere');

			eventoRespuesta.quejaRelacionada = quejaRel.get('value');
			var selectedQuejaRelacionada = quejaRel.get('selectedIndex');
			eventoRespuesta.descripcionQuejaRelacionada = selectedQuejaRelacionada != "" ? quejaRel.get('options')._nodes[selectedQuejaRelacionada].text : "";
			eventoRespuesta.quejaRefiere = quejaRef.get('value');
			var selectedQuejaRefiere = quejaRef.get('selectedIndex');
			eventoRespuesta.descripcionQuejaRefiere = selectedQuejaRefiere != "" ? quejaRef.get('options')._nodes[quejaRef.get('selectedIndex')].text : "";
			eventoRespuesta.comentarios = A.one('#<portlet:namespace />comentarios').get('value');
			console.log("eventoRespuesta.descripcionQuejaRelacionada = " + eventoRespuesta.descripcionQuejaRelacionada);
			console.log("eventoRespuesta.descripcionQuejaRefiere = " + eventoRespuesta.descripcionQuejaRefiere);

		}	
		Liferay.fire('validaEspecificacionesQuejaRespuesta', eventoRespuesta );
	});
});
</script>