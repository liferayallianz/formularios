<%@ include file="/init.jsp" %>
<%@page import="mx.com.allianz.quejas.sender.portlet.exception.InconsistenciaQuejaException"%>

<liferay-ui:error exception="<%= InconsistenciaQuejaException.class %>" message="mx.com.allianz.quejas.sender.portlet.inconsistencia.error.message" />

<portlet:actionURL var="atencionQuejasURL" name="processAction">
</portlet:actionURL>
<portlet:resourceURL var="actualizaQueja" />


<% 
		Boolean clienteHabilitado = false;
        Object object = request.getAttribute("quejaClienteHabilitado");
		if (object != null && object instanceof Boolean){
			clienteHabilitado = (Boolean) object;
		}
		
		String clienteFirmado = "";
        Object object2 = request.getAttribute("clienteFirmado");
		if (object2 != null && object2 instanceof String){
			clienteFirmado = (String) object2;
		}
		
		String queja = "";
        Object object3 = request.getAttribute("queja");
		if (object3 != null && object3 instanceof String){
			queja = (String) object3;
		}
		
		String currentPageURL = "/atencion-queja";
        Object object4 = request.getAttribute("currentPageURL");
		if (object4 != null && object4 instanceof String){
			currentPageURL = (String) object4;
		}
		
		String objetoRec = "null";
		Object object5 = request.getAttribute("quejaRec");
		if (object4 != null && object4 instanceof String){
			objetoRec = (String) object5;
		}
		
%>

<aui:form name="form_quejas" method="post"  action="<%=atencionQuejasURL.toString()%>">
    	<aui:input name="version-atencion.quejas" type="hidden" value="1.0.1"  />
    	<aui:button-row>
        		<aui:button class="btn pull-right" name="nextStep" type="button" value="atencion_quejas_AtencionQuejas.siguiente" onclick="validateAtencionQuejaForm();" />
        		<aui:button class="btn pull-right" name="cancelButton" type="button" value="atencion_quejas_AtencionQuejas.clean" onclick="currentPageURLJSCleanQueja();" />
    	</aui:button-row>
   		<aui:input name="queja_cliente_habilitado_hidden" type="hidden" value="<%=clienteHabilitado %>" />
</aui:form>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log('atencion.quejas v1.0.1');

var tramiteClienteHabilitadoJS = <%=clienteHabilitado%>;
console.log("atencion clienteFirmado");
console.log(tramiteClienteHabilitadoJS);
console.log(clienteQuejaJS);
var quejaRec = <%=objetoRec%>;

if(tramiteClienteHabilitadoJS) {
	var clienteQuejaJS = <%=clienteFirmado%>;
	if ( clienteQuejaJS && !clienteQuejaJS.nombre && clienteQuejaJS.productos)
		clienteQuejaJS.nombre = clienteQuejaJS.productos[0].contratante.nombre;
	var quejaJS = <%=queja%>;
	console.log(quejaJS);
	
	if(!quejaRec && clienteQuejaJS){
		console.log("disparando cargaDatosContactoDeCliente");
		Liferay.fire('cargaDatosContactoDeCliente', clienteQuejaJS);
		console.log("disparando cargaDatosPersonalesDeCliente");
		Liferay.fire('cargaDatosPersonalesDeCliente', clienteQuejaJS);
		console.log("disparando cliente queja habilitado");
		Liferay.fire('ocultaTipoSolicitante', quejaJS);
	}
	console.log("disparando cliente queja habilitado");
	Liferay.fire('ocultaTipoSolicitante', {});
}

if(quejaRec){
	console.log("Se recuperan datos ya ingresados -> quejaRecuperada");
	
	console.log("disparando cargaDatosContactoDeCliente");
	Liferay.fire('cargaDatosContactoDeQueja', quejaRec);
	console.log("disparando cargaDatosPersonalesDeCliente");
	Liferay.fire('cargaDatosPersonalesDeCliente', quejaRec);
	console.log("disparando cargaDatosFiscales");
	Liferay.fire('cargaDatosFiscales', quejaRec);
	if (clienteQuejaJS){
		var productos = clienteQuejaJS.productos;
		for ( var i = 0; i< productos.length; i++ ){
			if ( quejaRec.emisor == productos[i].codigoProducto )
				productos[i].selected = true;
		}
		console.log("disparando cliente queja habilitado");
		Liferay.fire('clienteQuejaHabilitado', clienteQuejaJS);
	}
	console.log("disparando cargaEstadoContacto");
	Liferay.fire('cargaEstadoContacto', quejaRec);
	console.log("disparando cargaTipoSolicitante");
	Liferay.fire('cargaTipoSolicitante', quejaRec);
} 

var currentPageURLJSQueja = '<%=currentPageURL%>';

Liferay.provide(window, 'currentPageURLJSCleanQueja', function() {
	console.log('currentPageURLJSCleanQueja');
	console.log(currentPageURLJSQueja);
	var redirQueja = currentPageURLJSQueja ;
	window.location.href = redirQueja;

});


var formsValidSectionQueja = [];

Liferay.provide(window, 'validateAtencionQuejaForm', function() {
	console.log("validateAtencionQuejaForm");
	formsValidSectionQueja = [];
	Liferay.fire('validaTipoContacto', {} );
	Liferay.fire('validaTipoSolicitante', {} );
	Liferay.fire('validaNumeroPoliza', {} );
	Liferay.fire('validaDatosPersonales', {} );
	Liferay.fire('validaDatosFiscales', {} );
	Liferay.fire('validaDatosContacto', {} );
	Liferay.fire('validaEstadoContacto', {} );
	console.log("fire end validateAtencionQuejaForm");
});

Liferay.on('validaTipoContactoRespuesta', validaSeccionQueja);
Liferay.on('validaTipoSolicitanteRespuesta', validaSeccionQueja);
Liferay.on('validaNumeroPolizaRespuesta', validaSeccionQueja);
Liferay.on('validaDatosPersonalesRespuesta', validaSeccionQueja);
Liferay.on('validaDatosFiscalesRespuesta', validaSeccionQueja);
Liferay.on('validaDatosContactoRespuesta', validaSeccionQueja);
Liferay.on('validaEstadoContactoRespuesta', validaSeccionQueja);

function validaSeccionQueja(respuestaSeccion) { 
	console.log("desde la respesta la seccion es" + respuestaSeccion.valido);
	if (respuestaSeccion.valido) {
		formsValidSectionQueja.push(respuestaSeccion.valid);
		callServeResourceQueja(respuestaSeccion);
		//sendSubmit();
	}
}

function sendSubmitQueja() {
	console.log("submit formsValidSectionQueja.length" + formsValidSectionQueja.length);
	if(formsValidSectionQueja.length % 7 === 0) {
		console.log("submit");
			var formaQueja = A.one('#<portlet:namespace/>form_quejas');
			console.log("submit formaQueja" +formaQueja);
			formaQueja.submit();
	}
}

function callServeResourceQueja(seccion) {
	
	 A.io.request (
			 '<%=actualizaQueja.toString()%>', {
	         data: {
	        	 tipoContacto: seccion.tipoContacto,
	     		solicitante: seccion.solicitante,
	         	emisorPoliza: seccion.emisorPoliza,
	         	numeroPoliza: seccion.numeroPoliza,
	         	nombre: seccion.nombre,
	         	apellidoPaterno: seccion.apellidoPaterno,
	         	apellidoMaterno: seccion.apellidoMaterno,
	         	razonSocial: seccion.razonSocial,
	         	rfc: seccion.rfc,
	     		telefonoParticular: seccion.telefonoParticular,
	     		telefonoCelular: seccion.telefonoCelular,
	     		email: seccion.email,
	     		estado: seccion.estado,
	     		estadoDescripcion: seccion.estadoDescripcion,
	     		claveAgente: seccion.claveAgente
	         },
	         dataType: 'json',
	         on: {
	                 failure: function() {
	                     console.log("Ajax failed! There was some error at the server");
	                 },
	                 success: function(event, id, obj) {
	             		sendSubmitQueja();

	                }
	             }
	         }); 

}

</aui:script>

