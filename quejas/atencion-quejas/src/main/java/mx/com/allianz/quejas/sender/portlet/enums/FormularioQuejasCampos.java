package mx.com.allianz.quejas.sender.portlet.enums;

import static javax.portlet.PortletSession.APPLICATION_SCOPE;
import static mx.com.allianz.commons.constants.FormularioConstants.EMPTY;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.function.Predicate;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;

import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;

public enum FormularioQuejasCampos {
	TIPO_CONTACTO("tipoContacto"), TIPO_SOLICITANTE("solicitante"), 
	EMISOR_POLIZA("emisorPoliza"),NUMERO_POLIZA("numeroPoliza"), 
	NOMBRE("nombre"), APELLIDO_PATERNO("apellidoPaterno"), 
	APELLIDO_MATERNO("apellidoMaterno"), RAZON_SOCIAL("razonSocial"),
	RFC("rfc"), TELEFONO_PARTICULAR("telefonoParticular"),
	TELEFONO_CELULAR("telefonoCelular"), EMAIL("email"), 
	ESTADO("estado"), ESTADO_DESCRIPCION("estadoDescripcion"),
	CLAVE_AGENTE("claveAgente");
	
	FormularioQuejasCampos(String valor) {
        try {
            Field fieldName = getClass().getSuperclass().getDeclaredField("name");
            fieldName.setAccessible(true);
            fieldName.set(this, valor);
            fieldName.setAccessible(false);
        } catch (Exception e) {}
	}
	
	public void build(QuejaFlujoDTO queja, String parametro) {
		switch (this) {
		case TIPO_CONTACTO:
			queja.setTipoContacto(getValue(parametro));
			break;
		case TIPO_SOLICITANTE:
			queja.setTipoSolicitante(getValue(parametro));
			break;
		case EMISOR_POLIZA:
			queja.setEmisor(getValue(parametro));
			break;
		case NUMERO_POLIZA:
			queja.setNumeroPoliza(getValue(parametro));
			break;
		case NOMBRE:
			queja.setNombre(getValue(parametro));
			break;
		case APELLIDO_PATERNO:
			queja.setApellidoPaterno(getValue(parametro));
			break;
		case APELLIDO_MATERNO:
			queja.setApellidoMaterno(getValue(parametro));
			break;
		case RAZON_SOCIAL:
			queja.setRazonSocial(getValue(parametro));
			break;
		case RFC:
			queja.setRfc(getValue(parametro));
			break;
		case TELEFONO_PARTICULAR:
			queja.setTelefonoParticular(getValue(parametro));
			break;
		case TELEFONO_CELULAR:
			queja.setTelefonoCelular(getValue(parametro));
			break;
		case EMAIL:
			queja.setEmail(getValue(parametro));
			break;
		case ESTADO:
			queja.setEstado(getValue(parametro));
			break;
		case ESTADO_DESCRIPCION:
			queja.setDescripcionEstado(getValue(parametro));
			break;
		case CLAVE_AGENTE:
			queja.setClaveAgente(getValue(parametro));
			break;
		default:
			break;
		}
	}
	
	private String getValue(String param) {
		return Optional.ofNullable(param).orElse(EMPTY);
	}
	
	public void uploadToSession(PortletSession session, String parametro){
		Optional.ofNullable(parametro).filter(NOT_EMPTY).ifPresent(value -> session.setAttribute(this.name(), value, APPLICATION_SCOPE));
	}
	
	public String getParamFromRequest(ResourceRequest resourceRequest){
		return Optional.ofNullable(ParamUtil.getString(resourceRequest, this.name())).map(this::tryString).orElse(EMPTY);
	}
	
	public String getParamFromSession(PortletSession session){
		return Optional.ofNullable(session.getAttribute(this.name(), APPLICATION_SCOPE)).map(this::tryString).orElse(EMPTY);
	}
	
	public Integer tryParseInt(String text) {
			return (text.matches("\\d+")) ? Integer.parseInt(text) : null;
	}
	
	public String tryString(Object param) {
		 return (param instanceof String) ? (String) param : null;
	}
	
	public static final Predicate<String> NOT_EMPTY = (String it) -> !it.isEmpty();


}
