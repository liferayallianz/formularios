package mx.com.allianz.quejas.sender.portlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.Cookie;
import javax.xml.namespace.QName;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.commons.constants.FileUtil;
import mx.com.allianz.commons.dto.cliente.ClienteDTO;
import mx.com.allianz.commons.dto.cliente.ClienteTramiteDTO;
import mx.com.allianz.commons.dto.cliente.ContactoDTO;
import mx.com.allianz.commons.dto.cliente.CorreoDTO;
import mx.com.allianz.commons.dto.cliente.ProductoClienteDTO;
import mx.com.allianz.commons.dto.cliente.TelefonoDTO;
import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;
import mx.com.allianz.module.service.facade.AllianzService;
//import mx.com.allianz.module.service.facade.AllianzService;
import mx.com.allianz.quejas.sender.portlet.configuration.AntencionQuejaConfiguration;
import mx.com.allianz.quejas.sender.portlet.enums.FormularioQuejasCampos;
import mx.com.allianz.quejas.sender.portlet.exception.InconsistenciaQuejaException;

@Component(
	configurationPid="mx.com.allianz.quejas.sender.portlet.configuration.AntencionQuejaConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-session-attributes=false",
        // fin parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Queja Atencion Sender",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agrega nombre de mensaje y cola del portlet para publicar
		"javax.portlet.supported-processing-event=atencionQuejaEvent;http://mx-allianz-liferay-namespace.com/events/atencionQuejaEvent",
		"javax.portlet.supported-processing-event=inconsistenciaQuejaEvent;http://mx-allianz-liferay-namespace.com/events/inconsistenciaQuejaEvent",
        "javax.portlet.supported-publishing-event=detalleQuejaEvent;http://mx-allianz-liferay-namespace.com/events/detalleQuejaEvent",
		// Fin Se agrega nombre de mensaje y cola del portlet para publicar
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class AtencionQuejasPortlet extends MVCPortlet {

	public static final String LLAVE_CURRENT_PAGE_URL = "currentPageURL"; 
	public static final String LLAVE_QUEJA = "queja"; 
	public static final String LLAVE_QUEJA_REC = "quejaRec"; 
	public static final String LLAVE_CLIENTE_FIRMADO = "clienteFirmado";
	public static final String LLAVE_ATENCION_QUEJA_EVENT = "atencionQuejaEvent";
	public static final String LLAVE_QUEJA_CLIENTE_HABILITADO = "quejaClienteHabilitado"; 
	public static final String LLAVE_INCONSISTENCIA_EVENT = "inconsistenciaQuejaEvent";
	public static final String LLAVE_INCONSISTENCIA_ERROR = "inconsistenciaQuejaError";
	public static final String PRODUCTO_ACTIVO = "ACTIVA"; 
	public static final String CONSULTA_EXITOSA_CLIENTE = "\"estatus\":\"true\"";
	public static final String LADA_CELULAR = "044"; 
	public static final String TIPO_TELEFONO_PARTICULAR = "P"; 
	public static final String TIPO_TELEFONO_CELULAR = "C"; 


	@Override
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
			// definicion de llamado de eventos de formulario
		_log.debug("QuejassenderenviomvcportletPortlet -> En processAction quejas sender method enviar");
		if (!invalidRequest(actionRequest,5000)) {

			Gson gson = new Gson();
			String currentURL = PortalUtil.getCurrentURL(actionRequest);
			_log.info("AtencionQuejasPortlet -> procesando queja -> currentURL = " + currentURL);

			String redirectURL = Arrays.stream(redirectPair())
					.map(uri -> uri.split(":"))
					.filter(pairURI -> currentURL.contains(pairURI[0]))
					.map(pairURI -> pairURI[1])
					.findAny()
					.orElse("/detalle-queja");
			
			_log.info("AtencionQuejasPortlet -> processAction -> redirectURL = " + redirectURL);
			
		    QuejaFlujoDTO queja = buildContact(actionRequest);
		    actionRequest.getPortletSession().removeAttribute(LLAVE_QUEJA_REC, PortletSession.APPLICATION_SCOPE);
		    QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/detalleQuejaEvent", "detalleQuejaEvent");
			actionResponse.setEvent(qName, gson.toJson(queja));
		    
			new Thread(() -> 
				FileUtil.getInstance().cleanOldFiles(_configuration.rootPath(), _configuration.waitClean())
			).start();
			
			_log.info("queja  = " + queja);

			actionResponse.sendRedirect(redirectURL);
		}
	}
	
	public boolean invalidRequest(PortletRequest request, long delay){
		return Optional.ofNullable(request.getPortletSession().getAttribute("invalidRequestQueja", 
				PortletSession.APPLICATION_SCOPE))
				.map(param -> (Long) param)
				.filter(lastTime -> (lastTime + delay) > new Date().getTime())
				.isPresent();
	}
	
	private QuejaFlujoDTO buildContact(ActionRequest actionRequest) {
		PortletSession session = actionRequest.getPortletSession();
		final QuejaFlujoDTO  queja = session.getAttribute(LLAVE_QUEJA_REC, PortletSession.APPLICATION_SCOPE) != null ?
				(QuejaFlujoDTO)session.getAttribute(LLAVE_QUEJA_REC, PortletSession.APPLICATION_SCOPE) : new QuejaFlujoDTO();
		Arrays.stream(FormularioQuejasCampos.values()).forEach(campo -> 
			campo.build(queja, campo.getParamFromSession(session))
		);
		queja.setCarpeta(FileUtil.getInstance().newFolder(_configuration.rootPath()));;
		
		return queja;
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		
		PortletSession session = resourceRequest.getPortletSession();
		Arrays.stream(FormularioQuejasCampos.values())
			  .forEach(campo -> 
			  		campo.uploadToSession(session, campo.getParamFromRequest(resourceRequest))
			  );
		
		super.serveResource(resourceRequest, resourceResponse);
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		Gson gson = new Gson();
		_log.info("Atencion queja -> render-> inicio");
		PortletSession session = renderRequest.getPortletSession();			
		if (isClientEnabled()) {
			if (allianzService != null) {
				QuejaFlujoDTO queja = Optional.ofNullable(renderRequest.getPortletSession()
						.getAttribute(LLAVE_QUEJA, PortletSession.APPLICATION_SCOPE))
								  .filter(p -> p instanceof String)
								  .map( p -> (String) p)
								  .map(json -> gson.fromJson((String) json, QuejaFlujoDTO.class))
								  .orElseGet(QuejaFlujoDTO::new);
				
				Arrays.stream(Optional.ofNullable(renderRequest.getCookies())
									  			.orElseGet(() -> new Cookie[0]))
								.filter(cookie -> allianzClientToken().equals(cookie.getName()))
								.map(Cookie::getValue)
								.findAny()
								.ifPresent(cookie -> {
									Optional.ofNullable(allianzService.obtenerDatosTestingZone(cookie))
											.filter(c -> c.contains(CONSULTA_EXITOSA_CLIENTE))
											.map(clienteV -> {_log.info("cliente = " + clienteV); return clienteV;})
											.map(json -> gson.fromJson(json, ClienteTramiteDTO.class))
											.ifPresent(cliente -> {
												_log.info("cliente parseado = " + cliente);
												session.setAttribute(LLAVE_CLIENTE_FIRMADO, 
														gson.toJson(cliente.getCliente()), PortletSession.APPLICATION_SCOPE);
												_log.info("AtencionQuejasPortlet -> render -> cliente = " + queja);

												renderRequest.setAttribute(LLAVE_CLIENTE_FIRMADO, 
														gson.toJson(cliente.getCliente()));
												fillClient(cliente, queja);
											});
								});
				_log.info("AtencionQuejasPortlet -> render -> queja  = " + queja);
				session.setAttribute(LLAVE_QUEJA, gson.toJson(queja), PortletSession.APPLICATION_SCOPE);
				renderRequest.setAttribute(LLAVE_QUEJA, gson.toJson(queja));
				
				_log.info("AtencionQuejasPortlet -> render -> isClientEnabled = " + isClientEnabled());
				session.setAttribute(LLAVE_QUEJA_CLIENTE_HABILITADO, isClientEnabled(), PortletSession.APPLICATION_SCOPE);
				renderRequest.setAttribute(LLAVE_QUEJA_CLIENTE_HABILITADO, isClientEnabled());

			} else {
				System.err.println("Reiniciar modulo mx.com.allianz.module.service.facade ");
				System.err.println("$ telnet localhost 11311 ");
				System.err.println("g! lb | grep mx.com.allianz.module.service.facade ");
				System.err.println("g! stop PID ");
				System.err.println("g! start PID ");
				System.err.println("g! disconnect ");
				System.err.println("YES ");
			}
		}	
		
		
		_log.info((session.getAttribute(LLAVE_QUEJA_REC, PortletSession.APPLICATION_SCOPE) != null ? "":"No ")+"Se encuentra el objeto de recuperación");
		if ( session.getAttribute(LLAVE_QUEJA_REC, PortletSession.APPLICATION_SCOPE) != null ){
			_log.info("*-*-*-*-*-*-*-*-*-*-* Objeto de recuperación *-*-*-*-*-*-*-*-*-*\n" + session.getAttribute(LLAVE_QUEJA_REC, PortletSession.APPLICATION_SCOPE));
			renderRequest.setAttribute(LLAVE_QUEJA_REC, gson.toJson(session.getAttribute(LLAVE_QUEJA_REC, PortletSession.APPLICATION_SCOPE)));
		}
		

		String currentURL = PortalUtil.getCurrentURL(renderRequest);
		String[] currentPair = Arrays.stream(redirectPair())
				  .map(uri -> uri.split(":"))
				  .peek(rurl -> System.out.println("redirectURL pair = [" + rurl[0] + ", " + rurl[1] +"]" ))
				  .filter(pairURI -> currentURL.contains(pairURI[0]))
				  .findAny()
				  .orElse(redirectPair()[0].split(":"));
		String[] paths = currentPair[1].split("/");
		String redirect = (paths.length > 2 ? Arrays.stream(Arrays.copyOfRange(paths, 0, paths.length-1)).collect(Collectors.joining("/")) : "" ) 
				+ (currentPair[0].contains("/") ? currentPair[0] : ("/"+currentPair[0]));
		
		session.setAttribute(LLAVE_CURRENT_PAGE_URL, redirect, PortletSession.APPLICATION_SCOPE);
		renderRequest.setAttribute(LLAVE_CURRENT_PAGE_URL, redirect);

		super.render(renderRequest, renderResponse);
	}
	
	public QuejaFlujoDTO fillClient(ClienteTramiteDTO clienteTramite, QuejaFlujoDTO queja) {
		Optional<ClienteDTO> cliente = Optional.ofNullable(clienteTramite)
									   .filter(c -> new Boolean(c.getEstatus()))
									   .map(ClienteTramiteDTO::getCliente);
		Optional<ContactoDTO> contacto = cliente.map(ClienteDTO::getContacto);
		
		cliente.ifPresent(c -> {
			queja.setNombre(c.getNombre());
			queja.setApellidoPaterno(c.getApellidoPaterno());
			queja.setApellidoMaterno(c.getApellidoMaterno());
		});
		
		Arrays.stream(contacto.map(ContactoDTO::getCorreos)
			  .orElseGet(() -> new CorreoDTO[0]))
			  .findAny()
			  .ifPresent(correo -> queja.setEmail(correo.getCorreoElectronico()));
		
		Arrays.stream(contacto.map(ContactoDTO::getTelefonos)
				  .orElseGet(() -> new TelefonoDTO[0]))
				  .forEach(telefono -> {
					  if(TIPO_TELEFONO_CELULAR.equals(telefono.getTipoTelefono()) || 
							  (telefono.getLada() != null && LADA_CELULAR.equals(telefono.getLada()))){
						  
						  queja.setTelefonoCelular(telefono.getTelefono());
					  } else {
						  queja.setTelefonoParticular(telefono.getTelefono());
					  }
				  });
		
		queja.setProductos(Arrays.stream(cliente.map(ClienteDTO::getProductos)
							 					  .orElseGet(() -> new ProductoClienteDTO[0]))
								   .map(cli -> {
									   cli.setNumeroPoliza(Optional.ofNullable(cli.getNumeroPoliza())
									   		   .filter(numeroPoliza -> !"".equals(numeroPoliza))
									   		   .orElse(""+getRandomPolicy()));
									   return cli;
								   })
								   .filter(producto -> 
									   PRODUCTO_ACTIVO.equals(producto.getEstatusProducto())
								   )
								   .collect(Collectors.toList())
		);
		
		return queja;
	}
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		_log.info(" ******************** processEvent de atención queja ********************");
		if(_log.isDebugEnabled()) {
			_log.debug("AtencionQuejasPortlet -> processEvent solicitante");
		}
		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		Optional<Event> event = Optional.ofNullable(request)
										.map(EventRequest::getEvent);
		String inconsistenciaErrorMessage = "Inconsistencia al intentar enviar queja a sistema remoto," + 
				" no se encontro uno o mas capos obligatorios, favor de registrar nuevamente su tramite o" + 
				" contacte a al administrador";
		if (event.map(Event::getName)
				 .filter(LLAVE_INCONSISTENCIA_EVENT::equals)
				 .isPresent()) {
			SessionErrors.add(request, InconsistenciaQuejaException.class , new InconsistenciaQuejaException("Hubo un error al registras su queja"));
			session.setAttribute(LLAVE_INCONSISTENCIA_ERROR, inconsistenciaErrorMessage, PortletSession.APPLICATION_SCOPE);

			event.map(Event::getValue)
				 .filter(val -> (val instanceof String))
				 .map(json -> gson.fromJson((String) json, QuejaFlujoDTO.class))
				 .ifPresent(obj -> {
					 if(_log.isErrorEnabled()) {
							_log.error("AtencionQuejasPortlet -> processEvent -> inconsistenciaQuejaEvent,  queja = " + obj);
					}
					
				 });
		}
		if (event.map(Event::getName)
				 .filter(LLAVE_ATENCION_QUEJA_EVENT::equals)
				 .isPresent()){
			_log.info("***************************-------- El otro evento ----***");
			event.map(Event::getValue)
			 .filter(val -> (val instanceof String))
			 .map(json -> gson.fromJson((String) json, QuejaFlujoDTO.class))
			 .ifPresent(obj -> {
				 if(_log.isErrorEnabled()) {
						_log.error("AtencionQuejasPortlet -> processEvent -> atencionQuejaEvent,  queja = " + obj);
				}
				session.setAttribute(LLAVE_QUEJA_REC, obj, PortletSession.APPLICATION_SCOPE);
			 });			
		}
	}
	
	
	public static String getRandomPolicy() {
		return "" + new Random().nextInt(((99_999 - 999) + 1) + 999 );
	}
	
	public String allianzClientToken() {
		return _configuration.allianzClientToken();
	}
	
	public Boolean isClientEnabled() {
		return _configuration.enableClientConfiguration();
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				AntencionQuejaConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info("Configured Queja (atencion.quejas v1.0.1) : \n{ " +
					"rooth Path : " + _configuration.rootPath() + ", " +
					"wait clean : " + _configuration.waitClean() 
					+ " }");
		}
	}
	
	private String[] redirectPair(){
		return _configuration.redirectPair();
	}
	
	
	@Reference
	protected void setAllianzService(AllianzService allianzService) {
		this.allianzService = allianzService;
	}

	protected void unsetAllianzService(AllianzService allianzService) {
		this.allianzService = null;
	}

	private AllianzService allianzService;

	private volatile AntencionQuejaConfiguration _configuration;
	private static Log _log = LogFactoryUtil.getLog(AtencionQuejasPortlet.class);

}