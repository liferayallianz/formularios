package mx.com.allianz.quejas.sender.portlet.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

/**
 * @author Testing Zone
 */
@ExtendedObjectClassDefinition(category = "Productivity")

@Meta.OCD(
		id = "mx.com.allianz.quejas.sender.portlet.configuration.AntencionQuejaConfiguration",
		localization = "content/Language",
		name = "Configuracion Queja Atencion"
	)
public interface AntencionQuejaConfiguration {

	/**
	 * rootPath: This is the root path that constrains all filesystem access.
	 */
	@Meta.AD(deflt = "/opt/docs_tramites", 
			description = "Ruta Temporal de Quejas ",
			required = false)
	public String rootPath();

	@Meta.AD(
		deflt = "7200000", 
		description = "Tiempo de espera para borrar archivos(millis) ",
		required = false
	)
	public long waitClean();
	
	@Meta.AD(
			deflt = "false", 
			description = "Activa configuracion de Clientes a modulo de Quejas",
			required = false
		)
	public Boolean enableClientConfiguration();
	
	@Meta.AD(
			deflt = "azctToken", 
			description = "Token con llave de cliente",
			required = false
		)
	public String allianzClientToken();
	
	@Meta.AD(
			deflt = "atencion-queja:/web/guest/detalle-queja,atencionp-queja:/group/guest/detallep-queja", 
			description = "Mapeo de posible ruta con redireccionamiento",
			required = false
		)
	public String[] redirectPair();
}

