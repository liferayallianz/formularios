package mx.com.allianz.quejas.sender.portlet.exception;

public class InconsistenciaQuejaException extends Exception {

	private static final long serialVersionUID = -6253212890502158784L;

	public InconsistenciaQuejaException() {
		super();
	}

	public InconsistenciaQuejaException(String message) {
		super(message);
	}

	public InconsistenciaQuejaException(Throwable cause) {
        super(cause);
    }
}