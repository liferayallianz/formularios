<%@ include file="/init.jsp" %>

<aui:form action="#" name="fm_datos_fiscales">
		<aui:input name="version-datos.fiscales" type="hidden" value="1.0.1"  />
		<aui:input cssClass="formularioCampoTexto" id="datos_fiscales_razon_social" name="razonSocial" type="text" value="" label="" placeholder="datos.fiscales_DatosFiscales.razonsocial" >
			<aui:validator name="rangeLength" errorMessage="La longitud de la raz&oacute;n social debe ser mayor a 2 y menor a 100 caracteres">[2,100]</aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" name="datos_fiscales_rfc" type="text" value="" label="" placeholder="datos.fiscales_DatosFiscales.rfc" >
		<aui:validator name="rangeLength" errorMessage="La longitud del rfc debe ser mayor a 10 y menor a 13 caracteres">[10,13]</aui:validator>
		<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras o n&uacute;meros">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~`!@#$%^&*()_+=-[]\\\';,./{}|\":<>? ";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>		
		</aui:input>
</aui:form>

<script type="text/javascript">

console.log("datos.fiscales v1.0.1");

Liferay.on('cargaDatosFiscales', function(queja){
	console.log("Cargando datos desde queja bien");
	if( queja ) {
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />datos_fiscales_razon_social').set('value', queja.razonSocial);
			A.one('#<portlet:namespace />datos_fiscales_rfc').set('value', queja.rfc);
		});
	}
});
</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_fiscales').formValidator;
// add valid form
Liferay.on('validaDatosFiscales', function(event){
	new AUI().use(function(A) {
		formValidator.validate();
		var eventoRespuesta = {};
		eventoRespuesta.valido = !formValidator.hasErrors();
		console.log('validaDatosFiscales eventoRespuesta.valido = ' + eventoRespuesta.valido);
		if (eventoRespuesta.valido) {
			eventoRespuesta.razonSocial = A.one('#<portlet:namespace />datos_fiscales_razon_social').get('value'),
			eventoRespuesta.rfc = A.one('#<portlet:namespace />datos_fiscales_rfc').get('value')
 		}	
		Liferay.fire('validaDatosFiscalesRespuesta', eventoRespuesta );
	});
});
</aui:script>
