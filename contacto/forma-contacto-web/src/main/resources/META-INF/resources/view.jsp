<%@ include file="/init.jsp" %>

<portlet:actionURL var="sendContactActionURL" name="processAction"/>
<portlet:resourceURL var="actualizaContacto" />

<aui:form name="form_contacto" method="post"  action="<%=sendContactActionURL.toString()%>">
    	<aui:button-row>
        		<aui:button class="btn pull-right" name="saveContact" type="button" value="sender_Sender.send"  onclick="validateContactForm();" /> 
        		<aui:button class="btn pull-right" name="cancelButton" type="button" value="sender_Sender.clean" onclick="window.location.href = '/contacto-form-allianz-mexico';" />         		       		
    	</aui:button-row>
</aui:form>

<script type="text/javascript"> 
var formsValidSection = [];

Liferay.provide(window, 'validateContactForm', function() {
	console.log("validateContactForm");
	formsValidSection = [];
	Liferay.fire('validaDatosPersonales', {} );
	Liferay.fire('validaNumeroPoliza', {} );
	Liferay.fire('validaDatosEmpresas', {} );
	Liferay.fire('validaDireccion', {} );
	Liferay.fire('validaDatosContacto', {} );
	Liferay.fire('validaEncuestaMediosContacto', {} );
	Liferay.fire('validaCaptcha', {} );
	console.log("fire end validateContactForm");
});

Liferay.on('validaDatosPersonalesRespuesta', validaSeccion);
Liferay.on('validaNumeroPolizaRespuesta', validaSeccion);
Liferay.on('validaDatosEmpresasRespuesta', validaSeccion);
Liferay.on('validaDireccionRespuesta', validaSeccion);
Liferay.on('validaDatosContactoRespuesta', validaSeccion);
Liferay.on('validaEncuestaMediosContactoRespuesta', validaSeccion);
Liferay.on('validaCaptchaRespuesta', validaSeccion);

function validaSeccion(respuestaSeccion) { 
	console.log("desde la respesta la seccion es" + respuestaSeccion.valido);
	if (respuestaSeccion.valido) {
		formsValidSection.push(respuestaSeccion.valid);
		callServeResource(respuestaSeccion);
		//sendSubmit();
	}
}

function sendSubmit() {
	console.log("submit formsValidSection.length" + formsValidSection.length);
	if(formsValidSection.length % 7 === 0) {
		console.log("submit");
		new AUI().use(function(A) {
			console.log("submit send" + A);
			var forma = A.one('#<portlet:namespace/>form_contacto');
			console.log("submit forma" +forma);
			forma.submit();
		});
	}
}

function callServeResource(seccion) {
	
	$.post('<%=actualizaContacto.toString()%>',{
	    	nombre: seccion.nombre,
	    	apellidoPaterno: seccion.apellidoPaterno,
	    	apellidoMaterno: seccion.apellidoMaterno,
	    	emisorPoliza: seccion.emisorPoliza,
	    	numeroPoliza: seccion.numeroPoliza,
	    	empresa: seccion.empresa,
	    	direccion: seccion.direccion,
		codigoPostal: seccion.codigoPostal,
		estado: seccion.estado,
		estadoDescripcion: seccion.estadoDescripcion,
		ciudad: seccion.ciudad,
		ciudadDescripcion: seccion.ciudadDescripcion,
		telefonoParticular: seccion.telefonoParticular,
		telefonoCelular: seccion.telefonoCelular,
		email: seccion.email,
		medioContacto: seccion.medioContacto,
		medioContactoDescripcion: seccion.medioContactoDescripcion,
		comentarios: seccion.comentarios,
		captcha: seccion.captcha
	},function(data, status){
		sendSubmit();
	}); 

}

</script>
