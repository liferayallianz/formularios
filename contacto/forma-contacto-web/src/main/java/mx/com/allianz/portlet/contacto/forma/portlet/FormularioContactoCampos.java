package mx.com.allianz.portlet.contacto.forma.portlet;

import static javax.portlet.PortletSession.APPLICATION_SCOPE;
import static mx.com.allianz.commons.constants.FormularioConstants.*;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.function.Predicate;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;

import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.dto.contacto.ContactoDTO;


public enum FormularioContactoCampos {
	EMISOR("emisorPoliza"), NUMERO_POLIZA("numeroPoliza"), NOMBRE("nombre"), 
	APELLIDO_PATERNO("apellidoPaterno"), APELLIDO_MATERNO("apellidoMaterno"),
	EMPRESA("empresa"), DIRECCION("direccion"), CODIGO_POSTAL("codigoPostal"),
	ESTADO("estado"), ESTADO_DESCRIPCION("estadoDescripcion"), CIUDAD("ciudad"),
	CIUDAD_DESCRIPCION("ciudadDescripcion"), TELEFONO_PARTICULAR("telefonoParticular"),
	TELEFONO_CELULAR("telefonoCelular"), EMAIL("email"), MEDIO_CONTACTO("medioContacto"),
	MEDIO_CONTACTO_DESCRIPCION("medioContactoDescripcion"), COMENTARIOS("comentarios"), CAPTCHA("captcha");
	
	FormularioContactoCampos(String valor){
        try {
            Field fieldName = getClass().getSuperclass().getDeclaredField("name");
            fieldName.setAccessible(true);
            fieldName.set(this, valor);
            fieldName.setAccessible(false);
        } catch (Exception e) {}
	}
	
	public void buildContacto(ContactoDTO contacto, String parametro){
		switch (this) {
		case EMISOR:
			contacto.setEmisor(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case NUMERO_POLIZA:
			contacto.setNumeroPoliza(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case NOMBRE:
			contacto.setNombre(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case APELLIDO_PATERNO:
			contacto.setApellidoPaterno(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case APELLIDO_MATERNO:
			contacto.setApellidoMaterno(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case DIRECCION:
			contacto.setDireccion(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case EMPRESA:
			contacto.setEmpresa(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case CODIGO_POSTAL:
			contacto.setCodigoPostal(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case ESTADO:
			contacto.setEstado(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case ESTADO_DESCRIPCION:
			contacto.setEstadoDescripcion(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case CIUDAD:
			contacto.setCiudad(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case CIUDAD_DESCRIPCION:
			contacto.setCiudadDescripcion(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case TELEFONO_PARTICULAR:
			contacto.setTelefonoParticular(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case TELEFONO_CELULAR:
			contacto.setTelefonoCelular(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case EMAIL:
			contacto.setEmail(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case MEDIO_CONTACTO:
			contacto.setMedioContacto(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case MEDIO_CONTACTO_DESCRIPCION:
			contacto.setMedioContactoDescripcion(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case COMENTARIOS:
			contacto.setComentarios(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case CAPTCHA:
			contacto.setCaptcha(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		default:
			break;
		}
	}
	
	public void uploadToSession(PortletSession session, String parametro){
		Optional.ofNullable(parametro).filter(NOT_EMPTY).ifPresent(value -> session.setAttribute(this.name(), value, APPLICATION_SCOPE));
	}
	
	public String getParamFromRequest(ResourceRequest resourceRequest){
		return Optional.ofNullable(ParamUtil.getString(resourceRequest, this.name())).map(this::tryString).orElse(EMPTY);
	}
	
	public String getParamFromSession(PortletSession session){
		return Optional.ofNullable(session.getAttribute(this.name(), APPLICATION_SCOPE)).map(this::tryString).orElse(EMPTY);
	}
	
	public Integer tryParseInt(String text) {
			return (text.matches("\\d+")) ? Integer.parseInt(text) : null;
	}
	
	public String tryString(Object param) {
		 return (param instanceof String) ? (String) param : null;
	}
	
	public static final Predicate<String> NOT_EMPTY = (String it) -> !it.isEmpty();

}
