package mx.com.allianz.portlet.contacto.forma.portlet;

import java.io.IOException;
import java.util.Arrays;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import mx.com.allianz.commons.dto.contacto.ContactoDTO;
import mx.com.allianz.service.contacto.api.ContactoService;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
		// Fin parametros para llamado de eventos de portlet
		// Se agregan parametros para compartir atributos de request en la session
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        "com.liferay.portlet.remoteable=true",
        "com.liferay.portlet.preferences-unique-per-layout=true",
		// Fin parametros para compartir atributos de request en la session
		"javax.portlet.display-name=Contacto Formulario Sender",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class FormaContactoPortlet extends MVCPortlet {
	
	@Override
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
	    System.out.println("FormularioContactoPortlet processAction ");
	      
	    ContactoDTO contacto = buildContact(actionRequest);
	    PortletSession session = actionRequest.getPortletSession();
		session.setAttribute("contacto", contacto, PortletSession.APPLICATION_SCOPE);
		contactoService.enviarCorreoContacto(contacto);
//		LocalBeanFactory.send(contacto);
		actionResponse.sendRedirect("/confirmacion-allianz-mexico-contacto");
	}
	
	private ContactoDTO buildContact(ActionRequest actionRequest) {
		ContactoDTO  contacto = new ContactoDTO();
		PortletSession session = actionRequest.getPortletSession();
		Arrays.stream(FormularioContactoCampos.values()).forEach((campo) -> {campo.buildContacto(contacto, campo.getParamFromSession(session));});
		return contacto;
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		System.out.println("FormularioContactoPortlet -> serveResource ");
		
		PortletSession session = resourceRequest.getPortletSession();
		Arrays.stream(FormularioContactoCampos.values()).forEach((campo) -> { campo.uploadToSession(session, campo.getParamFromRequest(resourceRequest));});
		
		super.serveResource(resourceRequest, resourceResponse);
	}
	
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	protected void setContactoService(ContactoService contactoService) {
		this.contactoService = contactoService;
	}

	protected void unsetContactoService(ContactoService contactoService) {
		this.contactoService = null;
	}

	private ContactoService contactoService;
}