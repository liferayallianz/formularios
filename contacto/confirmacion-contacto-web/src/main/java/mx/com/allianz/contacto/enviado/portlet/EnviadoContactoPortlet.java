package mx.com.allianz.contacto.enviado.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.xml.namespace.QName;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
		// Fin parametros para llamado de eventos de portlet
		// Se agregan parametros para compartir atributos de request en la session
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        "com.liferay.portlet.remoteable=true",
        "com.liferay.portlet.preferences-unique-per-layout=true",
		// Fin parametros para compartir atributos de request en la session
		"javax.portlet.display-name=Contacto Confirmacion Sender",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-processing-event=enviadoContactoEvent;http://mx-allianz-liferay-namespace.com/events/enviadoContacto",
		// Fin se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.security-role-ref=power-user,user,guest",
        "javax.portlet.supports.mime-type=text/html"
	},
	service = Portlet.class
)
public class EnviadoContactoPortlet extends MVCPortlet {

	@Override
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
	      String id = "123";//actionRequest.getParameter("inputParam");
	      System.out.println("submitted value is " + id);
	      QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/contacto","contactoEvent");
	      actionResponse.setEvent(qName, id);
	      QName qNameNumeroPoliza = new QName("http://mx-allianz-liferay-namespace.com/events/numeroPoliza","numeroPolizaEvent");
	      actionResponse.setEvent(qNameNumeroPoliza, id);
 
	      actionResponse.sendRedirect("/contacto-form-allianz-mexico");
	}
}