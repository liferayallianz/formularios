package mx.com.allianz.datosempresa.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para recibir de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        // fin Se agregan parametros para recibir de eventos de portlet
		// Se agregan parametros para compartir atributos de request en la session
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        "com.liferay.portlet.remoteable=true",
		"com.liferay.portlet.preferences-unique-per-layout=true",
		// Fin parametros para compartir atributos de request en la session
		"javax.portlet.display-name=Commons Empresa",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Fin Se agrega nombre de mensaje y cola del portlet para publicar
		"javax.portlet.security-role-ref=power-user,user,guest",
        "javax.portlet.supports.mime-type=text/html"
	},
	service = Portlet.class
)
public class DatosEmpresaPortlet extends MVCPortlet {
}