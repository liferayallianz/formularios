<%@ include file="/init.jsp" %>
	<aui:form action="#" name="fm_datos_empresa" >
		<aui:input cssClass="formularioCampoTexto" id="datos_empresa_empresa" name="empresa" type="text" value="" label="" placeholder="datos.empresa.company" >
			<aui:validator name="maxLength">150</aui:validator>	
		</aui:input>
	</aui:form>
	
<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_empresa').formValidator;

Liferay.on('validaDatosEmpresas', function(event) {
	formValidator.validate();
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaDatosEmpresas eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if(eventoRespuesta.valido){
		eventoRespuesta.empresa = A.one('#<portlet:namespace />datos_empresa_empresa').get('value');
	}
	Liferay.fire('validaDatosEmpresasRespuesta', eventoRespuesta );
});
</aui:script>
