<%@ include file="/init.jsp" %>

<p class="text-justify">
	Puedes acudir a nuestra Unidad Especializada de Atenci�n al P&uacute;blico, ubicada en Blvd. 
	Manuel &Aacute;vila Camacho 164 piso 1, Col. Lomas de Barrilaco, M&eacute;xico D.F. C.P. 11010, 
	en un horario de atenci&oacute;n de lunes a jueves de 09:00 a 13:00 y de 15:00 a 17:00 horas y 
	viernes de 09:00 a 14:00 horas, llamar a los tel&eacute;fonos (55) 5201-3039 y del interior 
	de la rep&uacute;blica al (01 800) 111 12 00 ext. 3039 o enviar un correo electr&oacute;nico a 
	claudia.espinosa@allianz.com.mx.
</p>
<p class="text-justify">
	<b>CONDUSEF</b>: Av. Insurgentes Sur #762 Col. Del Valle, M&eacute;xico D.F. C.P. 03100. 
	Tel&eacute;fonos: (55) 5340 0999 y (01 800) 999 80 80, www.condusef.gob.mx, asesor&iacute;a@condusef.gob.mx
</p>
<p>
	S&iacute;guenos en las redes sociales: 
	<a href="https://www.facebook.com/allianzmexico1"><img src="http://www.allianz.com.mx/documents/10180/1608992/facebook.jpg" alt="AzMxFb" style="width:42px;height:42px;border:0;"></a>
	<a href="https://twitter.com/allianzmexico"><img src="http://www.allianz.com.mx/documents/10180/1608992/twitter.jpg" alt="AzMxFb" style="width:42px;height:42px;border:0;"></a>
	<a href="https://mx.linkedin.com/company/allianz-mexico"><img src="http://www.allianz.com.mx/documents/10180/1608992/LinkedIn-InBug-2CRev.png" alt="AzMxFb" style="width:42px;height:42px;border:0;"></a>
</p>