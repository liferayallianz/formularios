<%@ include file="/init.jsp" %>

<aui:form action="#" name="fm_encuesta_medios_contacto">
	<p class="formularioEtiquetaTexto"><liferay-ui:message key="medioscontacto_MediosContacto.mediocontacto" /></p>
	<aui:select cssClass="formularioCampoTexto" label="" name="medioContacto" >
		<aui:option selected="true" value="1"><liferay-ui:message key="Internet" /></aui:option>
		<aui:option value="2"><liferay-ui:message key="Peri&oacute;co" /></aui:option>
		<aui:option value="3"><liferay-ui:message key="Revista" /></aui:option>
		<aui:option value="4"><liferay-ui:message key="Conocido" /></aui:option>
		<aui:option value="5"><liferay-ui:message key="Soy cliente de Allianz" /></aui:option>
		<aui:option value="6"><liferay-ui:message key="Otro" /></aui:option>
	</aui:select>
	<aui:input cssClass="formularioCampoTexto" type="textarea" label="" placeholder="medioscontacto_MediosContacto.comentarios" rows="5" cols="30" name="comentarios" >
			<aui:validator name="maxLength" errorMessage="La longitud del campo debe ser menor a 1000 caracteres">1000</aui:validator>	
	</aui:input>
</aui:form>


<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
var formValidator = Liferay.Form.get('<portlet:namespace />fm_encuesta_medios_contacto').formValidator;

Liferay.on('validaEncuestaMediosContacto', function(event) {
	formValidator.validate();
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaEncuestaMediosContacto eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if(eventoRespuesta.valido){
		var medioContacto = A.one('#<portlet:namespace />medioContacto');
		eventoRespuesta.medioContacto = medioContacto.get('value');
		eventoRespuesta.medioContactoDescripcion = medioContacto.get('options')._nodes[medioContacto.get('selectedIndex')].text;
		eventoRespuesta.comentarios = A.one('#<portlet:namespace />comentarios').get('value');
	}
	Liferay.fire('validaEncuestaMediosContactoRespuesta', eventoRespuesta );
});
</aui:script>
