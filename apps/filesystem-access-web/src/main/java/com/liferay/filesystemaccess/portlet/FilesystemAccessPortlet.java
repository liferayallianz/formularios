package com.liferay.filesystemaccess.portlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.Gson;
import com.liferay.filesystemaccess.api.FilesystemAccessService;
import com.liferay.filesystemaccess.api.FilesystemItem;
import com.liferay.filesystemaccess.portlet.config.FilesystemAccessDisplayContext;
import com.liferay.filesystemaccess.portlet.config.FilesystemAccessPortletInstanceConfiguration;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.PortletInstance;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import mx.com.allianz.commons.catalogos.dto.DocumentoDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.service.documento.service.DocumentoLocalServiceUtil;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;

/**
 * class FilesystemAccessPortlet: This portlet is used to provide filesystem
 * access.  Allows an administrator to grant access to users to access local
 * filesystem resources, useful in those cases where the user does not have
 * direct OS access.
 *
 * This portlet will provide access to download, upload, view, 'touch' and
 * edit files.
 * @author dnebinger
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Filesystem Access",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // Fin Se agregan parametros para llamado de eventos de portlet
		"javax.portlet.security-role-ref=power-user,user,guest",
		"javax.portlet.supported-processing-event=solicitanteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent"
	},
	service = Portlet.class
)
public class FilesystemAccessPortlet extends MVCPortlet {

	/**
	 * render: Overrides the parent method to handle the injection of our
	 * service as a render request attribute so it is available to all of the
	 * jsp files.
	 *
	 * @param renderRequest The render request.
	 * @param renderResponse The render response.
	 * @throws IOException In case of error.
	 * @throws PortletException In case of error.
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		TramiteDto tramite = Optional.ofNullable(renderRequest).map(RenderRequest::getPortletSession)
		.map(session->session.getAttribute("tramite", PortletSession.APPLICATION_SCOPE))
		.filter(attribute -> (attribute instanceof TramiteDto)).map(attribute -> (TramiteDto)attribute).orElse(null);
		
		String tipoTramite = tramite != null ? tramite.getTipoTramite() : "GMM";
		_log.debug("tipoTramite:" + tipoTramite);

		List<DocumentoDTO> documentos = DocumentoLocalServiceUtil.getDocumentos(-1,-1).stream()
			.filter(documento -> documento.getTipo().equalsIgnoreCase(tipoTramite))
			.map(documento -> new DocumentoDTO(documento.getIdDocumentos(),
					documento.getTipo(), documento.getCodigo(),
					documento.getDescripcion())).sorted(Comparator.comparing(DocumentoDTO::getIdDocumentos))
			.collect(Collectors.toList());
		renderRequest.setAttribute("documentos", documentos);
		
		// set the service as a render request attribute
		renderRequest.setAttribute(
			Constants.ATTRIB_FILESYSTEM_SERVICE, _filesystemAccessService);
		
		String folderArchivosTramite = tramite != null ? tramite.getFolderArchivosTramite() : String.valueOf(new Date().getTime());
		_log.debug("folderArchivosTramite:" + folderArchivosTramite);

		renderRequest.setAttribute(Constants.ATTRIB_TIME, folderArchivosTramite);
		_log.debug(renderRequest.getAttribute(Constants.ATTRIB_TIME));

		// invoke super class method to let the normal render operation run.
		super.render(renderRequest, renderResponse);
	}

	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		_log.debug("processEvent de complemento: cambio de nombre objeto enviado = " + request.getEvent());
		try {
			
			if(_log.isDebugEnabled()) {
				_log.debug("SingleflieuploadmvcmvcportletPortlet -> processEvent");
			}
			Gson gson = new Gson();
			PortletSession session = request.getPortletSession();
			Optional<Event> event = Optional.ofNullable(request)
											.map(EventRequest::getEvent);
			// Se obtiene el evento del request
			if (event.map(Event::getName)
					.filter("solicitanteTramiteClienteEvent"::equals).isPresent()){
				_log.debug("dentro del evento = " + request.getEvent().getValue());
				TramiteDto tramite = event.map(Event::getValue)
										  .filter(obj -> (obj instanceof String))
										  .map(json -> gson.fromJson((String) json, TramiteDto.class))
										  .orElseGet(TramiteDto::new);
	
				_log.debug("Antes de lo de liferay");
				initConfig(request);
				
				String rootPath = portletInstanceConfig.rootPath();
				_log.debug("rootPath = " + rootPath);
	
				String path = rootPath + File.separator + tramite.getFolderArchivosTramite();
				_log.debug("path = " + path);
				File directory = new File(String.valueOf(rootPath + File.separator + tramite.getFolderArchivosTramite() + File.separator));
				if (!directory.exists()){
					directory.mkdir();
			    }
				//String folderArchivosTramite = tramite != null ? tramite.getFolderArchivosTramite() : String.valueOf(new Date().getTime());
				session.setAttribute("tramite", tramite, PortletSession.APPLICATION_SCOPE);
				
			}
		}catch (Exception e) { e.printStackTrace();}
		super.processEvent(request, response);
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws IOException,
            PortletException {
        _log.debug("FilesystemAccessPortlet -> serveResource ");
        initConfig(resourceRequest);

        resourceRequest.getParameterMap().forEach((llave,valor) -> _log.debug("llave = " + llave + ", valor = " + Arrays.asList(valor)));

		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		JSONObject jsonMessageObject = JSONFactoryUtil.createJSONObject();
        FilesystemAccessPortletInstanceConfiguration config = portletInstanceConfig;
        resourceResponse.setContentType("text/plain");
        PrintWriter writer = resourceResponse.getWriter();
        
        try {
        	
        if (config == null) {
            _log.debug("No config found.");

            SessionErrors.add(
                resourceRequest, MissingConfigurationException.class.getName());

            return;
        }

        String cmd = ParamUtil.getString(resourceRequest, Constants.CMD);
        String currentPath = ParamUtil.getString(resourceRequest, Constants.PARM_CURRENT_PATH);
        String filename = ParamUtil.getString(resourceRequest, Constants.PARM_UPLOAD_NAME);
        String docId = ParamUtil.getString(resourceRequest, Constants.PARM_DOC_ID);
        String timeFolder = ParamUtil.getString(resourceRequest, Constants.ATTRIB_TIME);
        _log.debug("filename = " +filename);
        
        
        if (filename.contains(",")) {
            _log.info("El archivo contiene una comma en su nombre.");
            _log.warn("El archivo contiene una comma en su nombre.");
    		JSONObject jsonMessageSuccess = JSONFactoryUtil.createJSONObject();
    		jsonMessageSuccess.put("success", false); 
    		jsonMessageSuccess.put("errorMessage", "El nombre de archivo no debe contener comas (\",\"). "); 

    		writer.write(jsonMessageSuccess.toString());

            return;
        }
        
        if (filename.contains("\\")) {
        	filename = filename.substring(filename.lastIndexOf("\\")+1, filename.length());
        }
        if (docId == null) {
        	docId = "GMM_06";
        }
        
        _log.debug("Constants.UPDATE.equals(cmd) = " + Constants.UPDATE.equals(cmd));
        _log.debug("Constants.DELETE.equals(cmd) = " + Constants.DELETE.equals(cmd));
        _log.debug("cmd = " + cmd);
        
        if (Constants.UPDATE.equals(cmd)){
        	
        	UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
        	File file = uploadPortletRequest.getFile(Constants.PARM_UPLOAD_FILE);
        	
        	if (!validContentType(file, config.validContenTypes(), contentType)) {
        		_log.error("Uploads for mimetype not authorized but user tried to upload a file with changed extention.");
        		JSONObject jsonMessageSuccess = JSONFactoryUtil.createJSONObject();
        		jsonMessageSuccess.put("success", false); 
        		jsonMessageSuccess.put("errorMessage", "Tipo de archivo invalido"); 

        		writer.write(jsonMessageSuccess.toString());
        		return;
        	}
        	
        	TimeZone tz = TimeZone.getDefault();
        	
        	if (SessionErrors.isEmpty(resourceRequest)) {
        		
        		User user = null;
        		try {
        			user = PortalUtil.getUser(resourceRequest);
        			
        			if (user != null) {
        				tz = user.getTimeZone();
        			}
        			else {
        				Company company = PortalUtil.getCompany(resourceRequest);
        				
        				tz = company.getTimeZone();
        			}
        			
        			
        			_log.debug("Antes de enviar el archivo");
        			_log.debug("doProcessAction, rootPath:" + config.rootPath());
        			_log.debug("doProcessAction, timeFolder:"+timeFolder);
        			
        			_log.debug("doProcessAction, currentPath = " + currentPath);
        			
        			_log.debug("doProcessAction, docId = " + docId);
        			_log.debug("doProcessAction, filename = " + filename);
        			_log.debug("doProcessAction, filename:" + docId + "__" + filename);
        			
        			_log.debug("doProcessAction, file = " + file);
        			_log.debug("doProcessAction, file.getName(realname) = " + file.getName());
        			
        			_filesystemAccessService.uploadFile(
        					config.rootPath()+File.separator+timeFolder, currentPath, docId + "__" + filename, file, tz);
        		} catch (PortalException e) {
        			if(_log.isErrorEnabled()) {
        				_log.error(e.getMessage());
        			}
        			System.out.println("Error Message = " + e.getMessage());
        			//TODO Borrar printStackTrace
        			e.printStackTrace();
        			
            		JSONObject jsonMessageSuccess = JSONFactoryUtil.createJSONObject();
            		jsonMessageSuccess.put("success", false); 
            		jsonMessageSuccess.put("errorMessage",e.getMessage());

            		writer.write(jsonMessageSuccess.toString());
            		return;
        		} catch (Exception e) {
        			if(_log.isErrorEnabled()) {
        				_log.error(e.getMessage());
        			}
        		}
        		
        		Gson gson = new Gson();
        		List<FilesystemItem> filesystemItem = _filesystemAccessService.getFilesystemItems(config.rootPath()+File.separator+timeFolder, currentPath, 0, 100, tz);
        		Optional.ofNullable(filesystemItem)
        		.orElseGet(ArrayList<FilesystemItem>::new).stream()
        		.sorted((file1, file2) -> (int) (file1.getModified().getTime() - file2.getModified().getTime()))
        		.map(fileItem -> {
        			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
        			jsonObject.put("nombreRealArchivo", fileItem.getName());
        			jsonObject.put("nombreArchivo", fileItem.getNameDisplay());
        			jsonObject.put("docId", fileItem.getDocId());
        			
        			return jsonObject;
        		}).forEach(jsonArray::put); 
        	}		
        } else if (Constants.DELETE.equals(cmd)){
        	_log.debug("houston tenemos un problema");
        	String ruta = config.rootPath()+File.separator+timeFolder+File.separator+docId + "__" + filename;
        	_log.debug("ruta = " + ruta);
        	Path path = Paths.get(config.rootPath()+File.separator+timeFolder+File.separator+docId + "__" + filename);
    		_log.debug("Path = " + path);
        	if (Files.exists(path)){
    			if (path.toFile().delete()){
    				jsonMessageObject.put("success",true);
    				
    			} else {
    				jsonMessageObject.put("success",false);
    				jsonMessageObject.put("errorMessage","Error al eliminar el archivo: " + docId + "__" + filename);
    			}
    		}
        } else {
        	_log.debug("Operacion no soportada");
        }

            
        
        }catch(Exception e) {
        	e.printStackTrace();
        }
       

        writer.write(jsonArray.toString());
        super.serveResource(resourceRequest, resourceResponse);
    }
	
	private boolean validContentType(File file, String[] validContentTypes, Function<File, String> detectMimeType) {
		_log.debug("Validando ContentType");
		_log.debug("file name :" + file.getName());
		_log.debug("valid extensions :" + validContentTypes);
		boolean validContentType = false;
		String fileContentType = detectMimeType.apply(file);
		if (fileContentType == null) {
			fileContentType = magicMatch.apply(file);
		}
		
		String[] validContentTypesArray = validContentTypes;
		Arrays.stream(validContentTypesArray).forEach(System.out::println);
		validContentType = Arrays.stream(validContentTypesArray).anyMatch(fileContentType::equals);
		
		_log.debug("fileContentType :" + fileContentType);
		_log.debug("validContentType :" + validContentType);
		return validContentType;
	}
	
	private static Function<File, String> contentType = file -> {
		InputStream is;
		String mimeType = null;
		try {
			is = new BufferedInputStream(new FileInputStream(file));
			mimeType = URLConnection.guessContentTypeFromStream(is);
		} catch (IOException e) { e.printStackTrace(); }
		
		
		return mimeType;	
	};
	
	private static Function<File, String> magicMatch = file -> {
		String mimeType = "";
		try {
			mimeType = Magic.getMagicMatch(file, false).getMimeType();
		} catch (MagicParseException | MagicMatchNotFoundException | MagicException e) {
			mimeType = "notFound/magicMatch";
		}
		if(mimeType != null & mimeType.contains("zip")){
			mimeType = "image/png";
		}
		return mimeType;	
	};
	
	private void initConfig(PortletRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		PortletInstance instance = PortletInstance.fromPortletInstanceKey(
				FilesystemAccessPortletKeys.FILESYSTEM_ACCESS);
			
		Layout l = themeDisplay.getLayout();

			try {

				// get the config
				portletInstanceConfig =
					configurationProvider.getPortletInstanceConfiguration(
						FilesystemAccessPortletInstanceConfiguration.class, l,
						instance);
			}
			catch (ConfigurationException ce) {
				_log.error("Error getting instance config.", ce);
			}
	}
	
	/**
	 * setFilesystemAccessService: Sets the filesystem access service instance to use.
	 * @param filesystemAccessService The filesystem access service instance.
	 */
	@Reference(unbind = "-")
	protected void setFilesystemAccessService(
		final FilesystemAccessService filesystemAccessService) {

		_filesystemAccessService = filesystemAccessService;
	}

	private FilesystemAccessService _filesystemAccessService;

	/**
	 * getFilesystemAccessPortletInstanceConfiguration: Returns the instance config obj.
	 * @return FilesystemAccessPortletInstanceConfiguration The instance config object to use.
	 */
	public FilesystemAccessPortletInstanceConfiguration
		getFilesystemAccessPortletInstanceConfiguration() {

		return portletInstanceConfig;
	}

	/**
	 * setConfigurationProvider: Sets the config provider to use.
	 * @param configurationProvider The config provider to use.
	 */
	@Reference
	protected void setConfigurationProvider(
		ConfigurationProvider configurationProvider) {

		this.configurationProvider = configurationProvider;
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet com.liferay.filesystemaccess.web2 - 1.0.1, Cargado");
		}
	}

	private static final Log _log = LogFactoryUtil.getLog(
		FilesystemAccessDisplayContext.class);

	private ConfigurationProvider configurationProvider;
	private FilesystemAccessPortletInstanceConfiguration portletInstanceConfig =
		null;
}
