package com.liferay.filesystemaccess.portlet.config;

import aQute.bnd.annotation.metatype.Meta;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

/**
 * class FilesystemAccessPortletInstanceConfiguration: Instance configuration for
 * the portlet configuration.
 * @author dnebinger
 */
@ExtendedObjectClassDefinition(
	category = "Platform",
	scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
)
@Meta.OCD(
	localization = "content/Language",
	name = "FilesystemAccessPortlet.portlet.instance.configuration.name",
	id = "com.liferay.filesystemaccess.portlet.config.FilesystemAccessPortletInstanceConfiguration"
)
public interface FilesystemAccessPortletInstanceConfiguration {

	/**
	 * rootPath: This is the root path that constrains all filesystem access.
	 */
	@Meta.AD(deflt = "/opt/docs_tramites", required = false)
	public String rootPath();
	
	/**
	 * validContentTypes: The list of content types separated by commas.
	 * 
	 * @see <a href="http://www.freeformatter.com/mime-types-list.html">Mime Types List</a>
	 * 
	 */
	@Meta.AD(deflt = "application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,image/jpeg,image/x-citrix-jpeg,image/png,image/x-citrix-png,image/x-png,image/tiff,image/gif", required = false)
	public String[] validContenTypes();

	/**
	 * validExentions: The list of valid upload extensions separated by commas.
	 * 
	 * @see validContenTypes
	 * 
	 */
	@Meta.AD(deflt = "doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,png,tiff,gif", required = false)
	public String[] validExentions();
	/**
	 * showPermissions: This is a flag whether permissions should be displayed or not.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean showPermissions();

	/**
	 * deletesAllowed: This is a flag that determines whether files/folders can be deleted or not.
	 */
	@Meta.AD(deflt = "false", required = false)
	public boolean deletesAllowed();

	/**
	 * uploadsAllowed: This is a flag that determines whether file uploads are allowed.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean uploadsAllowed();

	/**
	 * downloadsAllowed: This is a flag that determines whether file/folder downloads are allowed or not.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean downloadsAllowed();

	/**
	 * editsAllowed: This is a flag that determines whether inline editing is allowed.
	 */
	@Meta.AD(deflt = "false", required = false)
	public boolean editsAllowed();

	/**
	 * addsAllowed: This is a flag that determines whether files/folders can be added or not.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean addsAllowed();

	/**
	 * viewSizeLimit: This is a size limit that determines whether a file can be viewed in the browser.
	 */
	@Meta.AD(deflt = "-1", required = false)
	public long viewSizeLimit();

	/**
	 * downloadableFolderSizeLimit: This defines the size limit for downloading folders.  This is the max size (uncompressed)
	 * that a folder can be in order to be downloadable.
	 */
	@Meta.AD(deflt = "-1", required = false)
	public long downloadableFolderSizeLimit();

	/**
	 * downloadableFolderItemLimit: This defines the max number of files that can be in a folder for downloading.
	 */
	@Meta.AD(deflt = "-1", required = false)
	public int downloadableFolderItemLimit();

}