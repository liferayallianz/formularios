
<%@ include file="/init.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="mx.com.allianz.commons.catalogos.dto.DocumentoDTO"%>

<%

	List<DocumentoDTO> documentos = new ArrayList<>();
	Object object = request.getAttribute("documentos");
	if (object != null && object instanceof List) {
		documentos = (List<DocumentoDTO>) object;
	}
	
	// okay, from current path we need to build a list of urls...

	String currentBaseName = FilenameUtil.getBaseName(currentPath);
	String parentPath = FilenameUtil.getParentPath(currentPath);
	String tempName;

	boolean onRootPath = StringPool.SLASH.equals(currentPath);

// prep the portletURL for the list.

	PortletURL portletUrl = renderResponse.createRenderURL();

// these are the default values

	portletUrl.setParameter("mvcPath", "/view.jsp");
	portletUrl.setParameter("redirect", currentURL);
	portletUrl.setParameter("currentPath", currentPath);

	PortletURL iterUrl = renderResponse.createRenderURL();

// these are the default values

	iterUrl.setParameter("mvcPath", "/view.jsp");
	iterUrl.setParameter("redirect", currentURL);
	iterUrl.setParameter("currentPath", currentPath);

// we need to create a list of links

	List<String> linkUrls = new LinkedList<>();
	List<String> linkTitles = new LinkedList<>();

	if (! onRootPath) {

		// while the parent path has length

		while (parentPath.length() > 1) {

			// the base name of the parent is our next link name

			tempName = FilenameUtil.getBaseName(parentPath);

			// update the parameter values

			portletUrl.setParameter("nextPath", parentPath);
			portletUrl.setParameter("linkName", tempName);

			// insert the url at the front of the list

			linkUrls.add(0, portletUrl.toString());
			linkTitles.add(0, tempName);

			// compute the new parent path

			parentPath = FilenameUtil.getParentPath(parentPath);
		}
	}
%>

<p class="formularioTituloEncabezado"><liferay-ui:message key="welcome-msg"/></p>
<liferay-ui:error exception="<%= OperationNotAllowedException.class %>" message="error-adds-are-not-authorized" />
<liferay-ui:error exception="<%= DuplicateFileException.class %>" message="error-duplicate-name" />
<liferay-ui:error exception="<%= CreateFilesystemItemException.class %>" message="error-creating-file-folder" />


<aui:button-row>
	
	
	
	<c:if test="<%= allowUploads %>">
		<p class="formularioEtiquetaTexto"><liferay-ui:message key="instructions-msg"/> <button class="btn btn-default" data-target="#<portlet:namespace/>uploadModal" data-toggle="modal" id="<portlet:namespace/>showUploadBtn"><liferay-ui:message key="Subir Archivo" /></button></p>
		<div aria-labelledby="<portlet:namespace/>UploadModalLabel" class="fade in lex-modal modal" id="<portlet:namespace/>uploadModal" role="dialog" tabindex="-1" style="display: none;">
			<div class="modal-dialog modal-full-screen"  style="height:500px;">
				<div class="modal-content">
					<portlet:actionURL name="/upload_file" var="uploadActionURL" />

					<div id="<portlet:namespace/>fm4">
						<aui:form id="form5" method="post" name="form5" cssClass="container lfr-dynamic-form mtm" enctype="multipart/form-data">
							<aui:input name="<%= ActionRequest.ACTION_NAME %>" type="hidden" />
							<aui:input name="currentPath" type="hidden" value="<%= currentPath %>" />
							<aui:input name="uploadFileName" type="hidden" />
							<aui:input name="uploadTime" type="hidden" value="<%= timeForFolder %>" />

							<div class="modal-header">
								<button aria-labelledby="Close" class="btn btn-default close" data-dismiss="modal" role="button" type="button">
									<svg aria-hidden="true" class="lexicon-icon lexicon-icon-times">
										<use xlink:href="<%= themeDisplay.getPathThemeImages() + "/lexicon/icons.svg" %>#times" />
									</svg>
								</button>

								<button class="btn btn-default modal-primary-action-button visible-xs" type="button">
									<svg aria-hidden="true" class="lexicon-icon lexicon-icon-check">
										<use xlink:href="<%= themeDisplay.getPathThemeImages() + "/lexicon/icons.svg" %>#check" />
									</svg>
								</button>

								<h4 class="modal-title" id="<portlet:namespace/>UploadModalLabel"><liferay-ui:message key="Subir Archivo" /></h4>
							</div>

							<div class="modal-body">
							
								<aui:fieldset>
									<aui:input cssClass="formularioCampoTexto" label="" name="uploadFileSelect" type="file" >
								    	<aui:validator name="required" errorMessage="El archivo es requerido"/>
								    	<aui:validator name="acceptFiles" >'doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,png,tif,gif'</aui:validator>
								    </aui:input>
								    <p class="formularioEtiquetaTexto">Formatos: Word, Excel, Power Point, PDF, JPG, JPEG, PNG, GIF m�ximo 10 MB.</p>
									</br>
									<aui:select cssClass="formularioCampoTexto" label="" id="upload_form_documentos" name="documentos" style="width:100%;" required="true" errorMessage="El tipo de documento es requerido">
										<aui:option selected="true" value="">
									        Seleccione un tipo de documento
									    </aui:option>
									    <% for(DocumentoDTO documento: documentos){ %>
								     	<aui:option value="<%=documento.getIdDocumentos()%>"><%=documento.getDescripcion()%></aui:option>
									   	<%  }; %>
									</aui:select>
									
								</aui:fieldset>
								<aui:input name="_dontcare" type="hidden" />
						        <span id="group-error-block"></span>
						        							
							</div>

							<div class="modal-footer">
								<button class="btn btn-default close-modal" id="<portlet:namespace/>uploadBtn" name="<portlet:namespace/>uploadBtn" type="button"><liferay-ui:message key="Subir" /></button>
								<button class="btn btn-link close-modal" data-dismiss="modal" type="button"><liferay-ui:message key="Cancelar" /></button>
							</div>
						</aui:form>
					</div>
				</div>
			</div>
		</div>
	</c:if>

	<!-- Now we need a path of links for each parent directory -->
</aui:button-row>
<div id="div_tabla" style="display:none;">

</div>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator,node,liferay-alert">
console.log("com.liferay.filesystemaccess.web2 v1.0.1");

	var uploadFile = A.one('#<portlet:namespace/>showUploadBtn');

	if (uploadFile) {
		uploadFile.after('click', function() {
			var uploadText = A.one('#<portlet:namespace/>uploadFile');

			if (uploadText) {
				uploadText.val('');
				uploadText.focus();
			}
		});
	}

	var uploadBtnVar = A.one('#<portlet:namespace/>uploadBtn');
	<portlet:resourceURL var="uploadActionURL" >
	<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.UPDATE %>" />
	</portlet:resourceURL>
	if (uploadBtnVar) {
		uploadBtnVar.after('click', function() {
			var fm = A.one('#<portlet:namespace/>form5');
			console.log('--------------------Liferay.form--------------------');
			var formValidator = Liferay.Form.get('<portlet:namespace />form5').formValidator;
			formValidator.validate();
			if (!formValidator.hasErrors()){
				A.io.request("<%= uploadActionURL %>", {
					  form: {
						  
						  id:"<portlet:namespace/>form5",
						  upload: true
					  },
					  dataType:'text',
					  on: {
		                    complete: function(data){
								
		                        console.log("Objeto respuesta !!!!!!!!!!!!");
		                        console.log(data);
		                        console.log("Objeto respuesta !!!!!!!!!!!!");
		                        var details = data.details;
		                        var detail1 = details[1];
		                        var rT = detail1.responseText;
		                        var response = '';
		                        try {
		                        	var response = JSON.parse(data.details[1].responseText);
		                        }catch(err) {
		                        	console.log(err.message)
		                        }
		                        console.log("response = " + response);
		                        if (!response){
									console.log("server error" + data.details[1].responseText);
		                        } else if(response.errorMessage){
									console.log("error" + response.errorMessage);

									var alert = new Liferay.Alert({
                                        closeable: true,
                                        message: response.errorMessage,
			                            type: 'warning'
			                        });
									alert.render('#group-error-block');
									
		                        	setTimeout(function(){
		                        		try {
		                        			if(alert){
				                        		alert.destroy();
		                        			}
		                        		}catch(err) {console.log(err);}
		                        	}, 4000); 

								} else {
							  		refrescarTabla(response);
			                        document.getElementById('<portlet:namespace/>form5').reset();
			                        console.log(A.one('#<portlet:namespace/>upload_form_documentos'));
			                        A.one('#<portlet:namespace/>upload_form_documentos').set('selectedIndex',0);
			                        document.getElementsByClassName('close-modal')[1].click();
								}
								
		                    }
		                }
				});
			}
		});
	}
	
	function refrescarTabla(lista){
			 var row = null;
			 var divTabla = document.getElementById("div_tabla");
             var td = null;
             var bold= null;
             var cellValue=null;
             var table = $('<table class="table">');
			 for(var position in lista) {
				 
				var elem = lista[position];
				(function (elemento) {
					if (elemento) {
					     var nombreArchivo = elemento.nombreArchivo;
					     var valorSelect = elemento.docId;
					     if (nombreArchivo && valorSelect) {
					    	     var self = this;
					    	 	 console.log(position + " Nombre = " + nombreArchivo.trim() + " Select = " + valorSelect.trim());
						         self.nombreArchivoReal = elemento.nombreRealArchivo;
						         row = $('<tr id="' + nombreArchivoReal + '">');
				                 td = $('<td >');
				                 console.log("id columna:"+nombreArchivoReal);
				                 bold = $("<p class=\"formularioEtiquetaTexto\">");
				                 bold.append("<span style=\"font-weight: bold;\">Nombre:</span> ");
				                 bold.append(nombreArchivo).appendTo(td);
				                 bold = $("<p class=\"formularioEtiquetaTexto\">");
				                 bold.append("<strong>Tipo Doc:</strong> ");
				                 bold.append(valorSelect).appendTo(td);
				                 td.appendTo(row);
				             	var btn = $("<input>",{
				                    type:"button",
				                    val:"Eliminar"
				                });
				             	btn.addClass("btn btn-default");
				             	
				             	btn.click(clickFunct2);
				             	td = $("<td>");
				             	td.append(btn).appendTo(row);
				             	
				             	row.appendTo(table);
					     }
				   	  
				 	}				
				})(elem);
				
			   
			 }
			 
			 divTabla.innerHTML = "";
			 divTabla.style.display = "block";
			 table.appendTo("#div_tabla");

		 
	}
	
	var clickFunct2 = function(){
		var fileName = this.parentNode.parentNode.id;
	    
	    <portlet:resourceURL var="deleteURL" >
	    	<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.DELETE %>" />
	    	<portlet:param name="currentPath" value="<%= currentPath %>" />
	    	<portlet:param name="uploadTime" value="<%= timeForFolder %>" />
	</portlet:resourceURL>
		A.io.request("<%= deleteURL %>", {
			  data:{
				<portlet:namespace/><%=Constants.PARM_DOC_ID%>: fileName.split("__")[0],
				<portlet:namespace/><%=Constants.PARM_UPLOAD_NAME%>: fileName.substring(fileName.indexOf("__")+2)
			  },
			  dataType:'json',
			  on: {
                    complete: function(data){
                    	console.log("Objeto respuesta elimina!!!!!!!!!!!!");
                        console.log(data);
                        console.log("Objeto respuesta elimina!!!!!!!!!!!!");
						
                        var response = JSON.parse(data.details[1].responseText);

                        if (response.errorMessage){
							console.log("error" + response.errorMessage);
						} else {
							var trAEliminar = document.getElementById( fileName );
                        	
                        	trAEliminar.parentNode.removeChild(trAEliminar);
						}                    
                    }
                }
			});
	};

	var fileSelect = A.one('#<portlet:namespace />uploadFileSelect');
	var title = A.one('#<portlet:namespace />uploadFileName');

	fileSelect.on('change',function(event) {
		var ct = event.currentTarget;
		var val = ct.val();

		if (val.indexOf('C:\\fakepath\\') != -1) {
			val = val.replace(/C:\\fakepath\\/, '');
		}

		title.attr('value', val);
	});
	
	Liferay.on('validaFileUplaod', function(event){
		var fileFolder = '<%= timeForFolder %>';
		console.log('fileFolder = ' + fileFolder);
		var eventoRespuesta = {};
		eventoRespuesta.fileFolder = fileFolder;
		eventoRespuesta.valido = true;
		Liferay.fire('validaFileUplaodRespuesta', eventoRespuesta );
	});
	
</aui:script>

<%!
	private static Log _vlog = LogFactoryUtil.getLog("com.liferay.filesystemaccess.portlet.view_jsp");
%>