<%@ include file="/init.jsp" %>

<%@page import="mx.com.allianz.commons.dto.cliente.AseguradoDTO"%>

<% 
		Boolean tramiteClienteHabilitado = false;
        Object object = request.getAttribute("tramiteClienteHabilitado");
		if (object != null && object instanceof Boolean){
			tramiteClienteHabilitado = (Boolean) object;
		}
%>

<div id="divAseguradosPolzaCliente" style="display:none;">
	<aui:form action="#" name="fm_asegurados_poliza_cliente">
		<aui:input name="version-datos.afectado.cliente.web" type="hidden" value="1.0.3"  />
		<aui:select cssClass="formularioCampoTexto" id="asegurado_poliza_cliente" name="aseguradoCliente" label="" onChange="saveAseguradoPoliza(this);" required="<%=tramiteClienteHabilitado %>" errorMessage="Debe seleccionar un asegurado" >
		    <aui:option selected="true" value="">
		        <liferay-ui:message key="Seleccione un Asegurado" />
		    </aui:option>
		    
    	</aui:select>
    	<aui:input type="hidden" id="hidden_asegurado_poliza_cliente" name="hiddenAseguradoNumeroPolizaSelected" value=""/>
    	
	</aui:form>
</div>



<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("Afectado Cliente v1.0.3");
var tramiteClienteHabilitadoJS = <%= tramiteClienteHabilitado %>;
console.log("afectado seleccion tramiteClienteHabilitadoJS = " + tramiteClienteHabilitadoJS);

Liferay.provide(window, 'saveAseguradoPoliza', function(param) {
	var selectedIndex = param.selectedIndex;
	A.one('#<portlet:namespace />hidden_asegurado_poliza_cliente').set('value',selectedIndex); 
});

var formValidator = Liferay.Form.get('<portlet:namespace />fm_asegurados_poliza_cliente').formValidator;

//add valid form
Liferay.on('validaDatosAfectado', function(event){
	console.log("validaDatosAfectado");

// 	if (tramiteClienteHabilitadoJS) {
		var eventoRespuesta = {};
		console.log("formValidator = " + formValidator);

		formValidator.validate();
		eventoRespuesta.valido = !formValidator.hasErrors();
		console.log("eventoRespuesta.valido = " + eventoRespuesta.valido);

		if(eventoRespuesta.valido){
			var nombreAfectadoSelect = A.one('#<portlet:namespace />asegurado_poliza_cliente');
			console.log("nombreAfectadoSelect = " + nombreAfectadoSelect);

			var nombreCompletoAfectadoOption = nombreAfectadoSelect.get('options')._nodes[nombreAfectadoSelect.get('selectedIndex')];
			console.log("start option");
			console.log(nombreCompletoAfectadoOption);
			console.log(nombreCompletoAfectadoOption.text);
			console.log(nombreAfectadoSelect.get('value'));

			console.log("end option");

			var nombreCompletoAfectado = nombreCompletoAfectadoOption.text;
			var nombreSeccionado = nombreCompletoAfectado ? nombreCompletoAfectado.split(" ") : ["",""];
			console.log(nombreSeccionado);
			var nombreAfectado = "";
			var longitudNombre = nombreSeccionado.length;
			if ( longitudNombre > 3 )
				nombreAfectado = nombreSeccionado.slice(0,longitudNombre - 2).join(" ");
			else nombreAfectado = nombreSeccionado[0];
			
			console.log("Valor del select de afectado:"+nombreAfectadoSelect.get('value'));
			eventoRespuesta.idAfectado =  nombreAfectadoSelect.get('value');
			eventoRespuesta.nombreAfectado = nombreAfectado.trim();
			eventoRespuesta.apellidoPaternoAfectado = nombreSeccionado.length > 2 ? nombreSeccionado[nombreSeccionado.length-2] : nombreSeccionado[nombreSeccionado.length-1] ;
			eventoRespuesta.apellidoMaternoAfectado = nombreSeccionado.length > 2 ? nombreSeccionado[nombreSeccionado.length-1] : "";
		}
		console.log("eventoRespuesta de datos-afectado-cliente-web");
		console.log(eventoRespuesta);
		Liferay.fire('validaDatosAfectadoRespuesta', eventoRespuesta );
// 	}
});

</aui:script>

<script type="text/javascript"> 
Liferay.on('cargaDatosAfectado', function(event){
	new AUI().use(function(A) {
		console.log("Por cargar datos del asegurado");
		console.log(event);
		var producto = null;
		var productos = event.productos;
		for  (var i = 0; i < productos.length; i++){
			if ( productos[i].descripcionProducto && productos[i].descripcionProducto == event.producto ){
				producto = {asegurados:productos[i].asegurados[0]};
				console.log(producto);
			}
		}
		if ( producto ){
			presentarAseguradosPolizaFunc(producto.asegurados,A);
			var aseguradoSelect = A.one('#<portlet:namespace />asegurado_poliza_cliente');
			aseguradoSelect.set('value',event.idAfectado);
			A.one('#<portlet:namespace />hidden_asegurado_poliza_cliente').set('value',aseguradoSelect.selectedIndex);
			
		}
	});
});
console.log("Evento cargaDatosAfectadoo levantado");
Liferay.on('presentarAseguradosPoliza', function(event) {
	
	new AUI().use(function(A) {
		console.log("Seest�  ejecutando desde persentarAseguradosEvent");
		presentarAseguradosPolizaFunc(event.asegurados,A);
	});
});

Liferay.provide(window,'obtenerIdAsegurado', function(event) {
	
	new AUI().use(function(A) {
		return A.one('#<portlet:namespace />asegurado_poliza_cliente').get("value");
	});
});

function presentarAseguradosPolizaFunc(asegurados,A){
	console.log("presentarAseguradosPolizaFunc");
	document.getElementById('divAseguradosPolzaCliente').style.display = 'block';
    console.log(asegurados);
	if (asegurados) { 
	   	console.log("Porcargar los  aseguradops:"+asegurados.length);
		var aseguradosSelect = A.one('#<portlet:namespace />asegurado_poliza_cliente');
   		aseguradosSelect.html("");	
   		aseguradosSelect.append("<option value=''>Seleccione un asegurado </option>");
        for (var j = 0; j < asegurados.length; j++) {
        	var asegurado = asegurados[j];
        	console.log(asegurado);
        	aseguradosSelect.append("<option value='" + asegurado.idAsegurado + "'>" + 
        			( asegurado.nombre ? asegurado.nombre : asegurado[0].contratante) + "</option>");
        }
	}
}


</script>