<%@ include file="/init.jsp" %>
<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ page import="mx.com.allianz.tramites.datos.cliente.sender.portlet.DatosClienteTramiteSenderPortlet" %>

<% 
		String tramite = null;
        Object object = request.getAttribute(DatosClienteTramiteSenderPortlet.LLAVE_TRAMITE);
		if (object != null && object instanceof String){
			tramite = (String) object;
		}
%>

<portlet:actionURL var="tramiteDatosClienteActionURL" name="processAction">
</portlet:actionURL>
<portlet:resourceURL var="actualizaTramite" />

<aui:form name="fm-cliente-tramite" action="<%=tramiteDatosClienteActionURL.toString()%>">
		<aui:input name="<%= Constants.CMD %>" type="hidden" value="cmd_next_page"  />
<aui:input name="version-datosclientetramitesender" type="hidden" value="1.0.1"  />
    	<aui:button-row>
        	<aui:button name="saveButton" class="btn btn-primary pull-right" type="button" value="datosclientetramitesender_DatosClienteTramiteSender.send" id="submitBtn" onclick = "validateClienteTramiteForm('cmd_next_page');"/>
    		<aui:button class="btn pull-right" name="cancelButton" type="button" value="datosclientetramitesender_DatosClienteTramiteSender.clean" onclick="validateClienteTramiteForm('cmd_back_page');" />
    	</aui:button-row>
</aui:form>


<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">

console.log("datosclientetramitesender v1.0.1");

var tramite = <%=tramite%>;
console.log("Tramite cargado");
console.log(tramite);

// Liferay.on('validaReglasAlIniciarTramiteCliente', function(event){
	
	if ( tramite && tramite.producto ){
		console.log("Cargando datos de poliza");
		Liferay.fire('cargadDatosPoliza', tramite );
 		Liferay.fire("cargaDatosAfectado", tramite);
 		var nombreContratante = "";
 		if (tramite.nombreContratante)
 			nombreContratante = tramite.nombreContratante;
 		else 
 			for(var i = 0; i < tramite.productos.length; i++ )
 				if ( tramite.producto == tramite.productos[i].codigoProducto )
 					nombreContratante = tramite.productos[i].contratante.nombre;
 		Liferay.fire("presentarContratantePoliza", 
 				{	contratante:{nombre:nombreContratante,
 						apellidoPaterno:tramite.apellidoPaternoContratante,
 						apellidoMaterno:tramite.apellidoMaternoContratante}});
 		Liferay.fire("presentarAfectadoPoliza", 
 				{	afectado:{idAfectado:tramite.idAfectado,
 						nombre:tramite.nombreAfectado,
 						apellidoPaterno:tramite.apellidoPaternoAfectado,
 						apellidoMaterno:tramite.apellidoMaternoAfectado}});
 		
	}
// });
</aui:script>

<script type="text/javascript"> 
var formsValidSectionClienteTramite = [];

Liferay.provide(window, 'validateClienteTramiteForm', function(accion) {
	console.log("validateClienteTramiteForm:" + accion);
	$("#<portlet:namespace/><%= Constants.CMD %>").val(accion);
	formsValidSectionClienteTramite = [];
	Liferay.fire('validaDatosAfectado', {} );
	Liferay.fire('validaDatosContratante', {} );
	Liferay.fire('validaNumeroPoliza', {} );
	console.log("fire end validateClienteTramiteForm");
});
console.log(validateClienteTramiteForm);

Liferay.on('validaDatosAfectadoRespuesta', validaSeccionTramite);
Liferay.on('validaDatosContratanteRespuesta', validaSeccionTramite);
Liferay.on('validaNumeroPolizaRespuesta', validaSeccionTramite);

function validaSeccionTramite(respuestaSeccion) { 
	console.log("desde la respesta la seccion es" + respuestaSeccion.valido);
// 	if (respuestaSeccion.valido) {
		formsValidSectionClienteTramite.push(respuestaSeccion.valido);
		callServeResourceTramite(respuestaSeccion);
// 		sendSubmit();
// 	}
}

function sendSubmitTramite() {
	console.log("submit formsValidSectionClienteTramite.length" + formsValidSectionClienteTramite.length);
<%-- 	if($("#<portlet:namespace/><%= Constants.CMD %>").val() == 'cmd_back_page' || (formsValidSectionClienteTramite.length % 3) === 0) { --%>
	if ( formsValidSectionClienteTramite.length % 2 === 0 ) {
		console.log("submit");
		
		if($("#<portlet:namespace/><%= Constants.CMD %>").val() == 'cmd_next_page'){
		
			console.log(formsValidSectionClienteTramite);
			var todosVerdaderos = true;
			
// 			for(llave in formsValidSectionClienteTramite){
// 				todosVerdaderos = todosVerdaderos && formsValidSectionClienteTramite[llave];
// 				console.log("if de llave todos verdaderos: " + formsValidSectionClienteTramite[llave]);
// 				if(!todosVerdaderos) return;
				
// 			}
		}
		new AUI().use(function(A) {
			console.log("submit send" + A);
			var formaClienteTramite = A.one('#<portlet:namespace/>fm-cliente-tramite');
			console.log("submit formaClienteTramite" +formaClienteTramite);
			formaClienteTramite.submit();
		});
	} else if ( formsValidSectionClienteTramite.length % 2 === 0){
		console.log("Al parecer esto no va a avanzar, falla alguno de los eventos");
	}
}

function callServeResourceTramite(seccion) {
	
	console.log(seccion);
	$.post('<%=actualizaTramite.toString()%>',{
		nombreContratante: seccion.nombreContratante,
		apellidoPaternoContratante: seccion.apellidoPaternoContratante,
		apellidoMaternoContratante: seccion.apellidoMaternoContratante,
		producto: seccion.emisorPoliza,
		numeroPoliza: seccion.numeroPoliza,
		idAfectado: seccion.idAfectado ? seccion.idAfectado : "",
		nombreAfectado: seccion.nombreAfectado,
		apellidoPaternoAfectado: seccion.apellidoPaternoAfectado,
		apellidoMaternoAfectado: seccion.apellidoMaternoAfectado
	},function(data, status){
		sendSubmitTramite();
	}); 
}



</script>