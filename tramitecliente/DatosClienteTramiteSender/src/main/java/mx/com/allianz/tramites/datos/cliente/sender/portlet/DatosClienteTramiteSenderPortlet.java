package mx.com.allianz.tramites.datos.cliente.sender.portlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.namespace.QName;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.tramites.datos.cliente.sender.portlet.configuration.TramiteClientePortletConfiguration;

@Component(
	configurationPid = "mx.com.allianz.tramites.datos.cliente.sender.portlet.configuration.TramiteClientePortletConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // Fin parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Tramite Cliente Sender",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest",
		//Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-publishing-event=clienteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/clienteTramiteClienteEvent",
		"javax.portlet.supported-processing-event=clienteTramiteEvent;http://mx-allianz-liferay-namespace.com/events/clienteTramiteEvent",
		"javax.portlet.supported-publishing-event=clienteFirmadoEvent;http://mx-allianz-liferay-namespace.com/events/clienteFirmadoEvent",
		"javax.portlet.supported-processing-event=clienteFirmadoEvent;http://mx-allianz-liferay-namespace.com/events/clienteFirmadoEvent",
		"javax.portlet.supported-publishing-event=tramiteClienteHabilitadoEvent;http://mx-allianz-liferay-namespace.com/events/tramiteClienteHabilitadoEvent",
		"javax.portlet.supported-processing-event=tramiteClienteHabilitadoEvent;http://mx-allianz-liferay-namespace.com/events/tramiteClienteHabilitadoEvent",
		"javax.portlet.supported-publishing-event=datosTramiteEvent;http://mx-allianz-liferay-namespace.com/events/datosTramiteEvent",
		"javax.portlet.supported-processing-event=datosTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/datosTramiteClienteEvent",
//		"javax.portlet.supported-publishing-event=datosTramiteSolicitanteEvent;http://mx-allianz-liferay-namespace.com/events/datosTramiteSolicitanteEvent",

	},
	service = Portlet.class
)
public class DatosClienteTramiteSenderPortlet extends MVCPortlet {
	
	public static final String LLAVE_TRAMITE = "tramite"; 
	public static final String LLAVE_CLIENTE_FIRMADO = "clienteFirmado"; 
	public static final String LLAVE_TRAMITE_CLIENTE_HABILITADO = "tramiteClienteHabilitado"; 
	public static final String CMD_BACK_PAGE = "cmd_back_page";
	public static final String CMD_NEXT_PAGE = "cmd_next_page";
	public static final String LLAVE_DATOS_TRAMITE_CLIENTE_EVENT = "datosTramiteClienteEvent";
	public static final String LLAVE_DATOS_TRAMITE_CLIENTE = "tramiteRecCliente";


	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		_log.debug("DatosClienteTramiteSenderPortlet -> procesando tramite");

		String currentURL = PortalUtil.getCurrentURL(actionRequest);
		_log.info("DatosClienteTramiteSenderPortlet -> procesando tramite -> currentURL = " + currentURL);
		
		Gson gson = new Gson();
		PortletSession session = actionRequest.getPortletSession();
		TramiteDto tramite = buildClienteTramite(session);
		
		_log.info("cmd: " + ParamUtil.getString(actionRequest, Constants.CMD));
		
		if ( CMD_NEXT_PAGE.equals(ParamUtil.getString(actionRequest, Constants.CMD))){
		
			QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/clienteTramiteClienteEvent", "clienteTramiteClienteEvent");
		    actionResponse.setEvent(qName, gson.toJson(tramite));
			
		    if(tramite.isTramiteClienteHabilitado()){
		    	String cliente = getParamFromSession(LLAVE_CLIENTE_FIRMADO, String.class, null, session);
				System.out.println("DatosClienteTramiteSenderPortlet -> processAction -> cliente firmado = " + cliente);
				if ( cliente != null && cliente.length() > 30 ){
		    		_log.info("*_*_*-*_*_*_*_*_*_*_*_Las cosas van bien:" + cliente);
					QName qNameCliente = new QName("http://mx-allianz-liferay-namespace.com/events/clienteFirmadoEvent", "clienteFirmadoEvent");
					actionResponse.setEvent(qNameCliente, cliente);
				} else {
		    		_log.info("*_*_*_*_*_*_*_*_*_*_*_Algo no iba bien:" + (cliente != null ? cliente : "El cliente era nulo"));
		    	}
				
		    	Boolean clienteTramiteHabilitado = getParamFromSession(LLAVE_TRAMITE_CLIENTE_HABILITADO, Boolean.class, null, session);
				System.out.println("DatosClienteTramiteSenderPortlet -> processAction -> clienteTramiteHabilitado = " + clienteTramiteHabilitado);
				QName qNameClienteHabilitado = new QName("http://mx-allianz-liferay-namespace.com/events/tramiteClienteHabilitadoEvent", "tramiteClienteHabilitadoEvent");
				actionResponse.setEvent(qNameClienteHabilitado, clienteTramiteHabilitado);
			}
	
			String redirectURL = Arrays.stream(_configuration.redirectPair())
					  .map(uri -> uri.split(":"))
					  .peek(rurl -> _log.info("redirectURL element = " + rurl))
					  .filter(pairURI -> currentURL.contains(pairURI[0]))
					  .map(pairURI -> pairURI[1])
					  .peek(rurl -> _log.info("redirectURL element found = " + rurl))
					  .findAny()
					  .orElse("/solicitante-tramite");
				
				_log.info("DatosClienteTramiteSenderPortlet -> processAction -> redirectURL = " + redirectURL);
				System.out.println("DatosClienteTramiteSenderPortlet -> processAction -> redirectURL = " + redirectURL);
			
			_log.info("DatosClienteTramiteSenderPortlet -> processAction -> redirectURL = " + redirectURL);
			actionResponse.sendRedirect(redirectURL);
		} else {
			QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/datosTramiteEvent", "datosTramiteEvent");
		    actionResponse.setEvent(qName, gson.toJson(tramite));
			actionResponse.sendRedirect("/group/guest/general-tramite");
		}
	}
	
	public TramiteDto buildClienteTramite(PortletSession session){
		final TramiteDto tramite = getParamFromSession(LLAVE_TRAMITE, TramiteDto.class, TramiteDto::new, session);
				
		if(_log.isDebugEnabled()) {
			_log.debug("tramite: " + tramite);
		}
		Arrays.stream(FormularioClienteTramiteCampos.values())
			  .forEach(campo -> 
				  	campo.buildTramite(tramite, campo.getParamFromSession(session))
			  );
		
		return tramite;
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		_log.info("DatosClienteTramiteSenderPortlet -> render -> inicio");
		PortletSession session = renderRequest.getPortletSession();	
		Gson gson = new Gson();

		TramiteDto tramite = getParamFromSession(LLAVE_TRAMITE, TramiteDto.class, null, session);
    	String cliente = getParamFromSession(LLAVE_CLIENTE_FIRMADO, String.class, null, session);
    	Boolean clienteTramiteHabilitado = getParamFromSession(LLAVE_TRAMITE_CLIENTE_HABILITADO, Boolean.class, null, session);

//		_log.info("DatosClienteTramiteSenderPortlet -> render -> tramite = " + tramite);
//		_log.info("DatosClienteTramiteSenderPortlet -> render -> clienteFirmado = " + cliente);
//		_log.info("DatosClienteTramiteSenderPortlet -> render -> clienteTramiteHabilitado = " + clienteTramiteHabilitado);
		
		renderRequest.setAttribute(LLAVE_TRAMITE, gson.toJson(tramite));
		renderRequest.setAttribute(LLAVE_CLIENTE_FIRMADO, cliente);
		renderRequest.setAttribute(LLAVE_TRAMITE_CLIENTE_HABILITADO, clienteTramiteHabilitado);

		super.render(renderRequest, renderResponse);
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		if(_log.isDebugEnabled()) {
			_log.debug("FormularioClienteTramiteCampos -> serveResource ");
		}
		
		PortletSession session = resourceRequest.getPortletSession();
		Arrays.stream(FormularioClienteTramiteCampos.values())
			  .forEach(campo -> {
				  _log.info("Campo -> "+campo.toString() + ", valor ->" + campo.getParamFromRequest(resourceRequest));
				   campo.uploadToSession(session, campo.getParamFromRequest(resourceRequest));}
			  );
		
		super.serveResource(resourceRequest, resourceResponse);
	}
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		if(_log.isDebugEnabled()) {
			_log.info("DatosClienteTramiteSenderPortlet -> processEvent cliente");
		}
		Optional<Event>  event = Optional.ofNullable(request)
										 .map(EventRequest::getEvent);
		try {
			
			uploadEventStringParamToSession("clienteTramiteEvent",LLAVE_TRAMITE, TramiteDto.class, session, event.get());
			uploadEventParamToSession("clienteFirmadoEvent",LLAVE_CLIENTE_FIRMADO, String.class, session, event.get());
			uploadEventParamToSession("tramiteClienteHabilitadoEvent",LLAVE_TRAMITE_CLIENTE_HABILITADO, Boolean.class, session, event.get());
			
		} catch (Exception e) {
			e.printStackTrace();
			if (_log.isErrorEnabled()) {
				_log.error(e.getStackTrace());
			}
			
		}
		
		if (event.map(Event::getName)
				.filter(LLAVE_DATOS_TRAMITE_CLIENTE_EVENT::equals)
				.isPresent()) {
			event.map(Event::getValue)
			.filter(val -> (val instanceof String))
			.map(json -> gson.fromJson((String) json, TramiteDto.class))
			.ifPresent(obj -> {
				if(_log.isErrorEnabled()) {
					_log.info("DatosClienteTramiteSenderPortlet -> processEvent -> datosTramiteClienteEvent,  solicitante  = " + obj);
				}
				session.setAttribute(LLAVE_TRAMITE, obj, PortletSession.APPLICATION_SCOPE);
			});
		}
	}
	
	private  <T> void uploadEventParamToSession(String nombreEvento, String llaveParametroSesion, Class<T> tipo, PortletSession session, Event event){
		//Gson gson = new Gson();
		Optional<Event> eventO = Optional.ofNullable(event);
		if (eventO.map(Event::getName)
				 .filter(nombreEvento::equals)
				 .isPresent()) {
			eventO.map(Event::getValue)
				 .filter(val -> val.getClass().equals(tipo))
				 .map(json -> (T)json)
				 .ifPresent(json -> {
					System.out.println("DatosClienteTramiteSenderPortlet -> processEvent -> " + llaveParametroSesion + " =  " + json); 
					session.setAttribute(llaveParametroSesion, json, PortletSession.APPLICATION_SCOPE);
				 });
		}
	}
	
	private  <T> void uploadEventStringParamToSession(String nombreEvento, String llaveParametroSesion, Class<T> tipo, PortletSession session, Event event){
		Gson gson = new Gson();
		Optional<Event> eventO = Optional.ofNullable(event);
		
		if (eventO.map(Event::getName)
				 .filter(nombreEvento::equals)
				 .isPresent()) {
			eventO.map(Event::getValue)
				 .filter(val -> val instanceof String)
				 .map(json -> (String)json)
				 .map(json -> gson.fromJson(json, tipo))
				 .ifPresent(json -> {
					System.out.println("DatosClienteTramiteSenderPortlet -> processEvent -> " + llaveParametroSesion + " =  " + json); 
					session.setAttribute(llaveParametroSesion, json, PortletSession.APPLICATION_SCOPE);
				 });
		}
	}
	
	private <T> T getParamFromSession(String llaveParametro, Class<T> tipo, Supplier<T> constructor, PortletSession session){
		return Optional.ofNullable(session)
				  .map(s -> s.getAttribute(llaveParametro, PortletSession.APPLICATION_SCOPE))
				  .filter(obj -> obj.getClass().equals(tipo))
				  .map(obj -> (T) obj)
				  .orElse(constructor != null ? constructor.get(): null);
	}
	
	private String[] redirectPair(){
		return _configuration.redirectPair();
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				TramiteClientePortletConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info("Configured Cliente Tramite (datosclientetramitesender - v1.0.1) : \n{ " +
					"redirect pair : " + _configuration.redirectPair() +
					" }");
		}
	}
	
	private TramiteClientePortletConfiguration _configuration ;
	private static Log _log = LogFactoryUtil.getLog(DatosClienteTramiteSenderPortlet.class);

}