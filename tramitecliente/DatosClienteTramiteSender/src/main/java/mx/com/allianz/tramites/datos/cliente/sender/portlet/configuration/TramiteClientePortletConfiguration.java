package mx.com.allianz.tramites.datos.cliente.sender.portlet.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
		category = "Productivity",
		scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
	)
	@Meta.OCD(
		localization = "content/Language",
		name = "Configuracion Tramite Cliente",
		id = "mx.com.allianz.tramites.datos.cliente.sender.portlet.configuration.TramiteClientePortletConfiguration"
	)
public interface TramiteClientePortletConfiguration {
	
	@Meta.AD(
			deflt = "cliente-tramite:/web/guest/solicitante-tramite,clientep-tramite:/group/guest/solicitantep-tramite", 
			description = "Mapeo de posible ruta con redireccionamiento",
			required = false
		)
	public String[] redirectPair();
	
}
