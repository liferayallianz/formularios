package mx.com.allianz.tramite.clientes.lugarrespuesta.portlet;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletSession;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import mx.com.allianz.commons.catalogos.dto.EstadoDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.service.estado.service.EstadoLocalServiceUtil;

@Component(
		immediate = true,
		property = {
			"com.liferay.portlet.display-category=category.sample",
			"com.liferay.portlet.instanceable=true",
			// Se agregan parametros para recibir de eventos de portlet
	        "com.liferay.portlet.requires-namespaced-parameters=false",
			// fin Se agregan parametros para recibir de eventos de portlet
			"javax.portlet.display-name=Tramite Lugar Respuesta Solicitud",
			"javax.portlet.init-param.template-path=/",
			"javax.portlet.init-param.edit-template=/edit.jsp",
			"javax.portlet.init-param.view-template=/view.jsp",
			"javax.portlet.portlet-mode=text/html;edit,edit-guest",
			"javax.portlet.resource-bundle=content.Language",
			// Se agrega nombre de mensaje y cola del portlet para recibir
			"javax.portlet.supported-processing-event=solicitanteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent",
			// Fin Se agrega nombre de mensaje y cola del portlet para recibir
			"javax.portlet.security-role-ref=power-user,user,guest"
		},
		service = Portlet.class
	)
public class LugarRespuestaPortlet extends MVCPortlet {
	private static String NACIONAL = "nacional",INTERNACIONAL = "internacional";
	private static String valor = "valor", desc = "desc";
	public static final int CODIGO_PAIS_MEXICO=412;
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {  
		if(_log.isDebugEnabled()) {
			_log.debug("LugarRespuestaPortlet -> processEvent");
		}

		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		Optional<Event> event = Optional.ofNullable(request)
										.map(EventRequest::getEvent);
		if (event.map(Event::getName)
				 .filter("solicitanteTramiteClienteEvent"::equals)
				 .isPresent()) {
			event.map(Event::getValue)
				 .filter(val -> (val instanceof String))
				 .map(json -> gson.fromJson((String) json, TramiteDto.class))
				 .ifPresent(tramiteStr -> {
					 if(_log.isDebugEnabled()) {
							_log.debug("Objeto tramiteStr = " + tramiteStr);
					 }
					 System.out.println("Objeto parseado: " + tramiteStr);
					 session.setAttribute("tipoTramiteGMM", tramiteStr.getTipoTramiteGMM(), PortletSession.APPLICATION_SCOPE);
					 session.setAttribute("tipoTramite", tramiteStr.getTipoTramite(), PortletSession.APPLICATION_SCOPE);
					 session.setAttribute("isClientEnabled", Boolean.valueOf(tramiteStr.getClienteFirmado() != null)
					 , PortletSession.APPLICATION_SCOPE);
				 });
		}
		super.processEvent(request, response);
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		SessionMessages.add(renderRequest,
				PortalUtil.getPortletId(renderRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		try {

			renderRequest.setAttribute("tipoTramiteGMM", renderRequest.getPortletSession().getAttribute("tipoTramiteGMM", PortletSession.APPLICATION_SCOPE));
			renderRequest.setAttribute("tipoTramite", renderRequest.getPortletSession().getAttribute("tipoTramite", PortletSession.APPLICATION_SCOPE));
			renderRequest.setAttribute("isClientEnabled", renderRequest.getPortletSession().getAttribute("isClientEnabled", PortletSession.APPLICATION_SCOPE));
			List<EstadoDTO> estados = EstadoLocalServiceUtil.getEstados(-1,-1).stream()
										.filter(estado -> 
											estado.getCodigoPais() == CODIGO_PAIS_MEXICO
										)
										.map(estado ->
											new EstadoDTO(estado.getCodigoPais(), estado.getCodigoEstado(),
														  estado.getDescripcion())
										)
										.sorted(Comparator.comparing(EstadoDTO::getDescripcion))
										.collect(Collectors.toList());
			

			PortletPreferences preferences = renderRequest.getPreferences();
			String lugaresAtencion= preferences.getValue("lugaresAtencion", "").toLowerCase();
			String tiposTramites = preferences.getValue("tiposTramites", "").toUpperCase();
			_log.debug("******** lugaresAtencion :: " + lugaresAtencion);
			_log.debug("******** tiposTramites :: " + tiposTramites);
			JSONArray arrayLugares = new JSONFactoryUtil().createJSONArray();
			JSONArray arrayTramites = new JSONFactoryUtil().createJSONArray();
			if(!lugaresAtencion.isEmpty()){
				JSONArray data = getListaLugares(lugaresAtencion);
				for(int i = 0; i < data.length(); i++){
					JSONObject jsonLugaresAtencion = new JSONFactoryUtil().createJSONObject();
					if(data.get(i).toString().equalsIgnoreCase(NACIONAL)){
						jsonLugaresAtencion.put(valor, "N");
						jsonLugaresAtencion.put(desc, capitalize(data.get(i).toString()));
						arrayLugares.put(jsonLugaresAtencion);
					}else{
						jsonLugaresAtencion.put(valor, "I");
						jsonLugaresAtencion.put(desc, capitalize(data.get(i).toString()));
						arrayLugares.put(jsonLugaresAtencion);
					}
				}
			}			
			if(!tiposTramites.isEmpty()){
				JSONArray dataTramites = getListaTramites(tiposTramites);
				
				for(int i = 0; i < dataTramites.length(); i++){
					JSONObject jsonTramites = new JSONFactoryUtil().createJSONObject();
					String codTramites[] = {"001","002","003","004","005","999"};
					String tiposTramite[] = {"cirugía","medicamentos","estudios","rehabilitación","enfermería y Home Care","otros"};
					if(dataTramites.get(i).toString().equals(codTramites[i])){
						jsonTramites.put(valor, dataTramites.get(i).toString());
						jsonTramites.put(desc, capitalize(tiposTramite[i].toString()));
						arrayTramites.put(jsonTramites);
					}
				}
			}
			
			_log.debug("****** arrayLugares :: " + arrayLugares);
			_log.debug("****** arrayTramites :: " + arrayTramites);
			
			if(arrayLugares.length() == 0 || arrayTramites.length() == 0){
				SessionErrors.add(renderRequest, "errorLeerPreferencias");
			}
			
			renderRequest.setAttribute("lugaresAtencion", arrayLugares);
			renderRequest.setAttribute("tiposTramites", arrayTramites);
			renderRequest.setAttribute("estados", estados);

		} catch (Exception e ) { 
			e.printStackTrace();
			if(_log.isErrorEnabled()){
				_log.error(e.getStackTrace());
			}
		}

		super.render(renderRequest, renderResponse);
	}
	
	public void getPreferencias(ActionRequest request, ActionResponse response)throws ValidatorException, ReadOnlyException {
		String lugaresAtencion = ParamUtil.getString(request, "lugaresAtencion");
		String tiposTramites = ParamUtil.getString(request, "tiposTramites");
		PortletPreferences preferences = request.getPreferences();
		try {
			preferences.setValue("lugaresAtencion", lugaresAtencion);
			preferences.setValue("tiposTramites", tiposTramites);

			preferences.store();
		} catch (IOException e) {
			SessionErrors.add(request, "errorPreferencias");
			e.printStackTrace();
		}
	}
	
	private String[] getLugaresAtencion(String param){
		String[] lugares = param.split("\\,");
		return lugares;
	}
	
	private String[] getTipoTramites(String param){
		String[] tramites = param.split("\\,");
		return tramites;
	}
	
	private JSONArray getListaLugares(String param){
		String[] list = getLugaresAtencion(param);
		JSONArray resp = new JSONFactoryUtil().createJSONArray();
		for(int i = 0; i < list.length; i++){
			resp.put(list[i].toString());
			
		}
		return resp;
	}
	
	private JSONArray getListaTramites(String param){
		String[] list = getTipoTramites(param);
		JSONArray resp = new JSONFactoryUtil().createJSONArray();
		for(int i = 0; i < list.length; i++){
			resp.put(list[i].toString());
		}
		return resp;
	}
	
	public static String capitalize(String s) {
		char firstChar = s.charAt(0);
		if (Character.isLowerCase(firstChar)) {
			s = Character.toUpperCase(firstChar) + s.substring(1);
		}
		return s;
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet lugarrespuesta - 1.0.2, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(LugarRespuestaPortlet.class);

}