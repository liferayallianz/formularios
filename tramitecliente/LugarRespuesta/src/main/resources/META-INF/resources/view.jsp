<%@ include file="/init.jsp" %>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mx.com.allianz.commons.catalogos.dto.EstadoDTO"%>
	<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
	<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
	<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
	<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
	<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
	<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
	<liferay-ui:error key="errorLeerPreferencias"
	message="Favor de dar de alta las preferencias para Lugar de Atenci�n y Tipo Tr�mite" />
	
	<portlet:defineObjects />
<% 
	List<EstadoDTO> estados = new ArrayList<>();
    Object object = request.getAttribute("estados");
	if (object != null && object instanceof List) {
		estados = (List<EstadoDTO>) object;
	}
	boolean tramiteHabilitado = request.getAttribute("isClientEnabled") != null && ((Boolean)request.getAttribute("isClientEnabled"));

		JSONArray lugarAtencion = (JSONArray)request.getAttribute("lugaresAtencion");
		JSONArray tipoTramite = (JSONArray)request.getAttribute("tiposTramites");
		

%>

<portlet:resourceURL var="fetchHospitalsResourceURL" />
<div id="contenedor_lugar_respuesta" style="display:<%="Vida".equals(request.getAttribute("tipoTramite"))?"none":"block"%>;">
<p class="formularioTituloEncabezado">Lugar donde se dar&aacute; respuesta a su solicitud: </p>
<aui:form action="" method="post" name="fm_lugar_respuesta">
	<aui:input name="version-lugarrespuesta" type="hidden" value="1.0.1"  />
	<aui:select cssClass="formularioCampoTexto" id="lugar_respuesta_estado" label="" name="estado" required="<%=("GMM".equals(request.getAttribute("tipoTramite"))&&"Reembolso".equals(request.getAttribute("tipoTramiteGMM")))%>" errorMessage="El estado donde se enviar&aacute; el pago es requerido">
		<aui:option selected="true" value="">
	        <liferay-ui:message key="lugar.respuesta.estado.obligatorio" />
	    </aui:option>
	    <% 
	    		for(EstadoDTO estado: estados){
	   	%>
     	<aui:option value="<%=estado.getCodigoEstado()%>"><%=estado.getDescripcion()%></aui:option>
	   	<% 	   
		   };
		%>
	</aui:select>
		<div id="<portlet:namespace />div_lugar_atencion_combo" style="display:none">
			<aui:select cssClass="formularioCampoTexto" label="" name="lugarAtencion"  id="lugarAtencion" onChange="">
					<aui:option value=""><liferay-ui:message key="tipo.reclamacion.atecion.lugar" /></aui:option>
					
					<% 
						
					
	    					for(int i = 0; i < lugarAtencion.length(); i++){
	    						JSONObject datos = lugarAtencion.getJSONObject(i);
	   				%>
     							<aui:option value="<%=datos.getString("valor")%>"><%=datos.getString("desc")%></aui:option>
	   				<% 	   
		   					};
		   				
					%>
					
			</aui:select>
			
		</div>
		<div id="<portlet:namespace />div_tipo_tramite_combo"  style="display:none">
			<aui:select cssClass="formularioCampoTexto" label="" id="tipoTramite" name="tipoTrmite" onChange="">
			
					<aui:option value=""><liferay-ui:message key="tipo.reclamacion.tipo.tramite" /></aui:option>
					<% 
					
					
	    				for(int i = 0; i < tipoTramite.length(); i++){
	    					JSONObject datos = tipoTramite.getJSONObject(i);
	   				%>
     						<aui:option value="<%=datos.getString("valor")%>"><%=datos.getString("desc")%></aui:option>
	   				<% 	   
		   				};
		   					
		   			
					%>
			</aui:select>
			
		</div>
	<%-- <div style="display:<%=tramiteHabilitado ? "none;" :"block;"%>"> --%>
	 <div style="display:none"> 
		<aui:row>
		    <aui:col span="4">
				<p class="formularioEtiquetaTexto">Grupo de Pago</p>    
		    </aui:col>
		    <aui:col span="8">
				<p class="formularioEtiquetaTexto"><div id="grupo_pago_desc" class="formularioEtiquetaTexto" name="grupo_pago">CAJA MATRIZ</div></p>                          
		    </aui:col>
		</aui:row>	
		<aui:row>
		    <aui:col span="4">
				<p class="formularioEtiquetaTexto">Sub Grupo de Pago</p>
		    </aui:col>
		    <aui:col span="8">
				<p class="formularioEtiquetaTexto"><div id="subgrupo_pago_desc" class="formularioEtiquetaTexto" name="sub_grupo_pago">AGENTES</div></p> 
		    </aui:col>
		</aui:row>
	</div>
</aui:form>
</div>

<script type="text/javascript"> 


if(sessionStorage.getItem('opcion-cirugia-programada')){
	if(sessionStorage.getItem('opcion-cirugia-programada') === "true"){
		console.log("sessionCirugia es true :: mostrar combos lugar atencion y tramites")
		$("#<portlet:namespace />div_lugar_atencion_combo").css("display", "block");
		$("#<portlet:namespace />div_tipo_tramite_combo").css("display", "block");
	}
}

if(sessionStorage.getItem('opcion-cirugia-reembolso')){
	if(sessionStorage.getItem('opcion-cirugia-reembolso') === "true"){
		console.log("sessionCirugia es true :: mostrar combos lugar atencion y tramites")
		$("#<portlet:namespace />div_lugar_atencion_combo").css("display", "none");
		$("#<portlet:namespace />div_tipo_tramite_combo").css("display", "none");
	}
}









Liferay.provide(window,'cargaDatosLugarRespuesta', function(datosLugarRespuesta) {
	if(datosLugarRespuesta){
		$('#<portlet:namespace />lugar_respuesta_estado').val(datosLugarRespuesta.estado);
	} 
});
</script>


<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">

console.log("lugarrespuesta v1.0.3");

var formValidator = Liferay.Form.get('<portlet:namespace />fm_lugar_respuesta').formValidator;
Liferay.on('validaDatosLugarRespuesta', function(event) {
	console.log("******* OBTENCION DE DATOS LUGAR RESPUESTA********");
	var eventoRespuesta = {};
	formValidator.validate();
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaDatosLugarRespuesta eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if(eventoRespuesta.valido) {
		eventoRespuesta.cpTramite = A.one('#<portlet:namespace />tipoTramite').get('value');
		eventoRespuesta.cplugarAtencion = A.one('#<portlet:namespace />lugarAtencion').get('value');
		console.log(eventoRespuesta.tramite);
		console.log(eventoRespuesta.lugar);
		eventoRespuesta.estado = A.one('#<portlet:namespace />lugar_respuesta_estado').get('value');
		eventoRespuesta.subgrupo = <%=!tramiteHabilitado ? "'0001'" :"'0001'"%>;
		eventoRespuesta.subgrupoDescripcion = <%=(!tramiteHabilitado) ? "'AGENTES'" :"'AGENTES'"%>;
		eventoRespuesta.grupoDescripcion = <%=!tramiteHabilitado ? "'CAJA MOTRIZ'" :"''CAJA MOTRIZ''"%>;
		eventoRespuesta.grupo = <%=!tramiteHabilitado ? "'80'" :"'80'"%>;
	}
	Liferay.fire('validaDatosLugarRespuestaRespuesta', eventoRespuesta );
});



</aui:script>
	
	
	
	
	
	
	
	
	