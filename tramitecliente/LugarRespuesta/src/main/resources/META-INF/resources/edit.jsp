<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>

<portlet:defineObjects />
<%
	PortletPreferences pref = renderRequest.getPreferences();
	String lugarAtencion = pref.getValue("lugaresAtencion", "");
	String tipoTramite = pref.getValue("tiposTramites", "");
%>

<portlet:actionURL name="getPreferencias" var="getPreferenciasURL"></portlet:actionURL>

<aui:form method="POST" action="<%=getPreferenciasURL%>">
	
	<aui:input name="lugaresAtencion" title="Lugares de atenci�n" placeholder="Nacional,Internacional" type="text" value="<%=lugarAtencion%>"></aui:input>
	<aui:input name="tiposTramites" title="Tipos tramite" placeholder="001,002,003,004,005,999" type="text" value="<%=tipoTramite%>"></aui:input>
	<aui:button type="submit" value="Guardar"></aui:button>
</aui:form>