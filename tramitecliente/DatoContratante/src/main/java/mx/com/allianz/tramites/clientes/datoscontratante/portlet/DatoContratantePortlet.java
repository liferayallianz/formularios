package mx.com.allianz.tramites.clientes.datoscontratante.portlet;


import java.util.Map;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(
	immediate = true,
	property = {
			"com.liferay.portlet.display-category=category.sample",
			"com.liferay.portlet.instanceable=true",
			// Se agregan parametros para recibir de eventos de portlet
	        "com.liferay.portlet.requires-namespaced-parameters=false",
	        "com.liferay.portlet.ajaxable=true",
	        // fin Se agregan parametros para recibir de eventos de portlet
			// Se agregan parametros para compartir atributos de request en la session
	        "com.liferay.portlet.private-request-attributes=false",
	        "com.liferay.portlet.private-session-attributes=false",
	        "com.liferay.portlet.remoteable=true",
			"com.liferay.portlet.preferences-unique-per-layout=true",
			// Fin parametros para compartir atributos de request en la session
			"requires-namespaced-parameters=false",
			"javax.portlet.display-name=Commons Datos Contratante",
			"javax.portlet.init-param.template-path=/",
			"javax.portlet.init-param.view-template=/view.jsp",
			"javax.portlet.resource-bundle=content.Language",
			// Se agregan parametro para identificar el portlet en otros contextos
	        "javax.portlet.security-role-ref=power-user,user,guest",
	        "javax.portlet.supports.mime-type=text/html"

		
	},
	service = Portlet.class
)
public class DatoContratantePortlet extends MVCPortlet {
	
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet datocontratante - 1.0.2, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(DatoContratantePortlet.class);
	
}