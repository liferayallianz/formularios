<%@ include file="/init.jsp" %>


<div id="<portlet:namespace />divContratantePolza" style="display:block" >
</br>

<p class="formularioTituloEncabezado">Datos del contratante</p>
<aui:form action="#" name="fm_datos_contratante">
		<aui:input name="version-datocontratante" type="hidden" value="1.0.2"  />
		<aui:input cssClass="formularioCampoTexto" id="datos_contratante_nombre" name="nombre" type="text" value="" label="" placeholder="datos.contratante.name"  >
			<aui:validator name="required" errorMessage="El nombre del contratante es requerido"/>
			<aui:validator name="maxLength" errorMessage="La longitud del nombre del contratante debe ser menor a 75 caracteres">75</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" id="datos_contratante_apellido_paterno" name="apellidoPaterno" type="text" value="" label="" placeholder="datos.contratante.lastname" >
			<aui:validator name="required" errorMessage="El apellido paterno del contratante es requerido" />
			<aui:validator name="maxLength" errorMessage="La longitud del apellido paterno del contratante debe ser menor a 50 caracteres">50</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" id="datos_contratante_apellido_materno" name="apellidoMaterno" type="text" value="" label="" placeholder="datos.contratante.lastname2" >
			<aui:validator name="maxLength" errorMessage="La longitud del apellido materno del contratante debe ser menor a 50 caracteres">50</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
		<aui:input type="hidden" id="hidden_contratante_poliza_cliente" name="hiddenContratanteNumeroPolizaSelected" value=""/>
		
</aui:form>
</div>
<aui:script use="aui-base,liferay-form,aui-form-validator">
console.log("contratante v1.0.2");
var tramiteClienteHabilitadoJS = 'false';
console.log("tramiteClienteHabilitadoJS = " + tramiteClienteHabilitadoJS);

// add valid form
Liferay.on('validaDatosContratante', function(event){
	var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_contratante').formValidator;
	formValidator.validate();
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	if(eventoRespuesta.valido){
		eventoRespuesta.nombreContratante = A.one('#<portlet:namespace />datos_contratante_nombre').get('value');
		eventoRespuesta.apellidoPaternoContratante = A.one('#<portlet:namespace />datos_contratante_apellido_paterno').get('value');
		eventoRespuesta.apellidoMaternoContratante = A.one('#<portlet:namespace />datos_contratante_apellido_materno').get('value');
	}
	eventoRespuesta.nombreSeccion = "DatosContratante";
	console.log("eventoRespuesta de datosContratante");
	console.log(eventoRespuesta);
	Liferay.fire('validaDatosContratanteRespuesta', eventoRespuesta );
});

Liferay.on("mostrarDatosContratantePoliza",function(event){
	A.one('#<portlet:namespace/>divContratantePolza')._node.style.display = 'block';
});

Liferay.on("ocultarDatosContratantePoliza",function(event){
	A.one('#<portlet:namespace/>divContratantePolza')._node.style.display = 'none';
});
</aui:script>

<script type="text/javascript"> 
Liferay.on('presentarContratantePoliza', function(event) {
	console.log("presentarContratantePoliza ");
	console.log( event);

	document.getElementById('<portlet:namespace />divContratantePolza').style.display = 'block';
	var contratante = event.contratante; 
	console.log( contratante);

	if (contratante) 
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />datos_contratante_nombre').set('value',contratante.nombre);
			A.one('#<portlet:namespace />datos_contratante_apellido_paterno').set('value',contratante.apellidoPaterno);
			A.one('#<portlet:namespace />datos_contratante_apellido_materno').set('value',contratante.apellidoMaterno);
			A.one('#<portlet:namespace />hidden_contratante_poliza_cliente').set('value',contratante.idContratante);
	   		
		});
                
});
</script>