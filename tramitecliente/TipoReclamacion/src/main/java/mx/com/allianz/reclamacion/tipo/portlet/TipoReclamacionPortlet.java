package mx.com.allianz.reclamacion.tipo.portlet;

import java.util.Map;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para recibir de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
		// fin Se agregan parametros para recibir de eventos de portlet
		"javax.portlet.display-name=Tramite Tipo Reclamacion",
		"javax.portlet.init-param.template-path=/",		
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class TipoReclamacionPortlet extends MVCPortlet {
	
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet tiporeclamacion - 1.0.2, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(TipoReclamacionPortlet.class);

}