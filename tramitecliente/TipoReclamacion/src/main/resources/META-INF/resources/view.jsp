<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@ include file="/init.jsp" %>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<portlet:defineObjects />

<div id="<portlet:namespace />div_tipo_reclamacion" style="display:block">
	<aui:form action="#" name="fm_tipo_reclamacion">
		<aui:input name="version-tiporeclamacion" type="hidden" value="1.0.3"  />
	
		
		<div id="<portlet:namespace />div_tipo_reclamacion_combo" style="display:block">
			<aui:select cssClass="formularioCampoTexto" label="" name="tipoReclamacion" onChange="ocultaDatosSiniestro();">
					<aui:option value=""><liferay-ui:message key="tipo.reclamacion.tipo.reclamacion" /></aui:option>
					<aui:option value="0"><liferay-ui:message key="Inicial" /></aui:option>
					<aui:option value="1"><liferay-ui:message key="Complemento" /></aui:option>
			</aui:select>
			<div id="<portlet:namespace />datosSiniestro" style="display:none">
				<aui:row>
				    <aui:col span="12">
				    		<p class="formularioEtiquetaTexto"><liferay-ui:message key="N&uacute;mero de Siniestro *" /></p>
				    </aui:col>
				</aui:row>
				<aui:row>
				    <aui:col span="6">
				        <aui:input cssClass="formularioCampoTexto" name="numeroSiniestro" type="number" maxlength="9" value="" label="" >
				        	<aui:validator name="max" errorMessage="El n&uacute;mero de siniestro debe ser menor a 999,999,999">999999999</aui:validator>
				        	<aui:validator name="digits" errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros"/>
				        	<aui:validator name="required" errorMessage="El campo n&uacute;mero de siniestro es n&uacute;merico y requerido">
				                function() {
			                        return A.one('#<portlet:namespace />tipoReclamacion').get('selectedIndex') == "2";
				                }
			        		</aui:validator>
				        </aui:input>
				    </aui:col>
				    <aui:col span="1" style="text-align:center;">
				    		<p class="formularioEtiquetaTexto"><liferay-ui:message key="/" /></p>
				    </aui:col>
				    <aui:col span="5">
				        <aui:input cssClass="formularioCampoTexto" name="tipoSiniestro" type="number" maxlength="4" value="" label="" >
				        	<aui:validator name="max" errorMessage="El n&uacute;mero de siniestro debe ser menor a 9999">9999</aui:validator>
				        	<aui:validator name="digits" errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros"/>
				        	<aui:validator name="required" errorMessage="El campo tipo de siniestro es n&uacute;merico y requerido">
				                function() {
			                        return A.one('#<portlet:namespace />tipoReclamacion').get('selectedIndex') == "2";
				                }
			        		</aui:validator>
				        </aui:input>
				    </aui:col>
				</aui:row>
			</div>
		</div>
		<aui:input cssClass="formularioCampoTexto" type="textarea" name="observaciones" label="" placeholder="tipo.reclamacion.observaciones" />
	</aui:form>
</div>

<script type="text/javascript"> 
console.log("tiporeclamacion v1.0.3");

Liferay.on(window,'cargaTipoReclamacion', function(datosTipoReclamacion) {
	if(datosTipoReclamacion){
		$('#<portlet:namespace />tipoSiniestro').val(datosTipoReclamacion.tipoSiniestro);
		$('#<portlet:namespace />numeroSiniestro').val(datosTipoReclamacion.numeroSiniestro);
		$('#<portlet:namespace />tipoReclamacion').val(datosTipoReclamacion.tipoReclamacion);
		$('#<portlet:namespace />observaciones').val(datosTipoReclamacion.observaciones);
	}
});

</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">


Liferay.provide(window, 'mostrarSeccionTipoReclamacion', function(seccionPoliza,mostrado) {
	A.one('#<portlet:namespace/>' + seccionPoliza)._node.style.display = mostrado;
});

Liferay.on('validaDatosReclamacion', function(event) {
	var formValidator = Liferay.Form.get('<portlet:namespace />fm_tipo_reclamacion').formValidator;
	var eventoRespuesta = {};
	formValidator.validate();
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaDatosReclamacionRespuesta eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if(eventoRespuesta.valido){
		var tipoReclamacionSelect = A.one('#<portlet:namespace />tipoReclamacion');

		eventoRespuesta.tipoSiniestro = A.one('#<portlet:namespace />tipoSiniestro').get('value');
		eventoRespuesta.numeroSiniestro = A.one('#<portlet:namespace />numeroSiniestro').get('value');
		eventoRespuesta.tipoReclamacion = tipoReclamacionSelect.get('value');
		eventoRespuesta.tipoReclamacionDescripcion = tipoReclamacionSelect.get('options')._nodes[tipoReclamacionSelect.get('selectedIndex')].text;
		eventoRespuesta.observaciones = A.one('#<portlet:namespace />observaciones').get('value');;
	}
	eventoRespuesta.nombreSeccion = "DatosReclamacion";
	Liferay.fire('validaDatosReclamacionRespuesta', eventoRespuesta );
});

Liferay.provide(window,'ocultaDatosSiniestro', function(event) {
	mostrarSeccionTipoReclamacion("datosSiniestro",A.one('#<portlet:namespace />tipoReclamacion').get('selectedIndex') == "2"? 'block':'none')
});

Liferay.on('mostrarTipoReclamacion', function(event) {
	mostrarSeccionTipoReclamacion('div_tipo_reclamacion','block');
	console.log(event);
	console.log(event.tipoTramite == "Vida" ? 'block' : 'none' );
	mostrarSeccionTipoReclamacion('div_tipo_reclamacion_combo',  event.tipoTramite == "Vida" ? 'block' : 'none' );
});

Liferay.on('ocultarTipoReclamacion', function(event) {
	mostrarSeccionTipoReclamacion('div_tipo_reclamacion','none');
});
</aui:script>