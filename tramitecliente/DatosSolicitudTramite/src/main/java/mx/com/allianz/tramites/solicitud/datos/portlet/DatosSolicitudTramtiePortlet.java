package mx.com.allianz.tramites.solicitud.datos.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.osgi.service.component.annotations.Component;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.catalogos.dto.CiudadDTO;
import mx.com.allianz.commons.catalogos.dto.EstadoDTO;
import mx.com.allianz.commons.catalogos.dto.SucursalDTO;
import mx.com.allianz.service.ciudad.model.Ciudad;
import mx.com.allianz.service.ciudad.service.CiudadLocalServiceUtil;
import mx.com.allianz.service.estado.service.EstadoLocalServiceUtil;
import mx.com.allianz.service.sucursal.service.SucursalLocalServiceUtil;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para recibir de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
		// fin Se agregan parametros para recibir de eventos de portlet
		"javax.portlet.display-name=Tramite Solicitud Reembolso",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-processing-event=datosSolicitudTramiteEvent;http://mx-allianz-liferay-namespace.com/events/datosSolicitudTramite",
		// Fin Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DatosSolicitudTramtiePortlet extends MVCPortlet {
	
	public static final int CODIGO_PAIS_MEXICO=412;
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
        System.out.println("datosSolicitudTramiteEvent Event search!");
	    Event event = request.getEvent();
	    if(event.getName().equals("datosSolicitudTramiteEvent")){
	    	System.out.println("datosSolicitudTramiteEvent Event found!");
	        String eventValue = (String) event.getValue();
	        System.out.println("show me that value from the datosSolicitudTramite-Event: " + eventValue);
	     }
	     super.processEvent(request, response);
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try {
		System.out.println("Dentro del render de solicitud tramite");
		
		List<SucursalDTO> sucursales = Optional.ofNullable(SucursalLocalServiceUtil.getSucursals(-1, -1))
				.orElse(new ArrayList<>()).stream().map(sucursal -> 
					new SucursalDTO(sucursal.getIdSucursal(), sucursal.getDescripcion())).
				collect(Collectors.toList());
		
		List<EstadoDTO> estados = Optional.ofNullable(EstadoLocalServiceUtil.getEstados(-1,-1))
				.orElse(new ArrayList<>()).stream().filter
			(estado -> estado.getCodigoPais() == CODIGO_PAIS_MEXICO).map(estado ->
			new EstadoDTO(estado.getCodigoPais(), estado.getCodigoEstado(),
			estado.getDescripcion())).sorted(Comparator.comparing(EstadoDTO::getDescripcion))
			.collect(Collectors.toList());
		
		System.out.println("municipio attr = " + Optional.ofNullable(renderRequest.
				getAttribute("municipio")).orElse("Nope"));
		System.out.println("municipio param = " + Optional.ofNullable(ParamUtil.getString(renderRequest,
				"municipio")).orElse("Nope"));
		
		System.out.println("Los estados:"+estados);
		renderRequest.setAttribute("estados", estados);
		renderRequest.setAttribute("sucursales", sucursales);
		} catch (Exception e ) { e.printStackTrace(); }

		super.render(renderRequest, renderResponse);
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws IOException,
            PortletException {
		System.out.println("DatosSolicitudTramtiePortlet -> serveResource");
		String valorParametro = "9";
		Gson gson = new Gson();

		try {
			String parametro = ParamUtil.getString(resourceRequest, "estado");
			if(parametro != null){
				valorParametro = parametro;
			}
			System.out.println("parametro = " + parametro);
			Integer codigoEstado = Integer.parseInt(valorParametro);
			
	        JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
	        
	        Optional.ofNullable(CiudadLocalServiceUtil.getCiudads(-1,-1))
	        .orElseGet(ArrayList<Ciudad>::new).stream()
	        .filter(ciudad -> ciudad.getCodigoPais() == CODIGO_PAIS_MEXICO)
	        .filter(ciudad -> ciudad.getCodigoEstado() == codigoEstado)
	        .filter(ciudad -> !"SIN CIUDAD".equals(ciudad.getDescripcion()))
	        .map(ciudad -> new CiudadDTO(ciudad.getCodigoPais(), ciudad.getCodigoEstado()
	        		, ciudad.getCodigoCiudad(), ciudad.getDescripcion()))
	        .sorted((ciudad1, ciudad2) -> ciudad1.getDescripcion().compareTo(ciudad2.getDescripcion()))
	        .map(gson::toJson).map(DatosSolicitudTramtiePortlet::parse)
	        .filter(Optional::isPresent).map(Optional::get)
	        .forEach(jsonArray::put); 
	        resourceResponse.setContentType("text/javascript");
	        PrintWriter writer = resourceResponse.getWriter();
	        writer.write(jsonArray.toString());
		} catch (Exception e ) { e.printStackTrace(); }
    }
	
	private static Optional<JSONObject> parse(String gsonString) {
		JSONParser parser = new JSONParser();
		Optional<JSONObject> objectJson = null;
		try {
			objectJson = Optional.ofNullable((JSONObject)parser.parse(gsonString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return objectJson;
	}
}