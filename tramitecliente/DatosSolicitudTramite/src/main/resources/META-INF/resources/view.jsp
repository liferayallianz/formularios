<%@ include file="/init.jsp" %>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mx.com.allianz.commons.catalogos.dto.EstadoDTO"%>
<%@page import="mx.com.allianz.commons.catalogos.dto.CiudadDTO"%>

<%@page import="mx.com.allianz.commons.catalogos.dto.SucursalDTO"%>

<% 
	List<EstadoDTO> estados = new ArrayList<>();
    Object object = request.getAttribute("estados");
	if (object != null && object instanceof List) {
		estados = (List<EstadoDTO>) object;
	}
	
	List<SucursalDTO> sucursales = new ArrayList<>();
    Object objectS = request.getAttribute("sucursales");
	if (objectS != null && objectS instanceof List) {
		sucursales = (List<SucursalDTO>) objectS;
	}
%>

<portlet:resourceURL var="fetchCitiesResourceURL" />

<div id="div_datos_solicitud_tramite" style="display:none;">
<div id="titulo_datos_tramite"></div>
<aui:form action="" method="post" name="fm_datos_tramite">
	<p class="formularioEtiquetaTexto">Lugar donde usted est&aacute; realizando el tr&aacute;mite:</p>
	<aui:select cssClass="formularioCampoTexto" id="estado_tramite" label="" name="estado"  onChange='fetchCities();' required="true" errorMessage="El estado donde usted est� realizando el tr�mite es requerido" >
		<aui:option selected="true" value="">
		        <liferay-ui:message key="datos.solicitud.tramite.estado.obligatorio" />
	    </aui:option>
		<% 
	    		for(EstadoDTO estado: estados){
	   	%>
     	<aui:option value="<%=estado.getCodigoEstado()%>"><%=estado.getDescripcion()%></aui:option>
	   	<% 	   
		   };
		%>
	</aui:select>
	<aui:select cssClass="formularioCampoTexto" id="municipio_tramite" label="" name="municipio" required="true" onChange='saveCityPosition(this);' errorMessage="El municipio donde usted est� realizando el tr�mite es requerido">
		<aui:option value="">
			<liferay-ui:message key="datos.solicitud.tramite.municipio.obligatorio" />
	    </aui:option>
	</aui:select>
	<aui:input name="municipio_tramite_position" type="hidden" value="" />
	<div id="<portlet:namespace />div_lugar_respuesta_solicitud" style="display:block">
		<p class="formularioEtiquetaTexto">Lugar donde quiere que sea atendido el tr&aacute;mite:</p>
		<aui:select cssClass="formularioCampoTexto" id="sucursal_tramite" label="" name="sucursal" required="true" errorMessage="El lugar donde quiere que sea atendido el tr�mite es requerido" >
			<aui:option selected="true" value="">
				<liferay-ui:message key="datos.solicitud.tramite.sucursal.obligatorio" />
		    </aui:option>
			<% 
		    		for(SucursalDTO sucursal: sucursales){
		   	%>
	     	<aui:option value="<%=sucursal.getIdSucursal()%>"><%=sucursal.getDescripcion()%></aui:option>
	 	   	<% 	  
			   };
			%>
		</aui:select>
	</div>
</aui:form>
</div>

<script type="text/javascript">
var yaCargoEstados =
Liferay.on('validaReglasAlIniciarDatosTramite', function(event){
	console.log(event);
	document.getElementById('div_datos_solicitud_tramite').style.display = event.tipoTramite?'block':'none';
	if (event.tipoTramite == 'Vida') {
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />sucursal_tramite').set('selectedIndex',2);
			A.one('#<portlet:namespace />sucursal_tramite').set('disabled', true);
			mostrarSeccionDatosSolicitudTramite("div_lugar_respuesta_solicitud","none");
		});	
	}
	document.getElementById("titulo_datos_tramite").innerHTML = '';
	//No guarda valores
	//TODO regresar titulo de cirugia programada
	document.getElementById("titulo_datos_tramite").innerHTML = (event.tipoTramite == 'Vida') ? '<p class="formularioTituloEncabezado">Solicitud de Tr&aacute;mite de Vida</p>' : '<pclass="formularioTituloEncabezado">Solicitud de Reembolso de Gastos M&eacute;dicos Mayores</p>';
	//if ( event.tipoTramite == 'GMM' )
		new AUI().use(function(A) {
			
			var estadoAjax = A.one('#<portlet:namespace />estado_tramite');
			estadoAjax.set("value",event.estado);
			A.one('#<portlet:namespace />sucursal_tramite').set('value', event.sucursal);
			A.one('#<portlet:namespace />municipio_tramite').set("value",event.municipio);
			A.one('#<portlet:namespace />municipio_tramite_position').set("value",event.municipio);
			if (estadoAjax && estadoAjax.get('value')) {
				console.log("Estado value= " + estadoAjax.get('value'));
				var fetchTopicsURL = '<%= fetchCitiesResourceURL.toString() %>';
				$.post(fetchTopicsURL, {
					estado: estadoAjax.get('value')
			    }, function(data){
					var ciudadAjax = A.one('#<portlet:namespace />municipio_tramite');
					var selectedIndexCity = A.one('#<portlet:namespace />municipio_tramite_position').get('value');
		            var cities = data;
		            for (var j=0; j < cities.length; j++) {
		        	 		ciudadAjax.append("<option value='" + cities[j].codigoCiudad + "'>" + cities[j].descripcion + "</option>");
		         	}
		            var selectedIndexCities =  selectedIndexCity || 0;
		            ciudadAjax.set('selectedIndex',selectedIndexCities);
				}, 'json');
			}
		});
});

Liferay.on('validaReglasCirugiaProgramada', function(event){
	document.getElementById("titulo_datos_tramite").innerHTML = '';
	document.getElementById("titulo_datos_tramite").innerHTML = '<p class="formularioTituloEncabezado">Solicitud de Cirug&iacute;a Programada de Gastos M&eacute;dicos Mayores</p>';
	
});
</script>
<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
// add valid form

var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_tramite').formValidator;

Liferay.provide(window, 'mostrarSeccionDatosSolicitudTramite', function(seccionPoliza,mostrado) {
	A.one('#<portlet:namespace/>' +  seccionPoliza)._node.style.display = mostrado;
});

  

Liferay.on('hideDivDatosSolicitudTramite', function(event){
	document.getElementById('div_datos_solicitud_tramite').style.display = 'none';
});

Liferay.on('validaDatosSolicitudTramite', function(event){
	console.log('Datos Solicitud Tramite  Liferay on');
	var eventoRespuesta = {};
	if(formValidator) {
		formValidator.validate();
		console.log('Datos Solicitud Tramite  Liferay on');
		eventoRespuesta.valido = !formValidator.hasErrors();
		console.log('validaDatosSolicitudTramite eventoRespuesta.valido = ' + eventoRespuesta.valido);
		if (eventoRespuesta.valido) {
			var estado = A.one('#<portlet:namespace />estado_tramite').get('value');
			var municipio = A.one('#<portlet:namespace />municipio_tramite').get('value');
			var sucursal = A.one('#<portlet:namespace />sucursal_tramite').get('value');
			eventoRespuesta.estado = estado;
			eventoRespuesta.municipio = municipio;
			eventoRespuesta.sucursal = sucursal;
		}	
	} else {
		eventoRespuesta.valido = false;
	}
	Liferay.fire('validaDatosSolicitudTramiteRespuesta', eventoRespuesta );
});

Liferay.provide(window, 'fetchCities', function() {
    var fetchTopicsURL = '<%= fetchCitiesResourceURL.toString() %>';
	var estadoAjax = A.one('#<portlet:namespace />estado_tramite');
	var ciudadAjax = A.one('#<portlet:namespace />municipio_tramite');
	console.log('Estado = ' + estadoAjax.get('value'));
    A.io.request (
         fetchTopicsURL, {
         data: {
             estado: estadoAjax.get('value')
         },
         dataType: 'json',
         on: {
                 failure: function() {
                     console.log("Ajax failed! There was some error at the server");
                 },
                 success: function(event, id, obj) {
                     var cities = this.get('responseData');
                     console.log(cities);
                    	 ciudadAjax.html("");
                    	 ciudadAjax.append("<option value=''>Seleccione una Delegacion/Municipio *</option>");
                     for (var j=0; j < cities.length; j++) {
                    	 	ciudadAjax.append("<option value='" + cities[j].codigoCiudad + "'>" + cities[j].descripcion + "</option>");
                     }
                 }
             }
         }); 
});

Liferay.provide(window, 'saveCityPosition', function(select) {
	A.one('#<portlet:namespace />municipio_tramite_position').set('value', select.selectedIndex);
});

Liferay.on('tipoTramiteVidaSucursalNormalEvent', function(event){
	document.getElementById('div_datos_solicitud_tramite').style.display = 'block';
	mostrarSeccionDatosSolicitudTramite("div_lugar_respuesta_solicitud","block");
	A.one('#<portlet:namespace />sucursal_tramite').set('selectedIndex',0);
	A.one('#<portlet:namespace />sucursal_tramite').set('disabled', false);
	Liferay.fire('tipoTramiteShowButtonEvent', {} );
});

Liferay.on('tipoTramiteVidaSucursalMatrizEvent', function(event){
	document.getElementById('div_datos_solicitud_tramite').style.display = 'block';
	mostrarSeccionDatosSolicitudTramite("div_lugar_respuesta_solicitud","none");
	A.one('#<portlet:namespace />sucursal_tramite').set('selectedIndex',2);
	A.one('#<portlet:namespace />sucursal_tramite').set('disabled', true);
	Liferay.fire('tipoTramiteShowButtonEvent', {} );

});

Liferay.on('cambiaTituloDatosTramite', function(event){
	document.getElementById("titulo_datos_tramite").innerHTML = '';
	document.getElementById("titulo_datos_tramite").innerHTML = '<p class="formularioTituloEncabezado titulotop">' + event.titulo + '</p>';
});

console.log('antes de fire inicial validaReglasVidaAlIniciar, datosTRamite ');
Liferay.fire('validaReglasVidaAlIniciar', { });	

</aui:script>
