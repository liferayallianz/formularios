<%@ include file="/init.jsp" %>

<div id="div_tipo_tramite_gmm" class="radioSpaceMargin" style="display:none;">
<p  class="formularioTituloEncabezado">Seleccione el tipo de tr&aacute;mite a realizar</p>
<div id="fm_div_tipo_tramite_gmm" style="padding:0px !important;">
	<aui:form action="#" name="fm_tipo_tramite_gmm">
		<aui:input name="version-tipostramitegmm" type="hidden" value="1.0.1"  />
		<aui:field-wrapper name="" cssClass="formularioRadioCentrado">
			<aui:input checked="<%=false%>" inlineLabel="right" id="tipoTramiteGMMReembolso" name="tipoTramiteGMM" type="radio" value="Reembolso" label="tipo.tramite.gmm.reembolso" onClick="showIndicacionesTramiteGMMModal();"/> &nbsp;
			<aui:input checked="<%=false%>" inlineLabel="right" id="tipoTramiteGMMCirugia" name="tipoTramiteGMM" type="radio" value="CirProgramada" label="tipo.tramite.gmm.cirugia" onClick="showIndicacionesTramiteGMMModalCirugia();"/>
		</aui:field-wrapper>
			<br/>
	</aui:form>
</div> 
</div>

<div class="fade in lex-modal modal" id="<portlet:namespace/>indicaciones_tramite_gmm_modal"
role="dialog" tabindex="-1" style="display:none;">
	<div class="modal-dialog modal-full-screen" style="height:500px;" >
		<div class="modal-content">
			<div id="<portlet:namespace/>indicaciones_tramite_gmm_modal_div">
					<div class="modal-header">
						<button aria-labelledby="Close" class="btn btn-default close" data-dismiss="modal" role="button" type="button" onClick="closeIndicacionesTramiteGMMModal();">
							<svg aria-hidden="true" class="lexicon-icon lexicon-icon-times">
								<use xlink:href="<%= themeDisplay.getPathThemeImages() + "/lexicon/icons.svg" %>#times" />
							</svg>
						</button>
						<h4 class="modal-title" ><liferay-ui:message key="Tr&aacute;mite @Clientes - Documentos para GMM" /></h4>
					</div>
					<div class="modal-body" >
						<h4>DOCUMENTOS PARA GMM</h4>
							<p>&emsp;1.-Aviso de accidente y/o enfermedad requisitado por completo y firmado.</p>
							<p>&emsp;2.-Informe M&eacute;dico (Historia cl&iacute;nica completa) requisitados por completo y firmados.</p>
							<p>&emsp;3.-Interpretación de estudios.</p>
							<p>&emsp;4.-Presupuesto de honorarios en caso que el m&eacute;dico no pertenezca a la red de Allianz.</p>
							<p>&emsp;5.-Comprobante de domicilio (con vigencia no mayor a 3 meses). Identificaci&oacute;n oficial con firma del cliente y formato de indentificaci&oacute;n o solicitud de reembolso requisitado en su totalidad.</p>
						<h4>NOTAS:</h4>
						<p>Para tr&aacute;mites de Reembolso es indispensable anexar a lo anterior factura y/o recibos con desglose a nombre del titular de la p&oacute;liza.</p>
						<p>En caso de solicitar transferencia es necesario ingresar el Aviso de Accidente / Enfermedad en original.</p>	
						<p>Allianz puede solicitar los comprobantes originales si as&iacute; lo requiere para continuar con su reclamaci&oacute;n.</p>
					</div>

					<div class="modal-footer">
						<button class="btn btn-link close-modal" data-dismiss="modal" type="button" onClick="closeIndicacionesTramiteGMMModal();"><liferay-ui:message key="Cerrar" /></button>
					</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
Liferay.on('validaReglasVidaAlIniciarTTGMM', function(event){
	console.log("Tipo tramite GMM -> validaReglasVidaAlIniciar -> event.tipoTramite = " + event.tipoTramite);
	if (event.tipoTramite == 'GMM'){
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />tipoTramiteGMM'+(event.tipoTramiteGMM == "Reembolso" ? event.tipoTramiteGMM : "Cirugia")).set('checked', true);
			A.one('#<portlet:namespace />indicaciones_tramite_gmm_modal')._node.style.display = 'block';
			Liferay.fire('cambiaTituloDatosTramite', {titulo:A.one('#<portlet:namespace />tipoTramiteGMMReembolso').get('checked')?
				"Solicitud de Reembolso de Gastos M&eacute;dicos Mayores":"Solicitud de Cirug&iacute;a Programada de Gastos M&eacute;dicos Mayores"} );
		});
		document.getElementById('div_tipo_tramite_gmm').style.display = 'block';
	} else {
		document.getElementById('div_tipo_tramite_gmm').style.display = 'none';
	}
});
</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">

console.log("tipotramitegmm v1.0.1");

// add valid form
var formValidator = Liferay.Form.get('<portlet:namespace />fm_tipo_tramite_gmm').formValidator;
Liferay.on('validaTipoTramiteGMM', function(event) {
	console.log('Tipo Tramite GMM Liferay on');
	var eventoRespuesta = {};
	if(formValidator) {
		formValidator.validate();
		console.log('Tipo Tramite GMM -> formValidator.validate()');
		eventoRespuesta.valido = !formValidator.hasErrors();
		console.log('validaTipoTramiteGMM eventoRespuesta.valido = ' + eventoRespuesta.valido);
		if (eventoRespuesta.valido) {
			var tipoTramite = A.one('#<portlet:namespace />tipoTramiteGMMReembolso');
			var tipoTramiteChecked = tipoTramite.get('checked') ? 'Reembolso' : 'CirProgramada';
			eventoRespuesta.tipoTramiteGMM = tipoTramiteChecked;
		}
	} else {
		eventoRespuesta.valido = false;
	}
	Liferay.fire('validaTipoTramiteGMMRespuesta', eventoRespuesta );
});

Liferay.on('hideTipoTramiteGMMFormEvent',function(event){ 
	console.log("on hideTipoTramiteGMMFormEvent");
	document.getElementById('div_tipo_tramite_gmm').style.display = 'none';
});

Liferay.on('checkTipoTramiteGMMReembolso',function(event){ 
	Liferay.fire('cambiaTituloDatosTramite', {titulo:A.one('#<portlet:namespace />tipoTramiteGMMReembolso').get('checked')?
			"Solicitud de Reembolso de Gastos M&eacute;dicos Mayores":"Solicitud de Cirug&iacute;a Programada de Gastos M&eacute;dicos Mayores"} );
});

Liferay.on('showTipoTramiteGMMFormEvent',function(event){ 
	A.one('#<portlet:namespace />tipoTramiteGMMReembolso').set('checked', false);
	A.one('#<portlet:namespace />tipoTramiteGMMCirugia').set('checked', false);
	
	console.log("on showTipoTramiteGMMFormEvent");
	document.getElementById('div_tipo_tramite_gmm').style.display = 'block';
	document.getElementById('fm_div_tipo_tramite_gmm').className = 'col-md-12 text-center';
	if (!event.hideModal){
		showIndicacionesTramiteGMMModal();
	}
	
});
Liferay.provide(window, 'showIndicacionesTramiteGMMModal', function() { 
	sessionStorage.setItem('opcion-cirugia-reembolso', 'true');
	if(sessionStorage.getItem('opcion-cirugia-programada')){
		sessionStorage.removeItem('opcion-cirugia-programada');
		console.log("Borrando opcion storage :: opcion-cirugia-programada");
	}
	
	A.one('#<portlet:namespace />indicaciones_tramite_gmm_modal')._node.style.display = 'block';
	Liferay.fire('cambiaTituloDatosTramite', {titulo:"Solicitud de Reembolso de Gastos M&eacute;dicos Mayores"} );
	Liferay.fire('tipoTramiteVidaSucursalNormalEvent', {} );
});
Liferay.provide(window, 'showIndicacionesTramiteGMMModalCirugia', function() { 
	A.one('#<portlet:namespace />indicaciones_tramite_gmm_modal')._node.style.display = 'block';
	sessionStorage.setItem('opcion-cirugia-programada', 'true');
	if(sessionStorage.getItem('opcion-cirugia-reembolso')){
		sessionStorage.removeItem('opcion-cirugia-reembolso');
		console.log("Borrando opcion storage :: opcion-cirugia-reembolso");
	}
	
	Liferay.fire('cambiaTituloDatosTramite', {titulo:"Solicitud de Cirug&iacute;a Programada de Gastos M&eacute;dicos Mayores"} );
	Liferay.fire('tipoTramiteVidaSucursalNormalEvent', {} );
});

Liferay.provide(window, 'closeIndicacionesTramiteGMMModal', function() { 
	A.one('#<portlet:namespace />indicaciones_tramite_gmm_modal')._node.style.display = 'none';
});

console.log('antes de fire inicial validaReglasVidaAlIniciar, tipoTramiteGMM ');
Liferay.fire('validaReglasVidaAlIniciar', { });	

</aui:script>