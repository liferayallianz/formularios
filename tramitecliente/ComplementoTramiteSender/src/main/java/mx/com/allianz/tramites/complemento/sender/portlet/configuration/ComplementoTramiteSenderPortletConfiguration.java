package mx.com.allianz.tramites.complemento.sender.portlet.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;


/**
 * @author Testing Zone
 */
@ExtendedObjectClassDefinition(category = "Productivity",
scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE)
@Meta.OCD(
		id = "mx.com.allianz.tramites.complemento.sender.portlet.configuration.ComplementoTramiteSenderPortletConfiguration",
		localization = "content/Language",
		name = "Configuracion Tramite Complemento"
	)
public interface ComplementoTramiteSenderPortletConfiguration {
	

	/**
	 * rootPath: This is the root path that constrains all filesystem access.
	 */
	@Meta.AD(
		deflt = "/opt/docs_tramites", 
		description = "Ruta Temporal de Tramites ",
		required = false
	)
	public String rootPath();

	@Meta.AD(
		deflt = "7200000", 
		description = "Tiempo de espera para borrar archivos(millis) ",
		required = false
	)
	public long waitClean();

	@Meta.AD(
		deflt = "5000", 
		description = "Request Wait ",
		required = false
	)
	public long waitRequest();
	
	@Meta.AD(
			deflt = "complemento-tramite:/web/guest/confirmacion-tramite:/web/guest/general-tramite", 
			description = "Mapeo de posible ruta con redireccionamiento",
			required = false
		)
	public String[] redirectPair();
}
