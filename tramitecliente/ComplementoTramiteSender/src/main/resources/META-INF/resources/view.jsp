<%@ include file="/init.jsp" %>
<%@page import="javax.portlet.RenderRequest"%>
<%@page import="com.liferay.portal.kernel.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.theme.PortletDisplay"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="mx.com.allianz.tramites.complemento.sender.portlet.CompelmentoTramiteSenderPortlet" %>


<%@page import="mx.com.allianz.tramites.complemento.sender.portlet.exception.TramiteException"%>
<%@page import="mx.com.allianz.tramites.complemento.sender.portlet.exception.TramiteWSException"%>

<%@ page import="com.liferay.portal.kernel.util.Constants" %>

<liferay-ui:error exception="<%= TramiteException.class %>" message="send.tramite.puc.failed" />
<liferay-ui:error exception="<%= TramiteWSException.class %>" message="send.tramite.puc.failed.ws" />

<portlet:actionURL var="complementoTramitesActionURL" name="processAction">
</portlet:actionURL>
<portlet:resourceURL var="actualizaTramite" />

<%
	ThemeDisplay themeDisplay2 = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	String themeCurrentURL = themeDisplay2.getURLCurrent();
	String themePortalURL = themeDisplay2.getURLPortal();
	String themeHomeURL = themeDisplay2.getURLHome();
	String themeLanguajeURL = themeDisplay2.getI18nPath();

%>

<aui:form name="complemento-form" action="<%=complementoTramitesActionURL.toString()%>">
    	<aui:input name="<%= Constants.CMD %>" type="hidden" value="cmd_next_page"  />
    	<aui:input name="version-complementotramitesender" type="hidden" value="1.1.3.1"  />
    	<aui:button-row>
        		<aui:button name="saveButton" class="btn btn-primary pull-right" type="button" value="complementotramitesender_ComplementoTramiteSender.send" id="submitBtn" onclick="validateComplementoTramiteForm('cmd_next_page');"/>
   			<aui:button class="btn pull-right" name="cancelButton" type="button" value="complementotramitesender_ComplementoTramiteSender.clean" onclick="window.history.back();" />
    	</aui:button-row>
    	<input id="complemento_form_flag" type="hidden" name="complemento_form_flag" value=""/>
</aui:form>
<div class="fade in lex-modal modal" id="<portlet:namespace/>cargando_envio_servicio_modal"
role="dialog" tabindex="-1" style="display:none;">
	<div class="modal-dialog modal-full-screen" style="height:500px;">
		<div class="modal-content" style="background-color:transparent;border-width:0px;box-shadow:0 0 0px 0px rgba(0, 0, 0, 0.2)">
			<div id="<portlet:namespace/>cargando_envio_servicio_modal_div">
					<div class="modal-body modal-body-no-header modal-body-no-footer" style="background-color:transparent;" >
						<div class="loading-icon loading-icon-lg"></div>
					</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript"> 
console.log('complemento tramite v1.1.3.1');

var formsValidSectionClienteTramite = [];
var formsValidSectionClienteTramiteReceived = [];
var allSucceed = true;
var allowProcesing = true;
var notSend = true;
<%="Vida".equals(request.getAttribute("tipoTramite"))?"":"Liferay.fire('muestraDatosReclamacion', {} );"%>

Liferay.provide(window, 'validateComplementoTramiteForm', function(accion) {
	if (allowProcesing) {
		allowProcesing = false;
		showCargandoEnvioServicioModal();
		console.log("validateComplementoTramiteForm");
		formsValidSectionClienteTramite = [];
		formsValidSectionClienteTramiteReceived = [];
		allSucceed = true;
	
		$("#<portlet:namespace/><%= Constants.CMD %>").val(accion);
		Liferay.fire('validaDatosLugarRespuesta', {} );
		Liferay.fire('validaDatosComplementoReclamacion', {} );
		Liferay.fire('validaCaptcha', {} );
		Liferay.fire('validaFileUplaod', {} );
		if (console){
			console.log("fire end validateComplementoTramiteForm");
		}
	} else {
		if (console){
			console.log("double click");
		}
	}
});

Liferay.on('validaDatosLugarRespuestaRespuesta', validaSeccionTramite);
Liferay.on('validaDatosComplementoReclamacionRespuesta', validaSeccionTramite);
Liferay.on('validaCaptchaRespuesta', validaSeccionTramite);
Liferay.on('validaFileUplaodRespuesta', validaSeccionTramite);

function validaSeccionTramite(respuestaSeccion) { 
	console.log("desde la respuesta la seccion es" + respuestaSeccion.valido);
	formsValidSectionClienteTramiteReceived.push(respuestaSeccion.valido);
	if (respuestaSeccion.valido) {
		formsValidSectionClienteTramite.push(respuestaSeccion.valido);
		callServeResourceTramite(respuestaSeccion);
	} else {
		allSucceed = false;
		hideCargandoEnvioServicioModal();
	}
	allowProcesing = (formsValidSectionClienteTramiteReceived.length % 3 == 0);
}

function sendSubmitTramite() {
	console.log("submit formsValidSectionClienteTramite.length" + formsValidSectionClienteTramite.length);
	if(formsValidSectionClienteTramite.length % 3 == 0) {
		if (notSend) {
			if($("#<portlet:namespace/><%= Constants.CMD %>").val() == 'cmd_next_page'){
				var todosVerdaderos = true;
				for(llave in formsValidSectionClienteTramite){
					todosVerdaderos = todosVerdaderos && formsValidSectionClienteTramite[llave];
					if(!todosVerdaderos) return;
					
				}
			}
			
			if(sessionStorage.getItem('opcion-cirugia-programada')){
				sessionStorage.removeItem('opcion-cirugia-programada');
				console.log("Borrando opcion storage :: opcion-cirugia-programada");
			}
			if(sessionStorage.getItem('opcion-cirugia-reembolso')){
				sessionStorage.removeItem('opcion-cirugia-reembolso');
				console.log("Borrando opcion storage :: opcion-cirugia-reembolso");
			}
			
			notSend = false;
			new AUI().use(function(A) {
				console.log("Enviado version sin captcha - submit send" + A);
				var formaComplemento = A.one('#<portlet:namespace/>complemento-form');
				console.log("submit formaComplemento" + formaComplemento);
				document.getElementById('complemento_form_flag').value='bandera';
				formaComplemento.submit();
				//notSend = true;
			});
		} 
		else console.log("No  pudo pasar!!!!");
	} 
}

function callServeResourceTramite(seccion) {
	$.post('<%=actualizaTramite.toString()%>',{
		// Lugar Respuesta 
		estado: seccion.estado,
		cplugarAtencion: seccion.cplugarAtencion,
		cpTramite :seccion.cpTramite,
		subgrupo: seccion.subgrupo,
		subgrupoDescripcion: seccion.subgrupoDescripcion,
		grupoDescripcion: seccion.grupoDescripcion,
		grupo: seccion.grupo,
		// Complemento Reclamacion
		moneda: seccion.moneda,
		montoReclamado: seccion.monto,
		cantidadFacturaRecibos: seccion.cantidadFacturaRecibos,
		// Filesystem Access
		fileFolder: seccion.fileFolder,
		// Captcha
		captcha: seccion.captcha
		
	},function(data, status){
		sendSubmitTramite();
	}); 
}

Liferay.provide(window, 'showCargandoEnvioServicioModal', function() { 
	new AUI().use(function(A) {
		A.one('#<portlet:namespace />cargando_envio_servicio_modal')._node.style.display = 'block';
	});
});

Liferay.provide(window, 'hideCargandoEnvioServicioModal', function() { 
	new AUI().use(function(A) {
		A.one('#<portlet:namespace />cargando_envio_servicio_modal')._node.style.display = 'none';
	});
});

var flag =  document.getElementById('complemento_form_flag');
console.log(flag);
if (flag && flag.value) {
	var newLocation = '<%= themePortalURL %>'.concat('<%= themeLanguajeURL %>').concat('/').concat('general-tramite');
	console.log('newLocation = ' + newLocation);
	window.location.replace(newLocation);
}
</script>


</script>
<% if(request.getAttribute(CompelmentoTramiteSenderPortlet.LLAVE_DATOS_COMPLEMENTO)!=null){ %>
<aui:script use="aui-base">
	console.log("iniciando carga de datos tramite complementp");
	var datosTramiteComplemento = <%=new Gson().toJson(request.getAttribute(CompelmentoTramiteSenderPortlet.LLAVE_DATOS_COMPLEMENTO))%>;
	console.log(datosTramiteComplemento);
	cargaDatosLugarRespuesta(datosTramiteComplemento);
	cargaDatosLugarRespuesta(datosTramiteComplemento);
	
</aui:script>
<%}%>