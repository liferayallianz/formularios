<%@ include file="/init.jsp" %>

</portlet:actionURL> 

<div class="formularioConfirmacionCentrado">
<p class="text-left">
	 Su solicitud ha sido enviada con &eacute;xito </br>
 En breve recibir&aacute; un correo de confirmaci&oacute;n.</br>
 Gracias.</br>
</br>
 En caso de no recibir el e-mail de confirmaci&oacute;n &oacute; si tiene alg&uacute;n comentario acerca del servicio, favor de contactarnos para tr&aacute;mites de GMM al 01 800 1111 400 y 5201 3181 y para tr&aacute;mites de Vida al 5201 3039 &oacute; bien al correo: atencion.quejas@allianz.com.mx 
</br>
</br>
 Atentamente: Allianz M&eacute;xico</br>
</p>
<p>
</p>
</div>
