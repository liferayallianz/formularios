package mx.com.allianz.web.tramite.portlet.portlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.web.tramite.portlet.portlet.configuration.TramiteConfirmacionPortletConfiguration;

@Component(
	configurationPid = "mx.com.allianz.web.tramite.portlet.portlet.configuration.TramiteConfirmacionPortletConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // fin parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Tramite Confirmacion Sender",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest",
		"javax.portlet.supported-processing-event=solicitanteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent"

	},
	service = Portlet.class
)
public class ConfirmacionTramitePortlet extends MVCPortlet {
	@Override
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		String currentURL = PortalUtil.getCurrentURL(actionRequest);
		_log.info("DatosClienteTramiteSenderPortlet -> procesando tramite -> currentURL = " + currentURL);

		String redirectURL = Arrays.stream(redirectPair())
			  .map(uri -> uri.split(":"))
			  .filter(pairURI -> currentURL.contains(pairURI[0]))
			  .map(pairURI -> pairURI[1])
			  .findAny()
			  .orElse("/general-tramite");
		
		_log.info("ConfirmacionTramitePortlet -> processAction -> redirectURL = " + redirectURL);
		actionResponse.sendRedirect(redirectURL);
	}
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		if (_log.isDebugEnabled()){
			_log.debug("ConfirmacionTramitePortlet -> processEvent de complemento");
		}
		try {
			PortletSession session = request.getPortletSession();
			if (Optional.ofNullable(request)
					.map(EventRequest::getEvent)
					.map(Event::getName)
					 .filter("solicitanteTramiteClienteEvent"::equals)
					 .isPresent()) {
				session.removeAttribute("tramite", PortletSession.APPLICATION_SCOPE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (_log.isErrorEnabled()) {
				_log.error(e.getStackTrace());
			}
		}
	}
	
	private String[] redirectPair(){
		return _configuration.redirectPair();
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				TramiteConfirmacionPortletConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info("Configured Cliente Tramite : { " +
					"redirect pair : " + _configuration.redirectPair() +
					" }");
		}
	}
	
	private TramiteConfirmacionPortletConfiguration _configuration ;
	private static Log _log = LogFactoryUtil.getLog(ConfirmacionTramitePortlet.class);

}