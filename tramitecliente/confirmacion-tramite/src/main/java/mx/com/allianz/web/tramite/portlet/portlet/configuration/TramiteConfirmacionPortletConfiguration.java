package mx.com.allianz.web.tramite.portlet.portlet.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
		category = "Productivity",
		scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
	)
	@Meta.OCD(
		localization = "content/Language",
		name = "Configuracion Tramite Confirmacion",
		id = "mx.com.allianz.web.tramite.portlet.portlet.configuration.TramiteConfirmacionPortletConfiguration"
	)
public interface TramiteConfirmacionPortletConfiguration {
	
	@Meta.AD(
			deflt = "confirmacion-tramite:/web/guest/general-tramite,confirmacionp-tramite:/group/guest/generalp-tramite", 
			description = "Mapeo de posible ruta con redireccionamiento",
			required = false
		)
	public String[] redirectPair();
	
}
