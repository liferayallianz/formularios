package mx.com.allianz.tramites.datos.sender.portlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.Cookie;
import javax.xml.namespace.QName;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.commons.dto.cliente.ClienteTramiteDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.module.service.facade.AllianzService;
import mx.com.allianz.tramites.datos.sender.portlet.configuration.TramiteGeneralPortletConfiguration;
import mx.com.allianz.tramites.datos.sender.portlet.exception.IncosistenciaDatosException;

@Component(
	configurationPid = "mx.com.allianz.tramites.datos.sender.portlet.configuration.TramiteGeneralPortletConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        "com.liferay.portlet.remoteable=true",
		// fin Se agregan parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Tramite General Sender",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agrega nombre de mensaje y cola del portlet para publicar
        "javax.portlet.supported-publishing-event=clienteTramiteEvent;http://mx-allianz-liferay-namespace.com/events/clienteTramiteEvent",
		"javax.portlet.supported-publishing-event=clienteFirmadoEvent;http://mx-allianz-liferay-namespace.com/events/clienteFirmadoEvent",
		"javax.portlet.supported-publishing-event=tramiteClienteHabilitadoEvent;http://mx-allianz-liferay-namespace.com/events/tramiteClienteHabilitadoEvent",
		"javax.portlet.supported-processing-event=inconsistenciaTramiteEvent;http://mx-allianz-liferay-namespace.com/events/inconsistenciaTramiteEvent",
		"javax.portlet.supported-processing-event=datosTramiteEvent;http://mx-allianz-liferay-namespace.com/events/datosTramiteEvent",
		// Fin Se agrega nombre de mensaje y cola del portlet para publicar
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DatosTramiteSenderPortlet extends MVCPortlet {

	public static final String LLAVE_CURRENT_PAGE_URL = "currentPageURL"; 
	public static final String LLAVE_TRAMITE = "tramite"; 
	public static final String LLAVE_TRAMITE_EVENT = "clienteTramiteEvent"; 
	public static final String LLAVE_CLIENTE_FIRMADO = "clienteFirmado"; 
	public static final String LLAVE_CLIENTE_FIRMADO_EVENT = "clienteFirmadoEvent"; 
	public static final String LLAVE_CLIENTE_HABILITADO = "tramiteClienteHabilitado"; 
	public static final String LLAVE_CLIENTE_HABILITADO_EVENT = "tramiteClienteHabilitadoEvent"; 
	
	public static final String LLAVE_DATOS_TRAMITE_EVENT = "datosTramiteEvent";
	public static final String LLAVE_DATOS_TRAMITE = "tramiteRec";
	
	public static final String LLAVE_INCONSISTENCIA_EVENT = "inconsistenciaTramiteEvent";
	public static final String LLAVE_INCONSISTENCIA_ERROR = "inconsistenciaTramiteError";
	public static final String CONSULTA_EXITOSA_CLIENTE = "\"estatus\":\"true\""; 


	@Override
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		_log.debug("DatosTramiteSenderPortlet -> procesando tramite");
		Gson gson = new Gson();

		String currentURL = PortalUtil.getCurrentURL(actionRequest);
		_log.info("DatosTramiteSenderPortlet -> procesando tramite -> currentURL = " + currentURL);
		
		// definicion de llamado de eventos de formulario
		TramiteDto tramite = buildTramite(actionRequest);
		actionRequest.getPortletSession().removeAttribute(LLAVE_TRAMITE, PortletSession.APPLICATION_SCOPE);
		String tramiteJson = gson.toJson(tramite);
		_log.info("DatosTramiteSenderPortlet -> processAction -> tramite.isTramiteClienteHabilitado = " + tramite.isTramiteClienteHabilitado());
		_log.debug("DatosTramiteSenderPortlet -> processAction -> tramiteJson = " + tramiteJson);

		QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/clienteTramiteEvent", LLAVE_TRAMITE_EVENT);
		actionResponse.setEvent(qName, tramiteJson);
		
		if(isClientEnabled()) {
			String cliente = buildClienteFirmado(actionRequest);
			System.out.println("DatosTramiteSenderPortlet -> processAction -> cliente firmado = " + cliente);
			QName qNameCliente = new QName("http://mx-allianz-liferay-namespace.com/events/clienteFirmadoEvent", LLAVE_CLIENTE_FIRMADO_EVENT);
			actionResponse.setEvent(qNameCliente, cliente);
			
			_log.info("DatosTramiteSenderPortlet -> processAction -> isClientEnabled() = " + isClientEnabled());
			System.out.println("DatosTramiteSenderPortlet -> processAction -> isClientEnabled() = " + isClientEnabled());

			QName qNameClienteHabilitado = new QName("http://mx-allianz-liferay-namespace.com/events/tramiteClienteHabilitadoEvent", LLAVE_CLIENTE_HABILITADO_EVENT);
			actionResponse.setEvent(qNameClienteHabilitado, isClientEnabled());
			
		}
		
		String redirectURL = Arrays.stream(redirectPair())
			  .map(uri -> uri.split(":"))
			  .peek(rurl -> _log.info("redirectURL element = " + rurl))
			  .filter(pairURI -> currentURL.contains(pairURI[0]))
			  .map(pairURI -> pairURI[1])
			  .peek(rurl -> _log.info("redirectURL element found = " + rurl))
			  .findAny()
			  .orElse("/cliente-tramite");
		
		_log.info("DatosTramiteSenderPortlet -> processAction -> redirectURL = " + redirectURL);
		System.out.println("DatosTramiteSenderPortlet -> processAction -> redirectURL = " + redirectURL);

		actionResponse.sendRedirect(redirectURL);
	}
	
	public String buildClienteFirmado(ActionRequest request) {
		return  Optional.ofNullable(request.getPortletSession().getAttribute(LLAVE_CLIENTE_FIRMADO, PortletSession.APPLICATION_SCOPE))
				  .filter(p -> p instanceof String)
				  .map( p -> (String) p).orElse(null);
	}
	
	public TramiteDto buildTramite(ActionRequest request) {
		Gson gson = new Gson();

		TramiteDto tramite = Optional.ofNullable(request.getPortletSession().getAttribute(LLAVE_TRAMITE, PortletSession.APPLICATION_SCOPE))
				  .filter(p -> p instanceof String)
				  .map( p -> (String) p)
				  .map(json -> gson.fromJson((String) json, TramiteDto.class))
				  .orElseGet(TramiteDto::new);
		PortletSession session = request.getPortletSession();
		Arrays.stream(FormularioTramiteCampos.values())
			  .forEach(campo -> 
			  	  campo.buildTramite(tramite, campo.getParamFromSession(session))
			  );
		
		return tramite;
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		PortletSession session = resourceRequest.getPortletSession();
		Arrays.stream(FormularioTramiteCampos.values())
			  .forEach(campo ->  
			  	  campo.uploadToSession(session, campo.getParamFromRequest(resourceRequest))
			  );
		super.serveResource(resourceRequest, resourceResponse);
	}
	
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws IOException, PortletException {

		PortletSession session = request.getPortletSession();			
		Gson gson = new Gson();

		Optional.ofNullable(session
					.getAttribute(LLAVE_INCONSISTENCIA_ERROR, PortletSession.APPLICATION_SCOPE)
				)
				.map(error -> {
					session.setAttribute(LLAVE_INCONSISTENCIA_ERROR, PortletSession.APPLICATION_SCOPE);
					return error;
				})
				.filter(p -> p instanceof String)
				.map( p -> (String) p)
				.ifPresent(message -> 
					SessionErrors.add(request, IncosistenciaDatosException.class, 
							new IncosistenciaDatosException("Hubo un error al registras su tramite"))
				);
		_log.info("isClienteEnabled = " + isClientEnabled());

		TramiteDto tramite = Optional.ofNullable(request.getPortletSession()
				.getAttribute(LLAVE_TRAMITE, PortletSession.APPLICATION_SCOPE))
				.filter(p -> p instanceof String)
				.map( p -> (String) p)
				.map(json -> gson.fromJson((String) json, TramiteDto.class))
				.orElseGet(TramiteDto::new);
		tramite.setTramiteClienteHabilitado(isClientEnabled());
		if (isClientEnabled()) {
			_log.info("allianzService = " + allianzService);
			if (allianzService != null) {
				_log.info("El servicio de Allianz se seteo correctamente");
				
				Arrays.stream(Optional.ofNullable(request.getCookies())
									  			.orElseGet(() -> new Cookie[0]))
								.filter(cookie -> allianzClientToken().equals(cookie.getName()))
								.map(Cookie::getValue)
								.peek(clienteToken -> _log.info("El token con id cliente es: " + clienteToken))
								.findAny()
								.ifPresent(cookie -> {
									//_log.info("Cookie a pata  = muDrUM0UNj2mGdfjjcobtzgtijjJ++mahBgY/9PC2Ypn+PfPOjUAPoItR1tvBtDmzTipU121Tu/63vZuQGhB7c8suZnFXiCsMBQ8NLoMWD0=");
									Optional.ofNullable(allianzService.obtenerDatosTestingZone(cookie))
											.map(clienteBuscado -> {
												_log.info("DatosTramiteSenderPortlet -> render -> clienteBuscado = " + clienteBuscado);
												return clienteBuscado;
											})
											.filter(c -> c.contains(CONSULTA_EXITOSA_CLIENTE))
											.map(json -> gson.fromJson(json, ClienteTramiteDTO.class))
											.ifPresent(cliente -> {
												session.setAttribute(LLAVE_CLIENTE_FIRMADO, 
														gson.toJson(cliente.getCliente()), PortletSession.APPLICATION_SCOPE);
												tramite.setClienteFirmado(cliente);
												tramite.fillClient();
											});
								});

			} else {
				_log.info("Reiniciar modulo mx.com.allianz.module.service.facade ");
				_log.info("$ telnet localhost 11311 ");
				_log.info("g! lb | grep mx.com.allianz.module.service.facade ");
				_log.info("g! stop PID ");
				_log.info("g! start PID ");
				_log.info("g! disconnect ");
				_log.info("YES ");
			}
		}
		
		_log.info("tramite antes de subir sesion, tramite = " + tramite);
		session.setAttribute(LLAVE_TRAMITE, gson.toJson(tramite), PortletSession.APPLICATION_SCOPE);
		request.setAttribute(LLAVE_TRAMITE, gson.toJson(tramite));
				
		_log.info("DatosTramiteSenderPortlet -> render -> Saliendo ");

		String currentURL = PortalUtil.getCurrentURL(request);
		String[] currentPair = Arrays.stream(redirectPair())
				  .map(uri -> uri.split(":"))
				  .peek(rurl -> System.out.println("redirectURL pair = [" + rurl[0] + ", " + rurl[1] +"]" ))
				  .filter(pairURI -> currentURL.contains(pairURI[0]))
				  .findAny()
				  .orElse(redirectPair()[0].split(":"));
		
		String[] paths = currentPair[1].split("/");
		String redirect = (paths.length > 2 ? Arrays.stream(Arrays.copyOfRange(paths, 0, paths.length-1)).collect(Collectors.joining("/")) : "" ) 
				+ (currentPair[0].contains("/") ? currentPair[0] : ("/"+currentPair[0]));
		session.setAttribute(LLAVE_CURRENT_PAGE_URL, redirect, PortletSession.APPLICATION_SCOPE);
		request.setAttribute(LLAVE_CURRENT_PAGE_URL, redirect);

		super.render(request, response);
	}
	


	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		_log.debug("processEvent de complemento");
		if(_log.isDebugEnabled()) {
			_log.debug("DatosTramiteSolicitanteSenderPortlet -> processEvent solicitante");
		}
		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		Optional<Event> event = Optional.ofNullable(request)
										.map(EventRequest::getEvent);
		String inconsistenciaErrorMessage = "Inconsistencia al intentar enviar tramite a sistema remoto," + 
				" no se encontro uno o mas capos obligatorios, favor de registrar nuevamente su tramite o" + 
				" contacte a al administrador";
		if (event.map(Event::getName)
				 .filter(LLAVE_INCONSISTENCIA_EVENT::equals)
				 .isPresent()) {
			SessionErrors.add(request, IncosistenciaDatosException.class , new IncosistenciaDatosException("Hubo un error al registras su tramite"));
			session.setAttribute(LLAVE_INCONSISTENCIA_ERROR, inconsistenciaErrorMessage, PortletSession.APPLICATION_SCOPE);

			event.map(Event::getValue)
				 .filter(val -> (val instanceof String))
				 .map(json -> gson.fromJson((String) json, TramiteDto.class))
				 .ifPresent(obj -> {
					 if(_log.isErrorEnabled()) {
							_log.error("DatosTramiteSolicitanteSenderPortlet -> processEvent -> inconsistenciaTramiteEvent,  tramite = " + obj);
					}
				 });
		}
		
		if (event.map(Event::getName)
				 .filter(LLAVE_DATOS_TRAMITE_EVENT::equals)
				 .isPresent()) {
			event.map(Event::getValue)
			 .filter(val -> (val instanceof String))
			 //.map(json -> gson.fromJson((String) json, TramiteDto.class))
			 .ifPresent(obj -> {
				 if(_log.isErrorEnabled()) {
						_log.info("DatosTramiteSolicitanteSenderPortlet -> processEvent -> datosTramiteEvent,  tramite = " + obj);
				}
				 session.setAttribute(LLAVE_TRAMITE, obj, PortletSession.APPLICATION_SCOPE);
			 });
		}
	}
	
	public String allianzClientToken() {
		return _configuration.allianzClientToken();
	}
	
	public Boolean isClientEnabled() {
		return _configuration.enableClientConfiguration();
	}
	
	private String[] redirectPair(){
		return _configuration.redirectPair();
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				TramiteGeneralPortletConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info("Configured General Tramite (datostramitesender -  v1.0.1) : \n{ " +
					"enable client : " + _configuration.enableClientConfiguration() + ", " +
					"allianz client token : " + _configuration.allianzClientToken() + ", " +
					"redirect pair: " + _configuration.redirectPair() +
					" }");
		}
	}
	
	@Reference
	protected void setAllianzService(AllianzService allianzService) {
		this.allianzService = allianzService;
	}

	protected void unsetAllianzService(AllianzService allianzService) {
		this.allianzService = null;
	}

	private AllianzService allianzService;
	private TramiteGeneralPortletConfiguration _configuration ;
	private static Log _log = LogFactoryUtil.getLog(DatosTramiteSenderPortlet.class);

}