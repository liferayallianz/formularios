package mx.com.allianz.tramites.datos.sender.portlet.exception;

public class IncosistenciaDatosException  extends Exception {

	private static final long serialVersionUID = 7492156517715792447L;

	public IncosistenciaDatosException() {
		super();
	}

	public IncosistenciaDatosException(String message) {
		super(message);
	}

	public IncosistenciaDatosException(Throwable cause) {
        super(cause);
    }
}