package mx.com.allianz.tramites.datos.sender.portlet.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
		category = "Productivity",
		scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
	)
	@Meta.OCD(
		localization = "content/Language",
		name = "Configuracion Tramite General",
		id = "mx.com.allianz.tramites.datos.sender.portlet.configuration.TramiteGeneralPortletConfiguration"
	)
public interface TramiteGeneralPortletConfiguration {
	
	@Meta.AD(
			deflt = "false", 
			description = "Activa configuracion de Clientes a modulo de Tramites",
			required = false
		)
	public Boolean enableClientConfiguration();
	
	@Meta.AD(
			deflt = "azctToken", 
			description = "Token con llave de cliente",
			required = false
		)
	public String allianzClientToken();
	
	@Meta.AD(
			deflt = "general-tramite:/web/guest/cliente-tramite,generalp-tramite:/group/guest/clientep-tramite", 
			description = "Mapeo de posible ruta con redireccionamiento",
			required = false
		)
	public String[] redirectPair();
	
}
