<%@ include file="/init.jsp" %>
<%@page import="mx.com.allianz.tramites.datos.sender.portlet.exception.IncosistenciaDatosException"%>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="mx.com.allianz.commons.dto.tramite.TramiteDto" %>
<%@ page import="mx.com.allianz.tramites.datos.sender.portlet.DatosTramiteSenderPortlet" %>

<% 

		String currentPageURL = "/general-tramite";
        Object object = request.getAttribute("currentPageURL");
		if (object != null && object instanceof String){
			currentPageURL = (String) object;
		}
		
		String tramite = null;
        object = request.getAttribute(DatosTramiteSenderPortlet.LLAVE_TRAMITE);
		if (object != null && object instanceof String){
			tramite = (String) object;
		}
		
		
		
%>

<liferay-ui:error exception="<%= IncosistenciaDatosException.class %>" message="mx.com.allianz.tramites.datos.sender.portlet.inconsistencia.error.message" />
<portlet:actionURL var="tramiteActionURL" name="processAction">
</portlet:actionURL>
<portlet:resourceURL var="actualizaTramite" />
<div id="div_datos_general_tramite_sender" style="display:none;">
<aui:form name="fm-tramite-grl" action="<%=tramiteActionURL.toString()%>">
		<aui:input name="version-datostramitesender" type="hidden" value="1.0.1"  />
    	<aui:button-row>
        	<aui:button name="saveButton" class="btn btn-primary pull-right" type="button" value="datostramitesender_DatosTramiteSender.send" id="submitBtn" onclick="validateAtencionTramiteForm();"/>
    		<aui:button class="btn pull-right" name="cancelButton" type="button" value="datostramitesender_DatosTramiteSender.clean" onclick="currentPageURLJSClean();" />
    	</aui:button-row>
</aui:form>
</div>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("datostramitesender v1.0.1");

var tramite = <%=tramite%>;
console.log("Tramite cargado");
console.log(tramite);

Liferay.on('validaReglasVidaAlIniciar', function(event){
	console.log(tramite.tipoTramite ? "Aparecerán los botones":"Sin tipo trámite");
	document.getElementById('div_datos_general_tramite_sender').style.display = tramite.tipoTramite ? 'block':'none';
	if ( tramite.tipoTramite ){
		Liferay.fire("cargaTipoTramite", tramite);
		Liferay.fire("validaReglasVidaAlIniciarTTGMM", tramite);
		Liferay.fire("validaReglasAlIniciarDatosTramite", tramite);
	}
});
</aui:script>

<script type="text/javascript"> 
var formsValidSectionTramite = [];


Liferay.on('hideDivDatosGeneralTramiteSender', function(event){
	document.getElementById('div_datos_general_tramite_sender').style.display = 'none';
});

Liferay.on('tipoTramiteShowButtonEvent', function(event){
	console.log('div_datos_general_tramite_sender -> tipoTramiteShowButtonEvent');
	document.getElementById('div_datos_general_tramite_sender').style.display = 'block';
});

var currentPageURLJS = '<%=currentPageURL%>';

Liferay.provide(window, 'currentPageURLJSClean', function() {
	console.log('currentPageURLJSClean');
	console.log(currentPageURLJS);
	var redir = currentPageURLJS;
	window.location.href = redir;

});

Liferay.provide(window, 'validateAtencionTramiteForm', function() {
	console.log("validateAtencionTramiteForm");
	formsValidSectionTramite = [];
	console.log('validateAtencionTramiteForm fires');
	
	Liferay.fire('validaTipoTramite', {} );
	console.log('validaTipoTramite fire');
	Liferay.fire('validaTipoTramiteGMM', {} );
	console.log('validaTipoTramiteGMM fire');
	Liferay.fire('validaDatosSolicitudTramite', {} );
	console.log('validaDatosSolicitudTramite fire');

	console.log("fire end validateAtencionTramiteForm");
});

Liferay.on('validaTipoTramiteRespuesta', validaSeccionTramite);
Liferay.on('validaTipoTramiteGMMRespuesta', validaSeccionTramite);
Liferay.on('validaDatosSolicitudTramiteRespuesta', validaSeccionTramite);

function validaSeccionTramite(respuestaSeccion) {
	console.log("desde la respesta la seccion es" + respuestaSeccion.valido);
	if (respuestaSeccion.valido) {
		formsValidSectionTramite.push(respuestaSeccion.valid);
		callServeResourceTramite(respuestaSeccion);
		//sendSubmit();
	}
}

function sendSubmitTramite() {
	console.log("submit formsValidSectionTramite.length" + formsValidSectionTramite.length);
	if(formsValidSectionTramite.length % 3 === 0) {
		console.log("submit");
		new AUI().use(function(A) {
			console.log("submit send" + A);
			var formaTramite = A.one('#<portlet:namespace/>fm-tramite-grl');
			console.log("submit formaQueja" +formaTramite);
			formaTramite.submit();
		});
	}
}

function callServeResourceTramite(seccion) {
	$.post('<%=actualizaTramite.toString()%>',{
		tipoTramite: seccion.tipoTramite,
		tipoTramiteGMM: seccion.tipoTramiteGMM,
		estado: seccion.estado,
		municipio: seccion.municipio,
    	sucursal: seccion.sucursal
	},function(data, status){
		sendSubmitTramite();
	}); 
}

</script>


