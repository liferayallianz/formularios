package mx.com.allianz.tramites.tipos.portlet;

import java.io.IOException;
import java.util.Map;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // Fin parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Tramite Tipo Tramite",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		// Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-processing-event=tipoTramiteEvent;http://mx-allianz-liferay-namespace.com/events/tipoTramite",
		"javax.portlet.supported-processing-event=inconsistenciaTramiteEvent;http://mx-allianz-liferay-namespace.com/events/inconsistenciaTramiteEvent",
		// Fin Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class TiposTramitePortlet extends MVCPortlet {
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException { 
		if(_log.isDebugEnabled()) {
			_log.debug("DatosTramiteSolicitanteSenderPortlet -> processEvent solicitante");
		}		
	     super.processEvent(request, response);
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet tipostramite - 1.0.1, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(TiposTramitePortlet.class);

}