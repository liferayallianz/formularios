<%@ include file="/init.jsp" %>

<div id="ir_really_dont_matter" class="radioSpaceMargin" >
<p class="formularioTituloEncabezado">Seleccione el tr&aacute;mite a realizar</p>
<aui:form action="#" name="fm_tipo_tramite" >
		<aui:input name="version-tipostramite" type="hidden" value="1.0.1"  />
		<aui:field-wrapper name="" cssClass="formularioRadioCentrado">
			<aui:input checked="<%=false%>" inlineLabel="right" id="radioTipoTramiteGMM" name="tipoTramite" type="radio" value="gmm" label="tipos.tramite.gmm" onClick="tipoTramiteGMM();"/> &nbsp;
			<aui:input checked="<%=false%>" inlineLabel="right" id="tipoTramiteVida" name="tipoTramite" type="radio" value="vida" label="tipos.tramite.vida" onClick="tipoTramiteVida();">
				<aui:validator name="required" errorMessage="El tipo de Tr&aacute;mite es requerido"/>
			</aui:input>&nbsp;
		</aui:field-wrapper>
</aui:form>	


<div aria-labelledby="Title" 
class="fade in lex-modal modal" id="<portlet:namespace/>indicaciones_tramite_vida_modal"
role="dialog" tabindex="-1" style="display:none;">
	<div class="modal-dialog modal-full-screen"  style="height:500px;">
		<div class="modal-content">
			<div id="<portlet:namespace/>indicaciones_tramite_vida_modal_div">
					<div class="modal-header">
						<button aria-labelledby="Close" class="btn btn-default close" data-dismiss="modal" role="button" type="button" onClick="closeIndicacionesTramiteVidaModal();">
							<svg aria-hidden="true" class="lexicon-icon lexicon-icon-times">
								<use xlink:href="<%= themeDisplay.getPathThemeImages() + "/lexicon/icons.svg" %>#times" />
							</svg>
						</button>
						<h4 class="modal-title " ><liferay-ui:message key="Tr&aacute;mites @Clientes - Documentos para Siniestros Vida" /></h4>
					</div>
					<div class="modal-body">
						<h4>DOCUMENTOS PARA INDEMNIZACI&Oacute;N VIDA INDIVIDUAL</h4>
						</br>
						<h4>DOCUMENTOS PARA COBERTURA B&Aacute;SICA POR FALLECIMIENTO Y GASTOS FUNERARIOS</h4>
						</br>
						<h4>DOCUMENTOS DEL ASEGURADO</h4>
						</br>
						<p>PRESENTAR ORIGINAL DE:<p>
							<p>&emsp;Acta de defunci&oacute;n.</p>
							<p>&emsp;Acta de nacimiento.</p>
						<p>P&oacute;liza que lo acredita como asegurado de Allianz M&eacute;xico vigente, que incluya:</p>
							<p>&emsp;1.-Contratante, n&uacute;mero de p&oacute;liza y anexos.</p>
							<p>&emsp;2.-Regla de suma asegurada y/o suma asegurada.</p>
							<p>&emsp;3.-Designaci&oacute;n de beneficiarios.</p>
							<p>&emsp;4.-Firma del Asegurado.</p>
							<p>&emsp;5.-Firma del Director de la Aseguradora.</p>
						</br>
						<p>PRESENTAR COPIA SIMPLE DE:</p>
							<p>&emsp;Identificaci&oacute;n Oficial vigente (credencial de elector, pasaporte, c&eacute;dula profesional, licencia de conducir, credencial del IMSS) (No aplica para gastos funerarios).</li>
							<p>&emsp;Comprobante de Domicilio en caso de ser diferente al registrado en el IFE o de no encontrarse en la identificaci&oacute;n presentada (Recibos Agua, Luz, Tel&eacute;fono, impuesto predial o estados de cuenta bancarios, con una vigencia no mayor a tres meses de su fecha de emisi&oacute;n) (No aplica para gastos funerarios).</p>
							<p>&emsp;Acta de matrimonio (En caso de que el beneficiario sea el c&oacute;nyuge).</p>
						<br>
						<p>DOCUMENTOS ADICIONALES PARA OTRAS COBERTURAS VIDA INDIVIDUAL</p>
						<p>PARA LA COBERTURA DE MUERTE ACCIDENTAL</p>
						<p>PRESENTAR COPIA SIMPLE POR EL MINISTERIO P&Uacute;BLICO DE:</p>
							<p>&emsp;Actuaciones de Ministerio P&uacute;blico, cuando la causa del fallecimiento sea a consecuencia de accidente.</p>
						</br>
						<p>PARA LA COBERTURA DE P&Eacute;RDIDAS ORG&Aacute;NICAS E INVALIDEZ TOTAL Y PERMANENTE</p>
						<p>PRESENTAR COPIA CERTIFICADA POR EL IMSS Y/O ISSSTE DE:</p>
							<p>&emsp;Formato ST-3 &oacute; ST-4 de Dictamen de Invalidez emitido por el IMSS, o Formato de Invalidez del ISSSTE. (para invalidez o p&eacute;rdidas org&aacute;nicas).</p>
							<p>&emsp;&Uacute;ltimas Radiograf&iacute;as (en caso necesario por p&eacute;rdida org&aacute;nica).</p>
						</br>
					</div>

					<div class="modal-footer">
						<button class="btn btn-link close-modal" data-dismiss="modal" type="button" onClick="closeIndicacionesTramiteVidaModal();"><liferay-ui:message key="Cerrar" /></button>
					</div>
			</div>
		</div>
	</div>
</div>
<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("tipostramite v1.0.1");

var formValidator = Liferay.Form.get('<portlet:namespace />fm_tipo_tramite').formValidator;

Liferay.on('validaTipoTramite', function(event){
	console.log('Tipo Tramite -> Liferay on');
	var eventoRespuesta = {};
	console.log('Tipo Tramite -> formValidator =  ' + formValidator);
	console.log(formValidator);
	if(formValidator) {
		formValidator.validate();
		console.log('Tipo Tramite -> formValidator.validate()');
		eventoRespuesta.valido = !formValidator.hasErrors();
		console.log('validaTipoTramite eventoRespuesta.valido = ' + eventoRespuesta.valido);
		if(eventoRespuesta.valido) {
			var tipoTramite = A.one('#<portlet:namespace />radioTipoTramiteGMM');
			var tipoTramiteChecked = tipoTramite.get('checked') ? 'GMM' : 'Vida';
			eventoRespuesta.tipoTramite = tipoTramiteChecked;
		}
	} else {
		eventoRespuesta.valido = false;
	}
	Liferay.fire('validaTipoTramiteRespuesta', eventoRespuesta );
});

Liferay.provide(window, 'tipoTramiteGMM', function() {
	console.log("Cargando tr�mite gmm");	
	Liferay.fire('showTipoTramiteGMMFormEvent', {hideModal:true} );
	Liferay.fire('hideDivDatosSolicitudTramite', {});
	Liferay.fire('hideDivDatosGeneralTramiteSender', {});
	//Liferay.fire('tipoTramiteVidaSucursalNormalEvent', {} );
	//Liferay.fire('checkTipoTramiteGMMReembolso', {} );
	//A.one('#<portlet:namespace />indicaciones_tramite_vida_modal').style.display = 'none';
});

Liferay.provide(window, 'tipoTramiteVida', function() {
	Liferay.fire('hideTipoTramiteGMMFormEvent', {} );
	Liferay.fire('tipoTramiteVidaSucursalMatrizEvent', {} );
	Liferay.fire('cambiaTituloDatosTramite', {titulo:"Solicitud de Tr&aacute;mite de Vida"} );
	var modalITVM = A.one('#<portlet:namespace />indicaciones_tramite_vida_modal');
	console.log(modalITVM);
	modalITVM._node.style.display = 'block';
});

Liferay.provide(window, 'closeIndicacionesTramiteVidaModal', function() { 
	A.one('#<portlet:namespace />indicaciones_tramite_vida_modal')._node.style.display = 'none';
});

var tipoTramiteVida = A.one('#<portlet:namespace />tipoTramiteVida');
var tipoTramiteChecked = '';
if (tipoTramiteVida.get('checked')) {
	tipoTramiteChecked = 'Vida';
	console.log('show modal Vida');
	A.one('#<portlet:namespace />indicaciones_tramite_vida_modal')._node.style.display = 'block';
}
var radioTipoTramiteGMM = A.one('#<portlet:namespace />radioTipoTramiteGMM');
if (radioTipoTramiteGMM.get('checked')) {
	tipoTramiteChecked = 'GMM';
	console.log('show modal GMM');
}

Liferay.on('cargaTipoTramite', function(event){
console.log("Se inicia carga de tipoTramite");
console.log(event);
if ( event.tipoTramite ){
	console.log("TipoTramite:"+event.tipoTramite);
	if ( event.tipoTramite == "GMM" ){
		A.one('#<portlet:namespace />radioTipoTramiteGMM').set('checked', true);
		Liferay.fire('showTipoTramiteGMMFormEvent', {hideModal:true} );
		Liferay.fire('hideDivDatosSolicitudTramite', {});
	}
	else {
		A.one('#<portlet:namespace />tipoTramiteVida').set('checked', true);
		Liferay.fire('hideTipoTramiteGMMFormEvent', {} );
		Liferay.fire('tipoTramiteVidaSucursalMatrizEvent', {} );
		Liferay.fire('cambiaTituloDatosTramite', {titulo:"Solicitud de Tr&aacute;mite de Vida"} );
		var modalITVM = A.one('#<portlet:namespace />indicaciones_tramite_vida_modal');
		console.log(modalITVM);
		modalITVM._node.style.display = 'block';
	}
console.log("Finaliza carga de tipoTramite");
}
});

console.log('antes de fire inicial validaReglasVidaAlIniciar, tipoTramite ');
Liferay.fire('validaReglasVidaAlIniciar', { });	



</aui:script>
