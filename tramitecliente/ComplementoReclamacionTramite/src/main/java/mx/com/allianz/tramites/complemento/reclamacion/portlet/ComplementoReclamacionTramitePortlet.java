package mx.com.allianz.tramites.complemento.reclamacion.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import mx.com.allianz.commons.catalogos.dto.MonedaDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.service.moneda.model.Moneda;
import mx.com.allianz.service.moneda.service.MonedaLocalServiceUtil;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // Fin parametros para llamado de eventos de portlet
        // Se agregan librerias de javascript adicionales por portlet
		"com.liferay.portlet.footer-portlet-javascript=content/js/lib/autoNumeric.js",
        // Fin librerias de javascript adicionales
		"javax.portlet.display-name=Tramite Datos Reclamacion",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest",
		// Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-processing-event=solicitanteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent"
	},
	service = Portlet.class
)
public class ComplementoReclamacionTramitePortlet extends MVCPortlet {
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try {

			List<MonedaDTO> monedas = Optional.ofNullable(MonedaLocalServiceUtil.getMonedas(-1,-1))
											  .orElseGet(ArrayList<Moneda>::new)
											  .stream()
											  .map(moneda -> 
											  		new MonedaDTO(moneda.getIdMoneda(),
											  					  moneda.getCodigoMoneda(), 
											  					  moneda.getDescripcion())
											  )
											  .collect(Collectors.toList());
			renderRequest.setAttribute("monedas", monedas);
			renderRequest.setAttribute("tipoTramite", renderRequest.getPortletSession().getAttribute("tipoTramite", PortletSession.APPLICATION_SCOPE));
			renderRequest.setAttribute("tipoTramiteGMM", renderRequest.getPortletSession().getAttribute("tipoTramiteGMM", PortletSession.APPLICATION_SCOPE));
		} catch (Exception e ) { 
			e.printStackTrace(); 
		}

		super.render(renderRequest, renderResponse);
	}
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		if(_log.isDebugEnabled()){
			_log.debug("ComplementoReclamacionTramitePortlet -> processEvent");
		}
		
		// Se obtiene el evento del request
		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		Optional<Event> event = Optional.ofNullable(request)
										.map(EventRequest::getEvent);
		if (event.map(Event::getName)
				 .filter("solicitanteTramiteClienteEvent"::equals)
				 .isPresent()) {
			event.map(Event::getValue)
				 .filter(val -> (val instanceof String))
				 .map(json -> gson.fromJson((String) json, TramiteDto.class))
				 .ifPresent(tramite -> {
					 	if(_log.isDebugEnabled()){
							_log.debug("Objeto tramite: " + tramite);
						}
						session.setAttribute("tipoTramite", tramite.getTipoTramite(), PortletSession.APPLICATION_SCOPE);
						session.setAttribute("tipoTramiteGMM", tramite.getTipoTramiteGMM(), PortletSession.APPLICATION_SCOPE);
					session.setAttribute("tipoTramite", tramite, PortletSession.APPLICATION_SCOPE);
				 });
		}
		super.processEvent(request, response);
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet complementoreclamaciontramite - 1.0.1, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(ComplementoReclamacionTramitePortlet.class);

}