package mx.com.allianz.tramte.solicitante.sender.portlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.Cookie;
import javax.xml.namespace.QName;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.commons.dto.cliente.ClienteTramiteDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.module.service.facade.AllianzService;
import mx.com.allianz.tramte.solicitante.sender.portlet.configuration.TramiteSolicitantePortletConfiguration;

@Component(
	configurationPid = "mx.com.allianz.tramte.solicitante.sender.portlet.configuration.TramiteSolicitantePortletConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // Terminan parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Tramite Solicitante Sender",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest",
		//Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-publishing-event=solicitanteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent",
		"javax.portlet.supported-processing-event=clienteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/clienteTramiteClienteEvent",
		"javax.portlet.supported-publishing-event=clienteFirmadoEvent;http://mx-allianz-liferay-namespace.com/events/clienteFirmadoEvent",
		"javax.portlet.supported-processing-event=clienteFirmadoEvent;http://mx-allianz-liferay-namespace.com/events/clienteFirmadoEvent",
		"javax.portlet.supported-publishing-event=tramiteClienteHabilitadoEvent;http://mx-allianz-liferay-namespace.com/events/tramiteClienteHabilitadoEvent",
		"javax.portlet.supported-processing-event=tramiteClienteHabilitadoEvent;http://mx-allianz-liferay-namespace.com/events/tramiteClienteHabilitadoEvent",
		"javax.portlet.supported-publishing-event=datosTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/datosTramiteClienteEvent",
		"javax.portlet.supported-processing-event=datosTramiteSolicitanteEvent;http://mx-allianz-liferay-namespace.com/events/datosTramiteSolicitanteEvent",
//		"javax.portlet.supported-publishing-event=datosTramiteComplementoEvent;http://mx-allianz-liferay-namespace.com/events/datosTramiteComplementoEvent",
		
	},
	service = Portlet.class
)

public class DatosTramiteSolicitanteSenderPortlet extends MVCPortlet {

	public static final String LLAVE_TRAMITE = "tramite"; 
	public static final String LLAVE_CLIENTE_FIRMADO_EVENT = "clienteTramiteClienteEvent"; 
	public static final String LLAVE_CLIENTE_FIRMADO = "clienteFirmado"; 
	public static final String LLAVE_TRAMITE_CLIENTE_HABILITADO = "tramiteClienteHabilitado"; 
	public static final String CMD_BACK_PAGE = "cmd_back_page";
	public static final String CMD_NEXT_PAGE = "cmd_next_page";
//	public static final String LLAVE_DATOS_TRAMITE_CLIENTE_EVENT = "datosTramiteClienteEvent";
	public static final String LLAVE_DATOS_TRAMITE_SOLICITANTE_EVENT = "datosTramiteSolicitanteEvent";
	public static final String LLAVE_DATOS_TRAMITE_SOLICITANTE = "tramiteRecSolicitante";
	public static final String CONSULTA_EXITOSA_CLIENTE = "\"estatus\":\"true\""; 

	@Override
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		_log.debug("DatosTramiteSolicitanteSenderPortlet -> processAction ");
		String currentURL = PortalUtil.getCurrentURL(actionRequest);
		_log.info("DatosClienteTramiteSenderPortlet -> procesando tramite -> currentURL = " + currentURL);
		
		
		Gson gson = new Gson();
		TramiteDto tramite = buildClienteTramite(actionRequest);
		
		_log.info("cmd:" + ParamUtil.getString(actionRequest, Constants.CMD));
		
		if (_log.isDebugEnabled()){
			_log.debug("Tramite en solicitanteTramtie: " + tramite);
		}
		
		if ( CMD_NEXT_PAGE.equals(ParamUtil.getString(actionRequest, Constants.CMD))){
		
			QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent", "solicitanteTramiteClienteEvent");
		    actionResponse.setEvent(qName,gson.toJson(tramite));
		    
		    
		    if(tramite.isTramiteClienteHabilitado()){
		    	ClienteTramiteDTO cliente = gson.fromJson(buildClienteFirmado(actionRequest.getPortletSession()), ClienteTramiteDTO.class);
		    	if ( cliente != null && cliente.getCliente() != null){
		    		_log.info("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Las cosas van bien:" + cliente);
		    		QName qNameCliente = new QName("http://mx-allianz-liferay-namespace.com/events/clienteFirmadoEvent", "clienteFirmadoEvent");
		    		actionResponse.setEvent(qNameCliente, buildClienteFirmado(actionRequest.getPortletSession()));
		    	} else {
		    		_log.info("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Algo no iba bien:" + (cliente != null ? cliente : "El cliente era nulo"));
		    	}
			}
		    
		    String redirectURL = Arrays.stream(redirectPair())
					  .map(uri -> uri.split(":"))
					  .filter(pairURI -> currentURL.contains(pairURI[0]))
					  .map(pairURI -> pairURI[1])
					  .findAny()
					  .orElse("/complemento-tramite");
				
			_log.info("DatosTramiteSenderPortlet -> processAction -> redirectURL = " + redirectURL);
			actionResponse.sendRedirect(redirectURL);
		} else {
			QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/datosTramiteClienteEvent", "datosTramiteClienteEvent");
		    actionResponse.setEvent(qName, gson.toJson(tramite));
			actionResponse.sendRedirect("/group/guest/cliente-tramite");
		}
}
	
	public String buildClienteFirmado(PortletSession session){
		return  Optional.ofNullable(session.getAttribute(LLAVE_CLIENTE_FIRMADO, PortletSession.APPLICATION_SCOPE))
				  .filter(p -> p instanceof String)
				  .map( p -> (String) p).orElse(null);
	}
	
	public TramiteDto buildClienteTramite(ActionRequest actionRequest){
		PortletSession session = actionRequest.getPortletSession();
		final TramiteDto tramite = Optional.ofNullable(session)
				  .map(s -> s.getAttribute("tramite", PortletSession.APPLICATION_SCOPE))
				  .filter(obj -> obj.getClass().equals(TramiteDto.class))
				  .map(obj -> (TramiteDto) obj)
				  .orElseGet(TramiteDto::new);
		
		Arrays.stream(FormularioSolicitanteTramiteCampos.values()).
			forEach(campo -> 
				campo.buildTramite(tramite, campo.getParamFromSession(session))
			);
		
		tramite.setFolderArchivosTramite(String.valueOf(new Date().getTime()));

		return tramite;
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		
		PortletSession session = resourceRequest.getPortletSession();
		Arrays.stream(FormularioSolicitanteTramiteCampos.values())
			  .forEach(campo -> 
				  campo.uploadToSession(session, campo.getParamFromRequest(resourceRequest))
			   );
		
		super.serveResource(resourceRequest, resourceResponse);
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		_log.info("DatosTramiteSolicitanteSenderPortlet -> render -> inicio");
		PortletSession session = renderRequest.getPortletSession();	
		Gson gson = new Gson();

		TramiteDto tramite = getParamFromSession(LLAVE_TRAMITE, TramiteDto.class, null, session);
    	String cliente = getParamFromSession(LLAVE_CLIENTE_FIRMADO, String.class, null, session);
    	Boolean clienteTramiteHabilitado = getParamFromSession(LLAVE_TRAMITE_CLIENTE_HABILITADO, Boolean.class, null, session);

		_log.info("DatosTramiteSolicitanteSenderPortlet -> render -> tramite = " + tramite);
		_log.info("DatosTramiteSolicitanteSenderPortlet -> render -> clienteFirmado = " + cliente);
		_log.info("DatosTramiteSolicitanteSenderPortlet -> render -> clienteTramiteHabilitado = " + clienteTramiteHabilitado);
		
		if ( clienteTramiteHabilitado != null && clienteTramiteHabilitado && tramite != null && tramite.getClienteFirmado() == null ){
			if (cliente != null && !cliente.isEmpty() && cliente.length() > 10){
				_log.info("*-*-*-*-*-*-*-*-Quien sabe porque no venia el cliente, se recupera en guardado en memoria");
				try{
					tramite.setClienteFirmado(gson.fromJson(cliente, ClienteTramiteDTO.class));
				}catch (Exception e) {
					_log.info("Tronó al tratar de recuperar el cliente:" + e.getMessage());
				}
			} else {
				recuperaDatosCliente(renderRequest,tramite);
			}
		}
		renderRequest.setAttribute(LLAVE_TRAMITE, gson.toJson(tramite));
		renderRequest.setAttribute(LLAVE_CLIENTE_FIRMADO, cliente);
		renderRequest.setAttribute(LLAVE_TRAMITE_CLIENTE_HABILITADO, clienteTramiteHabilitado);

		super.render(renderRequest, renderResponse);
	}
	
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		Gson gson = new Gson();
		Optional<Event> event = Optional.ofNullable(request)
				 						.map(EventRequest::getEvent);
		PortletSession session = request.getPortletSession();
		
		if(_log.isDebugEnabled()) {
			_log.info("DatosTramiteSolicitanteSenderPortlet -> processEvent solicitante");
		}
		try {
			
			uploadEventStringParamToSession("clienteTramiteClienteEvent",LLAVE_TRAMITE, TramiteDto.class, session, event.get());
			uploadEventParamToSession("clienteFirmadoEvent",LLAVE_CLIENTE_FIRMADO, String.class, session, event.get());
			uploadEventParamToSession("tramiteClienteHabilitadoEvent",LLAVE_TRAMITE_CLIENTE_HABILITADO, Boolean.class, session, event.get());
			
		} catch (Exception e) {
			e.printStackTrace();
			if (_log.isErrorEnabled()) {
				_log.error(e.getStackTrace());
			}
		}
		
		if (event.map(Event::getName)
				.filter(LLAVE_DATOS_TRAMITE_SOLICITANTE_EVENT::equals)
				.isPresent()) {
			event.map(Event::getValue)
			.filter(val -> (val instanceof String))
			.map(json -> gson.fromJson((String) json, TramiteDto.class))
			.ifPresent(obj -> {
				if(_log.isErrorEnabled()) {
					_log.info("DatosTramiteSolicitanteSenderPortlet -> processEvent -> datosTramiteSolicitanteEvent,  tramite = " + obj);
				}
				session.setAttribute(LLAVE_DATOS_TRAMITE_SOLICITANTE, obj, PortletSession.APPLICATION_SCOPE);
			});
		}
	}
	
	private  <T> void uploadEventParamToSession(String nombreEvento, String llaveParametroSesion, Class<T> tipo, PortletSession session, Event event){
		//Gson gson = new Gson();
		Optional<Event> eventO = Optional.ofNullable(event);
		if (eventO.map(Event::getName)
				 .filter(nombreEvento::equals)
				 .isPresent()) {
			eventO.map(Event::getValue)
				 .filter(val -> val.getClass().equals(tipo))
				 .map(json -> (T)json)
				 .ifPresent(json -> {
					_log.info("DatosTramiteSolicitanteSenderPortlet -> uploadEventParamToSession -> " + llaveParametroSesion + " =  " + json); 
					session.setAttribute(llaveParametroSesion, json, PortletSession.APPLICATION_SCOPE);
				 });
		}
	}
	
	private  <T> void uploadEventStringParamToSession(String nombreEvento, String llaveParametroSesion, Class<T> tipo, PortletSession session, Event event){
		Gson gson = new Gson();
		Optional<Event> eventO = Optional.ofNullable(event);
		if (eventO.map(Event::getName)
				 .filter(nombreEvento::equals)
				 .isPresent()) {
			eventO.map(Event::getValue)
				 .filter(val -> val instanceof String)
				 .map(json -> (String)json)
				 .map(json -> gson.fromJson(json, tipo))
				 .ifPresent(json -> {
					_log.info("DatosClienteTramiteSenderPortlet -> processEvent -> " + llaveParametroSesion + " =  " + json); 
					session.setAttribute(llaveParametroSesion, json, PortletSession.APPLICATION_SCOPE);
				 });
		}
	}
	
	private <T> T getParamFromSession(String llaveParametro, Class<T> tipo, Supplier<T> constructor, PortletSession session){
		return Optional.ofNullable(session)
				  .map(s -> s.getAttribute(llaveParametro, PortletSession.APPLICATION_SCOPE))
				  .filter(obj -> obj.getClass().equals(tipo))
				  .map(obj -> (T) obj)
				  .orElse(constructor != null ? constructor.get(): null);
	}
	

	private String[] redirectPair(){
		return _configuration.redirectPair();
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				TramiteSolicitantePortletConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info("Configured Solicitante Tramite (datostramitesendersolicitante v1.0.1) : \n{ " +
					"redirect pair : " + _configuration.redirectPair() +
					" }");
		}
	}
	
	private void recuperaDatosCliente(RenderRequest request,TramiteDto tramite){
		PortletSession session = request.getPortletSession();			
		Gson gson = new Gson();
		_log.info("allianzService = " + allianzService);
		if (allianzService != null) {
			_log.info("El servicio de Allianz se seteo correctamente");
			
			Arrays.stream(Optional.ofNullable(request.getCookies())
								  			.orElseGet(() -> new Cookie[0]))
							.filter(cookie -> _configuration.allianzClientToken().equals(cookie.getName()))
							.map(Cookie::getValue)
							.peek(clienteToken -> _log.info("El token con id cliente es: " + clienteToken))
							.findAny()
							.ifPresent(cookie -> {
								Optional.ofNullable(allianzService.obtenerDatosTestingZone(cookie))
								.map(clienteBuscado -> {
									_log.info("DatosTramiteSolicitanteSenderPortlet -> recuperaDatosCliente -> clienteBuscado = " + clienteBuscado);
									return clienteBuscado;
								})
								.filter(c -> c.contains(CONSULTA_EXITOSA_CLIENTE))
								.map(json -> gson.fromJson(json, ClienteTramiteDTO.class))
								.ifPresent(cliente -> {
									session.setAttribute(LLAVE_CLIENTE_FIRMADO, 
											gson.toJson(cliente.getCliente()), PortletSession.APPLICATION_SCOPE);
									tramite.setClienteFirmado(cliente);
									tramite.fillClient();
								});
							});

		} else {
			_log.info("Reiniciar modulo mx.com.allianz.module.service.facade ");
			_log.info("$ telnet localhost 11311 ");
			_log.info("g! lb | grep mx.com.allianz.module.service.facade ");
			_log.info("g! stop PID ");
			_log.info("g! start PID ");
			_log.info("g! disconnect ");
			_log.info("YES ");
		}
	}
	
	@Reference
	protected void setAllianzService(AllianzService allianzService) {
		this.allianzService = allianzService;
	}

	protected void unsetAllianzService(AllianzService allianzService) {
		this.allianzService = null;
	}

	private AllianzService allianzService;
	
	private TramiteSolicitantePortletConfiguration _configuration ;
	private static Log _log = LogFactoryUtil.getLog(DatosTramiteSolicitanteSenderPortlet.class);

	

}