package mx.com.allianz.tramte.solicitante.sender.portlet.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
		category = "Productivity",
		scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
	)
	@Meta.OCD(
		localization = "content/Language",
		name = "Configuracion Tramite Solicitante",
		id = "mx.com.allianz.tramte.solicitante.sender.portlet.configuration.TramiteSolicitantePortletConfiguration"
	)
public interface TramiteSolicitantePortletConfiguration {
	
	@Meta.AD(
			deflt = "solicitante-tramite:/web/guest/complemento-tramite,solicitantep-tramite:/group/guest/complementop-tramite", 
			description = "Mapeo de posible ruta con redireccionamiento",
			required = false
		)
	public String[] redirectPair();
	
	@Meta.AD(
			deflt = "azctToken", 
			description = "Token con llave de cliente",
			required = false
		)
	public String allianzClientToken();
	
}

