<%@ include file="/init.jsp" %>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mx.com.allianz.commons.dto.tramite.TramiteDto"%>
<%@page import="mx.com.allianz.commons.dto.cliente.ClienteTramiteDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.ClienteDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.ContactoDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.TelefonoDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.CorreoDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.DomicilioDTO"%>

<%@page import="mx.com.allianz.commons.catalogos.dto.ProductoDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.ProductoClienteDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.ContratanteDTO"%>
<%@page import="mx.com.allianz.commons.dto.cliente.AseguradoDTO"%>

<%@ page import="com.liferay.portal.kernel.util.Constants" %>

<%@ page import="com.google.gson.Gson" %>

<% 
	Boolean tramiteClienteHabilitado = false;
	Object object3 = request.getAttribute("tramiteClienteHabilitado");
	if (object3 != null && object3 instanceof Boolean){
		tramiteClienteHabilitado = (Boolean) object3;
	}
	
	String jsonTramite = "";
	Object object2 = request.getAttribute("tramite");
	if (object2 != null && object2 instanceof String) {
		jsonTramite = (String) object2;
	}

%>


<portlet:actionURL var="tramiteSolicitanteActionURL" name="processAction">
</portlet:actionURL>
<portlet:resourceURL var="actualizaTramiteSolicitante" />


<aui:form name="form-solicitante" action="<%=tramiteSolicitanteActionURL.toString()%>">
    	<aui:input name="<%= Constants.CMD %>" type="hidden" value="cmd_next_page"  />
    	<aui:input name="version-datostramitesendersolicitante" type="hidden" value="1.0.1"  />
    	<aui:button-row>
        	<aui:button name="saveButton" class="btn pull-right" type="button" value="datostramitesendersolicitante_DatosTramiteSolicitanteSender.send" id="submitBtn" onclick="validateSolicitanteTramiteForm('cmd_next_page');"/>
    		<aui:button class="btn pull-right" name="cancelButton" type="button" value="datostramitesendersolicitante_DatosTramiteSolicitanteSender.clean" onclick="validateSolicitanteTramiteForm('cmd_back_page');" />
    	</aui:button-row>
</aui:form>



<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("datostramitesendersolicitante v1.0.1");
console.log("Change made by HACM 12-12-2019"):

var formsValidSectionClienteTramite = [];

var tramiteClienteHabilitadoJS = <%=tramiteClienteHabilitado%>;
console.log(tramiteClienteHabilitadoJS);



var jsonTramiteJS = <%=jsonTramite%>;
console.log(jsonTramiteJS);
if( jsonTramiteJS && (jsonTramiteJS.nombreSolicitante || jsonTramiteJS.telefonoCelular || jsonTramiteJS.tipoDeReclamacion) ){
	Liferay.fire('cargaDatosPersonalesDeCliente', {nombre:jsonTramiteJS.nombreSolicitante, 
		apellidoPaterno: jsonTramiteJS.apellidoPaternoSolicitante,apellidoMaterno: jsonTramiteJS.apellidoMaternoSolicitante});
	
	Liferay.fire('cargaDatosContactoDeQueja', jsonTramiteJS);
	Liferay.fire('cargaTipoReclamacion',jsonTramiteJS);
} else if(jsonTramiteJS && jsonTramiteJS.clienteFirmado && jsonTramiteJS.clienteFirmado.cliente){
	console.log("jsonTramiteJS -> clienteFirmado");
	var clienteJS = jsonTramiteJS.clienteFirmado.cliente;
	if ( clienteJS && !clienteJS.nombre && clienteJS.productos)
		clienteJS.nombre = clienteJS.productos[0].contratante.nombre;
	Liferay.fire('cargaDatosContactoDeCliente', clienteJS);
	Liferay.fire('cargaDatosPersonalesDeCliente', clienteJS);
	Liferay.fire('cargaTipoReclamacion',clienteJS);

}

Liferay.provide(window, 'validateSolicitanteTramiteForm', function(accion) {
	console.log("validateSolicitanteTramiteForm" + accion);
	$("#<portlet:namespace/><%= Constants.CMD %>").val(accion);
	formsValidSectionClienteTramite = [];
	Liferay.fire('validaDatosPersonales', {} );
	Liferay.fire('validaDatosContacto', {} );
	Liferay.fire('validaDatosReclamacion', {} );
	console.log("fire end validateSolicitanteTramiteForm");
});

Liferay.on('validaDatosPersonalesRespuesta', validaSeccionTramite);
Liferay.on('validaDatosContactoRespuesta', validaSeccionTramite);
Liferay.on('validaDatosReclamacionRespuesta', validaSeccionTramite);

function validaSeccionTramite(respuestaSeccion) { 
	console.log("desde la respesta la seccion es" + respuestaSeccion.valido);
// 	if (respuestaSeccion.valido) {
		formsValidSectionClienteTramite.push(respuestaSeccion.valido);
		callServeResourceTramite(respuestaSeccion);
		//sendSubmit();
// 	}
}

function sendSubmitTramite() {
	console.log("submit formsValidSectionClienteTramite.length" + formsValidSectionClienteTramite.length);
// 	if($("#<portlet:namespace/><%= Constants.CMD %>").val() == 'cmd_back_page' || (formsValidSectionClienteTramite.length % 3) === 0) {
	if(formsValidSectionClienteTramite.length % 3 === 0) {
		console.log("submit");
		if($("#<portlet:namespace/><%= Constants.CMD %>").val() == 'cmd_next_page'){
			var todosVerdaderos = true;
			for(llave in formsValidSectionClienteTramite){
				todosVerdaderos = todosVerdaderos && formsValidSectionClienteTramite[llave];
				console.log(formsValidSectionClienteTramite[llave]);
				if(!todosVerdaderos) return;
				
			}
		}
		//new AUI().use(function(A) {
			//console.log("submit send" + A);
			//TODO checar con baldor si va 
			var formaSolicitante = A.one('#<portlet:namespace/>form-solicitante');
			console.log("submit formaSolicitante" +formaSolicitante);
			formaSolicitante.submit();
		//});
	}
}

function callServeResourceTramite(seccion) {
	
	 A.io.request (
			 '<%=actualizaTramiteSolicitante.toString()%>', {
	         data: {
	        	nombreSolicitante: seccion.nombre,
	     		apellidoPaternoSolicitante: seccion.apellidoPaterno,
	     		apellidoMaternoSolicitante: seccion.apellidoMaterno,
	     		telefonoParticular: seccion.telefonoParticular,
	     		telefonoCelular: seccion.telefonoCelular,
         		email: seccion.email,
         		tipoReclamacion: seccion.tipoReclamacion,
         		tipoReclamacionDescripcion: seccion.tipoReclamacionDescripcion,
         		numeroSiniestro: seccion.numeroSiniestro,
         		tipoSiniestro: seccion.tipoSiniestro,
         		observaciones: seccion.observaciones
	         },
	         dataType: 'json',
	         on: {
	                 failure: function() {
	                     console.log("Ajax failed! There was some error at the server");
	                 },
	                 success: function(event, id, obj) {
	             		sendSubmitTramite();
	                 }
             }
         }); 
	
}

</aui:script>