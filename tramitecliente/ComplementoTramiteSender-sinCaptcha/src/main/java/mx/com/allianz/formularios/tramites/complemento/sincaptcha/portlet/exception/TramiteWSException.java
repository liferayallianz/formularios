package mx.com.allianz.formularios.tramites.complemento.sincaptcha.portlet.exception;

public class TramiteWSException extends Exception {

	private static final long serialVersionUID = 8794371220217226163L;

	public TramiteWSException() {
		super();
	}

	public TramiteWSException(String message) {
		super(message);
	}

	public TramiteWSException(Throwable cause) {
        super(cause);
    }
}