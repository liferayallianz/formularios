package mx.com.allianz.formularios.tramites.complemento.sincaptcha.portlet;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceException;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.client.puc.tramites.api.PucTramitesServiceClient;
import mx.com.allianz.client.puc.tramites.api.exception.PucTramitesServiceClientException;
import mx.com.allianz.commons.constants.FileUtil;
import mx.com.allianz.commons.dto.puc.PucTramiteRespuestaRestDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.formularios.tramites.complemento.sincaptcha.portlet.configuration.ComplementoTramiteSinCaptchaPortletConfiguration;
import mx.com.allianz.formularios.tramites.complemento.sincaptcha.portlet.exception.TramiteWSException;

@Component(
	configurationPid="mx.com.allianz.formularios.tramites.complemento.sincaptcha.portlet.configuration.ComplementoTramiteSinCaptchaPortletConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        // fin parametros para llamado de eventos de portlet
		"javax.portlet.display-name=Tramite Complemento Sender Sin Captcha",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest",
		//Se agrega nombre de mensaje y cola del portlet para recibir
		"javax.portlet.supported-publishing-event=inconsistenciaTramiteEvent;http://mx-allianz-liferay-namespace.com/events/inconsistenciaTramiteEvent",
		"javax.portlet.supported-publishing-event=solicitanteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent",
		"javax.portlet.supported-processing-event=solicitanteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent",
		"javax.portlet.supported-publishing-event=datosTramiteSolicitanteEvent;http://mx-allianz-liferay-namespace.com/events/datosTramiteSolicitanteEvent",
	},
	service = Portlet.class
)
public class ComplementoTramiteSenderSinCaptchaPortlet extends MVCPortlet {
	
	public static final String CMD_BACK_PAGE = "cmd_back_page";
	public static final String CMD_NEXT_PAGE = "cmd_next_page";
	public static final String LLAVE_DATOS_TRAMITE_COMPLEMENTO_EVENT = "datosTramiteComplementoEvent";
	public static final String LLAVE_DATOS_TRAMITE_COMPLEMENTO = "tramiteRecComplemento";
	
	@Override
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		_log.info("Estoy en processAction de CompelmentoTramiteSenderPortlet");
		
		
		PortletSession session = actionRequest.getPortletSession();
		if (!invalidRequest(session)) {
			session.setAttribute("invalidRequest", new Date().getTime(), PortletSession.APPLICATION_SCOPE);
			Gson gson = new Gson();
			TramiteDto tramite = buildTramite(actionRequest);
			_log.debug("TramiteDto:" + tramite);
			
			_log.debug("ids_docs:" + session.getAttribute("ids_documentos", PortletSession.APPLICATION_SCOPE));
			
			_log.info("cmd:" + ParamUtil.getString(actionRequest, Constants.CMD));
			if ( CMD_NEXT_PAGE.equals(ParamUtil.getString(actionRequest, Constants.CMD))){
			
				String redirect = enviarTramite(tramite, actionRequest, actionResponse);
				
				QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent", "solicitanteTramiteClienteEvent");
			    actionResponse.setEvent(qName, gson.toJson(tramite));
				actionResponse.sendRedirect(redirect);
			} else {
				QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/datosTramiteSolicitanteEvent", "datosTramiteSolicitanteEvent");
			    actionResponse.setEvent(qName, gson.toJson(tramite));
				actionResponse.sendRedirect("/web/guest/solicitante-tramite");
			}
		}
	}
	
	private boolean invalidRequest(PortletSession session){
		return Optional.ofNullable(session.getAttribute("invalidRequest", PortletSession.APPLICATION_SCOPE))
					   .map(param -> (Long) param)
					   .filter(lastTime -> 
					   		(lastTime + _configuration.waitRequest()) > new Date().getTime())
					   .isPresent();
	}
	
	private String enviarTramite(TramiteDto tramite, PortletRequest request, ActionResponse response) {
		Integer folioPUC = 0;
		Gson gson = new Gson();
		
		String currentURL = PortalUtil.getCurrentURL(request);
		_log.info("ComplementoTramiteSenderSinCaptchaPortlet -> enviarTramite -> currentURL = " + currentURL);
		
		String uris[] = Arrays.stream(redirectPair())
				  .map(uri -> uri.split(":"))
				  .filter(pairURI -> currentURL.contains(pairURI[0]))
				  .findAny()
				  .orElse(new String[]{"complemento-tramite","/web/guest/confirmacion-tramite","/web/guest/general-tramite"});
			
		String redirect = uris[1];
		
		if (camposObligatoriosInvalidos(tramite)){
			redirect = uris[2];
			QName qName = new QName("http://mx-allianz-liferay-namespace.com/events/inconsistenciaTramiteEvent", "inconsistenciaTramiteEvent");
			response.setEvent(qName, gson.toJson(tramite));
			_log.error("Error en validacion de campos obligatorios");
			_log.error("Error en validacion de campos obligatorios, tramite = " + tramite);
			_log.error("\ntramite.getTipoTramite() = " + tramite.getTipoTramite() +
					  ", tramite.getTipoTramiteGMM() = " + tramite.getTipoTramiteGMM() + 
					  ", tramite.getEstado() = " + tramite.getEstado() + 
					  ", tramite.getMunicipio() = " + tramite.getMunicipio() + 
					  ", tramite.getSucursal() = " + tramite.getSucursal() + 
					  ", tramite.getNombreAfectado() = " + tramite.getNombreAfectado() + 
					  ", tramite.getApellidoPaternoAfectado() = " + tramite.getApellidoPaternoAfectado() + 
					  ", tramite.getNombreContratante() = " + tramite.getNombreContratante() + 
					  ", tramite.getApellidoPaternoContratante() = " + tramite.getApellidoPaternoContratante() + 
					  ", tramite.getNombreSolicitante() = " + tramite.getNombreSolicitante() + 
					  ", tramite.getApellidoPaternoSolicitante() = " + tramite.getApellidoPaternoSolicitante() + 
					  ", tramite.getTelefonoParticular() = " + tramite.getTelefonoParticular() + 
					  ", tramite.getEmail() = " + tramite.getEmail() +
					  ", tramite.getGrupoPago() = " + tramite.getGrupoPago() +
					  ", tramite.getLugarDeEnvio() = " + tramite.getLugarDeEnvio());
		} else {
			try {
				
				folioPUC = Optional.ofNullable(generarOTTramite(tramite))
							.map(PucTramiteRespuestaRestDTO::getNumeroFolio)
							.filter(folio -> folio.matches("\\d+"))
							.map(Integer::parseInt)
							.orElse(0);
				
				tramite.setFolioPUC(folioPUC);
				
				new Thread(() -> 
					FileUtil.getInstance().deleteFolder(_configuration.rootPath() + File.separator + tramite.getFolderArchivosTramite())
				).start();
				
				cleanSession(request);
			} catch ( TramiteWSException e) {
				_log.error("Tramite -> catch e " + e.getMessage());
				_log.error("Tramite -> catch e " + Arrays.stream(e.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.joining("\n")));

				SessionErrors.add(request, e.getClass(), e);

				String[] currentPair = Arrays.stream(redirectPair())
						  .map(uri -> uri.split(":"))
						  .peek(rurl -> System.out.println("redirectURL pair = [" + rurl[0] + ", " + rurl[1] + ", " + rurl[2] +"]" ))
						  .filter(pairURI -> currentURL.contains(pairURI[0]))
						  .findAny()
						  .orElse(redirectPair()[0].split(":"));
				
				String[] paths = currentPair[1].split("/");
				redirect = (paths.length > 2 ? Arrays.stream(Arrays.copyOfRange(paths, 0, paths.length-1)).collect(Collectors.joining("/")) : "" ) 
						+ (currentPair[2].contains("/") ? currentPair[2] : ("/"+currentPair[2]));			
			}
		}
		return redirect;
	}
	
	private boolean camposObligatoriosInvalidos(TramiteDto tramite) {
		return Arrays.asList(tramite.getTipoTramite(), tramite.getTipoTramiteGMM(), 
					tramite.getEstado(), tramite.getMunicipio(), tramite.getSucursal(), 
					tramite.getNombreAfectado(), tramite.getApellidoPaternoAfectado(), 
					tramite.getNombreContratante(), tramite.getApellidoPaternoContratante(), 
					tramite.getNombreSolicitante(), tramite.getApellidoPaternoSolicitante(), 
					tramite.getTelefonoParticular(), tramite.getEmail()
				)
				.stream()
				.peek(System.out::print)
				.anyMatch(p -> p == null || p.trim().isEmpty());
	}
	
	
	private PucTramiteRespuestaRestDTO generarOTTramite(TramiteDto tramite) throws TramiteWSException {
		PucTramiteRespuestaRestDTO pucRespuesta = null;
		try {
			pucRespuesta = 
					pucTramitesServiceClient.generarOtTramites(tramite);
		} catch ( PucTramitesServiceClientException | WebServiceException e) {
			_log.info("Error enviando tramite a puc ");
			e.printStackTrace();
			throw new TramiteWSException("Hubo un error al enviar su tramite a un sistema remoto, por favor contacte al administrador.");
		}
		return pucRespuesta;
	}
	
	private void cleanSession(PortletRequest request){
		Arrays.stream(FormularioTramiteCamposPrev.values())
		  .forEach(campo ->  
		  	request.getPortletSession().removeAttribute(campo.name(), PortletSession.APPLICATION_SCOPE)
		  );
		Arrays.stream(FormularioClienteTramiteCamposPrev.values())
		  .forEach(campo ->  
		  	request.getPortletSession().removeAttribute(campo.name(), PortletSession.APPLICATION_SCOPE)
		  );
		Arrays.stream(FormularioComplementoTramiteCampos.values())
		  .forEach(campo ->  
		  	request.getPortletSession().removeAttribute(campo.name(), PortletSession.APPLICATION_SCOPE)
		  );
		Arrays.stream(FormularioComplementoTramiteCampos.values())
		.forEach(campo ->  
		request.getPortletSession().removeAttribute(campo.name(), PortletSession.APPLICATION_SCOPE)
				);
	}
	
	public TramiteDto buildTramite(ActionRequest actionRequest){
		PortletSession session = actionRequest.getPortletSession();
		final TramiteDto tramite = Optional.ofNullable(session)
				  .map(s -> s.getAttribute("tramite", PortletSession.APPLICATION_SCOPE))
				  .filter(obj -> obj.getClass().equals(TramiteDto.class))
				  .map(obj -> (TramiteDto) obj)
				  .orElseGet(TramiteDto::new);
		
		Arrays.stream(FormularioComplementoTramiteCampos.values())
			  .forEach(campo -> 
				  campo.buildTramite(tramite, campo.getParamFromSession(session))
			  );
		if (tramite.getTipoDeReclamacion() == null || "".equals(tramite.getTipoDeReclamacion())){
			tramite.setTipoDeReclamacionDescripcion("");
		}
		
		String path = _configuration.rootPath() + File.separator + tramite.getFolderArchivosTramite();
		tramite.setDocumentos(FileUtil.getInstance().getFiles(path));

		return tramite;
	}
	

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		_log.debug("CompelmentoTramiteSenderPortlet -> serveResource ");
		
		PortletSession session = resourceRequest.getPortletSession();
		Arrays.stream(FormularioComplementoTramiteCampos.values())
			  .forEach(campo ->  
				  campo.uploadToSession(session, campo.getParamFromRequest(resourceRequest))
			  );
		
		super.serveResource(resourceRequest, resourceResponse);
	}

	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		Gson gson = new Gson();
		PortletSession session = request.getPortletSession();
		Optional<Event> event = Optional.ofNullable(request)
				.map(EventRequest::getEvent);
		_log.debug("processEvent de complemento");
		if(_log.isDebugEnabled()) {
			_log.debug("DatosTramiteSolicitanteSenderPortlet -> processEvent solicitante");
		}
		try {
			if (event.map(Event::getName)
					 .filter("solicitanteTramiteClienteEvent"::equals)
					 .isPresent()) {
				event.map(Event::getValue)
					 .filter(val -> (val instanceof String))
					 .map(json -> gson.fromJson((String) json, TramiteDto.class))
					 .ifPresent(obj -> {
						session.setAttribute("tramite", obj, PortletSession.APPLICATION_SCOPE);
					 });
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (_log.isErrorEnabled()) {
				_log.error(e.getStackTrace());
			}
		}
		
		if (event.map(Event::getName)
				.filter(LLAVE_DATOS_TRAMITE_COMPLEMENTO_EVENT::equals)
				.isPresent()) {
			event.map(Event::getValue)
			.filter(val -> (val instanceof String))
			.map(json -> gson.fromJson((String) json, TramiteDto.class))
			.ifPresent(obj -> {
				if(_log.isErrorEnabled()) {
					_log.info("DatosTramiteSolicitanteSenderPortlet -> processEvent -> datosTramiteSolicitanteEvent,  tramite = " + obj);
				}
				session.setAttribute(LLAVE_DATOS_TRAMITE_COMPLEMENTO, obj, PortletSession.APPLICATION_SCOPE);
			});
		}
		new Thread(() -> 
			FileUtil.getInstance().cleanOldFiles(_configuration.rootPath(), _configuration.waitClean())
		).start();
	}
	
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	protected void setPucTramitesServiceClient(PucTramitesServiceClient pucTramitesServiceClient) {
		System.out.println("pucTramitesServiceClient = " + pucTramitesServiceClient);
		this.pucTramitesServiceClient = pucTramitesServiceClient;
	}

	protected void unsetPucTramitesServiceClient(PucTramitesServiceClient pucTramitesServiceClient) {
		this.pucTramitesServiceClient = null;
	}

	private String[] redirectPair(){
		return _configuration.redirectPair();
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				ComplementoTramiteSinCaptchaPortletConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info("Configured Complemento Tramite sin captcha : { " +
					"rooth Path : " + _configuration.rootPath() + ", " +
					"wait clean : " + _configuration.waitClean() + ", " +
					"wait request : " + _configuration.waitRequest() + ", " +
					"redirect pair : " + _configuration.redirectPair() 
					+ " }");
		}
	}
	
	private volatile ComplementoTramiteSinCaptchaPortletConfiguration _configuration;
	private PucTramitesServiceClient pucTramitesServiceClient;
	private static Log _log = LogFactoryUtil.getLog(ComplementoTramiteSenderSinCaptchaPortlet.class);

}

enum FormularioTramiteCamposPrev {
	tipoTramite,tipoTramiteGMM,estado,municipio,sucursa
}
enum FormularioClienteTramiteCamposPrev {
	producto,numeroPoliza,nombreAfectado,nombreContratante,apellidoPaternoAfectado,
	apellidoMaternoAfectado,apellidoPaternoContratante,apellidoMaternoContratante
}

enum FormularioSolicitanteTramiteCamposPrev {
	nombreSolicitante,apellidoPaternoSolicitante,apellidoMaternoSolicitante,
	telefonoParticular,telefonoCelular,email,tipoReclamacion,tipoReclamacionDescripcion,
	numeroSiniestro,tipoSiniestro,observaciones
}


