package mx.com.allianz.formularios.tramites.complemento.sincaptcha.portlet;

import static javax.portlet.PortletSession.APPLICATION_SCOPE;
import static mx.com.allianz.commons.constants.FormularioConstants.EMPTY;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.function.Predicate;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;

import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.dto.tramite.TramiteDto;

public enum FormularioComplementoTramiteCampos {
	ESTADO_RESPUESTA("estado"), SUBGRUPO_PAGO("subgrupo"), 
	SUBGRUPO_PAGO_DESCRIPCION("subgrupoDescripcion"),
	GRUPO_PAGO("grupo"), GRUPO_PAGO_DESCRIPCION("grupoDescripcion"), 
	MONEDA("moneda"), MONTO_RECLAMADO("montoReclamado"), 
	CANTIDAD_FACTURA("cantidadFacturaRecibos"), 
	FILE_FOLDER("fileFolder"),CAPCHA("capcha") ;
	
	FormularioComplementoTramiteCampos(String valor){
		 try {
	            Field fieldName = getClass().getSuperclass().getDeclaredField("name");
	            fieldName.setAccessible(true);
	            fieldName.set(this, valor);
	            fieldName.setAccessible(false);
	        } catch (Exception e) {}
	}

	public void buildTramite(TramiteDto tramite, String parametro){
		switch (this) {
		case ESTADO_RESPUESTA:
			tramite.setEstadoDeRespuesta(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case SUBGRUPO_PAGO:
			tramite.setLugarDeEnvio(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case SUBGRUPO_PAGO_DESCRIPCION:
			tramite.setSubgrupoDescripcion(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case GRUPO_PAGO_DESCRIPCION:
			tramite.setGrupoPagoDescripcion(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case GRUPO_PAGO:
			tramite.setGrupoPago(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case MONEDA:
			tramite.setMoneda(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case MONTO_RECLAMADO:
			tramite.setMontoReclamado(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case CANTIDAD_FACTURA:
			tramite.setContidadFactura(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case FILE_FOLDER:
			
			break;
		case CAPCHA:
			tramite.setCapcha(Optional.ofNullable(parametro).orElse(EMPTY));
		default:
			break;
		}
	}
	
	public void uploadToSession(PortletSession session, String parametro){
		Optional.ofNullable(parametro).filter(NOT_EMPTY).ifPresent(value -> session.setAttribute(this.name(), value, APPLICATION_SCOPE));
	}
	
	public String getParamFromRequest(ResourceRequest resourceRequest){
		return Optional.ofNullable(ParamUtil.getString(resourceRequest, this.name())).map(this::tryString).orElse(EMPTY);
	}
	
	public String getParamFromSession(PortletSession session){
		return Optional.ofNullable(session.getAttribute(this.name(), APPLICATION_SCOPE)).map(this::tryString).orElse(EMPTY);
	}
	
	public Integer tryParseInt(String text) {
			return (text.matches("\\d+")) ? Integer.parseInt(text) : null;
	}
	
	public String tryString(Object param) {
		 return (param instanceof String) ? (String) param : null;
	}
	
	
	public static final Predicate<String> NOT_EMPTY = (String it) -> !it.isEmpty();

}
