<%@ include file="/init.jsp" %>
<style>
	.title-portlet-form-divider {
		position: relative;
		margin-top: 20px; 
	}
	.well-portlet-form-divider {
		position: relative;
		margin-top: 20px; 
	}
</style>
</br>
<h3>
	<b><liferay-ui:message key="oficinassucursales_OficinasSucursales.caption"/></b>
</h3>
<p>
	Tambi&eacute;n puedes contactarnos directamente en nuestras oficinas y sucursales.
</p>
</br>

<div class="row">
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Of. Regional - Distrito Federal (matriz)</b></p> 
    		<p class="text-left">Blvd. Manuel �vila Camacho # 164 </br>
				Col. Lomas de Barrilaco</br>
				C.P. 11010, M�xico, D.F.</br>
				Tel: 5201-3000</br>
				Lada sin costo: 01800-1111-200</br>
				Atenci�n siniestros: 01800-1111-400</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</br>
				</p> 
  	</div>
  </div>
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Of. Regional - Guadalajara</b></p> 
    		<p class="text-left">Edificio Corporativo Punto Sao Paulo</br>
				Av. Am�ricas #1545, piso 5 - local A</br>
				Col. Jardines Providencia</br>
				C.P. 44630, Guadalajara, Jalisco</br>
				Tel: (33) 3121-0182</br>
				Fax: (33)3121-0183</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</p> 
  	</div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Of. Regional - Monterrey</b></p> 
    		<p class="text-left">Calzada del Valle #110 Oriente, piso 1 </br>
				Col. del Valle</br>
				C.P. 66220, Garza Garc�a, Nuevo Le�n</br>
				Tel: (81) 8335-7500</br>
				Fax: (81) 8335-3736</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</p> 
  	</div>
  </div>
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Of. Regional - M�rida</b></p> 
    		<p class="text-left">Calle 60 # 282-A, local 3</br>
				Av. Am�ricas #1545, piso 5 - local A</br>
				Col. Fracc. Buenavista </br>
				C.P. 97127, M�rida, Yucat�n</br>
				Tel: (999) 9272-260</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</br>
				</p> 
  	</div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Of. Regional - Quer�taro</b></p> 
    		<p class="text-left">Manuel G�mez Mor�n #3870, piso 8 </br>
				Oficinas 1005 y 1006, Col. Centro Sur</br>
				C.P. 76090, Quer�taro, Qro.</br>
				Tel: (442) 2130-142</br>
				Fax: (442) 2130-742 ext. 112</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</p> 
  	</div>
  </div>
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Sucursal - Sur Distrito Federal</b></p> 
    		<p class="text-left">Insurgentes #1898, planta baja - local B</br>
				Col. Florida </br>
				C.P. 01030, M�xico, D.F.</br>
				Tels: 5661-0663 / 5661-4647</br>
				5661-0857</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</p> 
  	</div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Sucursal - Puebla</b></p> 
    		<p class="text-left">Av. Ju�rez #2925, piso 7 - int. 702</br>
				Col. La Paz</br>
				C.P. 72160, Puebla, Puebla</br>
				Tels: (222) 2266-632 / (222) 2492-597</br>
				(222) 2303-523</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</p> 
  	</div>
  </div>
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Sucursal - Le�n</b></p> 
    		<p class="text-left">Blvd. Campestre #2509, int. 403</br>
				Col. Campestres el Refugio </br>
				C.P. 37156, Le�n, Guanajuato</br>
				Tels: (477) 7180-999 / (477) 2931-000</br>
				(477) 2931-001</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</p> 
  	</div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Sucursal - San Luis Potos�</b></p> 
    		<p class="text-left">Av. Real de Lomas #1025, local 6 </br>
				Col.  Lomas Cuarta Secci�n</br>
				C.P. 78216, San Luis Potos�, SLP</br>
				Tels: (444) 8250-575 / (444) 8251-580</br>
				(444) 8256-350</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</p> 
  	</div>
  </div>
  <div class="col-sm-6">
  	<div class="well well-lg">
    		<p class="text-left"><b>Sucursal - Villahermosa</b></p> 
    		<p class="text-left">Plaza Via 2, local 13</br>
				Calle Via 2 esquina Samarkanda #114 </br>
				Col. Complejo Urban�stico Tabasco 2000</br>
				C.P. 86030, Villahermosa, Tabasco</br>
				Tels: (993) 3162-571 / (993) 3163-126</br>
				<a href="mailto:seguros@allianz.com.mx?subject=Contacto%20p%C3%A1gina%20web%20Allianz%20%3A%20Distrito%20Federal">seguros@allianz.com.mx</a>
				</p> 
  	</div>
  </div>
</div>
</br>
<p>
	<b>Allianz M�xico S.A. Compa��a de Seguros      RFC: AMS950419 EG4</b>
</p>