package mx.com.allianz.tramites.clientes.datosafectado.portlet;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(
	immediate = true,
	property = {
			"com.liferay.portlet.display-category=category.sample",
			"com.liferay.portlet.instanceable=true",
			// Se agregan parametros para recibir de eventos de portlet
	        "com.liferay.portlet.requires-namespaced-parameters=false",
	        "com.liferay.portlet.ajaxable=true",
	        // fin Se agregan parametros para recibir de eventos de portlet
			// Se agregan parametros para compartir atributos de request en la session
	        "com.liferay.portlet.private-request-attributes=false",
	        "com.liferay.portlet.private-session-attributes=false",
	        "com.liferay.portlet.remoteable=true",
			"com.liferay.portlet.preferences-unique-per-layout=true",
			// Fin parametros para compartir atributos de request en la session
			"requires-namespaced-parameters=false",
			"javax.portlet.display-name=Commons Datos Afectado",
			"javax.portlet.init-param.template-path=/",
			"javax.portlet.init-param.view-template=/view.jsp",
			"javax.portlet.resource-bundle=content.Language",
			// Se agregan parametro para identificar el portlet en otros contextos
			"javax.portlet.supported-processing-event=tramiteClienteHabilitadoEvent;http://mx-allianz-liferay-namespace.com/events/tramiteClienteHabilitadoEvent",
			// Fin parametro para identificar el portlet en otros contextos
	        "javax.portlet.security-role-ref=power-user,user,guest",
	        "javax.portlet.supports.mime-type=text/html"
	},
	service = Portlet.class
)
public class DatoAfectadoPortlet extends MVCPortlet {

	public static final String LLAVE_CLIENTE_HABILITADO = "tramiteClienteHabilitado"; 
	public static final String LLAVE_CLIENTE_HABILITADO_EVENT = "tramiteClienteHabilitadoEvent"; 
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try {
			PortletSession session = renderRequest.getPortletSession();
	        
	        Boolean tramiteClienteHabilitado = Optional.ofNullable(session)
					.map(s -> 
						  s.getAttribute(LLAVE_CLIENTE_HABILITADO, PortletSession.APPLICATION_SCOPE))
					.filter(o -> (o instanceof Boolean))
					.map(o -> (Boolean)o)
					.orElseGet(() -> false);
	        renderRequest.setAttribute(LLAVE_CLIENTE_HABILITADO, tramiteClienteHabilitado);
		} catch (Exception e) {
			e.printStackTrace();
		}
        super.render(renderRequest, renderResponse);
	}
	
	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {   
		if(_log.isDebugEnabled()) {
			_log.info("DatosClienteTramiteSenderPortlet -> processEvent cliente");
		}
		try {
//			Gson gson = new Gson();
			PortletSession session = request.getPortletSession();
			Optional<Event> event = Optional.ofNullable(request)
											.map(EventRequest::getEvent);
			if (event.map(Event::getName)
					 .filter(LLAVE_CLIENTE_HABILITADO_EVENT::equals)
					 .isPresent()) {
				event.map(Event::getValue)
//					 .filter(val -> (val instanceof String))
//					 .map(json -> gson.fromJson((String) json, TramiteDto.class))
//					 .map(TramiteDto::isTramiteClienteHabilitado)
					 .ifPresent(obj -> {
						 _log.debug("DatoAfectadoPortlet- > processEvent -> " + LLAVE_CLIENTE_HABILITADO + " = " + obj);
						session.setAttribute(LLAVE_CLIENTE_HABILITADO, obj, PortletSession.APPLICATION_SCOPE);
					 });
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (_log.isErrorEnabled()) {
				_log.error(e.getStackTrace());
			}
		}
	}
	
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet datoafectado - 1.0.1, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(DatoAfectadoPortlet.class);

}