<%@ include file="/init.jsp" %>

<% 
		Boolean tramiteClienteHabilitado = false;
        Object object = request.getAttribute("tramiteClienteHabilitado");
		if (object != null && object instanceof Boolean){
			tramiteClienteHabilitado = (Boolean) object;
		}
%>

</br>
<p class="formularioTituloEncabezado">Datos del afectado</p>
<div id="divDatosAfectadoPoliza" style="display:<%= tramiteClienteHabilitado ? "none" : "block" %>" >
<aui:form action="#" name="fm_datos_afectado">
		<aui:input name="version-datoafectado" type="hidden" value="1.0.1"  />
		<aui:input cssClass="formularioCampoTexto" name="nombre" type="text" value="" label="" placeholder="datos.afectado.nombre" >
			<aui:validator name="required" errorMessage="El nombre del afectado es requerido"/>
			<aui:validator name="rangeLength" errorMessage="La longitud del nombre del afectado debe ser menor a 75 caracteres">[1,75]</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" name="apellidoPaterno" type="text" value="" label="" placeholder="datos.afectado.apellido.paterno" >
			<aui:validator name="required" errorMessage="El apellido paterno del afectado es requerido" />
			<aui:validator name="rangeLength" errorMessage="La longitud del apellido paterno del afectado debe ser menor a 50 caracteres">[1,50]</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" name="apellidoMaterno" type="text" value="" label="" placeholder="datos.afectado.apellido.materno" >
			<aui:validator name="rangeLength" errorMessage="La longitud del apellido materno del afectado debe ser menor a 50 caracteres">[1,50]</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
</aui:form>
</div>
<aui:script use="aui-base,liferay-form,aui-form-validator">
console.log("afectado original v1.0.3");

var tramiteClienteHabilitadoJS = <%= tramiteClienteHabilitado %>;
console.log("afectado original tramiteClienteHabilitadoJS = " + tramiteClienteHabilitadoJS);
//Validation Fields 
var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_afectado').formValidator;

// add valid form
Liferay.on('validaDatosAfectado', function(event){
	console.log("Iniciando validacion en dato afectado");
	if (!tramiteClienteHabilitadoJS) {
		var eventoRespuesta = {};
		formValidator.validate();
		eventoRespuesta.valido = !formValidator.hasErrors();
		if(eventoRespuesta.valido){
			eventoRespuesta.idAfectado = obtenerIdAsegurado(),
			eventoRespuesta.nombreAfectado = A.one('#<portlet:namespace />nombre').get('value');
			eventoRespuesta.apellidoPaternoAfectado = A.one('#<portlet:namespace />apellidoPaterno').get('value');
			eventoRespuesta.apellidoMaternoAfectado = A.one('#<portlet:namespace />apellidoMaterno').get('value');
		}
		console.log("Final del evento datos afectado: " + eventoRespuesta);
		Liferay.fire('validaDatosAfectadoRespuesta', eventoRespuesta );
	}
});
</aui:script>


<script type="text/javascript"> 
Liferay.on('presentarAfectadoPoliza', function(event) {
	console.log("presentarAfectadoPoliza ");
	console.log( event);

	document.getElementById('divDatosAfectadoPoliza').style.display = 'block';
	var afectado = event.afectado; 
	console.log(  afectado);

	if (afectado) 
		new AUI().use(function(A) {
			A.one('#<portlet:namespace />nombre').set('value',afectado.nombre);
			A.one('#<portlet:namespace />apellidoPaterno').set('value',afectado.apellidoPaterno);
			A.one('#<portlet:namespace />apellidoMaterno').set('value',afectado.apellidoMaterno);
			//A.one('#<portlet:namespace />hidden_contratante_poliza_cliente').set('value',afectado.idafectado);
	   		
		});
                
});
</script>
