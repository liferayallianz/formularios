<%@ include file="/init.jsp" %>

<liferay-theme:defineObjects />
<aui:input name="Paises" type="text" label="combotest_ComboTest.pais"/>
 
<aui:script use="autocomplete-list,aui-base,aui-io-request-deprecated,autocomplete-filters,autocomplete-highlighters,datasource,datasource-get,datatable-datasource">
 
// Please note that this contact portlet service is a service builder generated JSON web service.
// We pass the groupId as a query param because our service expects it. Liferay has a nice javascript method for finding the group id.
var contactSearchDS = new A.DataSource.IO({source: '/api/jsonws/country/get-countries?p_auth=xQ9sMs9M'});
 
var contactSearchQueryTemplate = function(query) {
        // Here's an example on how to pass additional parameters to the query for you service
        // In our case we are fetching only the first 20 items and specify the ordering by name
	var output = '&name='+query.trim()+'&sort=name&dir=asc&start=0&end=20';
	return output;
}
 
var contactSearchLocator = function (response) {
	var responseData = A.JSON.parse(response[0].responseText);
// For debugging you can do: console.debug(responseData);
	//console.debug(responseData);
    return responseData;
};
 
var contactSearchFormatter = function (query, results) {
	return A.Array.map(results, function (result) {
// For debugging: console.debug(result.raw);
		//console.debug(result.raw.name);
		return '<strong>'+result.raw.nameCurrentValue+'</strong><br/>';
	});
};
 
var contactSearchTextLocator = function (result) {
// This is what we place in the input once the user selects an item from the autocomplete list.
// In our case we want to put contact full name in there.
	console.debug(result.row);
	/* console.debug(result.raw);
 */
	return result.row.nameCurrentValue;
};
 
var contactSearchInput = new A.AutoCompleteList({
	allowBrowserAutocomplete: 'false',
	resultHighlighter: 'phraseMatch',
	activateFirstItem: 'false',
	inputNode: '#<portlet:namespace/>Paises',
	render: 'true',
	source: contactSearchDS,
	//requestTemplate: contactSearchQueryTemplate,
	resultListLocator: contactSearchLocator,
    resultFormatter: contactSearchFormatter,
	resultTextLocator: 'nameCurrentValue'//contactSearchTextLocator		
});
 
</aui:script>	