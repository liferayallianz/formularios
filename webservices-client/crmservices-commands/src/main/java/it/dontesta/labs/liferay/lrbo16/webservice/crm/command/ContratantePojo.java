package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;

public class ContratantePojo {
	private String nombre;

    private String apellidoMaterno;

    private String apellidoPaterno;

    private String idContratante;

    public String getNombre ()
    {
        return nombre;
    }

    public void setNombre (String nombre)
    {
        this.nombre = nombre;
    }

    public String getApellidoMaterno ()
    {
        return apellidoMaterno;
    }

    public void setApellidoMaterno (String apellidoMaterno)
    {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno ()
    {
        return apellidoPaterno;
    }

    public void setApellidoPaterno (String apellidoPaterno)
    {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getIdContratante ()
    {
        return idContratante;
    }

    public void setIdContratante (String idContratante)
    {
        this.idContratante = idContratante;
    }

    @Override
    public String toString()
    {
        return "Contratante [nombre = "+nombre+", apellidoMaterno = "+apellidoMaterno+", apellidoPaterno = "+apellidoPaterno+", idContratante = "+idContratante+"]";
    }
}
