package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;

public class DomicilioPojo {
	 private String codigoEstado;

	    private String codigoMunicipio;

	    private String descripcionPais;

	    private String descripcionMunicipio;

	    private String codigoPais;

	    private String descripcionEstado;

	    public String getCodigoEstado ()
	    {
	        return codigoEstado;
	    }

	    public void setCodigoEstado (String codigoEstado)
	    {
	        this.codigoEstado = codigoEstado;
	    }

	    public String getCodigoMunicipio ()
	    {
	        return codigoMunicipio;
	    }

	    public void setCodigoMunicipio (String codigoMunicipio)
	    {
	        this.codigoMunicipio = codigoMunicipio;
	    }

	    public String getDescripcionPais ()
	    {
	        return descripcionPais;
	    }

	    public void setDescripcionPais (String descripcionPais)
	    {
	        this.descripcionPais = descripcionPais;
	    }

	    public String getDescripcionMunicipio ()
	    {
	        return descripcionMunicipio;
	    }

	    public void setDescripcionMunicipio (String descripcionMunicipio)
	    {
	        this.descripcionMunicipio = descripcionMunicipio;
	    }

	    public String getCodigoPais ()
	    {
	        return codigoPais;
	    }

	    public void setCodigoPais (String codigoPais)
	    {
	        this.codigoPais = codigoPais;
	    }

	    public String getDescripcionEstado ()
	    {
	        return descripcionEstado;
	    }

	    public void setDescripcionEstado (String descripcionEstado)
	    {
	        this.descripcionEstado = descripcionEstado;
	    }

	    @Override
	    public String toString()
	    {
	        return "Domicilio [codigoEstado = "+codigoEstado+", codigoMunicipio = "+codigoMunicipio+", descripcionPais = "+descripcionPais+", descripcionMunicipio = "+descripcionMunicipio+", codigoPais = "+codigoPais+", descripcionEstado = "+descripcionEstado+"]";
	    }
}
