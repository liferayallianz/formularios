package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;

public class CorreosPojo {
	private String correoElectronico;

    public String getCorreoElectronico ()
    {
        return correoElectronico;
    }

    public void setCorreoElectronico (String correoElectronico)
    {
        this.correoElectronico = correoElectronico;
    }

    @Override
    public String toString()
    {
        return "Correos [correoElectronico = "+correoElectronico+"]";
    }
}
