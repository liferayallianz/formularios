package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;

public class AseguradosPojo {
	private String nombre;

    private String idAsegurado;

    public String getNombre ()
    {
        return nombre;
    }

    public void setNombre (String nombre)
    {
        this.nombre = nombre;
    }

    public String getIdAsegurado ()
    {
        return idAsegurado;
    }

    public void setIdAsegurado (String idAsegurado)
    {
        this.idAsegurado = idAsegurado;
    }

    @Override
    public String toString()
    {
        return "Asegurados [nombre = "+nombre+", idAsegurado = "+idAsegurado+"]";
    }
}
