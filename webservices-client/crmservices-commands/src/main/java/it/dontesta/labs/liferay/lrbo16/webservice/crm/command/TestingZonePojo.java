package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;

public class TestingZonePojo {

	private String error;
	private ClientePojo cliente;
	private String estatus;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ClientePojo getCliente() {
		return cliente;
	}

	public void setCliente(ClientePojo cliente) {
		this.cliente = cliente;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Override
	public String toString() {
		return "TestingZone [error=" + error + ", cliente=" + cliente.toString() + ", estatus=" + estatus + "]";
	}
}
