package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;

public class ClientePojo
{
    private ContactoPojo contacto;

    private DomicilioPojo domicilio;

    private String idCliente;

    private String apellidoMaterno;

    private String apellidoPaterno;

    private ProductosPojo[] productos;

    public ContactoPojo getContacto ()
    {
        return contacto;
    }

    public void setContacto (ContactoPojo contacto)
    {
        this.contacto = contacto;
    }

    public DomicilioPojo getDomicilio ()
    {
        return domicilio;
    }

    public void setDomicilio (DomicilioPojo domicilio)
    {
        this.domicilio = domicilio;
    }

    public String getIdCliente ()
    {
        return idCliente;
    }

    public void setIdCliente (String idCliente)
    {
        this.idCliente = idCliente;
    }

    public String getApellidoMaterno ()
    {
        return apellidoMaterno;
    }

    public void setApellidoMaterno (String apellidoMaterno)
    {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno ()
    {
        return apellidoPaterno;
    }

    public void setApellidoPaterno (String apellidoPaterno)
    {
        this.apellidoPaterno = apellidoPaterno;
    }

    public ProductosPojo[] getProductos ()
    {
        return productos;
    }

    public void setProductos (ProductosPojo[] productos)
    {
        this.productos = productos;
    }
    
	private String productosToString() {
		String productosSalida = "";
		for (int i = 0; i < productos.length; i++) {
			productosSalida += productos[i].toString() + " ";
		}
		return productosSalida.toString();
	}

    @Override
    public String toString()
    {
        return "Cliente [contacto = "+contacto.toString()+", domicilio = "+domicilio.toString()+", idCliente = "+idCliente+", apellidoMaterno = "+apellidoMaterno+", apellidoPaterno = "+apellidoPaterno+", productos = "+productosToString()+"]";
    }
}
			
			