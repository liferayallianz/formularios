package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;

public class ContactoPojo {
	private TelefonosPojo[] telefonos;

	private CorreosPojo[] correos;

	public TelefonosPojo[] getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(TelefonosPojo[] telefonos) {
		this.telefonos = telefonos;
	}

	public CorreosPojo[] getCorreos() {
		return correos;
	}

	public void setCorreos(CorreosPojo[] correos) {
		this.correos = correos;
	}

	private String telefonosToString() {
		String telefonosSalida = "";
		for (int i = 0; i < telefonos.length; i++) {
			telefonosSalida += telefonos[i].toString() + " ";
		}
		return telefonosSalida.toString();
	}

	private String correosToString() {
		String correosSalida = "";
		for (int i = 0; i < correos.length; i++) {
			correosSalida += telefonos[i].toString() + " ";
		}
		return correosSalida.toString();
	}

	@Override
	public String toString() {
		return "Contacto [telefonos = " + telefonosToString() + ", correos = " + correosToString() + "]";
	}

}
