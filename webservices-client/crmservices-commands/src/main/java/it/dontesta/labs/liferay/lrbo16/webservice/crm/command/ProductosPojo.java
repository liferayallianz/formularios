package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;

public class ProductosPojo {
	private String descripcionProducto;

	private ContratantePojo contratante;

	private AseguradosPojo[][] asegurados;

	private String codigoProducto;

	private String estatusProducto;

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public ContratantePojo getContratante() {
		return contratante;
	}

	public void setContratante(ContratantePojo contratante) {
		this.contratante = contratante;
	}

	public AseguradosPojo[][] getAsegurados() {
		return asegurados;
	}

	public void setAsegurados(AseguradosPojo[][] asegurados) {
		this.asegurados = asegurados;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getEstatusProducto() {
		return estatusProducto;
	}

	public void setEstatusProducto(String estatusProducto) {
		this.estatusProducto = estatusProducto;
	}

	public String aseguradosToString() {
		String aseguradosSalida = "";
		for (int i = 0; i < asegurados.length; i++) {
			for (int j = 0; j < asegurados[i].length; j++) {
				aseguradosSalida = asegurados[i][j].toString() + " ";
			}
		}
		return aseguradosSalida;
	}

	@Override
	public String toString() {
		return "Productos [descripcionProducto = " + descripcionProducto + ", contratante = " + contratante.toString()
				+ ", asegurados = " + aseguradosToString() + ", codigoProducto = " + codigoProducto
				+ ", estatusProducto = " + estatusProducto + "]";
	}
}
