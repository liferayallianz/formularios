package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;

public class TelefonosPojo {
	 private String lada;

	    private String telefono;

	    public String getLada ()
	    {
	        return lada;
	    }

	    public void setLada (String lada)
	    {
	        this.lada = lada;
	    }

	    public String getTelefono ()
	    {
	        return telefono;
	    }

	    public void setTelefono (String telefono)
	    {
	        this.telefono = telefono;
	    }

	    @Override
	    public String toString()
	    {
	        return "Telefonos [lada = "+lada+", telefono = "+telefono+"]";
	    }
}
