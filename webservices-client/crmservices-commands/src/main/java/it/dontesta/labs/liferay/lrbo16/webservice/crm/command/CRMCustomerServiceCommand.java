package it.dontesta.labs.liferay.lrbo16.webservice.crm.command;


import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.WebServiceException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.dontesta.labs.liferay.lrbo16.webservice.crm.api.CRMService;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.exception.CRMServiceException;
import mx.com.allianz.commons.dto.login.LoginDTO;
import mx.com.allianz.commons.dto.rh.SacRequestDTO;
import mx.com.allianz.service.login.api.LoginSOAPServiceApi;
import mx.com.allianz.service.sap.api.SapService;
import mx.com.allianz.service.sap.exception.SapServiceException;

@Component(
	immediate = true,
	property = {
		"osgi.command.function=createOTQueja",
		"osgi.command.function=createDefaultCustomer",
		"osgi.command.function=getCustomer",
		"osgi.command.function=getCustomers",
		"osgi.command.function=executeSap",
		"osgi.command.function=consultarLogin",
		"osgi.command.scope=lfboug"
		
	},
	service = Object.class
)
public class CRMCustomerServiceCommand {

	public void createOTQueja() {
		if(_log.isInfoEnabled()){
			_log.info("Entrando a createOTQueja");
		}

		List<String> campos = new ArrayList<>();
		Integer idPucQuejas = 0;
		
		campos.add("");
		campos.add("Queja");
		campos.add("2016");
		campos.add("");
		campos.add("Cliente");
		campos.add(" ");
		campos.add("GMMC");
		campos.add("");
		campos.add("756");
		campos.add("prueba");
		campos.add("prueba");
		campos.add(" ");
		campos.add("1324567890");
		campos.add(" ");
		campos.add("aldo.lobato@allianz.com.mx");
		campos.add("Distrito Federal");
		campos.add("Probelmas en la atención de un siniestro");
		campos.add("No estoy de acuerdo");
		campos.add("1");
		campos.add("1");
		campos.add("publico");
		campos.add(" ");
		campos.add("KIJYMC");
		campos.add("-1138425460");
		
		if(_log.isInfoEnabled()){
			_log.info("antes de invocar al service OT Quejas");
		}
		try {
			idPucQuejas = crmService.generarOTQuejas("admin.quejas", "Siniestros",
							"Queja-1-GMMC", campos, "Prueba queja", null);
		} catch (CRMServiceException | WebServiceException e) {
			if (_log.isErrorEnabled()) {
				_log.error(e);
			}
			System.out.println(e);
		}
		
		if(_log.isInfoEnabled()){
			_log.info("QuejaOT with id: " + idPucQuejas + " created");
		}
	}
	
	public void consultarLogin(){
		LoginDTO respuesta = new LoginDTO("oscarg.pruebas","pruebaadsol");
		
		System.out.println("Login consulta: --------" + loginService);
		System.out.println(loginService.login(respuesta));
	}
	
	
	public void executeSap(){
		try {
			sapService.execute(null);
		} catch (SapServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	public void createDefaultCustomer() {
		if(_log.isInfoEnabled()){
			_log.info("antes de invocar al service OT Quejas");
		}
		int idPuc = 0;
		
		try {
			idPuc = crmService.createOTDefault();
		} catch (CRMServiceException | WebServiceException e) {
			if (_log.isErrorEnabled()) {
				_log.error(e);
			}
			System.out.println(e);
		}
		
		if(_log.isInfoEnabled()){
			_log.info("idPuc devuelto por service OT Quejas es : " + idPuc);
		}
	}
	
	
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	protected void setCRMService(CRMService crmService) {
		this.crmService = crmService;
	}

	protected void unsetCRMService(CRMService crmService) {
		this.crmService = null;
	}
	
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	protected void setSapService(SapService sapService) {
		this.sapService = sapService;
	}

	protected void unsetSapService(SapService sapService) {
		this.sapService = null;
	}
	
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	protected void setLoginService(LoginSOAPServiceApi loginService) {
		this.loginService = loginService;
	}

	protected void unsetLoginService(LoginSOAPServiceApi loginService) {
		this.loginService = null;
	}

	private CRMService crmService;
	private SapService sapService;
	private LoginSOAPServiceApi loginService;
	private static Log _log = LogFactoryUtil.getLog(CRMCustomerServiceCommand.class);

}