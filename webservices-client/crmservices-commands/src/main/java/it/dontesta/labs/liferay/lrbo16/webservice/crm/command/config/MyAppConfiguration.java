package it.dontesta.labs.liferay.lrbo16.webservice.crm.command.config;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.foo.bar.MyAppConfiguration")
public interface MyAppConfiguration {

    @Meta.AD(
        deflt = "blue",
        required = false
    )
    public String favoriteColor();

    @Meta.AD(
       deflt = "red|green|blue",
       required = false
    )
    public String[] validColors();

    @Meta.AD(required = false)
    public int itemsPerPage();

}