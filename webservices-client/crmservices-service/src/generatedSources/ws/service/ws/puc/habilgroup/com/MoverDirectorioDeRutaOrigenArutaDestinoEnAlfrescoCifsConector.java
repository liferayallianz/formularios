
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para moverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="moverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rutaOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rutaDestino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "moverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector", propOrder = {
    "rutaOrigen",
    "rutaDestino"
})
public class MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector {

    protected String rutaOrigen;
    protected String rutaDestino;

    /**
     * Obtiene el valor de la propiedad rutaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaOrigen() {
        return rutaOrigen;
    }

    /**
     * Define el valor de la propiedad rutaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaOrigen(String value) {
        this.rutaOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad rutaDestino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaDestino() {
        return rutaDestino;
    }

    /**
     * Define el valor de la propiedad rutaDestino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaDestino(String value) {
        this.rutaDestino = value;
    }

}
