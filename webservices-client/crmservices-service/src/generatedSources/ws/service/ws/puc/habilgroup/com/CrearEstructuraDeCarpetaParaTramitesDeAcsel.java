
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para crearEstructuraDeCarpetaParaTramitesDeAcsel complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="crearEstructuraDeCarpetaParaTramitesDeAcsel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rutaCarpetaTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "crearEstructuraDeCarpetaParaTramitesDeAcsel", propOrder = {
    "rutaCarpetaTramite",
    "tipoTramite"
})
public class CrearEstructuraDeCarpetaParaTramitesDeAcsel {

    protected String rutaCarpetaTramite;
    protected String tipoTramite;

    /**
     * Obtiene el valor de la propiedad rutaCarpetaTramite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaCarpetaTramite() {
        return rutaCarpetaTramite;
    }

    /**
     * Define el valor de la propiedad rutaCarpetaTramite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaCarpetaTramite(String value) {
        this.rutaCarpetaTramite = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoTramite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTramite() {
        return tipoTramite;
    }

    /**
     * Define el valor de la propiedad tipoTramite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTramite(String value) {
        this.tipoTramite = value;
    }

}
