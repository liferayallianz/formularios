
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para generarOTAcsel complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="generarOTAcsel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cveFlujo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreClasificador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="campos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="observaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generarOTAcsel", propOrder = {
    "username",
    "cveFlujo",
    "nombreClasificador",
    "campos",
    "observaciones"
})
public class GenerarOTAcsel {

    protected String username;
    protected String cveFlujo;
    protected String nombreClasificador;
    protected String campos;
    protected String observaciones;

    /**
     * Obtiene el valor de la propiedad username.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Define el valor de la propiedad username.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Obtiene el valor de la propiedad cveFlujo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCveFlujo() {
        return cveFlujo;
    }

    /**
     * Define el valor de la propiedad cveFlujo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCveFlujo(String value) {
        this.cveFlujo = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClasificador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClasificador() {
        return nombreClasificador;
    }

    /**
     * Define el valor de la propiedad nombreClasificador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClasificador(String value) {
        this.nombreClasificador = value;
    }

    /**
     * Obtiene el valor de la propiedad campos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampos() {
        return campos;
    }

    /**
     * Define el valor de la propiedad campos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampos(String value) {
        this.campos = value;
    }

    /**
     * Obtiene el valor de la propiedad observaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * Define el valor de la propiedad observaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservaciones(String value) {
        this.observaciones = value;
    }

}
