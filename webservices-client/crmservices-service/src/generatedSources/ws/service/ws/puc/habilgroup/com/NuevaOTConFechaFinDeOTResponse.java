
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para nuevaOTConFechaFinDeOTResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="nuevaOTConFechaFinDeOTResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outOtConFecha" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nuevaOTConFechaFinDeOTResponse", propOrder = {
    "outOtConFecha"
})
public class NuevaOTConFechaFinDeOTResponse {

    protected Integer outOtConFecha;

    /**
     * Obtiene el valor de la propiedad outOtConFecha.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutOtConFecha() {
        return outOtConFecha;
    }

    /**
     * Define el valor de la propiedad outOtConFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutOtConFecha(Integer value) {
        this.outOtConFecha = value;
    }

}
