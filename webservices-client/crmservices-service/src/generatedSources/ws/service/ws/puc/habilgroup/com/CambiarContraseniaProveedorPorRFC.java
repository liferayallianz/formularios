
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para cambiarContraseniaProveedorPorRFC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="cambiarContraseniaProveedorPorRFC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rfcProveedor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contraseniaNueva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cambiarContraseniaProveedorPorRFC", propOrder = {
    "rfcProveedor",
    "contraseniaNueva"
})
public class CambiarContraseniaProveedorPorRFC {

    protected String rfcProveedor;
    protected String contraseniaNueva;

    /**
     * Obtiene el valor de la propiedad rfcProveedor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRfcProveedor() {
        return rfcProveedor;
    }

    /**
     * Define el valor de la propiedad rfcProveedor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRfcProveedor(String value) {
        this.rfcProveedor = value;
    }

    /**
     * Obtiene el valor de la propiedad contraseniaNueva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContraseniaNueva() {
        return contraseniaNueva;
    }

    /**
     * Define el valor de la propiedad contraseniaNueva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContraseniaNueva(String value) {
        this.contraseniaNueva = value;
    }

}
