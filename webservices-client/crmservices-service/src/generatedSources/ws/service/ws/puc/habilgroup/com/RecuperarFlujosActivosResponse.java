
package service.ws.puc.habilgroup.com;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para recuperarFlujosActivosResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="recuperarFlujosActivosResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outFlujosActivos" type="{http://com.habilgroup.puc.ws.service}flujoPuc" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "recuperarFlujosActivosResponse", propOrder = {
    "outFlujosActivos"
})
public class RecuperarFlujosActivosResponse {

    protected List<FlujoPuc> outFlujosActivos;

    /**
     * Gets the value of the outFlujosActivos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the outFlujosActivos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOutFlujosActivos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlujoPuc }
     * 
     * 
     */
    public List<FlujoPuc> getOutFlujosActivos() {
        if (outFlujosActivos == null) {
            outFlujosActivos = new ArrayList<FlujoPuc>();
        }
        return this.outFlujosActivos;
    }

}
