
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para cerrarOTAcsel complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="cerrarOTAcsel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idOT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="textoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="archivoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cerrarOTAcsel", propOrder = {
    "username",
    "idOT",
    "fechaRespuesta",
    "tipoRespuesta",
    "textoRespuesta",
    "archivoRespuesta"
})
public class CerrarOTAcsel {

    protected String username;
    protected String idOT;
    protected String fechaRespuesta;
    protected String tipoRespuesta;
    protected String textoRespuesta;
    protected String archivoRespuesta;

    /**
     * Obtiene el valor de la propiedad username.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Define el valor de la propiedad username.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Obtiene el valor de la propiedad idOT.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOT() {
        return idOT;
    }

    /**
     * Define el valor de la propiedad idOT.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOT(String value) {
        this.idOT = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaRespuesta() {
        return fechaRespuesta;
    }

    /**
     * Define el valor de la propiedad fechaRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRespuesta(String value) {
        this.fechaRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoRespuesta() {
        return tipoRespuesta;
    }

    /**
     * Define el valor de la propiedad tipoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoRespuesta(String value) {
        this.tipoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad textoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextoRespuesta() {
        return textoRespuesta;
    }

    /**
     * Define el valor de la propiedad textoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextoRespuesta(String value) {
        this.textoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad archivoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchivoRespuesta() {
        return archivoRespuesta;
    }

    /**
     * Define el valor de la propiedad archivoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchivoRespuesta(String value) {
        this.archivoRespuesta = value;
    }

}
