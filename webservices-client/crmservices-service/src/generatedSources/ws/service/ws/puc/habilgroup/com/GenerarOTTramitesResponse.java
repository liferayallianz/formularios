
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para generarOTTramitesResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="generarOTTramitesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outIdOtTramites" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generarOTTramitesResponse", propOrder = {
    "outIdOtTramites"
})
public class GenerarOTTramitesResponse {

    protected Integer outIdOtTramites;

    /**
     * Obtiene el valor de la propiedad outIdOtTramites.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutIdOtTramites() {
        return outIdOtTramites;
    }

    /**
     * Define el valor de la propiedad outIdOtTramites.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutIdOtTramites(Integer value) {
        this.outIdOtTramites = value;
    }

}
