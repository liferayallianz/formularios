
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ordenDeTrabajoPuc complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ordenDeTrabajoPuc">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="campos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaFinEstimadaTareaActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flujo" type="{http://com.habilgroup.puc.ws.service}flujoPuc" minOccurs="0"/>
 *         &lt;element name="idOt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isCancelada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isCerrada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreContacto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreTareaActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="observaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ordenDeTrabajoPuc", propOrder = {
    "campos",
    "fechaFinEstimadaTareaActual",
    "fechaInicio",
    "flujo",
    "idOt",
    "isCancelada",
    "isCerrada",
    "nombreContacto",
    "nombreTareaActual",
    "observaciones"
})
public class OrdenDeTrabajoPuc {

    protected String campos;
    protected String fechaFinEstimadaTareaActual;
    protected String fechaInicio;
    protected FlujoPuc flujo;
    protected String idOt;
    protected String isCancelada;
    protected String isCerrada;
    protected String nombreContacto;
    protected String nombreTareaActual;
    protected String observaciones;

    /**
     * Obtiene el valor de la propiedad campos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampos() {
        return campos;
    }

    /**
     * Define el valor de la propiedad campos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampos(String value) {
        this.campos = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFinEstimadaTareaActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaFinEstimadaTareaActual() {
        return fechaFinEstimadaTareaActual;
    }

    /**
     * Define el valor de la propiedad fechaFinEstimadaTareaActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaFinEstimadaTareaActual(String value) {
        this.fechaFinEstimadaTareaActual = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Define el valor de la propiedad fechaInicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInicio(String value) {
        this.fechaInicio = value;
    }

    /**
     * Obtiene el valor de la propiedad flujo.
     * 
     * @return
     *     possible object is
     *     {@link FlujoPuc }
     *     
     */
    public FlujoPuc getFlujo() {
        return flujo;
    }

    /**
     * Define el valor de la propiedad flujo.
     * 
     * @param value
     *     allowed object is
     *     {@link FlujoPuc }
     *     
     */
    public void setFlujo(FlujoPuc value) {
        this.flujo = value;
    }

    /**
     * Obtiene el valor de la propiedad idOt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOt() {
        return idOt;
    }

    /**
     * Define el valor de la propiedad idOt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOt(String value) {
        this.idOt = value;
    }

    /**
     * Obtiene el valor de la propiedad isCancelada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCancelada() {
        return isCancelada;
    }

    /**
     * Define el valor de la propiedad isCancelada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCancelada(String value) {
        this.isCancelada = value;
    }

    /**
     * Obtiene el valor de la propiedad isCerrada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCerrada() {
        return isCerrada;
    }

    /**
     * Define el valor de la propiedad isCerrada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCerrada(String value) {
        this.isCerrada = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreContacto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreContacto() {
        return nombreContacto;
    }

    /**
     * Define el valor de la propiedad nombreContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreContacto(String value) {
        this.nombreContacto = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreTareaActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreTareaActual() {
        return nombreTareaActual;
    }

    /**
     * Define el valor de la propiedad nombreTareaActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreTareaActual(String value) {
        this.nombreTareaActual = value;
    }

    /**
     * Obtiene el valor de la propiedad observaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * Define el valor de la propiedad observaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservaciones(String value) {
        this.observaciones = value;
    }

}
