
package service.ws.puc.habilgroup.com;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para generarOTTramites complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="generarOTTramites">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cveFlujo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreClasificador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="campos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="observaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="archivos" type="{http://com.habilgroup.puc.ws.service}archivoPuc" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generarOTTramites", propOrder = {
    "username",
    "cveFlujo",
    "nombreClasificador",
    "campos",
    "observaciones",
    "archivos"
})
public class GenerarOTTramites {

    protected String username;
    protected String cveFlujo;
    protected String nombreClasificador;
    protected String campos;
    protected String observaciones;
    protected List<ArchivoPuc> archivos;

    /**
     * Obtiene el valor de la propiedad username.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Define el valor de la propiedad username.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Obtiene el valor de la propiedad cveFlujo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCveFlujo() {
        return cveFlujo;
    }

    /**
     * Define el valor de la propiedad cveFlujo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCveFlujo(String value) {
        this.cveFlujo = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClasificador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClasificador() {
        return nombreClasificador;
    }

    /**
     * Define el valor de la propiedad nombreClasificador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClasificador(String value) {
        this.nombreClasificador = value;
    }

    /**
     * Obtiene el valor de la propiedad campos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampos() {
        return campos;
    }

    /**
     * Define el valor de la propiedad campos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampos(String value) {
        this.campos = value;
    }

    /**
     * Obtiene el valor de la propiedad observaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * Define el valor de la propiedad observaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservaciones(String value) {
        this.observaciones = value;
    }

    /**
     * Gets the value of the archivos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the archivos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArchivos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArchivoPuc }
     * 
     * 
     */
    public List<ArchivoPuc> getArchivos() {
        if (archivos == null) {
            archivos = new ArrayList<ArchivoPuc>();
        }
        return this.archivos;
    }

}
