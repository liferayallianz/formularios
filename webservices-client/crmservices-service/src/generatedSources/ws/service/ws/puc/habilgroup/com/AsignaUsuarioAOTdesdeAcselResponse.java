
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asignaUsuarioAOTdesdeAcselResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asignaUsuarioAOTdesdeAcselResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outAsignarUsuarioOt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asignaUsuarioAOTdesdeAcselResponse", propOrder = {
    "outAsignarUsuarioOt"
})
public class AsignaUsuarioAOTdesdeAcselResponse {

    protected Integer outAsignarUsuarioOt;

    /**
     * Obtiene el valor de la propiedad outAsignarUsuarioOt.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutAsignarUsuarioOt() {
        return outAsignarUsuarioOt;
    }

    /**
     * Define el valor de la propiedad outAsignarUsuarioOt.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutAsignarUsuarioOt(Integer value) {
        this.outAsignarUsuarioOt = value;
    }

}
