
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para recuperarOrdenDeTrabajoPorIdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="recuperarOrdenDeTrabajoPorIdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outOT" type="{http://com.habilgroup.puc.ws.service}ordenDeTrabajoPuc" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "recuperarOrdenDeTrabajoPorIdResponse", propOrder = {
    "outOT"
})
public class RecuperarOrdenDeTrabajoPorIdResponse {

    protected OrdenDeTrabajoPuc outOT;

    /**
     * Obtiene el valor de la propiedad outOT.
     * 
     * @return
     *     possible object is
     *     {@link OrdenDeTrabajoPuc }
     *     
     */
    public OrdenDeTrabajoPuc getOutOT() {
        return outOT;
    }

    /**
     * Define el valor de la propiedad outOT.
     * 
     * @param value
     *     allowed object is
     *     {@link OrdenDeTrabajoPuc }
     *     
     */
    public void setOutOT(OrdenDeTrabajoPuc value) {
        this.outOT = value;
    }

}
