
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asignaUsuarioAOTdesdeAcsel complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asignaUsuarioAOTdesdeAcsel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idOT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asignaUsuarioAOTdesdeAcsel", propOrder = {
    "idOT",
    "username"
})
public class AsignaUsuarioAOTdesdeAcsel {

    protected String idOT;
    protected String username;

    /**
     * Obtiene el valor de la propiedad idOT.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOT() {
        return idOT;
    }

    /**
     * Define el valor de la propiedad idOT.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOT(String value) {
        this.idOT = value;
    }

    /**
     * Obtiene el valor de la propiedad username.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Define el valor de la propiedad username.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

}
