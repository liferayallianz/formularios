
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para generarOTQuejasConFechaYHoraFinResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="generarOTQuejasConFechaYHoraFinResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outIdOtConFecha" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generarOTQuejasConFechaYHoraFinResponse", propOrder = {
    "outIdOtConFecha"
})
public class GenerarOTQuejasConFechaYHoraFinResponse {

    protected Integer outIdOtConFecha;

    /**
     * Obtiene el valor de la propiedad outIdOtConFecha.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutIdOtConFecha() {
        return outIdOtConFecha;
    }

    /**
     * Define el valor de la propiedad outIdOtConFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutIdOtConFecha(Integer value) {
        this.outIdOtConFecha = value;
    }

}
