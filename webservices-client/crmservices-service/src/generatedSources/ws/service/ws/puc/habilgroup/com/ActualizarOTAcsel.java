
package service.ws.puc.habilgroup.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para actualizarOTAcsel complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="actualizarOTAcsel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idOt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idRecAcsel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actualizarOTAcsel", propOrder = {
    "idOt",
    "idRecAcsel"
})
public class ActualizarOTAcsel {

    protected Integer idOt;
    protected String idRecAcsel;

    /**
     * Obtiene el valor de la propiedad idOt.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdOt() {
        return idOt;
    }

    /**
     * Define el valor de la propiedad idOt.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdOt(Integer value) {
        this.idOt = value;
    }

    /**
     * Obtiene el valor de la propiedad idRecAcsel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRecAcsel() {
        return idRecAcsel;
    }

    /**
     * Define el valor de la propiedad idRecAcsel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRecAcsel(String value) {
        this.idRecAcsel = value;
    }

}
