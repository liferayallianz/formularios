
package service.ws.puc.habilgroup.com;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the service.ws.puc.habilgroup.com package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ActualizarOTAcsel_QNAME = new QName("http://com.habilgroup.puc.ws.service", "actualizarOTAcsel");
    private final static QName _GenerarOTAcsel_QNAME = new QName("http://com.habilgroup.puc.ws.service", "generarOTAcsel");
    private final static QName _NuevaOTConFechaFinDeOTResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "nuevaOTConFechaFinDeOTResponse");
    private final static QName _GuardarMensaje_QNAME = new QName("http://com.habilgroup.puc.ws.service", "guardarMensaje");
    private final static QName _CerrarTareaTramitesResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "cerrarTareaTramitesResponse");
    private final static QName _CrearEstructuraDeCarpetaParaTramitesDeAcselResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "crearEstructuraDeCarpetaParaTramitesDeAcselResponse");
    private final static QName _GenerarOTQuejasConFechaYHoraFin_QNAME = new QName("http://com.habilgroup.puc.ws.service", "generarOTQuejasConFechaYHoraFin");
    private final static QName _MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco_QNAME = new QName("http://com.habilgroup.puc.ws.service", "moverDirectorioDeRutaOrigenArutaDestinoEnAlfresco");
    private final static QName _GuardarMensajeResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "guardarMensajeResponse");
    private final static QName _GenerarOTQuejasConFechaYHoraFinResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "generarOTQuejasConFechaYHoraFinResponse");
    private final static QName _MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "moverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse");
    private final static QName _AsignaUsuarioAOTdesdeAcselResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "asignaUsuarioAOTdesdeAcselResponse");
    private final static QName _AsignaUsuarioAOTdesdeAcsel_QNAME = new QName("http://com.habilgroup.puc.ws.service", "asignaUsuarioAOTdesdeAcsel");
    private final static QName _MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "moverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse");
    private final static QName _NuevaOTResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "nuevaOTResponse");
    private final static QName _CerrarTareaTramites_QNAME = new QName("http://com.habilgroup.puc.ws.service", "cerrarTareaTramites");
    private final static QName _RecuperarFlujosActivosResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "recuperarFlujosActivosResponse");
    private final static QName _CambiarContraseniaProveedorPorRFC_QNAME = new QName("http://com.habilgroup.puc.ws.service", "cambiarContraseniaProveedorPorRFC");
    private final static QName _AsignarUsuarioOT_QNAME = new QName("http://com.habilgroup.puc.ws.service", "asignarUsuarioOT");
    private final static QName _MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector_QNAME = new QName("http://com.habilgroup.puc.ws.service", "moverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector");
    private final static QName _CrearEstructuraDeCarpetaParaTramitesDeAcsel_QNAME = new QName("http://com.habilgroup.puc.ws.service", "crearEstructuraDeCarpetaParaTramitesDeAcsel");
    private final static QName _ActualizarOTAcselResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "actualizarOTAcselResponse");
    private final static QName _GenerarOTAcselResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "generarOTAcselResponse");
    private final static QName _GenerarOTTramitesResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "generarOTTramitesResponse");
    private final static QName _CambiarContraseniaProveedorPorRFCResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "cambiarContraseniaProveedorPorRFCResponse");
    private final static QName _RecuperarOrdenDeTrabajoPorId_QNAME = new QName("http://com.habilgroup.puc.ws.service", "recuperarOrdenDeTrabajoPorId");
    private final static QName _CerrarOTAcselResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "cerrarOTAcselResponse");
    private final static QName _AsignarUsuarioOTResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "asignarUsuarioOTResponse");
    private final static QName _CerrarOTAcsel_QNAME = new QName("http://com.habilgroup.puc.ws.service", "cerrarOTAcsel");
    private final static QName _RecuperarOrdenDeTrabajoPorIdResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "recuperarOrdenDeTrabajoPorIdResponse");
    private final static QName _GenerarOTTramites_QNAME = new QName("http://com.habilgroup.puc.ws.service", "generarOTTramites");
    private final static QName _PucWSException_QNAME = new QName("http://com.habilgroup.puc.ws.service", "PucWSException");
    private final static QName _CerrarTareaResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "cerrarTareaResponse");
    private final static QName _GenerarOTQuejasResponse_QNAME = new QName("http://com.habilgroup.puc.ws.service", "generarOTQuejasResponse");
    private final static QName _GenerarOTQuejas_QNAME = new QName("http://com.habilgroup.puc.ws.service", "generarOTQuejas");
    private final static QName _NuevaOTConFechaFinDeOT_QNAME = new QName("http://com.habilgroup.puc.ws.service", "nuevaOTConFechaFinDeOT");
    private final static QName _CerrarTarea_QNAME = new QName("http://com.habilgroup.puc.ws.service", "cerrarTarea");
    private final static QName _NuevaOT_QNAME = new QName("http://com.habilgroup.puc.ws.service", "nuevaOT");
    private final static QName _RecuperarFlujosActivos_QNAME = new QName("http://com.habilgroup.puc.ws.service", "recuperarFlujosActivos");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: service.ws.puc.habilgroup.com
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RecuperarOrdenDeTrabajoPorId }
     * 
     */
    public RecuperarOrdenDeTrabajoPorId createRecuperarOrdenDeTrabajoPorId() {
        return new RecuperarOrdenDeTrabajoPorId();
    }

    /**
     * Create an instance of {@link CerrarOTAcselResponse }
     * 
     */
    public CerrarOTAcselResponse createCerrarOTAcselResponse() {
        return new CerrarOTAcselResponse();
    }

    /**
     * Create an instance of {@link AsignarUsuarioOTResponse }
     * 
     */
    public AsignarUsuarioOTResponse createAsignarUsuarioOTResponse() {
        return new AsignarUsuarioOTResponse();
    }

    /**
     * Create an instance of {@link CerrarOTAcsel }
     * 
     */
    public CerrarOTAcsel createCerrarOTAcsel() {
        return new CerrarOTAcsel();
    }

    /**
     * Create an instance of {@link AsignarUsuarioOT }
     * 
     */
    public AsignarUsuarioOT createAsignarUsuarioOT() {
        return new AsignarUsuarioOT();
    }

    /**
     * Create an instance of {@link MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector }
     * 
     */
    public MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector createMoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector() {
        return new MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector();
    }

    /**
     * Create an instance of {@link CrearEstructuraDeCarpetaParaTramitesDeAcsel }
     * 
     */
    public CrearEstructuraDeCarpetaParaTramitesDeAcsel createCrearEstructuraDeCarpetaParaTramitesDeAcsel() {
        return new CrearEstructuraDeCarpetaParaTramitesDeAcsel();
    }

    /**
     * Create an instance of {@link ActualizarOTAcselResponse }
     * 
     */
    public ActualizarOTAcselResponse createActualizarOTAcselResponse() {
        return new ActualizarOTAcselResponse();
    }

    /**
     * Create an instance of {@link GenerarOTAcselResponse }
     * 
     */
    public GenerarOTAcselResponse createGenerarOTAcselResponse() {
        return new GenerarOTAcselResponse();
    }

    /**
     * Create an instance of {@link GenerarOTTramitesResponse }
     * 
     */
    public GenerarOTTramitesResponse createGenerarOTTramitesResponse() {
        return new GenerarOTTramitesResponse();
    }

    /**
     * Create an instance of {@link CambiarContraseniaProveedorPorRFCResponse }
     * 
     */
    public CambiarContraseniaProveedorPorRFCResponse createCambiarContraseniaProveedorPorRFCResponse() {
        return new CambiarContraseniaProveedorPorRFCResponse();
    }

    /**
     * Create an instance of {@link CerrarTarea }
     * 
     */
    public CerrarTarea createCerrarTarea() {
        return new CerrarTarea();
    }

    /**
     * Create an instance of {@link NuevaOT }
     * 
     */
    public NuevaOT createNuevaOT() {
        return new NuevaOT();
    }

    /**
     * Create an instance of {@link RecuperarFlujosActivos }
     * 
     */
    public RecuperarFlujosActivos createRecuperarFlujosActivos() {
        return new RecuperarFlujosActivos();
    }

    /**
     * Create an instance of {@link RecuperarOrdenDeTrabajoPorIdResponse }
     * 
     */
    public RecuperarOrdenDeTrabajoPorIdResponse createRecuperarOrdenDeTrabajoPorIdResponse() {
        return new RecuperarOrdenDeTrabajoPorIdResponse();
    }

    /**
     * Create an instance of {@link GenerarOTTramites }
     * 
     */
    public GenerarOTTramites createGenerarOTTramites() {
        return new GenerarOTTramites();
    }

    /**
     * Create an instance of {@link PucWSException }
     * 
     */
    public PucWSException createPucWSException() {
        return new PucWSException();
    }

    /**
     * Create an instance of {@link CerrarTareaResponse }
     * 
     */
    public CerrarTareaResponse createCerrarTareaResponse() {
        return new CerrarTareaResponse();
    }

    /**
     * Create an instance of {@link GenerarOTQuejasResponse }
     * 
     */
    public GenerarOTQuejasResponse createGenerarOTQuejasResponse() {
        return new GenerarOTQuejasResponse();
    }

    /**
     * Create an instance of {@link GenerarOTQuejas }
     * 
     */
    public GenerarOTQuejas createGenerarOTQuejas() {
        return new GenerarOTQuejas();
    }

    /**
     * Create an instance of {@link NuevaOTConFechaFinDeOT }
     * 
     */
    public NuevaOTConFechaFinDeOT createNuevaOTConFechaFinDeOT() {
        return new NuevaOTConFechaFinDeOT();
    }

    /**
     * Create an instance of {@link CrearEstructuraDeCarpetaParaTramitesDeAcselResponse }
     * 
     */
    public CrearEstructuraDeCarpetaParaTramitesDeAcselResponse createCrearEstructuraDeCarpetaParaTramitesDeAcselResponse() {
        return new CrearEstructuraDeCarpetaParaTramitesDeAcselResponse();
    }

    /**
     * Create an instance of {@link GenerarOTQuejasConFechaYHoraFin }
     * 
     */
    public GenerarOTQuejasConFechaYHoraFin createGenerarOTQuejasConFechaYHoraFin() {
        return new GenerarOTQuejasConFechaYHoraFin();
    }

    /**
     * Create an instance of {@link ActualizarOTAcsel }
     * 
     */
    public ActualizarOTAcsel createActualizarOTAcsel() {
        return new ActualizarOTAcsel();
    }

    /**
     * Create an instance of {@link GenerarOTAcsel }
     * 
     */
    public GenerarOTAcsel createGenerarOTAcsel() {
        return new GenerarOTAcsel();
    }

    /**
     * Create an instance of {@link NuevaOTConFechaFinDeOTResponse }
     * 
     */
    public NuevaOTConFechaFinDeOTResponse createNuevaOTConFechaFinDeOTResponse() {
        return new NuevaOTConFechaFinDeOTResponse();
    }

    /**
     * Create an instance of {@link GuardarMensaje }
     * 
     */
    public GuardarMensaje createGuardarMensaje() {
        return new GuardarMensaje();
    }

    /**
     * Create an instance of {@link CerrarTareaTramitesResponse }
     * 
     */
    public CerrarTareaTramitesResponse createCerrarTareaTramitesResponse() {
        return new CerrarTareaTramitesResponse();
    }

    /**
     * Create an instance of {@link CerrarTareaTramites }
     * 
     */
    public CerrarTareaTramites createCerrarTareaTramites() {
        return new CerrarTareaTramites();
    }

    /**
     * Create an instance of {@link RecuperarFlujosActivosResponse }
     * 
     */
    public RecuperarFlujosActivosResponse createRecuperarFlujosActivosResponse() {
        return new RecuperarFlujosActivosResponse();
    }

    /**
     * Create an instance of {@link CambiarContraseniaProveedorPorRFC }
     * 
     */
    public CambiarContraseniaProveedorPorRFC createCambiarContraseniaProveedorPorRFC() {
        return new CambiarContraseniaProveedorPorRFC();
    }

    /**
     * Create an instance of {@link MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco }
     * 
     */
    public MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco createMoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco() {
        return new MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco();
    }

    /**
     * Create an instance of {@link GuardarMensajeResponse }
     * 
     */
    public GuardarMensajeResponse createGuardarMensajeResponse() {
        return new GuardarMensajeResponse();
    }

    /**
     * Create an instance of {@link GenerarOTQuejasConFechaYHoraFinResponse }
     * 
     */
    public GenerarOTQuejasConFechaYHoraFinResponse createGenerarOTQuejasConFechaYHoraFinResponse() {
        return new GenerarOTQuejasConFechaYHoraFinResponse();
    }

    /**
     * Create an instance of {@link MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse }
     * 
     */
    public MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse createMoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse() {
        return new MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse();
    }

    /**
     * Create an instance of {@link AsignaUsuarioAOTdesdeAcselResponse }
     * 
     */
    public AsignaUsuarioAOTdesdeAcselResponse createAsignaUsuarioAOTdesdeAcselResponse() {
        return new AsignaUsuarioAOTdesdeAcselResponse();
    }

    /**
     * Create an instance of {@link AsignaUsuarioAOTdesdeAcsel }
     * 
     */
    public AsignaUsuarioAOTdesdeAcsel createAsignaUsuarioAOTdesdeAcsel() {
        return new AsignaUsuarioAOTdesdeAcsel();
    }

    /**
     * Create an instance of {@link MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse }
     * 
     */
    public MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse createMoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse() {
        return new MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse();
    }

    /**
     * Create an instance of {@link NuevaOTResponse }
     * 
     */
    public NuevaOTResponse createNuevaOTResponse() {
        return new NuevaOTResponse();
    }

    /**
     * Create an instance of {@link OrdenDeTrabajoPuc }
     * 
     */
    public OrdenDeTrabajoPuc createOrdenDeTrabajoPuc() {
        return new OrdenDeTrabajoPuc();
    }

    /**
     * Create an instance of {@link ArchivoPuc }
     * 
     */
    public ArchivoPuc createArchivoPuc() {
        return new ArchivoPuc();
    }

    /**
     * Create an instance of {@link FlujoPuc }
     * 
     */
    public FlujoPuc createFlujoPuc() {
        return new FlujoPuc();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarOTAcsel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "actualizarOTAcsel")
    public JAXBElement<ActualizarOTAcsel> createActualizarOTAcsel(ActualizarOTAcsel value) {
        return new JAXBElement<ActualizarOTAcsel>(_ActualizarOTAcsel_QNAME, ActualizarOTAcsel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarOTAcsel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "generarOTAcsel")
    public JAXBElement<GenerarOTAcsel> createGenerarOTAcsel(GenerarOTAcsel value) {
        return new JAXBElement<GenerarOTAcsel>(_GenerarOTAcsel_QNAME, GenerarOTAcsel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevaOTConFechaFinDeOTResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "nuevaOTConFechaFinDeOTResponse")
    public JAXBElement<NuevaOTConFechaFinDeOTResponse> createNuevaOTConFechaFinDeOTResponse(NuevaOTConFechaFinDeOTResponse value) {
        return new JAXBElement<NuevaOTConFechaFinDeOTResponse>(_NuevaOTConFechaFinDeOTResponse_QNAME, NuevaOTConFechaFinDeOTResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarMensaje }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "guardarMensaje")
    public JAXBElement<GuardarMensaje> createGuardarMensaje(GuardarMensaje value) {
        return new JAXBElement<GuardarMensaje>(_GuardarMensaje_QNAME, GuardarMensaje.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CerrarTareaTramitesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "cerrarTareaTramitesResponse")
    public JAXBElement<CerrarTareaTramitesResponse> createCerrarTareaTramitesResponse(CerrarTareaTramitesResponse value) {
        return new JAXBElement<CerrarTareaTramitesResponse>(_CerrarTareaTramitesResponse_QNAME, CerrarTareaTramitesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearEstructuraDeCarpetaParaTramitesDeAcselResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "crearEstructuraDeCarpetaParaTramitesDeAcselResponse")
    public JAXBElement<CrearEstructuraDeCarpetaParaTramitesDeAcselResponse> createCrearEstructuraDeCarpetaParaTramitesDeAcselResponse(CrearEstructuraDeCarpetaParaTramitesDeAcselResponse value) {
        return new JAXBElement<CrearEstructuraDeCarpetaParaTramitesDeAcselResponse>(_CrearEstructuraDeCarpetaParaTramitesDeAcselResponse_QNAME, CrearEstructuraDeCarpetaParaTramitesDeAcselResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarOTQuejasConFechaYHoraFin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "generarOTQuejasConFechaYHoraFin")
    public JAXBElement<GenerarOTQuejasConFechaYHoraFin> createGenerarOTQuejasConFechaYHoraFin(GenerarOTQuejasConFechaYHoraFin value) {
        return new JAXBElement<GenerarOTQuejasConFechaYHoraFin>(_GenerarOTQuejasConFechaYHoraFin_QNAME, GenerarOTQuejasConFechaYHoraFin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "moverDirectorioDeRutaOrigenArutaDestinoEnAlfresco")
    public JAXBElement<MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco> createMoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco(MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco value) {
        return new JAXBElement<MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco>(_MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco_QNAME, MoverDirectorioDeRutaOrigenArutaDestinoEnAlfresco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuardarMensajeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "guardarMensajeResponse")
    public JAXBElement<GuardarMensajeResponse> createGuardarMensajeResponse(GuardarMensajeResponse value) {
        return new JAXBElement<GuardarMensajeResponse>(_GuardarMensajeResponse_QNAME, GuardarMensajeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarOTQuejasConFechaYHoraFinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "generarOTQuejasConFechaYHoraFinResponse")
    public JAXBElement<GenerarOTQuejasConFechaYHoraFinResponse> createGenerarOTQuejasConFechaYHoraFinResponse(GenerarOTQuejasConFechaYHoraFinResponse value) {
        return new JAXBElement<GenerarOTQuejasConFechaYHoraFinResponse>(_GenerarOTQuejasConFechaYHoraFinResponse_QNAME, GenerarOTQuejasConFechaYHoraFinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "moverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse")
    public JAXBElement<MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse> createMoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse(MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse value) {
        return new JAXBElement<MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse>(_MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse_QNAME, MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaUsuarioAOTdesdeAcselResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "asignaUsuarioAOTdesdeAcselResponse")
    public JAXBElement<AsignaUsuarioAOTdesdeAcselResponse> createAsignaUsuarioAOTdesdeAcselResponse(AsignaUsuarioAOTdesdeAcselResponse value) {
        return new JAXBElement<AsignaUsuarioAOTdesdeAcselResponse>(_AsignaUsuarioAOTdesdeAcselResponse_QNAME, AsignaUsuarioAOTdesdeAcselResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaUsuarioAOTdesdeAcsel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "asignaUsuarioAOTdesdeAcsel")
    public JAXBElement<AsignaUsuarioAOTdesdeAcsel> createAsignaUsuarioAOTdesdeAcsel(AsignaUsuarioAOTdesdeAcsel value) {
        return new JAXBElement<AsignaUsuarioAOTdesdeAcsel>(_AsignaUsuarioAOTdesdeAcsel_QNAME, AsignaUsuarioAOTdesdeAcsel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "moverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse")
    public JAXBElement<MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse> createMoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse(MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse value) {
        return new JAXBElement<MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse>(_MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse_QNAME, MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConectorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevaOTResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "nuevaOTResponse")
    public JAXBElement<NuevaOTResponse> createNuevaOTResponse(NuevaOTResponse value) {
        return new JAXBElement<NuevaOTResponse>(_NuevaOTResponse_QNAME, NuevaOTResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CerrarTareaTramites }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "cerrarTareaTramites")
    public JAXBElement<CerrarTareaTramites> createCerrarTareaTramites(CerrarTareaTramites value) {
        return new JAXBElement<CerrarTareaTramites>(_CerrarTareaTramites_QNAME, CerrarTareaTramites.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarFlujosActivosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "recuperarFlujosActivosResponse")
    public JAXBElement<RecuperarFlujosActivosResponse> createRecuperarFlujosActivosResponse(RecuperarFlujosActivosResponse value) {
        return new JAXBElement<RecuperarFlujosActivosResponse>(_RecuperarFlujosActivosResponse_QNAME, RecuperarFlujosActivosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CambiarContraseniaProveedorPorRFC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "cambiarContraseniaProveedorPorRFC")
    public JAXBElement<CambiarContraseniaProveedorPorRFC> createCambiarContraseniaProveedorPorRFC(CambiarContraseniaProveedorPorRFC value) {
        return new JAXBElement<CambiarContraseniaProveedorPorRFC>(_CambiarContraseniaProveedorPorRFC_QNAME, CambiarContraseniaProveedorPorRFC.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignarUsuarioOT }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "asignarUsuarioOT")
    public JAXBElement<AsignarUsuarioOT> createAsignarUsuarioOT(AsignarUsuarioOT value) {
        return new JAXBElement<AsignarUsuarioOT>(_AsignarUsuarioOT_QNAME, AsignarUsuarioOT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "moverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector")
    public JAXBElement<MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector> createMoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector(MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector value) {
        return new JAXBElement<MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector>(_MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector_QNAME, MoverDirectorioDeRutaOrigenArutaDestinoEnAlfrescoCifsConector.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearEstructuraDeCarpetaParaTramitesDeAcsel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "crearEstructuraDeCarpetaParaTramitesDeAcsel")
    public JAXBElement<CrearEstructuraDeCarpetaParaTramitesDeAcsel> createCrearEstructuraDeCarpetaParaTramitesDeAcsel(CrearEstructuraDeCarpetaParaTramitesDeAcsel value) {
        return new JAXBElement<CrearEstructuraDeCarpetaParaTramitesDeAcsel>(_CrearEstructuraDeCarpetaParaTramitesDeAcsel_QNAME, CrearEstructuraDeCarpetaParaTramitesDeAcsel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarOTAcselResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "actualizarOTAcselResponse")
    public JAXBElement<ActualizarOTAcselResponse> createActualizarOTAcselResponse(ActualizarOTAcselResponse value) {
        return new JAXBElement<ActualizarOTAcselResponse>(_ActualizarOTAcselResponse_QNAME, ActualizarOTAcselResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarOTAcselResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "generarOTAcselResponse")
    public JAXBElement<GenerarOTAcselResponse> createGenerarOTAcselResponse(GenerarOTAcselResponse value) {
        return new JAXBElement<GenerarOTAcselResponse>(_GenerarOTAcselResponse_QNAME, GenerarOTAcselResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarOTTramitesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "generarOTTramitesResponse")
    public JAXBElement<GenerarOTTramitesResponse> createGenerarOTTramitesResponse(GenerarOTTramitesResponse value) {
        return new JAXBElement<GenerarOTTramitesResponse>(_GenerarOTTramitesResponse_QNAME, GenerarOTTramitesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CambiarContraseniaProveedorPorRFCResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "cambiarContraseniaProveedorPorRFCResponse")
    public JAXBElement<CambiarContraseniaProveedorPorRFCResponse> createCambiarContraseniaProveedorPorRFCResponse(CambiarContraseniaProveedorPorRFCResponse value) {
        return new JAXBElement<CambiarContraseniaProveedorPorRFCResponse>(_CambiarContraseniaProveedorPorRFCResponse_QNAME, CambiarContraseniaProveedorPorRFCResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarOrdenDeTrabajoPorId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "recuperarOrdenDeTrabajoPorId")
    public JAXBElement<RecuperarOrdenDeTrabajoPorId> createRecuperarOrdenDeTrabajoPorId(RecuperarOrdenDeTrabajoPorId value) {
        return new JAXBElement<RecuperarOrdenDeTrabajoPorId>(_RecuperarOrdenDeTrabajoPorId_QNAME, RecuperarOrdenDeTrabajoPorId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CerrarOTAcselResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "cerrarOTAcselResponse")
    public JAXBElement<CerrarOTAcselResponse> createCerrarOTAcselResponse(CerrarOTAcselResponse value) {
        return new JAXBElement<CerrarOTAcselResponse>(_CerrarOTAcselResponse_QNAME, CerrarOTAcselResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignarUsuarioOTResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "asignarUsuarioOTResponse")
    public JAXBElement<AsignarUsuarioOTResponse> createAsignarUsuarioOTResponse(AsignarUsuarioOTResponse value) {
        return new JAXBElement<AsignarUsuarioOTResponse>(_AsignarUsuarioOTResponse_QNAME, AsignarUsuarioOTResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CerrarOTAcsel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "cerrarOTAcsel")
    public JAXBElement<CerrarOTAcsel> createCerrarOTAcsel(CerrarOTAcsel value) {
        return new JAXBElement<CerrarOTAcsel>(_CerrarOTAcsel_QNAME, CerrarOTAcsel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarOrdenDeTrabajoPorIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "recuperarOrdenDeTrabajoPorIdResponse")
    public JAXBElement<RecuperarOrdenDeTrabajoPorIdResponse> createRecuperarOrdenDeTrabajoPorIdResponse(RecuperarOrdenDeTrabajoPorIdResponse value) {
        return new JAXBElement<RecuperarOrdenDeTrabajoPorIdResponse>(_RecuperarOrdenDeTrabajoPorIdResponse_QNAME, RecuperarOrdenDeTrabajoPorIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarOTTramites }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "generarOTTramites")
    public JAXBElement<GenerarOTTramites> createGenerarOTTramites(GenerarOTTramites value) {
        return new JAXBElement<GenerarOTTramites>(_GenerarOTTramites_QNAME, GenerarOTTramites.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PucWSException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "PucWSException")
    public JAXBElement<PucWSException> createPucWSException(PucWSException value) {
        return new JAXBElement<PucWSException>(_PucWSException_QNAME, PucWSException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CerrarTareaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "cerrarTareaResponse")
    public JAXBElement<CerrarTareaResponse> createCerrarTareaResponse(CerrarTareaResponse value) {
        return new JAXBElement<CerrarTareaResponse>(_CerrarTareaResponse_QNAME, CerrarTareaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarOTQuejasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "generarOTQuejasResponse")
    public JAXBElement<GenerarOTQuejasResponse> createGenerarOTQuejasResponse(GenerarOTQuejasResponse value) {
        return new JAXBElement<GenerarOTQuejasResponse>(_GenerarOTQuejasResponse_QNAME, GenerarOTQuejasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarOTQuejas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "generarOTQuejas")
    public JAXBElement<GenerarOTQuejas> createGenerarOTQuejas(GenerarOTQuejas value) {
        return new JAXBElement<GenerarOTQuejas>(_GenerarOTQuejas_QNAME, GenerarOTQuejas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevaOTConFechaFinDeOT }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "nuevaOTConFechaFinDeOT")
    public JAXBElement<NuevaOTConFechaFinDeOT> createNuevaOTConFechaFinDeOT(NuevaOTConFechaFinDeOT value) {
        return new JAXBElement<NuevaOTConFechaFinDeOT>(_NuevaOTConFechaFinDeOT_QNAME, NuevaOTConFechaFinDeOT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CerrarTarea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "cerrarTarea")
    public JAXBElement<CerrarTarea> createCerrarTarea(CerrarTarea value) {
        return new JAXBElement<CerrarTarea>(_CerrarTarea_QNAME, CerrarTarea.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevaOT }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "nuevaOT")
    public JAXBElement<NuevaOT> createNuevaOT(NuevaOT value) {
        return new JAXBElement<NuevaOT>(_NuevaOT_QNAME, NuevaOT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarFlujosActivos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.habilgroup.puc.ws.service", name = "recuperarFlujosActivos")
    public JAXBElement<RecuperarFlujosActivos> createRecuperarFlujosActivos(RecuperarFlujosActivos value) {
        return new JAXBElement<RecuperarFlujosActivos>(_RecuperarFlujosActivos_QNAME, RecuperarFlujosActivos.class, null, value);
    }

}
