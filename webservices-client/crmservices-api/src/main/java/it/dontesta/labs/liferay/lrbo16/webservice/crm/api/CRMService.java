package it.dontesta.labs.liferay.lrbo16.webservice.crm.api;

import java.io.File;
import java.util.List;

import javax.jws.WebParam;

import aQute.bnd.annotation.ProviderType;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.exception.CRMServiceException;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.model.Customer;

/**
 * 
 * @author amusarra
 *
 */
@ProviderType
public interface CRMService {
	
	
    public Integer generarOTQuejas(String username, String cveFlujo, String nombreClasificador,
            List<String> campos, String observaciones, List<File> archivos) throws CRMServiceException;
	
	/**
	 * 
	 * @return
	 */
	public Integer createOTDefault() throws CRMServiceException;
	
}